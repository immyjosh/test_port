"use strict";
const jwt = require("jsonwebtoken");
const middlewares = require("../model/middlewares");
const CONFIG = require("../config/config");
const db = require("../model/mongodb");
const library = require("../model/library");

const ensureAuthorized = (req, res, next) => {
  // console.log("req.headers", req.headers.authorization);
  let token;
  if (!req.headers.authorization) {
    token = library.jwtSign({ username: req.headers.user, role: "agency" });
  } else {
    token = req.headers.authorization;
  }

  if (token) {
    jwt.verify(token, CONFIG.SECRET_KEY, (err, decoded) => {
      // console.log('TCL: ensureAuthorized -> err, decoded', err, decoded);
      if (err || !decoded.username) {
        res.send("Unauthorized Access");
      } else {
        db.GetOneDocument("agencies", { username: decoded.username, status: 1 }, {}, {}, (err, response) => {
          // console.log('TCL: ensureAuthorized -> err, response', err, response);
          if (err || !response) {
            res.send("Unauthorized Access");
          } else {
            req.params.loginId = response._id;
            req.params.loginData = response;
            next();
          }
        });
      }
    });
  } else {
    res.send("Unauthorized Access");
  }
};

module.exports = (app, io) => {
  try {
    /* Agency*/
    const agency = require("../controller/agency/agency")(app, io);
    app.post("/agencylogin", agency.login);
    app.post("/agencylogout", ensureAuthorized, agency.logout);
    app.post("/agencydashboard", ensureAuthorized, agency.agencydashboard);
    app.post("/agency/notifications", ensureAuthorized, agency.notifications);
    app.post("/agency/forgotpassword", agency.forgotpassword);
    app.post("/agency/resetpassword/:userid/:reset", agency.resetpassword);
    app.post("/agency/profile", ensureAuthorized, agency.profile);
    app.post("/agency/settings/save", ensureAuthorized, agency.settings);
    app.post("/agency/clearnotification", ensureAuthorized, agency.clearnotification);
    app.post("/agency/profile/deactivate", ensureAuthorized, agency.deactivateprofile);
    app.post("/agency/profile/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_AGENCY).fields([{ name: "avatar", maxCount: 1 }]), agency.saveprofile);
    app.post("/agency/company/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_AGENCY).fields([{ name: "company_logo", maxCount: 1 }]), agency.savecomapny);

    /* client*/
    const client = require("../controller/agency/client")(app, io);
    app.post("/administrators/client/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_CLIENT).fields([{ name: "avatar", maxCount: 1 }, { name: "company_logo", maxCount: 1 }]), client.save);
    app.post("/administrators/client/list", ensureAuthorized, client.list);
    app.post("/administrators/client/get", ensureAuthorized, client.get);
    app.post("/administrators/client/viewclientlocations", ensureAuthorized, client.viewclientlocations);
    app.get("/administrators/client/userexport", ensureAuthorized, client.userexport);
    app.post("/agency/client/locations", ensureAuthorized, client.client_locations);
    app.post("/agency/client/addbranch", ensureAuthorized, client.addbranch);
    app.post("/agency/client/deletebranch", ensureAuthorized, client.deletebranch);
    app.post("/agency/client/updatebranch", ensureAuthorized, client.updatebranch);
    app.post("/agency/client/actasclient", ensureAuthorized, client.actasclient);
    app.post("/agency/client/dashboard", ensureAuthorized, client.minidashboard);
    app.post("/agency/client/profile", ensureAuthorized, client.profile);

    const employee = require("../controller/agency/employee")(app, io);
    app.post("/administrators/employee/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{ name: "avatar", maxCount: 1 }]), employee.save);
    app.post("/administrators/employee/staffsave", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{ name: "staff_photo", maxCount: 1 }]), employee.staffsave);
    app.post("/administrators/employee/list", ensureAuthorized, employee.list);
    app.post("/administrators/employee/get", ensureAuthorized, employee.get);
    app.post("/administrators/employee/show_employees", ensureAuthorized, employee.show_employees);
    app.get("/administrators/employee/userexport", ensureAuthorized, employee.userexport);
    app.post("/agency/employee/update_availabilty", ensureAuthorized, employee.update_availabilty);
    app.post("/agency/employee/delete_availabilty", ensureAuthorized, employee.delete_availabilty);
    app.post("/agency/employee/actasemployee", ensureAuthorized, employee.actasemployee);
    app.post("/agency/employee/downloadpdf", ensureAuthorized, employee.downloadpdf);
    app.post("/agency/employee/dashboard", ensureAuthorized, employee.minidashboard);
    app.post("/agency/employee/profile", ensureAuthorized, employee.profile);
    app.post("/agency/employee/shiftlist", ensureAuthorized, employee.shiftlist);
    app.post("/agency/employee/view_shift", ensureAuthorized, employee.view_shift);
    app.post("/agency/employee/daysoff/forshiftslist", ensureAuthorized, employee.forshiftslist);
    app.post("/agency/employeedashboard", ensureAuthorized, employee.employeedashboard);
    app.post("/agency/employeerating", ensureAuthorized, employee.employeerating);

    const jobtype = require("../controller/agency/jobtype")(app, io);
    app.post("/agency/jobtype/save", ensureAuthorized, jobtype.save);
    app.post("/agency/jobtype/list", ensureAuthorized, jobtype.list);
    app.post("/agency/jobtype/common/list", ensureAuthorized, jobtype.CommonList);
    app.post("/agency/jobtype/filter/list", ensureAuthorized, jobtype.filterlist);
    app.post("/agency/jobtype/edit", ensureAuthorized, jobtype.edit);
    app.get("/agency/jobtype/jobtypeexport", ensureAuthorized, jobtype.jobtypeexport);

    const clientjobtype = require("../controller/agency/clientjobtype")(app, io);
    app.post("/agency/client/jobtype/add", ensureAuthorized, clientjobtype.add);
    app.post("/agency/client/jobtype/remove", ensureAuthorized, clientjobtype.remove);
    app.post("/agency/client/jobtype/save", ensureAuthorized, clientjobtype.save);
    app.post("/agency/client/jobtype/list", ensureAuthorized, clientjobtype.list);
    app.post("/agency/client/jobtype/edit", ensureAuthorized, clientjobtype.edit);
    app.get("/agency/client/jobtype/jobtypeexport", clientjobtype.jobtypeexport);

    const employeejobtype = require("../controller/agency/employeejobtype")(app, io);
    app.post("/agency/employee/jobtype/add", ensureAuthorized, employeejobtype.add);
    app.post("/agency/employee/jobtype/remove", ensureAuthorized, employeejobtype.remove);
    app.post("/agency/employee/jobtype/save", ensureAuthorized, employeejobtype.save);
    app.post("/agency/employee/jobtype/list", ensureAuthorized, employeejobtype.list);
    app.post("/agency/employee/jobtype/normallist", ensureAuthorized, employeejobtype.normallist);
    app.post("/agency/employee/jobtype/edit", ensureAuthorized, employeejobtype.edit);
    app.get("/agency/employee/jobtype/jobtypeexport", employeejobtype.jobtypeexport);

    const shifttemplate = require("../controller/agency/shifttemplate")(app, io);
    app.post("/agency/shifttemplate/save", ensureAuthorized, shifttemplate.save);
    app.post("/agency/shifttemplate/list", ensureAuthorized, shifttemplate.list);
    app.post("/agency/shifttemplate/edit", ensureAuthorized, shifttemplate.edit);
    app.get("/agency/shifttemplate/shifttemplateexport", ensureAuthorized, shifttemplate.shifttemplateexport);

    const shift = require("../controller/agency/shift")(app, io);
    app.post("/agency/shift/save", ensureAuthorized, shift.save);
    // app.post('/agency/shift/multiplesave',ensureAuthorized, shift.multiplesave);
    app.post("/agency/shift/list", ensureAuthorized, shift.list);
    app.post("/agency/shift/edit", ensureAuthorized, shift.edit);
    app.post("/agency/shift/view", ensureAuthorized, shift.view);
    app.post("/agency/send_shift_requests", ensureAuthorized, shift.shift_request);
    app.post("/agency/open_request_to_all", ensureAuthorized, shift.open_request_to_all);
    app.post("/agency/assign_shift", ensureAuthorized, shift.assign_shift);
    app.post("/agency/unassign_shift", ensureAuthorized, shift.unassignShift);
    app.post("/agency/shift/get_employees", ensureAuthorized, shift.get_employees);
    app.get("/agency/shift/shiftexport", shift.shiftexport);
    app.post("/agency/shift/deleteshift", ensureAuthorized, shift.deleteshift);

    const location = require("../controller/agency/location")(app, io);
    app.post("/agency/location/save", ensureAuthorized, location.save);
    app.post("/agency/location/list", ensureAuthorized, location.list);
    app.post("/agency/location/edit", ensureAuthorized, location.edit);
    app.get("/agency/location/locationexport", ensureAuthorized, location.locationexport);

    const subscription = require("../controller/agency/subscriptions")(app, io);
    app.post("/agency/subscription/subscribe", ensureAuthorized, subscription.subscribe);
    app.post("/agency/subscription/payment", ensureAuthorized, subscription.payment);
    // app.post('/agency/subscription/list', ensureAuthorized, subscription.list);
    // app.post('/agency/subscription/edit', ensureAuthorized, subscription.edit);
    app.post("/agency/subscription/subscriptions", ensureAuthorized, subscription.subscriptions);
    app.post("/agency/subscription/accountdetails", ensureAuthorized, subscription.accountDetails);
    app.post("/agency/subscription/downloadpdf", ensureAuthorized, subscription.downloadpdf);
    app.post("/agency/subscription/subscriptions/update", ensureAuthorized, subscription.UpdateAccount);
    app.post("/agency/subscription/subscriptions/delete", ensureAuthorized, subscription.DeleteAccount);
    app.post("/agency/subscription/subscriptions/autorenewal", ensureAuthorized, subscription.Autorenewal);
    app.get("/agency/subscription/subscriptionexport", subscription.subscriptionexport);

    const timesheet = require("../controller/agency/timesheet")(app, io);
    app.post("/agency/timesheet/save", ensureAuthorized, timesheet.save);
    app.post("/agency/timesheet/list", ensureAuthorized, timesheet.list);
    app.post("/agency/timesheet/employee/list", ensureAuthorized, timesheet.listforemp);
    app.post("/agency/timesheet/list/groupbyemployee", ensureAuthorized, timesheet.groupbyemployee);
    app.post("/agency/timesheet/edit", ensureAuthorized, timesheet.edit);
    app.post("/agency/timesheet/view", ensureAuthorized, timesheet.view);
    app.post("/agency/timesheet/approval", ensureAuthorized, timesheet.approval);
    app.post("/agency/timesheet/groupapproval", ensureAuthorized, timesheet.groupapproval);
    app.post("/agency/timesheet/timesheetexport",ensureAuthorized, timesheet.timesheetexport);
    app.post("/agency/timesheet/downloadpdf", ensureAuthorized, timesheet.downloadpdf);
    app.post("/agency/timesheet/mailpdf", ensureAuthorized, timesheet.mailpdf);
    app.post("/agency/timesheetdates", ensureAuthorized, timesheet.timesheetdates);

    const invoice = require("../controller/agency/invoice")(app, io);
    app.post("/agency/invoice/update", ensureAuthorized, invoice.update);
    app.post("/agency/invoice/list", ensureAuthorized, invoice.list);
    app.post("/agency/invoice/view", ensureAuthorized, invoice.view);
    app.post("/agency/invoice/approval", ensureAuthorized, invoice.approval);
    app.post("/agency/invoice/groupapproval", ensureAuthorized, invoice.groupapproval);
    app.post("/agency/invoice/employee/list", ensureAuthorized, invoice.listbyemployee);
    app.post("/agency/invoice/list/groupbyemployee", ensureAuthorized, invoice.groupbyemployee);
    app.post("/agency/invoice/export", ensureAuthorized, invoice.export);
    app.post("/agency/invoice/downloadpdf", ensureAuthorized, invoice.downloadpdf);
    app.post("/agency/invoice/addpayment", ensureAuthorized, invoice.addpayment);
    app.post("/agency/invoice/getinvoicecycle", ensureAuthorized, invoice.getinvoicecycle);
    app.post("/agency/invoicecycle/update", ensureAuthorized, invoice.invoicecycleupdate);
    app.post("/agency/invociedates", ensureAuthorized, invoice.invociedates);

    const daysoff = require("../controller/agency/daysoff")(app, io);
    app.post("/agency/daysoff/save", ensureAuthorized, daysoff.save);
    app.post("/agency/daysoff/list", ensureAuthorized, daysoff.list);
    app.get("/agency/daysoff/forshiftslist", ensureAuthorized, daysoff.forshiftslist);
    app.post("/agency/daysoff/edit", ensureAuthorized, daysoff.edit);
    app.get("/agency/daysoff/daysoffexport", ensureAuthorized, daysoff.daysoffexport);

    const commonform = require("../controller/agency/onlineform")(app, io);
    app.post("/agency/employees/apply-online-form/update", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{ name: "UploadIDPhoto", maxCount: 1 }, { name: "UploadCV", maxCount: 1 }, { name: "uploadcertificate", maxCount: 1 }, { name: "uploadpassport", maxCount: 1 }, { name: "uploadaddress", maxCount: 1 }, { name: "uploaddbs", maxCount: 1 }]), commonform.formupdate);
    app.post("/agency/employees/apply-online-form/list", ensureAuthorized, commonform.list);
    app.post("/agency/employees/apply-online-form/verify", ensureAuthorized, commonform.verification);
    app.post("/agency/employees/apply-online-form/interviewcall", ensureAuthorized, commonform.interviewcall);
    app.post("/agency/employees/apply-online-form/postinterviewcall", ensureAuthorized, commonform.postinterviewcall);
    app.post("/agency/employees/send-referee-form", ensureAuthorized, commonform.sendrefereeform);
    app.post("/agency/employees/send-mail-interview-call", ensureAuthorized, commonform.sendmailinterviewcall);
    app.post("/agency/employees/referee-form/verify", ensureAuthorized, commonform.verifyrefereeform);
    app.post("/agency/employees/referee-form/update", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{name: "campanystamp", maxCount: 1}]), commonform.referenceupdate);
    app.post("/agency/employees/recruitment/final-verify", ensureAuthorized, commonform.finalverification);
    app.get("/agency/employees/recruitment/export", ensureAuthorized, commonform.userexport);

    const ctimesheet = require("../controller/agency/clienttimesheet")(app, io);
    app.post("/agency/clienttimesheet/save", ensureAuthorized, ctimesheet.save);
    app.post("/agency/clienttimesheet/list", ensureAuthorized, ctimesheet.list);
    app.post("/agency/clienttimesheet/edit", ensureAuthorized, ctimesheet.edit);
    app.post("/agency/clienttimesheet/view", ensureAuthorized, ctimesheet.view);
    app.post("/agency/clienttimesheet/employee/list", ensureAuthorized, ctimesheet.listforemp);
    app.post("/agency/clienttimesheet/list/groupbyemployee", ensureAuthorized, ctimesheet.groupbyemployee);
    app.post("/agency/clienttimesheet/downloadpdf", ensureAuthorized, ctimesheet.downloadpdf);
    app.post("/agency/clienttimesheetdates", ensureAuthorized, ctimesheet.timesheetdates);
    app.post("/agency/clienttimesheet/list/export", ensureAuthorized, ctimesheet.timesheetexport);

    const cinvoice = require("../controller/agency/clientinvoice")(app, io);
    app.post("/agency/clientinvoice/list", ensureAuthorized, cinvoice.list);
    app.post("/agency/clientinvoice/view", ensureAuthorized, cinvoice.view);
    app.post("/agency/clientinvoice/employee/list", ensureAuthorized, cinvoice.listbyemployee);
    app.post("/agency/clientinvoice/list/groupbyemployee", ensureAuthorized, cinvoice.groupbyemployee);
    app.post("/agency/clientinvoice/export", ensureAuthorized, cinvoice.export);
    app.post("/agency/clientinvoice/downloadpdf", ensureAuthorized, cinvoice.downloadpdf);
    app.post("/agency/clientinvoice/getinvoicecycle", ensureAuthorized, cinvoice.getinvoicecycle);
    app.post("/agency/clientinvociedates", ensureAuthorized, cinvoice.invociedates);
    app.post("/agency/clientinvoice/list/export", ensureAuthorized, cinvoice.invoiceexport);

    const etimesheet = require("../controller/agency/employeetimesheet")(app, io);
    app.post("/agency/employeetimesheet/employee/list", ensureAuthorized, etimesheet.listforemp);
    app.post("/agency/employeetimesheet/list/groupbyemployee", ensureAuthorized, etimesheet.groupbyemployee);
    app.post("/agency/employeetimesheet/downloadpdf", ensureAuthorized, etimesheet.downloadpdf);
    app.post("/agency/employeetimesheetdates", ensureAuthorized, etimesheet.timesheetdates);
    app.post("/agency/employeetimesheet/list/timesheetexport", ensureAuthorized, etimesheet.timesheetexport);

    const einvoice = require("../controller/agency/employeeinvoice")(app, io);
    app.post("/agency/employeeinvoice/employee/list", ensureAuthorized, einvoice.listbyemployee);
    app.post("/agency/employeeinvoice/list/groupbyemployee", ensureAuthorized, einvoice.groupbyemployee);
    app.post("/agency/employeeinvoice/downloadpdf", ensureAuthorized, einvoice.downloadpdf);
    app.post("/agency/employeeinvociedates", ensureAuthorized, einvoice.invociedates);
    app.post("/agency/employeeinvoice/invoiceexport", ensureAuthorized, einvoice.invoiceexport);

    const emailtemplate = require("../controller/agency/emailtemplate")(app, io);
    app.post("/agency/emailtemplate/save", ensureAuthorized, emailtemplate.save);
    app.post("/agency/emailtemplate/list", ensureAuthorized, emailtemplate.list);
    app.post("/agency/emailtemplate/edit", ensureAuthorized, emailtemplate.edit);
    app.post("/agency/emailtemplate/selecttypedata", ensureAuthorized, emailtemplate.selecttypedata);
    app.post("/agency/emailtemplate/getemailcontent", ensureAuthorized, emailtemplate.getemailcontent);
    app.post("/agency/emailtemplate/defaultstatus", ensureAuthorized, emailtemplate.defaultstatus);
    app.post("/agency/emailtemplate/delete", ensureAuthorized, emailtemplate.delete);
  } catch (e) {
    console.log("error in index.js--->>>>", e);
  }
};
