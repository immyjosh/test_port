"use strict";
const jwt = require("jsonwebtoken");
const middlewares = require("../model/middlewares");
const CONFIG = require("../config/config");
const db = require("../model/mongodb");
let library = require("../model/library");

let ensureAuthorized = (req, res, next) => {
  let token;
  if (!req.headers.authorization) {
    token = library.jwtSign({ username: req.headers.user, role: "client" });
  } else {
    token = req.headers.authorization;

  }

  if (token) {
    jwt.verify(token, CONFIG.SECRET_KEY, (err, decoded) => {
      if (err || !decoded.username) {
        res.send("Unauthorized Access");
      } else {
        db.GetOneDocument("clients", { username: decoded.username, status: 1 }, {}, {}, (err, response) => {
          if (err || !response) {
            res.send("Unauthorized Access");
          } else {
            req.params.loginId = response._id;
            req.params.loginData = response;
            next();
          }
        });
      }
    });
  } else {
    res.send("Unauthorized Access");
  }
};

module.exports = (app, io) => {
  try {
    /* Client*/
    const client = require("../controller/client/client")(app, io);
    app.post("/clientlogin", client.login);
    app.post("/clientlogout", ensureAuthorized, client.logout);
    app.post("/clientdashboard", ensureAuthorized, client.clientdashboard);
    app.post("/client/notifications", ensureAuthorized, client.notifications);
    app.post("/client/profile", ensureAuthorized, client.profile);
    app.post("/client/profile/deactivate", ensureAuthorized, client.deactivatesave);
    app.post("/client/profile/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_CLIENT).fields([{ name: "avatar", maxcount: 1 }]), client.profilesave);
    app.post("/client/forgotpassword", client.forgotpassword);
    app.post("/client/resetpassword/:userid/:reset", client.resetpassword);
    // app.post("/client/jobtypes", ensureAuthorized, client.jobtypes);
    // app.post("/client/job_requests", ensureAuthorized, client.job_requests);
    app.post("/client/settings/save", ensureAuthorized, client.settingssave);
    app.post("/client/settings/job_types", ensureAuthorized, client.job_types);
    app.post("/client/clearnotification", ensureAuthorized, client.clearnotification);

    const shifttemplate = require("../controller/client/shifttemplate")(app, io);
    app.post("/client/shifttemplate/save", ensureAuthorized, shifttemplate.save);
    app.post("/client/shifttemplate/list", ensureAuthorized, shifttemplate.list);
    app.post("/client/shifttemplate/edit", ensureAuthorized, shifttemplate.edit);

    const shift = require("../controller/client/shift")(app, io);
    app.post("/client/shift/save", ensureAuthorized, shift.save);
    app.post("/client/shift/list", ensureAuthorized, shift.list);
    app.post("/client/shift/edit", ensureAuthorized, shift.edit);
    app.post("/client/shift/view", ensureAuthorized, shift.view);
    app.post("/client/send_shift_requests", ensureAuthorized, shift.shift_request);
    app.post("/client/assign_shift", ensureAuthorized, shift.assign_shift);
    app.post("/client/unassign_shift", ensureAuthorized, shift.unassignShift);
    app.post("/client/job_types", ensureAuthorized, shift.job_types);
    app.post("/client/shift/get_employees", ensureAuthorized, shift.get_employees);
    app.post("/client/open_request_to_all", ensureAuthorized, shift.open_request_to_all);
    app.get("/client/shift/shiftexport", shift.shiftexport);

    const location = require("../controller/client/location")(app, io);
    app.post("/client/location/save", ensureAuthorized, location.save);
    app.post("/client/location/list", ensureAuthorized, location.list);
    app.post("/client/location/edit", ensureAuthorized, location.edit);

    const timesheet = require("../controller/client/timesheet")(app, io);
    app.post("/client/timesheet/save", ensureAuthorized, timesheet.save);
    app.post("/client/timesheet/list", ensureAuthorized, timesheet.list);
    app.post("/client/timesheet/edit", ensureAuthorized, timesheet.edit);
    app.post("/client/timesheet/view", ensureAuthorized, timesheet.view);
    app.post("/client/timesheet/approval", ensureAuthorized, timesheet.approval);
    app.post("/client/timesheet/employee/list", ensureAuthorized, timesheet.listforemp);
    app.post("/client/timesheet/list/groupbyemployee", ensureAuthorized, timesheet.groupbyemployee);
    app.post("/client/timesheet/groupapproval", ensureAuthorized, timesheet.groupapproval);
    app.post("/client/timesheet/downloadpdf", ensureAuthorized, timesheet.downloadpdf);
    app.post("/client/timesheetdates", ensureAuthorized, timesheet.timesheetdates);
    app.post("/client/timesheet/export", ensureAuthorized, timesheet.export);
    
    const invoice = require("../controller/client/invoice")(app, io);
    app.post("/client/invoice/list", ensureAuthorized, invoice.list);
    app.post("/client/invoice/view", ensureAuthorized, invoice.view);
    app.post("/agency/invoice/approval", ensureAuthorized, invoice.approval);
    app.post("/client/invoice/groupapproval", ensureAuthorized, invoice.groupapproval);
    app.post("/client/invoice/employee/list", ensureAuthorized, invoice.listbyemployee);
    app.post("/client/invoice/list/groupbyemployee", ensureAuthorized, invoice.groupbyemployee);
    app.post("/client/invoice/export", ensureAuthorized, invoice.export);
    app.post("/client/invoice/downloadpdf", ensureAuthorized, invoice.downloadpdf);
    app.post("/client/invoice/getinvoicecycle", ensureAuthorized, invoice.getinvoicecycle);
    app.post("/client/invociedates", ensureAuthorized, invoice.invociedates);
    
    const clientjobtype = require("../controller/client/jobtype")(app, io);
    app.post("/client/jobtype/add", ensureAuthorized, clientjobtype.add);
    app.post("/client/jobtype/remove", ensureAuthorized, clientjobtype.remove);
    app.post("/client/jobtype/list", ensureAuthorized, clientjobtype.list);
    app.post("/client/jobtype/edit", ensureAuthorized, clientjobtype.edit);
    app.get("/client/jobtype/jobtypeexport", clientjobtype.jobtypeexport);

    const daysoff = require("../controller/client/daysoff")(app, io);
    app.get("/client/daysoff/forshiftslist", ensureAuthorized, daysoff.forshiftslist);

    const employee = require("../controller/client/employee")(app, io);
    app.post("/client/employee/downloadpdf", ensureAuthorized, employee.downloadpdf);
  } catch (e) {
    console.log("error in index.js--->>>>", e);
  }
};
