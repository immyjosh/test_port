"use strict";
const jwt = require("jsonwebtoken");
const middlewares = require("../model/middlewares");
const CONFIG = require("../config/config");
const db = require("../model/mongodb");
const library = require("../model/library");

let ensureAuthorized = (req, res, next) => {
  let token;
  if (!req.headers.authorization) {
    token = library.jwtSign({ username: req.headers.user, role: "admin" });
  } else {
    token = req.headers.authorization;
  }

  if (token) {
    jwt.verify(token, CONFIG.SECRET_KEY, (err, decoded) => {
      if (err || !decoded.username) {
        // console.log("err || !decoded.username",err ,decoded);
        res.send("Unauthorized Access");
      } else {
        db.GetOneDocument("administrators", { username: decoded.username, status: 1 }, {}, {}, (err, response) => {
          if (err || !response) {
            // console.log("err || !response ",err, response );
            res.send("Unauthorized Access");
          } else {
            req.params.loginId = response._id;
            req.params.loginData = response;
            next();
          }
        });
      }
    });
  } else {
    res.send("Unauthorized Access");
  }
};

module.exports = (app, io) => {
  try {
    /* Administrators */
    const administrators = require("../controller/administrators/administrators")(app, io);
    app.post("/adminlogin", administrators.login);
    app.post("/adminlogout", ensureAuthorized, administrators.logout);
    app.post("/admindashboard", ensureAuthorized, administrators.admindashboard);
    app.post("/administrators/notifications", ensureAuthorized, administrators.notifications);
    app.post("/administrators/administrators/save", ensureAuthorized, administrators.save);
    app.post("/administrators/administrators/list", ensureAuthorized, administrators.list);
    app.post("/administrators/administrators/edit", ensureAuthorized, administrators.edit);
    app.get("/administrators/administrators/userexport", administrators.userexport);
    app.post("/administrators/forgotpassword", administrators.forgotpassword);
    app.post("/admin/resetpassword/:userid/:reset", administrators.resetpassword);
    app.post("/admin/clearnotification", ensureAuthorized, administrators.clearnotification);

    /* Agency*/
    const agency = require("../controller/administrators/agency")(app, io);
    app.post("/administrators/agency/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_AGENCY).fields([{ name: "avatar", maxCount: 1 }, { name: "company_logo", maxCount: 1 }]), agency.save);
    app.post("/administrators/agency/list", ensureAuthorized, agency.list);
    app.post("/administrators/agency/edit", ensureAuthorized, agency.edit);
    app.get("/administrators/agency/userexport", ensureAuthorized, agency.userexport);
    app.post("/administrators/agency/actasagency", agency.actasagency);
    app.post("/administrators/agency/dashboard", agency.minidashboard);
    app.post("/administrators/agency/profile", agency.profile);
    app.post("/admin/agency/timesheetexport", ensureAuthorized, agency.timeexport);
    app.post("/admin/agency/invoiceexport", ensureAuthorized, agency.invoiceeexport);

    /* Settings*/
    const settings = require("../controller/administrators/settings")(app, io);
    app.post("/administrators/settings/:name", ensureAuthorized, settings.list);
    app.post("/administrators/timezones", ensureAuthorized, settings.timezones);
    // app.post('/administrators/settings/:name/save', ensureAuthorized, settings.save);
    // app.post('/administrators/settings/general/save', ensureAuthorized, settings.generalsave);
    app.post("/administrators/settings/general/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_OTHERS).fields([{ name: "logo", maxCount: 1 }, { name: "light_logo", maxCount: 1 }, { name: "favicon", maxCount: 1 }]), settings.generalsave);
    app.post("/administrators/settings/smtp/save", ensureAuthorized, settings.smtpsave);
    app.post("/administrators/settings/seo/save", settings.saveseo);
    app.post("/administrators/settings/sms/save", settings.savesms);
    app.post("/administrators/settings/social_networks/save", settings.savesocial_networks);
    app.post("/administrators/settings/payment_gateway/save", settings.savepayment_gateway);
    app.post(
      "/administrators/settings/mobile/save",
      middlewares.commonUpload(CONFIG.DIRECTORY_OTHERS).fields([{ name: "user_pem_files", maxCount: 1 }, { name: "tasker_pem_files", maxCount: 1 }]),
      settings.savemobile
    );

    /* plans*/
    const plans = require("../controller/administrators/plans")(app, io);
    app.post("/administrators/plans/save", ensureAuthorized, plans.save);
    app.post("/administrators/plans/list", plans.list);
    app.post("/administrators/plans/get", ensureAuthorized, plans.get);
    app.post("/administrators/plans/subscribers", ensureAuthorized, plans.subscribers);
    app.post("/administrators/plans/earnings", ensureAuthorized, plans.earnings);
    app.get("/administrators/plans/userexport", plans.userexport);
    app.get("/administrators/plans/subscribersexport", plans.subscribersexport);
    app.get("/administrators/plans/earningsexport", plans.earningsexport);

    const paymentgateway = require("../controller/administrators/paymentgateway")(app, io);
    app.post("/administrators/paymentgateway/save", paymentgateway.save);
    app.post("/administrators/paymentgateway/list", paymentgateway.list);
    app.post("/administrators/paymentgateway/edit", paymentgateway.edit);

    /* Email Template*/
    const emailtemplate = require("../controller/administrators/emailtemplate")(app, io);
    app.post("/administrators/emailtemplate/save", emailtemplate.save);
    app.post("/administrators/emailtemplate/list", emailtemplate.list);
    app.post("/administrators/emailtemplate/edit", emailtemplate.edit);

    const subscription = require("../controller/administrators/subscriptions")(app, io);
    app.post("/admin/agency/subscription/subscribe", ensureAuthorized, subscription.subscribe);
    app.post("/admin/agency/subscription/payment", ensureAuthorized, subscription.payment);
    // app.post('/agency/subscription/list', ensureAuthorized, subscription.list);
    // app.post('/agency/subscription/edit', ensureAuthorized, subscription.edit);
    app.post("/admin/agency/subscription/subscriptions", ensureAuthorized, subscription.subscriptions);
    app.post("/admin/agency/subscription/accountdetails", ensureAuthorized, subscription.accountDetails);
    app.post("/admin/agency/subscription/downloadpdf", ensureAuthorized, subscription.downloadpdf);
    app.post("/admin/agency/subscription/subscriptions/update", ensureAuthorized, subscription.UpdateAccount);
    app.post("/admin/agency/subscription/subscriptions/delete", ensureAuthorized, subscription.DeleteAccount);
    app.post("/admin/agency/subscription/subscriptions/autorenewal", ensureAuthorized, subscription.Autorenewal);
    app.get("/admin/agency/subscription/subscriptionexport", subscription.subscriptionexport);

    const timesheet = require("../controller/administrators/agencytimesheet")(app, io);
    app.post("/admin/agencytimesheet/save", ensureAuthorized, timesheet.save);
    app.post("/admin/agencytimesheet/list", ensureAuthorized, timesheet.list);
    app.post("/admin/agencytimesheet/employee/list", ensureAuthorized, timesheet.listforemp);
    app.post("/admin/agencytimesheet/list/groupbyemployee", ensureAuthorized, timesheet.groupbyemployee);
    app.post("/admin/agencytimesheet/edit", ensureAuthorized, timesheet.edit);
    app.post("/admin/agencytimesheet/view", ensureAuthorized, timesheet.view);
    app.get("/admin/agencytimesheet/timesheetexport", ensureAuthorized, timesheet.timesheetexport);
    app.post("/admin/agencytimesheet/downloadpdf", ensureAuthorized, timesheet.downloadpdf);
    app.post("/admin/agencytimesheet/mailpdf", ensureAuthorized, timesheet.mailpdf);
    app.post("/admin/agencytimesheetdates", ensureAuthorized, timesheet.timesheetdates);

    const invoice = require("../controller/administrators/agencyinvoice")(app, io);
    app.post("/admin/agencyinvoice/update", ensureAuthorized, invoice.update);
    app.post("/admin/agencyinvoice/list", ensureAuthorized, invoice.list);
    app.post("/admin/agencyinvoice/view", ensureAuthorized, invoice.view);
    app.post("/admin/agencyinvoice/employee/list", ensureAuthorized, invoice.listbyemployee);
    app.post("/admin/agencyinvoice/list/groupbyemployee", ensureAuthorized, invoice.groupbyemployee);
    app.post("/admin/agencyinvoice/export", ensureAuthorized, invoice.export);
    app.post("/admin/agencyinvoice/downloadpdf", ensureAuthorized, invoice.downloadpdf);
    app.post("/admin/agencyinvoice/addpayment", ensureAuthorized, invoice.addpayment);
    app.post("/admin/agencyinvoice/getinvoicecycle", ensureAuthorized, invoice.getinvoicecycle);
    app.post("/admin/agencyinvoicecycle/update", ensureAuthorized, invoice.invoicecycleupdate);
    app.post("/admin/agencyinvociedates", ensureAuthorized, invoice.invociedates);
  } catch (e) {
    console.log("error in index.js--->>>>", e);
  }
};
