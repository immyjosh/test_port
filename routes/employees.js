"use strict";
const jwt = require("jsonwebtoken");
const middlewares = require("../model/middlewares");
const CONFIG = require("../config/config");
const db = require("../model/mongodb");
const library = require("../model/library");

const ensureAuthorized = (req, res, next) => {
  /* const token = req.headers.authorization;*/
  let token;
  if (!req.headers.authorization) {
    token = library.jwtSign({ username: req.headers.user, role: "employee" });
  } else {
    token = req.headers.authorization;
  }
  if (token) {
    jwt.verify(token, CONFIG.SECRET_KEY, (err, decoded) => {
      if (err || !decoded.username) {
        res.send("Unauthorized Access");
      } else {
        db.GetOneDocument("employees", { username: decoded.username, status: 1 }, {}, {}, (err, response) => {
          if (err || !response) {
            res.send("Unauthorized Access");
          } else {
            req.params.loginId = response._id;
            req.params.loginData = response;
            // console.log("req.params",req.params);
            next();
          }
        });
      }
    });
  } else {
    res.send("Unauthorized Access");
  }
};

module.exports = (app, io) => {
  try {
    /* employee*/

    const employee = require("../controller/employees/employees")(app, io);

    app.post("/employeelogin", employee.login);
    app.post("/employeelogout", ensureAuthorized, employee.logout);
    app.post("/employee/notification", ensureAuthorized, employee.employeenotifications);
    app.post("/employeedashboard", ensureAuthorized, employee.employeedashboard);
    app.post("/employee/profile", ensureAuthorized, employee.profile);
    app.post("/employee/forgotpassword", employee.forget_password);
    app.post("/employee/resetpassword/:userid/:reset", employee.resetpassword);
    app.post("/employee/profiledetails", ensureAuthorized, employee.profileDetails);
    app.post("/employee/profile/save", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{ name: "avatar", maxCount: 1 }]), employee.saveprofile);
    app.post("/employee/update_availabilty", ensureAuthorized, employee.update_availabilty);
    app.post("/employee/delete_availabilty", ensureAuthorized, employee.delete_availabilty);
    app.post("/employee/settings/notifications/save", ensureAuthorized, employee.notificationssettings);
    app.post("/employee/clearnotification", ensureAuthorized, employee.clearnotification);

    // action - start,hold,restart,end
    app.post("/shift/update/:action", ensureAuthorized, employee.change_shift);
    app.post("/employee/shiftlist", ensureAuthorized, employee.shiftlist);
    app.post("/employee/view_shift", ensureAuthorized, employee.view_shift);
    app.post("/employee/job_requests", ensureAuthorized, employee.job_requests);
    // app.post("/employee/accept_job", ensureAuthorized, employee.accept_job);
    app.post("/employee/accept_job", ensureAuthorized, employee.assign_shift);
    app.post("/employee/unassignshift", ensureAuthorized, employee.unassignShift);
    app.post("/employee/timesheet_request", ensureAuthorized, employee.timesheet_request);
    app.post("/employee/earnings", ensureAuthorized, employee.earnings);
    app.post("/employee/earnings/export", ensureAuthorized, employee.earningsexport);
    app.post("/employee/earnings/view", ensureAuthorized, employee.earningsview);
    app.post("/employee/agencydetails", ensureAuthorized, employee.agencies);

    app.post("/employee/forget_password", employee.forget_password);
    app.post("/employee/resetpassword/:userid/:reset", employee.resetpassword);

    const daysoff = require("../controller/employees/daysoff")(app, io);
    app.get("/employee/daysoff/forshiftslist", ensureAuthorized, daysoff.forshiftslist);

    const timesheet = require("../controller/employees/timesheet")(app, io);
    app.post("/employee/timesheet/employee/list", ensureAuthorized, timesheet.listforemp);
    app.post("/employee/timesheet/list/groupbyemployee", ensureAuthorized, timesheet.groupbyemployee);
    app.post("/employee/timesheet/timesheetexport", ensureAuthorized, timesheet.timesheetexport);
    app.post("/employee/timesheet/downloadpdf", ensureAuthorized, timesheet.downloadpdf);
    app.post("/employee/timesheetdates", ensureAuthorized, timesheet.timesheetdates);

    const invoice = require("../controller/employees/invoice")(app, io);
    app.post("/employee/invoice/employee/list", ensureAuthorized, invoice.listbyemployee);
    app.post("/employee/invoice/list/groupbyemployee", ensureAuthorized, invoice.groupbyemployee);
    app.post("/employee/invoice/export", ensureAuthorized, invoice.export);
    app.post("/employee/invoice/downloadpdf", ensureAuthorized, invoice.downloadpdf);
    app.post("/employee/invociedates", ensureAuthorized, invoice.invociedates);

    const commonform = require("../controller/employees/onlineform")(app, io);
    // app.post("/employees/apply-online-form", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).any(), commonform.formupdate);
    app.post("/employees/apply-online-form", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{ name: "UploadIDPhoto", maxCount: 1 }, { name: "UploadCV", maxCount: 1 }, { name: "uploadcertificate", maxCount: 1 }, { name: "uploadpassport", maxCount: 1 }, { name: "uploadaddress", maxCount: 1 }, { name: "uploaddbs", maxCount: 1 }]), commonform.formupdate);
    app.post("/employees/apply-online-form/save-files", ensureAuthorized, middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{ name: "uploadfiles", maxCount: 1 }]), commonform.savefiles);
    app.post("/employees/apply-online-form/delete-files", ensureAuthorized, commonform.deletefiles);
    app.post("/employees/send-online-form", ensureAuthorized, commonform.formsubmit);
    app.post("/employees/onlineform/downloadpdf", ensureAuthorized, commonform.downloadpdf);
  } catch (e) {
    console.log("error in index.js--->>>>", e);
  }
};
