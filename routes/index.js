const path = require("path");
const CONFIG = require("../config/config");

module.exports = (app, passport, io) => {
  try {
    const administrators = require("./administrators")(app, io);
    const admin = require("./agency")(app, io);
    const client = require("./client")(app, io);
    const employee = require("./employees")(app, io);
    const user = require("./site")(app, io);
    // const image=require('./image')(app,io);

    app.get("/", function(req, res) {
      res.sendFile(path.join(__dirname, "../public/index.html"));
    });
    /* app.get("/portal", function (req, res) {
            res.sendFile(path.join(__dirname, "../public/portal/index.html"));
        });*/
    app.get("/portal/*", function(req, res) {
      res.sendFile(path.join(__dirname, "../public/portal/index.html"));
    });

    app.get("/*", function(req, res) {
      res.sendFile(path.join(__dirname, "../public/index.html"));
    });
  } catch (e) {
    console.log("Error in Router", e);
  }
};
