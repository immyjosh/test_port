"use strict";
const jwt = require("jsonwebtoken");
const middlewares = require("../model/middlewares");
const CONFIG = require("../config/config");
const db = require("../model/mongodb");
let library = require("../model/library");

module.exports = (app, io) => {
  try {
    const site = require("../controller/site/site")(app, io);
    app.post("/register/agency", middlewares.commonUpload(CONFIG.DIRECTORY_AGENCY).fields([{name: "avatar", maxCount: 1}]), site.agency_registration);
    app.post("/register/client", middlewares.commonUpload(CONFIG.DIRECTORY_CLIENT).fields([{name: "avatar", maxCount: 1}]), site.client_registration);
    app.post("/register/employee", middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{name: "avatar", maxCount: 1}]), site.employee_registration);

    app.post("/site/agencies", site.agencies);
    app.post("/site/settings", site.site_settings);
    app.post("/site/timesheetdates", site.timesheetdates);
    app.post("/site/invociedates", site.invociedates);
    app.post("/site/timesheetprefixnumber", site.timesheetprefixnumber);
    app.post("/site/invoiceprefixnumber", site.invoiceprefixnumber);
    app.post("/site/employee/reference/save", middlewares.commonUpload(CONFIG.DIRECTORY_EMPLOYEE).fields([{name: "campanystamp", maxCount: 1}]), site.referencesave);
  } catch (e) {
    console.log("error in index.js--->>>>", e);
  }
};
