let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let agencySchema = mongoose.Schema(
  {
    admin: { type: Schema.ObjectId, ref: "administrators" },
    username: { type: String, lowercase: true, index: { unique: true }, trim: true },
    email: { type: String, lowercase: true, index: { unique: true }, trim: true },
    name: String,
    password: { type: String, required: true },
    status: Number,
    phone: { code: String, number: String, dailcountry: String },
    address: { line1: String, line2: String, city: String, state: String, country: String, zipcode: String, formatted_address: String },
    avatar: String,
    activity: { last_login: { type: Date, default: Date.now }, last_logout: { type: Date, default: Date.now } },
    company_name: String,
    company_phone: { code: String, number: String, dailcountry: String },
    company_description: String,
    postal_address: String,
    registration_number: String,
    organisation_type: String,
    fax: String,
    vat_number: String,
    surname: String,
    company_logo: String,
    company_email: { type: String, trim: true },
    privileges: [],
    reset_code: String,
    isverified: { type: Number, default: 0 },
    settings: {
      notifications: { email: { type: Boolean, default: true }, sms: { type: Boolean, default: true } },
      general: { shift_request_expiry: { type: Number, default: 3600 }, cancel_shift: { type: Number, default: 3600 }, invoice_due_days: { type: Number, default: 7 } },
      shift: { prefix: { type: String, uppercase: true, trim: true }, number: { type: Number, trim: true }, count: Number },
      timesheet: { prefix: { type: String, uppercase: true, trim: true }, number: { type: Number, trim: true }, count: Number },
      invoice: { prefix: { type: String, uppercase: true, trim: true }, number: { type: Number, trim: true }, count: Number }
    },
    customer_id: String,
    auto_renewal: { type: Number, default: 0 },
    bank_details: {}
  },
  { timestamps: true, versionKey: false }
);

let agencies = mongoose.model("agencies", agencySchema, "agencies");
module.exports = agencies;
