const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const plansSchema = mongoose.Schema(
  {
    name: { type: String, index: { unique: true }, trim: true },
    freedays: Number,
    noofdays: Number,
    employees: Number,
    amount: Number,
    description: String,
    type: Number,
    createdby: { type: Schema.ObjectId, ref: "administrators" },
    status: Number,
    recruitment_module: { type: Number, default: 0 },
    remainder: Number,
    availability: Number,
  },
  { timestamps: true, versionKey: false }
);
const plans = mongoose.model("plans", plansSchema, "plans");
module.exports = plans;
