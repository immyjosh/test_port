let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let settingSchema = mongoose.Schema(
  {
    alias: {type: String, unique: true},
    settings: {}
  },
  {timestamps: true, versionKey: false}
);

let settings = mongoose.model("settings", settingSchema, "settings");
module.exports = settings;
