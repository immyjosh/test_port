let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let shiftSchema = mongoose.Schema(
  {
    addedBy: String,
    addedId: {type: Schema.ObjectId},
    agency: {type: Schema.ObjectId, ref: "agencies"},
    client: {type: Schema.ObjectId, ref: "clients"},
    branch: {type: Schema.ObjectId, ref: "clients"},
    timesheetID: {type: String, ref: "timesheetcycle"},
    shiftId: String,
    title: String,
    locations: {type: Schema.ObjectId, ref: "locations"},
    job_type: {type: Schema.ObjectId, ref: "jobtypes"},
    starttime: Number,
    endtime: Number,
    breaktime: Number,
    start_date: Date,
    end_date: Date,
    requested_time: Date,
    shift_type: String,
    shift_option: String,
    option_data: [],
    status: Number,
    latefee: {type: Boolean, default: false},
    employee_requested: [
      {
        employee: {type: Schema.ObjectId, ref: "employees", dropDups: true},
        status: {type: Number, default: 0}
      }
    ],
    employee: {type: Schema.ObjectId, ref: "employees"},
    employee_rate: Number,
    agency_rate: Number,
    client_rate: Number,
    timesheet_status: {type: Number, default: 0},
    timesheet: [
      {
        start: Date,
        end: Date,
        date: Date,
        holdedmins: Number,
        hold: Date,
        jobmins: Number,
        workmins: Number,
        breaks: [
          {
            hold: Date,
            restart: Date,
            holdedmins: Number
          }
        ]
      }
    ],
    agency_comment: String,
    agency_rating: {type: Number, default: 0},
    client_comment: String,
    client_rating: Number,
    employee_fare: Number,
    agency_fare: Number,
    client_fare: Number,
    late_amount: Number,
    notes: String,
  },
  {timestamps: true, versionKey: false}
);
let shifts = mongoose.model("shifts", shiftSchema, "shifts");
module.exports = shifts;
