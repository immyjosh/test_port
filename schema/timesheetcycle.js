let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let TimeSchema = mongoose.Schema(
  {
    addedBy: String,
    addedId: {type: Schema.ObjectId},
    agency: {type: Schema.ObjectId, ref: "agencies"},
    client: {type: Schema.ObjectId, ref: "clients"},
    employee: {type: Schema.ObjectId, ref: "employees"},
    shifts: [{type: Schema.ObjectId, ref: "shifts", index: {unique: true}}],
    timesheetID: {type: String, index: {unique: true}},
    from_date: String,
    to_date: String
  },
  {timestamps: true, versionKey: false}
);
let timesheetcycle = mongoose.model("timesheetcycle", TimeSchema, "timesheetcycle");
module.exports = timesheetcycle;
