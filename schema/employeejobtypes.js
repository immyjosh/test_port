let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let employeejobSchema = mongoose.Schema(
  {
    name: String,
    jobrate: Number,
    rate_details: [{name: String, from: Number, to: Number, client_rate: Number, employee_rate: Number}],
    late_fee: {amount: {type: Number, default: 0}, duration: {type: Number, default: 0}},
    status: Number,
    job_id: {type: Schema.ObjectId, ref: "jobtypes"},
    employee: {type: Schema.ObjectId, ref: "employees"},
    addedBy: String,
    addedId: {type: Schema.ObjectId},
    privileges: [],
  },
  {timestamps: true, versionKey: false}
);
let employeejobtypes = mongoose.model("employeejobtypes", employeejobSchema, "employeejobtypes");
module.exports = employeejobtypes;
