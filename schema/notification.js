var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var notificationSchema = mongoose.Schema({
	admin: { type: Schema.ObjectId, ref: 'admin' },
	agency: { type: Schema.ObjectId, ref: 'agencies' },
	client: { type: Schema.ObjectId, ref: 'clients' },
	employee: { type: Schema.ObjectId, ref: 'employees' },
	shift: { type: Schema.ObjectId, ref: 'shifts' },
	type : String, 
	action : String, 
	message : String, 
	raw_data: {},
	status: { type: Number, default: 1 }
}, { timestamps: true, versionKey: false });

var notifications = mongoose.model('notifications', notificationSchema, 'notifications');
module.exports = notifications;