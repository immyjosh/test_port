let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let invoicecycleschema = mongoose.Schema(
  {
    addedBy: String,
    addedId: {type: Schema.ObjectId},
    agency: {type: Schema.ObjectId, ref: "agencies"},
    client: {type: Schema.ObjectId, ref: "clients"},
    employee: {type: Schema.ObjectId, ref: "employees"},
    shifts: [{type: Schema.ObjectId, ref: "shifts", index: {unique: true}}],
    invoiceids: [{type: Schema.ObjectId, ref: "invoice", index: {unique: true}}],
    invoiceID: {type: String, index: {unique: true}},
    due_date: String,
    from_date: String,
    to_date: String,
    payment_details: [
      {
        date: {type: Date, Default: new Date()},
        amount: Number,
        through: String,
        reference: String
      }
    ],
    status: {type: Number, Default: 0}
  },
  {timestamps: true, versionKey: false}
);
let invoicecycle = mongoose.model("invoicecycle", invoicecycleschema, "invoicecycle");
module.exports = invoicecycle;
