const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const emailsSchema = mongoose.Schema(
  {
    heading: String,
    default_mail: Boolean,
    type: String,
    mail_def: String,
    agency: { type: Schema.ObjectId, ref: "agencies" },
    email_subject: String,
    sender_name: String,
    sender_email: String,
    email_content: String,
    description: String,
    status: Number,
    subscription: { type: Number, default: 0 }
  },
  { timestamps: true, versionKey: false }
);
const agencyemailtemplate = mongoose.model("agencyemailtemplate", emailsSchema, "agencyemailtemplate");
module.exports = agencyemailtemplate;
