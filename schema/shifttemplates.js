var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shiftSchema = mongoose.Schema({
    name:String,
    from:String,
    to:String,
    break:String,
    status:Number,
    addedBy:String,
    addedId:{ type: Schema.ObjectId },
},{ timestamps: true, versionKey: false});

var shifttemplates = mongoose.model('shifttemplates',shiftSchema,'shifttemplates');
module.exports = shifttemplates;