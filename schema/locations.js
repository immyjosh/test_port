const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const locationSchema = mongoose.Schema(
  {
    name: { type: String, trim: true },
    // phone:{
    //     code:String,
    //     number:String,
    // },
    address: String,
    color: String,
    radius: Number,
    geolocation: {
      lat: Number,
      lng: Number
    },
    // zipcode:String,
    status: Number,
    addedBy: String,
    addedId: { type: Schema.ObjectId }
  },
  { timestamps: true, versionkey: false }
);
const locations = mongoose.model("locations", locationSchema, "locations");
module.exports = locations;
