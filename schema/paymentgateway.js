var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var gatewaySchema = mongoose.Schema({
	alias: { type: String, lowercase: true, index: { unique: true }, trim: true },
	gateway_name:String,
	settings:{},
	status:Number
}, { timestamps: true, versionKey: false });
var paymentgateway = mongoose.model('paymentgateway', gatewaySchema, 'paymentgateway');
module.exports = paymentgateway;