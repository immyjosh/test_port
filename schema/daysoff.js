let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let daysoffschema = mongoose.Schema(
  {
    name: {type: String, index: {unique: true}},
    numberofday: Number,
    dates: [],
    reason: String,
    status: Number,
    addedBy: String,
    addedId: {type: Schema.ObjectId}
  },
  {timestamps: true, versionKey: false}
);

let daysoff = mongoose.model("daysoff", daysoffschema, "daysoff");
module.exports = daysoff;
