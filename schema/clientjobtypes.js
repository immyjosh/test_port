let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let clientjobSchema = mongoose.Schema(
  {
    name: String,
    jobrate: Number,
    rate_details: [{name: String, from: Number, to: Number, client_rate: Number, employee_rate: Number}],
    late_fee: {amount: {type: Number, default: 0}, duration: {type: Number, default: 0}},
    status: Number,
    job_id: {type: Schema.ObjectId, ref: "jobtypes"},
    client: {type: Schema.ObjectId, ref: "clients"},
    addedBy: String,
    addedId: {type: Schema.ObjectId},
    privileges: [],
  },
  {timestamps: true, versionKey: false}
);
let clientjobtypes = mongoose.model("clientjobtypes", clientjobSchema, "clientjobtypes");
module.exports = clientjobtypes;
