let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let jobSchema = mongoose.Schema(
  {
    name: String,
    jobrate: Number,
    rate_details: [{name: String, from: Number, to: Number, client_rate: Number, employee_rate: Number}],
    late_fee: {amount: {type: Number, default: 0}, duration: {type: Number, default: 0}},
    status: Number,
    addedBy: String,
    addedId: {type: Schema.ObjectId},
    job_id: {type: Schema.ObjectId, ref: "jobtypes"},
    client: {type: Schema.ObjectId, ref: "clients"},
    employee: {type: Schema.ObjectId, ref: "employees"},
    privileges: [],
  },
  {timestamps: true, versionKey: false}
);
let jobtypes = mongoose.model("jobtypes", jobSchema, "jobtypes");
module.exports = jobtypes;
