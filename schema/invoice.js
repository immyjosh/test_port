let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let invoiceSchema = mongoose.Schema(
  {
    shift_id: {type: String, index: {unique: true}, trim: true},
    agency: {type: Schema.ObjectId, ref: "agencies"},
    client: {type: Schema.ObjectId, ref: "clients"},
    job_type: {type: Schema.ObjectId, ref: "jobtypes"},
    employee: {type: Schema.ObjectId, ref: "employees"},
    locations: {type: Schema.ObjectId, ref: "locations"},
    branch: {type: Schema.ObjectId, ref: "clients"},
    shift_mid: {type: Schema.ObjectId, ref: "shifts", index: {unique: true}},
    timesheetID: {type: String, ref: "timesheetcycle"},
    invoiceID: {type: String, ref: "invoicecycle"},
    calc_schema: String,
    starttime: Number,
    start_date: Date,
    endtime: Number,
    agency_rate: Number,
    client_rate: Number,
    employee_rate: Number,
    late_amount: Number,
    rate_details: [{name: String, from: Number, to: Number, client_rate: Number, employee_rate: Number}],
    late_fee: {amount: Number, duration: Number},
    notes: {status: Number, msg: String},
    due_date: Date,
    status: {type: Number, Default: 1},
    addedId: {type: Schema.ObjectId}
  },
  {timestamps: true, versionkey: false}
);
let invoice = mongoose.model("invoice", invoiceSchema, "invoice");
module.exports = invoice;
