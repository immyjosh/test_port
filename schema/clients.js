let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let clientSchema = mongoose.Schema(
  {
    agency: {type: Schema.ObjectId, ref: "agencies"},
    username: {type: String, lowercase: true, index: {unique: true}, trim: true},
    email: {type: String, lowercase: true, index: {unique: true}, trim: true},
    name: String,
    password: {type: String, required: true},
    status: Number,
    isverified: Number,
    role: String,
    phone: {
      code: String,
      number: String,
      dailcountry: String
    },
    address: {
      line1: String,
      line2: String,
      city: String,
      state: String,
      country: String,
      zipcode: String,
      formatted_address: String,
      lat: Number,
      lng: Number,
      location: {type: Schema.ObjectId, ref: "locations"}
    },
    avatar: String,
    activity: {
      last_login: {type: Date, default: Date.now},
      last_logout: {type: Date, default: Date.now}
    },
    privileges: [],
    establishmenttype: String,
    numberofbeds: Number,
    patienttype: String,
    specialrequirements: String,
    shiftpatens: String,
    agreedstaffrates: String,
    attain: String,
    invoiceaddress: String,
    invoicephone: Number,
    invoicefax: String,
    invoiceemail: String,
    additionallocations: String,
    subscription: String,
    companyname: String,
    company_email: String,
    company_logo: String,
    fax: Number,
    locations: [{type: Schema.ObjectId, ref: "locations"}],
    additional_locations: [{type: Schema.ObjectId, ref: "locations"}],
    branches: [
      {
        branchname: String,
        branchaddress: String,
        branchline1: String,
        branchline2: String,
        branchcity: String,
        branchstate: String,
        branchcountry: String,
        branchzipcode: String,
        branchformatted_address: String,
        branchlat: Number,
        branchlng: Number,
        status: Number,
        branchlocation: {type: Schema.ObjectId, ref: "locations"},
        phone: {code: String, number: String,dailcountry:String}
      }
    ],
    settings: {
      notifications: {
        email: {type: Boolean, default: true},
        sms: {type: Boolean, default: true}
      },
      job_roles: [{_id: false, value: {type: Schema.ObjectId, ref: "job_types"}, label: String}]
    }
  },
  {timestamps: true, versionKey: false}
);

let clients = mongoose.model("clients", clientSchema, "clients");
module.exports = clients;
