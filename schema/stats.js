const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const statsSchema = mongoose.Schema(
  {
    agency: { type: Schema.ObjectId, ref: "agencies" },
    client: { type: Schema.ObjectId, ref: "clients" },
    employee: { type: Schema.ObjectId, ref: "employees" },
    shiftID: { type: Schema.ObjectId, ref: "shifts" },
    timesheetID: { type: String, ref: "timesheetcycle" },
    invoiceID: { type: String, ref: "invoicecycle" },
    locations: { type: Schema.ObjectId, ref: "locations" },
    branch: { type: Schema.ObjectId, ref: "clients" },
    job_type: { type: Schema.ObjectId, ref: "jobtypes" },
    employee_rate: Number,
    agency_rate: Number,
    client_rate: Number,
    starttime: Number,
    endtime: Number,
    start_date: Date,
    status: Number,
    date: String,
    type: String
  },
  { timestamps: true, versionKey: false }
);
const stats = mongoose.model("stats", statsSchema, "stats");
module.exports = stats;
