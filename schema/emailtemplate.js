const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const emailSchema = mongoose.Schema(
  {
    name: { type: String, index: { unique: true }, trim: true },
    slug: { type: String, index: { unique: true }, trim: true },
    email_subject: String,
    sender_name: String,
    sender_email: String,
    email_content: String,
    description: String,
    status: Number,
    subscription: { type: Number, default: 0 }
  },
  { timestamps: true, versionKey: false }
);
const emailtemplate = mongoose.model("emailtemplate", emailSchema, "emailtemplate");
module.exports = emailtemplate;
