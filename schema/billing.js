const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const billingSchema = mongoose.Schema(
  {
    agency: { type: Schema.ObjectId, ref: "agencies" },
    billingcycyle: String,
    type: String,
    start_date: { type: Date, default: Date.now },
    end_date: { type: Date, default: Date.now },
    status: { type: Number, default: 1 }
  },
  { timestamps: true, versionKey: false }
);
const stats = mongoose.model("billing", billingSchema, "billing");
module.exports = stats;
