let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let subscriptionSchema = mongoose.Schema(
  {
    agency: {type: Schema.ObjectId, ref: "agencies"},
    planId: {type: Schema.ObjectId, ref: "plans"},
    plan: {},
    status: Number,
    type: String,
    start: Date,
    end: Date,
    remainder: Date,
    charge_id: String,
    payment_status: Number,
    payment_response: String,
    account_details: {}
  },
  {timestamps: true, versionKey: false}
);
let subscriptions = mongoose.model("subscriptions", subscriptionSchema, "subscriptions");
module.exports = subscriptions;
