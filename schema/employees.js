const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const employeeSchema = mongoose.Schema(
  {
    agency: { type: Schema.ObjectId, ref: "agencies" },
    job_type: [{ type: Schema.ObjectId, ref: "jobtypes" }],
    locations: [{ type: Schema.ObjectId, ref: "locations" }],
    username: { type: String, lowercase: true, index: { unique: true }, trim: true },
    email: { type: String, lowercase: true, index: { unique: true }, trim: true },
    name: String,
    password: { type: String, required: true },
    status: Number,
    role: String,
    phone: {
      code: String,
      number: String,
      dailcountry: String
    },
    reset_code: String,
    employee_rate: Number,
    agency_rate: Number,
    client_rate: Number,
    address: {
      line1: String,
      line2: String,
      city: String,
      state: String,
      country: String,
      zipcode: String,
      formatted_address: String,
      lat: Number,
      lon: Number
    },
    holiday_allowance: Number,
    joining_date: Date,
    final_date: Date,
    avatar: String,
    activity: {
      last_login: { type: Date, default: Date.now },
      last_logout: { type: Date, default: Date.now }
    },
    isverified: { type: Number, default: 0 },
    settings: {
      notifications: {
        email: { type: Boolean, default: true },
        sms: { type: Boolean, default: true }
      }
    },
    available: [
      {
        _id: false,
        day: String,
        from: Number,
        to: Number,
        status: { type: Number, default: 0 }
      }
    ],
    timeoff: [
      {
        day: String,
        from: Number,
        to: Number,
        date: { type: Date }
      }
    ],
    shifts_accepted: [],
    roleapplyfor: String,
    staffprofile: {},
    // apply online form
    onlineform: {
      // tab 1:
      postapplyfor: String,
      postapplydetails: String,
      // formcreateddate: Date,
      availablestartdate: Date,
      // personal details
      personaldetails: {
        title: String,
        firstname: String,
        middlename: String,
        lastname: String,
        homephone: Number,
        mobileno: {
          code: String,
          number: String,
          dialcountry: String
        },
        email: String,
        dateofbirth: Date,
        insuranceno: Number,
        address: {
          streetAddress: String,
          addressline2: String,
          city: String,
          state: String,
          zip: String,
          country: String,
          label: String,
          title: String,
          lat: Number,
          lng: Number
        },
        nationality: String,
        gender: String,
        religion: String,
        race: String,
        sexualorientation: String
      },
      UploadIDPhoto: String,
      // employment eligibility
      employmenteligibility: {
        visapermit: String,
        visapermittype: String
      },
      // Driving Details
      drivingdetails: {
        drivinglicence: String,
        drivinglicenceno: String,
        traveldetails: String,
        caraccess: String,
        bannedfromdriving: String,
        carinsurance: String,
        vehicledocumentsvalid: String
      },
      // Languages
      langauages: {
        englishspoken: String,
        englishwritten: String,
        otherlanguage: String,
        IELTStest: String
      },
      // Next of kin details
      nextofkindetails: {
        kinprefix: String,
        kinfirstname: String,
        kinlastname: String,
        kinrelationship: String,
        kinhomephone: Number,
        kinmobileno: {
          code: String,
          number: String,
          dialcountry: String
        },
        kinemail: String,
        Address: {
          kinstreetAddress: String,
          kinaddressline2: String,
          kincity: String,
          kinstate: String,
          kinzip: String,
          kincountry: String
        }
      },
      // About Your Work
      aboutyourwork: {
        abletowork: {
          work_mornings: Boolean,
          work_evenings: Boolean,
          work_afternoons: Boolean,
          work_occasional: Boolean,
          work_fulltime: Boolean,
          work_parttime: Boolean,
          work_nights: Boolean,
          work_weekends: Boolean,
          work_anytime: Boolean
        },
        firstchoice: String,
        secondchoice: String,
        thirdchoice: String,
        shortnotice: String,
        reduceworkflexibility: String,
        workstate: String,
        worktime: String
      },
      // tab 2:
      // Employment History (array)
      employmenthistory: [
        {
          employer: String,
          position: String,
          duties: String,
          start_date: Date,
          end_date: Date,
          salaryonleaving: String
        }
      ],
      // Education (array)
      education: [
        {
          institution: String,
          course: String,
          year: Number,
          grade: String
        }
      ],
      UploadCV: String,
      // Reference 1
      reference1: {
        firstrefrelationship: String,
        firstreffirstname: String,
        firstreflastname: String,
        firstrefphone: {
          code: String,
          number: String,
          dialcountry: String
        },
        firstrefemail: String,
        firstrefconfirmemail: String,
        Address: {
          firstrefstreetAddress: String,
          firstrefcity: String,
          firstrefstate: String,
          firstrefzip: String
        },
        referee: {
          refereefirstname: String,
          refereelastname: String,
          refereeemail: String,
          refereephone: {
            code: String,
            number: String,
            dialcountry: String
          },
          refereejobtitle: String,
          refereecompany: String,
          refereecapacity: String,
          applicantfirstname: String,
          applicantlastname: String,
          applicantemail: String,
          applicantphone: {
            code: String,
            number: String,
            dialcountry: String
          },
          applicantjobtitle: String,
          applicantcompany: String,
          employedfrom: String,
          employedto: String,
          remployee: String,
          remployeenotes: String,
          followcareplans: String,
          reliability: String,
          character: String,
          dignity: String,
          attitude: String,
          communication: String,
          relationships: String,
          workunderinitiative: String,
          disciplinaryaction: String,
          disciplinaryactiondetails: String,
          investigations: String,
          investigationsdetails: String,
          vulnerablepeople: String,
          vulnerablepeopledetails: String,
          criminaloffence: String,
          criminaloffencedetails: String,
          additionalcomments: String,
          confirm: String,
          agree: String,
          campanystamp: String,
          submitted_date: Date,
          verified_date: Date,
          verified_notes: String,
          verified_status: { type: Number, default: 0 },
          status: { type: Number, default: 0 }
        }
      },
      // Reference 2
      reference2: {
        secondrefrelationship: String,
        secondreffirstname: String,
        secondreflastname: String,
        secondrefphone: {
          code: String,
          number: String,
          dialcountry: String
        },
        secondrefemail: String,
        secondrefconfirmemail: String,
        Address: {
          secondrefstreetAddress: String,
          secondrefcity: String,
          secondrefstate: String,
          secondrefzip: String
        },
        referee: {
          refereefirstname: String,
          refereelastname: String,
          refereeemail: String,
          refereephone: {
            code: String,
            number: String,
            dialcountry: String
          },
          refereejobtitle: String,
          refereecompany: String,
          refereecapacity: String,
          applicantfirstname: String,
          applicantlastname: String,
          applicantemail: String,
          applicantphone: {
            code: String,
            number: String,
            dialcountry: String
          },
          applicantjobtitle: String,
          applicantcompany: String,
          employedfrom: String,
          employedto: String,
          remployee: String,
          remployeenotes: String,
          followcareplans: String,
          reliability: String,
          character: String,
          dignity: String,
          attitude: String,
          communication: String,
          relationships: String,
          workunderinitiative: String,
          disciplinaryaction: String,
          disciplinaryactiondetails: String,
          investigations: String,
          investigationsdetails: String,
          vulnerablepeople: String,
          vulnerablepeopledetails: String,
          criminaloffence: String,
          criminaloffencedetails: String,
          additionalcomments: String,
          confirm: String,
          agree: String,
          campanystamp: String,
          submitted_date: Date,
          verified_date: Date,
          verified_notes: String,
          verified_status: { type: Number, default: 0 },
          status: { type: Number, default: 0 }
        }
      },
      // tab3:
      mandatory_training: String,
      movinghandling: Boolean,
      basiclifesupport: Boolean,
      healthsafety: Boolean,
      firesafety: Boolean,
      firstaid: Boolean,
      infectioncontrol: Boolean,
      foodsafety: Boolean,
      medicationadmin: Boolean,
      safeguardingvulnerable: Boolean,
      trainingdates: String,
      healthcare_training: String,
      diplomal3: Boolean,
      diplomal2: Boolean,
      personalsafetymental: Boolean,
      intermediatelife: Boolean,
      advancedlife: Boolean,
      complaintshandling: Boolean,
      handlingviolence: Boolean,
      doolsmental: Boolean,
      coshh: Boolean,
      dataprotection: Boolean,
      equalityinclusion: Boolean,
      loneworkertraining: Boolean,
      resuscitation: Boolean,
      interpretation: Boolean,
      trainindates2: String,
      // tab 4:
      // Health Declaration
      healthdeclaration: {
        sufferedlongtermillness: String,
        leaveforneckinjury: String,
        neckinjuries: String,
        sixweeksillness: String,
        communicabledisease: String,
        medicalattention: String,
        healthdeclarationdetails: String,
        registereddisabled: String,
        absentfromwork: String,
        statereason: String
      },
      // GP Details
      gpdetails: {
        GPname: String,
        gpaddress: {
          GPstreetAddress: String,
          GPcity: String,
          GPstate: String,
          GPzip: String,
          contactDoctor: String,
          GPhealthdetails: String
        }
      },
      // MEDICAL HISTORY
      medicalhistory: {
        medicalillness: String,
        medicalillnesscaused: String,
        treatment: String,
        needanyadjustments: String,
        needanyadjustmentsnotes: String,
      },
      // Tuberculosis
      tuberculosis: {
        livedUK: String,
        livedUKnotes: String,
        BCGvaccination: String,
        BCGvaccinationdate: Date,
        cough: String,
        weightloss: String,
        fever: String,
        TB: String
      },
      // Chicken Pox or Shingles
      chickenpoxorshingles: {
        chickenpox: String,
        chickenpoxdate: Date,
      },
      // Immunisation History
      immunisationhistory: {
        triplevaccination: String,
        polio: String,
        tetanus: String,
        hepatitisB: String,
        proneProcedures: String
      },
      // Declaration
      declaration: {
        occupationalHealthServices: String,
        criminaloffence: String,
        warningcriminaloffence: String,
        DBSdetails: String
      },
      // Right To Work
      righttowork: {
        passport: String
      },
      // Work Time Directives
      worktimedirectives: {
        workhours: String
      },
      submitted_date: Date,
      skillcheck: {
        date: Date,
        notes: String,
        status: { type: Number, default: 0 }
      },
      healthcheck: {
        date: Date,
        notes: String,
        status: { type: Number, default: 0 }
      },
      dbscheck: {
        date: Date,
        notes: String,
        status: { type: Number, default: 0 }
      },
      interview: {
        interviewdate: Date,
        interviewnotes: String,
        videocallurl: String,
        intervieweddate: Date,
        interviewednotes: String,
        status: { type: Number, default: 0 }
      },
      uploadcertificate: [],
      uploadpassport: [],
      uploadaddress: [],
      uploaddbs: [],
      uploadothers: [],
      form_status: { type: Number, default: 0 },
      final_verify_notes: String,
      final_verify_date: Date,
      final_verify_status: { type: Number, default: 0 },
      recruitment_module: Number
    }
  },
  { timestamps: true, versionKey: false }
);

const employees = mongoose.model("employees", employeeSchema, "employees");
module.exports = employees;
