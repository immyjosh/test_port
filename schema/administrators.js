var mongoose = require("mongoose");
var schema = mongoose.Schema({
  username: { type: String, lowercase: true, index: { unique: true }, trim: true },
  email: { type: String, lowercase: true, index: { unique: true }, trim: true },
  password: String,
  name: String,
  role: String,
  status: Number,
  phone: {
      code: String,
      number: String,
  },
  privileges: [],
  activity: {
    last_login: { type: Date, default: Date.now },
    last_logout: { type: Date, default: Date.now }
  },
  reset_code: String,
}, { timestamps: true, versionKey: false });

var administrators = mongoose.model('administrators', schema, 'administrators');
module.exports = administrators;