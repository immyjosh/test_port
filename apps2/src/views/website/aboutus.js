import React, { Component, Fragment } from "react";
import Header from '../common/header';
import Footer from '../common/footer';

class aboutus extends Component {
  render() {
    return (
      <Fragment>
 <Header/>

{/* <!--Header--> */}
<header className="header-home header-home--color header-home--bottom-space">
  <div className="background background--wave">
    <div className="container">
      <div className="row header-home__about-img-wrap">
        <img className="header-home__about-img" src="../img/img_bg_about.png" alt=""/>
        <div className="col-12">
          <h2 className="header-home__title ">Meet <span
                  className="header-home__title--accent">Agency Portal</span></h2>
          <p className="header-home__description header-home__description--big header-home__description--about">
            We are Agency Portal. We are a passionate team of software developers,
            marketers, and designers focused on driving results for our clients in the fast-growing. We believe
            businesses should make customers smile.</p>
        </div>
      </div>
    </div>
  </div>
</header>
{/* <!--Header--> */}

{/* <!--What is--> */}
<section className="section section--left-content">
  <div className="container">
    <div className="row">
      <div className="col-12">
        <h3 className="section__title">What is Agency Portal</h3>
        <p className="section__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ante urna,
          posuere in urna at, aliquet fringilla nulla. Duis finibus turpis eget ex sollicitudin, eget vehicula urna
          pulvinar. Fusce placerat velit vitae sem consequat elementum. Sed aliquam aliquet nulla eu porttitor.
          Etiam egestas erat et nisi consequat gravida. Sed ut varius libero. Cras laoreet bibendum ex, non rutrum
          metus dignissim nec. Proin vel tortor orci. Aenean porttitor neque ut congue varius. Proin non tortor
          finibus felis porta fermentum. Sed molestie odio non neque dignissim, eu facilisis lorem gravida.
        </p>
      </div>
    </div>
    <hr/>
  </div>
</section>
{/* <!--What is--> */}

{/* <!--With us--> */}
<section className="section section--left-content">
  <div className="container">
    <div className="row">
      <div className="col-12">
        <h3 className="section__title">Working with us</h3>
      </div>
    </div>
    <div className="row opportunities">
      <div className="col-4 col-l-6">
        <div className="opportunities__opportunity">
          <p className="opportunities__title">Creative Space</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div className="col-4 col-l-6">
        <div className="opportunities__opportunity">
          <p className="opportunities__title">Collabaration</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div className="col-4 col-l-6">
        <div className="opportunities__opportunity">
          <p className="opportunities__title">Growing Your Career</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div className="col-4 col-l-6">
        <div className="opportunities__opportunity">
          <p className="opportunities__title">Benefits</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div className="col-4 col-l-6">
        <div className="opportunities__opportunity">
          <p className="opportunities__title">Perks</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
      <div className="col-4 col-l-6">
        <div className="opportunities__opportunity">
          <p className="opportunities__title">Cookies</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec iaculis nibh, quis venenatis odio.
            Suspendisse cursus interdum sem, ultrices interdum.</p>
        </div>
      </div>
    </div>
    <div className="row gallery">
      <div className="col-6 c gallery__wrap">
        <img alt="" className="gallery__item gallery__item--big" src="../img/img_photo_1.png"/>
      </div>
      <div className="col-6 gallery__wrap">
        <img alt="" className="gallery__item gallery__item--small" src="../img/img_photo_2.png"/>
        <img alt="" className="gallery__item gallery__item--small" src="../img/img_photo_3.png"/>
        <div className="gallery__item gallery__item--medium">
          <h3 className="gallery__item-title">2014</h3>
          <p className="gallery__item-description">Agency Portal was founded</p>
        </div>
      </div>
      <div className="col-6 gallery__wrap">
        <img alt="" className="gallery__item gallery__item--medium" src="../img/img_photo_4.png"/>
        <div className="gallery__item gallery__item--small">
          <h3 className="gallery__item-title">5,000+</h3>
          <p className="gallery__item-description">Happy Agency Portal customers</p>
        </div>
        <img alt="" className="gallery__item gallery__item--small" src="../img/img_photo_5.png"/>
      </div>
      <div className="col-6 gallery__wrap">
        <img alt="" className="gallery__item gallery__item--big" src="../img/img_photo_6.png"/>
      </div>
      <div className="col-6 gallery__wrap">
        <img alt="" className="gallery__item gallery__item--medium" src="../img/img_photo_7.png"/>
      </div>
      <div className="col-6 gallery__wrap">
        <div className="gallery__item gallery__item--medium">
          <h3 className="gallery__item-title">$48 milion</h3>
          <p className="gallery__item-description">In funding</p>
        </div>
      </div>
    </div>
    <hr/>
  </div>
</section>
{/* <!--With us--> */}

{/* <!--Leadership--> */}
<section className="section">
  <div className="container">
    <div className="row">
      <div className="col-12">
        <h3 className="section__title">Leadership</h3>
      </div>
    </div>
    <div className="row leadership">
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_1.png"/>
          <p className="leadership__name">Michael Logan</p>
          <p className="leadership__work">Founder</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_2.png"/>
          <p className="leadership__name">John Brown</p>
          <p className="leadership__work">CEO / Founder</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_3.png"/>
          <p className="leadership__name">Mike Smith</p>
          <p className="leadership__work">CEO / Founder</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_4.png"/>
          <p className="leadership__name">George Right</p>
          <p className="leadership__work">VP, Worldwide Sales</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_5.png"/>
          <p className="leadership__name">Li Chang</p>
          <p className="leadership__work">Managing Director</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_6.png"/>
          <p className="leadership__name">Jill Valentine</p>
          <p className="leadership__work">Art Director</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_7.png"/>
          <p className="leadership__name">Sarah Jonson</p>
          <p className="leadership__work">Developer</p>
        </div>
      </div>
      <div className="col-3 col-t-6">
        <div className="leadership__item">
          <img alt="" className="leadership__avatar" src="../img/img_leadership_8.png"/>
          <p className="leadership__name">Stan Cloud</p>
          <p className="leadership__work">Designer</p>
        </div>
      </div>
    </div>
    <hr/>
  </div>
</section>
{/* <!--Leadership--> */}

{/* <!--Long Way--> */}
<section className="section">
  <div className="container">
    <div className="row">
      <div className="col-12">
        <h3 className="section__title">We Have Come A Long Way</h3>
      </div>
      <div className="row events">
        <div className="col-12">
          <div className="events__event events__event--right">
            <p className="events__event-title">Our Start</p>
            <p className="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec
              iaculis nibh, quis venenatis odio. Suspendisse cursus interdum sem, ultrices interdum.</p>
          </div>
        </div>
        <div className="col-12">
          <div className="events__event events__event--left">
            <p className="events__event-title">The early days</p>
            <p className="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
              pulvinar auctor libero, vel porttitor nulla laoreet quis. Nullam scelerisque libero sit amet blandit
              interdum. Fusce quis mauris varius, lobortis lacus eleifend, hendrerit dui. Curabitur aliquam metus
              nec tempus porttitor.</p>
          </div>
        </div>
        <div className="col-12">
          <div className="events__event events__event--right">
            <p className="events__event-title">We are growing</p>
            <p className="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              In nec iaculis nibh, quis venenatis odio.</p>
          </div>
        </div>
        <div className="col-12">
          <div className="events__event events__event--left">
            <p className="events__event-title">Now</p>
            <p className="events__event-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec
              iaculis nibh, quis venenatis odio.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{/* <!--Long Way--> */}

{/* <!--We believe--> */}
<section className="section section--light section--bottom-space">
  <div className="container">
    <div className="row believe">
      <div className="col-12">
        <img alt="" src="../img/img_believe_avatar.png" className="believe__avatar"/>
        <p className="believe__name">Li Chang</p>
        <p className="believe__work">Managing Director at Agency Portal</p>
        <h4 className="believe__quote">"We believe that Agency Portal is the future of data-driven platforms. Intelligence and
          usability is becoming a major competitive advantage in enterprise software."</h4>
      </div>
    </div>
  </div>
</section>
{/* <!--We believe--> */}

{/* <!--Trusted by--> */}
<section className="section">
  <div className="container">
    <div className="row">
      <div className="col-12">
        <h3 className="section__title">Investors</h3>
      </div>
    </div>
    <div className="row logo">
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo1.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo2.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo3.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo4.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo5.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo6.png" className="img-responsive"/>
        </div>
      </div>
      
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo11.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo8.png" className="img-responsive"/>
        </div>
      </div>
<div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo9.png" className="img-responsive"/>
        </div>
      </div>
      <div className="col-2 logo__img-wrap">
        <div className="logo__img">
          <img alt="" src="../img/logo10.png" className="img-responsive"/>
        </div>
      </div>
    
    </div>
  </div>
  <img alt="" className="section__img" src="../img/img_backgroud_footer.png"/>
</section>
{/* <!--Trusted by--> */}
<Footer/>
      </Fragment>
    );
  }
}

export default aboutus;
