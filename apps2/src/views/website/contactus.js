import React, { Component, Fragment } from "react";
import Header from '../common/header';
import Footer from '../common/footer';

var sectionStyle = {
  backgroundImage: "url('../img/img_bg_form.png')"
};
class contactus extends Component {
  render() {
    return (
      <Fragment>
    <Header/>

{/* <!--Form--> */}
<section className="section section--last section--top-space">
  <div className="container">
    <div className="row">
      <div className="col-12">
        <h3 className="section__title">Take the first step to Agency Portal</h3>
      </div>
    </div>
    <div className="row form">
      <div className="col-12 col-m-12 col-offset-1">
        <div className="form__card card form__card--background" style={sectionStyle}>
          <div className="form__wrap contact-employee">
            <h4>Get the Agency Portal for free</h4>
            <p>Learn how to sell smarter and faster with Agency Portal.</p>
            <form className="form__form js-form">
              <div className="form__form-group form__form-group--small">
                <label className="form__label">First Name</label>
                <input className="form__input js-field__first-name" type="text" placeholder="John"/>
                <span className="form-validation"></span>
              </div>
              <div className="form__form-group form__form-group--small form__form-group--right">
                <label className="form__label">Last Name</label>
                <input className="form__input js-field__last-name" type="text" placeholder="Doe"/>
                <span className="form-validation"></span>
              </div>
              <div className="form__form-group form__form-group--small">
                <label className="form__label">Email</label>
                <input className="form__input js-field__email" type="email" placeholder="example@mail.com" required/>
                <span className="form-validation"></span>
              </div>
              <div className="form__form-group form__form-group--small form__form-group--right">
                <label className="form__label">Phone Number</label>
                <input className="form__input js-field__phone" type="text" placeholder="( 123 ) 456 - 7890" required/>
                <span className="form-validation"></span>
              </div>
              <div className="form__form-group">
                <label className="form__label">Company Name</label>
                <input className="form__input js-field__company" type="text" placeholder="ABCDE Corporarion"/>
                <span className="form-validation"></span>
              </div>
              <div className="form__form-group">
                <label className="form__label">Company Size (Number of Emploees)</label>
                <label className="radio-btn">
                  <input className="radio-btn__radio" type="radio" value="1-5" name="company-size" checked/>
                  <span className="radio-btn__radio-custom"></span>
                  <span className="radio-btn__label">1-5</span>
                </label>
                <label className="radio-btn">
                  <input className="radio-btn__radio" type="radio" value="6-50" name="company-size"/>
                  <span className="radio-btn__radio-custom"></span>
                  <span className="radio-btn__label">6-50</span>
                </label>
                <label className="radio-btn">
                  <input className="radio-btn__radio" type="radio" value="51-200" name="company-size"/>
                  <span className="radio-btn__radio-custom"></span>
                  <span className="radio-btn__label">51-200</span>
                </label>
                <label className="radio-btn">
                  <input className="radio-btn__radio" type="radio" value="201-1000" name="company-size"/>
                  <span className="radio-btn__radio-custom"></span>
                  <span className="radio-btn__label">201-1000</span>
                </label>
                <label className="radio-btn">
                  <input className="radio-btn__radio" type="radio" value="1001+" name="company-size"/>
                  <span className="radio-btn__radio-custom"></span>
                  <span className="radio-btn__label">1001+</span>
                </label>
              </div>
              <div className="form__form-group form__form-group--read-and-agree">
                <label className="checkbox-btn">
                  <input className=" checkbox-btn__checkbox" type="checkbox" name="terms" required/>
                  <span className="checkbox-btn__checkbox-custom"><i className="mdi mdi-check"></i></span>
                  <span className="checkbox-btn__label">I read and agree to</span>
				  
                </label>
				
              </div>
			  
				  <div className="terms-conditions">
				 <button  target="_blank" className="btn link link--accent link--accent-bold"> Terms & Conditions</button>
				 </div>
				<div className="terms-cond"> 
              <button className="site-btn site-btn--accent form__submit disable" type="submit" value="Send" disabled>Get the
                Agency Portal
              </button>
			  </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <img className="section__img" src="../img/img_backgroud_footer.png" alt="profile"/>
</section>
{/* <!--Form--> */}
<Footer/>

      </Fragment>
    );
  }
}

export default contactus;
