import React, { Component, Fragment } from "react";
import request, { NodeURL } from "../../api/api";
import { toast } from "react-toastify";
import Gettoken from "../../api/gettoken";
import Context from "../../api/context";
import PropTypes from "prop-types";

// var proban = "../../img/user-profile.png";

class Sidebar extends Component {
  static contextTypes = {
    router: PropTypes.object
  };
  state = {
    token: "",
    itemscount: "",
    dropdownOpen: false,
    dropdownOpen1: false,
    dashboardData: [],
    notify_overall: "0",
    profileData: ""
  };
  componentDidMount() {
    document.addEventListener("scroll", () => {
      const isTop = window.scrollY < 150;
      if (isTop !== this.state.isTop) {
        this.setState({ isTop });
      }
    });

    const token = this.props.theme.Authtoken;
    if (token) {
      this.setState({
        token
      });
      request({
        url: "/employee/profile",
        method: "POST",
        data: token.username
      }).then(res => {
        if (res.status === 1) {
          this.setState({ profileData: res.response.result });
        } else if (res.status === 0) {
          let toastId = "profileResponse";
          if (!toast.isActive(toastId)) {
            toastId = toast.info(res.response);
          }
        }
      });
    } else {
      return this.context.router.history.push({ pathname: "/login" });
    }
  }
  Logout = e => {
    if (this.state.token) {
      request({
        url: "/employeelogout",
        method: "POST",
        data: { username: this.state.token.username }
      }).then(res => {
        if (res.status === 1) {
          localStorage.clear();
          this.context.router.history.push({
            pathname: "/login",
            state: "logout"
          });
          // window.location = "/login";
        } else if (res.status === 0) {
          toast.error("please try again");
        }
      });
    }
  };

  render() {
    return (
      <Fragment>
        {/* <ToastContainer position="top-right" autoClose={2500} /> */}
        <div className="profile-banners">
          <div className="profile-banners-steps">
            <div className="left-profile-banner">
              <div className="middle-pro">
                <div className="vre-pro">
                  {this.state.profileData ? (
                    <img className="prof-image img-fluid" width="180px" height="140px" src={NodeURL + "/" + this.state.profileData.avatar} alt="profile" />
                  ) : (
                    <img className="prof-image img-fluid" width="180px" height="140px" src={"../../img/avatars/9.jpg"} alt="profile" />
                  )}
                </div>
              </div>
              <div className="info-values">
                <h4>{this.state && this.state.profileData.name}</h4>
                <span>
                  {this.state.profileData && this.state.profileData.phone ? (
                    <Fragment>
                      {this.state.profileData.phone.code} - {this.state.profileData.phone.number}
                    </Fragment>
                  ) : null}
                </span>
              </div>
            </div>
            <div className="right-profile-banner">
              <ul>
                <li onClick={this.Logout.bind(this)}>Logout</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="left-edit-side">
          <div className="left-menus">
            <ul>
              <li
                className={this.context.router.history.location.pathname === "/user/dashboard" ? "active" : null}
                onClick={() => {
                  this.context.router.history.push("/user/dashboard");
                }}
              >
                {" "}
                <i className="fa fa-dashboard" /> Dashboard
              </li>
              <li
                className={this.context.router.history.location.pathname === "/user/showprofile" ? "active" : null}
                onClick={() => {
                  this.context.router.history.push("/user/showprofile");
                }}
              >
                {" "}
                <i className="fa fa-user" /> Profile
              </li>
              <li
                className={this.context.router.history.location.pathname === "/user/profile" ? "active" : null}
                onClick={() => {
                  this.context.router.history.push("/user/profile");
                }}
              >
                {" "}
                <i className="fa fa-edit" /> Edit Profile
              </li>
              <li
                className={
                  this.context.router.history.location.pathname === "/user/calenderview" || this.context.router.history.location.pathname === "/user/shiftslist" || this.context.router.history.location.pathname === "/user/shiftview"
                    ? "active"
                    : null
                }
                onClick={() => {
                  this.context.router.history.push("/user/calenderview");
                }}
              >
                <i className="fa fa-calendar" /> Shifts
              </li>
              <li
                className={this.context.router.history.location.pathname === "/user/timesheets" || this.context.router.history.location.pathname === "/user/timesheetdetails" ? "active" : null}
                onClick={() => {
                  this.context.router.history.push("/user/timesheets");
                }}
              >
                <i className="fa fa-clock-o" /> Timesheets
              </li>
              <li
                className={this.context.router.history.location.pathname === "/user/invoicelist" || this.context.router.history.location.pathname === "/user/invoicegroupview" ? "active" : null}
                onClick={() => {
                  this.context.router.history.push("/user/invoicelist");
                }}
              >
                <i className="fa fa-euro" /> Earnings
              </li>
              {this.state.token.recruitment_module === 1 ? (
                <li
                  className={this.context.router.history.location.pathname === "/user/onlineform" ? "active" : null}
                  onClick={() => {
                    this.context.router.history.push("/user/onlineform");
                  }}
                >
                  <i className="fa fa-check-square-o" /> Online Form
                </li>
              ) : null}
              {/* <li
                className={this.context.router.history.location.pathname === "/user/settings/notifications" ? "active" : null}
                onClick={() => {
                  this.context.router.history.push("/user/settings/notifications");
                }}
              >
                <i className="fa fa-cog" /> Settings
              </li> */}
            </ul>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Sidebar {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
