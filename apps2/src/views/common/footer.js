import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer-background">
        {/* <!--Footer menu--> */}
        <div className="container footer-menu">
          <div className="">
            <div className="col-12">
              <a href="index.html">
                <img src="../img/footerimg.png" className="img-responsive" alt="profile" />
              </a>
              <nav className="footer-menu__nav">
                <ul>
                  <li>
                    <a href="/aboutus" className="link link--gray">
                      About
                    </a>
                  </li>
                  <li>
                    <a href="/" className="link link--gray">FAQ</a>
                  </li>
                  <li>
                    <a href="/" className="link link--gray">Privacy</a>
                  </li>
                  <li>
                    <a className="link link--gray" href="/serviceexpertise">
                      Service & Expertise
                    </a>
                  </li>
                  <li>
                    <a className="link link--gray" href="/contactus">
                      Contact Us
                    </a>
                  </li>
                </ul>
              </nav>
              <p className="footer-menu__social">
                <a className="link link--gray" href="/">
                  <i className="mdi mdi-twitter" aria-hidden="true" />
                </a>
                <a className="link link--gray" href="/">
                  <i className="mdi mdi-facebook" aria-hidden="true" />
                </a>
                <a className="link link--gray" href="/">
                  <i className="mdi mdi-linkedin" aria-hidden="true" />
                </a>
              </p>
            </div>
          </div>
        </div>
        {/* <!--Footer menu--> */}

        <hr />

        {/* <!--Footer--> */}
        <div className="footer">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <p>
                  Copyright © 2019 Agency Portal Inc. All rights reserved.
                  {/* <!-- <a href="http://themes.aspirity.com" className="link link--gray">Aspirity</a> --> */}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
