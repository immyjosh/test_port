import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Gettoken from "../../api/gettoken";
import Context from "../../api/context";
import request, { NodeURL } from "../../api/api";
import PropTypes from "prop-types";
import { AvForm } from "availity-reactstrap-validation";
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Dropdown, Modal, ModalHeader, ModalBody, ModalFooter, TabContent, TabPane, Row, Col, Label, Button, Input } from "reactstrap";
// import classnames from "classnames";
import io from "socket.io-client";

class Header extends Component {
  static contextTypes = {
    router: PropTypes.object
  };
  state = {
    token: "",
    itemscount: "",
    dropdownOpen: false,
    dropdownOpen1: false,
    dropdownOpen2: false,
    dashboardData: [],
    notify_overall: "0",
    profileData: "",
    activeTab: "E1",
    settingmodal: false,
    mailstatus: 0,
    smsstatus: 0,
    form_recruitement_module: ""
  };

  componentDidMount() {
    const token = this.props.theme.Authtoken;
  /*   const socket = io(`${NodeURL}/notify`);
    socket.on("web_notification", data => {
      switch (data.type) {
        case "employee":
          if (token && token.username) {
            this.getEmployeenotify();
          }
          break;
        default:
          break;
      }
    });*/
    if (token && token.username) {
      this.setState({
        token
      });
      this.getEmployeenotify();
      request({
        url: "/employee/profile",
        method: "POST",
        data: token.username
      }).then(res => {
        if (res.status === 1) {
          this.setState({
            profileData: res.response.result.avatar,
            mailstatus: res.response.result && res.response.result.settings && res.response.result.settings.notifications.email,
            smsstatus: res.response.result && res.response.result.settings && res.response.result.settings.notifications.sms,
            form_recruitement_module: res.response.result && res.response.result.onlineform && res.response.result.onlineform.recruitment_module,
            form_status: res.response.result && res.response.result.onlineform && res.response.result.onlineform.form_status
          });
        } else if (res.status === 0) {
          let toastId = "profileResponse";
          if (!toast.isActive(toastId)) {
            toastId = toast.info(res.response);
          }
        }
      });
    } else {
      // return this.context.router.history.push({ pathname: "/login" });
    }
  }
  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  };
  toggleprofile = () => {
    this.setState({
      dropdownOpen1: !this.state.dropdownOpen1
    });
  };
  toggleprofile2 = () => {
    this.setState({
      dropdownOpen2: !this.state.dropdownOpen2
    });
  };
  Logout = e => {
    if (this.state.token) {
      request({
        url: "/employeelogout",
        method: "POST",
        data: { username: this.state.token.username }
      }).then(res => {
        if (res.status === 1) {
          // window.location = "/login";
        } else if (res.status === 0) {
          // toast.error("please try again");
        }
      });
    }
      localStorage.removeItem("APUSE");
      this.context.router.history.push({pathname: "/login", state: "logout"});
  };
  getEmployeenotify() {
    request({
      url: "/employee/notification",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ dashboardData: res.response.Notifications[0] || {} });
        this.setState({ notify_overall: res.response.overall });
      } else if (res.status === 0) {
        let toastId = "dashboardResponse";
        if (!toast.isActive(toastId)) {
          toastId = toast.error(res.response);
        }
      }
    });
  }
  SettingOpen = () => {
    this.setState({
      settingmodal: !this.state.settingmodal
    });
  };
  SettingClose = () => {
    this.setState({ settingmodal: false });
  };
  statusChangeemail = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      mailstatus: e.target.checked || value
    });
  };
  statusChangesms = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      smsstatus: e.target.checked || value
    });
  };
  SettingForm = (e, values) => {
    request({
      url: "/employee/settings/notifications/save",
      method: "POST",
      data: {
        mailstatus: this.state.mailstatus,
        smsstatus: this.state.smsstatus
      }
    })
      .then(response => {
        if (response.status === 1) {
          toast.success("Updated");
          this.componentDidMount();
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  };
  EmployeeClearNotification = () => {
    request({
      url: "/employee/clearnotification",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
      }
    });
  };
  render() {
    const { form_status } = this.state;
    return (
      <Fragment>
        <ToastContainer position="top-right" autoClose={2500} />
        {/* Main menu */}
        <div className={"menu " + (window.location.pathname.length < 2 ? "color" : "") + (this.state.isTop ? "" : "menu--active")}>
          <div className="container menu__wrapper">
            <div className="row">
              <div className="menu__logo menu__item">
                <Link to="/">
                  <img src={"../img/logo.png"} alt="logo" />
                </Link>
              </div>

              <div className="menu__item">
                {this.state.token.role === "employee" ? (
                  <nav className="menu__right-nav after-logins">
                    <ul>
                      <li
                        className={this.context.router.history.location.pathname === "/user/dashboard" ? "active" : null}
                        onClick={() => {
                          this.context.router.history.push("/user/dashboard");
                        }}
                      >
                        {" "}
                        <i className="fa fa-dashboard" /> Dashboard
                      </li>
                      {form_status >= 5 ? (
                        <>
                          <li
                            className={
                              this.context.router.history.location.pathname === "/user/calenderview" ||
                              this.context.router.history.location.pathname === "/user/shiftslist" ||
                              this.context.router.history.location.pathname === "/user/shiftview"
                                ? "active"
                                : null
                            }
                            onClick={() => {
                              this.context.router.history.push("/user/calenderview");
                            }}
                          >
                            <i className="fa fa-calendar" /> Shifts
                          </li>
                          <li
                            className={this.context.router.history.location.pathname === "/user/timesheets" || this.context.router.history.location.pathname === "/user/timesheetdetails" ? "active" : null}
                            onClick={() => {
                              this.context.router.history.push("/user/timesheets");
                            }}
                          >
                            <i className="fa fa-clock-o" /> Timesheets
                          </li>
                          <li
                            className={this.context.router.history.location.pathname === "/user/invoicelist" || this.context.router.history.location.pathname === "/user/invoicegroupview" ? "active" : null}
                            onClick={() => {
                              this.context.router.history.push("/user/invoicelist");
                            }}
                          >
                            <i className="fa fa-euro" /> Earnings
                          </li>
                        </>
                      ) : null}
                      {this.state && this.state.token.recruitment_module === 1 ? (
                        <>
                          {this.state && this.state.form_recruitement_module === 1 ? (
                            <>
                              {form_status >= 5 ? (
                                <li
                                  className={this.context.router.history.location.pathname === "/user/yourdetails" ? "active" : null}
                                  onClick={() => {
                                    this.context.router.history.push("/user/yourdetails");
                                  }}
                                >
                                  <i className="fa fa-check-square-o" /> Your Details
                                </li>
                              ) : (
                                <li
                                  className={this.context.router.history.location.pathname === "/user/onlineform" ? "active" : null}
                                  onClick={() => {
                                    this.context.router.history.push("/user/onlineform");
                                  }}
                                >
                                  <i className="fa fa-check-square-o" /> Online Form
                                </li>
                              )}
                            </>
                          ) : null}
                        </>
                      ) : null}
                      {form_status >= 5 ? (
                        <li className="link link--gray" onClick={this.SettingOpen}>
                          <i className="fa fa-cog" />{" "}
                        </li>
                      ) : null}
                      <Dropdown className="fordropdown" nav isOpen={this.state.dropdownOpen1} toggle={this.toggleprofile}>
                        <DropdownToggle nav>
                          <i className="icon-bell" />
                          <Badge pill color="danger">
                            {this.state.notify_overall ? <Fragment>{this.state.notify_overall}</Fragment> : 0}
                          </Badge>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem header tag="div" className="text-center">
                            <strong>You have {this.state.notify_overall ? <Fragment>{this.state.notify_overall}</Fragment> : 0} notifications</strong>
                          </DropdownItem>
                          <Fragment>
                            {this.state.dashboardData && this.state.dashboardData.job_request ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/shiftslist",
                                    state: { notificationName: "job_request" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-primary" />
                                New Shifts
                                <Badge pill color="primary">
                                  {this.state.dashboardData.job_request}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.job_assigned ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/shiftslist",
                                    state: { notificationName: "job_assigned" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Shift Assigned
                                <Badge pill color="success">
                                  {this.state.dashboardData.job_assigned}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.timesheet_approved ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/timesheets",
                                    state: { notificationName: "timesheet_approved" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Time Sheet Approved
                                <Badge pill color="success">
                                  {this.state.dashboardData.timesheet_approved}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.invoice_approved ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/invoicelist",
                                    state: { notificationName: "invoice_approved" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Time Sheet Approved
                                <Badge pill color="success">
                                  {this.state.dashboardData.invoice_approved}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.interview_call ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/dashboard",
                                    state: { notificationName: "interview_call" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                New Interview Call
                                <Badge pill color="success">
                                  {this.state.dashboardData.interview_call}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.interview_pass ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/dashboard",
                                    state: { notificationName: "interview_pass" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Qualified In Interview
                                <Badge pill color="success">
                                  {this.state.dashboardData.interview_pass}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.interview_fail ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/dashboard",
                                    state: { notificationName: "interview_fail" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Disqualified In Interview
                                <Badge pill color="success">
                                  {this.state.dashboardData.interview_fail}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.qualified_employee ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/dashboard",
                                    state: { notificationName: "qualified_employee" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Qualified As Employee
                                <Badge pill color="success">
                                  {this.state.dashboardData.qualified_employee}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            {this.state.dashboardData && this.state.dashboardData.disqualified_employee ? (
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push({
                                    pathname: "/user/dashboard",
                                    state: { notificationName: "disqualified_employee" }
                                  });
                                  setTimeout(() => {
                                    this.componentDidMount();
                                  }, 3000);
                                }}
                              >
                                <i className="icon-clock text-success" />
                                Disqualified As Employee
                                <Badge pill color="success">
                                  {this.state.dashboardData.disqualified_employee}
                                </Badge>
                              </DropdownItem>
                            ) : null}
                            <DropdownItem onClick={this.EmployeeClearNotification} className="text-center">
                              <i className="fa fa-remove text-success" /> Clear All Notification
                            </DropdownItem>
                          </Fragment>
                        </DropdownMenu>
                      </Dropdown>
                      <Dropdown className="fordropdown" nav isOpen={this.state.dropdownOpen2} toggle={this.toggleprofile2}>
                        <DropdownToggle nav>
                          <div>
                            {this.state.profileData ? (
                              <img
                                width="40px"
                                height="40px"
                                src={NodeURL + "/" + this.state.profileData}
                                className="img-avatar"
                                onError={() => {
                                  this.setState({ profileData: "../../img/avatars/9.jpg" });
                                }}
                                alt="profile"
                              />
                            ) : (
                              <img width="40px" height="40px" src={"../../img/avatars/9.jpg"} className="img-avatar" alt="profile" />
                            )}
                          </div>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem header tag="div" className="text-center">
                            <strong> {this.state.token ? <Fragment>{this.state.token.username}</Fragment> : null}</strong>
                          </DropdownItem>

                          <Fragment>
                            <DropdownItem
                              onClick={() => {
                                this.context.router.history.push("/user/showprofile");
                              }}
                            >
                              <i className="fa fa-edit" /> View Profile
                            </DropdownItem>{" "}
                            <DropdownItem
                              onClick={() => {
                                this.context.router.history.push("/user/profile");
                              }}
                            >
                              <i className="fa fa-user" /> Edit Profile
                            </DropdownItem>
                            {/* {this.state.token.username ? (
                                <DropdownItem className="text-center" active>
                                  <strong>{this.state.token.username}</strong>
                                </DropdownItem>
                              ) : null}

                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push("/user/calenderview");
                                }}
                              >
                                <i className="fa fa-calendar" />Shifts
                              </DropdownItem>
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push("/user/timesheets");
                                }}
                              >
                                <i className="fa fa-clock-o" />Timesheets
                              </DropdownItem>
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push("/user/invoicelist");
                                }}
                              >
                                <i className="fa fa-euro" />Earnings
                              </DropdownItem>
                              <DropdownItem
                                onClick={() => {
                                  this.context.router.history.push("/user/settings/notifications");
                                }}
                              >
                                <i className="fa fa-cog" />Settings
                              </DropdownItem> */}
                            <DropdownItem onClick={this.Logout.bind(this)}>
                              <i className="fa fa-lock" /> Logout
                            </DropdownItem>
                          </Fragment>
                        </DropdownMenu>
                      </Dropdown>
                    </ul>
                  </nav>
                ) : (
                  <nav className="menu__right-nav mt-2">
                    <ul>
                      {/* <li
                      className="link link--gray"
                      onClick={() => {
                        this.context.router.history.push("/");
                      }}
                    >
                      Home
                    </li> */}
                      <li>
                        <a className="link link--gray" href="/">
                          Home
                        </a>
                      </li>
                      <li>
                        <a className="link link--gray" href="/aboutus">
                          About Us
                        </a>
                      </li>
                      <li>
                        <a className="link link--gray" href="/serviceexpertise">
                          Service & Expertise
                        </a>
                      </li>

                      <li>
                        <a className="link link--gray" href="/contactus">
                          Contact Us
                        </a>
                      </li>

                      <li
                        className="site-btn site-btn--accent user-entry-side"
                        onClick={() => {
                          this.context.router.history.push("/login");
                        }}
                      >
                        Login
                      </li>
                      <li
                        className="site-btn site-btn--accent user-entry-side"
                        onClick={() => {
                          this.context.router.history.push("/register");
                        }}
                      >
                        Sign Up
                      </li>
                    </ul>
                  </nav>
                )}

                <div className="d-none d-t-block">
                  <button type="button" className="menu__mobile-button">
                    <span>
                      <i className="mdi mdi-menu" aria-hidden="true" />
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>

          {/* Main menu */}

          {/* <!--Mobile menu--> */}
          <div className="mobile-menu d-none d-t-block">
            <div className="container">
              <div className="mobile-menu__logo">
                <svg className="menu__logo-img" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                  <path
                    data-name="Sigma symbol"
                    className="svg-element"
                    d="M237.418,8583.56a12.688,12.688,0,0,0,.419-3.37c-0.036-5.24-2.691-9.68-7.024-13.2h-3.878a20.819,20.819,0,0,1,4.478,13.01c0,4.56-2.456,10.2-6.413,11.4a16.779,16.779,0,0,1-2.236.51c-10.005,1.55-14.109-17.54-9.489-23.31,2.569-3.21,6.206-4.08,11.525-4.08h17.935A24.22,24.22,0,0,1,237.418,8583.56Zm-12.145-24.45c-8.571.02-12.338,0.98-16.061,4.84-6.267,6.49-6.462,20.69,4.754,27.72a24.092,24.092,0,1,1,27.3-32.57h-16v0.01Z"
                    transform="translate(-195 -8544)"
                  />
                </svg>
              </div>
              <button type="button" className="mobile-menu__close">
                <span>
                  <i className="mdi mdi-close" aria-hidden="true" />
                </span>
              </button>
              <nav className="mobile-menu__wrapper">
                <ul className="mobile-menu__ul">
                  <li className="mobile-menu__li">
                    <a href="05_features.html" className="link link--dark-gray">
                      Home
                    </a>
                  </li>
                  <li className="mobile-menu__li">
                    <a href="05_features.html" className="link link--dark-gray">
                      About Us
                    </a>
                  </li>
                  <li className="mobile-menu__li">
                    <a href="06_pricing.html" className="link link--dark-gray">
                      Service & Expertise
                    </a>
                  </li>
                  <li className="mobile-menu__li">
                    <a href="06_pricing.html" className="link link--dark-gray">
                      Staffing
                    </a>
                  </li>
                  <li className="mobile-menu__li">
                    <a href="06_pricing.html" className="link link--dark-gray">
                      Contact Us
                    </a>
                  </li>
                  <li className="mobile-menu__li">
                    <a href="10_get-app.html" className="site-btn site-btn--accent">
                      Login
                    </a>
                  </li>
                  <li className="mobile-menu__li">
                    <a href="10_get-app.html" className="site-btn site-btn--accent">
                      Sign Up
                    </a>
                  </li>
                  <li className="mobile-menu__li mobile-menu__li-collapse">
                    <button className="link link--dark-gray">
                      En
                      <span>
                        <i className="mdi mdi-chevron-down" />
                      </span>
                    </button>
                  </li>
                  <li className="mobile-menu__ul--collapsed">
                    <ul className="mobile-menu__ul">
                      <li className="mobile-menu__li">
                        <a href="/" className="link link--gray link--gray-active">
                          En
                        </a>
                      </li>
                      <li className="mobile-menu__li">
                        <a href="/" className="link link--gray">
                          Fr
                        </a>
                      </li>
                      <li className="mobile-menu__li">
                        <a href="/" className="link link--gray">
                          Ch
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        {/* <!--Mobile menu--> */}
        <Modal isOpen={this.state.settingmodal} className={"modal-lg " + this.props.className}>
          <AvForm onValidSubmit={this.SettingForm}>
            <ModalHeader toggle={this.SettingClose}>
              {" "}
              <i className="fa fa-cogs" /> Settings
            </ModalHeader>
            <ModalBody className="p-0 setting-alert">
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="E1">
                  <Row>
                    <h5> Notifications</h5>
                    <Col xs="12" md="4">
                      <div>
                        <label>Mail</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.mailstatus} onChange={this.statusChangeemail} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                    <Col xs="12" md="4">
                      <div>
                        <label>SMS</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.smsstatus} onChange={this.statusChangesms} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </ModalBody>
            <ModalFooter>
              <Button color="success" className="pull-tight" type="submit">
                Save
              </Button>{" "}
              <Button color="secondary" className="pull-left" type="button" onClick={this.SettingClose}>
                Close
              </Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </Fragment>
    );
  }
}

// export default withRouter(Header);s
export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Header {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
