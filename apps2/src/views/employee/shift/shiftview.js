/* global google*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, Badge, CardBody, CardHeader, Row, Col, UncontrolledTooltip } from "reactstrap";
import request, { NodeURL } from "../../../api/api";
// import states from "../../Template/Forms/AdvancedForms/data/states";
// import Select from "react-select";
import "react-select/dist/react-select.css";
import { Link } from "react-router-dom";
import moment from "moment";
import { compose, withProps, lifecycle } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer, Marker } from "react-google-maps";

class shiftview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      job_type: "",
      value: [],
      worklist: [],
      employee_requested: [],
      shift_id: "",
      employee_id: "",
      status: "",
      hold: "",
      shiftviewdetails: [],
      timesheet_status: "",
      starttime_cond: "",
      branchaddress: "",
      start_date_iso: "",
      notes: "",
      timesheet: [],
      branchphone: {},
      branch_location: {},
      employee_location: {},
      maplocation: [],
      distance: ""
    };
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token) {
      return this.props.history.replace("/login");
    }
    request({
      url: "/employee/shiftlist",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        const getshiftdata = [];
        res.response.result.map((item, i) => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = item.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startsec = splithr[1];
          const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
          const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          const eveendhr = item.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          getshiftdata.push({
            _id: item._id,
            starttime: starttimehr,
            endtime: endtimehr,
            location: item.locations,
            client: item.client,
            breaktime: item.breaktime / 60 + " Minutes",
            start_date: item.start_date,
            end_date: item.end_date,
            job_type: item.job_type,
            branch: item.branch_data && item.branch_data.length > 0 ? item.branch_data[0].branches.branchname : "",
            title: item.title,
            shiftId: item.shiftId,
            notes: item.notes,
            status: item.status
          });
          this.setState({
            shiftlist: getshiftdata
          });
          return true;
        });
      }
    });
    request({
      url: "/employee/view_shift",
      method: "POST",
      data: {
        shiftId: this.props.location.state._id
      }
    })
      .then(res => {
        if (res.status === 1) {
          let holddata = "";
          if (res.response.timesheet.length > 0) {
            if (res.response.timesheet[0].hold) {
              holddata = res.response.timesheet[0].hold;
            }
          }
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = res.response.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startsec = splithr[1];
          const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
          const starttimehr = splithr[0] + ":" + startmins ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          const eveendhr = res.response.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          this.setState({
            shift_id: res.response.shiftId,
            title: res.response.title,
            client: res.response.client,
            branch: res.response.branch.length ? res.response.branch[0].branches.branchname : "",
            branch_location: res.response.branch.length ? res.response.branch[0].branches : "",
            employee_location: res.response.employeeaddress.length ? res.response.employeeaddress[0] : "",
            branchphone: res.response.branch.length ? res.response.branch[0].branches.phone : "",
            maplocation: [
              {
                lat: res.response.employeeaddress.length ? res.response.employeeaddress[0].lat : 51.50853,
                lng: res.response.employeeaddress.length ? res.response.employeeaddress[0].lon : -0.12574,
                label: "Y",
                draggable: false
              },
              {
                lat: res.response.branch.length ? res.response.branch[0].branches.branchlat : 51.50853,
                lng: res.response.branch.length ? res.response.branch[0].branches.branchlng : -0.12574,
                label: "S",
                draggable: false
              }
            ],
            branchaddress: `${res.response.branch.length ? res.response.branch[0].branches.branchformatted_address : ""} , ${res.response.branch.length ? res.response.branch[0].branches.branchzipcode : ""}`,
            timesheet: res.response.timesheet,
            timesheet_status: res.response.timesheet_status,
            breaktime: res.response.breaktime / 60 + " Minutes",
            end_date: moment(res.response.end_date).format("DD-MM-YYYY"),
            endtime: endtimehr,
            start_date: moment(res.response.start_date).format("DD-MM-YYYY"),
            start_date_iso: res.response.start_date,
            starttime: starttimehr,
            starttime_cond: res.response.starttime,
            location: res.response.locations,
            job_type: res.response.job_type ? res.response.job_type[0] : "",
            shift_type: res.response.shift_type,
            id: res.response._id,
            status: res.response.status,
            notes: res.response.notes,
            avatar: res.response.employee_avatar && res.response.employee_avatar.length > 0 ? res.response.employee_avatar[0] : "",
            distance: res.response.distance,
            hold: holddata
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  }
  accept(item) {
    let allowJob;
    this.state.shiftlist.map(list => {
      if (moment(list.start_date).format("DD-MM-YYYY") === item.start_date && list.starttime === item.starttime && list.endtime === item.endtime && (list.status === 3 || list.status === 4 || list.status === 5)) {
        return (allowJob = true);
      }
      return (allowJob = false);
    });
    if (allowJob) {
      toast.error("Shift on Same Date & Time already Accepted");
    } else {
      request({
        url: "/employee/accept_job",
        method: "POST",
        data: {
          shiftId: this.state.id
        }
      })
        .then(res => {
          if (res.status === 1) {
            let toastId = "acceptjobRequest";
            if (!toast.isActive(toastId)) {
              toastId = toast.success("Accepted");
            }
            this.componentDidMount();
          } else if (res.status === 0) {
            toast.error(res.response);
            this.componentDidMount();
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  }
  unAssign(item) {
    request({
      url: "/agency/unassign_shift",
      method: "POST",
      data: {
        id: item.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          let toastId = "Un_assigned_01";
          if (!toast.isActive(toastId)) {
            toastId = toast.success("Shift Cancelled");
          }
          this.props.history.push("/user/shiftslist");
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
          this.componentDidMount();
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  startshift(item) {
    let allowJob = false;
    this.state.shiftlist &&
      this.state.shiftlist.map(list => {
        // console.log(
        //   list.shiftId,
        //   this.state.shift_id,
        //   moment(list.start_date).format("DD-MM-YYYY"),
        //   item.start_date,
        //   list.starttime,
        //   item.starttime,
        //   list.endtime,
        //   item.endtime,
        //   list.status
        // );
        // console.log(list.status);
        if (list.shiftId !== this.state.shift_id && moment(list.start_date).format("DD-MM-YYYY") === item.start_date && list.starttime === item.starttime && list.endtime === item.endtime && list.status === 5) {
          return (allowJob = true);
        } else if (list.status === 5) {
          return (allowJob = true);
        }
        return true;
      });
    // console.log("allowJob", allowJob);
    if (allowJob) {
      toast.error("Shift on Same Date & Time already Accepted");
    } else {
      request({
        url: "/shift/update/start",
        method: "POST",
        data: {
          shiftId: this.state.id
        }
      })
        .then(res => {
          if (res.status === 1) {
            let toastId = "Shift_Started";
            if (!toast.isActive(toastId)) {
              toastId = toast.success("Shift Started");
            }
            // toast.success(res.response);
            this.componentDidMount();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  }
  endshift(item) {
    request({
      url: "/shift/update/end",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          let toastId = "Shift_Ended";
          if (!toast.isActive(toastId)) {
            toastId = toast.success("Shift Completed");
          }
          // toast.success(res.response);
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  holdshift(item) {
    request({
      url: "/shift/update/hold",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          // toast.success(res.response);
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  restartshift(item) {
    request({
      url: "/shift/update/restart",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          // toast.success(res.response);
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  sendTimeSheet(item) {
    request({
      url: "/employee/timesheet_request",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          let toastId = "Timesheet_Sent";
          if (!toast.isActive(toastId)) {
            toastId = toast.success("Timesheet Sent");
          }
          // toast.success(res.response);
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const date_cond_previous = this.state.starttime_cond - 3600;
    const date_cond_current = moment(new Date()).format("HH:mm");
    const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    const evestarthr = date_cond_previous / 3600;
    const splithr = evestarthr.toString().split(".");
    const startingsecs = splithr[1];
    const startingminutes = startingsecs === undefined ? "00" : (+"0" + "." + startingsecs) * 60;
    const Time_Cond = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startingminutes : +"0" + "" + splithr[0] + ":" + startingminutes;
    const MapWithADirectionsRenderer = compose(
      withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBWZKvXdWQy5J80V5HQabwV_lcL70CGHQg&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />
      }),
      withScriptjs,
      withGoogleMap,
      lifecycle({
        componentDidMount() {
          const DirectionsService = new google.maps.DirectionsService();
          DirectionsService.route(
            {
              origin: new google.maps.LatLng(
                this.props && this.props.locationdetails.employee && this.props.locationdetails.employee.lat ? this.props.locationdetails.employee.lat : 41.85073,
                this.props && this.props.locationdetails.employee && this.props.locationdetails.employee.lon ? this.props.locationdetails.employee.lon : -87.65126
              ),
              destination: new google.maps.LatLng(
                this.props && this.props.locationdetails.branch && this.props.locationdetails.branch.branchlat ? this.props.locationdetails.branch.branchlat : 41.85258,
                this.props && this.props.locationdetails.branch && this.props.locationdetails.branch.branchlng ? this.props.locationdetails.branch.branchlng : -87.65141
              ),
              travelMode: google.maps.TravelMode.DRIVING
            },
            (result, status) => {
              if (status === google.maps.DirectionsStatus.OK) {
                this.setState({
                  directions: result,
                  maplocation: this.props && this.props.locationdetails ? this.props.locationdetails.maplocation : [],
                  employee_image: this.props && this.props.locationdetails && this.props.locationdetails.avatar
                });
              } else {
                console.error(`error fetching directions ${result}`);
              }
            }
          );
        }
      })
    )(props => (
      <GoogleMap defaultZoom={4} defaultCenter={new google.maps.LatLng(41.85073, -87.65126)} defaultOptions={{ gestureHandling: "greedy" }}>
        {props.directions && <DirectionsRenderer directions={props.directions} options={{ suppressMarkers: true }} />}
        {props.maplocation && (
          <Marker
            position={{
              lat: props && props.maplocation && props.maplocation[0] ? props.maplocation[0].lat : "",
              lng: props && props.maplocation && props.maplocation[0] ? props.maplocation[0].lng : "",
              label: "S",
              draggable: false
            }}
            title={"Your Location"}
            // label={"Y"}
            icon={{ url: NodeURL + "/uploads/map/icons8-building-48.png", size: { width: 60, height: 100 }, anchor: { x: 15, y: 50 }, scaledSize: { width: 40, height: 40 } }}
          />
        )}
        {props.maplocation && (
          <Marker
            position={{
              lat: props && props.maplocation && props.maplocation[1] ? props.maplocation[1].lat : "",
              lng: props && props.maplocation && props.maplocation[1] ? props.maplocation[1].lng : "",
              label: "S",
              draggable: false
            }}
            title={"Shift Location"}
            // label={"S"}
            icon={{ url: props && props.employee_image ? NodeURL + "/" + props.employee_image : "", size: { width: 60, height: 100 }, anchor: { x: 15, y: 50 }, scaledSize: { width: 40, height: 40 } }}
          />
        )}
      </GoogleMap>
    ));
    return (
      <div className="right-edit-side">
        <div className="form__card card form__card--background float shift-views-larges">
          <div className="form__wrap extra">
            {this.state.status === 6 ? (
              <Fragment>
                <p className="task-compleed mr-3">Shift Completed</p>
              </Fragment>
            ) : null}
            {/* <ToastContainer position="top-right" autoClose={2500} />*/}
            <div className="wholetask">
              <CardHeader className="d-flex justify-content-between new-overall">
                {/* <span>
              <i className="icon-list" />Shift Details
            </span> */}
                <h3 className="shift-views">Shift Details</h3>
                <div className="pull-right back-prev">
                  <Link to="/user/shiftslist">
                    {" "}
                    <p className="left-right" />
                    <Button className="new-BtnNew mr-1" color="" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </div>
              </CardHeader>
              {/* <CardBody className="pay-comp p-0"> */}
              <CardBody className="p-0">
                <div className="shift-views">
                  <Row>
                    <Col xs="12" md="6" className="shift-models">
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Shift</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.title}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Client</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.client}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Area</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.location}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>
                              Work Location
                              {/* Branch*/}
                            </b>
                          </span>{" "}
                          <span className="name-lct">{this.state.branch}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Address</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.branchaddress}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Distance</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.distance}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Phone</b>
                          </span>{" "}
                          <span className="name-lct">
                            {this.state.branchphone.code} - {this.state.branchphone.number}
                          </span>
                        </div>
                      </Col>
                      <Col xs="12" md="12 value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Role</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.job_type}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12" className="value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Date</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.start_date}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12" className="value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Time</b>
                          </span>{" "}
                          <span className="name-lct">
                            {this.state.starttime} - {this.state.endtime}
                          </span>
                        </div>
                      </Col>
                      <Col xs="12" md="12" className="value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Break</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.breaktime}</span>
                        </div>
                      </Col>
                      <Col xs="12" md="12" className="value-adding">
                        <div className="view-reader">
                          <span>
                            <b>Notes</b>
                          </span>{" "}
                          <span className="name-lct">{this.state.notes}</span>
                        </div>
                      </Col>
                    </Col>
                    <Col xs="12" md="6" className={"pl-0"}>
                      {this.state &&
                        this.state.employee_location &&
                        this.state.branch_location && (
                          <MapWithADirectionsRenderer
                            locationdetails={{
                              branch: this.state.branch_location,
                              employee: this.state.employee_location,
                              maplocation: this.state.maplocation,
                              avatar: this.state.avatar
                            }}
                          />
                        )}
                    </Col>
                  </Row>{" "}
                  {this.state.timesheet ? (
                    <Row className="mt-3 mb-3 shf-bg-colors">
                      <Col xs="12" md="4">
                        {this.state.timesheet.length > 0 && this.state.timesheet[0].start ? (
                          <Fragment>
                            <b>Started Time : </b> {moment(this.state.timesheet[0].start).format("HH:mm")}
                          </Fragment>
                        ) : null}
                      </Col>
                      <Col xs="12" md="4">
                        {this.state.timesheet.length > 0 && this.state.timesheet[0].holdedmins ? (
                          <Fragment>
                            <b>Holded : </b> {Math.round(this.state.timesheet[0].holdedmins / 60)} Minutes
                          </Fragment>
                        ) : null}
                      </Col>
                      <Col xs="12" md="4">
                        {this.state.timesheet.length > 0 && this.state.timesheet[0].end ? (
                          <Fragment>
                            <b>Ended Time : </b> {moment(this.state.timesheet[0].end).format("HH:mm")}
                          </Fragment>
                        ) : null}
                      </Col>
                    </Row>
                  ) : null}
                </div>
              </CardBody>
            </div>
            <span className="changeShift mb-3">
              {this.state.status === 2 ? (
                <Fragment>
                  <Button color="primary" onClick={id => this.accept(this.state)} id={`edit2`}>
                    <i className="fa fa-paper-plane" />
                  </Button>
                  <UncontrolledTooltip placement="top" target={`edit2`}>
                    Accept Shift
                  </UncontrolledTooltip>
                </Fragment>
              ) : null}
              {this.state.status === 3 ? <Badge color="warning">Shift Accepted</Badge> : null}
              {this.state.status === 4 ? (
                <Fragment>
                  {moment(new Date()).format(`YYYY-MM-DD HH:mm:ss`) <= moment(this.state.start_date_iso).format(`YYYY-MM-DD ${this.state.starttime}:00`) ? (
                    <Button className=" mr-1 text-center" color="danger" onClick={id => this.unAssign(this.state)}>
                      Cancel Shift
                    </Button>
                  ) : null}
                </Fragment>
              ) : null}
              {this.state.status === 4 ? (
                <Fragment>
                  {this.state.start_date === moment(new Date()).format("DD-MM-YYYY") && date_cond_current >= Time_Cond ? (
                    <Button className=" mr-1" color="primary" onClick={id => this.startshift(this.state)}>
                      Start
                    </Button>
                  ) : (
                    <span> You Can Able to Start Shift On Assigned Date & Before Shift Assigned Time Only</span>
                  )}
                </Fragment>
              ) : null}
              {this.state.status === 5 && this.state.hold === "" ? (
                <Button className=" mr-1" color="warning" onClick={id => this.holdshift()}>
                  Hold
                </Button>
              ) : null}

              {this.state.status === 5 && this.state.hold !== "" ? (
                <Button className=" mr-1" color="warning" onClick={id => this.restartshift()}>
                  Restart
                </Button>
              ) : null}
              {this.state.status === 5 && this.state.hold === "" ? (
                <Button className=" mr-1" color="danger" onClick={id => this.endshift()}>
                  End
                </Button>
              ) : null}
              {this.state.status === 6 ? (
                <Fragment>
                  {/* <Badge color="danger" className="shiftComp">
                    Shift Completed
                  </Badge> */}
                  {this.state.timesheet_status === 0 ? (
                    <Button size="sm" className=" mr-1 ml-5" color="success" onClick={id => this.sendTimeSheet()}>
                      Send Time Sheet <i className="fa fa-envelope-open ml-2" />
                    </Button>
                  ) : null}
                </Fragment>
              ) : null}
              {this.state.status === 6 && this.state.timesheet_status === 1 ? (
                <Fragment>
                  <Badge color="warning" className=" timesheet-Send">
                    Time Sheet Sent
                  </Badge>
                </Fragment>
              ) : null}
              {this.state.status === 7 ? (
                <Badge color="danger" className="processCompleted">
                  Timesheet Approved
                </Badge>
              ) : null}
              {this.state.status === 8 ? (
                <Badge color="danger" className="processCompleted">
                  Invoice Approved
                </Badge>
              ) : null}
              {this.state.status === 9 ? (
                <Badge color="danger" className="processCompleted">
                  Payment Completed
                </Badge>
              ) : null}
            </span>
            <br />
          </div>
        </div>
      </div>
    );
  }
}

export default shiftview;
