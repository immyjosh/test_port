/* global google*/
import React, { Component, Fragment } from "react";
import { Button, Badge, UncontrolledTooltip, Row, Col, Card, CardHeader, CardBody, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import request, { NodeURL } from "../../../api/api";
import { toast } from "react-toastify";
import { compose, withProps, lifecycle } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer, Marker } from "react-google-maps";

const tStyle = {
  cursor: "pointer"
};
// const tStyles = {
//   width: "180px"
// };
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));

const currDate = new Date();
const currYear = currDate.getFullYear();
const currMonth = currDate.getMonth();

class Shiftcalender extends Component {
  state = {
    id: "",
    location: "",
    starttime: "",
    daycount: "",
    endtime: "",
    breaktime: "",
    slotdata: [],
    start_date: "",
    end_date: "",
    shift_type: "",
    title: "",
    job_type: "",
    eventlarge: false,
    eventsdata: [],
    date_list: [],
    Weekly_select_date_list: [],
    num: "",
    montofri: 0,
    Weekly_select_date_count: 0,
    monthly_date_count: 0,
    count: 0,
    start_date_time: "",
    end_date_time: "",
    option_data: [],
    status: "",
    hold: "",
    timesheet_status: "",
    timesheet: [],
    shiftlist: [],
    starttime_cond: "",
    branchaddress: "",
    shifttitle: "",
    maplocation: [],
    employee_location: "",
    branchphone: "",
    branch_location: "",
    notes: "",
    HolidayModal: false,
    HolidayName: "",
    HolidayDate: "",
    HolidayDesc: "",
    next: (
      <span>
        Next
        <i className="fa fa fa-angle-double-right ml-2" />
      </span>
    ),
    previous: (
      <span>
        <i className="fa fa fa-angle-double-left mr-2" />
        Previous
      </span>
    ),
    totalhours: "0:00",
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all"
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/login");
    }
    request({
      url: "/employee/shiftlist",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        const evelist = res.response.result || {};
        const eventsdata = evelist.map((eve, key) => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const start_split = (eve.starttime / 3600).toString().split(".");
          const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
          const starttimehr = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
          const start_times = starttimehr.split(":");
          const end_split = (eve.endtime / 3600).toString().split(".");
          const endmins = end_split[1] === undefined ? "00" : (+"0" + "." + end_split[1]) * 60;
          const endtimehr = num.indexOf(end_split[0]) === -1 ? end_split[0] + ":" + endmins : +"0" + "" + end_split[0] + ":" + endmins;
          const end_times = endtimehr.split(":");
          const start_d = new Date(eve.start_date);
          start_d.setHours(Number(start_times && start_times[0]));
          start_d.setMinutes(Number(start_times && start_times[1]));
          const end_d = new Date(eve.start_date);
          end_d.setHours(Number(end_times && end_times[0]));
          end_d.setMinutes(Number(end_times && end_times[1]));
          let title = eve.jobtype_data && eve.jobtype_data.length > 0 ? eve.jobtype_data[0].name : "";
          if (eve.status > 3) {
            title = `${eve.employee_data && eve.employee_data.length > 0 ? eve.employee_data[0].name : ""} - ${eve.branch_data && eve.branch_data.length > 0 && eve.branch_data[0].branches.branchname}`;
          } else {
            title = `${eve.jobtype_data && eve.jobtype_data.length > 0 ? eve.jobtype_data[0].name : ""} - ${eve.branch_data && eve.branch_data.length > 0 && eve.branch_data[0].branches.branchname}`;
          }
          return {
            title: title,
            shifttitle: eve.title,
            start: start_d ? start_d : eve.start_date,
            end: end_d ? end_d : eve.start_date,
            status: eve.status,
            // location: eve.location,
            // starttime: eve.starttime,
            // endtime: eve.endtime,
            // breaktime: eve.breaktime,
            // shift_type: eve.shift_type,
            // job_type: eve.job_type,
            id: eve._id
            // start_date: eve.start_date,
            // end_date: eve.end_date,
            // shift_option: eve.shift_option,
            // option_data: eve.option_data
          };
        });

        const getshiftdata = [];
        res.response.result.map((item, i) => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = item.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startmins = splithr[1] === undefined ? "00" : (+"0" + "." + splithr[1]) * 60;
          const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          const eveendhr = item.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          getshiftdata.push({
            _id: item._id,
            starttime: starttimehr,
            endtime: endtimehr,
            location: item.location,
            client: item.client,
            breaktime: item.breaktime / 60 + " mins",
            start_date: item.start_date,
            end_date: item.end_date,
            job_type: item.job_type,
            branch: item.branch_data[0].branches.branchname,
            shifttitle: item.shifttitle,
            shiftId: item.shiftId,
            notes: item.notes,
            status: item.status
          });
          request({
            url: "/employee/daysoff/forshiftslist",
            method: "GET"
          }).then(res => {
            if (res.status === 1) {
              const daysofflist =
                res.response &&
                res.response.result.length > 0 &&
                res.response.result.map(list => ({
                  allDay: true,
                  title: list.name,
                  start: list.dates,
                  end: list.dates,
                  desc: list.reason,
                  status: 12
                }));
              this.setState({
                shiftlist: getshiftdata.concat(daysofflist),
                eventsdata: eventsdata.concat(daysofflist)
              });
            }
          });
          return true;
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/employee/view_shift",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    }).then(res => {
      if (res.status === 1) {
        let holddata = "";
        if (res.response.timesheet.length > 0) {
          if (res.response.timesheet[0].hold) {
            holddata = res.response.timesheet[0].hold;
          }
        }
        this.setState({
          location: res.response.locations,
          // starttime: starttimehr,
          // endtime: endtimehr,
          breaktime: res.response.breaktime / 60 + " Minutes",
          timesheet: res.response.timesheet,
          shift_type: res.response.shift_type,
          job_type: res.response.jobtype_data[0].name,
          shifttitle: res.response.title,
          id: res.response._id,
          start_date: moment(res.response.start_date).format("DD-MM-YYYY"),
          end_date: moment(res.response.end_date).format("DD-MM-YYYY"),
          start_date_time: res.response.start_date,
          end_date_time: res.response.end_date,
          shift_option: res.response.shift_option,
          status: res.response.status,
          notes: res.response.notes,
          hold: holddata,
          timesheet_status: res.response.timesheet_status
        });
      }
    });
    const start = moment().startOf("month");
    const end = moment().endOf("month");
    request({
      url: "/employeedashboard",
      method: "post",
      data: { selectshift: this.state.selectshift, selecttimesheet: this.state.selecttimesheet, selectinvoice: this.state.selectinvoice, startdate: start, enddate: end }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          if (res.response.result[0].totalhours) {
            const pretotal = Math.abs(res.response.result[0].totalhours.hours);
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const start_split = (pretotal / 3600).toString().split(".");
            const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
            const totalhours = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
            // const total = (pretotal / 3600).toFixed(2);
            // const split = total.split('.');
            // const totalhours = `${split[0]}:` + split[1] || `00`;
            this.setState({ totalhours });
          } else {
            this.setState({ totalhours: "0:00" });
          }
        }
      }
    });
  }
  eventmodel = event => {
    if (event.status === 12) {
      this.setState({
        HolidayModal: !this.state.HolidayModal,
        HolidayName: event.title,
        HolidayDate: event.dates,
        HolidayDesc: event.desc
      });
    } else {
      this.setState({
        eventlarge: !this.state.eventlarge
      });
      this.eventset(event);
    }
  };
  HolidayModalclose = () => {
    this.setState({
      HolidayModal: false,
      HolidayName: "",
      HolidayDate: "",
      HolidayDesc: ""
    });
  };
  eventset = event => {
    const daycounts = 0;
    this.setState({
      daycount: daycounts,
      date_list: [],
      Weekly_select_date_list: [],
      montofri: 0,
      Weekly_select_date_count: 0,
      monthly_date_count: 0,
      count: 0
    });
    if (event) {
      request({
        url: "/employee/view_shift",
        method: "POST",
        data: {
          shiftId: event.id
        }
      }).then(res => {
        if (res.status === 1) {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = res.response.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startsec = splithr[1];
          const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
          const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          const eveendhr = res.response.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          const holddata = res.response.timesheet.length > 0 && res.response.timesheet[0].hold ? res.response.timesheet[0].hold : "";
          this.setState({
            location: res.response.locations,
            client: res.response.client,
            branch: res.response.branch[0].branches.branchname,
            starttime: starttimehr,
            endtime: endtimehr,
            breaktime: res.response.breaktime / 60 + " Minutes",
            shift_type: res.response.shift_type,
            timesheet: res.response.timesheet,
            job_type: res.response.jobtype_data[0].name,
            shifttitle: res.response.title,
            id: res.response._id,
            start_date: moment(res.response.start_date).format("DD-MM-YYYY"),
            end_date: moment(res.response.end_date).format("DD-MM-YYYY"),
            start_date_time: res.response.start_date,
            starttime_cond: res.response.starttime,
            end_date_time: res.response.end_date,
            shift_option: res.response.shift_option,
            status: res.response.status,
            notes: res.response.notes,
            hold: holddata,
            timesheet_status: res.response.timesheet_status,
            distance: res.response.distance,
            branchaddress: `${res.response.branch.length ? res.response.branch[0].branches.branchformatted_address : ""} , ${res.response.branch.length ? res.response.branch[0].branches.branchzipcode : ""}`,
            branch_location: res.response.branch.length ? res.response.branch[0].branches : "",
            employee_location: res.response.employeeaddress.length ? res.response.employeeaddress[0] : "",
            branchphone: res.response.branch.length ? res.response.branch[0].branches.phone : "",
            maplocation: [
              {
                lat: res.response.employeeaddress.length ? res.response.employeeaddress[0].lat : 51.50853,
                lng: res.response.employeeaddress.length ? res.response.employeeaddress[0].lon : -0.12574,
                label: "Y",
                draggable: false
              },
              {
                lat: res.response.branch.length ? res.response.branch[0].branches.branchlat : 51.50853,
                lng: res.response.branch.length ? res.response.branch[0].branches.branchlng : -0.12574,
                label: "S",
                draggable: false
              }
            ],
            avatar: res.response.employee_avatar && res.response.employee_avatar.length > 0 ? res.response.employee_avatar[0] : ""
          });
        }

        // if (res.response.shift_option) {
        //   var days = Math.floor((moment(res.response.end_date) - moment(res.response.start_date)) / 86400000) + 1;
        //   if (res.response.shift_option === "Daily") {
        //     this.setState({
        //       daycount: Math.floor((moment(res.response.end_date) - moment(res.response.start_date)) / 86400000) + 1
        //     });
        //   } else if (res.response.shift_option === "Every Weekday (Mon - Fri)") {
        //     var totaldays = Math.floor((moment(res.response.end_date) - moment(res.response.start_date)) / 86400000) + 1;
        //     for (var i = 0; i < totaldays; i++) {
        //       const startdate = moment(res.response.start_date)._d.getDay();
        //       var nums = startdate + i;
        //       if (nums < 7) {
        //         this.state.date_list.push(nums);
        //       } else {
        //         const newCount = nums - 7;
        //         if (newCount < 7) {
        //           this.state.date_list.push(newCount);
        //         } else {
        //           const div = Math.trunc(newCount / 7);
        //           const mul = div * 7;
        //           const sub = newCount - mul;
        //           this.state.date_list.push(sub);
        //         }
        //       }
        //     }
        //     if (this.state.date_list) {
        //       var n = 0;
        //       this.state.date_list.map((data, key) => {
        //         if (data !== 0 && data !== 6) {
        //           var count = n++ + 1;
        //           this.setState({
        //             daycount: count
        //           });
        //         }
        //         return true;
        //       });
        //     }
        //   } else if (res.response.shift_option === "Weekly (Selected Days)") {
        //     res.response.option_data.map((option_data_lists, key1) => {
        //       if (option_data_lists.sun === true) {
        //         const sundaynum = 0;
        //         this.state.Weekly_select_date_list.push(sundaynum);
        //       }
        //       if (option_data_lists.mon === true) {
        //         const mondaynum = 1;
        //         this.state.Weekly_select_date_list.push(mondaynum);
        //       }
        //       if (option_data_lists.tue === true) {
        //         const tuedaynum = 2;
        //         this.state.Weekly_select_date_list.push(tuedaynum);
        //       }
        //       if (option_data_lists.wed === true) {
        //         const weddaynum = 3;
        //         this.state.Weekly_select_date_list.push(weddaynum);
        //       }
        //       if (option_data_lists.thu === true) {
        //         const thudaynum = 4;
        //         this.state.Weekly_select_date_list.push(thudaynum);
        //       }
        //       if (option_data_lists.fri === true) {
        //         const fridaynum = 5;
        //         this.state.Weekly_select_date_list.push(fridaynum);
        //       }
        //       if (option_data_lists.sat === true) {
        //         const satdaynum = 6;
        //         this.state.Weekly_select_date_list.push(satdaynum);
        //       }
        //       return true;
        //     });
        //     var totaldays_weekly = Math.floor((moment(res.response.end_date) - moment(res.response.start_date)) / 86400000) + 1;
        //     for (var j = 0; j < totaldays_weekly; j++) {
        //       const startdate = moment(res.response.start_date)._d.getDay();
        //       var weeklynum = startdate + j;
        //       if (weeklynum < 7) {
        //         this.state.date_list.push(weeklynum);
        //       } else {
        //         const newCount = weeklynum - 7;
        //         if (newCount < 7) {
        //           this.state.date_list.push(newCount);
        //         } else {
        //           const div = Math.trunc(newCount / 7);
        //           const mul = div * 7;
        //           const sub = newCount - mul;
        //           this.state.date_list.push(sub);
        //         }
        //       }
        //     }
        //     if (this.state.date_list && this.state.Weekly_select_date_list) {
        //       var m = 0;
        //       const Weekly_select_date_list = [0, 1, 2];
        //       this.state.date_list.map((date_lists, key) => {
        //         this.state.Weekly_select_date_list.map((select_date_lists, key1) => {
        //           if (date_lists === select_date_lists) {
        //             var count = m++ + 1;
        //             this.setState({
        //               daycount: count
        //             });
        //           }
        //           return true;
        //         });
        //         return true;
        //       });
        //     }
        //   } else if (res.response.shift_option === "Monthly (Day: 20)") {
        //     var arr = [];
        //     var dt = new Date(moment(res.response.start_date));
        //     while (dt <= moment(res.response.end_date)) {
        //       arr.push(new Date(dt));
        //       dt.setDate(dt.getDate() + 1);
        //     }
        //     var x = 0;
        //     arr.map((dates, key1) => {
        //       if (dates.getDate() === 20) {
        //         var count = x++ + 1;
        //         this.setState({
        //           daycount: count
        //         });
        //       }
        //       return true;
        //     });
        //   } else {
        //     // const third = next_day(trunc(sysdate,'mm')-1,'Saturday') + 14
        //   }
        // }
      });
    }
  };
  eventcancel = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      shifttitle: "",
      location: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      start_date: "",
      end_date: "",
      shift_type: "",
      job_type: "",
      branch: "",
      client: "",
      shift_option: "",
      distance: "",
      branchaddress: "",
      maplocation: []
    });
  };
  accept(item) {
    let allowJob;
    this.state.shiftlist &&
      this.state.shiftlist.map(list => {
        if (moment(list.start_date).format("DD-MM-YYYY") === item.start_date && list.starttime === item.starttime && list.endtime === item.endtime && (list.status === 3 || list.status === 4 || list.status === 5)) {
          return (allowJob = true);
        }
        return (allowJob = false);
      });
    if (allowJob) {
      toast.error("Shift on Same Date & Time already Accepted");
    } else {
      request({
        url: "/employee/accept_job",
        method: "POST",
        data: {
          shiftId: this.state.id
        }
      })
        .then(res => {
          if (res.status === 1) {
            let toastId = "acceptjobRequest";
            if (!toast.isActive(toastId)) {
              toastId = toast.success("Accepted");
            }
            this.componentDidMount();
            this.eventset();
          } else if (res.status === 0) {
            toast.error(res.response);
            this.componentDidMount();
            this.eventset();
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  }
  unAssign(item) {
    request({
      url: "/employee/unassignshift",
      method: "POST",
      data: {
        id: item.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          let toastId = "Un-assigned";
          if (!toast.isActive(toastId)) {
            toastId = toast.success("Shift Cancelled");
          }
          this.props.history.push("/user/shiftslist");
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
          this.componentDidMount();
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  startshift(item) {
    let allowJob = false;
    this.state.shiftlist &&
      this.state.shiftlist.map(list => {
        if (list.shiftId !== item.shift_id && moment(list.start_date).format("DD-MM-YYYY") === item.start_date && list.starttime === item.starttime && list.endtime === item.endtime && list.status === 5) {
          return (allowJob = true);
        } else if (list.status === 5) {
          return (allowJob = true);
        }
        return true;
      });
    if (allowJob) {
      toast.error("Shift on Same Date & Time already Ongoing");
    } else {
      request({
        url: "/shift/update/start",
        method: "POST",
        data: {
          shiftId: this.state.id
        }
      })
        .then(res => {
          if (res.status === 1) {
            toast.success(res.response);
            this.componentDidMount();
            this.eventset();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  }
  endshift(item) {
    request({
      url: "/shift/update/end",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.componentDidMount();
          this.eventset();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  holdshift(item) {
    request({
      url: "/shift/update/hold",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          // this.eventset();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  restartshift(item) {
    request({
      url: "/shift/update/restart",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          // this.eventset();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  sendTimeSheet(item) {
    request({
      url: "/employee/timesheet_request",
      method: "POST",
      data: {
        shiftId: this.state.id
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.componentDidMount();
          this.eventset();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  eventStyleGetter(event, start, end, isSelected) {
    if (event.status === 2) {
      const styless = {
        backgroundColor: "#ffc107",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 3 || event.status === 4) {
      const styless = {
        backgroundColor: "#17a2b8",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 5) {
      const styless = {
        backgroundColor: "#4dbd74",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 6 || event.status === 7) {
      const styless = {
        backgroundColor: "#f86c6b",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 12) {
      const styless = {
        backgroundColor: "#e66929",
        color: "white"
      };
      return {
        style: styless
      };
    } else {
      const styless = {};
      return {
        style: styless
      };
    }
  }
  RangeHandler = range => {
    if (range) {
      if (range.length > 0) {
        if (range.length === 7) {
          const end = moment(range[6]).endOf("day");
          this.RangeRequest(range[0], end);
        } else {
          const end = moment(range[0]).endOf("day");
          this.RangeRequest(range[0], end);
        }
      }
    }
  };
  NavHandler = (range, view) => {
    if (range && view === "month") {
      const end = moment(range).endOf("month");
      this.RangeRequest(range, end);
    }
  };
  RangeRequest = (start, end) => {
    request({
      url: "/employeedashboard",
      method: "post",
      data: { selectshift: this.state.selectshift, selecttimesheet: this.state.selecttimesheet, selectinvoice: this.state.selectinvoice, startdate: start, enddate: end }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          if (res.response.result[0].totalhours) {
            const pretotal = Math.abs(res.response.result[0].totalhours.hours);
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const start_split = (pretotal / 3600).toString().split(".");
            const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
            const totalhours = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
            this.setState({ totalhours });
          } else {
            this.setState({ totalhours: "0:00" });
          }
        }
      }
    });
  };
  render() {
    const date_cond_previous = this.state.starttime_cond - 3600;
    const date_cond_current = moment(new Date()).format("HH:mm");
    const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    const evestarthr = date_cond_previous / 3600;
    const splithr = evestarthr.toString().split(".");
    const startingsecs = splithr[1];
    const startingminutes = startingsecs === undefined ? "00" : (+"0" + "." + startingsecs) * 60;
    const Time_Cond = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startingminutes : +"0" + "" + splithr[0] + ":" + startingminutes;
    const MapWithADirectionsRenderer = compose(
      withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBWZKvXdWQy5J80V5HQabwV_lcL70CGHQg&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />
      }),
      withScriptjs,
      withGoogleMap,
      lifecycle({
        componentDidMount() {
          const DirectionsService = new google.maps.DirectionsService();
          DirectionsService.route(
            {
              origin: new google.maps.LatLng(
                this.props && this.props.locationdetails.employee && this.props.locationdetails.employee.lat ? this.props.locationdetails.employee.lat : 41.85073,
                this.props && this.props.locationdetails.employee && this.props.locationdetails.employee.lon ? this.props.locationdetails.employee.lon : -87.65126
              ),
              destination: new google.maps.LatLng(
                this.props && this.props.locationdetails.branch && this.props.locationdetails.branch.branchlat ? this.props.locationdetails.branch.branchlat : 41.85258,
                this.props && this.props.locationdetails.branch && this.props.locationdetails.branch.branchlng ? this.props.locationdetails.branch.branchlng : -87.65141
              ),
              travelMode: google.maps.TravelMode.DRIVING
            },
            (result, status) => {
              if (status === google.maps.DirectionsStatus.OK) {
                this.setState({
                  directions: result,
                  maplocation: this.props && this.props.locationdetails ? this.props.locationdetails.maplocation : [],
                  employee_image: this.props && this.props.locationdetails && this.props.locationdetails.avatar
                });
              } else {
                console.error(`error fetching directions ${result}`);
              }
            }
          );
        }
      })
    )(props => (
      <GoogleMap defaultZoom={4} defaultCenter={new google.maps.LatLng(41.85073, -87.65126)} defaultOptions={{ gestureHandling: "greedy" }}>
        {props.directions && <DirectionsRenderer directions={props.directions} options={{ suppressMarkers: true }} />}
        {props.maplocation && (
          <Marker
            position={{
              lat: props && props.maplocation && props.maplocation[0] ? props.maplocation[0].lat : "",
              lng: props && props.maplocation && props.maplocation[0] ? props.maplocation[0].lng : "",
              label: "S",
              draggable: false
            }}
            title={"Your Location"}
            // label={"Y"}
            icon={{ url: NodeURL + "/uploads/map/icons8-building-48.png", size: { width: 60, height: 100 }, anchor: { x: 15, y: 50 }, scaledSize: { width: 40, height: 40 } }}
          />
        )}
        {props.maplocation && (
          <Marker
            position={{
              lat: props && props.maplocation && props.maplocation[1] ? props.maplocation[1].lat : "",
              lng: props && props.maplocation && props.maplocation[1] ? props.maplocation[1].lng : "",
              label: "S",
              draggable: false
            }}
            title={"Shift Location"}
            // label={"S"}
            icon={{ url: props && props.employee_image ? NodeURL + "/" + props.employee_image : "", size: { width: 60, height: 100 }, anchor: { x: 15, y: 50 }, scaledSize: { width: 40, height: 40 } }}
          />
        )}
      </GoogleMap>
    ));
    const formats = {
      timeGutterFormat: "HH:mm",
      eventTimeRangeFormat: () => ""
    };
    return (
      <div className="right-edit-side">
        <div className="form__card card form__card--background float new-forms-profile">
          <div className="form__wrap extra">
            <div className="animated">
              <Card className="p-0 mb-0 for-radius">
                <CardHeader className="for-radius">
                  Shifts
                  <div
                    className="float-right"
                    style={tStyle}
                    onClick={() => {
                      this.props.history.push("/user/shiftslist");
                    }}
                  >
                    <button type="button" className="btn btn-lists">
                      <i className="fa fa-list" /> List View
                      {/* <small className="text-muted" />*/}
                    </button>
                  </div>
                </CardHeader>
                <Row className="d-flex justify-content-end mt-2">
                  <Col md="3">
                    <Button color="success" title="Total Hours" className={"pull-right"}>
                      <i className="fa fa-clock-o" /> {this.state.totalhours} hrs
                    </Button>
                  </Col>
                </Row>
                <CardBody style={{ height: "40em" }} className="p-2 cal-widths">
                  <BigCalendar
                    className="d-sm-down-none"
                    {...this.props}
                    popup
                    events={this.state.eventsdata}
                    onSelectEvent={event => this.eventmodel(event)}
                    views={["month", "week", "day"]}
                    step={30}
                    messages={{
                      previous: this.state.previous,
                      today: "Today",
                      next: this.state.next,
                      month: "Month",
                      week: "Week",
                      day: "Day"
                    }}
                    defaultDate={new Date(currYear, currMonth, 1)}
                    defaultView="month"
                    toolbar={true}
                    eventPropGetter={this.eventStyleGetter}
                    formats={formats}
                    onRangeChange={range => this.RangeHandler(range)}
                    onNavigate={(range, view) => this.NavHandler(range, view)}
                  />
                </CardBody>
              </Card>
              <Modal isOpen={this.state.eventlarge} toggle={this.eventmodel} className={"modal-lg " + this.props.className}>
                <ModalHeader toggle={this.eventcancel}>Shift Details</ModalHeader>
                <ModalBody>
                  <Row className="shi-calen">
                    <Col xs="12" md="6">
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Shift
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.shifttitle}</span>
                        </Col>
                        <Col xs="12" md="12">
                          <b>
                            Client
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.client}</span>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Area
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.location}</span>
                        </Col>
                        <Col xs="12" md="12">
                          <b>
                            Work Location
                            {/* Branch */}
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.branch}</span>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Address
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.branchaddress}</span>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Distance
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.distance}</span>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Role
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.job_type}</span>
                        </Col>
                        <Col xs="12" md="12">
                          <b>
                            Date
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.start_date}</span>
                        </Col>
                        {/* <Col xs="12" md="6">
                          <b>
                            End Date<span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.end_date}</span>
                        </Col> */}
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Time
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">
                            {this.state.starttime} - {this.state.endtime}
                          </span>
                        </Col>
                        <Col xs="12" md="12">
                          <b>
                            Break
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.breaktime}</span>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <b>
                            Notes
                            <span>:</span>
                          </b>{" "}
                          <span className="name-lct">{this.state.notes}</span>
                        </Col>
                      </Row>
                      {this.state.timesheet ? (
                        <Row>
                          <Col xs="12" md="12">
                            {this.state.timesheet.length > 0 && this.state.timesheet[0].start ? (
                              <Fragment>
                                <b>
                                  Started Time <span>:</span>{" "}
                                </b>{" "}
                                {moment(this.state.timesheet[0].start).format("HH:mm")}
                              </Fragment>
                            ) : null}
                          </Col>
                          <Col xs="12" md="12">
                            {this.state.timesheet.length > 0 && this.state.timesheet[0].holdedmins ? (
                              <Fragment>
                                <b>
                                  Holded <span>:</span>{" "}
                                </b>{" "}
                                {Math.round(this.state.timesheet[0].holdedmins / 60)} Minutes
                              </Fragment>
                            ) : null}
                          </Col>
                          <Col xs="12" md="12">
                            {this.state.timesheet.length > 0 && this.state.timesheet[0].end ? (
                              <Fragment>
                                <b>
                                  Ended Time <span>:</span>{" "}
                                </b>{" "}
                                {moment(this.state.timesheet[0].end).format("HH:mm")}
                              </Fragment>
                            ) : null}
                          </Col>
                        </Row>
                      ) : null}
                    </Col>
                    <Col xs="12" md="6" className={"pl-0"}>
                      {this.state &&
                        this.state.employee_location &&
                        this.state.branch_location && (
                          <MapWithADirectionsRenderer
                            locationdetails={{
                              branch: this.state.branch_location,
                              employee: this.state.employee_location,
                              maplocation: this.state.maplocation,
                              avatar: this.state.avatar
                            }}
                          />
                        )}
                    </Col>
                  </Row>
                </ModalBody>
                <ModalFooter className="modal-gal">
                  <span className="changeShift">
                    {this.state.status === 2 ? (
                      <Fragment>
                        <Button color="primary" onClick={id => this.accept(this.state)} id={`edit2`}>
                          <i className="fa fa-paper-plane" />
                        </Button>
                        <UncontrolledTooltip placement="top" target={`edit2`}>
                          Accept Shift
                        </UncontrolledTooltip>
                      </Fragment>
                    ) : null}
                    {this.state.status === 3 ? <Badge color="warning">Shift Accepted</Badge> : null}
                    {this.state.status <= 4 ? (
                      <Fragment>
                        {this.state.start_date <= moment(new Date()).format("DD-MM-YYYY") && date_cond_current < this.state.starttime ? (
                          <Button className=" mr-1 text-center" color="danger" onClick={id => this.unAssign(this.state)}>
                            Cancel Shift
                          </Button>
                        ) : null}
                      </Fragment>
                    ) : null}
                    {this.state.status === 4 ? (
                      <Fragment>
                        {this.state.start_date === moment(new Date()).format("DD-MM-YYYY") && date_cond_current >= Time_Cond ? (
                          <Button className=" mr-1" color="primary" onClick={id => this.startshift(this.state)}>
                            Start
                          </Button>
                        ) : (
                          <span> You Can Able to Start Shift On Assigned Date & Before Shift Assigned Time Only </span>
                        )}
                      </Fragment>
                    ) : null}
                    {this.state.status === 5 && this.state.hold === "" ? (
                      <Button className=" mr-1" color="warning" onClick={id => this.holdshift()}>
                        Hold
                      </Button>
                    ) : null}
                    {this.state.status === 5 && this.state.hold !== "" ? (
                      <Button className=" mr-1" color="warning" onClick={id => this.restartshift()}>
                        Restart
                      </Button>
                    ) : null}
                    {this.state.status === 5 && this.state.hold === "" ? (
                      <Button className=" mr-1" color="danger" onClick={id => this.endshift()}>
                        End
                      </Button>
                    ) : null}
                    {this.state.status === 6 ? (
                      <Fragment>
                        <Badge color="danger" className="shiftComp">
                          Shift Completed
                        </Badge>
                        {this.state.timesheet_status === 0 ? (
                          <Button size="sm" className=" mr-1 ml-5" color="success" onClick={id => this.sendTimeSheet()}>
                            Send Time Sheet <i className="fa fa-envelope-open ml-2" />
                          </Button>
                        ) : null}
                      </Fragment>
                    ) : null}
                    {this.state.status === 6 && this.state.timesheet_status === 1 ? (
                      <Fragment>
                        <Badge color="warning" className=" timesheet-Send">
                          Time Sheet Send
                        </Badge>
                      </Fragment>
                    ) : null}
                    {this.state.status === 7 ? (
                      <Badge color="danger" className="processCompleted">
                        Timesheet Approved
                      </Badge>
                    ) : null}
                    {this.state.status === 8 ? (
                      <Badge color="danger" className="processCompleted">
                        Invoice Approved
                      </Badge>
                    ) : null}
                    {this.state.status === 9 ? (
                      <Badge color="danger" className="processCompleted">
                        Payment Completed
                      </Badge>
                    ) : null}
                  </span>
                  <Button color="secondary" className="ic-bt" onClick={this.eventcancel}>
                    <i className="fa fa-close" />
                  </Button>
                </ModalFooter>
              </Modal>
              <Modal isOpen={this.state.HolidayModal} toggle={this.HolidayModalOpen} className={this.props.className}>
                <ModalHeader toggle={this.HolidayModalOpen}>Holiday Details</ModalHeader>
                <ModalBody>
				
                  <Row>
				  <div className="dash-points">
                    <Col xs="12" md="12">
                      <span className="po-left"><b><i class="fa fa-user-o" aria-hidden="true"></i> Name:</b></span>
					  <span className="po-right"> {this.state.HolidayName}</span>
                    </Col>
					</div>	
                  </Row>
				  
				   <Row>
				  <div className="dash-points">
                  
                    <Col xs="12" md="12">
                     <span className="po-left"><b><i class="fa fa-clock-o" aria-hidden="true"></i> Date:</b></span>
					  <span className="po-right"> {moment(this.state.HolidayDate).format("DD-MM-YYYY")}</span>
                    </Col>

					</div>	
                  </Row>
				  
				   <Row>
				  <div className="dash-points">
                    <Col xs="12" md="12">
                      <span className="po-left"><b><i class="fa fa-commenting-o" aria-hidden="true"></i> Description:</b></span> <span className="po-right"> {this.state.HolidayDesc}</span>
                    </Col>
					</div>	
                  </Row>
				  
                </ModalBody>
                <ModalFooter className="modal-gal">
                  <Button color="secondary" className="ic-bt" onClick={this.HolidayModalclose}>
                    <i class="fa fa-close"></i>
                  </Button>
                </ModalFooter>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Shiftcalender;
