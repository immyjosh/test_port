import React, { Component, Fragment } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, Col, Row, Table, UncontrolledTooltip, Badge, CardHeader, Input, InputGroup } from "reactstrap";
import request from "../../../api/api";
import Widget02 from "../../../views/Template/Widgets/Widget02";
import Pagination from "react-js-pagination";
import moment from "moment";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "180px"
};

class shiftslist extends Component {
  state = {
    shiftlist: [],
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      status: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    requests: 0,
    accepted: 0,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    finished: 0
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (!token) {
      return this.props.history.replace("/login");
    }
    this.populateData();
  }
  populateData() {
    request({
      url: "/employee/shiftlist",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        let getshiftdata = res.response.result.map((item, i) => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          let splithr = (item.starttime / 3600).toString().split(".");
          let startmins = splithr[1] === undefined ? "00" : (+"0" + "." + splithr[1]) * 60;
          let starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          var splitendhr = (item.endtime / 3600).toString().split(".");
          let endmins = splitendhr[1] === undefined ? "00" : (+"0" + "." + splitendhr[1]) * 60;
          let endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          return {
            _id: item._id,
            starttime: starttimehr,
            endtime: endtimehr,
            location: item.locations,
            client: item.client,
            breaktime: item.breaktime / 60 + " mins",
            start_date: item.start_date,
            end_date: item.end_date,
            job_type: item.job_type,
            branch: item.branch_data.length && item.branch_data[0].branches.branchname,
            title: item.title,
            shiftId: item.shiftId,
            distance: item.distance,
            status: item.status
          };
        });
        this.setState({
          shiftlist: getshiftdata,
          requests: res.response.groupcount.requests,
          accepted: res.response.groupcount.accepted,
          assigned: res.response.groupcount.assigned,
          completed: res.response.groupcount.completed,
          ongoing: res.response.groupcount.ongoing,
          timehseet_approved: res.response.groupcount.timehseet_approved,
          invoice_approved: res.response.groupcount.invoice_approved,
          Payment_completed: res.response.groupcount.Payment_completed,
          expired: res.response.groupcount.expired,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["start_date", "starttime", "start_date", "branch", "location", "client", "title", "status", "distance"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }

  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Shift") {
        state.tableOptions.filter = "title";
      }
    });
    this.populateData();
  }
  buttonfilter(value) {
    this.setState(state => {
      if (value === "All") {
        state.tableOptions.status = "";
      } else if (value === "Request") {
        state.tableOptions.status = 2;
      } else if (value === "Requested") {
        state.tableOptions.status = 3;
      } else if (value === "Assigned") {
        state.tableOptions.status = 4;
      } else if (value === "Ongoing") {
        state.tableOptions.status = 5;
      } else if (value === "Completed") {
        state.tableOptions.status = 6;
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  //   export() {
  //     request({
  //       url: "/administrators/client/userexport",
  //       method: "GET"
  //     }).then(res => {
    // if (res.status === 0) {
    //   toast.error(res.response);
    // } else if (res.status === 1) {
    //   fileDownload(res.response, "export.csv");
    //   toast.success("Document Exported!");
    // }
  //     });
  //   }
  fromTo() {
    if (this.state.start_date !== null || this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  ShiftChange = num => {
    if (num) {
      this.setState(state => {
        state.tableOptions.status = num;
        this.populateData();
      })
    } else {
      this.setState(state => {
        state.tableOptions.status = "";
        this.populateData();
      })
    }
  }

  accept(e, item) {
    e.stopPropagation();
    let allowJob = false;
    this.state.shiftlist &&
      this.state.shiftlist.map(list => {
        // console.log(
        //   moment(list.start_date).format("DD-MM-YYYY"), moment(item.start_date).format("DD-MM-YYYY") ,
        //     list.starttime, item.starttime ,
        //     list.endtime, item.endtime ,
        //     list.status, 3
        // );
        if (
          moment(list.start_date).format("DD-MM-YYYY") === moment(item.start_date).format("DD-MM-YYYY") &&
          list.starttime === item.starttime &&
          list.endtime === item.endtime &&
          (list.status === 3 || list.status === 4 || list.status === 5)
        ) {
          return (allowJob = true);
        }
        return (allowJob = false);
      });
    if (allowJob) {
      toast.error("Shift on Same Date & Time already Accepted");
    } else {
      request({
        url: "/employee/accept_job",
        method: "POST",
        data: {
          shiftId: item._id
        }
      })
        .then(res => {
          if (res.status === 1) {
            let toastId = "acceptjobRequest";
            if (!toast.isActive(toastId)) {
              toastId = toast.success("Accepted");
            }
            this.componentDidMount();
          } else if (res.status === 0) {
            toast.error(res.response);
            this.componentDidMount();
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  }
  shiftview = item => {
    this.props.history.push({ pathname: "/user/shiftview", state: item });
  };
  render() {
    // let current = this.state.tableOptions.page.current;
    let limit = this.state.tableOptions.limit;
    let len = Math.ceil(this.state.pages / limit);
    let arr = [];
    for (let i = 1; i <= len; i++) {
      arr.push(i);
    }
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <Fragment>
        {/*<ToastContainer position="top-right" autoClose={2500} />*/}
        {/* <!--Form--> */}
        <div className="right-edit-side">
          <div className="form__card card form__card--background float shift-lidt mobile-shift">
            <div className="form__wrap extra card for-radius">
              <div id="menu1" className="card">
                <CardHeader className="for-radius">
                  <span className="cursor-pointer" onClick={() => this.ShiftChange("")}>Shifts List</span>
                  <div
                    className="float-right"
                    style={tStyle}
                    onClick={() => {
                      this.props.history.push("/user/calenderview");
                    }}
                  >
                    <button type="button" className="btn btn-lists" style={tStyles}>
                      <i className="fa fa-calendar" /> Calendar View
                      {/*<small className="text-muted" />*/}
                    </button>
                  </div>
                </CardHeader>

                <Row className="for-card">
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(2)} header={this.state.requests ? this.state.requests : "0"} mainText="Request" icon="fa fa-send" color="primary" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(3)} header={this.state.accepted ? this.state.accepted : "0"} mainText="Accepted" icon="fa fa-check-circle" color="warning" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(4)} header={this.state.assigned ? this.state.assigned : "0"} mainText="Assigned" icon="fa fa-list" color="info" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(5)} header={this.state.ongoing ? this.state.ongoing : "0"} mainText="Ongoing" icon="fa fa-spinner" color="success" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(6)} header={this.state.completed ? this.state.completed : "0"} mainText="Completed" icon="fa fa-check-square-o" color="success" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(7)} header={this.state.timehseet_approved ? this.state.timehseet_approved : "0"} mainText="Timesheets Approved" icon="fa fa-check" color="warning" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(8)} header={this.state.invoice_approved ? this.state.invoice_approved : "0"} mainText="Invoice Approved" icon="fa fa-check" color="info" />
                  </Col>
                  <Col xs="12" sm="6" lg="3" className="parti-widths">
                    <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(9)} header={this.state.Payment_completed ? this.state.Payment_completed : "0"} mainText="Paid" icon="fa fa-money" color="danger" />
                  </Col>
                </Row>

                <div id="menu1" className="">
                  {/*<h3 className="task1" />*/}
                  <div>
                    {/* <Row className="">
                              <Button
                                color="success"
                                className="ml-2"
                                onClick={() => this.buttonfilter("All")}
                              >
                                All
                              </Button>
                              <Button
                                color="success"
                                className="ml-2"
                                onClick={() => this.buttonfilter("Request")}
                              >
                                Request
                              </Button>
                              <Button
                                color="success"
                                className="ml-2"
                                onClick={() => this.buttonfilter("Requested")}
                              >
                                Requested
                              </Button>
                              <Button
                                color="success"
                                className="ml-2"
                                onClick={() => this.buttonfilter("Assigned")}
                              >
                                Assigned
                              </Button>
                              <Button
                                color="success"
                                className="ml-2"
                                onClick={() => this.buttonfilter("Ongoing")}
                              >
                                Ongoing
                              </Button>
                              <Button
                                color="success"
                                className="ml-2"
                                onClick={() => this.buttonfilter("Completed")}
                              >
                                Completed
                              </Button>
                            </Row> */}
                  </div>
                </div>
              </div>
              <div className="row d-flex adj-search">
                <div className="left-search">
                  <div className="left-dachs">
                    <DateRangePicker
                      showClearDates={true}
                      startDate={this.state.start_date}
                      startDateId="start_date"
                      endDate={this.state.end_date}
                      endDateId="end_date"
                      onDatesChange={({ startDate, endDate }) => {
                        this.setState({
                          start_date: startDate,
                          end_date: endDate
                        });
                      }}
                      isOutsideRange={day => day.isBefore(this.state.start_date)}
                      focusedInput={this.state.focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      displayFormat="DD-MM-YYYY"
                    />
                    <Button
                      size="lg"
                      color="primary"
                      onClick={() => {
                        this.fromTo();
                      }}
                    >
                      <i className="fa fa-search" />
                    </Button>
                  </div>
                </div>
                <div className="right-search">
                 {/*  <div className="right-select">
                    <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                      <option>All</option>
                    </Input>
                  </div> */}
                  <div>
                    <InputGroup>
                      <Input type="text" ref="search" placeholder="Search..." name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                    </InputGroup>
                  </div>
                </div>
              </div>
              <div className="table-responsive mt-2 p-2">
                <Table hover bordered responsive>
                  <thead>
                    <tr>
                      <th>S.No. </th>
                      <th
                        onClick={() => {
                          this.sort("title");
                        }}
                      >
                        Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("client");
                        }}
                      >
                        Client <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("location");
                        }}
                      >
                        Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="location" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("branch");
                        }}
                      >
                        Work Location {/*Branch*/} <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("distance");
                        }}
                      >
                        Distance {/*Branch*/} <i style={{ paddingLeft: "25px" }} className={sorticondef} id="distance" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("starttime");
                        }}
                      >
                        Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("start_date");
                        }}
                      >
                        Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" />
                      </th>
                      <th
                        onClick={() => {
                          this.sort("status");
                        }}
                      >
                        Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                      </th>
                      <th>Actions </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.shiftlist.length > 0 ? (
                      this.state.shiftlist.map((item, i) => (
                        <tr key={item.shiftId} onClick={id => this.shiftview(item)}>
                          <td>{this.state.tableOptions.skip + i + 1}</td>
                          <td>{item.title}</td>
                          <td>{item.client}</td>
                          <td>{item.location}</td>
                          <td>{item.branch}</td>
                          <td>{item.distance}</td>
                          <td>
                            {item.starttime} - {item.endtime}
                          </td>
                          <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                          <td className="border-curve">
                            {item.status === 2 ? <Badge color="secondary">Employee requested</Badge> : null}
                            {item.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                            {item.status === 4 ? <Badge color="secondary">Shift Assigned</Badge> : null}
                            {item.status === 5 ? <Badge color="info">Shift Ongoing</Badge> : null}
                            {item.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                            {item.status === 7 ? <Badge color="success">Timesheet Approved</Badge> : null}
                            {item.status === 8 ? <Badge color="success">Earnings Approved</Badge> : null}
                            {item.status === 9 ? <Badge color="success">Payment Completed</Badge> : null}
                            {item.status === 10 ? <Badge color="danger">Shift Expired</Badge> : null}
                          </td>
                          <td>
                            {item.status === 2 ? (
                              <Fragment>
                                <Button color="primary" onClick={(e, id) => this.accept(e, item)} id={`edit2${i}`}>
                                  <i className="fa fa-paper-plane" />
                                </Button>
                                <UncontrolledTooltip placement="top" target={`edit2${i}`}>
                                  Accept Shift
                                </UncontrolledTooltip>
                              </Fragment>
                            ) : null}
                            {item.status >= 2 ? (
                              <Fragment>
                                <Button color="view-table" className="ml-2" onClick={id => this.shiftview(item)} id={`edit${i}`}>
                                  <i className="fa fa-eye" />
                                </Button>
                                <UncontrolledTooltip placement="right" target={`edit${i}`}>
                                  View Details
                                </UncontrolledTooltip>
                              </Fragment>
                            ) : null}
                          </td>
                          {/* <td>{item.breaktime / 60 + " mins"}</td> */}
                        </tr>
                      ))
                    ) : (
                      <tr className="text-center">
                        <td colSpan={11}>
                          <h2>
                            <strong>
                              <h4>No record available</h4>
                            </strong>
                          </h2>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </div>
              <Row>
                <Col xs="12" md="2">
                  <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                  </Input>
                </Col>
                <Col xs="12" md="10" className="d-flex justify-content-end">
                  <Pagination
                    prevPageText="Prev"
                    nextPageText="Next"
                    firstPageText="First"
                    lastPageText="Last"
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.tableOptions.limit}
                    totalItemsCount={this.state.pages}
                    pageRangeDisplayed={this.state.pageRangeDisplayed}
                    onChange={this.paginate}
                  />
                </Col>
              </Row>
            </div>
          </div>
        </div>
        {/* <!--Form--> */}
      </Fragment>
    );
  }
}

export default shiftslist;
