import React, { Component, Fragment } from "react";
import Header from "../common/header";
import Footer from "../common/footer";
// import { Link } from "react-router-dom";
import request from "../../../src/api/api";
import { Row, InputGroupAddon, InputGroupText, CardBody, Col, Button } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import {toast, ToastContainer} from "react-toastify";

class forgotpassword extends Component {
  state = {
    newpassword: "",
    confirmnewpassword: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    request({
      url: `/employee/resetpassword/${this.props.match.params.id}/${this.props.match.params.resetcode}`,
      method: "POST",
      data: {
        password: this.state.newpassword,
        confirmpassword: this.state.confirmnewpassword
      }
    })
      .then(res => {
        console.log('res ', res);
        if (res.status === 1) {
          this.props.history.replace("/login");
          toast.success(res.response);
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        this.logerror();
      });
  };

  render() {
    return (
      <Fragment>
        <Header />
        <ToastContainer position="top-right" autoClose={2500} />
        <section className="section section--last section--top-space small-width">
          <div className="container">
            <div className="row" />
            <div className="row form">
              <div className="login-menu-lang">
                <div className="form__card card form__card--background">
                  <div className="col-12 col-md-12">
                    <h4>Change Password </h4>
                    <AvForm onValidSubmit={this.OnFormSubmit}>
                      <CardBody>
                        <AvGroup className="input-group">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <AvInput lable="New Password" type="password" name="newpassword" placeholder="Enter new password" onChange={this.onChange} value={this.state.newpassword} required />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                        <AvGroup className="input-group">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <AvInput type="password" name="confirmnewpassword" placeholder="Enter confirm new password" onChange={this.onChange} value={this.state.confirmnewpassword} required validate={{ match: { value: "newpassword" } }} />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                        <Row>
                          <Col xs="6">
                            <Button color="primary" className="px-4">
                              Submit
                            </Button>
                          </Col>
                        </Row>
                      </CardBody>
                    </AvForm>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <img alt="" className="section__img" src="../img/img_backgroud_footer.png" />
        </section>
        <Footer />
      </Fragment>
    );
  }
}

export default forgotpassword;
