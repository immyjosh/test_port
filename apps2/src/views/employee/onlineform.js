import React, { Component } from "react";
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, Button, CustomInput, CardBody, Label, Badge } from "reactstrap";
import moment from "moment";
import { AvForm, AvField, AvGroup, AvInput, AvRadioGroup, AvRadio } from "availity-reactstrap-validation";
import request, { client } from "../../api/api";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import classnames from "classnames";
import { DateRangePicker, SingleDatePicker } from "react-dates";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import FileSaver from "file-saver";
import { NodeURL } from "../../api/api";

class onlineform extends Component {
  state = {
    otheremp: false,
    drivelicenceyes: false,
    caraccessyes: false,
    DBSyes: false,
    IELTStestyes: false,
    activeTab: "1",
    postapplyfor: "",
    postapplydetails: "",
    availablestartdate: "",
    title: "",
    firstname: "",
    middlename: "",
    lastname: "",
    homephone: "",
    mobileno: {
      number: "",
      code: "",
      dailcountry: "gb"
    },
    email: "",
    dateofbirth: "",
    insuranceno: "",
    streetAddress: "",
    addressline2: "",
    city: "",
    state: "",
    zip: "",
    country: "",
    nationality: "",
    gender: "",
    religion: "",
    race: "",
    sexualorientation: "",
    UploadIDPhoto: "",
    visapermit: "",
    visapermittype: "",
    drivinglicence: "",
    traveldetails: "",
    drivinglicenceno: "",
    caraccess: "",
    bannedfromdriving: "",
    carinsurance: "",
    vehicledocumentsvalid: "",
    englishspoken: "",
    englishwritten: "",
    otherlanguage: "",
    IELTStest: "",
    kinprefix: "",
    kinfirstname: "",
    kinlastname: "",
    kinrelationship: "",
    kinhomephone: "",
    kinmobileno: {
      number: "",
      code: "",
      dailcountry: "gb"
    },
    kinemail: "",
    kinstreetAddress: "",
    kinaddressline2: "",
    kincity: "",
    kinstate: "",
    kinzip: "",
    kincountry: "",
    firstchoice: "",
    secondchoice: "",
    thirdchoice: "",
    shortnotice: "",
    reduceworkflexibility: "",
    workstate: "",
    employer: "",
    position: "",
    duties: "",
    datefromto: "",
    salaryonleaving: "",
    institution: "",
    course: "",
    year: "",
    grade: "",
    UploadCV: "",
    firstrefrelationship: "",
    firstreffirstname: "",
    firstreflastname: "",
    firstrefphone: {
      number: "",
      code: "",
      dailcountry: "gb"
    },
    firstrefemail: "",
    firstrefconfirmemail: "",
    firstrefstreetAddress: "",
    firstrefcity: "",
    firstrefstate: "",
    firstrefzip: "",
    secondrefrelationship: "",
    secondreffirstname: "",
    secondreflastname: "",
    secondrefphone: {
      number: "",
      code: "",
      dailcountry: "gb"
    },
    secondrefemail: "",
    secondrefconfirmemail: "",
    secondrefstreetAddress: "",
    secondrefcity: "",
    secondrefstate: "",
    secondrefzip: "",
    appraiserfirstname: "",
    appraiserlastname: "",
    gradeofappraiser: "",
    appraiserstreetAddress: "",
    appraisercity: "",
    appraiserstate: "",
    appraiserzip: "",
    appraiserphone: "",
    appraiserfax: "",
    appraiseremail: "",
    appraiserconfirmemail: "",
    DBS: "",
    clear: "",
    DBSdate: "",
    disclosurenumber: "",
    tunicsize: "",
    sufferedlongtermillness: "",
    leaveforneckinjury: "",
    neckinjuries: "",
    sixweeksillness: "",
    communicabledisease: "",
    medicalattention: "",
    healthdeclarationdetails: "",
    registereddisabled: "",
    absentfromwork: "",
    statereason: "",
    GPname: "",
    GPstreetAddress: "",
    GPcity: "",
    GPstate: "",
    GPzip: "",
    contactDoctor: "",
    GPhealthdetails: "",
    medicalillness: "",
    medicalillnesscaused: "",
    treatment: "",
    needanyadjustments: "",
    needanyadjustmentsnotes: "",
    livedUK: "",
    livedUKnotes: "",
    BCGvaccination: "",
    BCGvaccinationyes: false,
    BCGvaccinationdate: "",
    cough: "",
    weightloss: "",
    fever: "",
    TB: "",
    chickenpox: "",
    chickenpoxdate: "",
    triplevaccination: "",
    polio: "",
    tetanus: "",
    hepatitisB: "",
    proneProcedures: "",
    occupationalHealthServices: "",
    criminaloffence: "",
    warningcriminaloffence: "",
    DBSdetails: "",
    passport: "",
    workhours: "",
    addemployer: [],
    userlist: [],
    employmenthistorylist: [],
    educationlist: [],
    personaldetails: [],
    work_mornings: "",
    work_evenings: "",
    work_afternoons: "",
    work_occasional: "",
    work_fulltime: "",
    work_parttime: "",
    work_nights: "",
    work_weekends: "",
    work_anytime: "",
    movinghandling: "",
    basiclifesupport: "",
    healthsafety: "",
    firesafety: "",
    firstaid: "",
    infectioncontrol: "",
    foodsafety: "",
    medicationadmin: "",
    safeguardingvulnerable: "",
    trainingdates: "",
    healthcare_training: "",
    diplomal3: "",
    diplomal2: "",
    personalsafetymental: "",
    intermediatelife: "",
    advancedlife: "",
    complaintshandling: "",
    handlingviolence: "",
    doolsmental: "",
    coshh: "",
    dataprotection: "",
    equalityinclusion: "",
    loneworkertraining: "",
    resuscitation: "",
    interpretation: "",
    trainindates2: "",
    mandatory_training: "",
    phoneerror: false,
    phoneerror1: false,
    phoneerror2: false,
    phoneerror3: false,
    form_recruitement_module: 0,
    mandatorytrainingyes: false,
    healthcaretrainingyes: false,
    needanyadjustmentsyes: false,
    livedUKyes: false,
    certificatedocs: [],
    dbsdocs: [],
    passportdocs: [],
    addressdocs: [],
    otherdocs: []
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/employeelogin");
    }
    request({
      url: "/employee/agencydetails",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          worklist: res.response.result[0].locations || {},
          jobtypelist: res.response.result[0].jobtypes || {}
        });
      }
    });
    request({
      url: "/employee/profile",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        const form_data = res.response.result.onlineform ? res.response.result.onlineform : {};
        const form_recruitement_module = res.response.result.onlineform ? res.response.result.onlineform.recruitment_module : {};
        const form_personaldetails = res.response.result.onlineform && res.response.result.onlineform.personaldetails ? res.response.result.onlineform.personaldetails : {};
        const form_driving = res.response.result.onlineform && res.response.result.onlineform.drivingdetails ? res.response.result.onlineform.drivingdetails : {};
        const form_aboutwork = res.response.result.onlineform && res.response.result.onlineform.aboutyourwork ? res.response.result.onlineform.aboutyourwork.abletowork : {};
        const form_kin = res.response.result.onlineform && res.response.result.onlineform.nextofkindetails ? res.response.result.onlineform.nextofkindetails : {};
        const form_gp = res.response.result.onlineform && res.response.result.onlineform.gpdetails ? res.response.result.onlineform.gpdetails : {};
        const form_status = res.response.result.onlineform && res.response.result.onlineform.form_status ? res.response.result.onlineform.form_status : 0;

        if (form_recruitement_module) {
          if (form_data) {
            this.setState({
              form_recruitement_module,
              // activeTab: form_status > 0 ? "5" : "1",
              postapplyfor: form_data.postapplyfor,
              postapplydetails: form_data.postapplydetails,
              availablestartdate: moment(form_data.availablestartdate),
              title: form_personaldetails.title,
              firstname: form_personaldetails.firstname,
              middlename: form_personaldetails.middlename,
              lastname: form_personaldetails.lastname,
              homephone: form_personaldetails.homephone,
              mobileno: form_personaldetails.mobileno || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              email: form_personaldetails.email,
              dateofbirth: moment(form_personaldetails.dateofbirth),
              insuranceno: form_personaldetails.insuranceno,
              streetAddress: form_personaldetails.address && form_personaldetails.address.streetAddress,
              addressline2: form_personaldetails.address && form_personaldetails.address.addressline2,
              city: form_personaldetails.address && form_personaldetails.address.city,
              state: form_personaldetails.address && form_personaldetails.address.state,
              zip: form_personaldetails.address && form_personaldetails.address.zip,
              country: form_personaldetails.address && form_personaldetails.address.country,
              nationality: form_personaldetails.nationality,
              gender: form_personaldetails.gender,
              religion: form_personaldetails.religion,
              race: form_personaldetails.race,
              sexualorientation: form_personaldetails.sexualorientation,
              UploadIDPhoto: form_data.UploadIDPhoto,
              UploadIDName: form_data.UploadIDPhoto && form_data.UploadIDPhoto.substr(24, 30),
              visapermit: form_data.employmenteligibility && form_data.employmenteligibility.visapermit,
              visapermittype: form_data.employmenteligibility && form_data.employmenteligibility.visapermittype,
              drivinglicence: form_driving.drivinglicence,
              traveldetails: form_driving.traveldetails,
              drivinglicenceno: form_driving.drivinglicenceno,
              caraccess: form_driving.caraccess,
              bannedfromdriving: form_driving.bannedfromdriving,
              carinsurance: form_driving.carinsurance,
              UploadCV: form_data.UploadCV && form_data.UploadCV,
              UploadCVName: form_data.UploadCV && form_data.UploadCV.substr(24, 30),
              vehicledocumentsvalid: form_driving.vehicledocumentsvalid,
              englishspoken: form_data.langauages && form_data.langauages.englishspoken,
              englishwritten: form_data.langauages && form_data.langauages.englishwritten,
              otherlanguage: form_data.langauages && form_data.langauages.otherlanguage,
              IELTStest: form_data.langauages && form_data.langauages.IELTStest,
              work_mornings: form_aboutwork.work_mornings,
              work_evenings: form_aboutwork.work_evenings,
              work_afternoons: form_aboutwork.work_afternoons,
              work_occasional: form_aboutwork.work_occasional,
              work_fulltime: form_aboutwork.work_fulltime,
              work_parttime: form_aboutwork.work_parttime,
              work_nights: form_aboutwork.work_nights,
              work_weekends: form_aboutwork.work_weekends,
              work_anytime: form_aboutwork.work_anytime,
              kinprefix: form_kin.kinprefix,
              kinfirstname: form_kin.kinfirstname,
              kinlastname: form_kin.kinlastname,
              kinrelationship: form_kin.kinrelationship,
              kinhomephone: form_kin.kinhomephone,
              kinmobileno: form_kin.kinmobileno || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              kinemail: form_kin.kinemail,
              kinstreetAddress: form_kin.Address && form_kin.Address.kinstreetAddress,
              kinaddressline2: form_kin.Address && form_kin.Address.kinaddressline2,
              kincity: form_kin.Address && form_kin.Address.kincity,
              kinstate: form_kin.Address && form_kin.Address.kinstate,
              kinzip: form_kin.Address && form_kin.Address.kinzip,
              kincountry: form_kin.Address && form_kin.Address.kincountry,
              firstchoice: form_data.aboutyourwork && form_data.aboutyourwork.firstchoice,
              secondchoice: form_data.aboutyourwork && form_data.aboutyourwork.secondchoice,
              thirdchoice: form_data.aboutyourwork && form_data.aboutyourwork.thirdchoice,
              shortnotice: form_data.aboutyourwork && form_data.aboutyourwork.shortnotice,
              reduceworkflexibility: form_data.aboutyourwork && form_data.aboutyourwork.reduceworkflexibility,
              workstate: form_data.aboutyourwork && form_data.aboutyourwork.workstate,
              employmenthistorylist: form_data.employmenthistory,
              educationlist: form_data.education,
              firstrefrelationship: form_data.reference1 && form_data.reference1.firstrefrelationship,
              firstreffirstname: form_data.reference1 && form_data.reference1.firstreffirstname,
              firstreflastname: form_data.reference1 && form_data.reference1.firstreflastname,
              firstrefphone: (form_data.reference1 && form_data.reference1.firstrefphone) || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              firstrefemail: form_data.reference1 && form_data.reference1.firstrefemail,
              firstrefconfirmemail: form_data.reference1 && form_data.reference1.firstrefconfirmemail,
              firstrefstreetAddress: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefstreetAddress,
              firstrefcity: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefcity,
              firstrefstate: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefstate,
              firstrefzip: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefzip,
              secondrefrelationship: form_data.reference2 && form_data.reference2.secondrefrelationship,
              secondreffirstname: form_data.reference2 && form_data.reference2.secondreffirstname,
              secondreflastname: form_data.reference2 && form_data.reference2.secondreflastname,
              secondrefphone: (form_data.reference2 && form_data.reference2.secondrefphone) || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              secondrefemail: form_data.reference2 && form_data.reference2.secondrefemail,
              secondrefconfirmemail: form_data.reference2 && form_data.reference2.secondrefconfirmemail,
              secondrefstreetAddress: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefstreetAddress,
              secondrefcity: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefcity,
              secondrefstate: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefstate,
              secondrefzip: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefzip,
              mandatory_training: form_data.mandatory_training,
              movinghandling: form_data.movinghandling,
              basiclifesupport: form_data.basiclifesupport,
              healthsafety: form_data.healthsafety,
              firesafety: form_data.firesafety,
              firstaid: form_data.firstaid,
              infectioncontrol: form_data.infectioncontrol,
              foodsafety: form_data.foodsafety,
              medicationadmin: form_data.medicationadmin,
              safeguardingvulnerable: form_data.safeguardingvulnerable,
              trainingdates: form_data.trainingdates,
              healthcare_training: form_data.healthcare_training,
              diplomal3: form_data.diplomal3,
              diplomal2: form_data.diplomal2,
              personalsafetymental: form_data.personalsafetymental,
              intermediatelife: form_data.intermediatelife,
              advancedlife: form_data.advancedlife,
              complaintshandling: form_data.complaintshandling,
              handlingviolence: form_data.handlingviolence,
              doolsmental: form_data.doolsmental,
              coshh: form_data.coshh,
              dataprotection: form_data.dataprotection,
              equalityinclusion: form_data.equalityinclusion,
              loneworkertraining: form_data.loneworkertraining,
              resuscitation: form_data.resuscitation,
              interpretation: form_data.interpretation,
              trainindates2: form_data.trainindates2,
              sufferedlongtermillness: form_data.healthdeclaration && form_data.healthdeclaration.sufferedlongtermillness,
              leaveforneckinjury: form_data.healthdeclaration && form_data.healthdeclaration.leaveforneckinjury,
              neckinjuries: form_data.healthdeclaration && form_data.healthdeclaration.neckinjuries,
              sixweeksillness: form_data.healthdeclaration && form_data.healthdeclaration.sixweeksillness,
              communicabledisease: form_data.healthdeclaration && form_data.healthdeclaration.communicabledisease,
              medicalattention: form_data.healthdeclaration && form_data.healthdeclaration.medicalattention,
              healthdeclarationdetails: form_data.healthdeclaration && form_data.healthdeclaration.healthdeclarationdetails,
              registereddisabled: form_data.healthdeclaration && form_data.healthdeclaration.registereddisabled,
              absentfromwork: form_data.healthdeclaration && form_data.healthdeclaration.absentfromwork,
              statereason: form_data.healthdeclaration && form_data.healthdeclaration.statereason,
              GPname: form_gp.GPname,
              GPstreetAddress: form_gp.gpaddress && form_gp.gpaddress.GPstreetAddress,
              GPcity: form_gp.gpaddress && form_gp.gpaddress.GPcity,
              GPstate: form_gp.gpaddress && form_gp.gpaddress.GPstate,
              GPzip: form_gp.gpaddress && form_gp.gpaddress.GPzip,
              contactDoctor: form_gp.gpaddress && form_gp.gpaddress.contactDoctor,
              GPhealthdetails: form_gp.gpaddress && form_gp.gpaddress.GPhealthdetails,
              medicalillness: form_data.medicalhistory && form_data.medicalhistory.medicalillness,
              medicalillnesscaused: form_data.medicalhistory && form_data.medicalhistory.medicalillnesscaused,
              treatment: form_data.medicalhistory && form_data.medicalhistory.treatment,
              needanyadjustments: form_data.medicalhistory && form_data.medicalhistory.needanyadjustments,
              needanyadjustmentsnotes: form_data.medicalhistory && form_data.medicalhistory.needanyadjustmentsnotes,
              livedUK: form_data.tuberculosis && form_data.tuberculosis.livedUK,
              livedUKnotes: form_data.tuberculosis && form_data.tuberculosis.livedUKnotes,
              BCGvaccination: form_data.tuberculosis && form_data.tuberculosis.BCGvaccination,
              BCGvaccinationyes: form_data.tuberculosis && form_data.tuberculosis.BCGvaccination === "yes",
              BCGvaccinationdate: form_data.tuberculosis &&  moment(form_data.tuberculosis.BCGvaccinationdate) ,
              cough: form_data.tuberculosis && form_data.tuberculosis.cough,
              weightloss: form_data.tuberculosis && form_data.tuberculosis.weightloss,
              fever: form_data.tuberculosis && form_data.tuberculosis.fever,
              TB: form_data.tuberculosis && form_data.tuberculosis.TB,
              chickenpox: form_data.chickenpoxorshingles && form_data.chickenpoxorshingles.chickenpox,
              chickenpoxyes: form_data.chickenpoxorshingles && form_data.chickenpoxorshingles.chickenpox === "yes",
              chickenpoxdate: form_data.chickenpoxorshingles && moment(form_data.chickenpoxorshingles.chickenpoxdate),
              triplevaccination: form_data.immunisationhistory && form_data.immunisationhistory.triplevaccination,
              polio: form_data.immunisationhistory && form_data.immunisationhistory.polio,
              tetanus: form_data.immunisationhistory && form_data.immunisationhistory.tetanus,
              hepatitisB: form_data.immunisationhistory && form_data.immunisationhistory.hepatitisB,
              proneProcedures: form_data.immunisationhistory && form_data.immunisationhistory.proneProcedures,
              occupationalHealthServices: form_data.declaration && form_data.declaration.occupationalHealthServices,
              criminaloffence: form_data.declaration && form_data.declaration.criminaloffence,
              warningcriminaloffence: form_data.declaration && form_data.declaration.warningcriminaloffence,
              DBSdetails: form_data.declaration && form_data.declaration.DBSdetails,
              passport: form_data.righttowork && form_data.righttowork.passport,
              workhours: form_data.worktimedirectives && form_data.worktimedirectives.workhours,
              form_status: form_status,
              // uploadcertificate: form_data.uploadcertificate && form_data.uploadcertificate,
              // uploadcertificateName: form_data.uploadcertificate && form_data.uploadcertificate.substr(24, 30),
              // uploadaddress: form_data.uploadaddress && form_data.uploadaddress,
              // uploadaddressName: form_data.uploadaddress && form_data.uploadaddress.substr(24, 30),
              // uploadpassport: form_data.uploadpassport && form_data.uploadpassport,
              // uploadpassportName: form_data.uploadpassport && form_data.uploadpassport.substr(24, 30),
              // uploaddbs: form_data.uploaddbs && form_data.uploaddbs,
              final_verify_status: form_data.final_verify_status,
              // uploaddbsName: form_data.uploaddbs && form_data.uploaddbs.substr(24, 30),
              drivelicenceyes: form_driving.drivinglicence === "yes",
              caraccessyes: form_driving.caraccess === "yes",
              IELTStestyes: form_data.langauages && form_data.langauages.IELTStest === "yes",
              mandatorytrainingyes: form_data.mandatory_training === "yes",
              healthcaretrainingyes: form_data.healthcare_training === "yes",
              needanyadjustmentsyes: form_data.medicalhistory && form_data.medicalhistory.needanyadjustments === "yes",
              livedUKyes: form_data.tuberculosis && form_data.tuberculosis.livedUK === "no",
              certificatedocs: form_data.uploadcertificate && form_data.uploadcertificate.length > 0 ? form_data.uploadcertificate : [],
              addressdocs: form_data.uploadaddress && form_data.uploadaddress.length > 0 ? form_data.uploadaddress : [],
              passportdocs: form_data.uploadpassport && form_data.uploadpassport.length > 0 ? form_data.uploadpassport : [],
              dbsdocs: form_data.uploaddbs && form_data.uploaddbs.length > 0 ? form_data.uploaddbs : [],
              otherdocs: form_data.uploadothers && form_data.uploadothers.length > 0 ? form_data.uploadothers : []
              
            });

          }
        } else {
          return this.props.history.replace("/user/dashboard");
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });

  }
  addmploymenthistory = () => {
    if (this.state.employer === "" || this.state.position === "" || this.state.duties === "" || this.state.start_date === "" || this.state.end_date === "" || this.state.salaryonleaving === "") {
      toast.error("Please fill all fields");
    } else {
      const value = {
        employer: this.state.employer,
        position: this.state.position,
        duties: this.state.duties,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
        salaryonleaving: this.state.salaryonleaving
      };
      this.setState({ employmenthistorylist: this.state.employmenthistorylist.concat([value]), employer: "", position: "", duties: "", start_date: "", end_date: "", salaryonleaving: "" });
    }
  };
  removeemploymenthistory = value => {
    const arr = this.state.employmenthistorylist.filter((list, i) => i !== value);
    this.setState({ employmenthistorylist: arr });
  };

  addeducationlist = () => {
    if (this.state.institution === "" || this.state.year === "" || this.state.course === "" || this.state.grade === "") {
      toast.error("Please fill all fields");
    } else {
      const value = { year: this.state.year, institution: this.state.institution, course: this.state.course, grade: this.state.grade };
      this.setState({
        educationlist: this.state.educationlist.concat([value]),
        institution: "",
        year: "",
        course: "",
        grade: ""
      });
    }
  };
  removeeducationlist = value => {
    const arr = this.state.educationlist.filter((list, i) => i !== value);
    this.setState({ educationlist: arr });
  };
  otheremp = value => {
    if (value === "other") {
      this.setState({
        otheremp: true
      });
    } else {
      this.setState({
        otheremp: false
      });
    }
  };
  drivelicenceyes = yes => {
    if (yes === "yes") {
      this.setState({
        drivelicenceyes: true
      });
    } else {
      this.setState({
        drivelicenceyes: false
      });
    }
  };
  caraccessyes = yes => {
    if (yes === "yes") {
      this.setState({
        caraccessyes: true
      });
    } else {
      this.setState({
        caraccessyes: false
      });
    }
  };
  DBSyes = yes => {
    if (yes === "yes") {
      this.setState({
        DBSyes: true
      });
    } else {
      this.setState({
        DBSyes: false
      });
    }
  };
  IELTStestyes = yes => {
    if (yes === "yes") {
      this.setState({
        IELTStestyes: true
      });
    } else {
      this.setState({
        IELTStestyes: false
      });
    }
  };

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
      window.scrollTo(0, 0);
    }
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleChange = date => {
    this.setState({
      availablestartdate: date
    });
  };
  DBSChange = date => {
    this.setState({
      DBSdate: date
    });
  };
  dateChange = date => {
    this.setState({
      dateofbirth: date
      });
  };

  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          UploadIDPhoto: file_data,
          UploadIDfile: URL.createObjectURL(file_data),
          UploadIDName: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  uploadCV = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          UploadCV: file_data,
          UploadCVfile: URL.createObjectURL(file_data),
          UploadCVName: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  uploadDocs = (evt, place) => {
    evt.persist();
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
      this.files_api(file_data || null, place);
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  deleteDocs = (filepath, filefor) => {
    let toastId = "employee_files_delete_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/employees/apply-online-form/delete-files",
      method: "POST",
      data: { filefor, filepath }
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  files_api(uploadfiles, filefor) {
    let toastId = "employee_files_save_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = new FormData();
    data.append("filefor", filefor);
    data.append("uploadfiles", uploadfiles);
    request({
      url: "/employees/apply-online-form/save-files",
      method: "POST",
      data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  /* uploadCertificate = evt => {
    this.setState({
      uploadcertificate: evt.target.files[0],
      uploadcertificatefile: URL.createObjectURL(evt.target.files[0]),
      uploadcertificateName1: evt.target.files[0].name,
      certificatedocs: evt.target.files[0] ? [evt.target.files[0], ...this.state.certificatedocs] : null
    });
  };
  uploadPassport = evt => {
    this.setState({
      uploadpassport: evt.target.files[0],
      uploadpassportfile: URL.createObjectURL(evt.target.files[0]),
      uploadpassportName1: evt.target.files[0].name,
      passportdocs: evt.target.files[0] ? [evt.target.files[0], ...this.state.passportdocs] : null
    });
  };
  uploadAddress = evt => {
    this.setState({
      uploadaddress: evt.target.files[0],
      uploadaddressfile: URL.createObjectURL(evt.target.files[0]),
      uploadaddressName1: evt.target.files[0].name,
      addressdocs: evt.target.files[0] ? [evt.target.files[0], ...this.state.addressdocs] : null
    });
  };
  uploadDBS = evt => {
    this.setState({
      uploaddbs: evt.target.files[0],
      uploaddbsfile: URL.createObjectURL(evt.target.files[0]),
      uploaddbsName1: evt.target.files[0].name,
      dbsdocs: evt.target.files[0] ? [evt.target.files[0], ...this.state.dbsdocs] : null
    });
  };
  otherDocs = evt => {
    this.setState({
      uploadotherdocs: evt.target.files[0],
      uploadotherdocsfile: URL.createObjectURL(evt.target.files[0]),
      uploadotherdocsName1: evt.target.files[0].name,
      otherdocs: evt.target.files[0] ? [evt.target.files[0], ...this.state.otherdocs] : null
    });
  };*/
  mandatorytraining = yes => {
    if (yes === "yes") {
      this.setState({
        mandatorytrainingyes: true
      });
    } else {
      this.setState({
        mandatorytrainingyes: false
      });
    }
  };
  healthcaretraining = yes => {
    if (yes === "yes") {
      this.setState({
        healthcaretrainingyes: true
      });
    } else {
      this.setState({
        healthcaretrainingyes: false
      });
    }
  };
  needanyadjustments = yes => {
    if (yes === "yes") {
      this.setState({
        needanyadjustmentsyes: true
      });
    } else {
      this.setState({
        needanyadjustmentsyes: false
      });
    }
  };
  livedUK = no => {
    if (no === "no") {
      this.setState({
        livedUKyes: true
      });
    } else {
      this.setState({
        livedUKyes: false
      });
    }
  };
  BCGvaccination = yes => {
    if (yes === "yes") {
      this.setState({
        BCGvaccinationyes: true
      });
    } else {
      this.setState({
        BCGvaccinationyes: false
      });
    }
  };
  chickenpox = yes => {
    if (yes === "yes") {
      this.setState({
        chickenpoxyes: true
      });
    } else {
      this.setState({
        chickenpoxyes: false
      });
    }
  };
  OnFormSubmit = (event, errors, values) => {
    
    if (errors && errors.length === 0) {
      if (this.state.form_status < 5) {
        if (this.state.activeTab === "1") {
          if (this.state.availablestartdate === "" || this.state.dateofbirth === "") {
            toast.error("Select D.O.B / Available to work start date");
          } else {
            const data = new FormData();
            data.append("forpage", 1);
            data.append("postapplyfor", values.postapplyfor);
            data.append("postapplydetails", values.postapplydetails);
            data.append("availablestartdate", this.state.availablestartdate);
            data.append("personaldetails[title]", values.title);
            data.append("personaldetails[firstname]", values.firstname);
            data.append("personaldetails[middlename]", values.middlename);
            data.append("personaldetails[lastname]", values.lastname);
            data.append("personaldetails[homephone]", values.homephone);
            data.append("personaldetails[mobileno][number]", this.state.mobileno.number);
            data.append("personaldetails[mobileno][code]", this.state.mobileno.code);
            data.append("personaldetails[mobileno][dialcountry]", this.state.mobileno.dailcountry);
            data.append("personaldetails[email]", values.email);
            data.append("personaldetails[dateofbirth]", this.state.dateofbirth);
            data.append("personaldetails[insuranceno]", values.insuranceno);
            data.append("personaldetails[address][streetAddress]", values.streetAddress);
            data.append("personaldetails[address][addressline2]", values.addressline2);
            data.append("personaldetails[address][city]", values.city);
            data.append("personaldetails[address][state]", values.state);
            data.append("personaldetails[address][zip]", values.zip);
            data.append("personaldetails[address][country]", values.country);
            data.append("personaldetails[address][label]", values.label);
            data.append("personaldetails[address][title]", values.title);
            data.append("personaldetails[nationality]", values.nationality);
            data.append("personaldetails[gender]", values.gender);
            data.append("personaldetails[religion]", values.religion);
            data.append("personaldetails[race]", values.race);
            data.append("personaldetails[sexualorientation]", values.sexualorientation);
            data.append("UploadIDPhoto", this.state.UploadIDPhoto);
            data.append("employmenteligibility[visapermit]", values.visapermit);
            data.append("employmenteligibility[visapermittype]", values.visapermittype);
            data.append("drivingdetails[drivinglicence]", values.drivinglicence);
            data.append("drivingdetails[traveldetails]", values.traveldetails);
            if (values.drivinglicenceno) {
              data.append("drivingdetails[drivinglicenceno]", values.drivinglicenceno);
            }
            data.append("drivingdetails[caraccess]", values.caraccess);
            data.append("drivingdetails[bannedfromdriving]", values.bannedfromdriving);
            data.append("drivingdetails[carinsurance]", values.carinsurance);
            data.append("drivingdetails[vehicledocumentsvalid]", values.vehicledocumentsvalid);
            data.append("langauages[englishspoken]", values.englishspoken);
            data.append("langauages[englishwritten]", values.englishwritten);
            data.append("langauages[otherlanguage]", values.otherlanguage);
            data.append("langauages[IELTStest]", values.IELTStest);
            data.append("nextofkindetails[kinprefix]", values.kinprefix);
            data.append("nextofkindetails[kinfirstname]", values.kinfirstname);
            data.append("nextofkindetails[kinlastname]", values.kinlastname);
            data.append("nextofkindetails[kinrelationship]", values.kinrelationship);
            data.append("nextofkindetails[kinhomephone]", values.kinhomephone);
            data.append("nextofkindetails[kinmobileno][number]", this.state.kinmobileno.number);
            data.append("nextofkindetails[kinmobileno][code]", this.state.kinmobileno.code);
            data.append("nextofkindetails[kinmobileno][dialcountry]", this.state.kinmobileno.dailcountry);
            data.append("nextofkindetails[kinemail]", values.kinemail);
            data.append("nextofkindetails[Address][kinstreetAddress]", values.kinstreetAddress);
            data.append("nextofkindetails[Address][kinaddressline2]", values.kinaddressline2);
            data.append("nextofkindetails[Address][kincity]", values.kincity);
            data.append("nextofkindetails[Address][kinstate]", values.kinstate);
            data.append("nextofkindetails[Address][kinzip]", values.kinzip);
            data.append("nextofkindetails[Address][kincountry]", values.kincountry);
            data.append("aboutyourwork[abletowork][work_mornings]", values.work_mornings);
            data.append("aboutyourwork[abletowork][work_evenings]", values.work_evenings);
            data.append("aboutyourwork[abletowork][work_afternoons]", values.work_afternoons);
            data.append("aboutyourwork[abletowork][work_occasional]", values.work_occasional);
            data.append("aboutyourwork[abletowork][work_fulltime]", values.work_fulltime);
            data.append("aboutyourwork[abletowork][work_parttime]", values.work_parttime);
            data.append("aboutyourwork[abletowork][work_nights]", values.work_nights);
            data.append("aboutyourwork[abletowork][work_weekends]", values.work_weekends);
            data.append("aboutyourwork[abletowork][work_anytime]", values.work_anytime);
            data.append("aboutyourwork[firstchoice]", values.firstchoice);
            data.append("aboutyourwork[secondchoice]", values.secondchoice);
            data.append("aboutyourwork[thirdchoice]", values.thirdchoice);
            data.append("aboutyourwork[shortnotice]", values.shortnotice);
            data.append("aboutyourwork[reduceworkflexibility]", values.reduceworkflexibility);
            data.append("aboutyourwork[workstate]", values.workstate);
            this.save_online_form(data);
          }
        } else if (this.state.activeTab === "2") {
          if (this.state.educationlist.length === 0 || this.state.employmenthistorylist === 0) {
            toast.error("Please fill all required fields");
          } else {
            const data = new FormData();
            data.append("forpage", 2);
            data.append("UploadCV", this.state.UploadCV);
            data.append("reference1[firstrefrelationship]", values.firstrefrelationship);
            data.append("reference1[firstreffirstname]", values.firstreffirstname);
            data.append("reference1[firstreflastname]", values.firstreflastname);
            data.append("reference1[firstrefphone][number]", this.state.firstrefphone.number);
            data.append("reference1[firstrefphone][code]", this.state.firstrefphone.code);
            data.append("reference1[firstrefphone][dialcountry]", this.state.firstrefphone.dailcountry);
            data.append("reference1[firstrefemail]", values.firstrefemail);
            data.append("reference1[firstrefconfirmemail]", values.firstrefconfirmemail);
            data.append("reference1[Address][firstrefstreetAddress]", values.firstrefstreetAddress);
            data.append("reference1[Address][firstrefcity]", values.firstrefcity);
            data.append("reference1[Address][firstrefstate]", values.firstrefstate);
            data.append("reference1[Address][firstrefzip]", values.firstrefzip);
            data.append("reference2[secondrefrelationship]", values.secondrefrelationship);
            data.append("reference2[secondreffirstname]", values.secondreffirstname);
            data.append("reference2[secondreflastname]", values.secondreflastname);
            data.append("reference2[secondrefphone][number]", this.state.secondrefphone.number);
            data.append("reference2[secondrefphone][code]", this.state.secondrefphone.code);
            data.append("reference2[secondrefphone][dialcountry]", this.state.secondrefphone.dailcountry);
            data.append("reference2[secondrefemail]", values.secondrefemail);
            data.append("reference2[secondrefconfirmemail]", values.secondrefconfirmemail);
            data.append("reference2[Address][secondrefstreetAddress]", values.secondrefstreetAddress);
            data.append("reference2[Address][secondrefcity]", values.secondrefcity);
            data.append("reference2[Address][secondrefstate]", values.secondrefstate);
            data.append("reference2[Address][secondrefzip]", values.secondrefzip);
            this.state.employmenthistorylist.map((item, i) => {
              if (item) {
                data.append(`employmenthistory[${i}][employer]`, item.employer);
                data.append(`employmenthistory[${i}][position]`, item.position);
                data.append(`employmenthistory[${i}][duties]`, item.duties);
                data.append(`employmenthistory[${i}][start_date]`, item.start_date);
                data.append(`employmenthistory[${i}][end_date]`, item.end_date);
                data.append(`employmenthistory[${i}][salaryonleaving]`, item.salaryonleaving);
              }
              return true;
            });
            this.state.educationlist.map((item, i) => {
              if (item) {
                data.append(`education[${i}][institution]`, item.institution);
                data.append(`education[${i}][year]`, item.year);
                data.append(`education[${i}][course]`, item.course);
                data.append(`education[${i}][grade]`, item.grade);
              }
              return true;
            });
            this.save_online_form(data);
          }
        } else if (this.state.activeTab === "3") {
          const data = new FormData();
          data.append("forpage", 3);
          data.append("mandatory_training", values.mandatory_training);
          if (values.mandatory_training === "yes") {
            data.append("movinghandling", values.movinghandling);
            data.append("basiclifesupport", values.basiclifesupport);
            data.append("healthsafety", values.healthsafety);
            data.append("firesafety", values.firesafety);
            data.append("firstaid", values.firstaid);
            data.append("infectioncontrol", values.infectioncontrol);
            data.append("foodsafety", values.foodsafety);
            data.append("medicationadmin", values.medicationadmin);
            data.append("safeguardingvulnerable", values.safeguardingvulnerable);
            data.append("trainingdates", values.trainingdates);
          }
          data.append("healthcare_training", values.healthcare_training);
          if (values.healthcare_training === "yes") {
            data.append("diplomal3", values.diplomal3);
            data.append("diplomal2", values.diplomal2);
            data.append("personalsafetymental", values.personalsafetymental);
            data.append("intermediatelife", values.intermediatelife);
            data.append("advancedlife", values.advancedlife);
            data.append("complaintshandling", values.complaintshandling);
            data.append("handlingviolence", values.handlingviolence);
            data.append("doolsmental", values.doolsmental);
            data.append("coshh", values.coshh);
            data.append("dataprotection", values.dataprotection);
            data.append("equalityinclusion", values.equalityinclusion);
            data.append("loneworkertraining", values.loneworkertraining);
            data.append("resuscitation", values.resuscitation);
            data.append("interpretation", values.interpretation);
            data.append("trainindates2", values.trainindates2);
          }
          this.save_online_form(data);
        } else if (this.state.activeTab === "4") {
          if (this.state.certificatedocs.length === 0 || this.state.passportdocs.length === 0 || this.state.dbsdocs.length === 0 || this.state.addressdocs.length === 0) {
            toast.error("Please Upload Files Required");
          } else {
            this.setState({
              activeTab: (Number(this.state.activeTab) + 1).toString()
            });
            window.scrollTo(0, 0);
            /* const data = new FormData();
            data.append("forpage", 4);
            this.state.certificatedocs.map((item, i) => {
              if (item) {
                data.append(`uploadcertificate[${i}]`, item);
              }
              return true;
            });
            this.state.addressdocs.map((item, i) => {
              if (item) {
                data.append(`uploadaddress[${i}]`, item);
              }
              return true;
            });
            this.state.passportdocs.map((item, i) => {
              if (item) {
                data.append(`uploadpassport[${i}]`, item);
              }
              return true;
            });
            this.state.dbsdocs.map((item, i) => {
              if (item) {
                data.append(`uploaddbs[${i}]`, item);
              }
              return true;
            });
            this.state.otherdocs.map((item, i) => {
              if (item) {
                data.append(`uploadothers[${i}]`, item);
              }
              return true;
            });
            this.save_online_form(data); */
          }
        } else if (this.state.activeTab === "5") {
         
          const data = new FormData();
          data.append("forpage", 5);
          data.append("healthdeclaration[sufferedlongtermillness]", values.sufferedlongtermillness);
          data.append("healthdeclaration[leaveforneckinjury]", values.leaveforneckinjury);
          data.append("healthdeclaration[neckinjuries]", values.neckinjuries);
          data.append("healthdeclaration[sixweeksillness]", values.sixweeksillness);
          data.append("healthdeclaration[communicabledisease]", values.communicabledisease);
          data.append("healthdeclaration[medicalattention]", values.medicalattention);
          data.append("healthdeclaration[healthdeclarationdetails]", values.healthdeclarationdetails);
          data.append("healthdeclaration[registereddisabled]", values.registereddisabled);
          data.append("healthdeclaration[absentfromwork]", values.absentfromwork);
          data.append("healthdeclaration[statereason]", values.statereason);
          data.append("gpdetails[GPname]", values.GPname);
          data.append("gpdetails[gpaddress][GPstreetAddress]", values.GPstreetAddress);
          data.append("gpdetails[gpaddress][GPcity]", values.GPcity);
          data.append("gpdetails[gpaddress][GPstate]", values.GPstate);
          data.append("gpdetails[gpaddress][GPzip]", values.GPzip);
          data.append("gpdetails[gpaddress][contactDoctor]", values.contactDoctor);
          data.append("gpdetails[gpaddress][GPhealthdetails]", values.GPhealthdetails);
          data.append("medicalhistory[medicalillness]", values.medicalillness);
          data.append("medicalhistory[medicalillnesscaused]", values.medicalillnesscaused);
          data.append("medicalhistory[treatment]", values.treatment);
          data.append("medicalhistory[needanyadjustments]", values.needanyadjustments);
          data.append("medicalhistory[needanyadjustmentsnotes]", values.needanyadjustmentsnotes || "");
          data.append("tuberculosis[livedUK]", values.livedUK);
          data.append("tuberculosis[livedUKnotes]", values.livedUKnotes || "");
          data.append("tuberculosis[BCGvaccination]", values.BCGvaccination);
          data.append("tuberculosis[BCGvaccinationdate]", moment(this.state.BCGvaccinationdate) || "");
          data.append("tuberculosis[cough]", values.cough);
          data.append("tuberculosis[weightloss]", values.weightloss);
          data.append("tuberculosis[fever]", values.fever);
          data.append("tuberculosis[TB]", values.TB);
          data.append("chickenpoxorshingles[chickenpox]", values.chickenpox);
          data.append("chickenpoxorshingles[chickenpoxdate]", moment(this.state.chickenpoxdate) || "");
          data.append("immunisationhistory[triplevaccination]", values.triplevaccination);
          data.append("immunisationhistory[polio]", values.polio);
          data.append("immunisationhistory[tetanus]", values.tetanus);
          data.append("immunisationhistory[hepatitisB]", values.hepatitisB);
          data.append("immunisationhistory[proneProcedures]", values.proneProcedures);
          data.append("declaration[occupationalHealthServices]", values.occupationalHealthServices);
          data.append("declaration[criminaloffence]", values.criminaloffence);
          data.append("declaration[warningcriminaloffence]", values.warningcriminaloffence);
          data.append("declaration[DBSdetails]", values.DBSdetails);
          data.append("righttowork[passport]", values.passport);
          data.append("worktimedirectives[workhours]", values.workhours);
          this.save_online_form(data);
        }
      } else {
        toast.error("Already Form Submitted");
      }
    } else {
      toast.error("Please fill all required fields");
    }
  };
  save_online_form(data) {
    let toastId = "employee_save_online_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/employees/apply-online-form",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: "Saved!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
        this.setState({
          activeTab: (Number(this.state.activeTab) + 1).toString()
        });
        window.scrollTo(0, 0);
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  senRequest = () => {
    let toastId = "submit_mail_online_Time_01";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/employees/send-online-form",
      method: "POST",
      data: { form_status: 1 }
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: "Form Sent!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
      }
    });
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      mobileno: {
        number: value,
        code: countryData.dialCode,
        dailcountry: countryData.iso2
      }
    });
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      kinmobileno: {
        number: value,
        code: countryData.dialCode,
        dailcountry: countryData.iso2
      }
    });
  };
  handler2 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror2: false
      });
    } else {
      this.setState({
        phoneerror2: true
      });
    }
    this.setState({
      firstrefphone: {
        number: value,
        code: countryData.dialCode,
        dailcountry: countryData.iso2
      }
    });
  };
  handler3 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror3: false
      });
    } else {
      this.setState({
        phoneerror3: true
      });
    }
    this.setState({
      secondrefphone: {
        number: value,
        code: countryData.dialCode,
        dailcountry: countryData.iso2
      }
    });
  };
  DownloadPDF = () => {
    let toastId = "Down_online_m_Time_01";
    client.defaults.responseType = "blob";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/employees/onlineform/downloadpdf",
      method: "POST"
    })
      .then(res => {
        if (res.status === 0) {
          toast.error(res.response);
        } else {
          if (res) {
            const file = new Blob([res], { type: "application/pdf" });
            const name = `OnlineForm.pdf`;
            FileSaver(file, name);
            toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
          }
          client.defaults.responseType = "json";
        }
      })
      .catch(error => {
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        client.defaults.responseType = "json";
      });
  };

  render() {
    const { jobtypelist, form_status, final_verify_status } = this.state || {};
   
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <div className="right-edit-side online-forms">
          <div className="form__card card form__card--background float new-forms-profile">
            <div className="form__wrap extra">
              <h3 className="task1">Online Application- Complete the application form and submit</h3>
              <CardBody>
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "1"
                      })}
                      onClick={() => {
                        if (this.state.form_status > 0) {
                          this.toggle("1");
                        }
                      }}
                    >
                      Your Personal Details
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "2"
                      })}
                      onClick={() => {
                        if (this.state.form_status > 0) {
                          this.toggle("2");
                        }
                      }}
                    >
                      Employment & Education
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "3"
                      })}
                      onClick={() => {
                        if (this.state.form_status > 0) {
                          this.toggle("3");
                        }
                      }}
                    >
                      Skills & Training
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "4"
                      })}
                      onClick={() => {
                        if (this.state.form_status > 0) {
                          this.toggle("4");
                        }
                      }}
                    >
                      Documents
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "5"
                      })}
                      onClick={() => {
                        if (this.state.form_status > 0) {
                          this.toggle("5");
                        }
                      }}
                    >
                      Declaration & Terms
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "6"
                      })}
                      onClick={() => {
                        if (this.state.form_status > 0) {
                          this.toggle("6");
                        }
                      }}
                    >
                      Details
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                  <TabPane tabId="1">
                    <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField className="form-control fname" type="select" name="postapplyfor" label="Post Applying for:" placeholder="Select Post Apply For" onChange={this.onChange} value={this.state.postapplyfor} required>
                            <option>Please Select</option>
                            {jobtypelist &&
                              jobtypelist.length > 0 &&
                              jobtypelist.map((list, i) => (
                                <option key={i} value={list._id}>
                                  {list.name}
                                </option>
                              ))}
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="select" name="postapplydetails" label="How did you hear about this us?" onChange={this.onChange} value={this.state.postapplydetails} required>
                            <option>Please Select</option>
                            <option value="internetsearch">Internet Search</option>
                            <option value="jobcentre">Job Centre</option>
                            <option value="friend">Friend</option>
                            <option value="newspaper">Newspaper</option>
                            <option value="other">Other</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <label>Date available to start</label>
                          <SingleDatePicker
                            // className={this.state.finaldateError ? "error-color" : null}
                            date={this.state.availablestartdate}
                            onDateChange={availablestartdate => this.setState({ availablestartdate })}
                            focused={this.state.focused1}
                            onFocusChange={({ focused: focused1 }) => this.setState({ focused1 })}
                            id="availablestartdate"
                            displayFormat="DD-MM-YYYY"
                            isOutsideRange={() => false}
                          />
                        </Col>
                      </Row>
                      <br />
                      <h4>Personal Details 1</h4>
                      {/* <strong>Name</strong> */}
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="select" name="title" label="Title" placeholder="Select Title" onChange={this.onChange} value={this.state.title} required>
                            <option>Please Select</option>
                            <option value="mr">Mr.</option>
                            <option value="mrs">Mrs.</option>
                            <option value="miss">Miss.</option>
                            <option value="ms">Ms.</option>
                            <option value="dr">Dr.</option>
                            <option value="prof">Prof.</option>
                            <option value="rev">Rev.</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="First Name" name="firstname" placeholder="Enter First Name" onChange={this.onChange} value={this.state.firstname} required />
                        </Col>
                        {/* <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Middle Name"
                            name="middlename"
                            placeholder="Enter Middle Name"
                            onChange={this.onChange}
                            value={this.state.middlename}
                            required
                          />
                        </Col>*/}
                        <Col xs="12" md="4">
                          <AvField type="text" label="Last Name" name="lastname" placeholder="Enter Last Name" onChange={this.onChange} value={this.state.lastname} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="number" label="Home Phone" name="homephone" placeholder="Enter Phone number" onChange={this.onChange} value={this.state.homephone} />
                        </Col>
                        <Col xs="12" md="4">
                          <Label className={this.state.phoneerror ? "error-color" : null}>Mobile No</Label>
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={this.state.mobileno.dailcountry}
                            utilsScript={libphonenumber}
                            css={this.state.phoneerror ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={this.handler}
                            value={this.state.mobileno.number}
                            required
                          />
                          {this.state.phoneerror ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                          {/* <AvField type="number" label="Mobile No" name="mobileno" placeholder="Enter Mobile Number" onChange={this.onChange} value={this.state.mobileno} required /> */}
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="email" label="Email" name="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.email} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <label>Date Of Birth</label>
                          <SingleDatePicker
                            required={true}
                            id="dateofbirth"
                            date={this.state.dateofbirth}
                            onDateChange={dateofbirth => this.setState({ dateofbirth })}
                            focused={this.state.focused2}
                            onFocusChange={({ focused: focused2 }) => this.setState({ focused2 })}
                            displayFormat="DD-MM-YYYY"
                            isOutsideRange={day => day.isAfter(moment(new Date()))}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="number" label="National Insurance No" name="insuranceno" placeholder="Enter Insurance No" onChange={this.onChange} value={this.state.insuranceno} required />
                        </Col>
                      </Row>
                      <h4>Address</h4>
                      <strong className="sm-heading">Your Current Address</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="streetAddress" placeholder="Enter Street Address" onChange={this.onChange} value={this.state.streetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Address Line 2" name="addressline2" placeholder="Enter Address Line 2" onChange={this.onChange} value={this.state.addressline2} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="city" placeholder="Enter City" onChange={this.onChange} value={this.state.city} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="State / Region" name="state" placeholder="Enter State" onChange={this.onChange} value={this.state.state} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="zip" placeholder="Enter ZIP / Postal Code" onChange={this.onChange} value={this.state.zip} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Country" name="country" placeholder="Enter Country" onChange={this.onChange} value={this.state.country} required />
                        </Col>
                      </Row>
                      <br />
                      <h4>Personal Details 2</h4>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="select" name="nationality" label="Nationality" placeholder="Select Nationality" onChange={this.onChange} value={this.state.nationality} required>
                            <option>Please Select</option>
                            <option value="british">British</option>
                            <option value="eucitizen">EU Citizen</option>
                            <option value="other">Other</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="select" name="gender" label="Gender" placeholder="Select Gender" onChange={this.onChange} value={this.state.gender} required>
                            <option>Please Select</option>
                            <option value="female">Female</option>
                            <option value="male">Male</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="select" name="religion" label="Religion" placeholder="Select Religion.." onChange={this.onChange} value={this.state.religion} required>
                            <option>Please Select</option>
                            <option value="bhudist">Bhudist</option>
                            <option value="christian">Christian</option>
                            <option value="jewish">Jewish</option>
                            <option value="hindu">Hindu</option>
                            <option value="muslim">Muslim</option>
                            <option value="sikh">Sikh</option>
                            <option value="other">Other</option>
                          </AvField>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="select" name="race" label="Race/Ethnicity" placeholder="Select Race/Ethnicity.." onChange={this.onChange} value={this.state.race} required>
                            <option>Please Select</option>
                            <option value="whitebritish">White British</option>
                            <option value="whiteother">White(Other)</option>
                            <option value="whiteirish">White Irish</option>
                            <option value="mixesrace">Mixes Race</option>
                            <option value="indian">Indian</option>
                            <option value="pakistani">Pakistani</option>
                            <option value="bangladeshi">Bangladeshi</option>
                            <option value="otherasian">Other Asian(non-Chinese)</option>
                            <option value="blackcaribbean">Black Caribbean</option>
                            <option value="blackafrican">Black African</option>
                            <option value="blackothers">Black(others)</option>
                            <option value="chinese">Chinese</option>
                            <option value="other">Other</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="select" name="sexualorientation" label="Sexual Orientation" placeholder="Select Sexual Orientation.." onChange={this.onChange} value={this.state.sexualorientation} required>
                            <option>Please Select</option>
                            <option value="bisexual">Bisexual</option>
                            <option value="gayman">Gay Man</option>
                            <option value="gaywoman">Gay Woman/Lesbian</option>
                            <option value="straight">Straight/Heterosexual</option>
                            <option value="prefer">Prefer not to answer</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="4">
                          <Label for="exampleCustomFileBrowser">Upload ID Photo</Label>
                          <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser"
                            name="UploadIDPhoto"
                            onChange={this.fileChangedHandler}
                            label={this.state.UploadIDName ? this.state.UploadIDName : "Upload  Image"}
                            encType="multipart/form-data"
                            accept=".png, .jpg, .jpeg"
                          />
                          <p className="help-block">
                            <small>
                              <i>This Photo will be used on your ID badge, make sure your face is clear</i>
                            </small>
                          </p>
                        </Col>
                        {/* {this.state.UploadIDPhoto?(
                              <Col xs="6" md="3">
                          <img
                            width="180px"
                            height="180px"
                            src={NodeURL + "/" + this.state.UploadIDPhoto}
                            alt="Profile"
                            // onError={() => {
                            //   this.setState({ UploadIDPhoto: "../../img/user-profile.png" });
                            // }}
                          />
                        </Col>):null}*/}
                      </Row>
                      <br />
                      <h4>Employment Eligibility</h4>
                      <Row>
                        <Col xs="12" md="12">
                          <AvRadioGroup inline name="visapermit" label="What visa/permit/status do you currently hold?:" value={this.state.visapermit} required>
                            <AvRadio label="Working Holiday" value="workingholiday" onClick={this.otheremp} />
                            <AvRadio label="Work Permit" value="workpermit" onClick={this.otheremp} />
                            <AvRadio label="Leave to Remain" value="leavetoremain" onClick={this.otheremp} />
                            <AvRadio label="Other" value="other" onClick={() => this.otheremp("other")} />
                          </AvRadioGroup>
                        </Col>
                        {this.state.otheremp ? (
                          <Col xs="12" md="4">
                            <AvField type="text" label="Please state what visa/permit you hold:" name="visapermittype" placeholder="Enter visa/permit.." onChange={this.onChange} value={this.state.visapermittype} required />
                          </Col>
                        ) : null}
                      </Row>
                      <h4>Driving Details</h4>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="drivinglicence" label="Do you have full Driving Licence that allows you to drive in the UK?" value={this.state.drivinglicence} required>
                            <AvRadio label="Yes" value="yes" onClick={() => this.drivelicenceyes("yes")} />
                            <AvRadio label="No" value="no" onClick={this.drivelicenceyes} />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvField type="select" name="traveldetails" label="How would you travel to work if assigned?" value={this.state.traveldetails} required>
                            <option>Please Select</option>
                            <option value="drive">Drive</option>
                            <option value="public">Public Transport </option>
                            <option value="willgetalist">Will get a lift</option>
                            <option value="bicycle">Bicycle</option>
                            <option value="other">Other</option>
                          </AvField>
                        </Col>
                      </Row>
                      {this.state.drivelicenceyes ? (
                        <Row>
                          <Col xs="12" md="4">
                            <AvField type="text" label="Driving Licence No:" name="drivinglicenceno" placeholder="Enter Driving Licence No" onChange={this.onChange} value={this.state.drivinglicenceno} required />
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline name="caraccess" label="Do you have access to a car that you can use for work?" value={this.state.caraccess} required>
                              <AvRadio label="Yes" value="yes" onClick={() => this.caraccessyes("yes")} />
                              <AvRadio label="No" value="no" onClick={this.caraccessyes} />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline name="bannedfromdriving" label="Have you been banned from driving. or do you have any current endorsements on you licence?" value={this.state.bannedfromdriving} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                        </Row>
                      ) : null}
                      {this.state.caraccessyes ? (
                        <Row>
                          <Col xs="12" md="4">
                            <AvRadioGroup
                              inline
                              name="carinsurance"
                              label="Does you car insurance include Class 1 business insurance? (in order to use you vehicle for work you must have class 1 business insurance)"
                              value={this.state.carinsurance}
                              required
                            >
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline name="vehicledocumentsvalid" label={"Are all your vehicle documents up to date and valid?"} value={this.state.vehicledocumentsvalid} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                        </Row>
                      ) : null}
                      <br />
                      <h4>Languages</h4>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="englishspoken" label="English - Spoken" value={this.state.englishspoken} required>
                            <AvRadio label="Fluent" value="fluent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Fair" value="fair" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="englishwritten" label={"English - Written"} value={this.state.englishwritten} required>
                            <AvRadio label="Fluent" value="fluent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Fair" value="fair" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Other Languages Spoken" name="otherlanguage" placeholder="Enter Other Languages Spoken" onChange={this.onChange} value={this.state.otherlanguage} />
                        </Col>
                      </Row>
                      <br />
                      <Row>
                        <Col xs="12" md="12">
                          <AvRadioGroup inline label={"Have you passed each of the academic modules of the IELTS test?"} name="IELTStest" value={this.state.IELTStest} required>
                            <AvRadio label="Yes" value="yes" onClick={() => this.IELTStestyes("yes")} />
                            <AvRadio label="No" value="no" onClick={this.IELTStestyes} />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      {this.state.IELTStestyes ? (
                        <Row>
                          <Col xs="12" md="12">
                            <b>Please note: you have to provide copies of all IELTS certificates held by you</b>
                          </Col>
                        </Row>
                      ) : null}
                      <br />
                      <h4>Next of kin details</h4>
                      <strong className="sm-heading">Name</strong>
                      <Row>
                        <Col xs="12" md="3">
                          <AvField type="select" name="kinprefix" label="Prefix" placeholder="Select Prefix" value={this.state.kinprefix} required>
                            <option>Please Select</option>
                            <option value="mr">Mr.</option>
                            <option value="mrs">Mrs.</option>
                            <option value="miss">Miss.</option>
                            <option value="ms">Ms.</option>
                            <option value="dr">Dr.</option>
                            <option value="prof">Prof.</option>
                            <option value="rev">Rev.</option>
                          </AvField>
                        </Col>
                        <Col xs="12" md="3">
                          <AvField type="text" label="First Name" name="kinfirstname" placeholder="Enter First Name" onChange={this.onChange} value={this.state.kinfirstname} required />
                        </Col>
                        <Col xs="12" md="3">
                          <AvField type="text" label="Last Name" name="kinlastname" placeholder="Enter Last Name" onChange={this.onChange} value={this.state.kinlastname} required />
                        </Col>
                        <Col xs="12" md="3">
                          <AvField type="text" label="Relationship to you:" name="kinrelationship" placeholder="Enter Relationship to you:" onChange={this.onChange} value={this.state.kinrelationship} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="number" label="Home/Work Phone" name="kinhomephone" placeholder="Enter Home/Work Phone" onChange={this.onChange} value={this.state.kinhomephone} />
                        </Col>
                        <Col xs="12" md="4">
                          {/* <AvField type="number" label="Mobile No" name="kinmobileno" placeholder="Enter Mobile Number" onChange={this.onChange} value={this.state.kinmobileno} required /> */}
                          <Label className={this.state.phoneerror1 ? "error-color" : null}>Mobile No</Label>
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={this.state.kinmobileno.dailcountry}
                            utilsScript={libphonenumber}
                            css={this.state.phoneerror1 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={this.handler1}
                            value={this.state.kinmobileno.number}
                            required
                          />
                          {this.state.phoneerror1 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="email" label="Email" name="kinemail" placeholder="Enter Email" onChange={this.onChange} value={this.state.kinemail} required />
                        </Col>
                      </Row>
                      <strong className="sm-heading">Address</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="kinstreetAddress" placeholder="Enter Street Address" onChange={this.onChange} value={this.state.kinstreetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Address Line 2" name="kinaddressline2" placeholder="Enter Address Line 2" onChange={this.onChange} value={this.state.kinaddressline2} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="kincity" placeholder="Enter City" onChange={this.onChange} value={this.state.kincity} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="State / Region" name="kinstate" placeholder="Enter State" onChange={this.onChange} value={this.state.kinstate} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="kinzip" placeholder="Enter ZIP / Postal Code" onChange={this.onChange} value={this.state.kinzip} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Country" name="kincountry" placeholder="Enter Country" onChange={this.onChange} value={this.state.kincountry} required />
                        </Col>
                      </Row>
                      <br />
                      <h4>About Your Work</h4>
                      <label>When are you able to work?</label>
                      <Row>
                        <Col xs="12" md="12" className="checkbox-status">
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_mornings" onChange={this.onChange} checked={this.state.work_mornings} /> Mornings
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_evenings" onChange={this.onChange} checked={this.state.work_evenings} /> Evenings
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_afternoons" onChange={this.onChange} checked={this.state.work_afternoons} /> Afternoons
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_occasional" onChange={this.onChange} checked={this.state.work_occasional} /> Occasional Weeks
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_fulltime" onChange={this.onChange} checked={this.state.work_fulltime} /> Full Time
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_parttime" onChange={this.onChange} checked={this.state.work_parttime} /> Part Time
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_nights" onChange={this.onChange} checked={this.state.work_nights} /> Nights
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_weekends" onChange={this.onChange} checked={this.state.work_weekends} /> Weekends
                          </Col>
                          <Col className="che-box">
                            <AvInput type="checkbox" name="work_anytime" onChange={this.onChange} checked={this.state.work_anytime} /> Anytime
                          </Col>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="1st Choice" name="firstchoice" placeholder="Enter 1st Choice" onChange={this.onChange} value={this.state.firstchoice} />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="2nd Choice" name="secondchoice" placeholder="Enter 2nd Choice" onChange={this.onChange} value={this.state.secondchoice} />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="3rd Choice" name="thirdchoice" placeholder="Enter 3rd Choice" onChange={this.onChange} value={this.state.thirdchoice} />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="shortnotice" label={"Would you be willing to work at short notice?"} value={this.state.shortnotice} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="reduceworkflexibility" label={"Do you have any commitments that reduce your flexibility to work"} value={this.state.reduceworkflexibility} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="12">
                          <AvField type="textarea" label="If yes, please state:" name="workstate" placeholder="Enter State" onChange={this.onChange} value={this.state.workstate} />
                        </Col>
                      </Row>
                      <Row className="d-flex justify-content-end sav-cont">
                        {this.state.form_status > 0 ? (
                          <Button
                            color="info"
                            className="mr-3"
                            type="button"
                            onClick={() => {
                              this.toggle("2");
                            }}
                          >
                            Next
                          </Button>
                        ) : (
                          <Button color="success" className="mr-3" type="submit">
                            Save & Continue
                          </Button>
                        )}
                      </Row>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="2">
                    <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                      <h3>EDUCATION & EMPLOYMENT HISTORY</h3>
                      <h4>Your History</h4>
                      <p>
                        Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps unaccounted for
                        and it covers full work history including your education. Please use extra paper if required. Full work history including your education Dates to and from are shown in a mm/yy format Dates are continual with NO gaps
                        Where there have been gaps in work history please state the reason for the gaps Lists all relevant training undertaken
                      </p>
                      <p>
                        Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps unaccounted for
                        and it covers full work history including your education. Please use extra paper if required.
                      </p>
                      <br />
                      <p>Full work history including your education</p>
                      <p>Dates to and from are shown in a mm/yy format</p>
                      <p>Dates are continual with NO gaps</p>
                      <p>Where there have been gaps in work history please state the reason for the gaps</p>
                      <p>Lists all relevant training undertaken</p>
                      <h4>Employment History</h4>
                      {this.state.form_status > 0 ? null : (
                        <>
                          <Row className="some-select">
                            <Col xs="12" md="2">
                              <AvField type="text" label="Employer" name="employer" placeholder="Enter EMPLOYER" value={this.state.employer} onChange={this.onChange} />
                            </Col>
                            <Col xs="12" md="2">
                              <AvField type="text" label="Position" name="position" placeholder="Enter position" value={this.state.position} onChange={this.onChange} />
                            </Col>
                            <Col xs="12" md="2">
                              <AvField type="text" label="Duties" name="duties" placeholder="Enter DUTIES" value={this.state.duties} onChange={this.onChange} />
                            </Col>
                            <Col xs="12" md="4">
                              <label> Date </label>
                              <DateRangePicker
                                showClearDates={true}
                                startDate={this.state.start_date}
                                startDateId="start_date"
                                endDate={this.state.end_date}
                                endDateId="end_date"
                                onDatesChange={({ startDate, endDate }) => {
                                  this.setState({
                                    start_date: startDate,
                                    end_date: endDate
                                  });
                                }}
                                focusedInput={this.state.focusedInput}
                                onFocusChange={focusedInput => this.setState({ focusedInput })}
                                displayFormat="DD-MM-YYYY"
                                isOutsideRange={() => false}
                              />
                            </Col>
                            <Col xs="12" md="2">
                              <AvField type="number" label="Salary on Leaving" name="salaryonleaving" placeholder="Enter Salary on Leaving" value={this.state.salaryonleaving} onChange={this.onChange} />
                            </Col>
                          </Row>
                          <Row>
                            <Col xs="12" md="12">
                              <Button className="pull-right adding-infos" onClick={this.addmploymenthistory}>
                                Click To Add <i className="fa fa-plus" />
                              </Button>
                            </Col>
                          </Row>
                        </>
                      )}
                      {this.state.employmenthistorylist &&
                        this.state.employmenthistorylist.length > 0 &&
                        this.state.employmenthistorylist.map((list, key) => (
                          <>
                            <Row className="mt-3" key={key}>
                              <Col xs="12" md="2">
                                <p>{list.employer} </p>
                              </Col>
                              <Col xs="12" md="2">
                                <p>{list.position} </p>
                              </Col>
                              <Col xs="12" md="2">
                                <p>{list.duties} </p>
                              </Col>
                              <Col xs="12" md="3">
                                <p> {moment(list.start_date).format("DD-MM-YYYY") + " - " + moment(list.end_date).format("DD-MM-YYYY")} </p>
                              </Col>
                              <Col xs="12" md="2">
                                <p> {list.salaryonleaving}</p>
                              </Col>
                              {this.state.form_status > 0 ? null : (
                                <Col xs="12" md="1">
                                  <Button className="pull-right remove-btns" onClick={() => this.removeemploymenthistory(key)}>
                                    {" "}
                                    <i className="fa fa-remove" />
                                  </Button>
                                </Col>
                              )}
                            </Row>
                          </>
                        ))}
                      <br />
                      <h4>Education</h4>
                      <p className="help-block">Please supply documentary evidence.</p>
                      <p className="h5">Education</p>
                      {this.state.form_status > 0 ? null : (
                        <>
                          <Row>
                            <Col xs="12" md="3">
                              <AvField type="text" label="INSTITUTION" name="institution" placeholder="Enter INSTITUTION" value={this.state.institution} onChange={this.onChange} />
                            </Col>
                            <Col xs="12" md="3">
                              <AvField type="text" label="COURSE" name="course" placeholder="Enter COURSE" value={this.state.course} onChange={this.onChange} />
                            </Col>
                            <Col xs="12" md="3">
                              <AvField type="number" label="YEAR" name="year" placeholder="Enter YEAR" value={this.state.year} onChange={this.onChange} />
                            </Col>
                            <Col xs="12" md="3">
                              <AvField type="text" label="GRADE" name="grade" placeholder="Enter GRADE" value={this.state.grade} onChange={this.onChange} />
                            </Col>
                          </Row>
                          <Row>
                            <Col xs="12" md="12">
                              <Button className="pull-right adding-infos" onClick={this.addeducationlist}>
                                Click To Add <i className="fa fa-plus" />
                              </Button>
                            </Col>
                          </Row>
                        </>
                      )}
                      {this.state.educationlist &&
                        this.state.educationlist.length > 0 &&
                        this.state.educationlist.map((item, key) => (
                          <>
                            <Row className="mt-3" key={key}>
                              <Col xs="12" md="3">
                                <p> {item.institution} </p>
                              </Col>
                              <Col xs="12" md="3">
                                <p> {item.course} </p>
                              </Col>
                              <Col xs="12" md="2">
                                <p> {item.year} </p>
                              </Col>
                              <Col xs="12" md="2">
                                <p> {item.grade} </p>
                              </Col>
                              {this.state.form_status > 0 ? null : (
                                <Col xs="12" md="2">
                                  <Button className="pull-right remove-btns" onClick={() => this.removeeducationlist(key)}>
                                    {" "}
                                    <i className="fa fa-remove" />
                                  </Button>
                                </Col>
                              )}
                            </Row>
                          </>
                        ))}
                      <Row>
                        <Col xs="6" md="3">
                          <Label for="exampleCustomFileBrowser1">Upload CV if you have one</Label>
                          <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser1"
                            name="UploadCV"
                            onChange={this.uploadCV}
                            label={this.state.UploadCVName ? this.state.UploadCVName : "Upload  Image"}
                            accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                            encType="multipart/form-data"
                          />
                        </Col>
                      </Row>
                      <br />
                      <h4>REFERENCES</h4>
                      What visa/permit/status do you currently hold?:
                      <p>
                        Please supply us with two professional referees. One must be from your present or most recent employer and must be a senior grade to yourself and you must have worked for that person for a period of not less than
                        three months duration.
                      </p>
                      <br />
                      <h4>Reference 1</h4>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Relationship" name="firstrefrelationship" placeholder="Enter Relationship " onChange={this.onChange} value={this.state.firstrefrelationship} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="First Name" name="firstreffirstname" placeholder="Enter First Name" onChange={this.onChange} value={this.state.firstreffirstname} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Last Name" name="firstreflastname" placeholder="Enter Last Name" onChange={this.onChange} value={this.state.firstreflastname} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <Label className={this.state.phoneerror2 ? "error-color" : null}>Mobile No</Label>
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={this.state.firstrefphone.dailcountry}
                            utilsScript={libphonenumber}
                            css={this.state.phoneerror2 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={this.handler2}
                            value={this.state.firstrefphone.number}
                            required
                          />
                          {this.state.phoneerror2 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                          {/* <AvField type="number" label="Phone" name="firstrefphone" placeholder="Enter  Phone" onChange={this.onChange} value={this.state.firstrefphone} required /> */}
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="email" label="Email" name="firstrefemail" placeholder="Enter Email" onChange={this.onChange} value={this.state.firstrefemail} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Confirm Email"
                            name="firstrefconfirmemail"
                            placeholder="Enter Email"
                            onChange={this.onChange}
                            value={this.state.firstrefconfirmemail}
                            required
                            validate={{ match: { value: "firstrefemail" } }}
                          />
                        </Col>
                      </Row>
                      <strong className="sm-heading">Address</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="firstrefstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={this.state.firstrefstreetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="firstrefcity" placeholder="Enter City.." onChange={this.onChange} value={this.state.firstrefcity} required />
                        </Col>
                      
                        <Col xs="12" md="4">
                          <AvField type="text" label="State / Region" name="firstrefstate" placeholder="Enter State.." onChange={this.onChange} value={this.state.firstrefstate} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="firstrefzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={this.state.firstrefzip} required />
                        </Col>
                      </Row>
                      <br />
                      <h4>Reference 2</h4>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Relationship" name="secondrefrelationship" placeholder="Enter Relationship .." onChange={this.onChange} value={this.state.secondrefrelationship} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="First Name" name="secondreffirstname" placeholder="Enter First Name.." onChange={this.onChange} value={this.state.secondreffirstname} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Last Name" name="secondreflastname" placeholder="Enter Last Name.." onChange={this.onChange} value={this.state.secondreflastname} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          {/* <AvField type="number" label="Phone" name="secondrefphone" placeholder="Enter  Phone.." onChange={this.onChange} value={this.state.secondrefphone} required /> */}
                          <Label className={this.state.phoneerror3 ? "error-color" : null}>Mobile No</Label>
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={this.state.secondrefphone.dailcountry}
                            utilsScript={libphonenumber}
                            css={this.state.phoneerror3 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={this.handler3}
                            value={this.state.secondrefphone.number}
                            required
                          />
                          {this.state.phoneerror3 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="email" label="Email" name="secondrefemail" placeholder="Enter Email.." onChange={this.onChange} value={this.state.secondrefemail} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Confirm Email"
                            name="secondrefconfirmemail"
                            placeholder="Enter Email.."
                            onChange={this.onChange}
                            value={this.state.secondrefconfirmemail}
                            required
                            validate={{ match: { value: "secondrefemail" } }}
                          />
                        </Col>
                      </Row>
                      <strong className="sm-heading">Address</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="secondrefstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={this.state.secondrefstreetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="secondrefcity" placeholder="Enter City.." onChange={this.onChange} value={this.state.secondrefcity} required />
                        </Col>
                      
                        <Col xs="12" md="4">
                          <AvField type="text" label="State / Region" name="secondrefstate" placeholder="Enter State.." onChange={this.onChange} value={this.state.secondrefstate} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="secondrefzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={this.state.secondrefzip} required />
                        </Col>
                      </Row>
                      <Row className="d-flex justify-content-between prev-next">
                        <Button
                          color="secondary"
                          onClick={() => {
                            this.toggle("1");
                          }}
                          className="ml-3"
                        >
                          Previous{" "}
                        </Button>
                        {this.state.form_status > 0 ? (
                          <Button
                            color="info"
                            className="mr-3"
                            type="button"
                            onClick={() => {
                              this.toggle("3");
                            }}
                          >
                            Next
                          </Button>
                        ) : (
                          <Button color="success" className="mr-3" type="submit">
                            Save & Continue
                          </Button>
                        )}
                      </Row>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="3">
                    <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                      <h4>SKILLS, EXPERIENCE & TRAINING</h4>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you completed mandatory training within the last year"} name="mandatory_training" value={this.state.mandatory_training} required>
                            <AvRadio label="Yes" onClick={() => this.mandatorytraining("yes")} value="yes" />
                            <AvRadio label="No" onClick={this.mandatorytraining} value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      {this.state.mandatorytrainingyes ? (
                        <>
                          <h4>Mandatory Training</h4>
                          <h6>Please tick if you have completed the following training within the last 12 months</h6>
                          <Row>
                            <Col xs="12" md="12" className="checkbox-status ">
                              <Col className="che-box">
                                <AvInput type="checkbox" name="movinghandling" onChange={this.onChange} checked={this.state.movinghandling} value={this.state.movinghandling} /> Moving & Handling
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="basiclifesupport" onChange={this.onChange} checked={this.state.basiclifesupport} value={this.state.basiclifesupport} /> Basic life support
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="healthsafety" onChange={this.onChange} checked={this.state.healthsafety} value={this.state.healthsafety} /> Health and Safety
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="firesafety" onChange={this.onChange} checked={this.state.firesafety} value={this.state.firesafety} /> Fire Safety
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="firstaid" onChange={this.onChange} checked={this.state.firstaid} value={this.state.firstaid} /> First Aid
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="infectioncontrol" onChange={this.onChange} checked={this.state.infectioncontrol} value={this.state.infectioncontrol} /> Infection Control
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="foodsafety" onChange={this.onChange} checked={this.state.foodsafety} value={this.state.foodsafety} /> Food Safety & Nutrition
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="medicationadmin" onChange={this.onChange} checked={this.state.medicationadmin} value={this.state.medicationadmin} /> Medication Administration
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="safeguardingvulnerable" onChange={this.onChange} checked={this.state.safeguardingvulnerable} value={this.state.safeguardingvulnerable} /> Safeguarding Vulnerable Adults &
                                Children
                              </Col>
                            </Col>
                            <Col xs="12" md="12">
                              <AvField type="textarea" label="Training Dates:" name="trainingdates" placeholder="" onChange={this.onChange} value={this.state.trainingdates} required />
                            </Col>
                          </Row>
                        </>
                      ) : null}
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you completed other health care training?"} name="healthcare_training" value={this.state.healthcare_training} required>
                            <AvRadio label="Yes" onClick={() => this.healthcaretraining("yes")} value="yes" />
                            <AvRadio label="No" onClick={this.healthcaretraining} value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>

                      {this.state.healthcaretrainingyes ? (
                        <>
                          <h4>Other Training/Courses & Qualifications</h4>
                          <h6>Please check which training you have completed and the date on the notes (certificates must be provided).</h6>
                          <Row>
                            <Col xs="12" md="12" className="checkbox-status">
                              <Col className="che-box">
                                <AvInput type="checkbox" name="diplomal3" onChange={this.onChange} checked={this.state.diplomal3} value={this.state.diplomal3} /> Diploma in Health & Social Care L3
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="diplomal2" onChange={this.onChange} checked={this.state.diplomal2} value={this.state.diplomal2} /> Diploma in Health & Social Care L2
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="personalsafetymental" onChange={this.onChange} checked={this.state.personalsafetymental} value={this.state.personalsafetymental} /> Personal Safety (Mental Health & Learning
                                Disability)
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="intermediatelife" onChange={this.onChange} checked={this.state.intermediatelife} value={this.state.intermediatelife} /> Intermediate Life Support
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="advancedlife" onChange={this.onChange} checked={this.state.advancedlife} value={this.state.advancedlife} /> Advanced Life Support
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="complaintshandling" onChange={this.onChange} checked={this.state.complaintshandling} value={this.state.complaintshandling} /> Complaints Handling
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="handlingviolence" onChange={this.onChange} checked={this.state.handlingviolence} value={this.state.handlingviolence} /> Handling Violence and Aggression
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="doolsmental" onChange={this.onChange} checked={this.state.doolsmental} value={this.state.doolsmental} /> DOLLS & Mental Capacity
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="coshh" onChange={this.onChange} checked={this.state.coshh} value={this.state.coshh} /> COSHH
                              </Col>{" "}
                              <Col className="che-box">
                                <AvInput type="checkbox" name="dataprotection" onChange={this.onChange} checked={this.state.dataprotection} value={this.state.dataprotection} /> Data Protection
                              </Col>{" "}
                              <Col className="che-box">
                                <AvInput type="checkbox" name="equalityinclusion" onChange={this.onChange} checked={this.state.equalityinclusion} value={this.state.equalityinclusion} /> Equality & Inclusion
                              </Col>{" "}
                              <Col className="che-box">
                                <AvInput type="checkbox" name="loneworkertraining" onChange={this.onChange} checked={this.state.loneworkertraining} value={this.state.loneworkertraining} /> Lone Worker Training
                              </Col>{" "}
                              <Col className="che-box">
                                <AvInput type="checkbox" name="resuscitation" onChange={this.onChange} checked={this.state.resuscitation} value={this.state.resuscitation} /> Resuscitation of the Newborn (Midwifery)
                              </Col>
                              <Col className="che-box">
                                <AvInput type="checkbox" name="interpretation" onChange={this.onChange} checked={this.state.interpretation} value={this.state.interpretation} /> Interpretation of Cardiotocograph Traces (Midwifery)
                              </Col>
                            </Col>
                          </Row>

                          <Row>
                            <Col xs="12" md="12">
                              <AvField type="textarea" label="Training Dates:" name="trainindates2" placeholder="" onChange={this.onChange} value={this.state.trainindates2} required />
                            </Col>
                          </Row>
                        </>
                      ) : null}
                      {/* <br />
                      <h4>APRAISALS</h4>
                      <p>
                        In order to work in the NHS you will need to be appraised annually by a Senior Practitioner of the same discipline, this person will become your “appraiser” Please give details below of the Senior Practitioner who
                        you have made arrangements with to act as your appraiser.
                      </p>
                      <strong>Name of Appraiser</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="First Name" name="appraiserfirstname" placeholder="Enter First Name.." onChange={this.onChange} value={this.state.appraiserfirstname} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Last Name" name="appraiserlastname" placeholder="Enter Last Name.." onChange={this.onChange} value={this.state.appraiserlastname} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Position and Grade of Appraiser:" name="gradeofappraiser" placeholder="Enter Position and Grade of Appraiser: .." onChange={this.onChange} value={this.state.gradeofappraiser} required />
                        </Col>
                      </Row>
                      <strong>Branch Address:</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="appraiserstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={this.state.appraiserstreetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="appraisercity" placeholder="Enter City.." onChange={this.onChange} value={this.state.appraisercity} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="State / Region" name="appraiserstate" placeholder="Enter State.." onChange={this.onChange} value={this.state.appraiserstate} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="appraiserzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={this.state.appraiserzip} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="number" label="Phone" name="appraiserphone" placeholder="Enter  Phone.." onChange={this.onChange} value={this.state.appraiserphone} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Fax" name="appraiserfax" placeholder="Enter  Fax.." onChange={this.onChange} value={this.state.appraiserfax} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="email" label="Email" name="appraiseremail" placeholder="Enter Email.." onChange={this.onChange} value={this.state.appraiseremail} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Confirm Email"
                            name="appraiserconfirmemail"
                            placeholder="Enter Email.."
                            onChange={this.onChange}
                            value={this.state.appraiserconfirmemail}
                            required
                            validate={{ match: { value: "appraiseremail" } }}
                          />
                        </Col>
                      </Row>
                      <h3>YOUR DBS STATUS & UNIFORM</h3>
                      <h4>DBS Status</h4>
                      <p className="help-block">Please send a copy of your most recent DBS Disclosure (formally known as CRB)</p>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="DBS" label={"Do you have a current DBS (Disclosure Barring Service) (formally known as CRB)?"} onChange={this.onChange} value={this.state.DBS} required>
                            <AvRadio label="Yes" value="Yes" onClick={() => this.DBSyes("yes")} />
                            <AvRadio label="No" value="No" onClick={this.DBSyes} />
                          </AvRadioGroup>
                          <p className="help-block">Current DBS Disclosure (formally known as CRB)</p>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Clear"} name="clear" onChange={this.onChange} value={this.state.clear} required>
                            <AvRadio label="Yes" value="Yes" />
                            <AvRadio label="No" value="No" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      {this.state.DBSyes ? (
                        <Row>
                          <Col xs="12" md="4">
                            <p>Date of issue:</p>
                            <DatePicker value={this.state.DBSdate} onChange={this.DBSChange} placeholder="Enter Date of issue.." />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField type="text" label="Disclosure number" name="disclosurenumber" placeholder="Enter  Disclosure number.." onChange={this.onChange} value={this.state.disclosurenumber} required />
                          </Col>
                        </Row>
                      ) : null}
                      <p>
                        All applicants who cannot provide a registered DBS or full immunisation record will be required to complete at their own cost. The company will cover the cost of any Mandatory Training updates however cancellations
                        outside of 48 hours and late attendances will be charged to the candidate.
                      </p>
                      <br />
                      <h4>Uniform</h4>
                      <p>
                        Candidates will be required to purchase uniform if required at the cost of £20 this will be deducted from your timesheet once you have started working through us. Please fill in the box below stating your uniform
                        size and quantity.
                      </p>
                      <br />
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="select" name="tunicsize" label="Your Tunic Size" placeholder="Select Your Tunic Size.." onChange={this.onChange} value={this.state.tunicsize}>
                            <option>Please Select</option>
                            <option>XS</option>
                            <option>S</option>
                            <option>M</option>
                            <option>L</option>
                            <option>XL</option>
                            <option>XXL</option>
                          </AvField>
                        </Col>
                      </Row>*/}
                      <Row className="d-flex justify-content-between prev-next">
                        <Button
                          color="secondary"
                          onClick={() => {
                            this.toggle("2");
                          }}
                          className="ml-3"
                        >
                          Previous{" "}
                        </Button>
                        {this.state.form_status > 0 ? (
                          <Button
                            color="info"
                            className="mr-3"
                            type="button"
                            onClick={() => {
                              this.toggle("4");
                            }}
                          >
                            Next
                          </Button>
                        ) : (
                          <Button color="success" className="mr-3" type="submit">
                            Save & Continue
                          </Button>
                        )}
                      </Row>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="4">
                    <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                      <h4>Upload & Submit Required Documents</h4>
                      <Row className="cer-middle">
                        <Col xs="12" md="6" className="upl-files">
						<div className="upload-forms">
                          <Label for="exampleCustomFileBrowser3">Certificate</Label>
                          {this.state.form_status > 0 ? null : (
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser3"
                              name="uploadcertificate"
                              onChange={(e, id) => this.uploadDocs(e, "certificate")}
                              label={"Upload  Documents"}
                              encType="multipart/form-data"
                              accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                            />
                          )}

                          <Col className="doc-upload">
                            {this.state &&
                              this.state.certificatedocs &&
                              this.state.certificatedocs.length > 0 &&
                              this.state.certificatedocs.map((list, i) => (
                                <p key={i}>
                                  <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                  <span className="documents-name"> {list && list.name} </span>
                                  {this.state.form_status > 0 ? null : (
                                    <span className="documents-del">
                                      <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "certificate")}>
                                        {" "}
                                        <i className="fa fa-trash-o" aria-hidden="true" />
                                      </button>
                                    </span>
                                  )}
                                </p>
                              ))}
                          </Col>
						  </div>
                        </Col>

                        <Col xs="12" md="6" className="upl-files">
						<div className="upload-forms">
                          <Label for="exampleCustomFileBrowser4">Passport</Label>
                          {this.state.form_status > 0 ? null : (
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser4"
                              name="uploadpassport"
                              onChange={(e, id) => this.uploadDocs(e, "passport")}
                              label={"Upload  Documents"}
                              encType="multipart/form-data"
                              accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                            />
                          )}

                          <Col className="doc-upload">
                            {this.state &&
                              this.state.passportdocs &&
                              this.state.passportdocs.length > 0 &&
                              this.state.passportdocs.map((list, i) => (
                                <p key={i}>
                                  <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                  <span className="documents-name">{list && list.name} </span>
                                  {this.state.form_status > 0 ? null : (
                                    <span className="documents-del">
                                      <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "passport")}>
                                        <i className="fa fa-trash-o" aria-hidden="true" />
                                      </button>
                                    </span>
                                  )}
                                </p>
                              ))}
                          </Col>
						  </div>
                        </Col>

                        <Col xs="12" md="6" className="upl-files">
						<div className="upload-forms">
                          <Label for="exampleCustomFileBrowser5">Proof Of Address</Label>
                          {this.state.form_status > 0 ? null : (
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser5"
                              name="uploadaddress"
                              onChange={(e, id) => this.uploadDocs(e, "address")}
                              label={"Upload  Documents"}
                              encType="multipart/form-data"
                              accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                            />
                          )}
                          <Col className="doc-upload">
                            {this.state &&
                              this.state.addressdocs &&
                              this.state.addressdocs.length > 0 &&
                              this.state.addressdocs.map((list, i) => (
                                <p key={i}>
                                  <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                  <span className="documents-name"> {list && list.name} </span>
                                  {this.state.form_status > 0 ? null : (
                                    <span className="documents-del">
                                      <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "address")}>
                                        {" "}
                                        <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                                      </button>{" "}
                                    </span>
                                  )}
                                </p>
                              ))}
                          </Col>
						  </div>
                        </Col>

                        <Col xs="12" md="6" className="upl-files">
						<div className="upload-forms">
                          <Label for="exampleCustomFileBrowser6">DBS Certificate</Label>
                          {this.state.form_status > 0 ? null : (
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser6"
                              name="uploaddbs"
                              onChange={(e, id) => this.uploadDocs(e, "dbs")}
                              label={"Upload  Documents"}
                              encType="multipart/form-data"
                              accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                            />
                          )}
                          <Col className="doc-upload">
                            {this.state &&
                              this.state.dbsdocs &&
                              this.state.dbsdocs.length > 0 &&
                              this.state.dbsdocs.map((list, i) => (
                                <p key={i}>
                                  <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                  <span className="documents-name"> {list && list.name} </span>
                                  {this.state.form_status > 0 ? null : (
                                    <span className="documents-del">
                                      <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "dbs")}>
                                        {" "}
                                        <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                                      </button>{" "}
                                    </span>
                                  )}
                                </p>
                              ))}
                          </Col>
						  </div>
                        </Col>

                        <Col xs="12" md="6" className="upl-files">
						<div className="upload-forms">
                          <Label for="exampleCustomFileBrowser6">Other Documents</Label>
                          {this.state.form_status > 0 ? null : (
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser9"
                              name="uploadotherdocs"
                              onChange={(e, id) => this.uploadDocs(e, "other")}
                              label={"Upload  Documents"}
                              encType="multipart/form-data"
                              accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                            />
                          )}
                          <Col className="doc-upload">
                            {this.state &&
                              this.state.otherdocs &&
                              this.state.otherdocs.length > 0 &&
                              this.state.otherdocs.map((list, i) => (
                                <p key={i}>
                                  <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                  <span className="documents-name">{list && list.name} </span>
                                  {this.state.form_status > 0 ? null : (
                                    <span className="documents-del">
                                      <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "other")}>
                                        {" "}
                                        <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                                      </button>
                                    </span>
                                  )}
                                </p>
                              ))}
                          </Col>
						  </div>
                        </Col>
                      </Row>
                      <br />
                      <br />
                      <br />
                      <br />
                      <Row className="d-flex justify-content-between prev-next">
                        <Button
                          color="secondary"
                          onClick={() => {
                            this.toggle("3");
                          }}
                          className="ml-3"
                        >
                          Previous{" "}
                        </Button>
                        {this.state.form_status > 0 ? (
                          <Button
                            color="info"
                            className="mr-3"
                            type="button"
                            onClick={() => {
                              this.toggle("5");
                            }}
                          >
                            Next
                          </Button>
                        ) : (
                          <>
                            <Button color="success" className="mr-3" type="submit">
                              Save & Continue
                            </Button>
                          </>
                        )}
                      </Row>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="5">
                    <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                      <h4>DECLARATIONS</h4>
                      <h4>Health Declaration</h4>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Do you or have ever suffered from long term illness?:"} name="sufferedlongtermillness" value={this.state.sufferedlongtermillness} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you ever required sick leave for a back or neck injury?:"} name="leaveforneckinjury" value={this.state.leaveforneckinjury} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Do you suffer with any back or neck injuries?:"} name="neckinjuries" value={this.state.neckinjuries} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you been in contact with anyone who is suffering from a contagious illness within the last six weeks?:"} name="sixweeksillness" value={this.state.sixweeksillness} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Do you suffer with a communicable disease?:"} name="communicabledisease" value={this.state.communicabledisease} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Are you currently receiving active medical attention?:"} name="medicalattention" value={this.state.medicalattention} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        {/* <Col xs="12" md="4">
                                    <p>Do you suffer with a communicable disease?:</p>
                                   <AvRadioGroup
                                      name="clear"
                                      onChange={this.onChange}
                                      value={this.state.clear}
                                      required
                                    >
                                      <AvRadio label="Yes" value="Yes" />
                                      <AvRadio label="No" value="No"/>
                                    </AvRadioGroup>
                                    </Col> */}
                        <Col xs="12" md="12">
                          <AvField
                            type="textarea"
                            label="If you have answered ‘yes’ to any of the above, please give details:"
                            name="healthdeclarationdetails"
                            placeholder="Enter  Details"
                            onChange={this.onChange}
                            value={this.state.healthdeclarationdetails}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Are you registered disabled?:"} name="registereddisabled" value={this.state.registereddisabled} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvField
                            type="number"
                            label="How many days have you been absent from work due to illness in the last 12 months?:"
                            name="absentfromwork"
                            placeholder="Enter Field"
                            onChange={this.onChange}
                            value={this.state.absentfromwork}
                            required
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="12">
                          <AvField type="textarea" label="State reason(s) for absence:" name="statereason" placeholder="Enter  State reason(s) for absence:" onChange={this.onChange} value={this.state.statereason} required />
                        </Col>
                      </Row>
                      <strong className="sm-heading">GP Details</strong>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="text" label="GP Name" name="GPname" placeholder="Enter GP Name" onChange={this.onChange} value={this.state.GPname} required />
                        </Col>
                      </Row>
                      <strong className="sm-heading">GP Address:</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="GPstreetAddress" placeholder="Enter Street Address" onChange={this.onChange} value={this.state.GPstreetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="GPcity" placeholder="Enter City" onChange={this.onChange} value={this.state.GPcity} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="State / Region" name="GPstate" placeholder="Enter State" onChange={this.onChange} value={this.state.GPstate} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="GPzip" placeholder="Enter ZIP / Postal Code" onChange={this.onChange} value={this.state.GPzip} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"May we contact your Doctor for health check?:"} name="contactDoctor" value={this.state.contactDoctor} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <p>
                        The above information will be held in strict confidence. If you aware of any health issue that you feel may affect your ability to undertake responsibilities of the post it is your responsibility to inform the Care
                        Manager immediately. Again any details discussed in the meeting will be held in strict confidence.
                      </p>
                      <br />
                      <Row>
                        <Col xs="12" md="1" />
                        <Col xs="12" md="12" className="check-moves">
                          <AvGroup>
                            <AvInput type="checkbox" name="GPhealthdetails" onChange={this.onChange} checked={this.state.GPhealthdetails} /> I understand that my GP may be contacted for details about my health.
                          </AvGroup>
                        </Col>
                      </Row>{" "}
                      <br />
                      <h4>MEDICAL HISTORY</h4>
                      <p className="help-block">All staff groups complete this section</p>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Do you have any illness/impairment/disability (physical or psychological) which may affect your work?"} name="medicalillness" value={this.state.medicalillness} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup
                            inline
                            label={"Have you ever had any illness/impairment/disability which may have been caused or made worse by your work?"}
                            name="medicalillnesscaused"
                            value={this.state.medicalillnesscaused}
                            required
                          >
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup
                            inline
                            label={
                              "Are you having, or waiting for treatment (including medication) or investigations at present? If your answer is yes, please provide further details of\n" +
                              "                              the condition, treatment and dates?"
                            }
                            name="treatment"
                            value={this.state.treatment}
                            required
                          >
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Do you think you may need any adjustments or assistance to help you to do the job?"} name="needanyadjustments" value={this.state.needanyadjustments} required>
                            <AvRadio label="Yes" onClick={() => this.needanyadjustments("yes")} value="yes" />
                            <AvRadio label="No" onClick={this.needanyadjustments} value="no" />
                          </AvRadioGroup>
                        </Col>
                        {this.state.needanyadjustmentsyes ? (
                          <Col xs="12" md="6">
                            <AvField type="textarea" label="If yes, please Provide:" name="needanyadjustmentsnotes" placeholder="Enter Information" onChange={this.onChange} value={this.state.needanyadjustmentsnotes || ""} />
                          </Col>
                        ) : null}
                      </Row>
                      <p className="h5">Tuberculosis</p>
                      <p className="help-block">Clinical diagnosis and management of tuberculosis, and measures for its prevention and control (NICE 2006)</p>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you lived continuously in the UK for the last 5 years"} name="livedUK" value={this.state.livedUK} required>
                            <AvRadio label="Yes" onClick={this.livedUK} value="yes" />
                            <AvRadio label="No" onClick={() => this.livedUK("no")} value="no" />
                          </AvRadioGroup>
                        </Col>
                        {this.state.livedUKyes ? (
                          <Col xs="12" md="4">
                            <AvField
                              type="textarea"
                              label="Please list all the countries that you have lived in over the last 5 years"
                              name="livedUKnotes"
                              placeholder="Enter Information"
                              onChange={this.onChange}
                              value={this.state.livedUKnotes|| ""}
                            />
                          </Col>
                        ) : null}
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you had a BCG vaccination in relation to Tuberculosis?"} name="BCGvaccination" value={this.state.BCGvaccination} required>
                            <AvRadio label="Yes" onClick={() => this.BCGvaccination("yes")} value="yes" />
                            <AvRadio label="No" onClick={this.BCGvaccination} value="no" />
                          </AvRadioGroup>
                        </Col>
                        {this.state.BCGvaccinationyes ? (
                          <Col xs="12" md="4">
                            <label>Please state date</label>
                            <SingleDatePicker
                              id="BCGvaccinationdate"
                              date={moment(this.state.BCGvaccinationdate)}
                              onDateChange={BCGvaccinationdate => this.setState({ BCGvaccinationdate })}
                              focused={this.state.focused85}
                              onFocusChange={({ focused: focused85 }) => this.setState({ focused85 })}
                              displayFormat="DD-MM-YYYY"
                              isOutsideRange={() => false}
                            />
                          </Col>
                        ) : null}
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"A cough which has lasted for more than 3 weeks"} name="cough" value={this.state.cough} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Unexplained weight loss"} name="weightloss" value={this.state.weightloss} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          {" "}
                          <AvRadioGroup inline label={"Unexplained fever"} name="fever" value={this.state.fever} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Have you had tuberculosis (TB) or been in recent contact with open TB"} name="TB" value={this.state.TB} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <br />
                      <h4>Chicken Pox or Shingles</h4>
                      <Row>
                        <Col xs="12" md="12">
                          <AvRadioGroup inline label={"Have you ever had chicken pox or shingles?"} name="chickenpox" value={this.state.chickenpox} required>
                            <AvRadio label="Yes" onClick={() => this.chickenpox("yes")} value="yes" />
                            <AvRadio label="No" onClick={this.chickenpox} value="no" />
                          </AvRadioGroup>
                        </Col>
                        {this.state.chickenpoxyes ? (
                          <Col xs="12" md="4">
                            <label>Please state date</label>
                            <SingleDatePicker
                              id="chickenpoxdate"
                              date={moment(this.state.chickenpoxdate)}
                              onDateChange={chickenpoxdate => this.setState({ chickenpoxdate })}
                              focused={this.state.focused95}
                              onFocusChange={({ focused: focused95 }) => this.setState({ focused95 })}
                              displayFormat="DD-MM-YYYY"
                              isOutsideRange={() => false}
                            />
                          </Col>
                        ) : null}
                      </Row>
                      <p className="h5">Immunisation History</p>
                      <p>
                        The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body for receipt of
                        DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all staff. It is a condition of
                        proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could invalidate your application or lead to the
                        cancellation of your registration with us.
                      </p>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Triple vaccination as a child (Diptheria / Tetanus / Whooping cough)?"} name="triplevaccination" value={this.state.triplevaccination} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Polio"} name="polio" value={this.state.polio} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Tetanus"} name="tetanus" value={this.state.tetanus} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Hepatitis B"} name="hepatitisB" value={this.state.hepatitisB} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <p className="h5">Varicella</p>
                      <p>You must provide a written statement to confirm that you have had chicken pox or shingles however we strongly advise that you provide serology test result showing varicella immunity</p>
                      <p className="h5">Tuberculosis</p>
                      <p>We require an occupational health/GP certificate of a positive scar or a record of a positive skin test result (Do not Self Declare)</p>
                      <p className="h5">Rubella, Measles & Mumps</p>
                      <p>Certificate of “two” MMR vaccinations or proof of a positive antibody for Rubella Measles & Mumps</p>
                      <p className="h5">Hepatitis B</p>
                      <p>You must provide a copy of the most recent pathology report showing titre levels of 100lu/l or above</p>
                      <p className="h5">Hepatitis B Surface Antigen</p>
                      <p>Evidence of a negative Surface Antigen Test Report must be an identified validated sample. (IVS)</p>
                      <p className="h5">Hepatitis C</p>
                      <p>Evidence of a negative antibody test Report must be an identified validated sample. (IVS)</p>
                      <p className="h5">HIV</p>
                      <p>Evidence of a negative antibody test Report must be an identified validated s ample. (IVS)</p>
                      <p className="h5">Exposure Prone Procedures</p>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Will your role involve Exposure Prone Procedures"} name="proneProcedures" value={this.state.proneProcedures} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <br />
                      <h4>Declaration</h4>
                      <AvGroup className="check-moves">
                        <AvInput type="checkbox" name="occupationalHealthServices" onChange={this.onChange} checked={this.state.occupationalHealthServices} /> I declare that the answers to the above questions are true and complete to the
                        best of my knowledge and belief. I also give consent for our appointed Occupational Health Services provider to make recommendations to my employer
                      </AvGroup>
                      <p className="h5">Disclosure Barring Service (DBS)</p>
                      <p>
                        The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body for receipt of
                        DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all staff. It is a condition of
                        proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could invalidate your application or lead to the
                        cancellation of your registration with us
                      </p>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Have you been convicted of a criminal offence?"} name="criminaloffence" value={this.state.criminaloffence} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline label={"Have you ever been cautioned or issued with a formal warning for a criminal offence?"} name="warningcriminaloffence" value={this.state.warningcriminaloffence} required>
                            <AvRadio label="Yes" value="yes" />
                            <AvRadio label="No" value="no" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <AvGroup className="check-moves">
                        <AvInput type="checkbox" name="DBSdetails" onChange={this.onChange} checked={this.state.DBSdetails} /> I confirm the above is true and I understand that a DBS check will be sort in the event of a successful
                        application.
                      </AvGroup>
                      <p className="h5">Rehabilitation of Offenders Act 1974 and Criminal Records</p>
                      <p>
                        By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is
                        concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties. You should there force
                        list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                      </p>
                      <h4>Right To Work</h4>
                      <p>
                        It is a legal requirement that before any offer of work can be made all candidates provide the company with confirmation of their eligibility to work in the UK by providing one of the original documents detailed
                        below.
                      </p>
                      <Row>
                        <Col xs="12" md="12">
                          <AvRadioGroup inline name="passport" value={this.state.passport} required>
                            <AvRadio
                              label="A passport which describes the holder as a British Citizen or as having a right of abode in the United Kingdom or a passport or other travel document to show that the holder has IDENFINITE LEAVE TO REMAIN in the United Kingdom and is not precluded from taking the work in question."
                              value="britishcitizen"
                            />
                            <AvRadio
                              label="A passport or identity card issued by a State which is party to the European Union and EEA Agreement and which describes the holder as a national or a state which is a arty to that agreement."
                              value="state"
                            />
                            <AvRadio
                              label="A letter issued by the Home Office or the Department of Education and Employment indicating that the person named in the letter has permission to take agency work in question or a biometric residence permit."
                              value="homeoffice"
                            />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <br />
                      <h4>Work Time Directives</h4>
                      <p>
                        By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is
                        concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties. You should there force
                        list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                      </p>
                      <Row>
                        <Col xs="12" md="12" className="no-pad">
                          <AvRadioGroup inline name="workhours" value={this.state.workhours} required>
                            <AvRadio label="I DO NOT wish to work more than 48 hours per week" value="no" />
                            <AvRadio label="I DO wish to work more than 48 hours per week" value="yes" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <p className="h5">Registration form Declaration</p>
                      <p>I declare that all information given in this registration form is to the best of my knowledge complete and accurate in all respects and that I am eligible to work in the UK.</p>
                      <Row className="d-flex justify-content-between prev-next">
                        <Button
                          color="secondary"
                          onClick={() => {
                            this.toggle("4");
                          }}
                          className="ml-3"
                        >
                          Previous{" "}
                        </Button>
                        {this.state.form_status > 0 ? (
                          <Button
                            color="info"
                            className="mr-3"
                            type="button"
                            onClick={() => {
                              this.toggle("6");
                            }}
                          >
                            Next
                          </Button>
                        ) : (
                          <Button color="success" className="mr-3" type="submit">
                            Save & Continue
                          </Button>
                        )}
                      </Row>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="6">
                    <Row className="leading-btns ">
                      {this.state.form_status === 0 ? (
                        <Button color="secondary" className="form-sending" onClick={this.senRequest}>
                          Send Form
                        </Button>
                      ) : null}
                      {form_status === 1 ? <Badge color="success">Form Sent</Badge> : null}
                      {form_status === 2 ? <Badge color="success">Application Form Approved</Badge> : null}
                      {form_status === 3 ? <Badge color="success">Interview Scheduled</Badge> : null}
                      {form_status === 4 ? <Badge color="success">Interview Completed</Badge> : null}
                      {form_status === 5 ? <Badge color="success">Verification Process Completed</Badge> : null}
                      {form_status === 6 ? <Badge color="success">Verification Completed</Badge> : null}
                      {form_status === 7 ? <Badge color="success">Reference Form Sent</Badge> : null}
                      {form_status === 8 ? <Badge color="success">Reference Verification Completed</Badge> : null}
                      {form_status === 9 ? <>{final_verify_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</> : null}
                      <div className="cover-btns">
                        {form_status === 1 ? (
                          <Button color="secondary" className="form-resending" onClick={this.senRequest}>
                            Resend Form
                          </Button>
                        ) : null}
                      </div>
                    </Row>
                    <Row className="d-flex justify-content-between prev-next">
                      <Button
                        color="secondary"
                        onClick={() => {
                          this.toggle("5");
                        }}
                        className="ml-3"
                      >
                        Previous{" "}
                      </Button>
                      {this.state.form_status >= 0 && (
                        <Button
                          color="success"
                          onClick={() => {
                            this.DownloadPDF();
                          }}
                          className="mr-3"
                        >
                          <i className="fa fa-download pr-1" />
                          Download Form
                        </Button>
                      )}
                    </Row>
                  </TabPane>
                </TabContent>
              </CardBody>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default onlineform;
