/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import dashbordnodify from "./profile";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import request, { client } from "../../api/api";
import Header from "../common/header";
import Footer from "../common/footer";
import Gettoken from "../../api/gettoken";
import Context from "../../api/context";
import PropTypes from "prop-types";
import jwt_decode from "jwt-decode";

class Login extends Component {
  static contextTypes = {
    router: PropTypes.object
  };
  state = {
    email: "",
    password: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.theme.Authtoken;
    if (token) {
      if (token.username === "employee") {
        return this.props.history.replace("/");
      }
    }
    if (this.props.location.state === true) {
      return toast.success("Registered!"), this.props.history.push({ state: false });
    }
    if (this.props.location.state === "logout") {
      return toast.info("Loged Out!"), this.props.history.push({ state: false });
    }
  }
  flashdashboard = new dashbordnodify();
  OnFormSubmit = e => {
    const loginform = {
      email: this.state.email,
      password: this.state.password
    };
    request({
      url: "/employeelogin",
      method: "POST",
      data: loginform
    })
      .then(res => {
        if (res.status === 1) {
            let tokenEmployee;
            if (res.response.auth_token) {
                tokenEmployee = jwt_decode(res.response.auth_token);
            }
            if (tokenEmployee.subscription === 1) {
                localStorage.setItem("APUSE", res.response.auth_token);
                client.defaults.headers.common["Authorization"] = res.response.auth_token;
                this.loginnodify();
            } else if (tokenEmployee.subscription === 0) {
                toast.error("Agency Subscription Expired, Please Contact Agency.");
            }
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error(err);
      });
  };
  adminsavenodify() {
    return toast.success("Successfully Saved!");
  }
  loginnodify = () => {
      return this.props.history.push({pathname: "/user/dashboard", state: true});
    // return this.props.history.replace("/user/profile"), this.flashdashboard.nodifydashboard();
  };
  render() {
    return (
      <Fragment>
        {/* <ToastContainer position="top-right" autoClose={2500} /> */}
        <Header />
        {/* <!--Form--> */}
        <section className="section section--last section--top-space small-width">
          <div className="container">
            <div className="row" />
            <div className="row form">
              <div className="login-menu-lang">
                <div className="form__card card form__card--background">
                  <div className="col-md-12 right-left login_Credientials">
                    <div className="width_less">
                      <h4>Login</h4>
                      <AvForm onValidSubmit={this.OnFormSubmit}>
                        <AvGroup className="form__form-group">
                          <AvInput
                            className="form__input js-field__last-name"
                            type="text"
                            name="email"
                            placeholder="Enter email or Username.."
                            onChange={this.onChange}
                            value={this.state.email}
                            required
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                        <AvGroup className="form__form-group">
                          <AvInput
                            className="form__input js-field__last-name"
                            type="password"
                            name="password"
                            placeholder="Enter Password.."
                            onChange={this.onChange}
                            value={this.state.password}
                            required
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                        <p className="forget_password">
                          {" "}
                          <Link to="/forgotpassword">Forgot Password</Link>
                        </p>

                        <button className="site-btn site-btn--accent form__submit disable full-width" type="submit" value="Send">
                          Login
                        </button>
                      </AvForm>
                      <p className="reg-again">
                        Don't have an account? Creating one is quick and easy! <Link to="/register">Click Here</Link>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* </section> */}
        {/* <!--Form--> */}
        <Footer />
      </Fragment>
    );
  }
}
export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Login {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
