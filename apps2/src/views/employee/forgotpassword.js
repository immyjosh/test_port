import React, { Component, Fragment } from "react";
import Header from "../common/header";
import Footer from "../common/footer";
import { Link } from "react-router-dom";
import request from "../../../src/api/api";
import { Alert, Row, InputGroupAddon, InputGroupText } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast } from "react-toastify";

class forgotpassword extends Component {
  state = {
    email: "",
    mailalert: false,
    mailerror: false,
    nomail: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    request({
      url: "/employee/forgotpassword",
      method: "POST",
      data: { email: this.state.email }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response);
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  render() {
    return (
      <Fragment>
        <Header />
        {/* <!--Form--> */}
        <section className="section section--last section--top-space small-width">
          <div className="container">
            <div className="row" />
            <div className="row form">
              <div className="login-menu-lang">
                <div className="form__card card form__card--background">
                  <div className="col-12 col-md-12">
                    <h4>Forgot Your Password ? </h4>

                    <AvForm onValidSubmit={this.OnFormSubmit}>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="text" name="email" placeholder="Enter email" onChange={this.onChange} value={this.state.email} required />
                        <AvFeedback>This is invalid!</AvFeedback>
                      </AvGroup>

                      <p className="forget_password">
                        {" "}
                        <Link to="/login">Go to Login</Link>
                      </p>

                      <button className="site-btn site-btn--accent form__submit disable full-width mb-4" type="submit" value="Send">
                        Submit
                      </button>
                    </AvForm>
                  </div>
                  <Row>
                    {this.state.mailalert ? <Alert color="info">Check your email , Follow the instructions and Reset Password!</Alert> : null}
                    {this.state.mailerror ? <Alert color="danger">Please Enter Valid Email Address!</Alert> : null}
                    {this.state.nomail ? <Alert color="danger">Please Enter Valid Email Address!</Alert> : null}
                  </Row>
                </div>
              </div>
            </div>
          </div>
          <img alt="" className="section__img" src="../img/img_backgroud_footer.png" />
        </section>
        {/* <!--Form--> */}

        <Footer />
      </Fragment>
    );
  }
}

export default forgotpassword;
