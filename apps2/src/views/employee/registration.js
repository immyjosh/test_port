import { AvFeedback, AvForm, AvGroup, AvField, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Col, Label, Row, CustomInput } from "reactstrap";
import request from "../../api/api";
// import { SingleDatePicker } from "react-dates";
import Header from "../common/header";
import Footer from "../common/header";
// import classnames from "classnames";
import { timelist } from "../common/utils";
import addnodify from "./login";
import ReCAPTCHA from "react-google-recaptcha";
import { Settings } from "../../api/key";

class Registration extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    email: "",
    role: "",
    code: "",
    job_type: "",
    number: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    lat: "",
    lon: "",
    avatar: "",
    phoneerror: false,
    agencylist: [],
    phonestatus: "",
    timelist: [],
    agency: "",
    locations: "",
    joblist: [],
    locationlist: [],
    employee_rate: "",
    status: 1,
    isverified: 1,
    avatarfile: "",
    recapcha: ""
  };
  flash = new addnodify();
  componentDidMount() {
    this.setState({ timelist });
    request({
      url: "/site/agencies",
      method: "POST"
    }).then(res => {
      this.setState({ agencylist: res.response.result });
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangeCapcha = e => {
    this.setState({ recapcha: e && e });
  };
  selectjoblocation = e => {
    const filteredlist = this.state.agencylist.filter(list => list._id === e.target.value);
    this.setState({
      agency: e.target.value,
      joblist: filteredlist[0].jobtypes,
      locationlist: filteredlist[0].locations
    });
  };

  handleChange = address => {
    this.setState({ address });
    this.setState({ formatted_address: address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: "",
        lat: "",
        lon: ""
      });
    }
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        //this.setState({ lat: results[0].geometry.location.lat });
        //this.setState({ lon: results[0].geometry.location.lon });
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else {
      console.log("this.state.recapcha", this.state.recapcha);
      if (this.state.recapcha === "") {
        toast.error("Capcha Is Required");
      } else {
      const data = new FormData();
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("job_type", this.state.job_type);
      data.append("employee_rate", this.state.employee_rate);
      data.append("agency", this.state.agency);
      data.append("locations", this.state.locations);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      data.append("status", this.state.status);
      data.append("isverified", this.state.isverified);
      request({
        url: "/register/employee",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.setState({ number: "", address: "" });
            this.form && this.form.reset();
            this.props.history.push({ pathname: "/login", state: true });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
      }
    }
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      phonestatus: status
    });
  };

  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarfile: URL.createObjectURL(evt.target.files[0]),
      avatarName: evt.target.files[0].name
    });
  };
  render() {
    return (
      <Fragment>
        <ToastContainer position="top-right" autoClose={2500} />
        <Header />
        {/* <!--Form--> */}
        <section className="section section--last section--top-space">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h3 className="section__title">Take the first step to Agency Portal</h3>
              </div>
            </div>
            <div className="row form register-froms">
              <div className="col-8 col-offset-2">
                <div className="form__card card form__card--background">
                  <div className="form__wrap width_less">
                    <h4>Sign Up</h4>
                    {/* <p>Learn how to sell smarter and faster with Agency Portal.</p> */}
                    <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                      <Row>
                        <Col md="4">
                          <AvField
                            type="select"
                            name="agency"
                            label="Select Agency"
                            placeholder="Select Agency.."
                            onChange={this.selectjoblocation}
                            value={this.state.agency}
                            required
                          >
                            <option>Please Select</option>
                            {this.state.agencylist
                              ? this.state.agencylist.map(list => (
                                  <option key={list._id} value={list._id}>
                                    {list.name}
                                  </option>
                                ))
                              : null}
                          </AvField>
                        </Col>
                        <Col md="4">
                          <AvField
                            type="select"
                            name="locations"
                            label="Select Location"
                            placeholder="Select Location.."
                            onChange={this.onChange}
                            value={this.state.locations}
                            className="form__input js-field__last-name"
                            required
                          >
                            <option>Please select</option>
                            {this.state.locationlist
                              ? this.state.locationlist.map(list => (
                                  <option key={list._id} value={list._id}>
                                    {list.name}
                                  </option>
                                ))
                              : null}
                          </AvField>
                        </Col>
                        <Col md="4">
                          <AvField
                            type="select"
                            name="job_type"
                            label="Select Role"
                            placeholder="Select Jobtype.."
                            onChange={this.onChange}
                            value={this.state.job_type}
                            className="form__input js-field__last-name"
                            required
                          >
                            <option>Please select</option>
                            {this.state.joblist
                              ? this.state.joblist.map(list => (
                                  <option key={list._id} value={list._id}>
                                    {list.name}
                                  </option>
                                ))
                              : null}
                          </AvField>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="6">
                          <AvGroup>
                            <Label className="form__label">Name</Label>
                            <AvInput
                              type="text"
                              name="name"
                              placeholder="Enter Name.."
                              onChange={this.onChange}
                              value={this.state.name}
                              required
                              className="form__input js-field__first-name"
                              autoComplete="name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col md="6">
                          <AvGroup>
                            <Label className="form__label">Phone</Label>
                            <IntlTelInput
                              style={{ width: "100%" }}
                              defaultCountry={"gb"}
                              utilsScript={libphonenumber}
                              css={["intl-tel-input", "form-control"]}
                              onPhoneNumberChange={this.handler}
                              value={this.state.number}
                              className="form__input js-field__last-name"
                            />
                            {this.state.phoneerror ? <small style={{ color: "red" }}> Enter Valid Phone Number!</small> : null}
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Profile Image</Label>
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser"
                              name="avatar"
                              onChange={this.fileChangedHandler}
                              label={this.state.avatarName ? this.state.avatarName : "Upload  Image"}
                              encType="multipart/form-data"
                              className="form__input js-field__last-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        {this.state.avatarfile ? <img className="prof-image img-fluid" width="180px" height="140px" src={this.state.avatarfile} alt="Preview" /> : null}
                        {/* <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              type="text"
                              name="employee_rate"
                              placeholder="Enter Employee Rate.."
                              onChange={this.onChange}
                              value={this.state.employee_rate}
                              required
                              autoComplete="employee_rate"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col> */}
                      </Row>
                      <div className="reglabel">
                        <Label>Search address</Label>
                      </div>
                      <div className="coverwidths">
                        <Row>
                          <Col xs="12" md="12">
                            <PlacesAutocomplete value={this.state.address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                              {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                                <div>
                                  <input
                                    {...getInputProps({
                                      placeholder: "Search Places ...",
                                      className: "form__input js-field__first-name"
                                    })}
                                  />
                                  <div className="autocomplete-dropdown-container">
                                    {suggestions.map(suggestion => {
                                      const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                      // inline style for demonstration purpose
                                      const style = suggestion.active
                                        ? {
                                            backgroundColor: "#fafafa",
                                            cursor: "pointer"
                                          }
                                        : {
                                            backgroundColor: "#ffffff",
                                            cursor: "pointer"
                                          };
                                      return (
                                        <div
                                          {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                          })}
                                        >
                                          <span>{suggestion.description}</span>
                                        </div>
                                      );
                                    })}
                                  </div>
                                </div>
                              )}
                            </PlacesAutocomplete>
                          </Col>
                        </Row>
                      </div>
                      <Row className="mt-2">
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Line1</Label>
                            <AvInput
                              type="text"
                              name="line1"
                              placeholder="Enter Line1.."
                              onChange={this.onChange}
                              value={this.state.line1}
                              required
                              autoComplete="name"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Line2</Label>
                            <AvInput
                              type="text"
                              name="line2"
                              placeholder="Enter Line2.."
                              onChange={this.onChange}
                              value={this.state.line2}
                              required
                              autoComplete="name"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        {/* <Col xs="12" md="4">
                          <AvGroup>
                            <Label>City</Label>
                            <AvInput
                              type="text"
                              name="city"
                              placeholder="Enter City.."
                              onChange={this.onChange}
                              value={this.state.city}
                              required
                              autoComplete="name"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col> */}
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>State</Label>
                            <AvInput
                              type="text"
                              name="state"
                              placeholder="Enter State .."
                              onChange={this.onChange}
                              value={this.state.state}
                              required
                              autoComplete="name"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Country</Label>
                            <AvInput
                              type="text"
                              name="country"
                              placeholder="Enter Country.."
                              onChange={this.onChange}
                              value={this.state.country}
                              required
                              autoComplete="name"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Zipcode</Label>
                            <AvInput
                              type="text"
                              name="zipcode"
                              placeholder="Enter zipcode.."
                              onChange={this.onChange}
                              value={this.state.zipcode}
                              required
                              autoComplete="name"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        {/*<Col xs="12" md="6">
                          <AvGroup>
                            <Label>Client Rate</Label>
                            <AvInput
                              type="text"
                              name="client_rate"
                              placeholder="Enter Client Rate.."
                              onChange={this.onChange}
                              value={this.state.client_rate}
                              required
                              autoComplete="client_rate"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>*/}
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Username</Label>
                            <AvInput
                              type="text"
                              name="username"
                              placeholder="Enter username.."
                              onChange={this.onChange}
                              value={this.state.username}
                              required
                              className="form__input js-field__last-name"
                              autoComplete="name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Email</Label>
                            <AvInput
                              name="email"
                              type="email"
                              placeholder="Enter Email.."
                              onChange={this.onChange}
                              value={this.state.email}
                              autoComplete="email"
                              required
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <Label>Password</Label>
                          <AvInput
                            type="password"
                            name="password"
                            placeholder="Enter password.."
                            onChange={this.onChange}
                            value={this.state.password}
                            required
                            autoComplete="off"
                            className="form__input js-field__first-name"
                          />
                        </Col>
                        <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Confirm Password</Label>
                            <AvInput
                              type="password"
                              name="confirm_password"
                              placeholder="Enter Password.."
                              onChange={this.onChange}
                              value={this.state.confirm_password}
                              required
                              validate={{ match: { value: "password" } }}
                              autoComplete="off"
                              className="form__input js-field__first-name"
                            />
                            <AvFeedback>Match Password!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>

                      <div className="form__form-group form__form-group--read-and-agree">
                        <label className="checkbox-btn">
                          <AvGroup>
                            <Label check for="checkItOut" />
                            <AvInput className=" checkbox-btn__checkbox" type="checkbox" name="checkItOut" required />

                            <span className="checkbox-btn__checkbox-custom mt-3">
                              <i className="mdi mdi-check" />
                            </span>
                            <span className="checkbox-btn__label">
                              I read and agree to
                              <a href="09_privacy.html" target="_blank" className="link link--accent link--accent-bold">
                                {" "}
                                Terms & Conditions
                              </a>
                            </span>
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </label>
                      </div>
                      <div className="form__form-group form__form-group--read-and-agree d-flex justify-content-center">
                        <label className="checkbox-btn">
                           <ReCAPTCHA sitekey={Settings.googleCapcha} onChange={this.onChangeCapcha} />
                        </label>
                      </div>
                      <button
                        type="submit"
                        title="Click here to Register"
                        className="btn btn-primary btn-lg btn-block site-btn--accent form__submit mb-4"
                        onClick={() => {
                          this.phonefield(this.state.number);
                        }}
                      >
                        Register
                      </button>
                    </AvForm>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <img alt="" className="section__img" src="../img/img_backgroud_footer.png" />
        </section>
        {/* <!--Form--> */}

        <Footer />
      </Fragment>
    );
  }
}

export default Registration;
