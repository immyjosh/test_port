import React, { Component, Fragment } from "react";
import { Card, CardBody, CardHeader } from "reactstrap";
import request, { NodeURL } from "../../api/api";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import moment from "moment";

class Yourdetails extends Component {
  state = {};

  componentDidMount() {
    const token = this.props.token_role;
    if (this.props.location.state === true) {
      this.nodifydashboard(token);
    }
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/login");
    }
    request({
      url: "/employee/profile",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        const res_result = res.response.result ? res.response.result : "";
        const form_data = res_result.onlineform ? res_result.onlineform : {};
        const form_status = res_result.onlineform && res_result.onlineform.form_status ? res_result.onlineform.form_status : 0;
        if (form_data) {
          this.setState({
            form_status: form_status,
            form_data: form_data,
            employee_data: res_result,
            employee_email: res_result.email
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  render() {
    const { form_data } = this.state;
    const {
      personaldetails: form_personaldetails,
      drivingdetails: form_driving,
      nextofkindetails: form_kin,
      gpdetails: form_gp,
      postapplyfor,
      postapplydetails,
      availablestartdate,
      UploadIDPhoto,
      employmenteligibility: { visapermit, visapermittype } = {},
      // UploadCV,
      langauages: { englishspoken, englishwritten, otherlanguage, IELTStest } = {},
      employmenthistory: employmenthistorylist,
      education: educationlist,
      reference1,
      reference2,
      mandatory_training,
      movinghandling,
      basiclifesupport,
      healthsafety,
      firesafety,
      firstaid,
      infectioncontrol,
      foodsafety,
      medicationadmin,
      safeguardingvulnerable,
      trainingdates,
      healthcare_training,
      diplomal3,
      diplomal2,
      personalsafetymental,
      intermediatelife,
      advancedlife,
      complaintshandling,
      handlingviolence,
      doolsmental,
      coshh,
      dataprotection,
      equalityinclusion,
      loneworkertraining,
      resuscitation,
      interpretation,
      trainindates2,
      healthdeclaration,
      medicalhistory: { medicalillness, medicalillnesscaused, treatment, needanyadjustments, needanyadjustmentsnotes } = {},
      tuberculosis: { livedUK, livedUKnotes, BCGvaccination, BCGvaccinationdate, cough, weightloss, fever, TB } = {},
      chickenpoxorshingles: { chickenpox, chickenpoxdate } = {},
      immunisationhistory: { triplevaccination, polio, tetanus, hepatitisB, proneProcedures } = {},
      declaration: { criminaloffence, warningcriminaloffence } = {},
      righttowork: { passport } = {},
      worktimedirectives: { workhours } = {},
      // uploadcertificate,
      // uploadpassport,
      // uploadaddress,
      // uploaddbs
    } = form_data ? form_data : {};
    const { abletowork: form_aboutwork, firstchoice, secondchoice, thirdchoice, shortnotice, reduceworkflexibility, workstate } = form_data && form_data.aboutyourwork ? form_data.aboutyourwork : {};
    const {
      title,
      firstname,
      lastname,
      homephone,
      email,
      dateofbirth,
      insuranceno,
      nationality,
      gender,
      religion,
      race,
      sexualorientation,
      address: { streetAddress, addressline2, city, state, zip, country } = {},
      mobileno
    } = form_personaldetails ? form_personaldetails : {};
    const { vehicledocumentsvalid, drivinglicence, traveldetails, drivinglicenceno, caraccess, bannedfromdriving, carinsurance } = form_driving ? form_driving : {};
    const { work_mornings, work_evenings, work_afternoons, work_occasional, work_fulltime, work_parttime, work_nights, work_weekends, work_anytime } = form_aboutwork ? form_aboutwork : {};
    const { kinprefix, kinfirstname, kinlastname, kinrelationship, kinhomephone, kinmobileno, kinemail, Address: { kinstreetAddress, kinaddressline2, kincity, kinstate, kinzip, kincountry } = {} } = form_kin ? form_kin : {};
    const { firstrefrelationship, firstreffirstname, firstreflastname, firstrefphone, firstrefemail, Address: { firstrefstreetAddress, firstrefcity, firstrefstate, firstrefzip } = {} } = reference1 ? reference1 : {};
    const { secondrefrelationship, secondreffirstname, secondreflastname, secondrefphone, secondrefemail, Address: { secondrefstreetAddress, secondrefcity, secondrefstate, secondrefzip } = {} } = reference2 ? reference2 : {};
    const { sufferedlongtermillness, leaveforneckinjury, neckinjuries, sixweeksillness, communicabledisease, medicalattention, healthdeclarationdetails, registereddisabled, absentfromwork, statereason } = healthdeclaration
      ? healthdeclaration
      : {};
    const { GPname, gpaddress: { GPstreetAddress, GPcity, GPstate, GPzip, contactDoctor, GPhealthdetails } = {} } = form_gp ? form_gp : {};
    return (
      <Fragment>
        <div className="right-edit-side">
          <div className="form__card card form__card--background float mobile-earnings">
            <Card className="p-0 mb-0 for-radius">
              <CardHeader className="for-radius">Your Details</CardHeader>
              <CardBody className="p-2">
                <section className="sub-views">
                  <div className="employee-rules">
                    <div className="fl-width">
                      <h2> Your Personal Details </h2>
                      <p>
                        <span className="left-boxes">Post Applying for</span>
                        <span className="right-boxes">
                          {postapplyfor === "care_assistant"
                            ? "Care Assistant"
                            : postapplyfor === "rmn"
                              ? "RMN"
                              : postapplyfor === "rgn"
                                ? "RGN"
                                : postapplyfor === "odps"
                                  ? "ODPS"
                                  : postapplyfor === "social_worker"
                                    ? "Social Worker"
                                    : postapplyfor === "domestic_worker"
                                      ? "Domestic Worker"
                                      : postapplyfor === "other"
                                        ? "Other"
                                        : ""}
                        </span>
                      </p>
                      <p>
                        <span className="left-boxes">How did you hear about this us?</span>
                        <span className="right-boxes">
                          {" "}
                          {postapplydetails === "internetsearch"
                            ? "Internet Search"
                            : postapplydetails === "jobcentre"
                              ? "Job Centre"
                              : postapplydetails === "friend"
                                ? "Friend"
                                : postapplydetails === "newspaper"
                                  ? "Newspaper"
                                  : postapplydetails === "other"
                                    ? "Other"
                                    : ""}
                        </span>
                      </p>
                      <p>
                        <span className="left-boxes">Date available to start</span>
                        <span className="right-boxes">{moment(availablestartdate).format("DD-MM-YYYY")}</span>
                      </p>
                    </div>
                  </div>
                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Personal Details 1</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Title</span>
                          <span className="right-boxes">
                            {title === "mr" ? "Mr." : title === "mrs" ? "Mrs." : title === "miss" ? "Miss." : title === "ms" ? "Ms." : title === "dr" ? "Dr." : title === "prof" ? "Prof." : title === "rev" ? "Rev." : ""}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">First Name</span>
                          <span className="right-boxes">{firstname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Last Name</span>
                          <span className="right-boxes">{lastname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Home Phone</span>
                          <span className="right-boxes">{homephone}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Mobile No</span>
                          <span className="right-boxes">
                            {mobileno && mobileno.code}-{mobileno && mobileno.number}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">Email</span>
                          <span className="right-boxes">{email}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Date Of Birth</span>
                          <span className="right-boxes">{moment(dateofbirth).format("DD-MM-YYYY")}</span>
                        </p>

                        <p>
                          <span className="left-boxes">National Insurance No</span>
                          <span className="right-boxes">{insuranceno}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Address</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Street Address</span>
                          <span className="right-boxes">{streetAddress}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Address Line 2</span>
                          <span className="right-boxes">{addressline2}</span>
                        </p>

                        <p>
                          <span className="left-boxes">City</span>
                          <span className="right-boxes">{city}</span>
                        </p>

                        <p>
                          <span className="left-boxes">County / State / Region</span>
                          <span className="right-boxes">{state}</span>
                        </p>

                        <p>
                          <span className="left-boxes">ZIP / Postal Code</span>
                          <span className="right-boxes">{zip}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Country</span>
                          <span className="right-boxes">{country}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Personal Details 2</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Nationality</span>
                          <span className="right-boxes">{nationality === "british" ? "British" : nationality === "eucitizen" ? "EU Citizen" : nationality === "other" ? "Other" : ""}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Gender</span>
                          <span className="right-boxes">{gender === "female" ? "Female" : gender === "male" ? "Male" : ""}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Religion</span>
                          <span className="right-boxes">
                            {religion === "bhudist"
                              ? "Bhudist"
                              : religion === "christian"
                                ? "Christian"
                                : religion === "jewish"
                                  ? "Jewish"
                                  : religion === "hindu"
                                    ? "Hindu"
                                    : religion === "muslim"
                                      ? "Muslim"
                                      : religion === "sikh"
                                        ? "Sikh"
                                        : religion === "other"
                                          ? "Other"
                                          : ""}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">Race/Ethnicity</span>
                          <span className="right-boxes">
                            {race === "whitebritish"
                              ? "White British"
                              : race === "whiteother"
                                ? "White(Other)"
                                : race === "whiteirish"
                                  ? "White Irish"
                                  : race === "mixesrace"
                                    ? "Mixes Race"
                                    : race === "indian"
                                      ? "Indian"
                                      : race === "pakistani"
                                        ? "Pakistani"
                                        : race === "bangladeshi"
                                          ? "Bangladeshi"
                                          : race === "otherasian"
                                            ? "Other Asian(non-Chinese)"
                                            : race === "blackcaribbean"
                                              ? "Black Caribbean"
                                              : race === "blackafrican"
                                                ? "Black African"
                                                : race === "blackothers"
                                                  ? "Black(others)"
                                                  : race === "chinese"
                                                    ? "Chinese"
                                                    : race === "other"
                                                      ? "Other"
                                                      : ""}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">Sexual Orientation</span>
                          <span className="right-boxes">
                            {sexualorientation === "bisexual"
                              ? "Bisexual"
                              : sexualorientation === "gayman"
                                ? "Gay Man"
                                : sexualorientation === "gaywoman"
                                  ? "Gay Woman/Lesbian"
                                  : sexualorientation === "straight"
                                    ? "Straight/Heterosexual"
                                    : sexualorientation === "prefer"
                                      ? "Prefer not to answer"
                                      : ""}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">ID Photo</span>
                          <span className="right-boxes">
                            {" "}
                            <img width="180px" height="180px" src={NodeURL + "/" + UploadIDPhoto} alt="Profile" />
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Employment Eligibility</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">What visa/permit/status do you currently hold?</span>
                          <span className="right-boxes">
                            {" "}
                            {visapermit === "workingholiday" ? "Working Holiday" : visapermit === "workpermit" ? "Work Permit" : visapermit === "leavetoremain" ? "Leave to Remain" : visapermit === "other" ? "Other" : ""}
                          </span>
                        </p>
                        <p>
                          <span className="left-boxes"> Visa Type</span>
                          <span className="right-boxes"> {visapermittype}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Driving Details</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Do you have full Driving Licence that allows you to drive in the UK?</span>
                          <span className="right-boxes">{drivinglicence === "yes" ? "Yes" : "No"}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Do you have access to a car that you can use for work?</span>
                          <span className="right-boxes">{caraccess}</span>
                        </p>{" "}
                        
                        <p>
                          <span className="left-boxes">Does you car insurance include Class 1 business insurance? (in order to use you vehicle for work you must have class 1 business insurance)</span>
                          <span className="right-boxes">{carinsurance && carinsurance.toUpperCase()}</span>
                        </p>
						<p>
                          <span className="left-boxes">Driving Licence No:</span>
                          <span className="right-boxes">{drivinglicenceno}</span>
                        </p>
                       
                        <p>
                          <span className="left-boxes">Are all your vehicle documents up to date and valid?</span>
                          <span className="right-boxes">{vehicledocumentsvalid && vehicledocumentsvalid.toUpperCase()}</span>
                        </p>
						 <p>
                          <span className="left-boxes">Have you been banned from driving. or do you have any current endorsements on you licence?</span>
                          <span className="right-boxes">{bannedfromdriving && bannedfromdriving.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">How would you travel to work if assigned?</span>
                          <span className="right-boxes">
                            {traveldetails === "drive"
                              ? "Drive"
                              : traveldetails === "public"
                                ? "Public Transport"
                                : traveldetails === "willgetalist"
                                  ? "Will get a lift"
                                  : traveldetails === "bicycle"
                                    ? "Bicycle"
                                    : traveldetails === "other"
                                      ? "Other"
                                      : ""}
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Languages</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">English - Spoken</span>
                          <span className="right-boxes">{englishspoken === "fluent" ? "Fluent" : englishspoken === "good" ? "Good" : englishspoken === "fair" ? "Fair" : ""}</span>
                        </p>

                        <p>
                          <span className="left-boxes">English - Written</span>
                          <span className="right-boxes">{englishwritten === "fluent" ? "Fluent" : englishwritten === "good" ? "Good" : englishwritten === "fair" ? "Fair" : ""}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Other Languages Spoken</span>
                          <span className="right-boxes">{otherlanguage}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Have you passed each of the academic modules of the IELTS test?</span>
                          <span className="right-boxes">{IELTStest === "yes" ? "Yes" : "No"}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Next of kin details</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Prefix</span>
                          <span className="right-boxes">
                            {" "}
                            {kinprefix === "mr"
                              ? "Mr."
                              : kinprefix === "mrs"
                                ? "Mrs."
                                : kinprefix === "miss"
                                  ? "Miss."
                                  : kinprefix === "ms"
                                    ? "Ms."
                                    : kinprefix === "dr"
                                      ? "Dr."
                                      : kinprefix === "prof"
                                        ? "Prof."
                                        : kinprefix === "rev"
                                          ? "Rev."
                                          : ""}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">First Name</span>
                          <span className="right-boxes">{kinfirstname}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Last Name</span>
                          <span className="right-boxes">{kinlastname}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Relationship to you</span>
                          <span className="right-boxes">{kinrelationship}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Home/Work Phone</span>
                          <span className="right-boxes">{kinhomephone}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Mobile No</span>
                          <span className="right-boxes">
                            {kinmobileno && kinmobileno.code} - {kinmobileno && kinmobileno.number}
                          </span>
                        </p>
                        <p>
                          <span className="left-boxes">Email</span>
                          <span className="right-boxes">{kinemail}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Street Address</span>
                          <span className="right-boxes">{kinstreetAddress}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Address Line 2</span>
                          <span className="right-boxes">{kinaddressline2}</span>
                        </p>
                        <p>
                          <span className="left-boxes">City</span>
                          <span className="right-boxes">{kincity}</span>
                        </p>
                        <p>
                          <span className="left-boxes">County / State / Region</span>
                          <span className="right-boxes">{kinstate}</span>
                        </p>
                        <p>
                          <span className="left-boxes">ZIP / Postal Code</span>
                          <span className="right-boxes">{kinzip}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Country</span>
                          <span className="right-boxes">{kincountry}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>About Your Work</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">When are you able to work?</span>
                          <span className="right-boxes">
                            {work_mornings ? "Mornings ," : ""}
                            {work_evenings ? "Evenings ," : ""}
                            {work_afternoons ? "Afternoons ," : ""}
                            {work_occasional ? "Occasional Weeks ," : ""}
                            {work_fulltime ? "Full Time ," : ""}
                            {work_parttime ? "Part Time ," : ""}
                            {work_nights ? "Nights ," : ""}
                            {work_weekends ? "Weekends ," : ""}
                            {work_anytime ? "Anytime ," : ""}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">1st Choice</span>
                          <span className="right-boxes">{firstchoice}</span>
                        </p>
                        <p>
                          <span className="left-boxes">2nd Choice</span>
                          <span className="right-boxes">{secondchoice}</span>
                        </p>
                        <p>
                          <span className="left-boxes">3rd Choice</span>
                          <span className="right-boxes">{thirdchoice}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Would you be willing to work at short notice?</span>
                          <span className="right-boxes">{shortnotice === "yes" ? "Yes" : shortnotice === "no" ? "No" : ""}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Do you have any commitments that reduce your flexibility to work</span>
                          <span className="right-boxes">{reduceworkflexibility === "yes" ? "Yes" : reduceworkflexibility === "no" ? "No" : ""}</span>
                        </p>
                        <p>
                          <span className="left-boxes">If yes, please state</span>
                          <span className="right-boxes">{workstate}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="employee-rules">
                    <div className="hist-spaces">
                      <h2> Employment and Education</h2>
                      <div className="para-design">
                        <h4>Your History</h4>
                        <p>
                          Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps unaccounted for
                          and it covers full work history including your education. Please use extra paper if required. Full work history including your education Dates to and from are shown in a mm/yy format Dates are continual with NO
                          gaps Where there have been gaps in work history please state the reason for the gaps Lists all relevant training undertaken Please ensure you complete this section even if you have a CV. The NHS states that
                          “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps unaccounted for and it covers full work history including your education. Please use extra paper
                          if required.
                        </p>

                        <br />
                        <br />
                        <p>Full work history including your education</p>
                        <p>Dates to and from are shown in a mm/yy format</p>
                        <p>Dates are continual with NO gaps</p>
                        <p>Where there have been gaps in work history please state the reason for the gaps</p>
                        <p>Lists all relevant training undertaken</p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Employment History</h4>
                      {employmenthistorylist &&
                        employmenthistorylist.map((list, i) => (
                          <div className="box-way" key={i}>
                            <p>
                              <span className="left-boxes">Employer</span>
                              <span className="right-boxes">{list.employer}</span>
                            </p>

                            <p>
                              <span className="left-boxes">Position</span>
                              <span className="right-boxes">{list.position}</span>
                            </p>
                            <p>
                              <span className="left-boxes">Duties</span>
                              <span className="right-boxes">{list.duties}</span>
                            </p>
                            <p>
                              <span className="left-boxes">Start Date</span>
                              <span className="right-boxes">{moment(list.start_date).format("DD-MM-YYYY")}</span>
                            </p>
                            <p>
                              <span className="left-boxes">End Date</span>
                              <span className="right-boxes">{moment(list.end_date).format("DD-MM-YYYY")}</span>
                            </p>
                            <p>
                              <span className="left-boxes">Salary on Leaving</span>
                              <span className="right-boxes">{list.salaryonleaving}</span>
                            </p>
                          </div>
                        ))}
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Education</h4>
                      {educationlist &&
                        educationlist.map((list, i) => (
                          <div className="box-way" key={i}>
                            <p>
                              <span className="left-boxes">Institution</span>
                              <span className="right-boxes">{list.institution}</span>
                            </p>

                            <p>
                              <span className="left-boxes">Course</span>
                              <span className="right-boxes">{list.course}</span>
                            </p>
                            <p>
                              <span className="left-boxes">Year</span>
                              <span className="right-boxes">{list.year}</span>
                            </p>
                            <p>
                              <span className="left-boxes">Grade</span>
                              <span className="right-boxes">{list.grade}</span>
                            </p>
                            {/* <p>
                          <span className="left-boxes">Upload CV</span>
                          <span className="right-boxes">Yes</span>
                        </p>*/}
                          </div>
                        ))}
                    </div>
                  </div>

                  <div className="employee-rules">
                    <div className="hist-spaces">
                      <div className="para-design">
                        <h4>Reference</h4>
                        <h6>What visa/permit/status do you currently hold?</h6>
                        <p>
                          Please supply us with two professional referees. One must be from your present or most recent employer and must be a senior grade to yourself and you must have worked for that person for a period of not less than
                          three months duration.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Reference 1</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Relationship</span>
                          <span className="right-boxes">{firstrefrelationship}</span>
                        </p>

                        <p>
                          <span className="left-boxes">First Name</span>
                          <span className="right-boxes">{firstreffirstname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Last Name</span>
                          <span className="right-boxes">{firstreflastname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Mobile No</span>
                          <span className="right-boxes">
                            {firstrefphone && firstrefphone.code} - {firstrefphone && firstrefphone.number}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">Email</span>
                          <span className="right-boxes">{firstrefemail}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Street Address</span>
                          <span className="right-boxes">{firstrefstreetAddress}</span>
                        </p>

                        <p>
                          <span className="left-boxes">City</span>
                          <span className="right-boxes">{firstrefcity}</span>
                        </p>

                        <p>
                          <span className="left-boxes">County / State / Region</span>
                          <span className="right-boxes">{firstrefstate}</span>
                        </p>

                        <p>
                          <span className="left-boxes">ZIP / Postal Code</span>
                          <span className="right-boxes">{firstrefzip}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Reference 2</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Relationship</span>
                          <span className="right-boxes">{secondrefrelationship}</span>
                        </p>

                        <p>
                          <span className="left-boxes">First Name</span>
                          <span className="right-boxes">{secondreffirstname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Last Name</span>
                          <span className="right-boxes">{secondreflastname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Mobile No</span>
                          <span className="right-boxes">
                            {secondrefphone && secondrefphone.code} - {secondrefphone && secondrefphone.number}
                          </span>
                        </p>

                        <p>
                          <span className="left-boxes">Email</span>
                          <span className="right-boxes">{secondrefemail}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Street Address</span>
                          <span className="right-boxes">{secondrefstreetAddress}</span>
                        </p>

                        <p>
                          <span className="left-boxes">City</span>
                          <span className="right-boxes">{secondrefcity}</span>
                        </p>

                        <p>
                          <span className="left-boxes">County / State / Region</span>
                          <span className="right-boxes">{secondrefstate}</span>
                        </p>

                        <p>
                          <span className="left-boxes">ZIP / Postal Code</span>
                          <span className="right-boxes">{secondrefzip}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="employee-rules">
                    <div className="hist-spaces">
                      <h2> Skills and Training</h2>
                    </div>
                  </div>
                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Skills, Experience and Training</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Have you completed mandatory training within the last year</span>
                          <span className="right-boxes">{mandatory_training && mandatory_training.toUpperCase()}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Other Training/Courses & Qualifications</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Please check which training you have completed and the date on the notes (certificates must be provided).</span>
                          {movinghandling ? <span className="right-views">Moving & Handling</span> : null}
                          {basiclifesupport ? <span className="right-views">Basic life support</span> : null}
                          {healthsafety ? <span className="right-views">Health and Safety</span> : null}
                          {firesafety ? <span className="right-views">Fire Safety</span> : null}
                          {firstaid ? <span className="right-views">First Aid</span> : null}
                          {infectioncontrol ? <span className="right-views">Infection Control</span> : null}
                          {foodsafety ? <span className="right-views">Food Safety & Nutrition</span> : null}
                          {medicationadmin ? <span className="right-views">Medication Administration</span> : null}
                          {safeguardingvulnerable ? <span className="right-views">Safeguarding Vulnerable Adults & Children</span> : null}
                        </p>

                        <p>
                          <span className="left-boxes">Training Dates</span>
                          <span className="right-boxes">{trainingdates}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Skills, Experience and Training</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Have you completed other health care training?</span>
                          <span className="right-boxes">{healthcare_training && healthcare_training.toUpperCase()}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Other Training/Courses & Qualifications</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Please check which training you have completed and the date on the notes (certificates must be provided).</span>
                          {diplomal3 ? <span className="right-views">Diploma in Health & Social Care L3</span> : null}
                          {diplomal2 ? <span className="right-views">Diploma in Health & Social Care L2</span> : null}
                          {personalsafetymental ? <span className="right-views">Personal Safety (Mental Health & Learning Disability)</span> : null}
                          {intermediatelife ? <span className="right-views">Intermediate Life Support</span> : null}
                          {advancedlife ? <span className="right-views">Advanced Life Support</span> : null}
                          {complaintshandling ? <span className="right-views">Complaints Handling</span> : null}
                          {handlingviolence ? <span className="right-views">Handling Violence and Aggression</span> : null}
                          {doolsmental ? <span className="right-views">DOLLS & Mental Capacity</span> : null}
                          {coshh ? <span className="right-views">COSHH</span> : null}
                          {dataprotection ? <span className="right-views">Data Protection</span> : null}
                          {equalityinclusion ? <span className="right-views">Equality & Inclusion</span> : null}
                          {loneworkertraining ? <span className="right-views">Lone Worker Training</span> : null}
                          {resuscitation ? <span className="right-views">Resuscitation of the Newborn (Midwifery)</span> : null}
                          {interpretation ? <span className="right-views">Interpretation of Cardiotocograph Traces (Midwifery)</span> : null}
                        </p>

                        <p>
                          <span className="left-boxes">Training Dates</span>
                          <span className="right-boxes">{trainindates2}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="employee-rules">
                    <div className="hist-spaces">
                      <h2> Documents </h2>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Upload & Submit Required Documents</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Certificate</span>
                          <span className="right-boxes">Test</span>
                        </p>

                        <p>
                          <span className="left-boxes">Passport</span>
                          <span className="right-boxes">Test</span>
                        </p>

                        <p>
                          <span className="left-boxes">Proof Of Address</span>
                          <span className="right-boxes">Test</span>
                        </p>

                        <p>
                          <span className="left-boxes">DBS Certificate</span>
                          <span className="right-boxes">Test</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="employee-rules">
                    <div className="hist-spaces">
                      <h2> Declaration and Terms </h2>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Health Declaration</h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Do you or have ever suffered from long term illness?</span>
                          <span className="right-boxes">{sufferedlongtermillness && sufferedlongtermillness.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Have you ever required sick leave for a back or neck injury?:</span>
                          <span className="right-boxes">{leaveforneckinjury && leaveforneckinjury.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Have you ever required sick leave for a back or neck injury?</span>
                          <span className="right-boxes">{neckinjuries && neckinjuries.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Have you been in contact with anyone who is suffering from a contagious illness within the last six weeks?</span>
                          <span className="right-boxes">{sixweeksillness && sixweeksillness.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Do you suffer with a communicable disease?</span>
                          <span className="right-boxes">{communicabledisease && communicabledisease.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Are you currently receiving active medical attention?</span>
                          <span className="right-boxes">{medicalattention && medicalattention.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">If you have answered ‘yes’ to any of the above, please give details</span>
                          <span className="right-boxes">{healthdeclarationdetails}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Are you registered disabled?</span>
                          <span className="right-boxes">{registereddisabled && registereddisabled.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">How many days have you been absent from work due to illness in the last 12 months?</span>
                          <span className="right-boxes">{absentfromwork && absentfromwork.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">State reason(s) for absence</span>
                          <span className="right-boxes">{statereason}</span>
                        </p>

                        <p>
                          <span className="left-boxes">GP Name</span>
                          <span className="right-boxes">{GPname}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Street Address</span>
                          <span className="right-boxes">{GPstreetAddress}</span>
                        </p>

                        <p>
                          <span className="left-boxes">City</span>
                          <span className="right-boxes">{GPcity}</span>
                        </p>

                        <p>
                          <span className="left-boxes">County / State / Region</span>
                          <span className="right-boxes">{GPstate}</span>
                        </p>

                        <p>
                          <span className="left-boxes">ZIP / Postal Code</span>
                          <span className="right-boxes">{GPzip}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Health Details</span>
                          <span className="right-boxes">{GPhealthdetails}</span>
                        </p>

                        <p>
                          <span className="left-boxes">May we contact your Doctor for health check?</span>
                          <span className="right-boxes">{contactDoctor && contactDoctor.toUpperCase()}</span>
                        </p>
                        <span className="space-span">
                          The above information will be held in strict confidence. If you aware of any health issue that you feel may affect your ability to undertake responsibilities of the post it is your responsibility to inform the Care
                          Manager immediately. Again any details discussed in the meeting will be held in strict confidence.
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4>Medical History</h4>
                      <div className="box-way">
                        <h5> All staff groups complete this section </h5>
                        <p>
                          <span className="left-boxes">Do you have any illness/impairment/disability (physical or psychological) which may affect your work?</span>
                          <span className="right-boxes">{medicalillness && medicalillness.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Have you ever had any illness/impairment/disability which may have been caused or made worse by your work?</span>
                          <span className="right-boxes">{medicalillnesscaused && medicalillnesscaused.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">
                            Are you having, or waiting for treatment (including medication) or investigations at present? If your answer is yes, please provide further details of the condition, treatment and dates?
                          </span>
                          <span className="right-boxes">{treatment && treatment.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Do you think you may need any adjustments or assistance to help you to do the job?</span>
                          <span className="right-boxes">{needanyadjustments && needanyadjustments.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Notes</span>
                          <span className="right-boxes">{moment(needanyadjustmentsnotes).format("DD-MM-YYYY")}</span>
                        </p>

                        <h5> Tuberculosis </h5>
                        <span className="space-span"> Clinical diagnosis and management of tuberculosis, and measures for its prevention and control (NICE 2006) </span>

                        <p>
                          <span className="left-boxes">Have you lived continuously in the UK for the last 5 years</span>
                          <span className="right-boxes">{livedUK && livedUK.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Details</span>
                          <span className="right-boxes">{livedUKnotes}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Have you had a BCG vaccination in relation to Tuberculosis?</span>
                          <span className="right-boxes">{BCGvaccination && BCGvaccination.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Have you had a BCG vaccination in relation to Tuberculosis?</span>
                          <span className="right-boxes">{moment(BCGvaccinationdate).format("DD-MM-YYYY")}</span>
                        </p>

                        <p>
                          <span className="left-boxes">A cough which has lasted for more than 3 weeks</span>
                          <span className="right-boxes">{cough && cough.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Unexplained weight loss</span>
                          <span className="right-boxes">{weightloss && weightloss.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Unexplained fever</span>
                          <span className="right-boxes">{fever && fever.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Have you had tuberculosis (TB) or been in recent contact with open TB</span>
                          <span className="right-boxes">{TB && TB.toUpperCase()}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4> Chicken Pox or Shingles </h4>
                      <div className="box-way">
                        <p>
                          <span className="left-boxes">Have you ever had chicken pox or shingles?</span>
                          <span className="right-boxes">{chickenpox && chickenpox.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Date</span>
                          <span className="right-boxes">{moment(chickenpoxdate).format("DD-MM-YYYY")}</span>
                        </p>
                        <h5>Immunisation History</h5>
                        <span className="space-span">
                          {" "}
                          The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body for receipt
                          of DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all staff. It is a
                          condition of proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could invalidate your application or
                          lead to the cancellation of your registration with us.{" "}
                        </span>

                        <p>
                          <span className="left-boxes">Polio</span>
                          <span className="right-boxes">{polio && polio.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Tetanus</span>
                          <span className="right-boxes">{tetanus && tetanus.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Hepatitis B</span>
                          <span className="right-boxes">{hepatitisB && hepatitisB.toUpperCase()}</span>
                        </p>

                        <p>
                          <span className="left-boxes">Triple vaccination as a child (Diptheria / Tetanus / Whooping cough)?</span>
                          <span className="right-boxes">{triplevaccination && triplevaccination.toUpperCase()}</span>
                        </p>
                        <h5>Varicella</h5>
                        <span className="space-span">
                          {" "}
                          You must provide a written statement to confirm that you have had chicken pox or shingles however we strongly advise that you provide serology test result showing varicella immunity{" "}
                        </span>
                        <h5>Tuberculosis</h5>
                        <span className="space-span">We require an occupational health/GP certificate of a positive scar or a record of a positive skin test result (Do not Self Declare)</span>
                        <h5>Rubella, Measles & Mumps </h5>
                        <span className="space-span"> Certificate of “two” MMR vaccinations or proof of a positive antibody for Rubella Measles & Mumps </span>
                        <h5>Hepatitis B</h5>
                        <span className="space-span"> You must provide a copy of the most recent pathology report showing titre levels of 100lu/l or above </span>
                        <h5> Hepatitis B Surface Antigen </h5>
                        <span className="space-span"> Evidence of a negative Surface Antigen Test Report must be an identified validated sample. (IVS)</span>
                        <h5> Hepatitis C </h5>
                        <span className="space-span"> Evidence of a negative antibody test Report must be an identified validated sample. (IVS) </span>
                        <h5> HIV </h5>
                        <span className="space-span"> Evidence of a negative antibody test Report must be an identified validated s ample. (IVS) </span>
                        <h5>Exposure Prone Procedures</h5>
                        <p>
                          <span className="left-boxes">Will your role involve Exposure Prone Procedures</span>
                          <span className="right-boxes">{proneProcedures && proneProcedures.toUpperCase()}</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4> Declaration </h4>
                      <div className="box-way">
                        {/* <span className="space-span">
                            I declare that the answers to the above questions are true and complete to the best of my knowledge and belief. I also give consent for our appointed Occupational Health Services provider to make recommendations to
                          my employer
                        </span>*/}
                        <h5>Disclosure Barring Service (DBS)</h5>
                        <span className="space-span">
                          The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body for receipt
                          of DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all staff. It is a
                          condition of proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could invalidate your application or
                          lead to the cancellation of your registration with us
                        </span>
                        <p>
                          <span className="left-boxes">Have you been convicted of a criminal offence?</span>
                          <span className="right-boxes">{criminaloffence && criminaloffence.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-boxes">Have you ever been cautioned or issued with a formal warning for a criminal offence?</span>
                          <span className="right-boxes">{warningcriminaloffence && warningcriminaloffence.toUpperCase()}</span>
                        </p>
                        {/* <span className="space-span">I confirm the above is true and I understand that a DBS check will be sort in the event of a successful application.</span>*/}
                        <h5>Rehabilitation of Offenders Act 1974 and Criminal Records</h5>
                        <span className="space-span">
                          By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is
                          concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties. You should there force
                          list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4> Right To Work </h4>
                      <div className="box-way">
                        <h6>
                          It is a legal requirement that before any offer of work can be made all candidates provide the company with confirmation of their eligibility to work in the UK by providing one of the original documents detailed
                          below.
                        </h6>
                        <h6>A passport which describes the holder as a British Citizen or as having a right of abode in the United Kingdom or a passport or other </h6>
                        <span className="space-span">travel document to show that the holder has IDENFINITE LEAVE TO REMAIN in the United Kingdom and is not precluded from taking the work in question.</span>
                        <span>
                          {passport === "britishcitizen"
                            ? "A passport which describes the holder as a British Citizen or as having a right of abode in the United Kingdom or a passport or other travel document to show that the holder has IDENFINITE LEAVE TO REMAIN in the United Kingdom and is not precluded from taking the work in question."
                            : passport === "state"
                              ? "A passport or identity card issued by a State which is party to the European Union and EEA Agreement and which describes the holder as a national or a state which is a arty to that agreement."
                              : passport === "homeoffice"
                                ? "A letter issued by the Home Office or the Department of Education and Employment indicating that the person named in the letter has permission to take agency work in question or a biometric residence permit."
                                : ""}
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="per-infos">
                    <div className="fl-width">
                      <h4> Work Time Directives </h4>
                      <div className="box-way">
                        <span className="space-span">
                          {" "}
                          By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is
                          concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties. You should there force
                          list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.{" "}
                        </span>
                        <h6> {workhours === "yes" ? "I DO wish to work more than 48 hours per week" : workhours === "no" ? "I DO NOT wish to work more than 48 hours per week" : ""}</h6>
                        <h5>Registration form Declaration</h5>
                        <span className="space-span"> I declare that all information given in this registration form is to the best of my knowledge complete and accurate in all respects and that I am eligible to work in the UK.</span>
                      </div>
                    </div>
                  </div>
                </section>
              </CardBody>
            </Card>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(Yourdetails);
