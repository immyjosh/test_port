/* eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import request from "../../api/api";
import { Bar } from "react-chartjs-2";
import { Col, Row, Card, CardBody, CardHeader, Input, Button } from "reactstrap";
import Widget02 from "../../views/Template/Widgets/Widget02";
// import moment from "moment";

// var prouser = "../../img/user-profile.png";

class Dashboard extends Component {
  state = {
    token: "",
    employeedashboard: "",
    added: "",
    accepted: "",
    assigned: "",
    completed: "",
    ongoing: "",
    timehseet_approved: "",
    invoice_approved: "",
    Payment_completed: "",
    expired: "",
    draft: "",
    approved: "",
    part_paid: "",
    paid: "",
    draft_rate: "",
    approved_rate: "",
    part_paid_rate: "",
    paid_rate: "",
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all",
    form_recruitement_module: "",
    totalshiftCount: 0
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (token) {
      this.setState({ token });
    } else {
      return this.props.history.replace("/login");
    }
    this.populate();
    request({
      url: "/employee/profile",
      method: "POST",
      data: token.username
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          form_recruitement_module: res.response.result && res.response.result.onlineform && res.response.result.onlineform.recruitment_module,
          form_status: res.response.result && res.response.result.onlineform && res.response.result.onlineform.form_status,
        });
      } else if (res.status === 0) {
        let toastId = "profileResponse";
        if (!toast.isActive(toastId)) {
          toastId = toast.info(res.response);
        }
      }
    });
  }
  populate() {
    request({
      url: "/employeedashboard",
      method: "post",
      data: { selectshift: this.state.selectshift, selecttimesheet: this.state.selecttimesheet, selectinvoice: this.state.selectinvoice }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          this.setState({ employeedashboard: res.response.result && res.response.result.length > 0 ? res.response.result[0] : {} });
          if (res.response.result[0].shifts) {
            let data = res.response.result[0].shifts && res.response.result[0].shifts.length > 0 && res.response.result[0].shifts[0];
            this.setState({
              added: data.added || 0,
              accepted: data.accepted || 0,
              assigned: data.assigned || 0,
              completed: data.completed || 0,
              ongoing: data.ongoing || 0,
              timehseet_approved: data.timehseet_approved || 0,
              invoice_approved: data.invoice_approved || 0,
              Payment_completed: data.Payment_completed || 0,
              expired: data.expired || 0,
              totalshiftCount:
                (data.assigned ? data.assigned : 0) +
                (data.completed ? data.completed : 0) +
                (data.ongoing ? data.ongoing : 0) +
                (data.timehseet_approved ? data.timehseet_approved : 0) +
                (data.invoice_approved ? data.invoice_approved : 0) +
                (data.Payment_completed ? data.Payment_completed : 0) +
                (data.expired ? data.expired : 0)
            });
          }
          if (res.response.result[0].invoices) {
            let data = res.response.result[0].invoices && res.response.result[0].invoices.length > 0 && res.response.result[0].invoices[0];
            this.setState({
              draft: data.draft || 0,
              approved: data.approved || 0,
              part_paid: data.part_paid || 0,
              paid: data.paid || 0,
              draft_rate: data.draft_rate || 0,
              approved_rate: data.approved_rate || 0,
              part_paid_rate: data.part_paid_rate || 0,
              paid_rate: data.paid_rate || 0
            });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.populate());
  };
  render() {
    // const pie1 = {
    //   labels: ["Approved", "Pending"],
    //   datasets: [
    //     {
    //       data: [
    //         this.state.employeedashboard && this.state.employeedashboard.timesheetapproved ? this.state.employeedashboard.timesheetapproved : "0",
    //         this.state.employeedashboard && this.state.employeedashboard.timesheetpending ? this.state.employeedashboard.timesheetpending : "0"
    //       ],
    //       backgroundColor: ["#4dbd74", "#f8cb00"],
    //       hoverBackgroundColor: ["#4dbd74", "#f8cb00"]
    //     }
    //   ]
    // };
    const bar = {
      labels: ["Shifts"],
      datasets: [
        // {
        //   label: "Added",
        //   backgroundColor: "#20a8d8b8",
        //   borderColor: "#1382a9",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#20a8d8",
        //   hoverBorderColor: "#1382a9",
        //   data: [this.state && this.state.added]
        // },
        // {
        //   label: "Accepted",
        //   backgroundColor: "#f8cb00b0",
        //   borderColor: "#bb9a04",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#f8cb00",
        //   hoverBorderColor: "#bb9a04",
        //   data: [this.state && this.state.accepted]
        // },
        {
          label: "Total",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.totalshiftCount]
        },
        {
          label: "Assigned",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.assigned]
        },
        {
          label: "Ongoing",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.ongoing]
        },
        {
          label: "Completed",
          backgroundColor: "#4dbd74",
          borderColor: "#4dbd74",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#4dbd74",
          data: [this.state && this.state.completed]
        }
        // {
        //   label: "Timesheets Approved",
        //   backgroundColor: "#f8cb00b0",
        //   borderColor: "#bb9a04",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#f8cb00",
        //   hoverBorderColor: "#bb9a04",
        //   data: [this.state && this.state.timehseet_approved]
        // },
        // {
        //   label: "Invoice Approved",
        //   backgroundColor: "#63c2dead",
        //   borderColor: "#2fa0c1",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#63c2de",
        //   hoverBorderColor: "#2fa0c1",
        //   data: [this.state && this.state.invoice_approved]
        // },
        // {
        //   label: "Paid",
        //   backgroundColor: "#818a91b8",
        //   borderColor: "#595d61",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#818a91",
        //   hoverBorderColor: "#595d61",
        //   data: [this.state && this.state.Payment_completed]
        // },
        // {
        //   label: "Expired",
        //   backgroundColor: "#f86c6bb3",
        //   borderColor: "#c3403f",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#f86c6b",
        //   hoverBorderColor: "#c3403f",
        //   data: [this.state && this.state.expired]
        // }
      ]
    };
    const bar1 = {
      labels: ["Earnings"],
      datasets: [
        {
          label: "Pending",
          backgroundColor: "#20a8d8b8",
          borderColor: "#1382a9",
          borderWidth: 1,
          hoverBackgroundColor: "#20a8d8",
          hoverBorderColor: "#1382a9",
          data: [this.state && this.state.draft_rate]
        },
        {
          label: "Earnings",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.approved_rate]
        }
        // {
        //   label: "Part Paid",
        //   backgroundColor: "rgba(255,99,132,0.2)",
        //   borderColor: "rgba(255,99,132,1)",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "rgba(255,99,132,0.4)",
        //   hoverBorderColor: "rgba(255,99,132,1)",
        //   data: [this.state && this.state.part_paid]
        // },
        // {
        //   label: "Paid",
        //   backgroundColor: "rgba(255,99,132,0.2)",
        //   borderColor: "rgba(255,99,132,1)",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "rgba(255,99,132,0.4)",
        //   hoverBorderColor: "rgba(255,99,132,1)",
        //   data: [this.state && this.state.paid]
        // }
      ]
    };
    /* const bar2 = {
      labels: ["Timesheets"],
      datasets: [
        {
          label: "Approved",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state.employeedashboard && this.state.employeedashboard.timesheetapproved ? this.state.employeedashboard.timesheetapproved : "0"]
        },
        {
          label: "Pending",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state.employeedashboard && this.state.employeedashboard.timesheetpending ? this.state.employeedashboard.timesheetpending : "0"]
        }
      ]
    };*/
    const {form_status} = this.state;
    return (
      <Fragment>
        {/* <ToastContainer position="top-right" autoClose={2500} />*/}
        {/* <!--Form--> */}
        <div className="right-edit-side ">
          <div className="form__card card form__card--background float new-forms-profile">
            <div className="form__wrap extra">
              {this.state && this.state.token.recruitment_module === 1 ? (
                  <>
                    {this.state && this.state.form_recruitement_module === 1 ? (
                            <>
                              {form_status < 5?(
                                  <div className="app-forms mb-4">
                                    <h1 className="display-3">Application Online Form</h1>
                                    {/* <p className="lead">This is a simple hero unit, a simple Jumbotron-style component for calling extra attention to featured content or information.</p>*/}
                                    {/* <hr className="my-2" /> */}
                                    {/* <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>*/}
                                    <p className="lead">
                                      <Button color="primary" onClick={() => this.props.history.push("/user/onlineform")}>
                                        Click Here For Online Form
                                      </Button>
                                    </p>
                                  </div>
                              ):null}
                            </>
                    ) : null}
                  </>
              ) : null}
              {form_status >= 5 ? (
                  <Row>
                    <Col md="6" xs="12">
                      <Card className="chart-dashboard">
                        <CardHeader>
                          <i className="fa fa-bar-chart" /> Shifts
                          <div
                              className="card-actions"
                              onClick={() => {
                                this.props.history.push("/user/shiftslist");
                              }}
                          >
                            <button>
                              <i className="fa fa-th-list" />
                              <small className="text-muted">View Shifts</small>
                            </button>
                          </div>
                        </CardHeader>
                        <CardBody className="empl-dashboard">
                          <Row className="d-flex justify-content-end">
                            <Col md="4" xs="12">
                              <Input type="select" name="selectshift" value={this.state.selectshift} onChange={this.onChange}>
                                <option value="all">All</option>
                                <option value="0">Today</option>
                                <option value="7">Last 7 Days</option>
                                <option value="30">Last 30 Days</option>
                                <option value="365">Last 12 Months</option>
                              </Input>
                            </Col>
                          </Row>
                          <div className="chart-wrapper">
                            <Bar
                                data={bar}
                                options={{
                                  maintainAspectRatio: false
                                }}
                            />
                          </div>
                        </CardBody>
                      </Card>
                    </Col>

                    <Col md="6" xs="12">
                      <Card>
                        <CardHeader>
                          <i className="fa fa-area-chart" /> Earnings
                          <div
                              className="card-actions"
                              onClick={() => {
                                this.props.history.push("/user/invoicelist");
                              }}
                          >
                            <button>
                              <i className="fa fa-th-list" />
                              <small className="text-muted">View Earnings</small>
                            </button>
                          </div>
                        </CardHeader>
                        <CardBody className="empl-dashboard">
                          <Row className="d-flex justify-content-end">
                            <Col md="4" xs="12">
                              <Input type="select" name="selectinvoice" value={this.state.selectinvoice} onChange={this.onChange}>
                                <option value="all">All</option>
                                <option value="0">Today</option>
                                <option value="7">Last 7 Days</option>
                                <option value="30">Last 30 Days</option>
                                <option value="365">Last 12 Months</option>
                              </Input>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="12" xs="12" className="draft-details">
                              <p>
                                <strong>Pending : </strong> {this.state.draft ? <Fragment> {this.state.draft}</Fragment> : <Fragment> 0</Fragment>}{" "}
                              </p>
                              <p>
                                <strong>Pending Rate : </strong> {this.state.draft_rate ? <Fragment>£ {this.state.draft_rate}</Fragment> : <Fragment>£ 0</Fragment>}{" "}
                              </p>
                              <p>
                                <strong>Earnings : </strong> {this.state.approved ? <Fragment> {this.state.approved}</Fragment> : <Fragment> 0</Fragment>}{" "}
                              </p>
                              <p>
                                <strong>Earnings Rate : </strong> {this.state.approved_rate ? <Fragment>£ {this.state.approved_rate}</Fragment> : <Fragment>£ 0</Fragment>}{" "}
                              </p>
                            </Col>
                          </Row>
                          <div className="chart-wrapper">
                            <Bar
                                data={bar1}
                                options={{
                                  maintainAspectRatio: false
                                }}
                            />
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col md="12" xs="12">
                      <Card>
                        <CardHeader>
                          <i className="fa fa-pie-chart" /> Timesheets
                          <div
                              className="card-actions"
                              onClick={() => {
                                this.props.history.push("/user/timesheets");
                              }}
                          >
                            <button>
                              <i className="fa fa-th-list" />
                              <small className="text-muted">View Timesheets</small>
                            </button>
                          </div>
                        </CardHeader>
                        <Row className="dash-timesheet">
                          <Col xs="12" md="4" className="pr-2 rounded-0">
                            <Widget02
                                onClick={() => {
                                  this.props.history.push("/user/timesheets");
                                }}
                                header={this.state.employeedashboard ? this.state.employeedashboard.timesheetapproved + this.state.employeedashboard.timesheetpending : "0"}
                                mainText="Total"
                                icon="fa fa-users"
                                color="warning"
                                variant="1"
                            />
                          </Col>
                          <Col xs="12" md="4" className="pr-2 rounded-0">
                            <Widget02
                                onClick={() => {
                                  this.props.history.push("/user/timesheets");
                                }}
                                header={this.state.employeedashboard.timesheetapproved ? this.state.employeedashboard.timesheetapproved : "0"}
                                mainText="Approved"
                                icon="fa fa-users"
                                color="success"
                                variant="1"
                            />
                          </Col>
                          <Col xs="12" md="4" className="pl-2 rounded-0">
                            <Widget02
                                onClick={() => {
                                  this.props.history.push("/user/timesheets");
                                }}
                                header={this.state.employeedashboard.timesheetpending ? this.state.employeedashboard.timesheetpending : "0"}
                                mainText="Pending"
                                icon="fa fa-user-plus"
                                color="warning"
                                variant="1"
                            />
                          </Col>
                        </Row>
                        {/* <CardBody className="p-0">
                      <Row className="d-flex justify-content-end">
                        <Col md="3" xs="12">
                          <Input type="select" name="selecttimesheet" value={this.state.selecttimesheet}>
                              <option value="0">Today</option>
                              <option value="7">Last 7 Days</option>
                              <option value="30">Last 30 Days</option>
                              <option value="365">Last 12 Months</option>
                          </Input>
                        </Col>
                      </Row>
                      <div className="chart-wrapper">
                        <Bar
                          data={bar2}
                          options={{
                            maintainAspectRatio: false
                          }}
                        />
                      </div>
                    </CardBody>*/}
                      </Card>
                    </Col>
                  </Row>
              ):null}

            </div>
          </div>
        </div>

        {/* <!--Form--> */}
      </Fragment>
    );
  }
}

export default Dashboard;
