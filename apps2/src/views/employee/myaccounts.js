import classnames from "classnames";
import React, { Component, Fragment } from "react";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import Header from '../common/header';
import Footer from '../common/footer';

class myaccounts extends Component {
  state = {
    activemodeledittab: "1"
  };
  togglemodeledit = tab => {
    if (this.state.activemodeledittab !== tab) {
      this.setState({
        activemodeledittab: tab
      });
    }
  };
  render() {
    return (
      <Fragment>
       <Header/>

        {/* <!--Form--> */}
        <section className="section section--last section--top-space small-width">
          <div className="container">
            <div className="row" />
            <div className="row form">
              <div className="col-10 col-m-12 col-offset-1">
                <div className="form__card card form__card--background float">
                  <div className="form__wrap extra">
                    {/* <ul className="nav nav-tabs">
    <li className="active"><a data-toggle="tab" href="#home">Profile</a></li>
    <li><a data-toggle="tab" href="#menu1">Task Details</a></li>
 

  </ul> */}
                    <Nav tabs>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activemodeledittab === "1"
                          })}
                          onClick={() => {
                            this.togglemodeledit("1");
                          }}
                        >
                          Profile
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activemodeledittab === "2"
                          })}
                          onClick={() => {
                            this.togglemodeledit("2");
                          }}
                        >
                          Task Details
                        </NavLink>
                      </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activemodeledittab}>
                      <TabPane tabId="1">
                        <div id="home" className="">
                          <h3>
                            Account{" "}
                            <a className="pull-right" href="editprofile.html">
                              Edit
                            </a>
                          </h3>

                          <div className="accountextra">
                            <ul className="account-image">
                              <li>
                                {" "}
                                <i className="fa fa-user" aria-hidden="true" />
                                <span>Cathrine</span>
                              </li>
                              <li>
                                {" "}
                                <i className="fa fa-phone" />
                                <span>+800 123 456</span>
                              </li>
                            </ul>

                            <ul className="account-image">
                              <li>
                                {" "}
                                <i className="fa fa-envelope" />
                                <span>
                                  <a href="mailto:info@company.com">
                                    cathrine@gmail.com
                                  </a>
                                </span>
                              </li>
                              <li>
                                <i className="fa fa-map-marker" />
                                <span> Ut enim ad minim veniam , France</span>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </TabPane>
                      <TabPane tabId="2">
                        <div id="menu1" className="">
                          <div id="home" className="">
                            <h3 className="task1">Task Details</h3>
                            <div className="wholetask">
                              <div className="kitchen-clean">
                                <h2>Kitchen Cleaning-HFA-97456 </h2>
                              </div>
                              <div className="date">30 Aug</div>
                              <div className="map-location">
                                <div className="icon-loc">
                                  <i
                                    className="fa fa-map-marker"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="text-loc">
                                  1720 lane,united states
                                </div>
                              </div>

                              <div className="dollar">
                                <div className="icon-loc">
                                  <i className="fa fa-usd" aria-hidden="true" />
                                </div>
                                <div className="text-loc">59.50</div>
                              </div>

                              <div className="clock">
                                <div className="icon-loc">
                                  <i
                                    className="fa fa-clock-o"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="text-loc">Task Completed</div>
                              </div>
                            </div>

                            <div className="wholetask">
                              <div className="kitchen-clean">
                                <h2>Car Cleaning-HFA-97456 </h2>
                              </div>
                              <div className="date">30 Aug</div>
                              <div className="map-location">
                                <div className="icon-loc">
                                  <i
                                    className="fa fa-map-marker"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="text-loc">
                                  1720 lane,united states
                                </div>
                              </div>

                              <div className="dollar">
                                <div className="icon-loc">
                                  <i className="fa fa-usd" aria-hidden="true" />
                                </div>
                                <div className="text-loc">59.50</div>
                              </div>

                              <div className="clock">
                                <div className="icon-loc">
                                  <i
                                    className="fa fa-clock-o"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="text-loc">Task Completed</div>
                              </div>
                            </div>

                            <div className="wholetask">
                              <div className="kitchen-clean">
                                <h2>Plumbing-HFA-97456 </h2>
                              </div>
                              <div className="date">30 Aug</div>
                              <div className="map-location">
                                <div className="icon-loc">
                                  <i
                                    className="fa fa-map-marker"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="text-loc">
                                  1720 lane,united states
                                </div>
                              </div>

                              <div className="dollar">
                                <div className="icon-loc">
                                  <i className="fa fa-usd" aria-hidden="true" />
                                </div>
                                <div className="text-loc">59.50</div>
                              </div>

                              <div className="clock">
                                <div className="icon-loc">
                                  <i
                                    className="fa fa-clock-o"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="text-loc">Task Completed</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </TabPane>
                    </TabContent>
                    {/* <div className="tab-content">
   
     
    <div id="menu2" className="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" className="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
            
            
              </div>  */}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <img
            alt=""
            className="section__img"
            src="../img/img_backgroud_footer.png"
          />
        </section>
        {/* <!--Form--> */}

       <Footer/>
      </Fragment>
    );
  }
}

export default myaccounts;
