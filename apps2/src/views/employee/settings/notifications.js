import React, { Component, Fragment } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Input } from "reactstrap";
import { AvForm } from "availity-reactstrap-validation";
import { toast } from "react-toastify";
import request from "../../../api/api";

class Notifications extends Component {
  state = {
    mailstatus: 0,
    smsstatus: 0,
    adminlist: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/employee");
    }
    request({
      url: "/employee/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      this.setState({ adminlist: res.response.result });
      this.setState({
        mailstatus: this.state.adminlist.settings.notifications.email,
        smsstatus: this.state.adminlist.settings.notifications.sms
      });
    });
  }
  statusChangeemail = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      mailstatus: e.target.checked || value
    });
  };
  statusChangesms = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      smsstatus: e.target.checked || value
    });
  };
  onEditAdmin = (e, values) => {
    request({
      url: "/employee/settings/notifications/save",
      method: "POST",
      data: {
        mailstatus: this.state.mailstatus,
        smsstatus: this.state.smsstatus
      }
    })
      .then(response => {
        if (response.status === 1) {
          toast.success("Updated");
          this.componentDidMount();
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  };
  render() {
    return (
      <Fragment>
        <div className="right-edit-side">
          <div className="form__card card form__card--background float mobile-earnings">
            <AvForm onValidSubmit={this.onEditAdmin}>
              <Card className="p-0 mb-0 for-radius">
                <CardHeader className="for-radius">
                  <strong>Notifications</strong> Settings
                </CardHeader>
                <CardBody className="p-2">
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>Mail</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.mailstatus} onChange={this.statusChangeemail} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>SMS</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.smsstatus} onChange={this.statusChangesms} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" className="site-btn site-btn--accent pull-right mr-2" title="Save">
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </Card>
            </AvForm>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Notifications;
