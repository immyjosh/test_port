import { AvFeedback, AvForm, AvGroup, AvField, AvInput, AvRadioGroup, AvRadio } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
// import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Col, Label, Row, CustomInput } from "reactstrap";
import request from "../../api/api";
// import { SingleDatePicker } from "react-dates";
import Header from "../common/header";
import Footer from "../common/header";
// import classnames from "classnames";
import addnodify from "./login";
import { SingleDatePicker } from "react-dates";

class Registration extends Component {
  state = {
    remployeeyes: false,
    refereephone: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    applicantphone: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    phoneerror: false,
    phoneerror1: false
  };
  flash = new addnodify();
  componentDidMount() {}
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  remployeeyes = yes => {
    if (yes === "no") {
      this.setState({
        remployeeyes: true
      });
    } else {
      this.setState({
        remployeeyes: false
      });
    }
  };
  disciplinaryactionyes = yes => {
    if (yes === "yes") {
      this.setState({
        disciplinaryactionyes: true
      });
    } else {
      this.setState({
        disciplinaryactionyes: false
      });
    }
  };
  investigationsyes = yes => {
    if (yes === "yes") {
      this.setState({
        investigationsyes: true
      });
    } else {
      this.setState({
        investigationsyes: false
      });
    }
  };
  vulnerablepeopleyes = yes => {
    if (yes === "yes") {
      this.setState({
        vulnerablepeopleyes: true
      });
    } else {
      this.setState({
        vulnerablepeopleyes: false
      });
    }
  };
  criminaloffenceyes = yes => {
    if (yes === "yes") {
      this.setState({
        criminaloffenceyes: true
      });
    } else {
      this.setState({
        criminaloffenceyes: false
      });
    }
  };
  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          campanystamp: file_data,
          campanystampfile: URL.createObjectURL(file_data),
          campanystampName: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  OnFormSubmit = (event, errors, values) => {
    if (errors && errors.length === 0) {
      if (this.state.campanystamp) {
        let toastId = "referee_form_front_site_01";
        toastId = toast.info("Please Wait......", { autoClose: false });
        const data = new FormData();
        data.append("refereefirstname", values.refereefirstname);
        data.append("refereelastname", values.refereelastname);
        data.append("refereeemail", values.refereeemail);
        data.append("refereephone[number]", this.state.refereephone.number);
        data.append("refereephone[code]", this.state.refereephone.code);
        data.append("refereephone[dialcountry]", this.state.refereephone.dialcountry);
        data.append("refereejobtitle", values.refereejobtitle);
        data.append("refereecompany", values.refereecompany);
        data.append("refereecapacity", values.refereecapacity);
        data.append("applicantfirstname", values.applicantfirstname);
        data.append("applicantlastname", values.applicantlastname);
        data.append("applicantemail", values.applicantemail);
        data.append("applicantphone[number]", this.state.applicantphone.number);
        data.append("applicantphone[code]", this.state.applicantphone.code);
        data.append("applicantphone[dialcountry]", this.state.applicantphone.dialcountry);
        data.append("applicantjobtitle", values.applicantjobtitle);
        data.append("applicantcompany", values.applicantcompany);
        data.append("employedfrom", this.state.employedfrom);
        data.append("employedto", this.state.employedto);
        data.append("remployee", values.remployee);
        data.append("remployeenotes", values.remployeenotes || "");
        data.append("followcareplans", values.followcareplans);
        data.append("reliability", values.reliability);
        data.append("character", values.character);
        data.append("dignity", values.dignity);
        data.append("attitude", values.attitude);
        data.append("communication", values.communication);
        data.append("relationships", values.relationships);
        data.append("workunderinitiative", values.workunderinitiative);
        data.append("disciplinaryaction", values.disciplinaryaction);
        data.append("disciplinaryactiondetails", values.disciplinaryactiondetails || "");
        data.append("investigations", values.investigations);
        data.append("investigationsdetails", values.investigationsdetails || "");
        data.append("vulnerablepeople", values.vulnerablepeople);
        data.append("vulnerablepeopledetails", values.vulnerablepeopledetails || "");
        data.append("criminaloffence", values.criminaloffence);
        data.append("criminaloffencedetails", values.criminaloffencedetails || "");
        data.append("additionalcomments", values.additionalcomments);
        data.append("confirm", values.confirm);
        data.append("agree", values.agree);
        data.append("campanystamp", this.state.campanystamp);
        data.append("referencefor", this.props.match.params.referencefor);
        data.append("username", this.props.match.params.username);
        request({
          url: "/site/employee/reference/save",
          method: "POST",
          data: data
        }).then(res => {
          if (res.status === 1) {
            this.componentDidMount();
            toast.update(toastId, { render: "Form Sent!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            window.location = "/";
          } else if (res.status === 0) {
            toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
          }
        });
      } else {
        toast.error("Stamp Required");
      }
    } else {
      toast.error("Please fill all required fields");
    }
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      refereephone: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler1 = (status, value, countryData, number, id) => {
    console.log("kinmobileno", this.state.kinmobileno);
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      applicantphone: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  render() {
    return (
      <Fragment>
        <ToastContainer position="top-right" autoClose={2500} />
        <Header />
        {/* <!--Form--> */}
        <section className="section section--last section--top-space online-forms ref-on-forms">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h3 className="section__title">Reference Form</h3>
                <h6 className="text-center">Complete the reference form and submit</h6>
              </div>
            </div>
            <div className="row form register-froms">
              <div className="col-8 col-offset-2">
                <div className="form__card card form__card--background">
                  <div className="form__wrap width_less">
                    <h5>Sign Up</h5>
                    {/* <p>Learn how to sell smarter and faster with Agency Portal.</p> */}
                    <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                      <Row>
                        <h4>Referee Details</h4>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="text" name="refereefirstname" label="First name" required />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField type="text" name="refereelastname" label="Last name" required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="email" name="refereeemail" label="Email" required />
                        </Col>
                        <Col xs="12" md="6">
                          {/* <AvField type="number" name="refereephone" label="Phone" required /> */}
                          <Label className={this.state.phoneerror ? "error-color" : null}>Phone</Label>
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={this.state.refereephone.dialcountry}
                            utilsScript={libphonenumber}
                            css={this.state.phoneerror ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={this.handler}
                            value={this.state.refereephone.number}
                            required
                          />
                          {this.state.phoneerror ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="text" name="refereejobtitle" label="Job Title" required />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField type="text" name="refereecompany" label="Company" required />
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="12">
                          <AvField type="textarea" name="refereecapacity" label="In what capacity did you know the applicant?:" required />
                        </Col>
                      </Row>
                      <Row>
                        <h4>Applicant Details</h4>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="text" name="applicantfirstname" label="First name" required />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField type="text" name="applicantlastname" label="Last name" required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="email" name="applicantemail" label="Email" required />
                        </Col>
                        <Col xs="12" md="6">
                          {/* <AvField type="number" name="applicantphone" label="Phone" required /> */}
                          <Label className={this.state.phoneerror1 ? "error-color" : null}>Phone</Label>
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={this.state.applicantphone.dialcountry}
                            utilsScript={libphonenumber}
                            css={this.state.phoneerror1 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={this.handler1}
                            value={this.state.applicantphone.number}
                            required
                          />
                          {this.state.phoneerror1 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvField type="text" name="applicantjobtitle" label="Job Title" required />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField type="text" name="applicantcompany" label="Company" required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <label>Date Employed from:</label>
                          <SingleDatePicker
                            // className={this.state.finaldateError ? "error-color" : null}
                            date={this.state.employedfrom}
                            onDateChange={employedfrom => this.setState({ employedfrom })}
                            focused={this.state.focused1}
                            onFocusChange={({ focused: focused1 }) => this.setState({ focused1 })}
                            id="employedfrom"
                            displayFormat="DD-MM-YYYY"
                            isOutsideRange={() => false}
                          />
                        </Col>
                        <Col xs="12" md="6">
                          <label>Date Employed to:</label>
                          <SingleDatePicker
                            // className={this.state.finaldateError ? "error-color" : null}
                            date={this.state.employedto}
                            onDateChange={employedto => this.setState({ employedto })}
                            focused={this.state.focused2}
                            onFocusChange={({ focused: focused2 }) => this.setState({ focused2 })}
                            id="employedto"
                            displayFormat="DD-MM-YYYY"
                            isOutsideRange={() => false}
                          />
                        </Col>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="remployee" label="Would you re-employ this person?" value={this.state.drivinglicence} required>
                            <AvRadio label="Yes" value="yes" onClick={this.remployeeyes} />
                            <AvRadio label="No" value="no" onClick={() => this.remployeeyes("no")} />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      {this.state.remployeeyes ? (
                        <Row>
                          <Col xs="12" md="12">
                            <AvField type="textarea" label="Please explain why you would not re-employ them." name="remployeenotes" placeholder="" value={this.state.remployeenotes || ""} onChange={this.onChange} />
                          </Col>
                        </Row>
                      ) : null}
                      <Row>
                        <h4>SECTION 1 - HOW WOULD YOU ASSESS THE FOLLOWING?</h4>
                        <h6>Please tick the relevant boxes Excellent, Good, Average, Poor</h6>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="followcareplans" label="Ability to follow care plans" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="reliability" label="Reliability, timekeeping,attendance" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="character" label="Character" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="attitude" label="Attitude" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="dignity" label="Ability to ensure dignity is upheld" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="communication" label="Communication" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="relationships" label="Relationships with colleagues" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="workunderinitiative" label="Ability to work under own initiative" required>
                            <AvRadio label="Excellent" value="excellent" />
                            <AvRadio label="Good" value="good" />
                            <AvRadio label="Average" value="average" />
                            <AvRadio label="Poor" value="poor" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      <Row>
                        <h4>SECTION - 2 Please answer the following questions</h4>
                      </Row>
                      <Row>
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="disciplinaryaction" label="Has the applicant been subject to any disciplinary action?" required>
                            <AvRadio label="Yes" value="yes" onClick={() => this.disciplinaryactionyes("yes")} />
                            <AvRadio label="No" value="no" onClick={this.disciplinaryactionyes} />
                          </AvRadioGroup>
                        </Col>
                        {this.state.disciplinaryactionyes ? (
                          <Col xs="12" md="6">
                            <AvField type="textarea" label="Please provide details:" name="disciplinaryactiondetails" value={this.state.disciplinaryactiondetails || ""} placeholder="" />
                          </Col>
                        ) : null}
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="investigations" label="Are you aware of the applicants involvement in any safeguarding investigations previous or current ." required>
                            <AvRadio label="Yes" value="yes" onClick={() => this.investigationsyes("yes")} />
                            <AvRadio label="No" value="no" onClick={this.investigationsyes} />
                          </AvRadioGroup>
                        </Col>
                        {this.state.investigationsyes ? (
                          <Col xs="12" md="6">
                            <AvField type="textarea" label="Please provide details:" name="investigationsdetails" value={this.state.investigationsdetails || ""} placeholder="" />
                          </Col>
                        ) : null}
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="vulnerablepeople" label="Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?" required>
                            <AvRadio label="Yes" value="yes" onClick={() => this.vulnerablepeopleyes("yes")} />
                            <AvRadio label="No" value="no" onClick={this.vulnerablepeopleyes} />
                          </AvRadioGroup>
                        </Col>
                        {this.state.vulnerablepeopleyes ? (
                          <Col xs="12" md="6">
                            <AvField type="textarea" label="Please provide details:" name="vulnerablepeopledetails" value={this.state.vulnerablepeopledetails || ""} placeholder="" />
                          </Col>
                        ) : null}
                        <Col xs="12" md="6">
                          <AvRadioGroup inline name="criminaloffence" label="To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?" required>
                            <AvRadio label="Yes" value="yes" onClick={() => this.criminaloffenceyes("yes")} />
                            <AvRadio label="No" value="no" onClick={this.criminaloffenceyes} />
                          </AvRadioGroup>
                        </Col>
                        {this.state.criminaloffenceyes ? (
                          <Col xs="12" md="6">
                            <AvField type="textarea" label="Please provide details:" name="criminaloffencedetails" value={this.state.criminaloffencedetails || ""} placeholder="" />
                          </Col>
                        ) : null}
                      </Row>
                      <Row>
                        <Col xs="12" md="12">
                          <AvField type="textarea" label="Additional Comments" name="additionalcomments" placeholder="" />
                        </Col>
                      </Row>
                      <Row>
                        <h4>PLEASE CONFIRM:</h4>
                        <p>
                          I can confirm that all the details provided are accurate at the time that this reference was completed. I can confirm that I am authorised to provide a reference on behalf of my organisation. I understand this
                          reference may be shown to a third party for auditing purposes and I can confirm that Local Care Force has this organisation s consent and authorisation to disclose the contents of this reference to its end user,
                          hirer clients. I understand that the applicant has the legal right to request a copy of their reference.
                        </p>
                      </Row>
                      <div className="form__form-group form__form-group--read-and-agree">
                        <label className="checkbox-btn ref-plc">
                          <AvGroup>
                            <Label check for="confirm" />
                            <AvInput className=" checkbox-btn__checkbox" type="checkbox" name="confirm" required />
                            <span className="checkbox-btn__checkbox-custom mt-3">
                              <i className="mdi mdi-check" />
                            </span>
                            <span className="checkbox-btn__label">Check to confirm</span>
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </label>
                      </div>
                      <Row>
                        <Col xs="12" md="12">
                          <Label for="exampleCustomFileBrowser">Official company stamp.</Label>
                          <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser"
                            name="campanystamp"
                            onChange={this.fileChangedHandler}
                            label={this.state.campanystampName ? this.state.campanystampName : "Upload  Image"}
                            encType="multipart/form-data"
                            accept=".png, .jpg, .jpeg"
                          />
                        </Col>
                      </Row>
                      <div className="mid-values">
                        <p>If you are not using an official company email please upload a picture of your company stamp / business card/letterhead/compliment slip.</p>
                      </div>
                      <div className="form__form-group form__form-group--read-and-agree">
                        <label className="checkbox-btn">
                          <AvGroup>
                            <Label check for="agree" />
                            <AvInput className="checkbox-btn__checkbox" type="checkbox" name="agree" required />

                            <span className="checkbox-btn__checkbox-custom mt-3">
                              <i className="mdi mdi-check" />
                            </span>
                            <span className="checkbox-btn__label">By using this form you agree with the storage and handling of your data by this website as defined in our Privacy Policy.</span>
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </label>
                      </div>
                      <button type="submit" title="Click here to Register" className="btn btn-primary btn-lg btn-block site-btn--accent form__submit mb-4">
                        Submit
                      </button>
                    </AvForm>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <img alt="" className="section__img" src="../img/img_backgroud_footer.png" />
        </section>
        {/* <!--Form--> */}

        <Footer />
      </Fragment>
    );
  }
}

export default Registration;
