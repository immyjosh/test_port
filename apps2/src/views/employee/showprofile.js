import React, { Component, Fragment } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Card, CardBody, CardHeader } from "reactstrap";
import request, { NodeURL } from "../../api/api";
import { withRouter } from "react-router-dom";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";

var prouser = "../../img/user-profile.png";
class showprofile extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    email: "",
    status: "",
    role: "",
    code: "",
    job_type: "",
    employee_rate: "",
    client_rate: "",
    number: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    avatar: ""
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (this.props.location.state === true) {
      this.nodifydashboard(token);
    }
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/login");
    }
    request({
      url: "/employee/profiledetails",
      method: "POST",
      data: token.username
    }).then(res => {
      if (res.status === 1) {
        console.log("res", res);
        if (res.response.result !== undefined) {
          this.setState({ adminlist: res.response.result[0] });
          this.setState({
            username: this.state.adminlist.username,
            name: this.state.adminlist.name,
            email: this.state.adminlist.email,
            job_type: this.state.adminlist.jobtypes,
            holiday_allowance: this.state.adminlist.holiday_allowance,
            locations: this.state.adminlist.locations,
            employee_rate: this.state.adminlist.employee_rate,
            status: this.state.adminlist.status,
            number: this.state.adminlist.phone.number,
            code: this.state.adminlist.phone.code,
            line1: this.state.adminlist.address.line1,
            line2: this.state.adminlist.address.line2,
            city: this.state.adminlist.address.city,
            state: this.state.adminlist.address.state,
            country: this.state.adminlist.address.country,
            zipcode: this.state.adminlist.address.zipcode,
            lat: this.state.adminlist.address.lat,
            lon: this.state.adminlist.address.lon,
            formatted_address: this.state.adminlist.address.formatted_address,
            avatar_return: this.state.adminlist.avatar
          });
        }
      }
    });
  }
  nodifydashboard = token => {
    let toastId = "loginNotify";
    if (!toast.isActive(toastId)) {
      toastId = toast.success(`Welcome  ${token.username}`);
      this.props.history.push({ state: false });
    }
  };
  render() {
    return (
      <Fragment>
        <div className="right-edit-side">
          <div className="form__card card form__card--background float mobile-earnings">
            <Card className="p-0 mb-0 for-radius">
              <CardHeader className="for-radius">
                Profile Details
              </CardHeader>
              <CardBody className="p-2">
                <div className="showing-profiles">
                  <div className="show-prog">
                  <div className="listing-catrgories">
                      <p>
                        <span className="left-pro">Name <span>:</span></span>
                        <span className="right-pro">{this.state.name}</span>
                      </p>
                      <p>
                        <span className="left-pro">Email Address<span>:</span></span>
                        <span className="right-pro">{this.state.email}</span>
                      </p>
                      <p>
                        <span className="left-pro">Phone Number<span>:</span></span>
                        <span className="right-pro">{this.state.number}</span>
                      </p>
                      <p>
                        <span className="left-pro">Area<span>:</span></span>
                        <span className="right-pro">
                          {this.state.locations &&
                            this.state.locations.map((list, key) => (
                              <Fragment key={key}>
                                {list.name}
                                <Fragment>,</Fragment>
                              </Fragment>
                            ))}
                        </span>
                      </p>
                      <p>
                        <span className="left-pro">Job Role<span>:</span></span>
                        <span className="right-pro">
                          {this.state.job_type &&
                            this.state.job_type.map((list, key) => (
                              <Fragment key={key}>
                                {list.name}
                                <Fragment>,</Fragment>
                              </Fragment>
                            ))}
                        </span>
                      </p>
                      {/* <p>
                        <span className="left-pro">Employee Rate:</span>
                        <span className="right-pro">£  {this.state.employee_rate}</span>
                      </p> */}
                      <p>
                        <span className="left-pro">Address<span>:</span></span>
                        <span className="right-pro">
                          {this.state.formatted_address} , {this.state.zipcode}
                        </span>
                      </p>
                    </div>
				            <div className="showpro-lefts">
                    <div className="user-views">
                      <img
                        className="img-fluid"
                        width="240px"
                        height="180px"
                        src={NodeURL + "/" + this.state.avatar_return}
                        onError={() => {
                          this.setState({ avatar_return: prouser });
                        }}
                        alt="profile"
                      />
                      <button
                        type="button"
                        className="btn profile-edit"
                        onClick={() => {
                          this.props.history.push("/user/profile");
                        }}
                      >
                        <i class="fa fa-pencil"></i>
                      </button>
                    </div>
					          </div>
                   
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(showprofile);
