/*eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { toast, ToastContainer } from "react-toastify";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardFooter
  //  Table,
  //   UncontrolledTooltip
} from "reactstrap";
import moment from "moment";
import request from "../../../api/api";

class Invoicedetails extends Component {
  state = {
    client_rate: "",
    agency_details: "",
    branch: "",
    client_details: "",
    employee_details: "",
    employee_rate: "",
    job_type: "",
    locations_details: "",
    starttime: "",
    endtime: "",
    late_amount: "",
    notes: {},
    rate_details: [],
    start_date: "",
    shift_id: "",
    breaktime: "",
    holded: "",
    total_duration: "",
    started_time: "",
    ended_time: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/employee");
    }
    request({
      url: "/employee/invoice/view",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    })
      .then(res => {
        if (res.status === 1) {
          var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          var evestarthr = res.response.result[0].starttime / 3600;
          var splithr = evestarthr.toString().split(".");
          var startsec = splithr[1];
          if (startsec === undefined) {
            var startmins = "00";
          } else {
            startmins = (+"0" + "." + startsec) * 60;
          }

          if (num.indexOf(splithr[0]) === -1) {
            var starttimehr = splithr[0] + ":" + startmins;
          } else {
            starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
          }
          var eveendhr = res.response.result[0].endtime / 3600;
          var splitendhr = eveendhr.toString().split(".");
          var endsec = splitendhr[1];
          if (endsec === undefined) {
            var endmins = "00";
          } else {
            endmins = (+"0" + "." + endsec) * 60;
          }

          if (num.indexOf(splitendhr[0]) === -1) {
            var endtimehr = splitendhr[0] + ":" + endmins;
          } else {
            endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
          }
          let ConvertedRateDetails = [];
          let CB = res.response.result[0].rate_details.map(list => {
            var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            var evestarthr = list.from / 3600;
            var splithr = evestarthr.toString().split(".");
            var startsec = splithr[1];
            if (startsec === undefined) {
              var startmins = "00";
            } else {
              startmins = (+"0" + "." + startsec) * 60;
            }

            if (num.indexOf(splithr[0]) === -1) {
              var starttimehr = splithr[0] + ":" + startmins;
            } else {
              starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
            }
            var eveendhr = list.to / 3600;
            var splitendhr = eveendhr.toString().split(".");
            var endsec = splitendhr[1];
            if (endsec === undefined) {
              var endmins = "00";
            } else {
              endmins = (+"0" + "." + endsec) * 60;
            }

            if (num.indexOf(splitendhr[0]) === -1) {
              var endtimehr = splitendhr[0] + ":" + endmins;
            } else {
              endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
            }
            let day,
              period = "";
            let Shift_week = list.name.slice(0, 3).toLowerCase();
            let words = list.name;
            let Shift_period = words.substr(words.length - 3);
            if (Shift_period === "day") {
              period = "Morning";
            } else if (Shift_period === "ght") {
              period = "Night";
            }
            switch (Shift_week) {
              case "mon":
                day = "Monday";
                break;
              case "tue":
                day = "Tuesday";
                break;
              case "wed":
                day = "Wednesday";
                break;
              case "thu":
                day = "Thursday";
                break;
              case "fri":
                day = "Friday";
                break;
              case "sat":
                day = "Saturday";
                break;
              case "sun":
                day = "Sunday";
                break;
              case "pub":
                day = "Public Holiday";
                break;
              default:
                day = "";
                break;
            }
            /* eslint no-unused-vars: 0*/
            let _id, hold, restart, holdedmins;
            ConvertedRateDetails.push({
              _id: list._id,
              from: starttimehr,
              to: endtimehr,
              day: day,
              period: period,
              name: list.name,
              client_rate: list.client_rate,
              employee_rate: list.employee_rate
            });
            return true;
          });

          this.setState({
            _id: res.response.result[0]._id,
            agency_details: res.response.result[0].agency_details[0].name,
            shift_id: res.response.result[0].shift_id,
            branch: res.response.result[0].branch_data[0].branches.branchname,
            client_details: res.response.result[0].client_details[0].name,
            employee_details: res.response.result[0].employee_details[0].name,
            client_rate: res.response.result[0].client_rate,
            employee_rate: res.response.result[0].employee_rate,
            job_type: res.response.result[0].job_type,
            late_amount: res.response.result[0].late_amount,
            notes: res.response.result[0].notes,
            rate_details: ConvertedRateDetails,
            starttime: starttimehr,
            endtime: endtimehr,
            start_date: res.response.result[0].start_date,
            started_time: moment(res.response.result[0].shift_details[0].timesheet[0].start).format("HH:mm"),
            ended_time: moment(res.response.result[0].shift_details[0].timesheet[0].end).format("HH:mm"),
            breaktime: res.response.result[0].shift_details[0].breaktime / 60,
            holded: res.response.result[0].shift_details[0].timesheet[0].holded ? res.response.result[0].shift_details[0].timesheet[0].holded : 0,
            total_duration: res.response.result[0].shift_details[0].timesheet[0].workmins / 60,
            locations_details: res.response.result[0].locations_details[0].name
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => toast.error(err));
  }
  approve = () => {
    request({
      url: "/employee/invoice/approval",
      method: "POST",
      data: { invoiceid: this.state._id }
    }).then(res => {
      if (res.status === 1) {
        toast.success("Approved");
        this.componentDidMount();
        setTimeout(() => {
          this.componentDidMount();
        }, 2000);
        // this.setState({ Showapprovebtn: false });
      } else {
        toast.error(res.response);
      }
    });
  };
  backTopage = id => {
    return this.props.history.push({
      pathname: "/user/invoicegroupview",
      state: { rowid: this.props.location.state.Getpage.previd, Dates: this.props.location.state.Getpage.Dates }
    });
  };
  render() {
    return (
      <Fragment>
        <div className="right-edit-side">
          <div className="form__card card form__card--background float mobile-earnings">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <CardHeader>
                <i className="icon-list" />
                Earning Details
              </CardHeader>
              <CardBody>
                <Fragment>
                  <ul className="invoice-view">
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Shift <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc"> {this.state.shift_id} </span>
                    </li>
                    {/* <li>
                    <span className="inv-heading">
                      {" "}
                      Client <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.client_details} </span>
                  </li> */}
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Location <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc"> {this.state.locations_details} </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Branch <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc"> {this.state.branch} </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Employee <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc"> {this.state.employee_details} </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Job Role <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc"> {this.state.job_type} </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Date
                        <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc">
                        {" "}
                        {moment(this.state.start_date).format("DD-MM-YYYY")} ({moment(this.state.start_date).format("dddd")})
                      </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Time <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc">
                        {" "}
                        {this.state.starttime} - {this.state.endtime}{" "}
                      </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Break <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc"> {this.state.breaktime} Minutes </span>
                    </li>
                    {/* <li><span className="inv-heading">  Duration <span className="mvr-space"> : </span> </span> <span className="inv-desc">  05:00 PM </span></li> */}
                    {/* <li className="invoice-widths">
                    <div className="days-durations">
                      <h5>Job Rate Details</h5>
                      {this.state.rate_details.map(list => (
                        <Fragment>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              Day <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              {" "}
                              {list.day} ({list.period}){" "}
                            </span>
                          </div>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              From - To <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              {" "}
                              {list.from} - {list.to}
                            </span>
                          </div>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              Client Rate <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              £ {list.client_rate} <small> (per hour)</small>
                            </span>
                          </div>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              Employee Rate <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              £ {list.employee_rate} <small> (per hour)</small>
                            </span>
                          </div>
                        </Fragment>
                      ))}
                    </div>
                  </li> */}
                    <li className="invoice-widths">
                      <div className="days-durations">
                        <h5>Worked Details</h5>
                        <div className="days-dura">
                          <span className="inv-heading">
                            {" "}
                            Time <span className="mvr-space"> : </span>{" "}
                          </span>{" "}
                          <span className="inv-desc">
                            {" "}
                            {this.state.started_time} - {this.state.ended_time}{" "}
                          </span>
                        </div>
                        <div className="days-dura">
                          <span className="inv-heading">
                            {" "}
                            Holded <span className="mvr-space"> : </span>{" "}
                          </span>{" "}
                          <span className="inv-desc"> {this.state.holded} Minutes</span>
                        </div>
                        <div className="days-dura">
                          <span className="inv-heading">
                            {" "}
                            Duration <span className="mvr-space"> : </span>{" "}
                          </span>{" "}
                          <span className="inv-desc"> {Math.round(this.state.total_duration)} Minutes</span>
                        </div>
                        {/* <div className="days-dura"><span className="inv-heading">  Duration <span className="mvr-space"> : </span> </span> <span className="inv-desc">  05:00 PM </span></div> */}
                      </div>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Client Rate <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc">£ {this.state.client_rate} </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Employee Rate <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc">£ {this.state.employee_rate} </span>
                    </li>
                    <li>
                      <span className="inv-heading">
                        {" "}
                        Late Booking Fee <span className="mvr-space"> : </span>{" "}
                      </span>{" "}
                      <span className="inv-desc">£ {this.state.late_amount}</span>
                    </li>
                  </ul>
                </Fragment>
              </CardBody>
              <CardFooter>
                {this.props.location.state.Getpage ? (
                  <Button type="button" size="md" color="secondary" title="Back" onClick={this.backTopage}>
                    <i className="fa fa-arrow-left" /> Back
                  </Button>
                ) : (
                  <Link to="/client/invoice">
                    <Button type="button" size="md" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                )}
                {this.state.status === 1 ? (
                  <Button type="button" size="md" color="success" className={"pull-right"} title="Back" onClick={this.approve}>
                    <i className="fa fa-check" /> Approve
                  </Button>
                ) : null}
              </CardFooter>
            </Card>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Invoicedetails;
