import fileDownload from "js-file-download";
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
// import { SingleDatePicker } from "react-dates";
import moment from "moment";
import { Row, Col, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import Pagination from "react-js-pagination";
import Widget02 from "../../Template/Widgets/Widget02";
import { breaklist, timelist } from "../../common/utils";
import "react-select/dist/react-select.css";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "100px"
};

class Invoice extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    timesheet_endtimeview: "",
    timesheet_starttimeview: "",
    status_timesheet: "",
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      status: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    viewtimesheetid: "",
    timesheet: [],
    datesList: [],
    datesvalue: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/employee");
    }
    this.setState({ timelist, breaklist, isLoader: true });
    request({
      url: "/employee/invociedates",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        const datesList = res.response.datelist.map((list, key) => ({
          //value: key,
          label: list.billingcycyle,
          value: `${list.start_date} / ${list.end_date}`
        }));
        datesList.unshift({
          label: `${moment(res.response.lastinvoice).format("DD/MM/YYYY")} - All`,
          value: `${moment(res.response.lastinvoice)} / ${new Date()}`
        })
        this.setState({ datesList, datesvalue: datesList[0] });
        const parts = datesList[0].value.split("/");
        this.setState(state => {
          state.tableOptions.from_date = parts[0];
          state.tableOptions.to_date = parts[1];
        });
        this.populateData();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  populateData() {
    request({
      url: "/employee/invoice/list/groupbyemployee",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            if (earn._id > 1) {
              this.setState({
                earning: earn.employee_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 1) {
              this.setState({
                pending: earn.employee_rate,
                pendingcount: earn.count
              });
            }
            return true;
          });
          this.setState({
            timesheetlists: res.response.result,
            adminlist: res.response.result,
            pages: res.response.fullcount,
            currPage: res.response.length
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["branch", "shifts"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/employee/invoice/export",
      method: "POST"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }

  editpage = id => {
    return this.props.history.push({
      pathname: "/user/invoicegroupview",
      state: { rowid: id, Dates: { from: this.state.tableOptions.from_date, to: this.state.tableOptions.to_date } }
    });
  };
  invoiceStatus = num => {
    if(num){
      this.setState(state => {
        state.tableOptions.status = num;
        this.populateData();
      });
    }else{
        this.setState(state => {
          state.tableOptions.status = "";
          this.populateData();
        });
    }
  };
  changeDates = value => {
    if (value) {
      this.setState({ datesvalue: value.value });
      let parts = value.value.split("/");
      this.setState(state => {
        state.tableOptions.from_date = parts[0];
        state.tableOptions.to_date = parts[1];
      });
      this.populateData();
    }
  };

  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="right-edit-side">
        <div className="form__card card form__card--background float mobile-earnings">
          <ToastContainer position="top-right" autoClose={2500} />
          <Row className="mt-4">
            <Col xs="12" sm="6" lg="2" />
            <Col xs="12" sm="6" lg="4">
              <Widget02
                header={this.state.earning ? <Fragment>£ {this.state.earning}</Fragment> : "0"}
                mainText="Earnings"
                icon="fa fa-check"
                color="primary"
                style = {tStyle} 
                onClick={()=>this.invoiceStatus("")}
              />
            </Col>
            <Col xs="12" sm="6" lg="4">
              <Widget02
                header={this.state.pending ? <Fragment>£ {this.state.pending}</Fragment> : "0"}
                mainText="pending"
                icon="fa fa-clock-o"
                color="warning"
                style = {tStyle} 
                onClick={()=>this.invoiceStatus(1)}
              />
            </Col>
          </Row>
          <Card className="p-0 for-radius">
            <CardHeader className="for-radius">
              <span style = {tStyle} onClick={()=>this.invoiceStatus("")}>Earnings</span>
              <div className="card-actions" style={tStyle} onClick={this.export}>
                <button className="card-actions" style={tStyles}>
                  <i className="fa fa-upload" /> Export
                  <small className="text-muted" />
                </button>
              </div>
            </CardHeader>
            <CardBody>
              <div className="row">
                <div className="new_deisn_warp">
                  <div className="col-lg-5 col-md-5">
                    <Select
                      name="timezone"
                      className="time-zone-style"
                      value={this.state.datesvalue}
                      options={this.state.datesList}
                      onChange={this.changeDates}
                    />
                  </div>
                
                <div className="col-lg-7 col-md-7 pr-0">
                  <InputGroup>
                    <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                      <option>All</option>
                      <option>Name</option>
                    </Input>
                    <Input
                      type="text"
                      ref="search"
                      placeholder="Search"
                      name="search"
                      onChange={e => this.search(e.target.value)}
                      className="rounded-0 col-lg-6"
                    />
                    <Button
                      className="rounded-0"
                      color="primary"
                      id="clear"
                      onClick={() => {
                        ReactDOM.findDOMNode(this.refs.search).value = "";
                        this.search("");
                      }}
                    >
                      <i className="fa fa-remove" />
                    </Button>
                    <UncontrolledTooltip placement="top" target="clear">
                      Clear
                    </UncontrolledTooltip>
                  </InputGroup>
                </div>
              </div>
              </div>
              <div className="table-responsive mt-2">
                <Table hover bordered responsive>
                  <thead>
                    <tr>
                      <th>S.No.</th>
                      {/* <th
                      onClick={() => {
                        this.sort("employee");
                      }}
                    >
                      Employee <i className="fa fa-sort" />
                    </th><th
                      onClick={() => {
                        this.sort("branch");
                      }}
                    >
                      Branch <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                    </th> */}
                      {/* <th
                      onClick={() => {
                        this.sort("location");
                      }}
                    >
                      Location <i className="fa fa-sort" />
                    </th>
                    */}

                      <th
                        onClick={() => {
                          this.sort("client");
                        }}
                      >
                        Client <i className="fa fa-sort" />
                      </th>
                      {/* <th
                      onClick={() => {
                        this.sort("job_type");
                      }}
                    >
                      Role <i className="fa fa-sort" />
                    </th> */}

                      <th
                        onClick={() => {
                          this.sort("shifts");
                        }}
                      >
                        Shifts <i style={{ paddingLeft: "25px" }} className={sorticondef} id="shifts" />
                      </th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.adminlist.length > 0 ? (
                      this.state.adminlist.map((item, i) => (
                        <tr key={i} onClick={e => this.editpage(item)}>
                          <td>{this.state.tableOptions.skip + i + 1}</td>
                          {/* <td>{item.employee[0]}</td>
                        <td>{item.locations[0]}</td>
                        <td>{item.branch[0]}</td>*/}
                          <td>{item.client[0]}</td>

                          {/* <td>{item.job_type[0]}</td> */}
                          <td>{item.count}</td>
                          <td>
                            <div>
                              <button type="button" title="Edit" className="btn view-table btn-sm" id={`edit${i}`} onClick={e => this.editpage(item)}>
                                <i className="fa fa-eye" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                View
                              </UncontrolledTooltip>
                            </div>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr className="text-center">
                        <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                      </tr>
                    )}
                    {this.state.isLoader && <Loader />}
                  </tbody>
                </Table>
              </div>

              <nav className="float-left">
                {/*<Label>Show no.of items : </Label>*/}
                <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                  <option>10</option>
                  <option>25</option>
                  <option>50</option>
                  <option>100</option>
                  <option>200</option>
                </Input>
              </nav>
              <nav className="float-right">
                <div>
                  <Pagination
                    prevPageText="Prev"
                    nextPageText="Next"
                    firstPageText="First"
                    lastPageText="Last"
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.tableOptions.limit}
                    totalItemsCount={this.state.pages}
                    pageRangeDisplayed={this.state.pageRangeDisplayed}
                    onChange={this.paginate}
                  />
                </div>
              </nav>
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

export default Invoice;
