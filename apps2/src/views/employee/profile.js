/* eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import classnames from "classnames";
import React, { Component, Fragment } from "react";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, CardFooter, Col, Input, Label, Nav, NavItem, NavLink, Row, TabContent, TabPane, Popover, PopoverBody } from "reactstrap";
import request, { NodeURL, client } from "../../api/api";
import { timelist } from "../common/utils";
import Select from "react-select";
import "react-select/dist/react-select.css";
import moment from "moment";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import Avatar from "react-avatar-edit";

// var prouser = "../../img/user-profile.png";

class profile extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    email: "",
    status: "",
    role: "",
    code: "",
    job_type: "",
    employee_rate: "",
    client_rate: "",
    number: "",
    dailcountry: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    avatar: "",
    phoneerror: false,
    phonestatus: "",
    avatar_return: "",
    jobtypelist: [],
    activeTab: "1",
    availableactiveTab: "1",
    unavailableactiveTab: "1",
    activemodeledittab: "1",
    holiday_allowance: "",
    joining_date: null,
    final_date: null,
    timelist: [],
    sunstarttime: "",
    sunendtime: "",
    sunavailablestatus: 0,
    monstarttime: "",
    monendtime: "",
    monavailablestatus: 0,
    tuestarttime: "",
    tueendtime: "",
    tueavailablestatus: 0,
    wedstarttime: "",
    wedendtime: "",
    wedavailablestatus: 0,
    thustarttime: "",
    thuendtime: "",
    thuavailablestatus: 0,
    fristarttime: "",
    friendtime: "",
    friavailablestatus: 0,
    satstarttime: "",
    satendtime: "",
    satavailablestatus: 0,
    unavaiablestarttime: "",
    unavaiableendtime: "",
    unavailablestatus: 0,
    from: "",
    to: "",
    unavailabledate: "",
    worklist: [],
    locations: "",
    timeoffdata: [],
    dates: [],
    stausactivesun: false,
    stausactivemon: false,
    stausactivetue: false,
    stausactivewed: false,
    stausactivethu: false,
    stausactivefri: false,
    stausactivesat: false,
    job_role_error: false,
    location_error: false,
    popoverDateOpen: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidMount() {
    const token = this.props.token_role;

    if (!token || (token && (!token.username || token.role !== "employee"))) {
      return this.props.history.replace("/login");
    }
    this.setState({ timelist });
    request({
      url: "/employee/agencydetails",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          worklist: res.response.result[0].locations || {},
          jobtypelist: res.response.result[0].jobtypes || {}
        });
        const worklist = this.state.worklist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        const jobtypelist = this.state.jobtypelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          worklist,
          jobtypelist
        });
      } else if (res.status === 0) {
        let toastId = "jobtypelistResponse";
        if (!toast.isActive(toastId)) {
          toastId = toast.error(res.response);
        }
      }
    });

    request({
      url: "/employee/profile",
      method: "POST",
      data: token.username
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result !== undefined) {
          const data = res.response.result || {};
          this.setState({ adminlist: data });
          if (data) {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            let timeoffdata;
            if (data.timeoff) {
              timeoffdata = data.timeoff.map((time, key) => {
                const timefromhr = time.from / 3600;
                const timefromsplithr = timefromhr.toString().split(".");
                const timefromsec = timefromsplithr[1];
                const timefrommins = timefromsec === undefined ? "00" : (+"0" + "." + timefromsec) * 60;
                const timehr = num.indexOf(timefromsplithr[0]) === -1 ? timefromsplithr[0] + ":" + timefrommins : +"0" + "" + timefromsplithr[0] + ":" + timefrommins;
                const timetohr = time.to / 3600;
                const timetosplithr = timetohr.toString().split(".");
                const timetosec = timetosplithr[1];
                const timetomins = timetosec === undefined ? "00" : (+"0" + "." + timetosec) * 60;
                const timeofftohr = num.indexOf(timetosplithr[0]) === -1 ? timetosplithr[0] + ":" + timetomins : +"0" + "" + timetosplithr[0] + ":" + timetomins;
                return {
                  date: moment(time.date).format("DD-MM-YYYY"),
                  dates: time.date,
                  day: time.day,
                  from: timehr,
                  to: timeofftohr,
                  from_sec: time.from,
                  to_sec: time.to
                };
              });
            }
            this.setState({
              id: data._id,
              username: data.username,
              name: data.name,
              email: data.email,
              job_type: data.job_type,
              // holiday_allowance: data.holiday_allowance,
              locations: data.locations,
              // employee_rate: data.employee_rate,
              status: data.status,
              number: data.phone.number,
              dailcountry: data.phone ? data.phone.dailcountry : "gb",
              code: data.phone.code,
              line1: data.address.line1,
              line2: data.address.line2,
              city: data.address.city,
              state: data.address.state,
              country: data.address.country,
              zipcode: data.address.zipcode,
              lat: data.address.lat,
              lon: data.address.lon,
              formatted_address: data.address.formatted_address,
              avatar_return: data.avatar,
              // avatarName: data.avatar.substr(24, 30),
              sunstarttime: data.available[0].from,
              sunendtime: data.available[0].to,
              sunavailablestatus: data.available[0].status,
              monstarttime: data.available[1].from,
              monendtime: data.available[1].to,
              monavailablestatus: data.available[1].status,
              tuestarttime: data.available[2].from,
              tueendtime: data.available[2].to,
              tueavailablestatus: data.available[2].status,
              wedstarttime: data.available[3].from,
              wedendtime: data.available[3].to,
              wedavailablestatus: data.available[3].status,
              thustarttime: data.available[4].from,
              thuendtime: data.available[4].to,
              thuavailablestatus: data.available[4].status,
              fristarttime: data.available[5].from,
              friendtime: data.available[5].to,
              friavailablestatus: data.available[5].status,
              satstarttime: data.available[6].from,
              satendtime: data.available[6].to,
              satavailablestatus: data.available[6].status,
              // timeoffdata: data.timeoff,
              form_status: data && data.onlineform && data.onlineform.form_status,
              stausactivesun: data.available[0].status === 1? true: false,
              stausactivemon: data.available[1].status === 1?  true: false,
              stausactivetue: data.available[2].status === 1? true:false,
              stausactivewed: data.available[3].status === 1? true:false,
              stausactivethu: data.available[4].status === 1?true:false,
              stausactivefri: data.available[5].status === 1? true :false,
              stausactivesat: data.available[6].status === 1? true :false,
              timeoffdata
            });
          }
        }
      }
    });
  }
  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handleAddedu = () => {
    if (this.state.dates.length < 0 || this.state.unavailablefrom === "" || this.state.unavailableto === "") {
      let toastId = "unavail5645546556";
      if (!toast.isActive(toastId)) {
        toastId = toast.error("Please Fill All Fields");
      }
    } else {
      const datesCond = this.state.dates.map(list => moment(list).format("DD-MM-YYYY"));
      const Filter_Cond = this.state.timeoffdata && this.state.timeoffdata.filter(list => datesCond.indexOf(list.date) !== -1).filter(list => this.state.unavailablefrom === list.from_sec && this.state.unavailableto === list.to_sec);
      if (Filter_Cond && Filter_Cond.length > 0) {
        toast.error("Already Added On Same Date & Time");
      } else {
        let toastId_01 = "unavailable_Save_001";
        toastId_01 = toast.info("Please Wait......", { autoClose: false });
        const UnavailableData =
          this.state.dates &&
          this.state.dates.map(list => ({
            date: list,
            from: this.state.unavailablefrom,
            to: this.state.unavailableto
          }));
        request({
          url: "/employee/update_availabilty",
          method: "POST",
          data: { UnavailableData }
        })
          .then(res => {
            if (res.status === 1) {
              const toastId = "unavail";
              if (!toast.isActive(toastId)) {
                toast.update(toastId_01, { render: "Added!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
              }
              this.componentDidMount();
              this.setState({
                dates: [],
                unavailablefrom: "",
                unavailableto: "",
                activeTab: "3"
              });
            } else if (res.status === 0) {
              toast.update(toastId_01, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
            }
          })
          .catch(error => {
            toast.error(error);
          });
      }
    }
  };
  deletetimeoff = index => {
    request({
      url: "/employee/delete_availabilty",
      method: "POST",
      data: {
        index: index
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Deleted!");
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  selectstarttimeChange = selecttime => value => {
    if (selecttime === "sunstarttime") {
      if (value) {
        this.setState({ sunstarttime: value.value });
      } else {
        this.setState({ sunstarttime: "" });
      }
    }
    if (selecttime === "sunendtime") {
      if (value) {
        this.setState({ sunendtime: value.value });
      } else {
        this.setState({ sunendtime: "" });
      }
    }
    if (selecttime === "monstarttime") {
      if (value) {
        this.setState({ monstarttime: value.value });
      } else {
        this.setState({ monstarttime: "" });
      }
    }
    if (selecttime === "monendtime") {
      if (value) {
        this.setState({ monendtime: value.value });
      } else {
        this.setState({ monendtime: "" });
      }
    }
    if (selecttime === "tuestarttime") {
      if (value) {
        this.setState({ tuestarttime: value.value });
      } else {
        this.setState({ tuestarttime: "" });
      }
    }
    if (selecttime === "tueendtime") {
      if (value) {
        this.setState({ tueendtime: value.value });
      } else {
        this.setState({ tueendtime: "" });
      }
    }
    if (selecttime === "wedstarttime") {
      if (value) {
        this.setState({ wedstarttime: value.value });
      } else {
        this.setState({ wedstarttime: "" });
      }
    }
    if (selecttime === "wedendtime") {
      if (value) {
        this.setState({ wedendtime: value.value });
      } else {
        this.setState({ wedendtime: "" });
      }
    }
    if (selecttime === "thustarttime") {
      if (value) {
        this.setState({ thustarttime: value.value });
      } else {
        this.setState({ thustarttime: "" });
      }
    }
    if (selecttime === "thuendtime") {
      if (value) {
        this.setState({ thuendtime: value.value });
      } else {
        this.setState({ thuendtime: "" });
      }
    }
    if (selecttime === "fristarttime") {
      if (value) {
        this.setState({ fristarttime: value.value });
      } else {
        this.setState({ fristarttime: "" });
      }
    }
    if (selecttime === "friendtime") {
      if (value) {
        this.setState({ friendtime: value.value });
      } else {
        this.setState({ friendtime: "" });
      }
    }
    if (selecttime === "satstarttime") {
      if (value) {
        this.setState({ satstarttime: value.value });
      } else {
        this.setState({ satstarttime: "" });
      }
    }
    if (selecttime === "satendtime") {
      if (value) {
        this.setState({ satendtime: value.value });
      } else {
        this.setState({ satendtime: "" });
      }
    }
    if (selecttime === "unavailablefrom") {
      if (value) {
        this.setState({ unavailablefrom: value.value });
      } else {
        this.setState({ unavailablefrom: "" });
      }
    }
    if (selecttime === "unavailableto") {
      if (value) {
        this.setState({ unavailableto: value.value });
      } else {
        this.setState({ unavailableto: "" });
      }
    }
  };
  availablestatusChangesessun = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivesun: e.target.checked,
      sunavailablestatus: value
    });
  };
  availablestatusChangesesmon = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivemon: e.target.checked,
      monavailablestatus: value
    });
  };
  availablestatusChangesestue = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivetue: e.target.checked,
      tueavailablestatus: value
    });
  };
  availablestatusChangeseswed = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivewed: e.target.checked,
      wedavailablestatus: value
    });
  };
  availablestatusChangesesthu = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivethu: e.target.checked,
      thuavailablestatus: value
    });
  };
  availablestatusChangesesfri = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivefri: e.target.checked,
      friavailablestatus: value
    });
  };
  availablestatusChangesessat = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivesat: e.target.checked,
      satavailablestatus: value
    });
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
      toast.error("Enter Phone Number");
    } else if (!this.state.locations || this.state.locations === undefined || this.state.locations.length === 0) {
      this.setState({
        location_error: true
      });
      toast.error("Select Location");
    } else if (this.state.job_type === "") {
      this.setState({
        job_role_error: true
      });
      toast.error("Select Job Role");
    } else {
      client.defaults.responseType = "json";
      const data = new FormData();
      this.state.locations.map((item, i) => {
        if (item.value) {
          this.setState({ locationsValue: true });
        }
        return true;
      });
      if (this.state.locationsValue) {
        this.state.locations.map((item, i) => {
          if (item.value) {
            data.append("locations", item.value);
          }
          return true;
        });
      } else {
        this.state.locations.map((item, i) => {
          if (item) {
            data.append("locations", item);
          }
          return true;
        });
      }
      this.state.job_type.map((item, i) => {
        data.append("job_type", item);
        return true;
      });
      data.append("_id", this.state.id);
      data.append("username", this.state.username);
      if (this.state.password !== "") {
        data.append("password", this.state.password);
      }
      if (this.state.confirm_password !== "") {
        data.append("confirm_password", this.state.confirm_password);
      }
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      data.append("status", this.state.status);
      // data.append("job_type", this.state.job_type);
      // data.append("holiday_allowance", this.state.holiday_allowance);
      // data.append("employee_rate", this.state.employee_rate);
      // data.append("client_rate", this.state.client_rate);
      // data.append("locations", this.state.locations);
      // data.append("joining_date", this.state.joining_date);
      // data.append("final_date", this.state.final_date);
      data.append("available[0][day]", "sun");
      data.append("available[0][from]", this.state.sunstarttime || "");
      data.append("available[0][to]", this.state.sunendtime || "");
      data.append("available[0][status]", this.state.sunavailablestatus || 0);
      data.append("available[1][day]", "mon");
      data.append("available[1][from]", this.state.monstarttime || "");
      data.append("available[1][to]", this.state.monendtime || "");
      data.append("available[1][status]", this.state.monavailablestatus || 0);
      data.append("available[2][day]", "tue");
      data.append("available[2][from]", this.state.tuestarttime || "");
      data.append("available[2][to]", this.state.tueendtime || "");
      data.append("available[2][status]", this.state.tueavailablestatus || 0);
      data.append("available[3][day]", "wed");
      data.append("available[3][from]", this.state.wedstarttime || "");
      data.append("available[3][to]", this.state.wedendtime || "");
      data.append("available[3][status]", this.state.wedavailablestatus || 0);
      data.append("available[4][day]", "thu");
      data.append("available[4][from]", this.state.thustarttime || "");
      data.append("available[4][to]", this.state.thuendtime || "");
      data.append("available[4][status]", this.state.thuavailablestatus || 0);
      data.append("available[5][day]", "fri");
      data.append("available[5][from]", this.state.fristarttime || "");
      data.append("available[5][to]", this.state.friendtime || "");
      data.append("available[5][status]", this.state.friavailablestatus || 0);
      data.append("available[6][day]", "sat");
      data.append("available[6][from]", this.state.satstarttime || "");
      data.append("available[6][to]", this.state.satendtime || "");
      data.append("available[6][status]", this.state.satavailablestatus || 0);
      data.append("_id", this.state.id);

      request({
        url: "/employee/profile/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            // this.saveuser();
            let toastId = "profileEditSaved";
            if (!toast.isActive(toastId)) {
              toastId = toast.success("Updated");
            }
            this.componentDidMount();
          } else if (res.status === 0) {
            let toastId = "profileEditResponse";
            if (!toast.isActive(toastId)) {
              toastId = toast.error(res.response);
            }
          }
        })
        .catch(error => {
          let toastId = "profileEditError";
          if (!toast.isActive(toastId)) {
            toastId = toast.error(error);
          }
        });
    }
  };
  saveuser = () => {
    return this.props.history.push("/agency/employeelist"); // , this.flash.nodify();
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2,
      phonestatus: status
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarName: evt.target.files[0].name
    });
  };
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  availabletoggle(tab) {
    if (this.state.availableactiveTab !== tab) {
      this.setState({
        availableactiveTab: tab
      });
    }
  }
  unavailabletoggle(tab) {
    if (this.state.unavailableactiveTab !== tab) {
      this.setState({
        unavailableactiveTab: tab
      });
    }
  }
  togglemodeledit = tab => {
    if (this.state.activemodeledittab !== tab) {
      this.setState({
        activemodeledittab: tab
      });
    }
  };
  locationsselect = value => {
    if (value) {
      this.setState({ locations: value });
    }
  };
  jobtypeselect = value => {
    if (value) {
      const val = value.map(list => list.value);
      this.setState({ job_type: val });
    }
  };
  ApplytoallAvailability = () => {
    if (this.state.stausactivesun === "" && this.state.sunstarttime === "" && this.state.sunendtime === "") {
      toast.error("Please fill all fields");
    } else {
      this.setState({
        monstarttime: this.state.sunstarttime,
        tuestarttime: this.state.sunstarttime,
        wedstarttime: this.state.sunstarttime,
        thustarttime: this.state.sunstarttime,
        fristarttime: this.state.sunstarttime,
        satstarttime: this.state.sunstarttime,
        monendtime: this.state.sunendtime,
        tueendtime: this.state.sunendtime,
        wedendtime: this.state.sunendtime,
        thuendtime: this.state.sunendtime,
        friendtime: this.state.sunendtime,
        satendtime: this.state.sunendtime,
        stausactivemon: this.state.stausactivesun,
        monavailablestatus: this.state.sunavailablestatus,
        stausactivetue: this.state.stausactivesun,
        tueavailablestatus: this.state.sunavailablestatus,
        stausactivewed: this.state.stausactivesun,
        wedavailablestatus: this.state.sunavailablestatus,
        stausactivethu: this.state.stausactivesun,
        thuavailablestatus: this.state.sunavailablestatus,
        stausactivefri: this.state.stausactivesun,
        friavailablestatus: this.state.sunavailablestatus,
        stausactivesat: this.state.stausactivesun,
        satavailablestatus: this.state.sunavailablestatus
      });
      toast.success("Applied");
    }
  };
  toggleDate = () => {
    this.setState({
      popoverDateOpen: !this.state.popoverDateOpen
    });
  };
  handleDayClick = (day, { selected, highlighted }) => {
    if (selected) {
      const selectedIndex = this.state.dates.findIndex(selectedDay => DateUtils.isSameDay(selectedDay, day));
      this.state.dates.splice(selectedIndex, 1);
    } else {
      this.state.dates.push(day);
    }
    this.setState({ dates: this.state.dates });
  };
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  render() {
    const highl = this.state.timeoffdata && this.state.timeoffdata.map(list => new Date(list.dates));
    const modifiers = {
      highlighted: highl
    };
    const { form_status } = this.state || {};
    return (
      <Fragment>
        <ToastContainer position="top-right" autoClose={2500} />
        {/* <!--Form--> */}
        <div className="right-edit-side ">
          <div className="form__card card form__card--background float new-forms-profile">
            <div className="form__wrap extra">
              {/* <AvForm onValidSubmit={this.OnFormSubmit}> */}
              <Nav tabs>
                {form_status >= 5 ? (
                  <>
                    <NavItem>
                      <NavLink
                          className={classnames({
                            active: this.state.activemodeledittab === "1"
                          })}
                          onClick={() => {
                            this.togglemodeledit("1");
                          }}
                      >
                        Profile
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activemodeledittab === "2"
                        })}
                        onClick={() => {
                          this.togglemodeledit("2");
                        }}
                      >
                        Availablity
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activemodeledittab === "3"
                        })}
                        onClick={() => {
                          this.togglemodeledit("3");
                        }}
                      >
                        Unavailablity
                      </NavLink>
                    </NavItem>
                  </>
                ) : null}
              </Nav>
              <TabContent activeTab={this.state.activemodeledittab}>
                <TabPane tabId="1">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    {/* <div> */}
                    <h3 className="verify-headings">Account</h3>
                    <Row>
                      <div className="edit-changes">
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>Username</Label>
                            <AvInput className="form-control fname" type="text" name="username" placeholder="Enter Username" onChange={this.onChange} value={this.state.username} disabled autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>First Name</Label>
                            <AvInput className="form-control fname" type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Phone</Label>
                          {this.state && this.state.dailcountry ? (
                            <IntlTelInput
                              style={{ width: "100%" }}
                              defaultCountry={this.state.dailcountry}
                              utilsScript={libphonenumber}
                              css={["intl-tel-input", "form-control"]}
                              onPhoneNumberChange={this.handler}
                              value={this.state.number}
                            />
                          ) : null}
                          {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                        </Col>
                      </div>
                    </Row>
                    <Row>
                      <div className="edit-changes">
                        <Col xs="6" md="4">
                          <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                        </Col>
                        {this.state.propreview ? (
                          <Col xs="6" md="4">
                            <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                          </Col>
                        ) : null}
                        <Col xs="6" md="4" className="mov-end">
                          <div className="multiple-gal">
                            <img
                              width="180px"
                              height="180px"
                              src={NodeURL + "/" + this.state.avatar_return}
                              alt="Profile"
                              onError={() => {
                                this.setState({ avatar_return: "../../img/user-profile.png" });
                              }}
                            />
                          </div>
                        </Col>
                      </div>
                    </Row>
                    <br />
                    <h3 className="verify-headings">Login Details</h3>
                    <Row>
                      <div className="edit-changes">
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>Email</Label>
                            <AvInput className="form-control fname" name="email" type="email" placeholder="Enter Email" disabled onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>Password</Label>
                            <AvInput className="form-control fname" type="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password} autoComplete="off" />
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>Confirm Password</Label>
                            <AvInput
                              className="form-control fname"
                              type="password"
                              name="confirm_password"
                              placeholder="Re-Enter Password"
                              onChange={this.onChange}
                              value={this.state.confirm_password}
                              validate={{ match: { value: "password" } }}
                              autoComplete="off"
                            />
                            <AvFeedback>Match Password!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </div>
                    </Row>
                    <h3 className="verify-headings">Work Details</h3>
                    <Row>
                      <div className="edit-changes">
                        <Col xs="12" md="6">
                          <Label>Area</Label>
                          <Select name="locations" value={this.state.locations} options={this.state.worklist} onChange={this.locationsselect} multi disabled />
                          {this.state.location_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="6">
                          <Label>Job Role</Label>
                          <Select name="job_type" value={this.state.job_type} options={this.state.jobtypelist} onChange={this.jobtypeselect} multi disabled />
                          {this.state.job_role_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        {/* <Col xs="12" md="4">
                        <AvGroup className="form-group left">
                          <Label>Employee Rate</Label>
                          <AvInput
                            className="form-control fname"
                            type="text"
                            name="employee_rate"
                            placeholder="Enter Employee Rate"
                            onChange={this.onChange}
                            value={this.state.employee_rate}
                            required
                            autoComplete="employee_rate"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col> */}
                      </div>
                    </Row>
                    <h3 className="verify-headings">Address</h3>
                    
                    <Row>
                        <Col xs="12" md="4">
						
								 <div className="edit-changes">
							  <Label>Search address</Label>
							</div>
                         
                          <PlacesAutocomplete value={this.state.formatted_address}>
                            {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                              <div>
                                <input
                                  {...getInputProps({
                                    placeholder: "Search Places",
                                    className: "form-control serach-place"
                                  })}
                                />
                                <div className="autocomplete-dropdown-container absolute">
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                          backgroundColor: "#fafafa",
                                          cursor: "pointer"
                                        }
                                      : {
                                          backgroundColor: "#ffffff",
                                          cursor: "pointer"
                                        };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                          </PlacesAutocomplete>
						   
                        </Col>
                     
                        <Col xs="12" md="4">
						<div className="edit-changes">
                          <AvGroup className="form-group left">
                            <Label>Line1</Label>
                            <AvInput className="form-control fname" type="text" name="line1" placeholder="Enter Line1" onChange={this.onChange} value={this.state.line1} required disabled autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
						  </div>
                        </Col>
                        <Col xs="12" md="4">
						<div className="edit-changes">
                          <AvGroup className="form-group left">
                            <Label>City/Town</Label>
                            <AvInput className="form-control fname" type="text" name="line2" placeholder="Enter City/Town" onChange={this.onChange} value={this.state.line2} required disabled autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
						    </div>
                        </Col>

                        {/* <Col xs="12" md="4">
                        <AvGroup className="form-group left">
                          <Label>City</Label>
                          <AvInput
                            className="form-control fname"
                            type="text"
                            name="city"
                            placeholder="Enter City"
                            onChange={this.onChange}
                            value={this.state.city}
                            required
                            autoComplete="name"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col> */}
                      
                    </Row>
                    <Row>
                      <div className="edit-changes">
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>State/Region</Label>
                            <AvInput className="form-control fname" type="text" name="state" placeholder="Enter State " onChange={this.onChange} value={this.state.state} required disabled autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>Country</Label>
                            <AvInput className="form-control fname" type="text" name="country" placeholder="Enter Country" onChange={this.onChange} value={this.state.country} required disabled autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup className="form-group left">
                            <Label>Postal Code</Label>
                            <AvInput className="form-control fname" type="text" name="zipcode" placeholder="Enter Zipcode" onChange={this.onChange} value={this.state.zipcode} required disabled autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </div>
                    </Row>
                    <CardFooter>
                      <Button
                        type="submit"
                        className="site-btn site-btn--accent"
                        onClick={() => {
                          this.phonefield(this.state.number);
                        }}
                      >
                        <i className="fa fa-dot-circle-o" title="Save Details" /> Save Details
                      </Button>
                    </CardFooter>
                  </AvForm>
                </TabPane>
                <TabPane tabId="2" className="TabpaneList">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <Col xs="12" md="12" className="mb-4">
                      <Nav tabs className="week-subs">
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "1"
                            })}
                            onClick={() => {
                              this.availabletoggle("1");
                            }}
                          >
                            {" "}
                            Sun 
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "2"
                            })}
                            onClick={() => {
                              this.availabletoggle("2");
                            }}
                          >
                            Mon
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "3"
                            })}
                            onClick={() => {
                              this.availabletoggle("3");
                            }}
                          >
                            Tue
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "4"
                            })}
                            onClick={() => {
                              this.availabletoggle("4");
                            }}
                          >
                            Wed
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "5"
                            })}
                            onClick={() => {
                              this.availabletoggle("5");
                            }}
                          >
                            Thu
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "6"
                            })}
                            onClick={() => {
                              this.availabletoggle("6");
                            }}
                          >
                            Fri
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: this.state.availableactiveTab === "7"
                            })}
                            onClick={() => {
                              this.availabletoggle("7");
                            }}
                          >
                            Sat
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent className="nonseq-pages" activeTab={this.state.availableactiveTab}>
                        <TabPane tabId="1">
                          <Row>
                           
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="sunstarttime" value={this.state.sunstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("sunstarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="sunendtime" value={this.state.sunendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("sunendtime")} />
                            </Col>
							 <Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivesun} onChange={this.availablestatusChangesessun} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                            <Col xs="12" md="3" className="mt-4">
                              <button type="button" className="btn btn-info apl-all" onClick={this.ApplytoallAvailability}>
                                Apply to all
                              </button>
                            </Col>
							
                          </Row>
                        </TabPane>
                        <TabPane tabId="2">
                          <Row>
                           
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="monstarttime" value={this.state.monstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("monstarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="monendtime" value={this.state.monendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("monendtime")} />
                            </Col>
							 <Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivemon} onChange={this.availablestatusChangesesmon} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId="3">
                          <Row>
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="tuestarttime" value={this.state.tuestarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("tuestarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="tueendtime" value={this.state.tueendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("tueendtime")} />
                            </Col>
							 <Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivetue} onChange={this.availablestatusChangesestue} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId="4">
                          <Row>
                            
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="wedstarttime" value={this.state.wedstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("wedstarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="wedendtime" value={this.state.wedendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("wedendtime")} />
                            </Col>
							<Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivewed} onChange={this.availablestatusChangeseswed} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId="5">
                          <Row>
                           
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="thustarttime" value={this.state.thustarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("thustarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="thuendtime" value={this.state.thuendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("thuendtime")} />
                            </Col>
							 <Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivethu} onChange={this.availablestatusChangesesthu} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId="6">
                          <Row>
                            
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="fristarttime" value={this.state.fristarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("fristarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="friendtime" value={this.state.friendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("friendtime")} />
                            </Col>
							<Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivefri} onChange={this.availablestatusChangesesfri} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId="7">
                          <Row>
                           
                            <Col xs="12" md="3">
                              <Label for="from">Start Time</Label>
                              <Select name="satstarttime" value={this.state.satstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("satstarttime")} />
                            </Col>
                            <Col xs="12" md="3">
                              <Label for="from">End Time</Label>
                              <Select name="satendtime" value={this.state.satendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("satendtime")} />
                            </Col>
							 <Col xs="12" md="3">
                              <div>
                                <label>Status</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.stausactivesat} onChange={this.availablestatusChangesessat} />
                                <span className="switch-label" data-on="active" data-off="inactive" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          </Row>
                        </TabPane>
                      </TabContent>
                    </Col>
                    <CardFooter>
                      <Button type="submit" className="site-btn site-btn--accent">
                        <i
                          className="fa fa-dot-circle-o"
                          onClick={() => {
                            this.phonefield(this.state.number);
                          }}
                          title="Save Details"
                        />{" "}
                        Save Details
                      </Button>
                    </CardFooter>
                  </AvForm>
                </TabPane>
                <TabPane tabId="3">
                  <AvForm onValidSubmit={this.OnFormSubmit} className="comm-border">
                    <Row>
                      <Col xs="12" md="3">
                        <Label>Select Date</Label>
                        <Input type="text" name="date" id="PopoverDate" placeholder="Date" onClick={this.toggleDate} value={this.state.dates.map(list => moment(list).format("DD-MM-YYYY"))} />
                        <Popover placement="bottom" isOpen={this.state.popoverDateOpen} target="PopoverDate" toggle={this.toggleDate}>
                          <PopoverBody className="emp-date">
                            <DayPicker selectedDays={this.state.dates} disabledDays={{ before: new Date() }} modifiers={modifiers} onDayClick={this.handleDayClick} />
                          </PopoverBody>
                        </Popover>
                      </Col>
                      <Col xs="12" md="3">
                        <Label for="from">Start Time</Label>
                        <Select name="from" value={this.state.unavailablefrom} options={this.state.timelist} onChange={this.selectstarttimeChange("unavailablefrom")} />
                      </Col>
                      <Col xs="12" md="3">
                        <Label for="to">End Time</Label>
                        <Select name="to" value={this.state.unavailableto} options={this.state.timelist} onChange={this.selectstarttimeChange("unavailableto")} />
                      </Col>
                      <Col xs="12" md="2">
                        <Button onClick={this.handleAddedu} color="success" className="mt-5 additionalAdd pro-added">
                          <i className="fa fa-plus" />
                        </Button>
                      </Col>
                    </Row>
                  </AvForm>
                  {this.state.timeoffdata.map((timeoff, key) => {
                    return (
                      <Row className="mt-1 comm-border text-center" key={key}>
                        <div className="out-order">
                          <Col xs="12" md="4">
                            <div className="date-inputs">
                              <p>
                                <b>{timeoff.date}</b>
                              </p>
                            </div>
                          </Col>
                          <Col xs="12" md="3">
                            <div className="date-inputs">
                              <p>{timeoff.from}</p>
                            </div>
                          </Col>
                          <Col xs="12" md="3">
                            <div className="date-inputs">
                              <p>{timeoff.to}</p>
                            </div>
                          </Col>
                          <Col xs="12" md="2">
                            <div className="date-inputs">
                              <button onClick={item => this.deletetimeoff(key)} className="btn btn-xs btn-danger">
                                <i className="fa fa-minus" />
                              </button>
                            </div>
                          </Col>
                        </div>
                      </Row>
                    );
                  })}
                </TabPane>
              </TabContent>
            </div>
          </div>
        </div>

        {/* <!--Form--> */}
      </Fragment>
    );
  }
}

export default profile;
