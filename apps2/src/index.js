import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import { ReactTitle } from "react-meta-tags";
// Styles
// Import Flag Icons Set
import "./../node_modules/flag-icon-css/css/flag-icon.min.css";
// Import Font Awesome Icons Set
import "./../node_modules/font-awesome/css/font-awesome.min.css";
// Import Simple Line Icons Set
import "./../node_modules/simple-line-icons/css/simple-line-icons.css";
// Import Main styles for this application
import "./scss/style.scss";
// import "./scss/custom.css";
// Temp fix for reactstrap
import "./scss/core/_dropdown-menu-right.scss";
import "./scss/skyblue.scss";
import "./scss/responsive.scss";

import Routing from "./containers/routing";
import home from "./views/website/home";
import login from "./views/employee/login";
import registration from "./views/employee/registration";
import aboutus from "./views/website/aboutus";
import contactus from "./views/website/contactus";
import forgotpassword from "./views/employee/forgotpassword";
import changepassword from "./views/employee/changepassword";
import myaccounts from "./views/employee/myaccounts";
import serviceexpertise from "./views/website/serviceexpertise";
import Referenceform from "./views/employee/referenceform";
import Header from "./views/common/header";

ReactDOM.render(
  <>
    <ReactTitle title="Agency Portal" />
    <BrowserRouter>
      <Switch>
        <Route exact path="/header" name="home" component={Header} />
        <Route exact path="/" name="home" component={home} />
        <Route exact path="/login" name="login" component={login} />
        <Route exact path="/register" name="registration" component={registration} />
        <Route exact path="/aboutus" name="aboutus" component={aboutus} />
        <Route exact path="/contactus" name="contactus" component={contactus} />
        <Route exact path="/forgotpassword" name="forgotpassword" component={forgotpassword} />
        <Route exact path="/resetpassword/:id/:resetcode" name="changepassword" component={changepassword} />
        <Route exact path="/myaccounts" name="myaccounts" component={myaccounts} />
        <Route exact path="/serviceexpertise" name="serviceexpertise" component={serviceexpertise} />
        <Route exact path="/referenceform/:referencefor/:username" name="referenceform" component={Referenceform} />
        <Routing />
      </Switch>
    </BrowserRouter>
  </>,
  document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
