import React, { Component, Fragment } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
// import { Container } from "reactstrap";

import Header from "../views/common/header";
import Footer from "../views/common/footer";
// import Sidebar from "../views/common/sidebar";

import Profile from "../views/employee/profile";
import shiftslist from "../views/employee/shift/shiftslist";
import shiftview from "../views/employee/shift/shiftview";
import Calenderview from "../views/employee/shift/shiftcalender";
// import Notifications from "../views/employee/settings/notifications";
import Showprofile from "../views/employee/showprofile";
import Yourdetails from "../views/employee/yourdetails";
import TimesheetList from "../views/employee/timesheets/timesheetlist";
import TimesheetDetails from "../views/employee/timesheets/timesheetdetails";
import InvoiceList from "../views/employee/invoice/clientlist";
import InvoiceGroupView from "../views/employee/invoice/invoicegroup";
import InvoiceDetails from "../views/employee/timesheets/timesheetdetails";
import Dashboard from "../views/employee/dashboard";
import onlineform from "../views/employee/onlineform";

import Gettoken from "../api/gettoken";
import Context from "../api/context";
import jwt_decode from "jwt-decode";
// import Page404 from "../views/Template/Pages/Page404";
// import request from "../api/api";
// import { toast } from "react-toastify";

class Routing extends Component {
  state = {
    token_roleid: "",
    form_recruitement_module: ""
  };
  componentDidMount() {
    window.scrollTo(0, 0);

    this.setState({
      token_roleid: this.props.theme.Authtoken
    });
    if (this.props.theme.Authtoken) {
      this.setState({
        tokenrole: this.props.theme.Authtoken.role
      });
    }
    // request({
    //   url: "/employee/profile",
    //   method: "POST",
    //   data: this.props.theme.Authtoken.username
    // }).then(res => {
    //   if (res.status === 1) {
    //     this.setState({
    //       form_recruitement_module: res.response.result && res.response.result.onlineform && res.response.result.onlineform.recruitment_module
    //     });
    //   } else if (res.status === 0) {
    //     let toastId = "profileResponse";
    //     if (!toast.isActive(toastId)) {
    //       toastId = toast.info(res.response);
    //     }
    //   }
    // });
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }
  render() {
    return (
      <Fragment>
        {this.state.tokenrole === "employee" ? (
          <Fragment>
            <Header />
            <section className="section section--last section--top-space small-width">
              <div className="evenspace">
                <div className="row" />
                <div className=" form pro-covers">
                  {/*<Sidebar />*/}
                  <Switch>
                    <PrivateRoute path="/user/profile" component={Profile} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/shiftslist" component={shiftslist} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/shiftview" component={shiftview} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/showprofile" component={Showprofile} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/calenderview" component={Calenderview} value={this.state.token_roleid} />
                    {/* <PrivateRoute path="/user/settings/notifications" component={Notifications} value={this.state.token_roleid} /> */}
                    <PrivateRoute path="/user/timesheets" component={TimesheetList} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/timesheetdetails" component={TimesheetDetails} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/invoicelist" component={InvoiceList} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/invoicegroupview" component={InvoiceGroupView} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/invoicedetails" component={InvoiceDetails} value={this.state.token_roleid} />
                    <PrivateRoute path="/user/dashboard" component={Dashboard} value={this.state.token_roleid} />
                    {this.state.token_roleid && this.state.token_roleid.recruitment_module === 1 ? (
                     <>
                         <PrivateRoute path="/user/onlineform" component={onlineform} value={this.state.token_roleid} />
                         <PrivateRoute path="/user/yourdetails" component={Yourdetails} value={this.state.token_roleid} />
                     </>
                    ) : null}
                      {/*<PrivateRoute component={Page404} />*/}
                  </Switch>
                </div>
              </div>
              {/* <img alt="" className="section__img" src="../img/img_backgroud_footer.png" /> */}
            </section>
            <Footer />
          </Fragment>
        ) : null}
      </Fragment>
    );
  }
}
const PrivateRoute = ({ component: Component, value, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const tokens = localStorage.getItem("APUSE");
      if (tokens) {
        var token = jwt_decode(tokens);
      } else {
        token = "not available";
      }
      if (token) {
        return <Component {...props} token_role={value} />;
      } else {
        return <Redirect to={{ pathname: "/", state: { from: props.location } }} />;
      }
    }}
  />
);
// export default Routing;
export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Routing {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
