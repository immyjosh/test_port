import axios from "axios";

const AUTH_TOKEN = localStorage.getItem("APUSE");
export const NodeURL = "http://localhost:3006";
// export const NodeURL = "http://agency.casperon.co";
export const client = axios.create({
  baseURL: NodeURL
});
client.defaults.headers.common["Authorization"] = AUTH_TOKEN;
client.defaults.responseType = "json";
const request = options => {
  const onSuccess = response => {
    return response.data;
  };
  const onError = error => {
    if (error.response) {
      // Request was made but server responded with something other than 2xx
    } else {
      // Something else happened while setting up the request triggered the error
    }
    return Promise.reject(error.response || error.message);
  };
  return client(options)
    .then(onSuccess)
    .catch(onError);
};
export default request;
