/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React from "react";
import Context from "./context";
import jwt_decode from "jwt-decode";

class Gettoken extends React.Component {
  render() {
    const token = localStorage.getItem("APUSE");
      if(token){
          var decoded = jwt_decode(token);
      }else{
          decoded = "No Token Available";
      }
    return (
      <Context.Provider value={{ Authtoken: decoded }}>
        {this.props.children}
      </Context.Provider>
    );
  }
}
export default Gettoken;
