const db = require("../../model/mongodb");
const moment = require("moment");
const mongoose = require("mongoose");
const events = require("events");
const event = new events.EventEmitter();

const Cycle_Dates = (agency, type, schema) => {
  db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, settings) => {
    if (err) {
      console.log("err ", err);
    } else {
      const billingcycle = parseInt(settings.settings.billingcycle);
      const ext = {};
      ext.sort = { createdAt: -1 };
      db.GetOneDocument("billing", { agency: new mongoose.Types.ObjectId(agency), type: type }, {}, ext, (err, billingcycyle) => {
        if (err) {
          console.log("err ", err);
        } else {
          if (!billingcycyle) {
            const ext = {};
            ext.sort = { createdAt: -1 };
            db.GetOneDocument(schema, { agency: new mongoose.Types.ObjectId(agency) }, {}, ext, (err, docdata) => {
              if (err) {
                console.log("err ", err);
              } else {
                if (docdata) {
                  const date = docdata.createdAt;
                  const startDate = moment(date).format("YYYY/MM/DD 00:00:00.000");
                  const endDate = moment().format("YYYY/MM/DD 00:00:00.000");
                  const diffDays = moment(new Date(endDate)).diff(moment(new Date(startDate)), "days");
                  if (diffDays >= billingcycle) {
                    const data = {};
                    data.agency = agency;
                    data.type = type;
                    data.start_date = moment(new Date(startDate));
                    data.end_date = moment(new Date(endDate));
                    data.billingcycyle = data.start_date.format("DD/MM/YYYY") + "-" + data.end_date.format("DD/MM/YYYY");
                    db.InsertDocument("billing", data, (err, result) => {});
                  }
                }
              }
            });
          } else {
            const date = billingcycyle.end_date;
            const startDate = moment(date).format("YYYY/MM/DD 00:00:00.000");
            const endDate = moment(date)
              .add(billingcycle, "day")
              .format("YYYY/MM/DD 00:00:00.000");
            const currentDate = moment().format("YYYY/MM/DD 00:00:00.000");
            const endDateUnix = moment(new Date(endDate)).unix();
            const currentDateUnix = moment(new Date(currentDate)).unix();
            if (endDateUnix <= currentDateUnix) {
              const data = {};
              data.agency = agency;
              data.type = type;
              data.start_date = moment(new Date(startDate));
              data.end_date = moment(new Date(endDate));
              data.billingcycyle = data.start_date.format("DD/MM/YYYY") + "-" + data.end_date.format("DD/MM/YYYY");
              db.GetOneDocument("billing", { billingcycyle: data.billingcycyle, agency: new mongoose.Types.ObjectId(agency), type: type }, {}, {}, (err, billingcycyle) => {
                if (err) {
                  console.log("err ", err);
                } else {
                  if (!billingcycyle) {
                    db.InsertDocument("billing", data, (err, result) => {});
                  }
                }
              });
            }
          }
        }
      });
    }
  });
};

event.on("shiftstat", data => {
  db.GetOneDocument("shifts", { _id: data.id }, {}, {}, (err, shift) => {
    if (shift) {
      const Data = {
        agency: shift.agency,
        client: shift.client,
        employee: shift.employee,
        shiftID: shift._id,
        timesheetID: shift.timesheetID,
        invoiceID: shift.invoiceID,
        locations: shift.locations,
        branch: shift.branch,
        job_type: shift.job_type,
        starttime: shift.starttime,
        endtime: shift.endtime,
        start_date: shift.start_date,
        status: data.status,
        employee_rate: data.employee_rate,
        agency_rate: data.agency_rate,
        client_rate: data.client_rate,
        date: moment(new Date()).format("DD-MM-YYYY"),
        type: "shifts"
      };
      db.InsertDocument("stats", Data, (err, result) => {});
    }
  });
});
event.on("invoicestat", data => {
  db.GetOneDocument("invoices", { _id: data.id }, {}, {}, (err, shift) => {
    if (shift) {
      const Data = {
        agency: shift.agency,
        client: shift.client,
        employee: shift.employee,
        shiftID: shift.shiftID,
        timesheetID: shift.timesheetID,
        invoiceID: shift.invoiceID,
        locations: shift.locations,
        branch: shift.branch,
        job_type: shift.job_type,
        status: data.status,
        employee_rate: data.employee_rate,
        agency_rate: data.agency_rate,
        client_rate: data.client_rate,
        date: moment(new Date()).format("DD-MM-YYYY"),
        type: "invoices"
      };
      db.InsertDocument("stats", Data, (err, result) => {});
    }
  });
});
event.on("timesheet_dates_list", Indata => {
  Cycle_Dates(Indata.agency, "timesheet", "shifts");
});
event.on("invoice_dates_list", Indata => {
  Cycle_Dates(Indata.agency, "invoice", "invoice");
});
event.on("shiftstatdelete", Indata => {
  const updatedata = Indata && Indata.status ? { shiftID: Indata.id, status: Indata.status } : { shiftID: Indata.id };
  db.DeleteDocument("stats", updatedata, (err, result) => {});
});
event.on("agencyemailtemplates", Indata => {
  const slugData = ["profile", "invocie", "timesheet", "candidatewelcome", "sendrefereeform", "interviewcall", "signupsuccessmessage", "compliancemail", "applicationapproval", "interviewstatus", "verificationstatus", "refereeverification"];
  if (Indata) {
    db.GetDocument("emailtemplate", { slug: { $in: slugData } }, {}, {}, (err, emaildata) => {
      if (emaildata && emaildata.length > 0) {
        const dataarray = emaildata.map((list, i) => {
          return {
            description: list.description,
            email_subject: list.email_subject,
            sender_name: list.sender_name,
            sender_email: list.sender_email,
            email_content: list.email_content,
            status: list.status,
            heading: `Template${i + 1}`,
            default_mail: 1,
            type: list.slug,
            agency: Indata.agency,
            mail_def: "yes"
          };
        });
        if (dataarray) {
          db.InsertMultiple("agencyemailtemplate", dataarray, (err, result) => {});
        }
      }
    });
  }
});

module.exports = event;
