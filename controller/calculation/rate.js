const db = require("../../model/mongodb");
const mongoose = require("mongoose");
const moment = require("moment");
// let async = require("async");
const ConvertSecsToHrs = (valueInSeconds, callbacks) => {
  const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
  const evestarthr = valueInSeconds / 3600;
  const splithr = evestarthr.toString().split(".");
  const startmins = splithr[1] === undefined ? "00" : (+"0" + "." + splithr[1]) * 60;
  const JobTimeinhrs = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + Math.round(startmins) : +"0" + "" + splithr[0] + ":" + Math.round(startmins);
  callbacks(JobTimeinhrs);
};
const PriceSumCalc = (_Price, TotalHRS, TotalMins, callbacks) => {
  let user_ratemins = 0;
  const user_ratehrs = _Price * TotalHRS;
  if (TotalMins === 0) {
    user_ratemins = 0;
  } else if (TotalMins > 1 && TotalMins < 15) {
    user_ratemins = _Price / 4;
  } else if (TotalMins > 16 && TotalMins < 30) {
    user_ratemins = (_Price / 4) * 2;
  } else if (TotalMins > 31 && TotalMins < 45) {
    user_ratemins = (_Price / 4) * 3;
  } else if (TotalMins > 46 && TotalMins < 59) {
    user_ratemins = _Price;
  }
  const user_rate = user_ratehrs + user_ratemins; // Final rate
  callbacks(user_rate);
};

module.exports = (ShiftData, JobData, ClientJobData, EmployeeJobData, DaysoffData, reqbody, reqparams, callback) => {
  const Daysoff_Cond_Dates = DaysoffData.filter(list => moment(list.dates).format("DD-MM-YYYY") === moment(ShiftData.start_date).format("DD-MM-YYYY") && list.addedId.toString() === ShiftData.agency.toString());
  const data = {};
  data.status = 0;
  let schema;
  if (EmployeeJobData) {
    schema = "employeejobtypes";
  } else if (ClientJobData) {
    schema = "clientjobtypes";
  } else {
    schema = "jobtypes";
  }
  const startarr = moment(ShiftData.timesheet[0].start)
    .format("HH:mm")
    .split(":");
  const hour = startarr[0] * 3600;
  const mins = startarr[1] * 60;
  const JobStartedTime = hour + mins;
  const endarr = moment(ShiftData.timesheet[0].end)
    .format("HH:mm")
    .split(":");
  const endhour = endarr[0] * 3600;
  const endmins = endarr[1] * 60;
  const JobendTime = endhour + endmins;
  const Shift_week = moment(ShiftData.start_date)
    .format("dddd")
    .slice(0, 3)
    .toLowerCase();
  let Shift_day;
  if (Daysoff_Cond_Dates.length > 0) {
    Shift_day = "pub";
  } else {
    Shift_day = Shift_week;
  }
  /* const ConvertedBreaks = ShiftData.timesheet[0].breaks.map(list => {
    const startarr = moment(list.hold)
      .format("HH:mm")
      .split(":");
    const hour = startarr[0] * 3600;
    const mins = startarr[1] * 60;
    const holds = hour + mins;
    const endarr = moment(list.restart)
      .format("HH:mm")
      .split(":");
    const endhour = endarr[0] * 3600;
    const endmins = endarr[1] * 60;
    const restarts = endhour + endmins;
    return {
      _id: list._id,
      hold: holds,
      restart: restarts,
      holdedmins: list.holdedmins / 60
    };
  });*/
  const finalQuery = [];
  if (schema === "jobtypes") {
    finalQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(ShiftData.job_type) } } });
  } else if (schema === "employeejobtypes") {
    finalQuery.push({ $match: { job_id: { $eq: new mongoose.Types.ObjectId(ShiftData.job_type) }, employee: { $eq: new mongoose.Types.ObjectId(ShiftData.employee) } } });
  } else if (schema === "clientjobtypes") {
    finalQuery.push({ $match: { job_id: { $eq: new mongoose.Types.ObjectId(ShiftData.job_type) }, client: { $eq: new mongoose.Types.ObjectId(ShiftData.client) } } });
  }
  // finalQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(ShiftData.job_type) } } });
  finalQuery.push(
    {
      $project: {
        name: 1,
        late_fee: 1,
        rate_details: {
          $filter: {
            input: "$rate_details",
            as: "rates",
            cond: {
              $or: [
                { $and: [{ $gte: [JobStartedTime, "$$rates.from"] }, { $lte: [JobendTime, "$$rates.to"] }] },
                { $and: [{ $gte: [JobStartedTime, "$$rates.from"] }, { $lte: [JobStartedTime, "$$rates.to"] }] },
                { $and: [{ $gte: [JobendTime, "$$rates.from"] }, { $lte: [JobendTime, "$$rates.to"] }] }
              ]
            }
          }
        }
      }
    },
    { $unwind: "$rate_details" },
    { $match: { "rate_details.name": { $regex: "^" + Shift_day } } },
    { $group: { _id: null, name: { $push: "$name" }, rate_details: { $push: "$rate_details" }, late_fee: { $push: "$late_fee" } } }
  );
  db.GetAggregation("jobtypes", finalQuery, (err, FilteredData) => {
    if (err || !FilteredData) {
      data.response = "Unable to Get Your Data Please Try Again";
      data.err = err;
      callback(data);
    } else {
      if (FilteredData && FilteredData.length > 0) {
        let client_rate = 0;
        let employee_rate = 0;
        const FilterRateDetails = FilteredData[0].rate_details[0];
        const FilterRateDetailstwo = FilteredData[0].rate_details[1];
        if (FilteredData[0].rate_details.length === 0) {
          data.response = "Please Check Job Role Rate Configration";
          data.err = err;
          callback(data);
        } else if (FilteredData[0].rate_details.length === 1) {
          // let extra_hold_mins = 0;
          // if (ShiftData.timesheet[0].holdedmins > ShiftData.breaktime) {
          //   extra_hold_mins = ShiftData.timesheet[0].holdedmins - ShiftData.breaktime; // extra time calculation based on total holded minus breaktime given in shift
          // }
          let totalTimeinHrs;
          ConvertSecsToHrs(JobendTime - JobStartedTime, Finalresult => {
            totalTimeinHrs = Finalresult.split(":"); // totalTimeinHrs = job worked time in hrs
          });
          // job rate calculation based on hrs, case: in between day or nights only
          if (JobendTime - JobStartedTime <= 3600) {
            // case To calculate client rates calculation if employee worked below 1hrs
            client_rate = FilterRateDetails.client_rate;
          } else {
            // case To calculate client rates calculation if employee worked above 1hrs
            PriceSumCalc(FilterRateDetails.client_rate, totalTimeinHrs[0], totalTimeinHrs[1], FinalRate => {
              client_rate = FinalRate; // Final client rate
            });
          }
          // case To calculate employee rates calculation if employee worked above or below 1hrs
          PriceSumCalc(FilterRateDetails.employee_rate, totalTimeinHrs[0], totalTimeinHrs[1], FinalRate => {
            employee_rate = FinalRate; // Final employee rate
          });
        } else if (FilteredData[0].rate_details.length === 2) {
          /* let Agency_Morning_Extra_Rate = 0,
            Agency_Night_Extra_Rate = 0,
            Employee_Night_Extra_Rate = 0,
            Employee_Morning_Extra_Rate = 0;
          if (ShiftData.timesheet[0].holdedmins / 60 > ShiftData.breaktime) {
            let availablebreak = ShiftData.breaktime;
            let initialbreak = 0;
            ConvertedBreaks.map((list) => {
              if (availablebreak >= list.holdedmins) {
                availablebreak = availablebreak - list.holdedmins;
                initialbreak = initialbreak + list.holdedmins;
              } else {
                let totalextrabreak = list.holdedmins - availablebreak;
                if (list.hold >= FilterRateDetails.from && list.restart <= FilterRateDetails.to) {
                  // morning
                  Agency_Morning_Extra_Rate = (totalextrabreak / 60) * (FilterRateDetails.client_rate / 60);
                  Employee_Morning_Extra_Rate = (totalextrabreak / 60) * (FilterRateDetails.employee_rate / 60);
                } else if (list.hold >= FilterRateDetailstwo.from && list.restart <= FilterRateDetailstwo.to) {
                  // night
                  Agency_Night_Extra_Rate = (totalextrabreak / 60) * (FilterRateDetailstwo.client_rate / 60);
                  Employee_Night_Extra_Rate = (totalextrabreak / 60) * (FilterRateDetailstwo.employee_rate / 60);
                } else {
                  // both morning & night
                  if (list.hold >= FilterRateDetails.from && list.hold <= FilterRateDetails.to) {
                    let morningholded = FilterRateDetails.to - list.hold,
                      Agency_Morning_Extra_Rate = (morningholded / 60) * (FilterRateDetails.client_rate / 60),
                      Employee_Morning_Extra_Rate = (morningholded / 60) * (FilterRateDetails.rate / 60),
                      nightholded = totalextrabreak - morningholded,
                      Agency_Night_Extra_Rate = (nightholded / 60) * (FilterRateDetailstwo.client_rate / 60);
                    Employee_Night_Extra_Rate = (nightholded / 60) * (FilterRateDetailstwo.employee_rate / 60);
                  }
                  // both night & morning
                  if (list.hold >= FilterRateDetailstwo.from && list.hold <= FilterRateDetailstwo.to) {
                    let nightholded = FilterRateDetailstwo.from - list.restart,
                      Agency_Night_Extra_Rate = (nightholded / 60) * (FilterRateDetails.client_rate / 60),
                      Employee_Night_Extra_Rate = (nightholded / 60) * (FilterRateDetails.employee_rate / 60),
                      morningholded = totalextrabreak - nightholded,
                      Agency_Morning_Extra_Rate = (morningholded / 60) * (FilterRateDetailstwo.client_rate / 60);
                    Employee_Morning_Extra_Rate = (morningholded / 60) * (FilterRateDetailstwo.employee_rate / 60);
                  }
                }
              }
            });
          }*/
          if (JobStartedTime >= FilterRateDetails.from && JobStartedTime <= FilterRateDetails.to && JobendTime >= FilterRateDetailstwo.from && JobendTime <= FilterRateDetailstwo.to) {
            let totalTimeinHrsMorning;
            let totalTimeinHrsNight;
            ConvertSecsToHrs(FilterRateDetails.to - JobStartedTime, FinalR => {
              totalTimeinHrsMorning = FinalR.split(":"); // totalTimeinHrs = job worked time in hrs // morning
            });
            ConvertSecsToHrs(JobendTime - FilterRateDetailstwo.from, FResult => {
              totalTimeinHrsNight = FResult.split(":"); // totalTimeinHrs = job worked time in hrs // in night
            });
            // job rate calculation based on hrs, case: in between day or nights only
            if (JobendTime - JobStartedTime <= 3600) {
              // case To calculate client rates calculation if employee worked below 1hrs
              client_rate = FilterRateDetails.client_rate;
            } else {
              // case To calculate client rates calculation if employee worked above 1hrs
              let client_rate_morningss;
              PriceSumCalc(FilterRateDetails.client_rate, totalTimeinHrsMorning[0], totalTimeinHrsMorning[1], FinalRate => {
                client_rate_morningss = FinalRate; // Final Client Rate Morning
              });
              // rate calculation for employee for night
              let client_rate_nightss;
              PriceSumCalc(FilterRateDetailstwo.client_rate, totalTimeinHrsNight[0], totalTimeinHrsNight[1], FinalRate => {
                client_rate_nightss = FinalRate; // Final Client Rate Morning
              });
              // total employee rate calculation from morning and night
              client_rate = client_rate_morningss + client_rate_nightss;
            }
            // case To calculate employee rates calculation if employee worked above or below 1hrs
            // rate calculation for employee for morning
            let employee_rate_morningss;
            PriceSumCalc(FilterRateDetails.employee_rate, totalTimeinHrsMorning[0], totalTimeinHrsMorning[1], FinalRate => {
              employee_rate_morningss = FinalRate; // Final Client Rate Morning
            });
            // rate calculation for employee for night
            let employee_rate_nightss;
            PriceSumCalc(FilterRateDetailstwo.employee_rate, totalTimeinHrsNight[0], totalTimeinHrsNight[1], FinalRate => {
              employee_rate_nightss = FinalRate; // Final Client Rate Morning
            });
            // total employee rate calculation from morning and night
            employee_rate = employee_rate_morningss + employee_rate_nightss;
          } else if (JobStartedTime >= FilterRateDetailstwo.from && JobStartedTime <= FilterRateDetailstwo.to && JobendTime >= FilterRateDetails.from && JobendTime <= FilterRateDetails.to) {
            // rate calculation:- if shift lies between morning and night
            let totalTimeinHrsMorning;
            let totalTimeinHrsNight;
            ConvertSecsToHrs(FilterRateDetailstwo.to - JobStartedTime, Fresultss => {
              totalTimeinHrsNight = Fresultss.split(":"); // totalTimeinHrs = job worked time in hrs // in night
            });
            ConvertSecsToHrs(JobendTime - FilterRateDetails.from, FFResult => {
              totalTimeinHrsMorning = FFResult.split(":"); // totalTimeinHrs = job worked time in hrs // morning
            });
            // job rate calculation based on hrs, case: in between day or nights only
            if (JobendTime - JobStartedTime <= 3600) {
              // case To calculate client rates calculation if employee worked below 1hrs
              client_rate = FilterRateDetails.client_rate;
            } else {
              // case To calculate client rates calculation if employee worked above 1hrs
              let client_rate_morningss;
              PriceSumCalc(FilterRateDetails.client_rate, totalTimeinHrsMorning[0], totalTimeinHrsMorning[1], FinalRate => {
                client_rate_morningss = FinalRate; // Final Client Rate Morning
              });
              // rate calculation for employee for night
              let client_rate_nightss;
              PriceSumCalc(FilterRateDetailstwo.client_rate, totalTimeinHrsNight[0], totalTimeinHrsNight[1], FinalRate => {
                client_rate_nightss = FinalRate; // Final Client Rate Morning
              });
              // total employee rate calculation from morning and night
              client_rate = client_rate_morningss + client_rate_nightss;
            }
            // case To calculate employee rates calculation if employee worked above or below 1hrs
            // rate calculation for employee for morning
            let employee_rate_morningss;
            PriceSumCalc(FilterRateDetails.employee_rate, totalTimeinHrsMorning[0], totalTimeinHrsMorning[1], FinalRate => {
              employee_rate_morningss = FinalRate; // Final Client Rate Morning
            });
            // rate calculation for employee for night
            let employee_rate_nightss;
            PriceSumCalc(FilterRateDetailstwo.employee_rate, totalTimeinHrsNight[0], totalTimeinHrsNight[1], FinalRate => {
              employee_rate_nightss = FinalRate; // Final Client Rate Morning
            });
            // total employee rate calculation from morning and night
            employee_rate = employee_rate_morningss + employee_rate_nightss;
          } else {
            data.response = "Please Check Job Role Rate Configration";
            data.err = err;
            callback(data);
          }
        }
        // Late Fee Amount Calculation
        let late_amount = 0;
        if (ShiftData.latefee) {
          late_amount = FilteredData[0].late_fee[0].amount === undefined || FilteredData[0].late_fee[0].duration === undefined ? 0 : FilteredData[0].late_fee[0].amount;
        } else {
          if (
            !FilteredData[0].late_fee ||
            FilteredData[0].late_fee === undefined ||
            FilteredData[0].late_fee.length === 0 ||
            FilteredData[0].late_fee[0].amount === undefined ||
            FilteredData[0].late_fee[0].duration === undefined ||
            FilteredData[0].late_fee[0].amount === 0 ||
            FilteredData[0].late_fee[0].duration === 0
          ) {
            late_amount = 0;
          } else if (moment(ShiftData.createdAt) < moment(ShiftData.start_date)) {
            late_amount = 0;
          } else {
            const late_fee_time = ShiftData.starttime - FilteredData[0].late_fee[0].duration;
            let late_time = 0;
            if (late_fee_time !== undefined) {
              ConvertSecsToHrs(late_fee_time, LateResult => {
                late_time = LateResult; // late_time in hrs
              });
            }
            const startTime = moment(moment(ShiftData.createdAt).format("HH:mm"), "HH:mm");
            const endTime = moment(late_time, "HH:mm");
            const duration = moment.duration(endTime.diff(startTime));
            const late_difference = duration.as("milliseconds");
            late_amount = late_difference > 0 ? 0 : FilteredData[0].late_fee[0].amount;
          }
        }
        if (client_rate >= 0 && employee_rate >= 0 && late_amount >= 0) {
          client_rate = client_rate.toFixed(2);
          employee_rate = employee_rate.toFixed(2);
          late_amount = (late_amount || 0).toFixed(2);
          const invoice_details = {
            shift_mid: ShiftData._id,
            start_date: ShiftData.start_date,
            starttime: ShiftData.starttime,
            endtime: ShiftData.endtime,
            shift_id: ShiftData.shiftId,
            late_fee: FilteredData[0].late_fee[0] || 0,
            rate_details: FilteredData[0].rate_details,
            client_rate: client_rate,
            employee_rate: employee_rate,
            late_amount: late_amount,
            locations: new mongoose.Types.ObjectId(ShiftData.locations),
            employee: new mongoose.Types.ObjectId(ShiftData.employee),
            client: new mongoose.Types.ObjectId(ShiftData.client),
            branch: new mongoose.Types.ObjectId(ShiftData.branch),
            job_type: new mongoose.Types.ObjectId(ShiftData.job_type),
            agency: new mongoose.Types.ObjectId(ShiftData.agency),
            timesheetID: reqbody.timesheetID,
            status: 1,
            timesheet_status: 2,
            calc_schema: schema,
            addedId: reqparams
          };
          // db.GetOneDocument("invoice", {}, {}, {}, (err, result) => {
          db.InsertDocument("invoice", invoice_details, (err, result) => {
            if (err || !result) {
              data.response = "Unable to save your Invoice data, Please try again";
              data.err = err;
              callback(data);
            } else {
              const updates = { timesheet_status: 2 };
              if (reqbody.comment) {
                updates.agency_comment = reqbody.comment;
              }
              updates.rating = 0;
              if (reqbody.rating) {
                updates.agency_rating = reqbody.rating;
              }
              updates.status = 7;
              updates.late_amount = late_amount;
              updates.employee_rate = employee_rate;
              updates.client_rate = client_rate;
              (updates.timesheetID = reqbody.timesheetID),
                // db.GetOneDocument("shifts", {}, {}, {}, (err, ShiftDatas) => {
                db.UpdateDocument("shifts", { _id: reqbody.shiftId || ShiftData._id }, updates, {}, (err, ShiftDatas) => {
                  if (err || !ShiftDatas) {
                    data.response = "Unable to update your Timesheet data, Please try again";
                    data.err = err;
                    callback(data);
                  } else {
                    db.GetOneDocument("timesheetcycle", { timesheetID: reqbody.timesheetID }, {}, {}, (err, Docs) => {
                      if (Docs != null) {
                        db.UpdateDocument("timesheetcycle", { timesheetID: reqbody.timesheetID }, { $push: { shifts: ShiftData._id }, to_date: reqbody.to_date, from_date: reqbody.from_date }, { upsert: true }, (err, result) => {
                          if (err || !result) {
                            data.response = "Unable to update your Timesheet Cycle, Please try again";
                            data.err = err;
                            callback(data);
                          } else {
                            data.status = 1;
                            data.response = { result: client_rate && employee_rate && late_amount };
                            callback(err, data);
                          }
                        });
                      } else {
                        const Query = {
                          shifts: [ShiftData._id],
                          employee: new mongoose.Types.ObjectId(ShiftData.employee),
                          client: new mongoose.Types.ObjectId(ShiftData.client),
                          agency: new mongoose.Types.ObjectId(ShiftData.agency),
                          timesheetID: reqbody.timesheetID,
                          to_date: reqbody.to_date,
                          from_date: reqbody.from_date
                        };
                        db.InsertDocument("timesheetcycle", Query, (err, result) => {
                          if (err || !result) {
                            data.response = "Unable to save your Timesheet Cycle, Please try again";
                            data.err = err;
                            callback(data);
                          } else {
                            data.status = 1;
                            data.response = { result: client_rate && employee_rate && late_amount };
                            callback(err, data);
                          }
                        });
                      }
                    });
                  }
                });
            }
          });
        } else {
          data.response = "Please Check Job Role Rate Configration";
          data.err = err;
          callback(data);
        }
      } else {
        data.response = "Rate Details Not Available";
        data.err = err;
        callback(data);
      }
    }
  });
};
