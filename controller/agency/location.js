"use strict";
const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const moment = require("moment");
const mongoose = require("mongoose");
const json2csv = require("json2csv").Parser;
const fs = require("fs");

module.exports = () => {
  const router = {};
  const data = {};
  data.status = 0;

  router.save = (req, res) => {
    req.checkBody("name", "Name is Required").notEmpty();
    // req.checkBody('phone','Phone Number is Required').notEmpty();
    req.checkBody("address", "Address is Required").notEmpty();
    // req.checkBody('color','Color is Required').notEmpty();
    req.checkBody("radius", "Radius is Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const loc = {
      name: req.body.name,
      // phone:req.body.phone,
      // color:req.body.color,
      address: req.body.address,
      geolocation: req.body.geolocation,
      radius: req.body.radius,
      status: req.body.status,
      addedBy: "agency",
      addedId: req.params.loginId
    };
    db.GetDocument("locations", { addedId: new mongoose.Types.ObjectId(req.params.loginId), name: req.body.name }, {}, {}, (err, user) => {
      if (err) {
        data.response = "Invalid Credentials";
        res.send(data);
      } else {
        if (user && user.length > 0) {
          data.response = "Name is already Exists ";
          res.send(data);
        } else {
          if (req.body._id) {
            db.UpdateDocument("locations", { _id: req.body._id }, loc, {}, (err, result) => {
              if (err) {
                data.response = "Unable to Save Your Data Please try again";
                data.status = 0;
                res.send(data);
              } else {
                data.status = 1;
                data.response = "Successfully Updated";
                res.send(data);
              }
            });
          } else {
            db.InsertDocument("locations", loc, (err, result) => {
              if (err || !result) {
                if (err.code === 11000) {
                  data.response = "Name is already Exists ";
                } else {
                  data.response = "Unable to Save Your Data Please try again";
                }
                data.status = 0;
                res.send(data);
              } else {
                data.status = 1;
                data.response = { result: result };
                res.send(data);
              }
            });
          }
        }
      }
    });
  };

  router.list = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const locationQuery = [];

    locationQuery.push({ $match: { status: { $exists: true } } });

    locationQuery.push({ $match: { addedBy: { $eq: "agency" } } });
    locationQuery.push({ $match: { addedId: { $eq: req.params.loginId } } });

    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    // if(req.body.search){
    //     var searches = req.body.search;
    //     var searching = {};
    //     searching[req.body.filter] = {$regex: searches +'.*',$options:'si'};
    //     locationQuery.push({'$match':searching});
    // }
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        locationQuery.push({
          $match: {
            $or: [
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { address: { $regex: searchs + ".*", $options: "si" } },
              { color: { $regex: searchs + ".*", $options: "si" } },
              { zipcode: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        locationQuery.push({ $match: searching });
      }
    }
    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      locationQuery.push({ $sort: sorting });
    } else {
      locationQuery.push({ $sort: { createdAt: -1 } });
    }
    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }
    locationQuery.push({ $project: { createdAt: 1, updatedAt: 1, status: 1, name: 1, color: 1, geolocation: 1, radius: 1, address: 1, addedBy: 1 } });

    // var withoutlimit = JSON.parse(JSON.stringify(locationQuery));
    // withoutlimit.push({
    //     $count :'count'
    // });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });

    locationQuery.push({
      $addFields: {
        lat: "$geolocation.lat",
        lng: "$geolocation.lng",
        label: { $substr: ["$name", 0, 1] }
      }
    });
    if (req.body.skip >= 0) {
      locationQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      locationQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("locations", finalQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };

        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    req.checkBody("id", "Invalid id").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    db.GetDocument("locations", { _id: req.body.id }, {}, {}, (err, locationData) => {
      if (err || !locationData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: locationData };
        res.send(data);
      }
    });
  };

  router.locationexport = function(req, res) {
    const data = {};
    data.status = 0;

    const locationQuery = [];
    locationQuery.push({ $match: { status: { $exists: true } } });
    locationQuery.push({ $match: { addedBy: { $eq: "agency" } } });
    locationQuery.push({ $match: { addedId: { $eq: req.params.loginId } } });
    locationQuery.push({
      $addFields: {
        lat: "$geolocation.lat",
        lng: "$geolocation.lng",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    db.GetAggregation("locations", locationQuery, function(err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData && docData.length > 0) {
          const fieldNames = ["Date", "Work Location", "Address", "Radius", "Status"];
          let finaldata = [];
          for (let i = 0; i < docData.length; i++) {
            let temp = {};
            if (docData[i].status == 0) {
              docData[i].status = "In-active";
            } else if (docData[i].status == 1) {
              docData[i].status = "Active";
            }
            temp["Date"] = docData[i].createdAt ? moment(docData[i].createdAt).format("DD/MM/YYYY") : "-";
            temp["Work Location"] = docData[i].name || "";
            temp["Address"] = docData[i].address || "";
            temp["Radius"] = docData[i].radius || "";
            temp["Status"] = docData[i].status || "";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  return router;
};
