module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const mongoose = require("mongoose");
  const dashboard = require("../../model/dashboard.js");
  const moment = require("moment");
  const json2csv = require("json2csv").Parser;
  const mail = require("../../model/mail.js");
  const async = require("async");
  const pdf = require("html-pdf");
  const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };

  const router = {};
  const data = {};
  data.status = 0;

  router.invoiceexport = (req, res) => {
    const invoiceQuery = [];
    if (req.body.id) {
      invoiceQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    }
    invoiceQuery.push({ $match: { status: { $gte: 2 } } });
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );
    invoiceQuery.push({
      $group: {
        _id: { client: "$client" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 }
      }
    });
    db.GetAggregation("invoice", invoiceQuery, (err, docdata) => {
      if (err || !docdata) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Client", "Shifts"];
          const mydata = docdata;
          const finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Client"] = mydata[i].client && mydata[i].client.length > 0 ? mydata[i].client.join() : "";
            temp["Shifts"] = mydata[i].count || 0;
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  router.groupbyemployee = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });

    if (req.body.status) {
      invoiceQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      invoiceQuery.push({ $match: { status: { $gte: 2 } } });
    }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gte: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lte: new Date(req.body.to_date) } } });
    }

    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        invoiceQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift_id: { $regex: searchs + ".*", $options: "si" } },
              { "client.companyname": { $regex: searchs + ".*", $options: "si" } },
              { "employee.name": { $regex: searchs + ".*", $options: "si" } },
              { "locations.name": { $regex: searchs + ".*", $options: "si" } },
              { "branch.branches.branchname": { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { "job_type.name": { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        invoiceQuery.push({ $match: searching });
      }
    }
    invoiceQuery.push({
      $group: {
        // _id: {client: "$client", locations: "$locations", employee: "$employee", branch: "$branch", job_type: "$job_type"},
        _id: { client: "$client" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 }
      }
    });

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.id) } } },
            {
              $group: {
                _id: "$status",
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "employee", employee: new mongoose.Types.ObjectId(req.body.id), action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        data.status = 1;
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          invoiceQuery: invoiceQuery
        };
        res.send(data);
      }
    });
  };
  router.listbyemployee = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    // invoiceQuery.push({$match: {employee: {$eq: new mongoose.Types.ObjectId(req.body.employeeID)}}});
    // invoiceQuery.push({$match: {job_type: {$eq: new mongoose.Types.ObjectId(req.body.jobtypeID)}}});
    invoiceQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.clientID) } } });
    // invoiceQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // invoiceQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.branchID)}}});
    invoiceQuery.push({ $match: { status: { $gte: 2 } } });

    if (req.body.status) {
      invoiceQuery.push({ $match: { status: { $eq: req.body.status } } });
    }
    // else {
    //   invoiceQuery.push({
    //     $match: {
    //       $or: [{status: 6}, {status: 7}],
    //     },
    //   });
    // }

    if (req.body.search) {
      const searches = req.body.search;
      const searching = {};
      searching[req.body.filter] = { $regex: searches + ".*", $options: "si" };
      invoiceQuery.push({ $match: searching });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      invoiceQuery.push({ $sort: sorting });
    } else {
      invoiceQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, email: 1, avatar: 1, phone: 1, address: 1, company_logo: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "shifts",
          let: { shiftid: "$shift_mid" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$shiftid"]
                }
              }
            },
            { $project: { company_name: 1, timesheet: 1, breaktime: 1 } }
          ],
          as: "shift_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, email: 1, avatar: 1, phone: 1, address: 1, company_logo: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.id) } } },
            {
              $group: {
                _id: "$status",
                agency_rate: { $sum: "$agency_rate" },
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "employee", employee: new mongoose.Types.ObjectId(req.body.id), action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        data.status = 1;
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          invoiceQuery: invoiceQuery
        };
        res.send(data);
      }
    });
  };
  router.export = (req, res) => {
    const data = {};

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    const invoiceQuery = [];
    invoiceQuery.push({ $match: { status: { $eq: 2 } } });
    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      invoiceQuery.push({ $sort: sorting });
    } else {
      invoiceQuery.push({ $sort: { createdAt: -1 } });
    }

    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$location" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { location: "$location_data.name" } }
    );

    db.GetAggregation("invoice", invoiceQuery, (err, docData) => {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          const fields = ["createdAt", "title", "location[0]", "job_type[0]", "employee[0]", "employee_rate", "starttime", "endtime", "start_date", "end_date", "timesheet_status"];
          const fieldNames = ["Date", "Title", "Location", "Role", "Employee", "Employee Rate", "Start Time", "End Time", "Start Date", "End Date", "Timesheet Status"];

          for (let i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");

            if (docData[i].status == 2) {
              docData[i].tiemsheet_status = "Approved";
            } else {
              docData[i].status = "Pending";
            }
          }

          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(docData);

          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        }
      }
    });
  };
  router.downloadpdf = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.id) } } });
    // invoiceQuery.push({$match: {employee: {$eq: new mongoose.Types.ObjectId(req.body.employeeID)}}});
    // invoiceQuery.push({$match: {job_type: {$eq: new mongoose.Types.ObjectId(req.body.jobtypeID)}}});
    invoiceQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.clientID) } } });
    // invoiceQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // invoiceQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.TabOpt.branchID)}}});
    invoiceQuery.push({ $match: { status: { $gte: 2 } } });

    if (req.body.search) {
      const searches = req.body.search;
      const searching = {};
      searching[req.body.filter] = { $regex: searches + ".*", $options: "si" };
      invoiceQuery.push({ $match: searching });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      invoiceQuery.push({ $sort: sorting });
    } else {
      invoiceQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, avatar: 1, email: 1, address: 1, phone: 1, company_logo: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );
    invoiceQuery.push({
      $lookup: {
        from: "shifts",
        let: { shiftid: "$shift_mid" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$shiftid"]
              }
            }
          },
          { $project: { name: 1, timesheet: 1, breaktime: 1 } }
        ],
        as: "shift_data"
      }
    });

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1, email: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, avatar: 1, address: 1, phone: 1, email: 1, company_logo: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.id) } } },
            {
              $group: {
                _id: "$status",
                agency_rate: { $sum: "$agency_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (docData && docData[0].result && docData[0].result[0]) {
          const getshiftdata = [];
          // console.log("docData[0].result[0]", docData[0].result[0]);
          let totalrate,
            clienttotalrate,
            latetotalrate = 0;
          const clientAr = docData[0].result.map(list => list.employee_rate);
          clienttotalrate = clientAr.reduce((a, b) => a + b);
          const lateAr = docData[0].result.map(list => list.late_amount);
          latetotalrate = lateAr.reduce((a, b) => a + b);
          // totalrate = clienttotalrate + latetotalrate;
          totalrate = clienttotalrate;
          docData[0].result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = item.starttime / 3600;
            const splithr = evestarthr.toString().split(".");
            const startsec = splithr[1];
            if (startsec === undefined) {
              var startmins = "00";
            } else {
              startmins = (+"0" + "." + startsec) * 60;
            }

            if (num.indexOf(splithr[0]) === -1) {
              var starttimehr = splithr[0] + ":" + startmins;
            } else {
              starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
            }
            const eveendhr = item.endtime / 3600;
            const splitendhr = eveendhr.toString().split(".");
            const endsec = splitendhr[1];
            if (endsec === undefined) {
              var endmins = "00";
            } else {
              endmins = (+"0" + "." + endsec) * 60;
            }

            if (num.indexOf(splitendhr[0]) === -1) {
              var endtimehr = splitendhr[0] + ":" + endmins;
            } else {
              endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
            }
            if (item.timesheet_status) {
              var timesheet_status = item.timesheet_status;
            } else {
              timesheet_status = "";
            }
            getshiftdata.push({
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              client_data: item.client_data,
              agency_data: item.agency_data,
              timesheet: item.timesheet,
              job_type: item.job_type,
              job_rate: item.rate_details.length > 0 ? item.rate_details[0].employee_rate : "",
              employee: item.employee,
              employee_data: item.employee_data,
              employee_rate: item.employee_rate,
              late_amount: item.late_amount,
              client_rate: item.client_rate,
              title: item.shift_id,
              shiftId: item.shift_id,
              shift_data: item.shift_data,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            });
            return true;
          });
          // console.log('docData[0].result', docData[0].result);
          const html = `
          <style>
html {
zoom: 0.55;
}
</style>
<table style="margin: 0; padding: 0; color: #000; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
          <tbody>
           <tr>
             <td>
               <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>
                   <tr>
                     <td height="10">&nbsp;</td>
                   </tr>
                   <tr>
                    <td align="right" style="padding-right: 21%;"> 
                    <img src="${CONFIG.LIVEURL}/uploads/logo.png" width="240">
                    </td>
                            </tr>
                           <!--  <tr>
                     <td height="10" style="border-bottom:1px solid #ddd;">&nbsp;</td>
                    </tr> -->
                  
                  <tr>
                    <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                        <tr>
                          <td style="width: 50%; vertical-align: top;">
                           <h4 style="font-size: 30px;font-weight: normal;color: #000;margin-bottom: 0;">INVOICE</h4>
                         <p style="font-size: 15px;line-height: 26px;color: #4e4c4c;width: 80%;text-align: justify;margin: 0 auto; padding-top: 5px;word-break: break-word;">
                         ${getshiftdata[0].client_data[0].companyname} <br>
                                ${getshiftdata[0].client_data[0].address.formatted_address}, ${getshiftdata[0].client_data[0].address.zipcode} .</p>
                         
                        </td>
                         <td style="width: 25%; vertical-align: top;word-break:break-word;">
                           <span style="font-size: 17px; font-weight: bold; display: block;">Invoice Date</span>
                         
                           <span style="font-size: 14px; color: #4e4c4c; line-height: 30px;">${moment(new Date(req.body.invoice_from_date)).format("DD-MM-YYYY")} - ${moment(new Date(req.body.invoice_to_date)).format("DD-MM-YYYY")}</span>
                         <br>
                         
                          <span style="font-size: 17px; font-weight: bold; display: block;">Invoice Number</span>
                         
                         <span style="font-size: 14px; color: #4e4c4c; line-height: 30px;">${req.body.invoice_invoicePrefix}</span>
                         
                         <br>
                         ${getshiftdata[0].agency_data[0].vat_number? 
                            `<span style="font-size: 17px; font-weight: bold; display: block;">VAT Number</span>
                         
                         <span style="font-size: 14px; color: #4e4c4c; line-height: 30px;"> ${getshiftdata[0].agency_data[0].vat_number} </span>`:''
                         }
                         
                         
                        </td>
                         <td style="width: 25%; vertical-align: top;">
                           <!--  <img src="user.png" style=" width: 120px;    height: 120px;"> -->
                          <p style="font-size: 15px; line-height: 26px;   color: #4e4c4c; width: 90%;    word-break: break-word;">${getshiftdata[0].agency_data[0].address.formatted_address}, ${getshiftdata[0].agency_data[0].address.zipcode} .</p>
                        </td>
                        </tr>
                        </tbody>
                     </table>
                    </td>
                  </tr>
                  
                  <tr>
                    <td>
                       <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <thead>
                        <tr>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Shift No. </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Work Location  </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Employee </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Shift </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Time </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Rate </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Hours </th>
                                      <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Total Price </th>
                                  </tr>
                      </thead>
                      <tbody>
          
                              ${getshiftdata
                                    .map(
                                      item => `<tr>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${item.title}	 </td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${item.branch} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${item.employee} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${moment(item.start_date).format("DD-MM-YYYY")} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${item.starttime} - ${item.endtime} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> £ ${item.job_rate}	</td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${Math.round(item.shift_data[0].timesheet[0].workmins / 60)} Minutes </td>
                                    <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> £ ${item.client_rate}</td>
                                  </tr>`
                                    )
                                    .join(" ")}
                      </tbody>
                                  </table>							 
                    </td>
                  </tr>
                  
                  
                  <tr>
                   <td>
                   <table style="     padding-top: 0 !important;   padding: 15px 30px;margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%;    padding-bottom: 10px;">
                     <tbody>
                       <tr>
                       <td width="85%" align="right" style="font-size:13px;color:#000;padding-right:10px;    padding-right: 27px;">
                        Subtotal
                       </td>
                       <td width="15%" align="right" style="font-size:13px;color:#000;">
                        £ ${totalrate}
                       </td>
                       </tr>
                      </tbody>
                    </table>				 
                   </td>
                  </tr>
                  
                  
                 <tr>
                   <td>
                   <table style="padding-top:0px !important;padding: 15px 30px;margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%;padding-bottom: 10px;">
                     <tbody>
                       <tr>
                       <td width="85%" align="right" style="font-size:13px;color:#000;    padding-right: 27px;">
                       Late Booking Fee
                       </td>
                       <td width="15%" align="right" style="font-size:13px;color:#000;">
                       £ ${latetotalrate}
                       </td>
                       </tr>
                      </tbody>
                    </table>				 
                   </td>
                 </tr>
          
                 <tr>
                 <td>
                 <table style="padding-top:0px !important;padding: 15px 30px;margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%;padding-bottom: 10px;">
                   <tbody>
                     <tr>
                     <td width="85%" align="right" style="font-size:13px;color:#000;    padding-right: 27px;">
                     Due Amount
                     </td>
                     <td width="15%" align="right" style="font-size:13px;color:#000;">
                     £ ${totalrate - CurrentRate}
                     </td>
                     </tr>
                    </tbody>
                  </table>				 
                 </td>
               </tr>
                 
                
                <tr>
                  <td>
                    <table style="margin-top: 200; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                   <tbody>
                      <tr>
                       <td style="font-size: 18px; font-weight: bold; color: #000;">Due Date: ${moment(new Date(INV_Data.due_date)).format("DD-MM-YYYY")}</td>
                     </tr>
                     
                     
                     
                     <tr>
                       <td style="font-size: 15px; font-weight: bold; color: #000; line-height: 40px;">PAYMENT ADVICE</td>
                     </tr>
                     <tr>
                       <td style="font-size: 14px; color: #4e4c4c;">Thank You for your Business!</td>
                     </tr>
                     
                     <tr>
                         <td style="line-height:30px;">
                         <span style="font-size: 14px; color: #4e4c4c;"> Bank Name: ${getshiftdata[0].agency_data[0].bank_details &&
                                  getshiftdata[0].agency_data[0].bank_details.name},</span><span style="font-size: 14px; color: #4e4c4c;"> Sort Code: 30-98-97,</span><span style="font-size: 14px; color: #4e4c4c;"> Bank Acc No: ${getshiftdata[0].agency_data[0].bank_details &&
                       getshiftdata[0].agency_data[0].bank_details.ac_no} </span>
                       </td>
                     </tr>
                     <tr>
                         <td style="line-height:30px;">
                         <span style="font-size: 14px; color: #4e4c4c;"> Account name: ${getshiftdata[0].agency_data[0].bank_details.ac_name}, </span> <span style="font-size: 14px; color: #4e4c4c;"> Sort code: ${getshiftdata[0].agency_data[0].bank_details.sort_code}, </span>
                       </td>
                     </tr>
                   </tbody>
                  </table>		  
                  </td>
                </tr>
                 <tr>
                  <td>
                    <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                   <tbody>
                     <tr>
                     <td style="width:5%;vertical-align:top;">
                     <a><img src="${CONFIG.LIVEURL}/uploads/visa.png" style="width: 58px;    vertical-align: top;"></a>
                     <a><img src="${CONFIG.LIVEURL}/uploads/mastercard.jpg" style="width: 58px;    vertical-align: top;"></a>
                     <a>	<img src="${CONFIG.LIVEURL}/uploads/paypal.jpg" style="width: 58px;    vertical-align: top;"></a>
                     </td>
                     </tr>
                     <tr>
                        <td>
                          <a style="font-size: 13px; color: #00adf1; line-height: 30px; text-transform: uppercase;    font-weight: bold;" href="#">View and pay online now </a>
                        </td>
                     </tr>
                   </tbody>
                  </table>		  
                  </td>
                </tr>
                 <tr>
                   <td>
                    <table style="margin: 0px auto;margin-top: 70px; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;    border-top: 1px solid #ddd;">
                      <tbody>
                          <tr>
                          <td style="font-size: 30px; font-weight: normal; color: #000;vertical-align: top;width: 50%;"> PAYMENT ADVICE <p style="font-weight: normal;
           font-size: 16px;
           float: left; 
           width: 100%;
           padding-left: 10%;"><span  style="width:7%;float: left;font-size:15px;padding-top:3px;color:#4e4c4c;">To:</span> <span style="font-size: 15px;line-height: 26px;color: #4e4c4c;width: 46%;float: left;    word-break: break-word;">${getshiftdata[0].client_data[0].companyname},${getshiftdata[0].client_data[0].address.formatted_address},${
                       getshiftdata[0].client_data[0].address.zipcode
                     }. Tel. : ${getshiftdata[0].client_data[0].phone.code} -  ${getshiftdata[0].client_data[0].phone.number} .</span></p> </td>
                          <td width="50%" style="vertical-align:top;">
                           <p style="padding-bottom: 7px;float: left;font-size: 15px; line-height: 16px;   color: #4e4c4;margin: 0; "> <span style="    font-weight: bold; color: #000;float: left;
           width: 40%;"> Customer </span> <span style="float:right;width:60%;    word-break: break-word;"> ${getshiftdata[0].client_data[0].companyname} </span></p>
                           <p style="font-size: 15px; line-height: 16px;   color: #4e4c4c;margin: 0;float:left;width:100%;padding-bottom: 6px;    border-bottom: 1px solid #ddd; "> <span style="float:left;width:40%;    font-weight: bold; color: #000;"> Invoice No </span> <span style="float:right;width:60%;word-break:break-word;">  ${req.body.invoice_invoicePrefix} </span></p>
                           <p style=" float:left; width:100%;    padding-bottom: 7px;   padding-top: 9px;font-size: 15px; line-height: 16px;   color: #4e4c4c;margin: 0;    "> <span style="    font-weight: bold; color: #000;float:left;width:40%;"> Amount Due </span> <span style="float:right;width:60%">  £ ${totalrate - CurrentRate}</span></p>
                           <p style="     padding-bottom: 6px;
           border-bottom: 1px solid #ddd;   float: left;width:100%;font-size: 15px; line-height: 16px;   color: #4e4c4c;margin: 0;    padding-bottom: 7px;    "> <span style=" float:left;width:40%;   font-weight: bold; color: #000;"> Due Date </span> <span style="float:right;width:60%;word-break:break-word;"> ${moment(new Date(INV_Data.due_date)).format("DD-MM-YYYY")}</span></p>
                          
                         
                         </td>
                        </tr>
                        <tr>
                          <td style="font-size: 18px; line-height: 37px; color: #888888;"></td>
                        </tr>
                        <tr>
                           <td width="50%" style="vertical-align:top;" >
                           
                         </td>
                        </tr>
                      </body>
                     </table>				 
                   </td>
                 </tr>
           <tr>
                </tbody>
                </table>
             </td>
           </tr>
          </tbody>
          </table>  
                         `;
          if (req.body.for === "download") {
            pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
          } else {
            pdf.create(html, pdfoptions).toFile("./uploads/pdf/invoices.pdf", function(err, document) {
              if (err) {
                res.send(err);
              } else {
                data.status = 1;
                data.msg = "Mail Sent";
                res.send(data);
                const mailOptions = {
                  from: docData[0].result[0].employee_data[0].email,
                  to: docData[0].result[0].agency_data[0].email,
                  subject: "Invoices",
                  text: "Please Download the attachment to see Invoices",
                  html: "<b>Please Download the attachment to see Invoices</b>",
                  attachments: [
                    {
                      filename: "invoices.pdf",
                      path: "./uploads/pdf/invoices.pdf",
                      contentType: "application/pdf"
                    }
                  ]
                };
                mail.send(mailOptions, function(err, response) {});
              }
            });
          }
        } else {
          data.status = 0;
          data.response = "Invoice Details Not Available";
          res.send(data);
        }
      }
    });
  };
  router.invociedates = (req, res) => {
    const data = {};
    async.waterfall(
      [
        callback => {
          db.GetDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, { _id: 1 }, {}, (err, EmployeeData) => {
            if (err || !EmployeeData) {
              data.response = "Date details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, EmployeeData);
            }
          });
        },
        (EmployeeData, callback) => {
          db.GetDocument("billing", { agency: new mongoose.Types.ObjectId(EmployeeData.agency), type: "invoice" }, {}, {}, (err, Billing_Dates) => {
            if (err || !Billing_Dates) {
              data.response = "Date details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, EmployeeData, Billing_Dates);
            }
          });
        },
        (EmployeeData, Billing_Dates, callback) => {
          const options = {};
          options.sort = { createdAt: -1 };
          db.GetOneDocument("invoice", { agency: new mongoose.Types.ObjectId(EmployeeData.agency) }, { createdAt: 1, start_date: 1 }, options, (err, Invocies) => {
            if (err) {
              data.response = "Shift details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, EmployeeData, Billing_Dates, Invocies);
            }
          });
        }
      ],
      (err, EmployeeData, Billing_Dates, Invocies) => {
        if (err) {
          data.status = 0;
          data.response = "Unable to get Dates, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { datelist: Billing_Dates || [], lastinvoice: (Invocies && Invocies.createdAt) || new Date() };
          res.send(data);
        }
      }
    );
  };
  return router;
};
