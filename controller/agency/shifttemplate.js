"use strict";
var db = require("../../model/mongodb");
var CONFIG = require("../../config/config");
const moment = require("moment");
var json2csv = require("json2csv").Parser;
const fs = require("fs");

module.exports = () => {
  const router = {};

  var data = {};
  data.status = 0;

  router.save = (req, res) => {
    req.checkBody("name", "Name is Required").notEmpty();
    req.checkBody("from", "From Field is Required").notEmpty();
    req.checkBody("to", "To Field is Required").notEmpty();
    req.checkBody("break", "Break Field is Required").notEmpty();

    var error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    var shift = {
      name: req.body.name,
      from: req.body.from,
      to: req.body.to,
      break: req.body.break,
      status: req.body.status
    };

    shift.addedBy = "agency";
    shift.addedId = req.params.loginId;

    if (req.body._id) {
      db.UpdateDocument("shifttemplates", { _id: req.body._id }, shift, {}, (err, result) => {
        if (err) {
          data.response = "Unable to Save Your data Please try Again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Successfully Updated";
          res.send(data);
        }
      });
    } else {
      db.InsertDocument("shifttemplates", shift, (err, result) => {
        if (err || !result) {
          data.response = "Unable to Save Your Data Please try Again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
  };

  router.list = (req, res) => {
    var error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    var shiftQuery = [];

    shiftQuery.push({ $match: { addedBy: { $eq: "agency" } } });
    shiftQuery.push({ $match: { addedId: { $eq: req.params.loginId } } });

    shiftQuery.push({ $match: { status: { $exists: true } } });
    if (req.body.status) {
      shiftQuery.push({ $match: { status: { $eq: req.body.status } } });
    }
    if (req.body.search) {
      var searchs = req.body.search;
      if (req.body.filter === "all") {
        shiftQuery.push({
          $match: {
            $or: [{ from: { $regex: searchs + ".*", $options: "si" } }, { to: { $regex: searchs + ".*", $options: "si" } }, { break: { $regex: searchs + ".*", $options: "si" } }, { name: { $regex: searchs + ".*", $options: "si" } }]
          }
        });
      } else {
        var searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        shiftQuery.push({ $match: searching });
      }
    }
    if (req.body.field && req.body.order) {
      var sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      shiftQuery.push({ $sort: sorting });
    } else {
      shiftQuery.push({ $sort: { createdAt: -1 } });
    }

    shiftQuery.push({ $project: { createdAt: 1, updatedAt: 1, status: 1, name: 1, from: 1, to: 1, break: 1 } });

    // var withoutlimit = JSON.parse(JSON.stringify(shiftQuery));
    // withoutlimit.push({
    //     $count:'count'
    // });

    var withoutlimit = Object.assign([], shiftQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      shiftQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      shiftQuery.push({ $limit: parseInt(req.body.limit) });
    }

    var finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          result: shiftQuery
        }
      }
    ];

    db.GetAggregation("shifttemplates", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, finalQuery: finalQuery };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    req.checkBody("id", "Invalid id").notEmpty();

    var error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    db.GetDocument("shifttemplates", { _id: req.body.id }, {}, {}, (err, shiftData) => {
      if (err || !shiftData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: shiftData };
        res.send(data);
      }
    });
  };

  /* router.shifttemplateexport = function(req, res) {
    var data = {};
    var errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    var shiftQuery = [];

    shiftQuery.push({ $match: { status: { $exists: true } } });

    if (req.body.field && req.body.order) {
      var sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      shiftQuery.push({ $sort: sorting });
    } else {
      shiftQuery.push({ $sort: { createdAt: -1 } });
    }

    db.GetAggregation("shifttemplates", shiftQuery, function(err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          var fields = ["createdAt", "name", "from", "to", "break", "status"];
          var fieldNames = ["Date", "Shift Name", "Start Time", "End Time", "Break", "Status"];

          for (var i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");
            if (docData[i].status == 0) {
              docData[i].status = "In-active";
            } else if (docData[i].status == 1) {
              docData[i].status = "Active";
            }
          }

          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(docData);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */

  router.shifttemplateexport = function (req, res) {
    var data = {};
    data.status = 0;

    var shiftQuery = [];
    shiftQuery.push({ $match: { addedBy: { $eq: "agency" } } });
    shiftQuery.push({ $match: { addedId: { $eq: req.params.loginId } } });
    shiftQuery.push({ $match: { status: { $exists: true } } });

    db.GetAggregation("shifttemplates", shiftQuery, function (err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData && docData.length > 0) {
          var fieldNames = ["Shift Name", "Start Time", "End Time", "Break", "Status"];
          let finaldata = [];
          const time = (from) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const start_split = (from / 3600).toString().split(".");
            const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
            const starttimehr = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
            /* let total = (from / 3600).toFixed(2);
            let split = total.split('.');
            const totalhours = `${split[0]}:` + split[1] || `00`; */
            return starttimehr;
          };
          for (var i = 0; i < docData.length; i++) {
            let temp = {};
            if (docData[i].status == 0) {
              docData[i].status = "In-active";
            } else if (docData[i].status == 1) {
              docData[i].status = "Active";
            }
            temp["Shift Name"] = docData[i].name || "";
            temp["Start Time"] = time(docData[i].from) || "";
            temp["End Time"] = time(docData[i].to) || "";
            temp["Break"] = `${docData[i].break} mins` || "";
            temp["Status"] = docData[i].status || "";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  return router;
};
