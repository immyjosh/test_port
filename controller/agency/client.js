const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library");
const attachment = require("../../model/attachments");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const moment = require("moment");
const dashboard = require("../../model/dashboard");
const nodemailer = require("nodemailer");
const mongoose = require("mongoose");
const mail = require("../../model/mail.js");
const pdf = require("html-pdf");
const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };

module.exports = () => {
  const router = {};

  router.save = (req, res) => {


    const data = {};
    data.status = 0;

    req.checkBody("username", "Username Required").notEmpty();
    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("name", "Name Required").notEmpty();
    if (!req.body._id || req.body.password !== "") {
      req.checkBody("password", "Invalid Password").notEmpty();
      req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
    }
    req.checkBody("phone.code", "Phone Number Required").notEmpty();
    req.checkBody("phone.number", "Phone Number Required").notEmpty();
    req.checkBody("address.country", "Address Required").notEmpty();
    req.checkBody("address.zipcode", "Address Required").notEmpty();
    req.checkBody("address.formatted_address", "Address Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();
    req.checkBody("isverified", "Verification Required").notEmpty();
    req.checkBody("locations", "Location Required").notEmpty();
    req.checkBody("company_email", "Company Email Required").notEmpty();
    // req.checkBody('avatar', 'Must select image').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let client = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      phone: req.body.phone,
      status: req.body.status,
      address: req.body.address,
      establishmenttype: req.body.establishmenttype,
      numberofbeds: req.body.numberofbeds,
      patienttype: req.body.patienttype,
      specialrequirements: req.body.specialrequirements,
      shiftpatens: req.body.shiftpatens,
      agreedstaffrates: req.body.agreedstaffrates,
      attain: req.body.attain,
      invoiceaddress: req.body.invoiceaddress,
      invoicephone: req.body.invoicephone,
      invoicefax: req.body.invoicefax,
      invoiceemail: req.body.invoiceemail,
      additionallocations: req.body.additionallocations,
      companyname: req.body.companyname,
      company_email: req.body.company_email,
      fax: req.body.fax,
      isverified: req.body.isverified
      // branches: req.body.branches,
    };
    if (!req.body._id) {
      client = {
        username: req.body.username,
        email: req.body.email,
        name: req.body.name,
        phone: req.body.phone,
        status: req.body.status,
        address: req.body.address,
        establishmenttype: req.body.establishmenttype,
        numberofbeds: req.body.numberofbeds,
        patienttype: req.body.patienttype,
        specialrequirements: req.body.specialrequirements,
        shiftpatens: req.body.shiftpatens,
        agreedstaffrates: req.body.agreedstaffrates,
        attain: req.body.attain,
        invoiceaddress: req.body.invoiceaddress,
        invoicephone: req.body.invoicephone,
        invoicefax: req.body.invoicefax,
        invoiceemail: req.body.invoiceemail,
        additionallocations: req.body.additionallocations,
        companyname: req.body.companyname,
        company_email: req.body.company_email,
        fax: req.body.fax,
        isverified: req.body.isverified,
        branches: req.body.branches
      };
    }

    if (req.body.password && req.body.confirm_password) {
      client.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.params.loginId) {
      client.agency = req.params.loginId;
    }
    if (req.body.locations) {
      client.locations = req.body.locations;
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        client.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // client.avatar = req.body.avatar;
    }
    if (req.files && typeof req.files.company_logo !== "undefined") {
      if (req.files.company_logo.length > 0) {
        client.company_logo = attachment.get_attachment(req.files.company_logo[0].destination, req.files.company_logo[0].filename);
      }
    }

    if (req.body._id) {
      db.UpdateDocument("clients", { _id: req.body._id }, client, {}, function (err, result) {
        if (err) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          data.err = err;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Client updated successfully.";
          res.send(data);
        }
      });
    } else {
      db.InsertDocument("clients", client, (err, result) => {
        if (err || !result) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          data.err = err;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Client added successfully.";
          res.send(data);
        }
      });
    }
  };
  router.addbranch = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("clientID", "clientID Required").notEmpty();
    req.checkBody("branchname", "Branchname Required").notEmpty();
    req.checkBody("branchaddress", "Branchaddress Required").notEmpty();
    req.checkBody("branchline1", "Branchline1 Required").notEmpty();
    req.checkBody("branchline2", "Branchline2 Required").notEmpty();
    req.checkBody("branchcity", "Branchcity Required").notEmpty();
    req.checkBody("branchstate", "Branchstate Required").notEmpty();
    req.checkBody("branchcountry", "Branchcountry Required").notEmpty();
    req.checkBody("branchzipcode", "branch Zipcode Required").notEmpty();
    req.checkBody("branchformatted_address", "Branch address Required").notEmpty();
    req.checkBody("branchlat", "Latitude Required").notEmpty();
    req.checkBody("branchlng", "Longitude Required").notEmpty();
    req.checkBody("branchlocation", "Branch Location Required").notEmpty();
    req.checkBody("code", "Phone Required").notEmpty();
    req.checkBody("number", "Phone Required").notEmpty();
    req.checkBody("dailcountry", "Phone Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const branches = {
      branchname: req.body.branchname,
      branchaddress: req.body.branchaddress,
      branchzipcode: req.body.branchzipcode,
      branchline1: req.body.branchline1,
      branchline2: req.body.branchline2,
      branchcity: req.body.branchcity,
      branchstate: req.body.branchstate,
      branchcountry: req.body.branchcountry,
      branchformatted_address: req.body.branchformatted_address,
      branchlat: req.body.branchlat,
      branchlng: req.body.branchlng,
      branchlocation: req.body.branchlocation,
      status: req.body.status,
      phone: { code: req.body.code, number: req.body.number, dailcountry: req.body.dailcountry }
    };

    db.UpdateDocument("clients", { _id: req.body.clientID }, { $push: { branches: branches } }, {}, (err, result) => {
      if (err || !result) {
        data.err = err;
        data.response = "Unable to Save Your Data Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Client added successfully.";
        res.send(data);
      }
    });
  };
  router.deletebranch = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("clientID", "clientID Required").notEmpty();
    req.checkBody("branchID", "branchID Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const branchdata = {
      $set: {
        "branches.$.status": req.body.status
      }
    };
    db.UpdateDocument("clients", { "_id": req.body.clientID, "branches._id": req.body.branchID }, branchdata, (err, result) => {
      if (err || !result) {
        data.err = err;
        data.response = "Unable to Delete";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Deleted successfully.";
        res.send(data);
      }
    });
  };
  router.updatebranch = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("clientID", "clientID Required").notEmpty();
    req.checkBody("branchID", "branchID Required").notEmpty();
    req.checkBody("branchname", "Name Required").notEmpty();
    req.checkBody("branchaddress", "Address Required").notEmpty();
    req.checkBody("branchzipcode", "Zipcode Required").notEmpty();
    req.checkBody("branchline1", "Line1 Required").notEmpty();
    req.checkBody("branchline2", "Line2 Required").notEmpty();
    req.checkBody("branchcity", "City Required").notEmpty();
    req.checkBody("branchstate", "State Required").notEmpty();
    req.checkBody("branchcountry", "Country Required").notEmpty();
    req.checkBody("branchformatted_address", "Addrss Required").notEmpty();
    req.checkBody("branchlat", "lat Required").notEmpty();
    req.checkBody("branchlng", "lng Required").notEmpty();
    req.checkBody("branchlocation", "location Required").notEmpty();
    req.checkBody("code", "Phone Required").notEmpty();
    req.checkBody("number", "Phone Required").notEmpty();
    req.checkBody("dailcountry", "Phone Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const branchdata = {
      $set: {
        "branches.$.branchname": req.body.branchname,
        "branches.$.branchaddress": req.body.branchaddress,
        "branches.$.branchzipcode": req.body.branchzipcode,
        "branches.$.branchline1": req.body.branchline1,
        "branches.$.branchline2": req.body.branchline2,
        "branches.$.branchcity": req.body.branchcity,
        "branches.$.branchstate": req.body.branchstate,
        "branches.$.branchcountry": req.body.branchcountry,
        "branches.$.branchformatted_address": req.body.branchformatted_address,
        "branches.$.branchlat": req.body.branchlat,
        "branches.$.branchlng": req.body.branchlng,
        "branches.$.branchlocation": req.body.branchlocation,
        "branches.$.status": req.body.status,
        "branches.$.phone": { code: req.body.code, number: req.body.number, dailcountry: req.body.dailcountry }
      }
    };
    db.UpdateDocument("clients", { "_id": req.body.clientID, "branches._id": req.body.branchID }, branchdata, (err, result) => {
      if (err || !result) {
        data.err = err;
        data.response = "Unable to Update Details";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated successfully.";
        res.send(data);
      }
    });
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    userQuery.push({ $match: { status: { $exists: true } } });
    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }
    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({
      $lookup: {
        from: "stats",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$client", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
          { $project: { client: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
          { $group: { _id: "$client", count: { $sum: '$hours' } } },
          { $project: { total: { $divide: ['$count', 3600] } } },
        ],
        as: "hours"
      }
    });

    userQuery.push({
      $lookup: {
        from: "shifts",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$client", "$$agencyid"] }] } } },
          { $sort: { 'createdAt': -1 } },
          { $limit: 1 },
          { $project: { start_date: 1 } }
          /* { $project: { employee: 1, hours: { $subtract: ["$starttime", "$endtime"] } } },
          { $group: {_id: "$employee", count: {$sum: '$hours'}}},
          { $project: { total: {$divide: ['$count', 3600]}}}, */
        ],
        as: "lastshift"
      }
    });

    userQuery.push({
      $project: {
        createdAt: 1,
        updatedAt: 1,
        username: 1,
        email: 1,
        status: 1,
        address: 1,
        phone: 1,
        name: 1,
        locations: 1,
        isverified: 1,
        avatar: 1,
        companyname: 1,
        hours: '$hours.total',
        lastshift: '$lastshift.start_date'
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lng",
        label: { $substr: ["$name", 0, 1] },
        avatar: "$avatar"
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { establishmenttype: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];

    db.GetAggregation("clients", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.err = err;
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status(
            {
              type: "agency",
              agency: req.params.loginId,
              action: req.body.notification
            },
            function (err, response) { }
          );
        }

        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          userQuery: userQuery
        };
        res.send(data);
      }
    });
  };
  router.get = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid Id").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const populate = {};
    if (req.body.pagefor && req.body.pagefor == "view") {
      populate.populate = "locations";
    }

    db.GetDocument("clients", { _id: req.body.id }, {}, populate, (err, clientData) => {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: clientData };
        res.send(data);
      }
    });
  };

  router.viewclientlocations = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("locations", "Invalid Id").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    db.GetDocument("locations", { _id: { $in: req.body.locations } }, { name: 1, _id: 1 }, {}, (err, locations) => {
      if (err || !locations) {
        data.response = "Unable to get Data";
        data.status = 0;
        res.send(data);
      } else {
        data.response = { result: locations };
        data.status = 1;
        res.send(data);
      }
    });
  };

  router.userexport = (req, res) => {
    const data = {};
    data.status = 0;
    const userQuery = [];
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    userQuery.push({ $match: { status: { $exists: true } } });
    userQuery.push(
      {
        $lookup: {
          from: "stats",
          let: { agencyid: "$_id" },
          pipeline: [
            { $match: { $expr: { $and: [{ $eq: ["$client", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
            { $project: { client: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
            { $group: { _id: "$client", count: { $sum: '$hours' } } },
            { $project: { total: { $divide: ['$count', 3600] } } },
          ],
          as: "hours"
        }
      }
    );

    userQuery.push(
      {
        $lookup: {
          from: "shifts",
          let: { agencyid: "$_id" },
          pipeline: [
            { $match: { $expr: { $and: [{ $eq: ["$client", "$$agencyid"] }] } } },
            { $sort: { 'createdAt': -1 } },
            { $limit: 1 },
            { $project: { start_date: 1 } }
            /* { $project: { employee: 1, hours: { $subtract: ["$starttime", "$endtime"] } } },
            { $group: {_id: "$employee", count: {$sum: '$hours'}}},
            { $project: { total: {$divide: ['$count', 3600]}}}, */
          ],
          as: "lastshift"
        }
      }
    );

    userQuery.push(
      {
        $project: {
          createdAt: 1,
          updatedAt: 1,
          username: 1,
          email: 1,
          status: 1,
          address: 1,
          phone: 1,
          name: 1,
          locations: 1,
          isverified: 1,
          avatar: 1,
          companyname: 1,
          hours: '$hours.total',
          lastshift: '$lastshift.start_date'
        }
      }
    );

    userQuery.push(
      {
        $addFields: {
          lat: "$address.lat",
          lng: "$address.lng",
          label: { $substr: ["$name", 0, 1] },
          avatar: "$avatar"
        }
      }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("clients", userQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Name","Contact","Tel","Area","Status","Hours","Last Shift"];
          const mydata = docdata;
          let finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            let temp = {};
            temp["Name"] = mydata[i].companyname || "";
            temp["Contact"] = mydata[i].name || "";
            temp["Tel"] = (mydata[i].phone ? (mydata[i].phone.code || "")+"-"+(mydata[i].phone.number || "") : "");
            temp["Area"] = mydata[i].locations ? mydata[i].locations.join() : "";
            temp["Status"] = mydata[i].status == 1 ? "Active" : "In-Active";
            temp["Hours"] = (mydata[i].hours && mydata[i].hours.length>0) ? (mydata[i].hours[0] || "0") : "0";
            temp["Last Shift"] = (mydata[i].lastshift && mydata[i].lastshift.length>0) ? (moment(mydata[i].lastshift[0]).format("DD-MM-YYYY") || "-") : "-";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  /* router.userexport = (req, res) => {
    const data = {};
    const bannerQuery = [
      {
        // "$match": { status: { $ne: 0 } }
        $match: { status: { $exists: true } }
      },
      {
        $project: {
          username: 1,
          email: 1,
          name: 1,
          createdAt: 1,
          phone: 1,
          address: 1,
          status: 1
        }
      },
      {
        $project: {
          document: "$$ROOT"
        }
      },
      {
        $group: { _id: null, count: { $sum: 1 }, documentData: { $push: "$document" } }
      }
    ];
    db.GetAggregation("clients", bannerQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata.length != 0) {
          const fields = [
            "createdAt",
            "username",
            "name",
            "email",
            "phone.code",
            "phone.number",
            "address.line1",
            "address.line2",
            "address.city",
            "address.state",
            "address.zipcode",
            "address.country",
            "address.formatted_address",
            "status"
          ];
          const fieldNames = ["Date", "User Name", "Name", "User mail", "Phone code", "Phone number", "Address 1", "Address 2", "city", "State", "zipcode", "Country", "Full Address", "Status"];
          const mydata = docdata[0].documentData;
          for (let i = 0; i < mydata.length; i++) {
            mydata[i].createdAt = moment(mydata[i].createdAt).format("DD/MM/YYYY");
          }
          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(mydata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */

  router.client_locations = (req, res) => {
    const data = {};
    data.status = 0;

    const clientQuery = [];
    clientQuery.push({ $match: { status: { $eq: 1 } } });
    clientQuery.push({ $match: { agency: { $eq: req.params.loginId } } });

    clientQuery.push({
      $lookup: {
        from: "locations",
        let: { locations: "$locations" },
        pipeline: [
          {
            $match: {
              $expr: {
                $or: [{ $in: ["$_id", "$$locations"] }]
              }
            }
          },
          {
            $match: {
              $expr: {
                $or: [{ $eq: ["$status", 1] }]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "locations"
      }
    });

    clientQuery.push({ $project: { companyname: 1, locations: 1, branches: 1 } });

    db.GetAggregation("clients", clientQuery, (err, docData) => {
      if (err || !docData) {
        data.err = err;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };
  router.actasclient = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetOneDocument("clients", { _id: new mongoose.Types.ObjectId(req.body.id), status: 1, isverified: 1 }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Agency is currently Inactive or not verified";
        res.send(data);
      } else {
        const auth_token = library.jwtSign({ username: user.username, role: "client" });
        data.status = 1;
        data.response = { auth_token: auth_token };
        res.send(data);
      }
    });
  };
  // router.viewbrancheslist = (req, res) => {
  //   const data = {};
  //   data.status = 0;
  //
  //   const errors = req.validationErrors();
  //   if (errors) {
  //     data.response = errors[0].msg;
  //     return res.send(data);
  //   }
  //
  //   const userQuery = [
  //     { $match: { agency: { $eq: req.params.loginId } } },
  //     { $match: { status: { $exists: true } } },
  //     {
  //       $lookup: {
  //         from: "locations",
  //         let: { location_type: "$locations" },
  //         pipeline: [
  //           {
  //             $match: {
  //               $expr: {
  //                 $in: ["$_id", "$$location_type"]
  //               }
  //             }
  //           },
  //           { $project: { name: 1 } }
  //         ],
  //         as: "location_data"
  //       }
  //     },
  //     {
  //       $lookup: {
  //         from: "locations",
  //         let: { location_type: "$branches.branchlocation" },
  //         pipeline: [
  //           {
  //             $match: {
  //               $expr: {
  //                 $in: ["$_id", "$$location_type"]
  //               }
  //             }
  //           },
  //           { $project: { name: 1 } }
  //         ],
  //         as: "branch_data"
  //       }
  //     },
  //     { $addFields: { locations: "$location_data.name" } }
  //   ];
  //   db.GetAggregation("clients", userQuery, (err, docData) => {
  //     if (err || !docData || docData.length <= 0) {
  //       data.err = err;
  //       data.response = "Unable to get Your Data Please try Again";
  //       res.send(data);
  //     } else {
  //       data.status = 1;
  //       data.response = { result: docData[0] };
  //       res.send(data);
  //     }
  //   });
  // };


  router.minidashboard = (req, res) => {
    req.checkBody("client", "Invalid Details").notEmpty();
    req.checkBody("selectshift", "Invalid Details").notEmpty();
    req.checkBody("selecttimesheet", "Invalid Details").notEmpty();
    req.checkBody("selectinvoice", "Invalid Details").notEmpty();

    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let populate = { selectshift: req.body.selectshift, selecttimesheet: req.body.selecttimesheet, selectinvoice: req.body.selectinvoice };
    dashboard.clientdashboardCount({ type: "client", client: new mongoose.Types.ObjectId(req.body.client), populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };

  router.profile = (req, res) => {
    req.checkBody("id", "Invalid Details").notEmpty();
    req.checkBody("purpose", "Invalid Details").notEmpty();
    req.checkBody("email", "Invalid Details").optional();

    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    

    db.GetOneDocument("clients", { '_id': new mongoose.Types.ObjectId(req.body.id) }, {}, { populate: 'locations branches.branchlocation' }, function (err, client) {
      if (err || !client) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        const html = `<style>html {
zoom: 0.55;
}</style><table style="margin: 0; padding: 0; color: #000; background: #fff; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td>
                              <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                                <tbody>
                                  <tr>
                                    <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">Basic
                                      Details</td>
                                  </tr>
                                  <tr>
                                    <td height="10">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                        <tbody>
                                          <tr>
                                            <td width="49%" style="vertical-align: top;">
                                              <img src="${CONFIG.LIVEURL}/${client.avatar}" style=" width: 150px; height: 150px;">
                                            </td>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Username
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${client.username}</span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Name
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${client.name}</span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Phone
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${client.phone.code}-${client.phone.number}</span>
                                              </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0;font-size: 14px;"><span style="width: 50%;float: left; font-weight: bold;">Email
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${client.email}</span>
                                              </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Status
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">${client.status == 1 ? "Active" : "In-Active"}</span>
                                              </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">Organisation
                                                      Details
                                                  </td>
                                  </tr>
                                  <tr>
                                    <td height="10">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                        <tbody>
                                          <tr>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Name
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${client.companyname} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Establishment
                                                  Type <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${client.establishmenttype}</span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Type
                                                  of Patients <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${client.patienttype}</span>
                                              </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Logo
                                                <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                                              <img src="${CONFIG.LIVEURL}/${client.company_logo}" style=" width: 120px;">
                                                                          </span> 
                                                                      </p>
                                            </td>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Fax
                                                  Number <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">${client.fax}</span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Number
                                                  of Beds <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${client.numberofbeds} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">
                                                  Special requirements <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span>
                                                <span style="width: 50%; float: left;"> ${client.specialrequirements} </span> </p>

                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">Address</td>
                                  </tr>
                                  <tr>
                                    <td height="10">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                        <tbody>
                                          <tr>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  Search Location <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span
                                                style="width: 50%;float: left;"> ${client.address.formatted_address} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">State
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${client.address.state} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  Post Code <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${client.address.zipcode}</span> </p>
                                            </td>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Line1
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${client.address.line1}</span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Line2
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${client.address.line2} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">
                                                  Country <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${client.address.country} </span> </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">Location
                                                      Details
                                                  </td>
                                  </tr>
                                  <tr>
                                    <td height="10">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                        <tbody>
                                          <tr>
                                            <td style="width: 98%;float:left; vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  Area <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> 
                                                  ${client.locations.map(item => `<span style="float: left;">${item.name} , </span>`).join("")} 
                                              </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      ${client.branches.map((item, i) => `<tr>
                                    <td style="width: 100%; font-size: 15px;color: #009688; padding: 15px 30px;  font-weight: bold;">${i > 0 ? "Other Branch" : "Main Branch"}</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                        <tbody>
                                          <tr>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  Branch Name <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${item.branchname}  </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  Post Code <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${item.branchzipcode}  </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  phone <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${item.phone.code}-${item.phone.number}  </span> </p>
                                            </td>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Address<span
                                                  style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${item.branchaddress} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Area
                                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${item.branchlocation.name}  </span> </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>`).join("")}
                                  <tr>
                                    <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">Billing
                                      Details</td>
                                  </tr>
                                  <tr>
                                    <td height="10">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                        <tbody>
                                          <tr>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                                  Billing Fax <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                                  ${client.invoicefax} </span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Billing
                                                  Phone Number <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span
                                                style="width: 50%;float: left;"> ${client.invoicephone} </span> </p>

                                            </td>
                                            <td width="49%" style="vertical-align: top;">
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Billing
                                                  Address <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${client.invoiceaddress}</span> </p>
                                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Billing
                                                  Email <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                                  ${client.invoiceemail} </span> </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>`;
        if (req.body.purpose == "download") {
          pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
        } else {
          pdf.create(html, pdfoptions).toFile(`./uploads/pdf/${client.name}.pdf`, function (err, document) {
            if (err) {
              data.response = "Unable to Send Mail Please Try Again";
              res.send(data);
            } else {
              if (req.body.email) {
                data.status = 1;
                data.response = "Mail Sent";
                res.send(data);
                const mailOptions = {
                  from: client.email,
                  to: req.body.email,
                  subject: "Client Profile",
                  text: "Please Download the attachment to see the Profile",
                  html: "<b>Please Download the attachment to see the Profile</b>",
                  attachments: [
                    {
                      filename: `${client.name}.pdf`,
                      path: `./uploads/pdf/${client.name}.pdf`,
                      contentType: "application/pdf"
                    }
                  ]
                };
                mail.send(mailOptions, function (err, response) { });
              } else {
                data.response = "Mail Recepient Required";
                res.send(data);
              }
            }
          });
        }
      }
    });
  };

  return router;
};
