const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library.js");
const attachment = require("../../model/attachments.js");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const moment = require("moment");
const dashboard = require("../../model/dashboard");
const nodemailer = require("nodemailer");
const mongoose = require("mongoose");
const mail = require("../../model/mail.js");
const mailcontent = require("../../model/mailcontent");
const Google = require("../../model/googleapis");
const pdf = require("html-pdf");
const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };
const async = require("async");

module.exports = () => {
  const router = {};

  router.save = (req, res) => {
    const data = {};
    data.status = 0;

    // req.checkBody('client', "Client Required").notEmpty();
    req.checkBody("username", "Username Required").notEmpty();
    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("name", "Name Required").notEmpty();
    req.checkBody("surname", "Surname Required").notEmpty();
    if (!req.body._id || req.body.password != "") {
      req.checkBody("password", "Invalid Password").notEmpty();
      req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
    }
    req.checkBody("phone.number", "Phone Number Required").notEmpty();
    req.checkBody("phone.code", "Phone Number Required").notEmpty();
    req.checkBody("address.country", "Address Required").notEmpty();
    req.checkBody("address.zipcode", "Address Required").notEmpty();
    req.checkBody("address.formatted_address", "Address Required").notEmpty();

    req.checkBody("status", "Status Required").notEmpty();
    req.checkBody("job_type", "Jobtype Required").notEmpty();
    req.checkBody("isverified", "Verification Required").notEmpty();
    req.checkBody("locations", "Locations Required").notEmpty();
    // req.checkBody('available','Available Required').notEmpty();
    // req.checkBody('timeoff','Timeoff Required').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const employee = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      surname: req.body.surname,
      phone: req.body.phone,
      status: req.body.status,
      address: req.body.address,
      job_type: req.body.job_type,
      holiday_allowance: req.body.holiday_allowance,
      agency_rate: req.body.agency_rate,
      employee_rate: req.body.employee_rate,
      // client_rate: parseInt(req.body.agency_rate) + parseInt(req.body.employee_rate),
      joining_date: req.body.joining_date,
      // final_date: req.body.final_date,
      // available: req.body.available,
      isverified: req.body.isverified,
      locations: req.body.locations,
      // timeoff:req.body.timeoff,
      position: req.body.position,
      staffprofile: req.body.staffprofile,
      employmenthistory: req.body.employmenthistory
    };
    if (req.body.available) {
      employee.available = req.body.available;
    }
    if (req.body.staffprofile) {
      employee.staffprofile = req.body.staffprofile;
    }
    if (req.params.loginId) {
      employee.agency = req.params.loginId;
    }

    if (req.body.password && req.body.confirm_password) {
      employee.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar != "undefined") {
      if (req.files.avatar.length > 0) {
        employee.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // employee.avatar = req.body.avatar;
    }
    if (req.body._id) {
      db.UpdateDocument("employees", { _id: req.body._id }, employee, {}, (err, result) => {
        if (err) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.err = err;
          data.status = 0;
          data.request = req.body;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated!";
          res.send(data);
        }
      });
    } else {
      const options = {};
      options.sort = { createdAt: -1 };
      db.GetDocument("subscriptions", { agency: req.params.loginId, status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
        if (subscriptionData.length >= 1) {
          if (subscriptionData[0].plan.recruitment_module === 0) {
            employee["onlineform.form_status"] = 9;
            employee["onlineform.final_verify_status"] = 1;
            employee["onlineform.recruitment_module"] = 0;
          } else if (subscriptionData[0].plan.recruitment_module === 1) {
            employee["onlineform.form_status"] = 0;
            employee["onlineform.recruitment_module"] = 1;
          }
          db.InsertDocument("employees", employee, (err, result) => {
            if (err || !result) {
              if (err.code === 11000) {
                data.response = "Username/Email is already Exists ";
              } else {
                data.response = "Unable to Save Your Data Please try again";
              }
              data.err = err;
              data.status = 0;
              data.request = req.body;
              res.send(data);
            } else {
              data.status = 1;
              data.response = "Added!";
              res.send(data);
            }
          });
        }
      });
    }
  };
  router.staffsave = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("_id", "ID Required").notEmpty();
    // req.checkBody("staffprofile", "Data Required").notEmpty();
    // req.checkBody('available','Available Required').notEmpty();
    // req.checkBody('timeoff','Timeoff Required').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const staffprofile = req.body.staffprofile;
    if (req.files && typeof req.files.staff_photo != "undefined") {
      if (req.files.staff_photo.length > 0) {
        staffprofile.staff_photo = attachment.get_attachment(req.files.staff_photo[0].destination, req.files.staff_photo[0].filename);
      }
    } else {
      staffprofile.staff_photo = req.body.staff_photo;
    }

    db.UpdateDocument("employees", { _id: req.body._id }, { staffprofile: staffprofile }, {}, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Unable to Save Your Data Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated Successfully.";
        res.send(data);
      }
    });
  };

  /* router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    // userQuery.push({'$match':{'addedBy':{$eq:'agency'}}});
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    userQuery.push({ $match: { status: { $exists: true } } });
    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    // if (req.body.search) {
    //     var searchs = req.body.search;
    //     var searching = {};
    //     searching[req.body.filter] = { $regex: searchs + '.*', $options: 'si' };
    //     userQuery.push({ '$match': searching });
    // }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }
    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({
      $project: {
        createdAt: 1,
        updatedAt: 1,
        username: 1,
        email: 1,
        status: 1,
        address: 1,
        phone: 1,
        name: 1,
        job_type: 1,
        isverified: 1,
        locations: 1,
        avatar: 1,
        holiday_allowance: 1,
        joining_date: 1,
        final_date: 1
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lon",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "job_data"
        }
      },
      { $addFields: { job_type: "$job_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    //  var withoutlimit = JSON.parse(JSON.stringify(userQuery));
    //  withoutlimit.push({
    //      $count:'count'
    //  });

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }
    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];
    db.GetAggregation("employees", finalquery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = { finalquery: finalquery, result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };
        res.send(data);
      }
    });
  }; */

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    // userQuery.push({'$match':{'addedBy':{$eq:'agency'}}});
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    userQuery.push({ $match: { status: { $exists: true } } });
    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    // if (req.body.search) {
    //     var searchs = req.body.search;
    //     var searching = {};
    //     searching[req.body.filter] = { $regex: searchs + '.*', $options: 'si' };
    //     userQuery.push({ '$match': searching });
    // }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }
    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({
      $lookup: {
        from: "stats",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$employee", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
          { $project: { employee: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
          { $group: { _id: "$employee", count: { $sum: "$hours" } } },
          { $project: { total: { $divide: ["$count", 3600] } } }
        ],
        as: "hours"
      }
    });

    userQuery.push({
      $lookup: {
        from: "shifts",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$employee", "$$agencyid"] }] } } },
          { $sort: { createdAt: -1 } },
          { $limit: 1 },
          { $project: { start_date: 1 } }
          /* { $project: { employee: 1, hours: { $subtract: ["$starttime", "$endtime"] } } },
          { $group: {_id: "$employee", count: {$sum: '$hours'}}},
          { $project: { total: {$divide: ['$count', 3600]}}}, */
        ],
        as: "lastshift"
      }
    });

    userQuery.push({
      $project: {
        createdAt: 1,
        updatedAt: 1,
        username: 1,
        email: 1,
        status: 1,
        address: 1,
        phone: 1,
        name: 1,
        job_type: 1,
        isverified: 1,
        locations: 1,
        avatar: 1,
        holiday_allowance: 1,
        joining_date: 1,
        final_date: 1,
        hours: "$hours.total",
        lastshift: "$lastshift.start_date"
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lon",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "job_data"
        }
      },
      { $addFields: { job_type: "$job_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    //  var withoutlimit = JSON.parse(JSON.stringify(userQuery));
    //  withoutlimit.push({
    //      $count:'count'
    //  });

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];
    db.GetAggregation("employees", finalquery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = { finalquery: finalquery, result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };
        res.send(data);
      }
    });
  };

  router.get = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid Id").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const populate = {};
    if (req.body.pagefor && req.body.pagefor == "view") {
      populate.populate = "job_type locations";
    }

    db.GetDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, populate, (err, employeeData) => {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: employeeData };
        res.send(data);
      }
    });
  };
  router.employeerating = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid Id").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const Query = [{ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.id) } } }, { $addFields: { employee_ratings: { $avg: "$agency_rating" } } }, { $addFields: { employee_ratings: { $ifNull: ["$employee_ratings", 0] } } }];

    db.GetAggregation("shifts", Query, (err, employeeData) => {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: employeeData && employeeData.length > 0 ? employeeData[0].employee_ratings || 0 : "" };
        res.send(data);
      }
    });
  };

  router.userexport = (req, res) => {
    const data = {};
    data.status = 0;

    const userQuery = [];

    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });

    userQuery.push({ $match: { status: { $exists: true } } });

    userQuery.push({
      $lookup: {
        from: "stats",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$employee", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
          { $project: { employee: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
          { $group: { _id: "$employee", count: { $sum: "$hours" } } },
          { $project: { total: { $divide: ["$count", 3600] } } }
        ],
        as: "hours"
      }
    });

    userQuery.push({
      $lookup: {
        from: "shifts",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$employee", "$$agencyid"] }] } } },
          { $sort: { createdAt: -1 } },
          { $limit: 1 },
          { $project: { start_date: 1 } }
          /* { $project: { employee: 1, hours: { $subtract: ["$starttime", "$endtime"] } } },
          { $group: {_id: "$employee", count: {$sum: '$hours'}}},
          { $project: { total: {$divide: ['$count', 3600]}}}, */
        ],
        as: "lastshift"
      }
    });

    userQuery.push({
      $project: {
        createdAt: 1,
        updatedAt: 1,
        username: 1,
        email: 1,
        status: 1,
        address: 1,
        phone: 1,
        name: 1,
        job_type: 1,
        isverified: 1,
        locations: 1,
        avatar: 1,
        holiday_allowance: 1,
        joining_date: 1,
        final_date: 1,
        hours: "$hours.total",
        lastshift: "$lastshift.start_date"
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lon",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "job_data"
        }
      },
      { $addFields: { job_type: "$job_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("employees", userQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Name", "Job Role", "Tel", "Area", "Status", "Hours", "Last Shift"];
          const mydata = docdata;
          const finaldata = [];
          /* const hours = hour => {
            if (hour && hour[0]) {
              let total = hour[0].toFixed(2);
              let split = total.split(".");
              let totalhours = `${split[0]}:` + split[1] || `00`;
              return totalhours;
            } else {
              return "0";
            }
          }; */
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Name"] = mydata[i].name || "";
            temp["Job Role"] = mydata[i].job_type.join() || "";
            temp["Tel"] = (mydata[i].phone && (mydata[i].phone.code || "")) + "-" + (mydata[i].phone && (mydata[i].phone.number || ""));
            temp["Area"] = mydata[i].locations.join() || "";
            temp["Status"] = mydata[i].status == 1 ? "Active" : "In-Active";
            temp["Hours"] = mydata[i].hours && mydata[i].hours.length > 0 ? mydata[i].hours[0] || "0" : "0";
            temp["Last Shift"] = mydata[i].lastshift && mydata[i].lastshift.length > 0 ? moment(mydata[i].lastshift[0]).format("DD-MM-YYYY") || "-" : "-";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  /* router.userexport = (req, res) => {
    const data = {};
    const bannerQuery = [
      {
        // "$match": { status: { $ne: 0 } }
        $match: { status: { $exists: true } }
      },
      {
        $project: {
          username: 1,
          email: 1,
          name: 1,
          createdAt: 1,
          phone: 1,
          address: 1,
          status: 1
        }
      },
      {
        $project: {
          document: "$$ROOT"
        }
      },
      {
        $group: { _id: null, count: { $sum: 1 }, documentData: { $push: "$document" } }
      }
    ];
    db.GetAggregation("employees", bannerQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata.length != 0) {
          const fields = [
            "createdAt",
            "username",
            "name",
            "email",
            "phone.code",
            "phone.number",
            "address.line1",
            "address.line2",
            "address.city",
            "address.state",
            "address.zipcode",
            "address.country",
            "address.formatted_address",
            "status"
          ];
          const fieldNames = ["Date", "User Name", "Name", "User mail", "Phone code", "Phone number", "Address 1", "Address 2", "city", "State", "zipcode", "Country", "Full Address", "Status"];
          const mydata = docdata[0].documentData;
          for (let i = 0; i < mydata.length; i++) {
            mydata[i].createdAt = moment(mydata[i].createdAt).format("DD/MM/YYYY");
          }
          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(mydata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */

  router.show_employees = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("employees", userQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        data.status = 1;
        const locations = [];
        docData.map(a => {
          // console.log(a);
          locations.push({
            lat: a.address.lat,
            lng: a.address.lon,
            label: a.name.slice(0, 1),
            title: a.name,
            email: a.email,
            locations: a.locations,
            job_type: a.job_type,
            avatar: a.avatar,
            available: a.available,
            timeoff: a.timeoff,
            phone: a.phone,
            _id: a._id
          });
        });

        data.response = { locations: locations };
        res.send(data);
      }
    });
  };

  router.update_availabilty = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("employee", "Employee Required").notEmpty();
    req.checkBody("UnavailableData", "Date Required").notEmpty();
    // req.checkBody("date", "Date Required").notEmpty();
    // req.checkBody("from", "From time Required").notEmpty();
    // req.checkBody("to", "To time Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const unavailability = [];
    req.body.UnavailableData.map(list => {
      const dayTemp = new Date(list.date);
      const dayCheck = dayTemp.getDay();
      let day = "";
      if (dayCheck == 0) {
        day = "Sunday";
      } else if (dayCheck == 1) {
        day = "Monday";
      } else if (dayCheck == 2) {
        day = "Tuesday";
      } else if (dayCheck == 3) {
        day = "Wednesday";
      } else if (dayCheck == 4) {
        day = "Thursday";
      } else if (dayCheck == 5) {
        day = "Friday";
      } else if (dayCheck == 6) {
        day = "Saturday";
      }
      unavailability.push({
        day: day,
        from: list.from,
        to: list.to,
        date: list.date
      });
    });
    const ResultData = [];
    unavailability &&
      unavailability.length > 0 &&
      unavailability.map((list, key) => {
        db.UpdateDocument("employees", { _id: req.body.employee }, { $push: { timeoff: list } }, {}, (err, result) => {
          if (err) {
            data.response = "Unable to Save Your Data Please try again";
            data.err = err;
            data.request = req.body;
            res.send(data);
          } else {
            ResultData.push(result);
            if (unavailability.length === ResultData.length) {
              data.status = 1;
              data.response = "employee updated successfully.";
              res.send(data);
            }
          }
        });
      });
  };

  router.delete_availabilty = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("index", "Index Required").notEmpty();
    req.checkBody("employee", "Employee ID Required").notEmpty();

    db.GetOneDocument("employees", { _id: req.body.employee }, {}, {}, function(err, employeeData) {
      if (err) {
        res.send(err);
      } else {
        const timeoff = Object.assign([], employeeData.timeoff);
        delete timeoff[req.body.index];
        const newtimeoff = timeoff.filter(x => x != null);

        db.UpdateDocument("employees", { _id: req.body.employee }, { timeoff: newtimeoff }, {}, (err, result) => {
          if (err) {
            data.response = "Unable to Save Your Data Please try again";
            data.err = err;
            data.request = req.body;
            res.send(data);
          } else {
            data.status = 1;
            data.response = "employee updated successfully.";
            res.send(data);
          }
        });
      }
    });
  };
  router.actasemployee = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id), status: 1, isverified: 1 }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Agency is currently Inactive or not verified";
        res.send(data);
      } else {
        // const auth_token = library.jwtSign({ username: user.username, role: "employee" });
        // data.status = 1;
        // data.response = { auth_token: auth_token };
        // res.send(data);
        const options = {};
        options.sort = { createdAt: -1 };
        db.GetDocument("subscriptions", { agency: user.agency, status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
          if (subscriptionData.length >= 1) {
            data.status = 1;
            const auth_token = library.jwtSign({
              username: user.username,
              role: "employee",
              subscription: 1,
              employee_count: subscriptionData[0].plan.employees,
              recruitment_module: subscriptionData[0].plan.recruitment_module
            });
            data.response = { auth_token: auth_token };
            data.auth = { username: user.username, role: "employee", subscription: 1, employee_count: subscriptionData[0].plan.employees, recruitment_module: subscriptionData[0].plan.recruitment_module };
            res.send(data);
          } else {
            data.status = 1;
            const auth_token = library.jwtSign({ username: user.username, role: "employee", subscription: 0, employee_count: 0, recruitment_module: 0 });
            data.response = { auth_token: auth_token };
            data.auth = { username: user.username, role: "employee", subscription: 0, employee_count: 0 };
            res.send(data);
          }
        });
      }
    });
  };
  router.downloadpdf = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid Id").notEmpty();
    req.checkBody("client_email", "Email Address Required").optional();
    req.checkBody("email_subject", "Invalid Subject").optional();
    req.checkBody("email_content", "Invalid Content").optional();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const userQuery = [];
    userQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );
    userQuery.push({
      $lookup: {
        from: "agencies",
        let: { agency: new mongoose.Types.ObjectId(req.params.loginId) },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$agency"]
              }
            }
          },
          { $project: { name: 1, email: 1 } }
        ],
        as: "agency_data"
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );
    db.GetAggregation("employees", userQuery, (err, employeeData) => {
      if (err || !employeeData) {
        res.send(err);
      } else {
        if (employeeData && employeeData.length > 0) {
          const employee = employeeData && employeeData[0];
          
          const html = `
        <style>html {
zoom: 0.55;
}</style>
        <table style="margin: 0; padding: 0; color: #000; background: #fff;  padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td>
              <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>

                  <tr>
        <td>
        <table style="margin: 0px auto; padding: 0 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
          <tbody>
          <tr>
             <td width="30%">
              <img src="${CONFIG.LIVEURL}/uploads/logo.png">
           </td>
           <td width="70%" style="background-image:url(${CONFIG.LIVEURL}/uploads/staff-profile.png);background-repeat: no-repeat;background-size: cover;height: 124px;font-size: 24px;color: #fff;    padding: 0px 0 0 70px; font-weight: 600;">
               Staff Profile
           </td>
          </tr>
        </tbody>
        </table>
      </td>
      </tr>


                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                        <tbody>

                          <tr>
                            <td width="75%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:20%;float:left;font-size: 12px; ">Name</label>
                                
            <input type="text" value="${" " + employee.name}" style="width:80%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"><label
                                style="width:20%;float:left;font-size: 12px; ">Preferred Name</label>
            <input type="text" value="${" " + employee.username}" style="width:80%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>
            <p align="left" style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%;"><label
                                style="width:50%;float:left;font-size: 12px; ">Job Role</label>

                                  <input type="text" value="${" " + employee.job_type[0].name}" style="width:46%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                              
                                </p>  
            <p align="right" style="font-weight: 600; float:left; margin:0px; padding: 5px 
            0;width:50%">
            <label
                                style="width:50%;float:left;font-size: 12px;text-align:left; ">Grade</label>
            <input type="text" value="${" " + employee.staffprofile.staff_grade }" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>  
            <p align="left" style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%;"><label
                                style="width:50%;float:left;font-size: 12px; ">DOB</label>
            <input type="text" value="${" " + employee.staffprofile.staff_dateofbirth &&
              moment(new Date(employee.staffprofile.staff_dateofbirth)).format("DD-MM-YYYY")}" style="width:46%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>
            <p align="right" style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%">
            <label
                                style="width:50%;float:left;font-size: 12px;text-align:left; ">Gender</label>
            <input type="text" value="${" " +employee.staffprofile.staff_gender === "female" ? "Female" : employee.staffprofile.staff_gender === "male" ? "Male" : ""}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>
                            
                            </td>
                            <td width="24%" align="right">
                              <img src="" style=" width: 120px; height: 120px;">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      NMC Details </td>
                  </tr>

                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">PIN No</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_pinno}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
                            <td width="40%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Expiry Date</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_nmc_expiry &&
              moment(new Date(employee.staffprofile.staff_nmc_expiry)).format("DD-MM-YYYY")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
                            <td width="10%">
                             
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
      
                 <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      DBS Details </td>
                  </tr>
      
       <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Certificate No:</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_dbs_certificate}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Disclosure Employer Name: </label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_disclosure_name}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
                            <td width="40%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Issue Date</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_dbs_issue_date &&
              moment(new Date(employee.staffprofile.staff_dbs_issue_date)).format("DD-MM-YYYY")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Online Check Status:</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_online_status == "yes" ? "checked" : ""}> Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_online_status == "no" ? "checked" : ""}> No</label>
            
            </span>
            </p>
                            </td>
          <td width="10%">
                             
                            </td>
                           
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      Checks & Verification </td>
                  </tr>
      
      <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Right to Work in UK</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_workinuk == "yes" ? "checked" : ""} name="">Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_workinuk == "no" ? "checked" : ""} name="">No</label>
            
            </span>
            </p>
             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">2 Reference on file</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_reference == "yes" ? "checked" : ""} name="" > Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_reference == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Face to Face Interview</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_face_interview == "yes" ? "checked" : ""} name="" > Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_face_interview == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
                            </td>
                            <td width="40%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Skills Check</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_skill_check == "yes" ? "checked" : ""} name=""> Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_skill_check == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
            
            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Qualification Verified</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_qualification_verify == "yes" ? "checked" : ""} name=""> Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_qualification_verify == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
            
                            </td>
          <td width="10%">
                             
                            </td>
                           
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
                 <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                     Education/Qualifications </td>
                  </tr>
      <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      <textarea style="width:90%;height:100px; border: 1px solid #e2e5f1;background-color: #e2e5f1;" type="command" id=""  name="command">${" " + employee.staffprofile.education_qualification}</textarea>
                  </tr>

                  <tr>
                      <td height="160"></td>
                  </tr>
      <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                    Mandatory training </td>
                  </tr>
      
       <tr>
                    <td>
                      <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%" style="vertical-align:top;">
                               
              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
              <thead>
                <tr>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;">Mandatory training</th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;"></th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;">Date Completed</th>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Health and Safety</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" id=""  ${employee.staffprofile.healthsafety == "true" ? "checked" : ""} name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.health_safety_date && moment(new Date(employee.staffprofile.health_safety_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Fire Safety</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.firesafety == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.fire_safety_date && moment(new Date(employee.staffprofile.fire_safety_date)).format("DD-MM-YYYY")}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Food Safety & Nutrition</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.foodsafety == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.food_safety_date && moment(new Date(employee.staffprofile.food_safety_date)).format("DD-MM-YYYY")}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Basic life support</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.basiclifesupport == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.basiclife_support_date &&
                    moment(new Date(employee.staffprofile.basiclife_support_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Equality & Inclusion</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.equalityinclusion == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.equalityinclusion_date &&
                    moment(new Date(employee.staffprofile.equalityinclusion_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Conflict Management</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.conflictmanagement == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.conflict_mgn_date && moment(new Date(employee.staffprofile.conflict_mgn_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Managing Challenging Behaviour</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.challengingbehaviour == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.challenging_behaviour_date &&
                    moment(new Date(employee.staffprofile.challenging_behaviour_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Mental Capacity Act 2005</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.mentalcapacity == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.mental_capacity_date &&
                    moment(new Date(employee.staffprofile.mental_capacity_date)).format("DD-MM-YYYY")}</td>
                </tr>
              </thead>
               </table>	
                          
                              
                            </td>
                            <td width="50%" style="vertical-align:top;">
                                <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
              <thead>
                <tr>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;">Mandatory training</th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;background-color: #a5a4a4;"></th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;background-color: #a5a4a4;">Date Completed</th>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">COSHH</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.coshh == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.coshh_end_date && moment(new Date(employee.staffprofile.coshh_end_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Infection Control</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.infectioncontrol == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.infection_control_date &&
                    moment(new Date(employee.staffprofile.infection_control_date)).format("DD-MM-YYYY")}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Moving & Handling</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.movinghandling == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.moving_handling_date &&
                    moment(new Date(employee.staffprofile.moving_handling_date)).format("DD-MM-YYYY")}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Data Protection</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.dataprotection == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.data_protection_date &&
                    moment(new Date(employee.staffprofile.data_protection_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Safeguarding Vulnerable Adults & Children</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.safeguardingvulnerable == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.safeguardingvulnerable_date &&
                    moment(new Date(employee.staffprofile.safeguardingvulnerable_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Lone Worker Training</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" id="" ${employee.staffprofile.loneworkertraining == "true" ? "checked" : ""} name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.loneworkertraining_date &&
                    moment(new Date(employee.staffprofile.loneworkertraining_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Restraint Training</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" id="" ${employee.staffprofile.restrainttraining == "true" ? "checked" : ""} name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.restraint_trg_date && moment(new Date(employee.staffprofile.restraint_trg_date)).format("DD-MM-YYYY")}</td>
                </tr>
               
                 
              </thead>
               </table>	
                            </td>
          
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="90%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:25%;float:left;font-size: 14px; ">Languages Spoken</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_language}" style="width:75%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
             
                            </td>
                            
          <td width="10%">
                             
                            </td>
                           
                          </tr>
          
                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="45%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:25%;float:left;font-size: 14px; ">Religion</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_religion}" style="width:73%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
             
                            </td>
                            
          <td width="45%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:25%;float:left;font-size: 14px; ">Work Experience</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_work_experience}" style="width:75%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
          
          <td width="10%">
                             
                            </td>
                           
                          </tr>
          
                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td style="width: 100%; font-size: 13px;color: #000; padding: 15px 30px;color: #848181;">
                     <i>We confirm that this staff member is not presently under any investigation or suspended by any client that we are currently aware of</i></td>
                  </tr>
      

        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
      </table>  `;

          if (req.body.for === "download") {
            pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
          } else {
            pdf.create(html, pdfoptions).toFile("./uploads/pdf/profile.pdf", (err, document) => {
              if (err) {
                res.send(err);
              } else {
                data.status = 1;
                data.response = "Mail Sent";
                res.send(data);
                const mailOptions = {
                  from: req.params.loginData.company_email || req.body.client_email || employeeData[0].agency_data[0].email,
                  to: req.body.client_email || employeeData[0].agency_data[0].email,
                  subject: req.body.email_subject || "Profile",
                  text: "Please Download the attachment to see Profile",
                  html: req.body.email_content,
                  attachments: [
                    {
                      filename: "User Profile.pdf",
                      path: "./uploads/pdf/profile.pdf",
                      contentType: "application/pdf"
                    }
                  ]
                };
                mail.send(mailOptions, (err, response) => {});
              }
            });
          }
        } else {
          data.status = 0;
          data.response = "Employee Details Not Available";
          res.send(data);
        }
      }
    });
  };

  router.minidashboard = (req, res) => {
    req.checkBody("employee", "Invalid Details").notEmpty();
    req.checkBody("selectshift", "Invalid Details").notEmpty();
    req.checkBody("selecttimesheet", "Invalid Details").notEmpty();
    req.checkBody("selectinvoice", "Invalid Details").notEmpty();

    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const populate = { selectshift: req.body.selectshift, selecttimesheet: req.body.selecttimesheet, selectinvoice: req.body.selectinvoice };
    dashboard.employeedashboardCount({ type: "employee", employee: new mongoose.Types.ObjectId(req.body.employee), populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };

  router.profile = (req, res) => {
    req.checkBody("id", "Invalid Details").notEmpty();
    req.checkBody("purpose", "Invalid Details").notEmpty();
    req.checkBody("email", "Invalid Details").optional();

    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, { populate: "job_type locations" }, function(err, employee) {
      if (err || !employee) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        const time = from => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const start_split = (from / 3600).toString().split(".");
          const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
          const starttimehr = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
          return starttimehr;
          /* let total = (from / 3600).toFixed(2);
          let split = total.split('.');
          const totalhours = `${split[0]}:` + split[1] || `00`;
          return totalhours; */
        };
        const html = `
        <style>
html {
zoom: 0.55;
}
</style>
        <table style="margin: 0; padding: 0; color: #000; background: #fff;  padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td>
              <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>

                  <tr>
        <td>
        <table style="margin: 0px auto; padding: 0 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
          <tbody>
          <tr>
             <td width="30%">
              <img src="${CONFIG.LIVEURL}/uploads/logo.png">
           </td>
           <td width="70%" style="background-image:url(${CONFIG.LIVEURL}/uploads/staff-profile.png);background-repeat: no-repeat;background-size: cover;height: 124px;font-size: 24px;color: #fff;    padding: 0px 0 0 70px; font-weight: 600;">
               Staff Profile
           </td>
          </tr>
        </tbody>
        </table>
      </td>
      </tr>


                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                        <tbody>

                          <tr>
                            <td width="75%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:20%;float:left;font-size: 12px; ">Name</label>
                                
            <input type="text" value="${" " + employee.name}" style="width:80%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"><label
                                style="width:20%;float:left;font-size: 12px; ">Preferred Name</label>
            <input type="text" value="${" " + employee.username}" style="width:80%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>
            <p align="left" style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%;"><label
                                style="width:50%;float:left;font-size: 12px; ">Job Role</label>

                                  <input type="text" value="${" " + employee.job_type[0].name}" style="width:46%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                              
                                </p>  
            <p align="right" style="font-weight: 600; float:left; margin:0px; padding: 5px 
            0;width:50%">
            <label
                                style="width:50%;float:left;font-size: 12px;text-align:left; ">Grade</label>
            <input type="text" value="${" " +employee.staffprofile.staff_grade}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>  
            <p align="left" style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%;"><label
                                style="width:50%;float:left;font-size: 12px; ">DOB</label>
            <input type="text" value="${" " + employee.staffprofile.staff_dateofbirth &&
              moment(new Date(employee.staffprofile.staff_dateofbirth)).format("DD-MM-YYYY")}" style="width:46%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>
            <p align="right" style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%">
            <label
                                style="width:50%;float:left;font-size: 12px;text-align:left; ">Gender</label>
            <input type="text" value="${" " + employee.staffprofile.staff_gender}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                </p>
                            
                            </td>
                            <td width="24%" align="right">
                              <img src="" style=" width: 120px; height: 120px;">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      NMC Details </td>
                  </tr>

                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">PIN No</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_pinno}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
                            <td width="40%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Expiry Date</label>
                                
            <input type="text" value="${" " +employee.staffprofile.staff_nmc_expiry ==='' ? '' : employee.staffprofile.staff_nmc_expiry &&
              moment(new Date(employee.staffprofile.staff_nmc_expiry)).format("DD-MM-YYYY")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
                            <td width="10%">
                             
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
      
                 <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      DBS Details </td>
                  </tr>
      
       <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Certificate No:</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_dbs_certificate}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Disclosure Employer Name: </label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_disclosure_name}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
                            <td width="40%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Issue Date</label>
                                
            <input type="text" value="${" " +employee.staffprofile.staff_dbs_issue_date === '' ? '' : employee.staffprofile.staff_dbs_issue_date &&
              moment(new Date(employee.staffprofile.staff_dbs_issue_date)).format("DD-MM-YYYY") }" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Online Check Status:</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_online_status == "yes" ? "checked" : ""}> Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_online_status == "no" ? "checked" : ""}> No</label>
            
            </span>
            </p>
                            </td>
          <td width="10%">
                             
                            </td>
                           
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      Checks & Verification </td>
                  </tr>
      
      <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Right to Work in UK</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_workinuk == "yes" ? "checked" : ""} name="">Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_workinuk == "no" ? "checked" : ""} name="">No</label>
            
            </span>
            </p>
             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">2 Reference on file</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_reference == "yes" ? "checked" : ""} name="" > Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_reference == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:40%;float:left;font-size: 12px; ">Face to Face Interview</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_face_interview == "yes" ? "checked" : ""} name="" > Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_face_interview == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
                            </td>
                            <td width="40%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Skills Check</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_skill_check == "yes" ? "checked" : ""} name=""> Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_skill_check == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
            
            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:50%;float:left;font-size: 12px; ">Qualification Verified</label>
                                
            <span style="width:50%;font-size: 12px;">
            <label> <input type="radio" ${employee.staffprofile.staff_qualification_verify == "yes" ? "checked" : ""} name=""> Yes</label>
            <label> <input type="radio" ${employee.staffprofile.staff_qualification_verify == "no" ? "checked" : ""} name="" > No</label>
            
            </span>
            </p>
            
                            </td>
          <td width="10%">
                             
                            </td>
                           
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
                 <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                     Education/Qualifications </td>
                  </tr>
      <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                      <textarea style="width:90%;height:100px; border: 1px solid #e2e5f1;background-color: #e2e5f1;" type="command" id=""  name="command">${" " + employee.staffprofile.education_qualification}</textarea>
                  </tr>

                  <tr>
                      <td height="160"></td>
                  </tr>
      <tr>
                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                    Mandatory training </td>
                  </tr>
      
       <tr>
                    <td>
                      <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="50%" style="vertical-align:top;">
                               
              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
              <thead>
                <tr>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;">Mandatory training</th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;"></th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;">Date Completed</th>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Health and Safety</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" id=""  ${employee.staffprofile.healthsafety == "true" ? "checked" : ""} name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${ employee.staffprofile.health_safety_date && moment(new Date(employee.staffprofile.health_safety_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Fire Safety</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.firesafety == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.fire_safety_date && moment(new Date(employee.staffprofile.fire_safety_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Food Safety & Nutrition</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.foodsafety == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.food_safety_date && moment(new Date(employee.staffprofile.food_safety_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Basic life support</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.basiclifesupport == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.basiclife_support_date &&
                    moment(new Date(employee.staffprofile.basiclife_support_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Equality & Inclusion</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.equalityinclusion == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.equalityinclusion_date &&
                    moment(new Date(employee.staffprofile.equalityinclusion_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Conflict Management</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.conflictmanagement == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.conflict_mgn_date && moment(new Date(employee.staffprofile.conflict_mgn_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Managing Challenging Behaviour</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.challengingbehaviour == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.challenging_behaviour_date &&
                    moment(new Date(employee.staffprofile.challenging_behaviour_date)).format("DD-MM-YYYY")}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Mental Capacity Act 2005</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.mentalcapacity == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.mental_capacity_date &&
                    moment(new Date(employee.staffprofile.mental_capacity_date)).format("DD-MM-YYYY")}</td>
                </tr>
              </thead>
               </table>	
                          
                              
                            </td>
                            <td width="50%" style="vertical-align:top;">
                                <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
              <thead>
                <tr>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left; background-color: #a5a4a4;">Mandatory training</th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;background-color: #a5a4a4;"></th>
                  <th style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;background-color: #a5a4a4;">Date Completed</th>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">COSHH</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.coshh == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.coshh_end_date && moment(new Date(employee.staffprofile.coshh_end_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Infection Control</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.infectioncontrol == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.infection_control_date &&
                    moment(new Date(employee.staffprofile.infection_control_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Moving & Handling</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.movinghandling == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.moving_handling_date &&
                    moment(new Date(employee.staffprofile.moving_handling_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Data Protection</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.dataprotection == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.data_protection_date &&
                    moment(new Date(employee.staffprofile.data_protection_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Safeguarding Vulnerable Adults & Children</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" ${employee.staffprofile.safeguardingvulnerable == "true" ? "checked" : ""} id="" name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.safeguardingvulnerable_date &&
                    moment(new Date(employee.staffprofile.safeguardingvulnerable_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Lone Worker Training</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" id="" ${employee.staffprofile.loneworkertraining == "true" ? "checked" : ""} name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.loneworkertraining_date &&
                    moment(new Date(employee.staffprofile.loneworkertraining_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
                 <tr>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">Restraint Training</td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;"> 
                    <input type="checkbox" id="" ${employee.staffprofile.restrainttraining == "true" ? "checked" : ""} name="">
                </td>
                  <td style=" border: 1px solid #ccc; font-size: 13px; padding: 10px;    text-align: left;">${employee.staffprofile.restraint_trg_date && moment(new Date(employee.staffprofile.restraint_trg_date)).format("DD-MM-YYYY") || ""}</td>
                </tr>
               
                 
              </thead>
               </table>	
                            </td>
          
                          </tr>

                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="90%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:25%;float:left;font-size: 14px; ">Languages Spoken</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_language}" style="width:75%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
             
                            </td>
                            
          <td width="10%">
                             
                            </td>
                           
                          </tr>
          
                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>

                          <tr>
                            <td width="45%">
                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:25%;float:left;font-size: 14px; ">Religion</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_religion}" style="width:73%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
             
                            </td>
                            
          <td width="45%">
                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                style="width:25%;float:left;font-size: 14px; ">Work Experience</label>
                                
            <input type="text" value="${" " + employee.staffprofile.staff_work_experience}" style="width:75%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
            </p>
                            </td>
          
          <td width="10%">
                             
                            </td>
                           
                          </tr>
          
                        </tbody>
                      </table>
                    </td>
                  </tr>
      
      <tr>
                    <td style="width: 100%; font-size: 13px;color: #000; padding: 15px 30px;color: #848181;">
                     <i>We confirm that this staff member is not presently under any investigation or suspended by any client that we are currently aware of</i></td>
                  </tr>
      

        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
      </table>
        `;
        if (req.body.purpose == "download") {
          pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
        } else {
          pdf.create(html, pdfoptions).toFile(`./uploads/pdf/${employee.name}.pdf`, function(err, document) {
            if (err) {
              data.response = "Unable to Send Mail Please Try Again";
              res.send(data);
            } else {
              if (req.body.email) {
                data.status = 1;
                data.response = "Mail Sent";
                res.send(data);
                const mailOptions = {
                  from: req.params.loginData.company_email || employee.email,
                  to: req.body.email,
                  subject: req.body.email_subject || "Employee Profile",
                  text: "Please Download the attachment to see the Profile",
                  html: req.body.email_content || "<b>Please Download the attachment to see the Profile</b>",
                  attachments: [
                    {
                      filename: `${employee.name}.pdf`,
                      path: `./uploads/pdf/${employee.name}.pdf`,
                      contentType: "application/pdf"
                    }
                  ]
                };
                mail.send(mailOptions, function(err, response) {});
              } else {
                data.response = "Mail Recepient Required";
                res.send(data);
              }
            }
          });
        }
      }
    });
  };

  router.shiftlist = (req, res) => {
    let data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const listQuery = [];

    listQuery.push({
      $match: {
        $or: [
          // { "status" : 2 },
          {
            employee: { $eq: new mongoose.Types.ObjectId(req.body.id) }
          },
          {
            employee_requested: {
              $elemMatch: {
                employee: new mongoose.Types.ObjectId(req.body.id),
                status: 1
              }
            }
          },
          {
            "employee_requested.employee": {
              $eq: new mongoose.Types.ObjectId(req.body.id)
            },
            "status": 2
          }
          // {"employee_requested.employee": {$eq: new mongoose.Types.ObjectId(req.body.id)}, "status": 3},
        ]
      }
    });
    listQuery.push({ $match: { status: { $ne: 10 } } });
    if (req.body.status) {
      req.body.status = parseInt(req.body.status);
      listQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    if (req.body.from_date) {
      listQuery.push({
        $match: { createdAt: { $gt: new Date(req.body.from_date) } }
      });
    }

    if (req.body.to_date) {
      listQuery.push({
        $match: { createdAt: { $lt: new Date(req.body.to_date) } }
      });
    }

    const withoutlimit = Object.assign([], listQuery);
    withoutlimit.push({ $count: "count" });

    listQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { agencyid: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    listQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { agencyid: req.body.id },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1, address: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
        // $addFields: { "employee": { $arrayElemAt: ["$employee_data.name", 0] } }
      }
    );
    listQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { locations: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$locations"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "locations_data"
        }
      },
      {
        $addFields: { locations: "$locations_data.name" }
      }
    );

    listQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { clientid: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$clientid"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    listQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$location" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { location: "$location_data.name" } }
    );
    listQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch_data"
      }
    });
    if (req.body.from_date) {
      listQuery.push({
        $match: { createdAt: { $gt: new Date(req.body.from_date) } }
      });
    }

    if (req.body.to_date) {
      listQuery.push({
        $match: { createdAt: { $lt: new Date(req.body.to_date) } }
      });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      listQuery.push({ $sort: sorting });
    } else {
      listQuery.push({ $sort: { createdAt: -1 } });
    }
    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        listQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } },
              { distance: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        listQuery.push({ $match: searching });
      }
    }
    if (req.body.skip >= 0) {
      listQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      listQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          groupcount: [
            {
              $match: {
                $or: [
                  {
                    employee: {
                      $eq: new mongoose.Types.ObjectId(req.body.id)
                    }
                  },
                  {
                    "employee_requested.employee": {
                      $eq: new mongoose.Types.ObjectId(req.body.id)
                    },
                    "status": { $eq: 2 }
                  },
                  {
                    "employee_requested.employee": {
                      $eq: new mongoose.Types.ObjectId(req.body.id)
                    },
                    "status": { $eq: 3 }
                  }
                ]
              }
            },
            { $group: { _id: "$status", count: { $sum: 1 } } },
            {
              $addFields: {
                status: {
                  $arrayElemAt: [["added", "added", "requests", "accepted", "assigned", "ongoing", "completed", "timehseet_approved", "invoice_approved", "Payment_completed", "expired"], "$_id"]
                }
              }
            },
            { $addFields: { nott: [["$status", "$count"]] } },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ],
          result: listQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, function(err, docData) {
      if (err || !docData) {
        data.err = err;
        data.response = "unable to get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ employee: req.body.id, action: req.body.notification }, function(err, response) {});
        }
        async.mapSeries(
          docData && docData[0].result,
          (emp, callback) => {
            let distance = "Currently Not Available";
            const bran_data = emp.branch_data && emp.branch_data.length > 0 && emp.branch_data[0].branches;
            data = {
              lat1: emp.employee_data && emp.employee_data.length > 0 && emp.employee_data[0].address.lat,
              lon1: emp.employee_data && emp.employee_data.length > 0 && emp.employee_data[0].address.lon,
              lat2: bran_data.branchlat,
              lon2: bran_data.branchlng
            };
            Google.distancematrix(data, LResult => {
              if (LResult && LResult.rows.length > 0 && LResult.rows[0].elements.length > 0) {
                distance = LResult.rows[0].elements[0].distance && LResult.rows[0].elements[0].distance.text;
              }
              const empobj = {
                _id: emp._id,
                title: emp.title,
                shift_id: emp.shift_id,
                shiftId: emp.shiftId,
                endtime: emp.endtime,
                starttime: emp.starttime,
                client: emp.client,
                breaktime: emp.breaktime,
                end_date: emp.end_date,
                start_date: emp.start_date,
                locations: emp.locations,
                job_type: emp.job_type,
                employee_data: emp.employee_data,
                branch: emp.branch,
                branch_data: emp.branch_data,
                jobtype_data: emp.jobtype_data,
                distance: distance,
                createdAt: emp.createdAt,
                status: emp.status
              };
              if (err) {
                callback(err);
              } else {
                callback(null, empobj);
              }
            });
          },
          (err, results) => {
            if (err) {
              res.send(err);
            } else {
              data.status = 1;
              let fullcount;
              if (docData[0].overall[0] && docData[0].overall[0].count) {
                fullcount = docData[0].overall[0].count;
              } else {
                fullcount = docData[0].result.length;
              }
              // data.response = docData;
              data.response = {};
              if (req.body.field === "distance") {
                if (req.body.order === 1) {
                  data.response.result = results.sort((a, b) => a.distance - b.distance);
                }
                if (req.body.order === -1) {
                  data.response.result = results.sort((a, b) => b.distance - a.distance);
                }
              } else if (req.body.order && req.body.field) {
                data.response.result = results;
              } else {
                data.response.result = results.sort((a, b) => b.createdAt - a.createdAt);
              }
              (data.response.length = docData[0].result.length), (data.response.fullcount = fullcount), (data.response.groupcount = docData[0].groupcount[0]);
              return res.send(data);
            }
          }
        );
      }
    });
  };
  router.view_shift = (req, res) => {
    let data = {};
    data.status = 0;

    req.checkBody("id", "shiftId Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const shiftQuery = [];
    shiftQuery.push({
      $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } }
    });

    // console.log('req.body.id', req.body.id);
    shiftQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { employeeid: req.body.id },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$employeeid"]
                }
              }
            },
            { $project: { name: 1, address: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: {
          employee_avatar: "$employee_data.avatar",
          employee: "$employee_data.name",
          employeeaddress: "$employee_data.address"
        }
      }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );
    shiftQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    shiftQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("shifts", shiftQuery, function(err, shiftData) {
      if (err || !shiftData) {
        data.err = err;
        data.shiftData = shiftData;
        data.response = "Job details not available";
        return res.send(data);
      } else {
        async.mapSeries(
          shiftData,
          (item, callback) => {
            let distance = "Currently Not Available";
            const bran_data = item.branch && item.branch.length > 0 && item.branch[0].branches;
            data = {
              lat1: item.employeeaddress && item.employeeaddress.length > 0 && item.employeeaddress[0].lat,
              lon1: item.employeeaddress && item.employeeaddress.length > 0 && item.employeeaddress[0].lon,
              lat2: bran_data.branchlat,
              lon2: bran_data.branchlng
            };
            Google.distancematrix(data, LResult => {
              if (LResult && LResult.rows.length > 0 && LResult.rows[0].elements.length > 0) {
                distance = LResult.rows[0].elements[0].distance && LResult.rows[0].elements[0].distance.text;
              }
              const empobj = {
                _id: item._id,
                distance: distance,
                option_data: item.option_data,
                latefee: item.latefee,
                timesheet_status: item.timesheet_status,
                agency_rating: item.agency_rating,
                locations: item.locations,
                job_type: item.job_type,
                branch: item.branch,
                title: item.title,
                shiftId: item.shiftId,
                notes: item.notes,
                status: item.status,
                starttime: item.starttime,
                endtime: item.endtime,
                breaktime: item.breaktime,
                start_date: item.start_date,
                end_date: item.end_date,
                shift_type: item.shift_type,
                shift_option: item.shift_option,
                client: item.client,
                agency: item.agency,
                employee_requested: item.employee_requested,
                timesheet: item.timesheet,
                location_data: item.location_data,
                jobtype_data: item.jobtype_data,
                client_data: item.client_data,
                employee_avatar: item.employee_avatar,
                employeeaddress: item.employeeaddress,
                employee_data: item.employee_data,
                employee: item.employee,
                employee_rate: item.employee_rate
              };
              if (err) {
                callback(err);
              } else {
                callback(null, empobj);
              }
            });
          },
          (err, results) => {
            if (err) {
              res.send(err);
            } else {
              data.status = 1;
              data.response = results && results[0];
              return res.send(data);
            }
          }
        );
      }
    });
  };
  router.forshiftslist = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, function(err, EmployeeData) {
      if (err || !EmployeeData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        const daysQuery = [];
        daysQuery.push({ $match: { addedBy: { $eq: "agency" } } });
        daysQuery.push({ $match: { addedId: { $eq: new mongoose.Types.ObjectId(EmployeeData.agency) } } });
        daysQuery.push({ $match: { status: { $exists: true } } });
        daysQuery.push({ $match: { status: { $eq: 1 } } }, { $unwind: "$dates" });
        daysQuery.push({ $sort: { createdAt: -1 } });
        daysQuery.push({ $project: { createdAt: 1, updatedAt: 1, name: 1, activity: 1, status: 1, reason: 1, numberofday: 1, dates: 1 } });

        db.GetAggregation("daysoff", daysQuery, function(err, docData) {
          if (err || !docData) {
            data.response = "Unable to Get Your Data Please Try Again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = { result: docData, daysQuery: daysQuery };
            res.send(data);
          }
        });
      }
    });
  };
  router.employeedashboard = (req, res) => {
    const populate = {
      selectshift: req.body.selectshift,
      selecttimesheet: req.body.selecttimesheet,
      selectinvoice: req.body.selectinvoice,
      start: req.body.startdate,
      end: req.body.enddate,
      id: req.body.id
    };
    const data = {};
    data.status = 0;
    dashboard.employeedashboardCount({ type: "employee", employee: new mongoose.Types.ObjectId(req.body.id), populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };
  return router;
};
