module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library");
  const nodemailer = require("nodemailer");
  const dashboard = require("../../model/dashboard");
  const moment = require("moment");
  const mail = require("../../model/mail.js");
  const mailcontent = require("../../model/mailcontent");
  const async = require("async");
  const attachment = require("../../model/attachments");
  const push = require("../../model/pushNotification.js")(io);
  const mongoose = require("mongoose");
  const event = require("../../controller/events/events");
  const router = {};

  router.agencydashboard = (req, res) => {
    const populate = { selectshift: req.body.selectshift, selecttimesheet: req.body.selecttimesheet, selectinvoice: req.body.selectinvoice, start: req.body.startdate, end: req.body.enddate };
    const data = {};
    data.status = 0;
    dashboard.agencydashboardCount({ type: "agency", agency: req.params.loginId, populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };

  router.notifications = (req, res) => {
    const data = {};
    data.status = 0;

    dashboard.notifications({ type: "agency", agency: req.params.loginId }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        res.send(response);
      }
    });
  };

  router.login = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("password", "Password Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    const email = req.body.email;

    const password = req.body.password;

    db.GetOneDocument("agencies", { $or: [{ email: email, status: 1 }, { username: email, status: 1 }] }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Invalid Credentials";
        res.send(data);
      } else {
        if (library.validPassword(password, user.password)) {
          db.GetOneDocument("agencies", { $or: [{ email: email, status: 1, isverified: 1 }, { username: email, status: 1, isverified: 1 }] }, {}, {}, (err, user) => {
            if (err || !user) {
              data.response = "Your account is not verified.";
              res.send(data);
            } else {
              const updata = { activity: {} };
              updata.activity.last_login = Date();
              db.UpdateDocument("agencies", { _id: user._id }, updata, {}, (err, docdata) => {
                if (err) {
                  data.response = "Invalid Credentials";
                  res.send(data);
                } else {
                  // var auth_token = library.jwtSign({ username: user.username , role : "agency" });
                  // , 'end': { $gte: new Date() }
                  const options = {};
                  options.sort = { createdAt: -1 };
                  db.GetDocument("subscriptions", { agency: user._id, status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
                    if (subscriptionData.length >= 1) {
                      data.status = 1;
                      const auth_token = library.jwtSign({
                        username: user.username,
                        role: "agency",
                        subscription: 1,
                        employee_count: subscriptionData[0].plan.employees,
                        recruitment_module: subscriptionData[0].plan.recruitment_module
                      });
                      data.response = { auth_token: auth_token };
                      data.auth = { username: user.username, role: "agency", subscription: 1, employee_count: subscriptionData[0].plan.employees, recruitment_module: subscriptionData[0].plan.recruitment_module };
                      res.send(data);
                    } else {
                      data.status = 1;
                      const auth_token = library.jwtSign({ username: user.username, role: "agency", subscription: 0, employee_count: 0, recruitment_module: 0 });
                      data.response = { auth_token: auth_token };
                      data.auth = { username: user.username, role: "agency", subscription: 0, employee_count: 0 };
                      res.send(data);
                    }
                    event.emit("agencyemailtemplates", { agency: user._id});
                  });
                }
              });
            }
          });
        } else {
          data.response = "Invalid Credentials";
          res.send(data);
        }
      }
    });
  };

  router.logout = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Username Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.UpdateDocument("agencies", { username: req.body.username }, { "activity.last_logout": new Date() }, {}, (err, response) => {
      if (err) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "logged out";
        res.send(data);
      }
    });
  };

  router.forgotpassword = (req, res) => {
    const data = {};
    const request = {};
    req.checkBody("email", "Invalid email").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    request.email = req.body.email;
    request.reset = library.randomString(8, "#A");

    async.waterfall(
      [
        callback => {
          db.GetOneDocument("agencies", { email: request.email }, {}, {}, (err, agency) => {
            callback(err, agency);
          });
        },
        (agency, callback) => {
          if (agency) {
            db.UpdateDocument("agencies", { _id: agency._id }, { reset_code: request.reset }, {}, (err, response) => {
              callback(err, agency);
            });
          } else {
            callback(null, agency);
          }
        },
        (agency, callback) => {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, settings) => {
            if (err) {
              callback(err, callback);
            } else {
              callback(err, agency, settings);
            }
          });
        }
      ],
      (err, agency, settings) => {
        if (err || !agency) {
          data.status = "0";
          data.response = "Errror!";
          res.status(400).send(data);
        } else {
          let name;
          if (agency.name) {
            name = agency.name + " (" + agency.username + ")";
          } else {
            name = agency.username;
          }
          const agencyId = agency._id;
          const mailData = {};
          mailData.template = "forgotpassword";
          mailData.to = agency.email;
          mailData.html = [];
          mailData.html.push({ name: "name", value: name });
          mailData.html.push({ name: "site_url", value: settings.settings.site_url });
          mailData.html.push({ name: "logo", value: settings.settings.logo });
          mailData.html.push({ name: "email", value: agency.email });
          mailData.html.push({ name: "url", value: settings.settings.site_url + "portal/agency/resetpassword" + "/" + agencyId + "/" + request.reset });
          mailcontent.sendmail(mailData, (err, response) => {});
          data.status = "1";
          data.url = settings.settings.site_url + "portal/agency/resetpassword" + "/" + agencyId + "/" + request.reset;
          data.response = "Reset Code Sent Successfully!";
          res.send(data);
        }
      }
    );
  };

  router.resetpassword = (req, res) => {
    const id = req.params.userid;
    const resetid = req.params.reset;
    const data = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

    db.UpdateDocument("agencies", { _id: id, reset_code: resetid }, { password: data }, {}, (err, docdata) => {
      if (err || docdata.nModified == 0) {
        var data = {};
        data.status = "0";
        data.response = "Reset Password error!";
        res.send(data);
      } else {
        data = {};
        data.status = "1";
        data.response = "Reset Successfully!";
        res.send(data);
      }
    });
  };

  router.profile = (req, res) => {
    const data = {};
    data.status = 0;
    db.GetDocument("agencies", { _id: req.params.loginId }, {}, {}, (err, agencyData) => {
      if (err || !agencyData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: agencyData };
        res.send(data);
      }
    });
  };

  router.saveprofile = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("email", "Invalid Email").notEmpty();
    req.checkBody("name", "Invalid Name").notEmpty();
    req.checkBody("phone.code", "Invalid Number").notEmpty();
    req.checkBody("phone.number", "Invalid Number").notEmpty();
    // req.checkBody('avatar','Must select image').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const agency = {
      email: req.body.email,
      name: req.body.name,
      surname: req.body.surname,
      status: req.body.status,
      phone: req.body.phone
    };

    if (req.body.password != "") {
      req.checkBody("password", "Invalid Password").notEmpty();
      req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
    }
    if (req.body.password && req.body.confirm_password) {
      agency.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        agency.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // agency.avatar = req.body.avatar;
    }

    db.UpdateDocument("agencies", { _id: req.params.loginId }, agency, { upsert: true }, (err, result) => {
      if (err) {
        if (err.code === 11000) {
          data.response = "Username/Email is already Exists ";
        } else {
          data.response = "Unable to Save Your Data Please try again";
        }
        data.status = 0;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated successfully.";
        res.send(data);
      }
    });
  };
  router.deactivateprofile = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const agency = {
      status: req.body.status
    };

    db.UpdateDocument("agencies", { _id: req.params.loginId }, agency, { upsert: true }, (err, result) => {
      if (err) {
        if (err.code === 11000) {
          data.response = "Username/Email is already Exists ";
        } else {
          data.response = "Unable to Save Your Data Please try again";
        }
        data.status = 0;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated successfully.";
        res.send(data);
      }
    });
  };
  router.savecomapny = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("address.country", "Invalid Address").notEmpty();
    req.checkBody("address.zipcode", "Invalid Address").notEmpty();
    req.checkBody("address.formatted_address", "Invalid Address").notEmpty();
    // req.checkBody('avatar','Must select image').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const agency = {
      company_name: req.body.company_name,
      company_phone: req.body.company_phone,
      company_description: req.body.company_description,
      postal_address: req.body.postal_address,
      registration_number: req.body.registration_number,
      organisation_type: req.body.organisation_type,
      company_email: req.body.company_email,
      fax: req.body.fax,
      bank_details: {
        name: req.body.bank_name,
        ac_no: req.body.bank_ac,
        ac_name: req.body.bank_ac_name,
        sort_code: req.body.bank_sort_code,
        // becs: req.body.bank_becs,
        // sepa: req.body.bank_sepa,
      }
    };
    if (req.body.vat_number) {
      agency.vat_number = req.body.vat_number;
    }
    if (req.files && typeof req.files.company_logo !== "undefined") {
      if (req.files.company_logo.length > 0) {
        agency.company_logo = attachment.get_attachment(req.files.company_logo[0].destination, req.files.company_logo[0].filename);
      }
    }
    db.UpdateDocument("agencies", { _id: req.params.loginId }, agency, { upsert: true }, (err, result) => {
      if (err) {
        if (err.code === 11000) {
          data.response = "Username/Email is already Exists ";
        } else {
          data.response = "Unable to Save Your Data Please try again";
        }
        data.status = 0;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated successfully.";
        res.send(data);
      }
    });
  };
  router.settings = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("mailstatus", "Required Email Status").optional();
    req.checkBody("smsstatus", "Required Name Status").optional();
    if (req.body.for === "shift") {
      req.checkBody("prefix", "Prefix Required Minimum 3 Letters").isLength({ min: 3 });
      req.checkBody("number", "Number Required Minimum 3 Numbers").isLength({ min: 3 });
    } else if (req.body.for === "timesheet") {
      req.checkBody("prefix", "Prefix Required Minimum 4 Letters").isLength({ min: 4 });
      req.checkBody("number", "Number Required Minimum 4 Numbers").isLength({ min: 4 });
    } else if (req.body.for === "invoice") {
      req.checkBody("prefix", "Prefix Required Minimum 5 Letters").isLength({ min: 5 });
      req.checkBody("number", "Number Required Minimum 5 Numbers").isLength({ min: 5 });
    }
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    let agency;
    let Data;
    let query;
    agency = {
      prefix: req.body.prefix,
      number: req.body.number
    };
    if (req.body.for === "notifications") {
      const notifications = {
        email: req.body.mailstatus,
        sms: req.body.smsstatus
      };
      Data = { $set: { "settings.notifications": notifications } };
    } else if (req.body.for === "general") {
      const general = {
        shift_request_expiry: req.body.shift_request_expiry,
        cancel_shift: req.body.cancel_shift,
        invoice_due_days: req.body.invoice_due_days
      };
      Data = { $set: { "settings.general": general } };
    } else if (req.body.for === "shift") {
      query = { "settings.shift.prefix": req.body.prefix };
      Data = { $set: { "settings.shift": agency } };
    } else if (req.body.for === "timesheet") {
      query = { "settings.timesheet.prefix": req.body.prefix };
      Data = { $set: { "settings.timesheet": agency } };
    } else if (req.body.for === "invoice") {
      query = { "settings.invoice.prefix": req.body.prefix };
      Data = { $set: { "settings.invoice": agency } };
    }
    if (req.body.for === "shift" || req.body.for === "timesheet" || req.body.for === "invoice") {
      db.GetDocument("agencies", query, {}, {}, (err, agencyData) => {
        if (err) {
          data.response = "Unable to get agency data, Please try again";
          res.send(data);
        } else {
          if (agencyData && agencyData.length > 0) {
            data.status = 0;
            data.response = "Please try different Prefix";
            res.send(data);
          } else {
            db.UpdateDocument("agencies", { _id: req.params.loginId }, Data, { upsert: true }, (err, result) => {
              if (err) {
                data.status = 0;
                data.response = "Unable to save settings, Please try again";
                res.send(data);
              } else {
                data.status = 1;
                data.response = "Updated successfully.";
                res.send(data);
              }
            });
          }
        }
      });
    } else {
      db.UpdateDocument("agencies", { _id: req.params.loginId }, Data, { upsert: true }, (err, result) => {
        if (err) {
          data.status = 0;
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated successfully.";
          res.send(data);
        }
      });
    }
  };
  router.clearnotification = (req, res) => {
    const data = {};
    db.UpdateMany("notifications", { agency: req.params.loginId }, { status: 2 }, {}, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Please try again later";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Cleared";
        res.send(data);
      }
    });
  };


  return router;
};
