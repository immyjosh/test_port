module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library");
  const attachment = require("../../model/attachments");
  const mongoose = require("mongoose");
  const push = require("../../model/pushNotification.js")(io);
  const dashboard = require("../../model/dashboard.js");
  const nodemailer = require("nodemailer");
  const moment = require("moment");
  const json2csv = require("json2csv").Parser;
  const fs = require("fs");
  const mail = require("../../model/mail.js");
  const mailcontent = require("../../model/mailcontent");
  const pdf = require("html-pdf");
  const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };
  const async = require("async");
  const event = require("../../controller/events/events");
  const RateCalculation = require("../calculation/rate");

  const router = {};
  const data = {};
  data.status = 0;

  router.save = (req, res) => {
    const data = {};
    data.status = 0;
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const updates = {
      agency_rate: req.body.agency_rate,
      employee_rate: req.body.employee_rate,
      starttime: req.body.starttime,
      endtime: req.body.endtime,
      breaktime: req.body.breaktime,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      agency_comment: req.body.comment,
      agency_rating: req.body.rating
    };

    if (req.body.shiftId) {
      db.UpdateDocument("shifts", { _id: req.body.shiftId }, updates, {}, (err, result) => {
        if (err) {
          data.response = "Unable to Save Your Data Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Shift updated successfully.";
          res.send(data);
        }
      });
    } else {
      data.response = "Shift details not available";
      res.send(data);
    }
  };
  router.list = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const locationQuery = [];
    locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });

    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      locationQuery.push({
        $match: {
          $or: [{ status: 6 }, { status: 7 }]
        }
      });
    }

    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    locationQuery.push({ $project: { employee_requested: 0 } });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      locationQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      locationQuery.push({ $limit: parseInt(req.body.limit) });
    }
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        locationQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { location: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        locationQuery.push({ $match: searching });
      }
    }
    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      locationQuery.push({ $sort: sorting });
    } else {
      locationQuery.push({ $sort: { createdAt: -1 } });
    }
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $match: {
                $or: [{ timesheet_status: 1 }, { timesheet_status: 2 }]
              }
            },
            {
              $group: {
                _id: "$timesheet_status",
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.status = 1;
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          locationQuery: locationQuery
        };

        res.send(data);
      }
    });
  };
  router.listforemp = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    const locationQuery = [];
    locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    locationQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.employeeID) } } });
    locationQuery.push({ $match: { job_type: { $eq: new mongoose.Types.ObjectId(req.body.jobtypeID) } } });
    locationQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.clientID) } } });
    // locationQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // locationQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.branchID)}}});
    locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });

    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      locationQuery.push({ $match: { status: { $gte: 6 } } });
    }
    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, email: 1, address: 1, avatar: 1, company_logo: 1, phone: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1, email:1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, phone: 1, address: 1, avatar: 1, email: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    locationQuery.push({ $project: { employee_requested: 0 } });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });
    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }
    if (req.body.skip >= 0) {
      locationQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      locationQuery.push({ $limit: parseInt(req.body.limit) });
    }
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        locationQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { location: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        locationQuery.push({ $match: searching });
      }
    }
    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      locationQuery.push({ $sort: sorting });
    } else {
      locationQuery.push({ $sort: { createdAt: -1 } });
    }
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $match: {
                $or: [{ status: 6 }, { status: 7 }]
              }
            },
            {
              $group: {
                _id: "$status",
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.status = 1;
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          locationQuery: locationQuery
        };

        res.send(data);
      }
    });
  };
  router.groupbyemployee = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const locationQuery = [];
    locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });

    if (req.body.timesheet_status) {
      locationQuery.push({ $match: { timesheet_status: req.body.timesheet_status } });
    } else {
      locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });
    }

    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      locationQuery.push({ $match: { status: { $gte: 6 } } });
    }

    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );

    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, phone: 1, avatar: 1, address: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );

    locationQuery.push({ $project: { employee_requested: 0 } });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      locationQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      locationQuery.push({ $limit: parseInt(req.body.limit) });
    }
    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gte: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lte: new Date(req.body.to_date) } } });
    }
    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        locationQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { "client.companyname": { $regex: searchs + ".*", $options: "si" } },
              { "employee.name": { $regex: searchs + ".*", $options: "si" } },
              { "locations.name": { $regex: searchs + ".*", $options: "si" } },
              { "branch.branches.branchname": { $regex: searchs + ".*", $options: "si" } },
              { "job_type.name": { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        locationQuery.push({ $match: searching });
      }
    }
    locationQuery.push({
      $group: {
        // _id: {client: "$client", locations: "$locations", employee: "$employee", branch: "$branch", job_type: "$job_type"},
        _id: { employee: "$employee", client: "$client", job_type: "$job_type" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 }
      }
    });

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      locationQuery.push({ $sort: sorting });
    } else {
      locationQuery.push({ $sort: { createdAt: -1 } });
    }
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            { $match: { status: { $gte: 7 } } },
            {
              $match: {
                $or: [{ timesheet_status: 1 }, { timesheet_status: 2 }]
              }
            },
            {
              $group: {
                _id: "$timesheet_status",
                count: { $sum: 1 }
              }
            }
          ],
          result: locationQuery
        }
      }
    ];
    db.GetAggregation("shifts", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.status = 1;
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          locationQuery: locationQuery
        };

        res.send(data);
      }
    });
  };
  router.edit = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Invalid Id").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("shifts", { _id: req.body.id }, {}, {}, (err, shiftData) => {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: shiftData };
        res.send(data);
      }
    });
  };
  router.view = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Shift Id required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const shiftQuery = [];
    shiftQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });

    /* shiftQuery.push({
          $lookup: {
             from: "employees",
             let: { employeeid : "$employee_requested.$.employee" },
             pipeline: [
              { $match:
                 { $expr:
                    {
                        $eq: [ "$_id",  "$$employeeid" ] ,
                    }
                }
              },
              {
                 $addFields: {
                   emmmid : "$$employeeid"
                 }
               },
              { $project: { emmmid : 1  } }
             ],
             as: "employee_details",
          }
        });*/

    db.GetAggregation("shifts", shiftQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };
  router.approval = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("shiftId", "Shift Id required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, (err, ShiftData) => {
      if (err || !ShiftData) {
        data.response = "Job details not available";
        return res.send(data);
      } else {
        async.waterfall(
          [
            callback => {
              db.GetOneDocument("jobtypes", { _id: new mongoose.Types.ObjectId(ShiftData.job_type) }, {}, {}, (err, JobData) => {
                if (err || !JobData) {
                  data.response = "Job details not available";
                  data.status = 0;
                  callback(data);
                } else {
                  callback(err, JobData);
                }
              });
            },
            (JobData, callback) => {
              db.GetOneDocument("clientjobtypes", { job_id: new mongoose.Types.ObjectId(ShiftData.job_type) }, {}, {}, (err, ClientJobData) => {
                ClientJobData = ClientJobData == null || ClientJobData == undefined ? null : ClientJobData;
                callback(err, JobData, ClientJobData);
              });
            },
            (JobData, ClientJobData, callback) => {
              db.GetOneDocument("employeejobtypes", { job_id: new mongoose.Types.ObjectId(ShiftData.job_type) }, {}, {}, (err, EmployeeJobData) => {
                EmployeeJobData = EmployeeJobData == null || EmployeeJobData == undefined ? null : EmployeeJobData;
                callback(err, JobData, ClientJobData, EmployeeJobData);
              });
            },
            (JobData, ClientJobData, EmployeeJobData, callback) => {
              db.GetAggregation("daysoff", [{ $match: { status: { $eq: 1 } } }, { $unwind: "$dates" }], (err, DaysoffData) => {
                DaysoffData = DaysoffData == null || DaysoffData == undefined ? null : DaysoffData;
                callback(err, JobData, ClientJobData, DaysoffData, EmployeeJobData);
              });
            }
          ],
          (err, JobData, ClientJobData, DaysoffData, EmployeeJobData) => {
            async.mapSeries(
              ShiftData,
              (ShiftArray, callback) => {
                RateCalculation(ShiftArray, JobData, ClientJobData, EmployeeJobData, DaysoffData, req.body, req.params.loginId, (err, FinalResult) => {
                  if (err) {
                    callback(err);
                  } else {
                    callback(null, FinalResult);
                  }
                });
              },
              (err, results) => {
                if (err) {
                  data.response = err.response;
                  data.status = 0;
                  res.send(data);
                } else {
                  // add notification
                  const message = "Your timesheet has been approved.";
                  const options = { shiftid: req.body.shiftId };
                  push.addnotification(ShiftData.client, message, "timesheet_approved", options, "CLIENT", (err, response, body) => {});
                  push.addnotification(ShiftData.employee, message, "timesheet_approved", options, "EMPLOYEE", (err, response, body) => {});
                  data.status = 1;
                  data.response = "Timesheet Approved & Invoice Created";
                  res.send(data);
                }
              }
            );

            // RateCalculation(ShiftData, JobData, ClientJobData, EmployeeJobData, DaysoffData, req.body, req.params.loginId, (err, FinalResult) => {
            //   if (err || !FinalResult) {
            //     data.response = err.response;
            //     data.status = 0;
            //     res.send(data);
            //   } else {
            //     // add notification
            //     let message = "Your timesheet has been approved.";
            //     let options = {shiftid: req.body.shiftId};
            //     push.addnotification(ShiftData.employee, message, "timesheet_approved", options, "EMPLOYEE", (err, response, body) => {});
            //     data.status = 1;
            //     data.response = "Timesheet Approved & Invoice Created";
            //     res.send(data);
            //   }
            // });
          }
        );
      }
    });
  };
  router.groupapproval = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("shiftIds", "Shift Id's required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.GetDocument("shifts", { _id: { $in: req.body.shiftIds } }, {}, {}, (err, ShiftData) => {
      if (err || !ShiftData) {
        data.response = "Job details not available";
        return res.send(data);
      } else {
        async.waterfall(
          [
            callback => {
              db.GetOneDocument("jobtypes", { _id: new mongoose.Types.ObjectId(ShiftData[0].job_type) }, {}, {}, (err, JobData) => {
                if (err || !JobData) {
                  data.response = "Job details not available";
                  data.status = 0;
                  callback(data);
                } else {
                  callback(err, JobData);
                }
              });
            },
            (JobData, callback) => {
              db.GetOneDocument("jobtypes", { job_id: new mongoose.Types.ObjectId(ShiftData[0].job_type), client: new mongoose.Types.ObjectId(ShiftData[0].client) }, {}, {}, (err, ClientJobData) => {
                ClientJobData = ClientJobData === null || ClientJobData === undefined ? null : ClientJobData;
                callback(err, JobData, ClientJobData);
              });
            },
            (JobData, ClientJobData, callback) => {
              db.GetOneDocument("jobtypes", { job_id: new mongoose.Types.ObjectId(ShiftData[0].job_type), employee: new mongoose.Types.ObjectId(ShiftData[0].employee) }, {}, {}, (err, EmployeeJobData) => {
                EmployeeJobData = EmployeeJobData === null || EmployeeJobData === undefined ? null : EmployeeJobData;
                callback(err, JobData, ClientJobData, EmployeeJobData);
              });
            },
            (JobData, ClientJobData, EmployeeJobData, callback) => {
              db.GetAggregation("daysoff", [{ $match: { status: { $eq: 1 } } }, { $unwind: "$dates" }], (err, DaysoffData) => {
                DaysoffData = DaysoffData == null || DaysoffData == undefined ? null : DaysoffData;
                callback(err, JobData, ClientJobData, DaysoffData, EmployeeJobData);
              });
            }
          ],
          (err, JobData, ClientJobData, DaysoffData, EmployeeJobData) => {
            async.mapSeries(
              ShiftData,
              (ShiftArray, callback) => {
                RateCalculation(ShiftArray, JobData, ClientJobData, EmployeeJobData, DaysoffData, req.body, req.params.loginId, (err, FinalResult) => {
                  if (err) {
                    callback(err);
                  } else {
                    callback(null, FinalResult);
                  }
                });
              },
              (err, results) => {
                if (err) {
                  data.response = err.response;
                  data.status = 0;
                  res.send(data);
                } else {
                  // add notification
                  const message = "Your timesheet has been approved.";
                  const options = { shiftid: req.body.shiftId };
                  push.addnotification(ShiftData.employee, message, "timesheet_approved", options, "EMPLOYEE", (err, response, body) => {});
                  data.status = 1;
                  data.response = "Timesheet Approved & Invoice Created";
                  res.send(data);
                  event.emit("timesheet_dates_list", { agency: ShiftData.agency });
                  event.emit("invoice_dates_list", { agency: ShiftData.agency });
                }
              }
            );
          }
        );
      }
    });
  };
  /* router.timesheetexport = (req, res) => {
    const data = {};

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const timesheetQuery = [];
    timesheetQuery.push({ $match: { timesheet_status: { $gte: 1 } } });

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      timesheetQuery.push({ $sort: sorting });
    } else {
      timesheetQuery.push({ $sort: { createdAt: -1 } });
    }

    timesheetQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    timesheetQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
      }
    );

    timesheetQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$location" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { location: "$location_data.name" } }
    );

    db.GetAggregation("shifts", timesheetQuery, (err, docData) => {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          const fields = ["createdAt", "title", "location", "job type", "employee", "employee_rate", "starttime", "endtime", "start_date", "end_date", "timesheet_status"];
          const fieldNames = ["Date", "Title", "Location", "Role", "Employee", "Employee Rate", "Start Time", "End Time", "Start Date", "End Date", "Timesheet Status"];

          for (let i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");

            if (docData[i].timesheet_status == 2) {
              docData[i].tiemsheet_status = "Approval";
            } else {
              docData[i].timesheet_status = "Pending";
            }
          }

          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(docData);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        }
      }
    });
  }; */
  router.timesheetexport = (req, res) => {
    const data = {};
    data.status = 0;

    const locationQuery = [];
    if (req.body.id) {
      locationQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    } else {
      locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    }
    locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });
    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      locationQuery.push({ $match: { status: { $gte: 6 } } });
    }
    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );
    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );
    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );
    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );
    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, phone: 1, avatar: 1, address: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );
    locationQuery.push({ $project: { employee_requested: 0 } });
    locationQuery.push({
      $group: {
        _id: { employee: "$employee", client: "$client", job_type: "$job_type" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 }
      }
    });

    db.GetAggregation("shifts", locationQuery, (err, docData) => {
      if (err || !docData) {
        data.response = err;
        res.send(data);
      } else {
        if (docData && docData.length > 0) {
          const fieldNames = ["Employee", "Client", "Role", "Shifts"];
          const mydata = docData;
          const finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Employee"] = mydata[i].employee ? (mydata[i].employee && mydata[i].employee.length > 0 ? mydata[i].employee[0][0] : "") : "";
            temp["Client"] = mydata[i].client ? (mydata[i].client && mydata[i].client.length > 0 ? mydata[i].client[0][0] : "") : "";
            temp["Role"] = mydata[i].job_type ? (mydata[i].job_type && mydata[i].job_type.length > 0 ? mydata[i].job_type[0][0] : "") : "";
            temp["Shifts"] = mydata[i].count || 0;
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  router.downloadpdf = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("client_email", "Email Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const locationQuery = [];
    locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    locationQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.employeeID) } } });
    locationQuery.push({ $match: { job_type: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.jobtypeID) } } });
    locationQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.clientID) } } });
    // locationQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // locationQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.branchID)}}});
    locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });
    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, email: 1, address: 1, avatar: 1, company_logo: 1, phone: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, phone: 1, address: 1, avatar: 1, email: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    // locationQuery.push({ $project: { employee_requested: 0 } });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });
    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gt: new Date(req.body.TabOpt.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lt: new Date(req.body.TabOpt.to_date) } } });
    }
    locationQuery.push({ $sort: { createdAt: -1 } });
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $match: {
                $or: [{ status: 6 }, { status: 7 }]
              }
            },
            {
              $group: {
                _id: "$status",
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        console.log("docData[0].result.length ", docData[0].result.length);
        if (docData && docData[0].result && docData[0].result[0]) {
          const getshiftdata = docData[0].result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = item.starttime / 3600;
            const splithr = evestarthr.toString().split(".");
            const startmins = splithr[1] === undefined ? "00" : (+"0" + "." + splithr[1]) * 60;
            const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            const eveendhr = item.endtime / 3600;
            const splitendhr = eveendhr.toString().split(".");
            const endmins = splitendhr[1] === undefined ? "00" : (+"0" + "." + splitendhr[1]) * 60;
            const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            const timesheet_status = item.timesheet_status ? item.timesheet_status : "";
            return {
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.title,
              shiftId: item.shiftId,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            };
          });
          const html = `
          <style>
html {
zoom: 0.55;
}
</style>
          <table style="margin: 0; padding: 0; color: #000; background: #fff; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody>
                     <tr>
                       <td>
                         <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                          <tbody>
                            <tr>
                              <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                  <tbody>
                                  <tr>
                        
                                    
                                   <td style="width: 55%; vertical-align: top;border: 1px solid #333;padding: 0px 10px 0;">
                          <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474; ">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Name:</td>
                            <td width="60%" style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${docData[0].result[0].employee} </td>
                          </tr>
                          </tbody>
                        </table>
                        
                        <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474;">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Job Role:</td>
                            <td width="60%"  style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${docData[0].result[0].job_type} </td>
                          </tr>
                          </tbody>
                        </table>
                        
                        <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474;">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Timesheet Date: </td>
                            <td width="60%"  style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${req.body.timesheet_from_date} -  ${req.body.timesheet_to_date} </td>
                          </tr>
                          </tbody>
                        </table>						
                        
                        <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474;">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Timesheet Number: </td>
                            <td width="60%"  style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${req.body.timesheet_timesheetID} </td>
                          </tr>
                          </tbody>
                        </table>
                        
                                  </td>
                      <td style="width: 5%; vertical-align: top;">
                      </td>
                                   <td style="width: 40%; vertical-align: top;" align="right">
                                      <img src="${CONFIG.LIVEURL}/${docData[0].result[0].agency_data[0].company_logo}" style=" max-width: 320px;object-fit:contain;">
                                    <p style="font-size: 13px; line-height: 26px;   color: #4e4c4c; width: 90%;">${docData[0].result[0].agency_data[0].address.formatted_address}, ${docData[0].result[0].agency_data[0].address.zipcode}. <br> <b>Tel.</b> : ${
                          docData[0].result[0].agency_data[0].phone.code
                          } - ${docData[0].result[0].agency_data[0].phone.number}
                          <br> <b> E: </b> ${docData[0].result[0].agency_data[0].email} <br> <b> W: </b> agency.com </p>
                                  </td>
                                  </tr>
                                  </tbody>
                               </table>
                              </td>
                            </tr>
                            
                            <tr>
                              <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                  <thead>
                                  <tr>
                                      <th style="padding: 10px 5px; font-size: 14px; border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;    background-color: #ddd;    color: #000;"> Shift No. </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Work Location  </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Shift </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Time </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Break </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Hours </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;border-right: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Status </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  ${getshiftdata
                                    .map(
                                      item =>
                                        ` <tr>
                                    <td style="padding: 10px 5px; font-size: 13px;text-align: left; color: #545151;    border-bottom: 1px solid #333;  border-left: 1px solid #333;"> ${item.title} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333; border-left: 1px solid #333;"> ${item.branch} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333;border-left: 1px solid #333;"> ${moment(item.start_date).format("DD-MM-YYYY")} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333;border-left: 1px solid #333;"> ${item.starttime} - ${item.endtime}	 </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333; border-left: 1px solid #333;"> ${item.breaktime / 60} Minutes	</td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333; border-left: 1px solid #333;"> ${Math.round(item.timesheet[0].workmins / 60)} Minutes</td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333;     border-right: 1px solid #333; border-left: 1px solid #333;"> ${item.timesheet_status === 2 ? `Approved` : `Pending`}</td>
                                  </tr>`
                                    )
                                    .join(" ")}
                                </tbody>
                                </table>							 
                              </td>
                            </tr> 
                          </tbody>
                          </table>
                       </td>
                     </tr>
                    </tbody>
                 </table>     
                   `;
          pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
        } else {
          data.status = 0;
          data.response = "Timesheet Details Not Available";
          res.send(data);
        }
      }
    });
  };
  router.mailpdf = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("client_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Invalid Subject").notEmpty();
    req.checkBody("email_content", "Invalid Content").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const locationQuery = [];
    locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    locationQuery.push({ $match: { employee: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.employeeID) } } });
    locationQuery.push({ $match: { job_type: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.jobtypeID) } } });
    locationQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.clientID) } } });
    // locationQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // locationQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.branchID)}}});
    locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });
    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, email: 1, address: 1, avatar: 1, company_logo: 1, phone: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, phone: 1, address: 1, avatar: 1, email: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    // locationQuery.push({ $project: { employee_requested: 0 } });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });
    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gt: new Date(req.body.TabOpt.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lt: new Date(req.body.TabOpt.to_date) } } });
    }
    locationQuery.push({ $sort: { createdAt: -1 } });
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $match: {
                $or: [{ status: 6 }, { status: 7 }]
              }
            },
            {
              $group: {
                _id: "$status",
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        console.log("docData[0].result.length ", docData[0].result.length);
        if (docData && docData[0].result && docData[0].result[0]) {
          const getshiftdata = docData[0].result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = item.starttime / 3600;
            const splithr = evestarthr.toString().split(".");
            const startmins = splithr[1] === undefined ? "00" : (+"0" + "." + splithr[1]) * 60;
            const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            const eveendhr = item.endtime / 3600;
            const splitendhr = eveendhr.toString().split(".");
            const endmins = splitendhr[1] === undefined ? "00" : (+"0" + "." + splitendhr[1]) * 60;
            const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            const timesheet_status = item.timesheet_status ? item.timesheet_status : "";
            return {
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.title,
              shiftId: item.shiftId,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            };
          });
          const html = `
          <style>
html {
zoom: 0.55;
}
</style>
          <table style="margin: 0; padding: 0; color: #000; background: #fff; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody>
                     <tr>
                       <td>
                         <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                          <tbody>
                            <tr>
                              <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                  <tbody>
                                  <tr>
                        
                                    
                                   <td style="width: 55%; vertical-align: top;border: 1px solid #333;padding: 0px 10px 0;">
                          <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474; ">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Name:</td>
                            <td width="60%" style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${docData[0].result[0].employee} </td>
                          </tr>
                          </tbody>
                        </table>
                        
                        <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474;">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Job Role:</td>
                            <td width="60%"  style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${docData[0].result[0].job_type} </td>
                          </tr>
                          </tbody>
                        </table>
                        
                        <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474;">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Timesheet Date: </td>
                            <td width="60%"  style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${req.body.timesheet_from_date} -  ${req.body.timesheet_to_date} </td>
                          </tr>
                          </tbody>
                        </table>						
                        
                        <table style="margin: 0px auto; padding: 8px 0px; border-spacing: 0; border-collapse: initial; width: 100%;border-bottom: 1px solid #797474;">
                          <tbody>
                            <tr>
                            <td width="40%" style="font-size: 14px; font-weight: bold;"> Timesheet Number: </td>
                            <td width="60%"  style="font-size: 13px; color: #4e4c4c; line-height: 30px;"> ${req.body.timesheet_timesheetID} </td>
                          </tr>
                          </tbody>
                        </table>
                        
                                  </td>
                      <td style="width: 5%; vertical-align: top;">
                      </td>
                                   <td style="width: 40%; vertical-align: top;" align="right">
                                      <img src="${CONFIG.LIVEURL}/${docData[0].result[0].agency_data[0].company_logo}" style=" max-width: 320px;object-fit:contain;">
                                    <p style="font-size: 13px; line-height: 26px;   color: #4e4c4c; width: 90%;">${docData[0].result[0].agency_data[0].address.formatted_address}, ${docData[0].result[0].agency_data[0].address.zipcode}. <br> <b>Tel.</b> : ${
                          docData[0].result[0].agency_data[0].phone.code
                          } - ${docData[0].result[0].agency_data[0].phone.number}
                          <br> <b> E: </b> ${docData[0].result[0].agency_data[0].email} <br> <b> W: </b> agency.com </p>
                                  </td>
                                  </tr>
                                  </tbody>
                               </table>
                              </td>
                            </tr>
                            
                            <tr>
                              <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                                  <thead>
                                  <tr>
                                      <th style="padding: 10px 5px; font-size: 14px; border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;    background-color: #ddd;    color: #000;"> Shift No. </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Work Location  </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Shift </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Time </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Break </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Hours </th>
                                      <th style="padding: 10px 5px; font-size: 14px;  border-top: 1px solid #333; border-bottom: 1px solid #333;border-left: 1px solid #333;border-right: 1px solid #333;   text-align: left;background-color: #ddd;    color: #000;"> Status </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  ${getshiftdata
                                    .map(
                                      item =>
                                        ` <tr>
                                    <td style="padding: 10px 5px; font-size: 13px;text-align: left; color: #545151;    border-bottom: 1px solid #333;  border-left: 1px solid #333;"> ${item.title} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333; border-left: 1px solid #333;"> ${item.branch} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333;border-left: 1px solid #333;"> ${moment(item.start_date).format("DD-MM-YYYY")} </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333;border-left: 1px solid #333;"> ${item.starttime} - ${item.endtime}	 </td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333; border-left: 1px solid #333;"> ${item.breaktime / 60} Minutes	</td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333; border-left: 1px solid #333;"> ${Math.round(item.timesheet[0].workmins / 60)} Minutes</td>
                                    <td style="padding: 10px 5px; font-size: 13px; text-align: left;color: #545151; border-bottom: 1px solid #333;     border-right: 1px solid #333; border-left: 1px solid #333;"> ${item.timesheet_status === 2 ? `Approved` : `Pending`}</td>
                                  </tr>`
                                    )
                                    .join(" ")}
                                </tbody>
                                </table>							 
                              </td>
                            </tr> 
                          </tbody>
                          </table>
                       </td>
                     </tr>
                    </tbody>
                 </table>     
                   `;
          pdf.create(html, pdfoptions).toFile("./uploads/pdf/timesheet.pdf", function(err, document) {
            if (err) {
              res.send(err);
            } else {
              data.status = 1;
              data.msg = "Mail Sent";
              res.send(data);
              const mailOptions = {
                from: req.params.loginData.company_email || req.body.client_email || docData[0].result.agency_data[0].email,
                to: req.body.client_email || docData[0].result.agency_data[0].email,
                subject: req.body.email_subject || "Timesheets",
                text: "Please Download the attachment to see Timesheets",
                html: req.body.email_content,
                attachments: [
                  {
                    filename: "Timesheet.pdf",
                    path: "./uploads/pdf/Timesheet.pdf",
                    contentType: "application/pdf"
                  }
                ]
              };
              mail.send(mailOptions, function(err, response) {});
            }
          });
        } else {
          data.status = 0;
          data.response = "Timesheet Details Not Available";
          res.send(data);
        }
      }
    });
  };
  router.timesheetdates = (req, res) => {
    const data = {};
    async.waterfall(
      [
        callback => {
          db.GetDocument("billing", { agency: new mongoose.Types.ObjectId(req.params.loginId), type: "timesheet" }, {}, {}, (err, Billing_Dates) => {
            if (err || !Billing_Dates) {
              data.response = "Date details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, Billing_Dates);
            }
          });
        },
        (Billing_Dates, callback) => {
          const options = {};
          options.sort = { createdAt: -1 };
          db.GetOneDocument("shifts", { agency: new mongoose.Types.ObjectId(req.params.loginId) }, { createdAt: 1, start_date: 1 }, options, (err, Shifts) => {
            if (err) {
              data.response = "Shift details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, Billing_Dates, Shifts);
            }
          });
        }
      ],
      (err, Billing_Dates, Shifts) => {
        if (err) {
          data.status = 0;
          data.response = "Unable to get Dates, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { datelist: Billing_Dates || [], lastshift: Shifts && Shifts.createdAt || new Date() };
          res.send(data);
        }
      }
    );
  };
  return router;
};
