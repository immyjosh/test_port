const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const moment = require("moment");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const mongoose = require("mongoose");

module.exports = () => {
  const router = {};

  router.add = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID is Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("jobtypes", { _id: req.body.id }, {}, {}, (err, jobData) => {
      if (err || !jobData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        // data.status = 1;
        // data.response = {'result': jobData};
        // res.send(data);
        // console.log(req.body.employee_id);
        const job = {
          name: String(jobData.name),
          late_fee: jobData.late_fee,
          rate_details: jobData.rate_details,
          status: jobData.status,
          job_id: new mongoose.Types.ObjectId(jobData._id),
          addedBy: "agency",
          addedId: req.params.loginId,
          employee: new mongoose.Types.ObjectId(req.body.employee_id)
        };
        // console.log(jobData,jobData.name,jobData.late_fee);
        db.InsertDocument("jobtypes", job, (err, result) => {
          if (err || !result) {
            data.response = "Unable to save your data, Please try again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = { result: result };
            res.send(data);
          }
        });
      }
    });
  };
  router.remove = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID is Required").optional();
    req.checkBody("employee_id", "Employee ID is Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    if (req.body.ids) {
      db.DeleteDocument("jobtypes", { job_id: { $in: req.body.ids }, employee: req.body.employee_id }, (err, result) => {
        if (err || !result) {
          data.response = "Unable to Delete your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
    if (req.body.id) {
      db.DeleteDocument("jobtypes", { _id: req.body.id, employee: req.body.employee_id }, (err, result) => {
        if (err || !result) {
          data.response = "Unable to Delete your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
  };
  router.save = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("name", "Name is Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const job = {
      name: req.body.name,
      late_fee: req.body.late_fee,
      rate_details: req.body.rate_details,
      status: req.body.status,
      addedBy: "agency",
      addedId: req.params.loginId,
      employee: req.body.employee
    };

    if (req.body._id) {
      db.UpdateDocument("jobtypes", { _id: req.body._id }, job, {}, (err, result) => {
        if (err) {
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated successfully.";
          res.send(data);
        }
      });
    } else {
      db.InsertDocument("jobtypes", job, (err, result) => {
        if (err || !result) {
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("employee_id", "Employee ID is Required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.GetDocument("jobtypes", { employee: new mongoose.Types.ObjectId(req.body.employee_id) }, { job_id: 1 }, {}, (err, jobData) => {
      if (err) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        let curjobids = [];
        if (jobData && jobData.length > 0) {
          curjobids = jobData.map(list => new mongoose.Types.ObjectId(list.job_id));
        }
        db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.employee_id) }, { job_type: 1 }, {}, (err, EmployeeData) => {
          if (err) {
            data.response = "Unable to Get Your Data Please Try Again";
            res.send(data);
          } else {
            let jobsidds = [];
            if (EmployeeData) {
              jobsidds = EmployeeData.job_type.map(list => new mongoose.Types.ObjectId(list));
            }
            const jobQuery = [];
            jobQuery.push({ $match: { addedBy: { $eq: "agency" } } });
            jobQuery.push({ $match: { client: { $exists: false } } });
             jobQuery.push({
              $match: {
                $or: [
                  { $and: [{ addedId: { $eq: req.params.loginId } }, { employee: { $exists: false } }, { _id: { $in: jobsidds } }] },
                  { $and: [{ addedId: { $eq: req.params.loginId } }, { employee: { $eq: new mongoose.Types.ObjectId(req.body.employee_id) } }] }
                ]
              }
            });
             jobQuery.push({
              $match: {
                $and: [
                  { addedId: { $eq: req.params.loginId } }, { _id: { $nin: curjobids } }
                ]
              }
            });
            // jobQuery.push({ $match: { _id: { $nin: curjobids } } });
            // jobQuery.push({ $match: { status: { $exists: true } } });
            if (req.body.status) {
              jobQuery.push({ $match: { status: { $eq: req.body.status } } });
            } else {
              jobQuery.push({ $sort: { createdAt: -1 } });
            }

            if (req.body.search) {
              const searchs = req.body.search;

              if (req.body.filter === "all") {
                jobQuery.push({
                  $match: {
                    $or: [{ name: { $regex: searchs + ".*", $options: "si" } }]
                  }
                });
              } else {
                const searching = {};
                searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
                jobQuery.push({ $match: searching });
              }
            }

            if (req.body.filter && req.body.order) {
              const sorting = {};
              sorting[req.body.filter] = parseInt(req.body.order);
              jobQuery.push({ $sort: sorting });
            } else {
              jobQuery.push({ $sort: { createdAt: -1 } });
            }

            jobQuery.push({ $project: { createdAt: 1, updatedAt: 1, status: 1, name: 1, jobrate: 1, job_id: 1, employee: 1 } });

            const withoutlimit = Object.assign([], jobQuery);
            withoutlimit.push({ $count: "count" });

            if (req.body.skip >= 0) {
              jobQuery.push({ $skip: parseInt(req.body.skip) });
            }

            if (req.body.limit >= 0) {
              jobQuery.push({ $limit: parseInt(req.body.limit) });
            }

            const finalQuery = [
              {
                $facet: {
                  overall: withoutlimit,
                  result: jobQuery
                }
              }
            ];

            db.GetAggregation("jobtypes", finalQuery, (err, docData) => {
              if (err || !docData) {
                data.response = "Unalbe to Get Your data Please try Again";
                res.send(err);
              } else {
                data.status = 1;
                let fullcount;
                if (docData[0].overall[0] && docData[0].overall[0].count) {
                  fullcount = docData[0].overall[0].count;
                } else {
                  fullcount = docData[0].result.length;
                }
                data.response = {
                  result: docData[0].result,
                  length: docData[0].result.length,
                  fullcount: fullcount
                };
                res.send(data);
              }
            });
          }
        });
      }
    });
  };
  router.normallist = (req, res) => {
    const data = {};
    data.status = 0;

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const jobQuery = [];
    jobQuery.push({ $match: { addedBy: { $eq: "agency" } } });
    jobQuery.push({ $match: { addedId: { $eq: req.params.loginId } } });

    jobQuery.push({ $match: { status: { $exists: true } } });

    if (req.body.status) {
      jobQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    //  if(req.body.search){
    //      var searches = req.body.searches;
    //      var searching = {};
    //      searching[req.body.filter] = {$regex:searches + '.*',$options:'si' };
    //      jobQuery.push({'$match':searching});
    //  }

    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        jobQuery.push({
          $match: {
            $or: [{ name: { $regex: searchs + ".*", $options: "si" } }]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        jobQuery.push({ $match: searching });
      }
    }

    if (req.body.filter && req.body.order) {
      const sorting = {};
      sorting[req.body.filter] = parseInt(req.body.order);
      jobQuery.push({ $sort: sorting });
    } else {
      jobQuery.push({ $sort: { createdAt: -1 } });
    }

    jobQuery.push({
      $project: { createdAt: 1, updatedAt: 1, status: 1, name: 1, jobrate: 1 }
    });

    //  var withoutlimit = JSON.parse(JSON.stringify(jobQuery));
    //  withoutlimit.push({
    //      $count:'count'
    //  });

    const withoutlimit = Object.assign([], jobQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      jobQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      jobQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          result: jobQuery
        }
      }
    ];

    db.GetAggregation("jobtypes", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unalbe to Get Your data Please try Again";
        res.send(err);
      } else {
        data.status = 1;
        let fullcount, filtered;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        if (docData && docData[0].result && docData[0].result.length > 0 && req.body.job_id) {
          filtered = docData[0].result.filter(item => req.body.job_id.indexOf(item._id.toString()) !== -1);
        }
        data.response = {
          result: filtered || [],
          length: docData[0].result.length,
          fullcount: fullcount
        };
        res.send(data);
      }
    });
  };
  router.edit = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid id").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    db.GetDocument("jobtypes", { _id: req.body.id }, {}, {}, (err, jobData) => {
      if (err || !jobData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: jobData };
        res.send(data);
      }
    });
  };

  router.jobtypeexport = function(req, res) {
    const data = {};

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const jobQuery = [];
    jobQuery.push({ $match: { status: { $exists: true } } });

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      jobQuery.push({ $sort: sorting });
    } else {
      jobQuery.push({ $sort: { createdAt: -1 } });
    }

    db.GetAggregation("jobtypes", jobQuery, function(err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          const fields = ["createdAt", "name", "status"];
          const fieldNames = ["Date", "Job Role", "Status"];

          for (let i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");
            if (docData[i].status == 0) {
              docData[i].status = "In-active";
            } else if (docData[i].status == 1) {
              docData[i].status = "Active";
            }
          }

          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(docData);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  return router;
};
