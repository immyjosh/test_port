let db = require("../../model/mongodb");
let CONFIG = require("../../config/config");
let json2csv = require("json2csv").Parser;
const moment = require("moment");

module.exports = () => {
  const router = {};

  let data = {};
  data.status = 0;

  router.save = (req, res) => {
    req.checkBody("name", "Name is required").notEmpty();
    req.checkBody("numberofday", "days required").notEmpty();
    req.checkBody("dates", "dates required").notEmpty();

    let error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    let daysoff = {
      name: req.body.name,
      numberofday: req.body.numberofday,
      dates: req.body.dates,
      reason: req.body.reason,
      status: req.body.status,
      addedBy: "agency",
      addedId: req.params.loginId
    };

    if (req.body._id) {
      db.UpdateDocument("daysoff", {_id: req.body._id}, daysoff, {}, function(err, result) {
        if (err) {
          if (err.code === 11000) {
            data.response = "Name is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated Successfully";
          res.send(data);
        }
      });
    } else {
      db.InsertDocument("daysoff", daysoff, (err, result) => {
        if (err || !result) {
          if (err.code === 11000) {
            data.response = "Name is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Added Succeefully";
          res.send(data);
        }
      });
    }
  };

  router.list = (req, res) => {
    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let daysQuery = [];
    daysQuery.push({$match: {addedBy: {$eq: "agency"}}});
    daysQuery.push({$match: {addedId: {$eq: req.params.loginId}}});

    daysQuery.push({$match: {status: {$exists: true}}});
    if (req.body.status) {
      daysQuery.push({$match: {status: {$eq: req.body.status}}});
    }

    if (req.body.search) {
      let searchs = req.body.search;

      if (req.body.filter === "all") {
        daysQuery.push({
          $match: {
            $or: [
              {name: {$regex: searchs + ".*", $options: "si"}},
              {numberofday: {$regex: searchs + ".*", $options: "si"}},
              {dates: {$regex: searchs + ".*", $options: "si"}},
              {reason: {$regex: searchs + ".*", $options: "si"}}
            ]
          }
        });
      } else {
        let searching = {};
        searching[req.body.filter] = {$regex: searchs + ".*", $options: "si"};
        daysQuery.push({$match: searching});
      }
    }
    if (req.body.field && req.body.order) {
      let sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      daysQuery.push({$sort: sorting});
    } else {
      daysQuery.push({$sort: {createdAt: -1}});
    }

    if (req.body.from_date) {
      daysQuery.push({$match: {createdAt: {$gt: new Date(req.body.from_date)}}});
    }

    if (req.body.to_date) {
      daysQuery.push({$match: {createdAt: {$lt: new Date(req.body.to_date)}}});
    }

    daysQuery.push({$project: {createdAt: 1, updatedAt: 1, name: 1, activity: 1, status: 1, reason: 1, numberofday: 1, dates: 1}});

    let withoutlimit = JSON.parse(JSON.stringify(daysQuery));
    withoutlimit.push({
      $count: "count"
    });

    if (req.body.skip >= 0) {
      daysQuery.push({$skip: parseInt(req.body.skip)});
    }
    if (req.body.limit >= 0) {
      daysQuery.push({$limit: parseInt(req.body.limit)});
    }

    let finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          result: daysQuery
        }
      }
    ];

    db.GetAggregation("daysoff", finalQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          var fullcount = docData[0].overall[0].count;
        } else {
          var fullcount = docData[0].result.length;
        }
        data.response = {result: docData[0].result, length: docData[0].result.length, fullCount: fullcount, daysQuery: daysQuery};
        res.send(data);
      }
    });
  };
  router.forshiftslist = (req, res) => {
    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let daysQuery = [];
    daysQuery.push({$match: {addedBy: {$eq: "agency"}}});
    daysQuery.push({$match: {addedId: {$eq: req.params.loginId}}});
    daysQuery.push({$match: {status: {$exists: true}}});
    daysQuery.push({$match: {status: {$eq: 1}}}, {$unwind: "$dates"});
    daysQuery.push({$sort: {createdAt: -1}});
    daysQuery.push({$project: {createdAt: 1, updatedAt: 1, name: 1, activity: 1, status: 1, reason: 1, numberofday: 1, dates: 1}});

    db.GetAggregation("daysoff", daysQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = {result: docData, daysQuery: daysQuery};
        res.send(data);
      }
    });
  };
  router.edit = function(req, res) {
    req.checkBody("id", "Invalid ID").notEmpty();
    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("daysoff", {_id: req.body.id}, {}, {}, function(err, daysData) {
      if (err || !daysData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = {result: daysData};
        res.send(data);
      }
    });
  };
  router.daysoffexport = function(req, res) {
    let data = {};
    data.status = 0;

    let daysQuery = [];
    daysQuery.push({$match: {status: {$exists: true}}});
    daysQuery.push({$match: {addedBy: {$eq: "agency"}}});
    daysQuery.push({$match: {addedId: {$eq: req.params.loginId}}});

    db.GetAggregation("daysoff", daysQuery, function(err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData && docData.length > 0) {
          let fieldNames = ["Date", "Reason Name", "Number Of Day", "Reason", "Dates", "Status"];
          let finaldata = [];
          for (let i = 0; i < docData.length; i++) {
            let temp = {};
            let dates = docData[i].dates.map(date => {
              if(date){
                return moment(date).format("DD/MM/YYYY")
              }else{
                return ""
              }
            });
            if (docData[i].status == 0) {
              docData[i].status = "In-active";
            } else if (docData[i].status == 1) {
              docData[i].status = "Active";
            }
            temp["Date"] = docData[i].createdAt ? moment(docData[i].createdAt).format("DD/MM/YYYY") : "-";
            temp["Reason Name"] = docData[i].name || "";
            temp["Number Of Day"] = docData[i].numberofday || "";
            temp["Reason"] = docData[i].reason || "";
            temp["Dates"] = dates.join() || "";
            temp["Status"] = docData[i].status || "";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({fieldNames});
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  /* router.daysoffexport = function(req, res) {
    let data = {};

    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let daysQuery = [];
    daysQuery.push({$match: {status: {$exists: true}}});
    daysQuery.push({$match: {addedBy: {$eq: "agency"}}});
    daysQuery.push({$match: {addedId: {$eq: req.params.loginId}}});

    if (req.body.field && req.body.order) {
      let sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      daysQuery.push({$sort: sorting});
    } else {
      daysQuery.push({$sort: {createdAt: -1}});
    }

    db.GetAggregation("daysoff", daysQuery, function(err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          let fields = ["createdAt", "name", "numberofday", "reason", "dates", "status"];
          let fieldNames = ["Date", "Reason Name", "Number Of Day", "Reason", "Dates", "Status"];

          for (let i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");
            if (docData[i].status == 0) {
              docData[i].status = "In-active";
            } else if (docData[i].status == 1) {
              docData[i].status = "Active";
            }
          }

          const json2csvParser = new json2csv({fields, fieldNames});
          const csv = json2csvParser.parse(docData);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */
  return router;
};
