const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library");
const attachment = require("../../model/attachments");
const mongoose = require("mongoose");
const mailcontent = require("../../model/mailcontent");
const moment = require("moment");
// const superadminschemas = require("../schema/superadmin.schema");
const nodemailer = require("nodemailer");

module.exports = () => {
  const router = {};

  router.save = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("heading", "Template Name Required").notEmpty();
    req.checkBody("sender_name", "Sender Name Required").notEmpty();
    req.checkBody("sender_email", "Sender Email required").notEmpty();
    req.checkBody("email_content", "Email Content required").notEmpty();
    req.checkBody("type", "Type required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const plan = {
      description: req.body.description,
      email_subject: req.body.email_subject,
      sender_name: req.body.sender_name,
      sender_email: req.body.sender_email,
      email_content: req.body.email_content,
      heading: req.body.heading,
      type: req.body.type,
      agency: req.params.loginId,
      status: 1
    };

    if (req.body._id) {
      db.UpdateDocument("agencyemailtemplate", { _id: req.body._id }, plan, {}, (err, result) => {
        if (err) {
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated successfully.";
          res.send(data);
        }
      });
    } else {
      plan.default_mail = 0;
      db.InsertDocument("agencyemailtemplate", plan, (err, result) => {
        if (err || !result) {
          data.response = "Unable to save your data, Please try again";
          res.send(err);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { status: { $exists: true } } });
    userQuery.push({ $match: { agency: { $exists: true } } });
    userQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } });
    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }
    if (req.body.type && req.body.type !== "all") {
      userQuery.push({ $match: { type: { $eq: req.body.type } } });
    }

    // if(req.body.search){
    //   var searchs = req.body.search;
    //   var searching = {};
    //   searching[req.body.filter] = { $regex: searchs + '.*', $options: 'si' };
    //   userQuery.push({"$match": searching});
    // }
    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [{ heading: { $regex: searchs + ".*", $options: "si" } }, { description: { $regex: searchs + ".*", $options: "si" } }, { email_subject: { $regex: searchs + ".*", $options: "si" } }]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    userQuery.push({ $project: { createdAt: 1, updatedAt: 1, description: 1, email_subject: 1, heading: 1, type: 1, default_mail: 1, mail_def: 1 } });

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];

    db.GetAggregation("agencyemailtemplate", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount = docData[0].result.length;
        if (docData[0].overall[0]) {
          fullcount = docData[0].overall[0].count;
        }
        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("agencyemailtemplate", { _id: req.body.id }, {}, {}, (err, emailData) => {
      if (err || !emailData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: emailData };
        res.send(data);
      }
    });
  };
  router.selecttypedata = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("type", "Invalid Type").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    let Query;
    if (req.body.pagefor === "editpage") {
      Query = { agency: req.params.loginId, type: req.body.type, default_mail: 1 };
    } else {
      Query = { agency: req.params.loginId, type: req.body.type };
    }

    db.GetDocument("agencyemailtemplate", Query, {}, {}, (err, emailData) => {
      if (err || !emailData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: emailData };
        res.send(data);
      }
    });
  };
  router.getemailcontent = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Invalid ID").notEmpty();
    req.checkBody("refereename", "Invalid Reference Name").optional();
    req.checkBody("employee", "Invalid Employee Name").optional();
    req.checkBody("employee_username", "Invalid Employee Username").optional();
    req.checkBody("referencefor", "Invalid Reference").optional();
    req.checkBody("client", "Invalid Client Name").optional();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    db.GetOneDocument("settings", { alias: "general" }, {}, {}, async (err, settings) => {
      if (err || !settings) {
        data.response = "Unable to get Site Settings data, Please Contact admin";
        res.send(data);
      } else {
        try {
          const mailData = {};
          mailData.template = req.body.id;
          mailData.html = [];
          mailData.html.push({ name: "client", value: req.body.client || "Client" });
          mailData.html.push({ name: "employee", value: req.body.employee || "Employee" });
          mailData.html.push({ name: "refereename", value: req.body.refereename || "Reference" });
          mailData.html.push({ name: "agency", value: req.params.loginData.company_name || "Company" });
          mailData.html.push({ name: "referenceurl", value: settings.settings.site_url + "referenceform/" + datas.referencefor + "/" + datas.employee_username });
          mailData.html.push({ name: "interviewnotes", value: req.body.interviewnotes });
          mailData.html.push({ name: "videocallurl", value: req.body.videocallurl });
          mailData.html.push({ name: "interviewtime", value: req.body.interviewtime });
          mailData.html.push({ name: "interviewdate", value: moment(req.body.interviewdate).format("DD-MM-YYYY") });
          mailData.html.push({ name: "intervieweddate", value: moment(req.body.intervieweddate).format("DD-MM-YYYY") });
          mailData.html.push({ name: "interviewednotes", value: req.body.interviewednotes });
          mailData.html.push({ name: "interviewedstatus", value: req.body.interviewedstatus === 1 ? "Approved" : "Denied" });
          mailData.html.push({ name: "skillcheckstatus", value: req.body.skillcheckstatus === 1 ? "Approved" : "Denied" });
          mailData.html.push({ name: "dbscheckstatus", value: req.body.dbscheckstatus === 1 ? "Approved" : "Denied" });
          mailData.html.push({ name: "healthcheckstatus", value: req.body.healthcheckstatus === 1 ? "Approved" : "Denied" });
          mailData.html.push({ name: "refereestatus", value: req.body.refereestatus === 1 ? "Approved" : "Denied" });
          mailData.html.push({ name: "refereenamesfor", value: req.body.refereenamesfor});
          mailData.html.push({ name: "empstatus", value: req.body.empstatus === 1 ? "Approved" : "Denied" });
          mailData.html.push({ name: "empnote", value: req.body.empnote});
          mailData.html.push({ name: "refereenotes", value: req.body.refereenotes});
          const result = await mailcontent.getmaildata(mailData);
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        } catch (err) {
          data.status = 0;
          data.response = err;
          res.send(data);
        }
      }
    });
  };
  router.defaultstatus = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    req.checkBody("type", "Type Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    db.UpdateDocument("agencyemailtemplate", { _id: new mongoose.Types.ObjectId(req.body.id) }, { default_mail: 1 }, {}, (err, emailData) => {
      if (err || !emailData) {
        data.status = 0;
        data.response = "Unable to update your data, Please try again";
        res.send(data);
      } else {
        db.UpdateMany("agencyemailtemplate", { _id: { $ne: new mongoose.Types.ObjectId(req.body.id) }, agency: req.params.loginId, type: req.body.type }, { default_mail: 0 }, {}, (err, emailDatas) => {
          if (err || !emailDatas) {
            data.status = 0;
            data.response = "Unable to update your data, Please try again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Updated";
            res.send(data);
          }
        });
      }
    });
  };
  router.delete = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    db.DeleteDocument("agencyemailtemplate", { _id: new mongoose.Types.ObjectId(req.body.id) }, (err, emailDatas) => {
      if (err || !emailDatas) {
        data.status = 0;
        data.response = "Unable to delete your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Deleted";
        res.send(data);
      }
    });
  };

  return router;
};
