module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library.js");
  const nodemailer = require("nodemailer");
  const mail = require("../../model/mail.js");
  const mailcontent = require("../../model/mailcontent");
  const twilio = require("../../model/twilio.js");
  const dashboard = require("../../model/dashboard.js");
  const push = require("../../model/pushNotification.js")(io);
  const moment = require("moment");
  const mongoose = require("mongoose");
  const attachment = require("../../model/attachments");
  const async = require("async");
  const json2csv = require("json2csv").Parser;
  const fs = require("fs");
  const Google = require("../../model/googleapis");
  const event = require("../../controller/events/events");
  const { promisify } = require("util");
  const unlinkAsync = promisify(fs.unlink);
  const pdf = require("html-pdf");
  const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };
  const router = {};

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    // userQuery.push({'$match':{'addedBy':{$eq:'agency'}}});
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    userQuery.push({ $match: { status: { $exists: true } } });
    userQuery.push({ $match: { status: { $gte: 1 } } });
    userQuery.push({ $match: { "onlineform.form_status": { $gte: 1 } } });

    // if (req.body.search) {
    //     var searchs = req.body.search;
    //     var searching = {};
    //     searching[req.body.filter] = { $regex: searchs + '.*', $options: 'si' };
    //     userQuery.push({ '$match': searching });
    // }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { "onlineform.submitted_date": -1 } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { "onlineform.submitted_date": { $gt: new Date(req.body.from_date) } } });
    }
    if (req.body.to_date) {
      userQuery.push({ $match: { "onlineform.submitted_date": { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({
      $project: {
        "createdAt": 1,
        "updatedAt": 1,
        "username": 1,
        "email": 1,
        "status": 1,
        "address": 1,
        "phone": 1,
        "name": 1,
        "job_type": 1,
        "isverified": 1,
        "locations": 1,
        "avatar": 1,
        "holiday_allowance": 1,
        "joining_date": 1,
        "final_date": 1,
        "onlineform.form_status": 1,
        "onlineform.final_verify_status": 1,
        "onlineform.submitted_date": 1
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lon",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "job_data"
        }
      },
      { $addFields: { job_type: "$job_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    //  var withoutlimit = JSON.parse(JSON.stringify(userQuery));
    //  withoutlimit.push({
    //      $count:'count'
    //  });

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }
    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];
    db.GetAggregation("employees", finalquery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = { finalquery: finalquery, result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };
        res.send(data);
      }
    });
  };

  router.formupdate = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("forpage", "Invalid Form").notEmpty();
    req.checkBody("id", "Invalid Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const employeeid = req.body.id;
    const forpage = Number(req.body.forpage);
    const det = req.body;
    let tab;
    const update = tab => {
      if (tab) {
        db.UpdateDocument("employees", { _id: employeeid }, tab, {}, function(err, updatedoc) {
          if (err || !updatedoc) {
            data.response = "Unable to save your data";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Data Submitted";
            res.send(data);
          }
        });
      }
    };
    if (employeeid) {
      if (forpage === 1) {
        if (det.postapplyfor && det.availablestartdate && det.postapplydetails && det.personaldetails && det.employmenteligibility && det.drivingdetails && det.langauages && det.nextofkindetails && det.aboutyourwork) {
          let attach;
          if (req.files && typeof req.files.UploadIDPhoto !== "undefined") {
            if (req.files.UploadIDPhoto.length > 0) {
              attach = attachment.get_attachment(req.files.UploadIDPhoto[0].destination, req.files.UploadIDPhoto[0].filename);
              tab = {
                $set: {
                  "onlineform.postapplyfor": det.postapplyfor,
                  "onlineform.availablestartdate": det.availablestartdate,
                  "onlineform.postapplydetails": det.postapplydetails,
                  "onlineform.personaldetails": det.personaldetails,
                  "onlineform.employmenteligibility": det.employmenteligibility,
                  "onlineform.drivingdetails": det.drivingdetails,
                  "onlineform.langauages": det.langauages,
                  "onlineform.nextofkindetails": det.nextofkindetails,
                  "onlineform.aboutyourwork": det.aboutyourwork,
                  "onlineform.UploadIDPhoto": attach
                }
              };
            }
          } else {
            tab = {
              $set: {
                "onlineform.postapplyfor": det.postapplyfor,
                "onlineform.availablestartdate": det.availablestartdate,
                "onlineform.postapplydetails": det.postapplydetails,
                "onlineform.personaldetails": det.personaldetails,
                "onlineform.employmenteligibility": det.employmenteligibility,
                "onlineform.drivingdetails": det.drivingdetails,
                "onlineform.langauages": det.langauages,
                "onlineform.nextofkindetails": det.nextofkindetails,
                "onlineform.aboutyourwork": det.aboutyourwork
              }
            };
          }
          update(tab);
        } else {
          data.response = "Unable to save your data";
          res.send(data);
        }
      } else if (forpage === 2) {
        let attachCV;
        if (det.reference1 && det.reference2 && det.employmenthistory && det.employmenthistory.length > 0 && det.education && det.education.length > 0) {
          if (req.files && typeof req.files.UploadCV !== "undefined") {
            if (req.files.UploadCV.length > 0) {
              attachCV = attachment.get_attachment(req.files.UploadCV[0].destination, req.files.UploadCV[0].filename);
              tab = {
                $set: {
                  "onlineform.reference1.firstrefrelationship": det.reference1.firstrefrelationship,
                  "onlineform.reference1.firstreffirstname": det.reference1.firstreffirstname,
                  "onlineform.reference1.firstreflastname": det.reference1.firstreflastname,
                  "onlineform.reference1.firstrefphone": det.reference1.firstrefphone,
                  "onlineform.reference1.firstrefemail": det.reference1.firstrefemail,
                  "onlineform.reference1.firstrefconfirmemail": det.reference1.firstrefconfirmemail,
                  "onlineform.reference1.Address": det.reference1.Address,
                  "onlineform.reference2.secondrefrelationship": det.reference2.secondrefrelationship,
                  "onlineform.reference2.secondreffirstname": det.reference2.secondreffirstname,
                  "onlineform.reference2.secondreflastname": det.reference2.secondreflastname,
                  "onlineform.reference2.secondrefphone": det.reference2.secondrefphone,
                  "onlineform.reference2.secondrefemail": det.reference2.secondrefemail,
                  "onlineform.reference2.secondrefconfirmemail": det.reference2.secondrefconfirmemail,
                  "onlineform.reference2.Address": det.reference2.Address,
                  "onlineform.employmenthistory": det.employmenthistory,
                  "onlineform.education": det.education,
                  "onlineform.UploadCV": attachCV
                }
              };
            }
          } else {
            tab = {
              $set: {
                "onlineform.reference1.firstrefrelationship": det.reference1.firstrefrelationship,
                "onlineform.reference1.firstreffirstname": det.reference1.firstreffirstname,
                "onlineform.reference1.firstreflastname": det.reference1.firstreflastname,
                "onlineform.reference1.firstrefphone": det.reference1.firstrefphone,
                "onlineform.reference1.firstrefemail": det.reference1.firstrefemail,
                "onlineform.reference1.firstrefconfirmemail": det.reference1.firstrefconfirmemail,
                "onlineform.reference1.Address": det.reference1.Address,
                "onlineform.reference2.secondrefrelationship": det.reference2.secondrefrelationship,
                "onlineform.reference2.secondreffirstname": det.reference2.secondreffirstname,
                "onlineform.reference2.secondreflastname": det.reference2.secondreflastname,
                "onlineform.reference2.secondrefphone": det.reference2.secondrefphone,
                "onlineform.reference2.secondrefemail": det.reference2.secondrefemail,
                "onlineform.reference2.secondrefconfirmemail": det.reference2.secondrefconfirmemail,
                "onlineform.reference2.Address": det.reference2.Address,
                "onlineform.employmenthistory": det.employmenthistory,
                "onlineform.education": det.education
              }
            };
          }
          update(tab);
        } else {
          data.response = "Unable to save your data";
          res.send(data);
        }
      } else if (forpage === 3) {
        tab = {
          $set: {
            "onlineform.mandatory_training": det.mandatory_training || "",
            "onlineform.movinghandling": det.movinghandling || 0,
            "onlineform.basiclifesupport": det.basiclifesupport || 0,
            "onlineform.healthsafety": det.healthsafety || 0,
            "onlineform.firesafety": det.firesafety || 0,
            "onlineform.firstaid": det.firstaid || 0,
            "onlineform.infectioncontrol": det.infectioncontrol || 0,
            "onlineform.foodsafety": det.foodsafety || 0,
            "onlineform.medicationadmin": det.medicationadmin || 0,
            "onlineform.safeguardingvulnerable": det.safeguardingvulnerable || 0,
            "onlineform.trainingdates": det.trainingdates || "",
            "onlineform.healthcare_training": det.healthcare_training || "",
            "onlineform.diplomal3": det.diplomal3 || 0,
            "onlineform.diplomal2": det.diplomal2 || 0,
            "onlineform.personalsafetymental": det.personalsafetymental || 0,
            "onlineform.intermediatelife": det.intermediatelife || 0,
            "onlineform.advancedlife": det.advancedlife || 0,
            "onlineform.complaintshandling": det.complaintshandling || 0,
            "onlineform.handlingviolence": det.handlingviolence || 0,
            "onlineform.doolsmental": det.doolsmental || 0,
            "onlineform.coshh": det.coshh || 0,
            "onlineform.dataprotection": det.dataprotection || 0,
            "onlineform.equalityinclusion": det.equalityinclusion || 0,
            "onlineform.loneworkertraining": det.loneworkertraining || 0,
            "onlineform.resuscitation": det.resuscitation || 0,
            "onlineform.interpretation": det.interpretation || 0,
            "onlineform.trainindates2": det.trainindates2 || ""
          }
        };
        update(tab);
      } else if (forpage === 4) {
        let attach1;
        let attach2;
        let attach3;
        let attach4;
        // if (det.uploadcertificate && det.uploadpassport && det.uploadaddress && det.uploaddbs) {
        if (req.files && typeof req.files.uploadcertificate !== "undefined" && typeof req.files.uploadpassport !== "undefined" && typeof req.files.uploadaddress !== "undefined" && typeof req.files.uploaddbs !== "undefined") {
          if (req.files.uploadcertificate.length > 0 && req.files.uploadpassport.length > 0 && req.files.uploadaddress.length > 0 && req.files.uploaddbs.length > 0) {
            attach1 = attachment.get_attachment(req.files.uploadcertificate[0].destination, req.files.uploadcertificate[0].filename);
            attach2 = attachment.get_attachment(req.files.uploadpassport[0].destination, req.files.uploadpassport[0].filename);
            attach3 = attachment.get_attachment(req.files.uploadaddress[0].destination, req.files.uploadaddress[0].filename);
            attach4 = attachment.get_attachment(req.files.uploaddbs[0].destination, req.files.uploaddbs[0].filename);
            tab = {
              $set: {
                "onlineform.uploadcertificate": attach1,
                "onlineform.uploadpassport": attach2,
                "onlineform.uploadaddress": attach3,
                "onlineform.uploaddbs": attach4
              }
            };
            update(tab);
          } else {
            data.response = "Form Fields Incomplete";
            res.send(data);
          }
        } else {
          data.response = "Form Fields Incomplete";
          res.send(data);
        }
        // }
      } else if (forpage === 5) {
        if (det.healthdeclaration && det.gpdetails && det.medicalhistory && det.tuberculosis && det.chickenpoxorshingles && det.immunisationhistory && det.declaration && det.righttowork && det.worktimedirectives) {
          tab = {
            $set: {
              "onlineform.healthdeclaration": det.healthdeclaration,
              "onlineform.gpdetails": det.gpdetails,
              "onlineform.medicalhistory": det.medicalhistory,
              "onlineform.tuberculosis": det.tuberculosis,
              "onlineform.chickenpoxorshingles": det.chickenpoxorshingles,
              "onlineform.immunisationhistory": det.immunisationhistory,
              "onlineform.declaration": det.declaration,
              "onlineform.righttowork": det.righttowork,
              "onlineform.worktimedirectives": det.worktimedirectives
            }
          };
          update(tab);
        } else {
          data.response = "Unable to save your data";
          res.send(data);
        }
      }
    } else {
      // console.log("Error : Employee ID required");
      data.response = "Unable to save your data";
      res.send(data);
    }
  };
  router.verification = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Invalid").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const update_data = { $set: { "onlineform.skillcheck": req.body.skillcheck, "onlineform.healthcheck": req.body.healthcheck, "onlineform.dbscheck": req.body.dbscheck } };
    db.UpdateDocument("employees", { _id: req.body.id }, update_data, {}, (err, updateDoc) => {
      if (err || !updateDoc) {
        data.response = "Unable to submit your data";
        res.send(data);
      } else {
        db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, EmployeeData) => {
          if (err || !EmployeeData) {
            data.response = "Unable to get employee data";
            res.send(data);
          } else {
            if (EmployeeData.onlineform.skillcheck.status === 1 && EmployeeData.onlineform.dbscheck.status === 1 && EmployeeData.onlineform.healthcheck.status === 1) {
              db.UpdateDocument("employees", { _id: req.body.id }, { $set: { "onlineform.form_status": 5 } }, {}, (err, updateDoc) => {
                if (err || !updateDoc) {
                  data.response = "Unable to submit your data";
                  res.send(data);
                } else {
                  data.status = 1;
                  data.response = "Updated Successfully";
                  res.send(data);
                }
              });
            } else {
              data.status = 1;
              data.response = "Updated Successfully";
              res.send(data);
            }
          }
        });
      }
    });
  };
  router.complianceemail = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Compliance",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "form not Send";
        res.send(data);
      } else {
        if (req.body.edit_form) {
          const update_data = {
            $set: {
              "onlineform.form_status": 0
            }
          };
          db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(datas.employee_data._id) }, update_data, {}, (err, updateDoc) => {});
        }
        data.status = 1;
        data.response = "Mail Sent";
        res.send(data);
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} , Please provie details requested to complete Online Application Form Process (${req.params.loginData.company_name}).`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
      }
    });
  };
  router.applicationform = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Online Application Form",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "Mail not Send";
        res.send(data);
      } else {
        if (req.body.application_status) {
          const update_data = {
            $set: {
              "onlineform.form_status": 2
            }
          };
          db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(datas.employee_data._id) }, update_data, {}, (err, updateDoc) => {});
        }
        data.status = 1;
        data.response = "Mail Sent";
        res.send(data);
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} , Your Online Application Form is Approved, You will Contacted for Interview very Shoortly (${req.params.loginData.company_name}).`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
      }
    });
  };
  router.interviewdetailssave = (req, res) => {
    const datas = req.body;
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Invalid").notEmpty();
    if (datas.pagefor === "interviewcall") {
      req.checkBody("interviewdate", "Date Required").notEmpty();
      req.checkBody("interviewtime", "Time Required").notEmpty();
      req.checkBody("videocallurl", "Video Call URL Required").notEmpty();
      req.checkBody("interviewnotes", "Notes Required").notEmpty();
    } else if (datas.pagefor === "postinterviewcall") {
      req.checkBody("intervieweddate", "Date Required").notEmpty();
      req.checkBody("interviewednotes", "Notes Required").notEmpty();
      req.checkBody("interviewedstatus", "Status Required").notEmpty();
    }

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    let update_data;
    if (datas.pagefor === "interviewcall") {
      update_data = {
        $set: {
          "onlineform.interview.interviewdate": req.body.interviewdate,
          "onlineform.interview.interviewtime": req.body.interviewtime,
          "onlineform.interview.interviewnotes": req.body.interviewnotes,
          "onlineform.interview.videocallurl": req.body.videocallurl
          // "onlineform.form_status": 3
        }
      };
    } else if (datas.pagefor === "postinterviewcall") {
      update_data = {
        $set: {
          "onlineform.interview.status": req.body.interviewedstatus,
          "onlineform.interview.intervieweddate": req.body.intervieweddate,
          "onlineform.interview.interviewednotes": req.body.interviewednotes
          // "onlineform.form_status": 4
        }
      };
    }
    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, EmployeeData) => {
      if (err || !EmployeeData) {
        data.response = "Unable to get employee data";
        res.send(data);
      } else {
        if (EmployeeData.onlineform.form_status > 1) {
          db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, update_data, {}, (err, updateDoc) => {
            if (err || !updateDoc) {
              data.response = "Unable to submit your data";
              res.send(data);
            } else {
              data.status = 1;
              data.response = "Updated!";
              res.send(data);
            }
          });
        } else {
          data.status = 0;
          data.response = "Verify Application form";
          res.send(data);
        }
      }
    });
  };
  router.interviewcall = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Interview Call",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "form not Send";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Mail Sent";
        res.send(data);
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} , You have Interview Request from ${req.params.loginData.company_name}.`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
        const update_data = {
          $set: {
            "onlineform.form_status": 3
          }
        };
        db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, update_data, {}, (err, updateDoc) => {});
      }
    });
  };
  router.postinterviewcall = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Interview Status",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "Mail not Send";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Mail Sent";
        res.send(data);
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} ,You Was ${datas.interviewedstatus} in Interview from ${req.params.loginData.company_name}.`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
        const update_data = {
          $set: {
            "onlineform.form_status": 4
          }
        };
        db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, update_data, {}, (err, updateDoc) => {});
      }
    });
  };
  router.verificationstatus = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Verification Status",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "Mail not Send";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Mail Sent";
        res.send(data);
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} ,Your Skills,DBS, HEalth Checks Verification Completed from ${req.params.loginData.company_name}.`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
        const update_data = {
          $set: {
            "onlineform.form_status": 6
          }
        };
        db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, update_data, {}, (err, updateDoc) => {});
      }
    });
  };
  router.referenceverificationstatus = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Required").notEmpty();
    req.checkBody("referencefor", "Reference Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Verification Status",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "Mail not Send";
        res.send(data);
      } else {
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} ,Your Reference Verification Completed from ${req.params.loginData.company_name}.`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });

        const update_data = {
          $set: {
            ["onlineform." + datas.referencefor + ".referee.status"]: 4
          }
        };
        db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(datas.id) }, update_data, {}, (err, updateDoc) => {
          if (err || !updateDoc) {
            data.response = "Unable to submit your data";
            res.send(data);
          } else {
            db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, EmployeeData) => {
              if (EmployeeData) {
                console.log("EmployeeData.onlineform.reference1.referee.status ", EmployeeData.onlineform.reference1.referee.status);
                if (EmployeeData.onlineform.reference1.referee.status === 4 && EmployeeData.onlineform.reference2.referee.status === 4) {
                  db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, { $set: { "onlineform.form_status": 8 } }, {}, (err, updateDoc) => {});
                }
              }
            });
            data.status = 1;
            data.response = "Mail Sent";
            res.send(data);
          }
        });
      }
    });
  };
  router.finalstatus = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("employee_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Required").notEmpty();
    req.checkBody("final_verify_status", "Status Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: datas.employee_email || req.params.loginData.company_email,
      to: datas.employee_email,
      subject: req.body.email_subject,
      text: "Verification Status",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "Mail not Send";
        res.send(data);
      } else {
        const update_data = {
          $set: {
            "onlineform.form_status": 9
          }
        };
        const message = Number(datas.final_verify_status) === 1 ? "Qualified As Employee" : "Disqualified As Employee";
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.employee_data.username} ,You are ${message} for ${req.params.loginData.company_name}.`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
        db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(datas.id) }, update_data, {}, (err, updateDoc) => {
          if (err || !updateDoc) {
            data.response = "Unable to submit your data";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Mail Sent";
            res.send(data);
          }
        });
      }
    });
  };
  router.sendrefereeform = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("refe_email", "Email Required").notEmpty();
    req.checkBody("email_subject", "Email Subject Required").notEmpty();
    req.checkBody("email_content", "Email Content Required").notEmpty();
    req.checkBody("referencefor", "Reference Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    const mailOptions = {
      from: req.params.loginData.company_email || datas.refe_email,
      to: datas.refe_email,
      subject: req.body.email_subject,
      text: "Reference Form",
      html: req.body.email_content
    };
    mail.send(mailOptions, function(err, response) {
      if (err) {
        data.status = 0;
        data.response = "form not Send";
        res.send(data);
      } else {
        const smsto = datas.employee_data.phone.code + datas.employee_data.phone.number;
        const smsmessage = `Hi ${datas.refereename} ,You Was Referred  ${datas.employee_data.username} for ${req.params.loginData.company_name}. Please fill the form to accept his application (Apllication send in mail).`;
        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
          // console.log("SMS err,SMS response", err, response);
        });
        db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, EmployeeData) => {
          if (EmployeeData) {
            const refereefr = datas.referencefor === "refrence1" ? "reference1" : datas.referencefor === "refrence2" ? "reference2" : "";
            if (EmployeeData.onlineform[refereefr].referee.status === 0) {
              db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, { $set: { ["onlineform." + refereefr + ".referee.status"]: 1 } }, {}, (err, updateDoc) => {
                if (!err && updateDoc) {
                  db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, EmployeeDatas) => {
                    if (EmployeeDatas) {
                      if (EmployeeDatas.onlineform.reference1.referee.status === 1 && EmployeeDatas.onlineform.reference2.referee.status === 1) {
                        if (EmployeeDatas.onlineform.form_status < 8) {
                          db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, { $set: { "onlineform.form_status": 7 } }, {}, (err, updateDoc) => {});
                        }
                      }
                    }
                  });
                }
              });
            }
            if (EmployeeData.onlineform.reference1.referee.status === 1 && EmployeeData.onlineform.reference2.referee.status === 1) {
              if (EmployeeData.onlineform.form_status < 8) {
                console.log(' EmployeeData.onlineform.form_status',EmployeeData.onlineform.form_status )
                db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, { $set: { "onlineform.form_status": 7 } }, {}, (err, updateDoc) => {});
              }
            }
          }
        });
        data.status = 1;
        data.response = "Form Sen";
        res.send(data);
      }
    });
  };
  router.verifyrefereeform = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Invalid").notEmpty();
    req.checkBody("referencefor", "Invalid Form").notEmpty();
    req.checkBody("referenceverifydate", "Date Required").notEmpty();
    req.checkBody("referenceverifynotes", "Notes Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(datas.id) }, {}, {}, (err, employeeData) => {
      if (err || !employeeData) {
        data.response = "Unable to get employee data";
        res.send(data);
      } else {
        if (employeeData.onlineform.form_status >= 4) {
          const update_data = {
            $set: {
              ["onlineform." + datas.referencefor + ".referee.status"]: 3,
              ["onlineform." + datas.referencefor + ".referee.verified_date"]: req.body.referenceverifydate,
              ["onlineform." + datas.referencefor + ".referee.verified_notes"]: req.body.referenceverifynotes,
              ["onlineform." + datas.referencefor + ".referee.verified_status"]: req.body.referenceverifystatus
            }
          };

          db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(datas.id) }, update_data, {}, (err, updateDoc) => {
            if (err || !updateDoc) {
              data.response = "Unable to submit your data";
              res.send(data);
            } else {
              // db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, EmployeeData) => {
              //     if (EmployeeData) {
              //         if (EmployeeData.onlineform.reference1.referee.status === 3 && EmployeeData.onlineform.reference2.referee.status === 3) {
              //             db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, { $set: { "onlineform.form_status": 8 } }, {}, (err, updateDoc) => {});
              //         }
              //     }
              // });
              data.status = 1;
              data.response = "Updated Successfully";
              res.send(data);
            }
          });
        } else {
          data.status = 0;
          data.response = "Complete the interview process";
          res.send(data);
        }
      }
    });
  };
  router.referenceupdate = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("referencefor", "Invalid Form").notEmpty();
    req.checkBody("campanystamp", "Stamp Required").optional();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const value = req.body;
    const newkey = req.body.referencefor;
    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(value.id) }, {}, {}, (err, employeeData) => {
      if (err || !employeeData) {
        data.response = "Unable to get employee data";
        res.send(data);
      } else {
        let attach;
        if (req.files && req.files.campanystamp && req.files.campanystamp.length > 0) {
          attach = attachment.get_attachment(req.files.campanystamp[0].destination, req.files.campanystamp[0].filename);
        }
        const update = {
          $set: {
            ["onlineform." + newkey + ".referee.refereefirstname"]: value.refereefirstname,
            ["onlineform." + newkey + ".referee.refereelastname"]: value.refereelastname,
            ["onlineform." + newkey + ".referee.refereeemail"]: value.refereeemail,
            ["onlineform." + newkey + ".referee.refereephone"]: value.refereephone,
            ["onlineform." + newkey + ".referee.refereejobtitle"]: value.refereejobtitle,
            ["onlineform." + newkey + ".referee.refereecompany"]: value.refereecompany,
            ["onlineform." + newkey + ".referee.refereecapacity"]: value.refereecapacity,
            ["onlineform." + newkey + ".referee.applicantfirstname"]: value.applicantfirstname,
            ["onlineform." + newkey + ".referee.applicantlastname"]: value.applicantlastname,
            ["onlineform." + newkey + ".referee.applicantemail"]: value.applicantemail,
            ["onlineform." + newkey + ".referee.applicantphone"]: value.applicantphone,
            ["onlineform." + newkey + ".referee.applicantjobtitle"]: value.applicantjobtitle,
            ["onlineform." + newkey + ".referee.applicantcompany"]: value.applicantcompany,
            ["onlineform." + newkey + ".referee.employedfrom"]: value.employedfrom,
            ["onlineform." + newkey + ".referee.employedto"]: value.employedto,
            ["onlineform." + newkey + ".referee.remployee"]: value.remployee,
            ["onlineform." + newkey + ".referee.remployeenotes"]: value.remployeenotes || "",
            ["onlineform." + newkey + ".referee.followcareplans"]: value.followcareplans,
            ["onlineform." + newkey + ".referee.reliability"]: value.reliability,
            ["onlineform." + newkey + ".referee.character"]: value.character,
            ["onlineform." + newkey + ".referee.dignity"]: value.dignity,
            ["onlineform." + newkey + ".referee.attitude"]: value.attitude,
            ["onlineform." + newkey + ".referee.communication"]: value.communication,
            ["onlineform." + newkey + ".referee.relationships"]: value.relationships,
            ["onlineform." + newkey + ".referee.workunderinitiative"]: value.workunderinitiative,
            ["onlineform." + newkey + ".referee.disciplinaryaction"]: value.disciplinaryaction,
            ["onlineform." + newkey + ".referee.disciplinaryactiondetails"]: value.disciplinaryactiondetails || "",
            ["onlineform." + newkey + ".referee.investigations"]: value.investigations,
            ["onlineform." + newkey + ".referee.investigationsdetails"]: value.investigationsdetails || "",
            ["onlineform." + newkey + ".referee.vulnerablepeople"]: value.vulnerablepeople,
            ["onlineform." + newkey + ".referee.vulnerablepeopledetails"]: value.vulnerablepeopledetails || "",
            ["onlineform." + newkey + ".referee.criminaloffence"]: value.criminaloffence,
            ["onlineform." + newkey + ".referee.criminaloffencedetails"]: value.criminaloffencedetails || "",
            ["onlineform." + newkey + ".referee.additionalcomments"]: value.additionalcomments || "",
            ["onlineform." + newkey + ".referee.confirm"]: value.confirm,
            ["onlineform." + newkey + ".referee.agree"]: value.agree,
            ["onlineform." + newkey + ".referee.campanystamp"]: attach,
            ["onlineform." + newkey + ".referee.status"]: value.status
          }
        };
        db.UpdateDocument("employees", { _id: employeeData._id }, update, {}, (err, updatedoc) => {
          if (err || !updatedoc) {
            data.response = "Unable to save your data";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Data Submitted";
            res.send(data);
          }
        });
      }
    });
  };
  router.finalverification = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Invalid").notEmpty();
    req.checkBody("final_verify_notes", "Notes Required").notEmpty();
    req.checkBody("final_verify_date", "Date Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const datas = req.body;
    db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(datas.id) }, {}, {}, (err, EmployeeData) => {
      if (err || !EmployeeData) {
        data.response = "Unable to get employee data";
        res.send(data);
      } else {
        if (EmployeeData.onlineform.form_status >= 8) {
          const update_data = {
            $set: {
              // "onlineform.form_status": 9,
              "onlineform.final_verify_notes": datas.final_verify_notes,
              "onlineform.final_verify_date": datas.final_verify_date,
              "onlineform.final_verify_status": datas.final_verify_status
            }
          };
          db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(datas.id) }, update_data, {}, (err, updateDoc) => {
            if (err || !updateDoc) {
              data.response = "Unable to submit your data";
              res.send(data);
            } else {
              const message_status = Number(datas.final_verify_status) === 1 ? "qualified_employee" : "disqualified_employee";
              const message = Number(datas.final_verify_status) === 1 ? "Qualified As Employee" : "Disqualified As Employee";
              const options = { employee: datas.id };
              push.addnotification(datas.id, message, message_status, options, "EMPLOYEE", (err, response, body) => {});
              /* db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(EmployeeData.agency) }, {}, {}, (err, AgencyData) => {
                if (EmployeeData && AgencyData) {
                  if (EmployeeData.settings && EmployeeData.settings.notifications.email) {
                    // Mail Sent To All.
                    const mailData = {};
                    mailData.template = "finalverifystatustoemployee";
                    mailData.from = req.params.loginData.company_email;
                    mailData.to = EmployeeData.email;
                    mailData.html = [];
                    mailData.html.push({ name: "employee", value: EmployeeData.username });
                    mailData.html.push({ name: "empstatus", value: message });
                    mailData.html.push({ name: "agency", value: AgencyData.company_name });
                    mailcontent.sendmail(mailData, (err, response) => {
                      // console.log("mail err,mail response", err, response);
                    });
                  }
                  if (EmployeeData.settings && EmployeeData.settings.notifications.sms) {
                    // SMS Send To All
                    const smsto = EmployeeData.phone.code + EmployeeData.phone.number;
                    const smsmessage = `Hi ${EmployeeData.username} , You are ${message} for ${AgencyData.company_name}`;
                    twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                      // console.log("SMS err,SMS response", err, response);
                    });
                  }
                  if (Number(datas.final_verify_status) === 1) {
                    const mailData = {};
                    mailData.template = "candidatewelcome";
                    mailData.to = EmployeeData.email;
                    mailData.from = req.params.loginData.company_email;
                    mailData.agency = EmployeeData.agency || req.params.loginId;
                    mailData.html = [];
                    mailData.html.push({ name: "employee", value: EmployeeData.username });
                    mailData.html.push({ name: "agency", value: req.params.loginData.company_name });
                    mailcontent.sendcustommail(mailData, (err, response) => {
                      // console.log("err, response",err, response);
                    });
                  }
                }
              });*/
              data.status = 1;
              data.response = "Updated Successfully";
              res.send(data);
            }
          });
        } else {
          data.status = 0;
          data.response = "Please Complete Previous Steps";
          res.send(data);
        }
      }
    });
  };
  router.userexport = (req, res) => {
    const data = {};
    data.status = 0;

    const userQuery = [];
    /* if(req.body.id){
      userQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    }else{ */
    userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    // }
    userQuery.push({ $match: { status: { $exists: true } } });
    userQuery.push({ $match: { status: { $gte: 1 } } });
    userQuery.push({ $match: { "onlineform.form_status": { $gte: 1 } } });
    userQuery.push({
      $project: {
        "createdAt": 1,
        "updatedAt": 1,
        "username": 1,
        "email": 1,
        "status": 1,
        "address": 1,
        "phone": 1,
        "name": 1,
        "job_type": 1,
        "isverified": 1,
        "locations": 1,
        "avatar": 1,
        "holiday_allowance": 1,
        "joining_date": 1,
        "final_date": 1,
        "onlineform.form_status": 1,
        "onlineform.final_verify_status": 1,
        "onlineform.submitted_date": 1
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lon",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "job_data"
        }
      },
      { $addFields: { job_type: "$job_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("employees", userQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Name", "Email", "Job Role", "Area", "Submitted Date", "Status"];
          const mydata = docdata;
          const finaldata = [];
          const status = (num, verify) => {
            if (num && verify) {
              if (num == 1) {
                return "New";
              } else if (num == 2) {
                return "Verified";
              } else if (num == 3) {
                return "Interview Scheduled";
              } else if (num == 4) {
                return "Interview Completed";
              } else if (num == 5) {
                if (verify == 1) {
                  return "Qualified";
                } else {
                  return "Disqualified";
                }
              }
            } else {
              return "-";
            }
          };
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Name"] = mydata[i].name || "";
            temp["Email"] = mydata[i].email || "";
            temp["Job Role"] = mydata[i].job_type.join() || "";
            temp["Area"] = mydata[i].locations.join() || "";
            temp["Submitted Date"] = mydata[i].onlineform && mydata[i].onlineform.submitted_date ? moment(mydata[i].onlineform.submitted_date).format("DD-MM-YYYY") : "-";
            temp["Status"] = status(mydata[i].onlineform ? mydata[i].onlineform.form_status : "", mydata[i].onlineform ? mydata[i].onlineform.final_verify_status : "");
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  /* router.userexport = (req, res) => {
    const data = {};
    data.status = 0;

    const userQuery = [];

    userQuery.push({ $match: { status: { $exists: true } } });

    userQuery.push({
      $lookup: {
        from: "stats",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$employee", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
          { $project: { employee: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
          { $group: { _id: "$employee", count: { $sum: '$hours' } } },
          { $project: { total: { $divide: ['$count', 3600] } } },
        ],
        as: "hours"
      }
    });

    userQuery.push({
      $lookup: {
        from: "shifts",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$employee", "$$agencyid"] }] } } },
          { $sort: { 'createdAt': -1 } },
          { $limit: 1 },
          { $project: { start_date: 1 } }
          /* { $project: { employee: 1, hours: { $subtract: ["$starttime", "$endtime"] } } },
          { $group: {_id: "$employee", count: {$sum: '$hours'}}},
          { $project: { total: {$divide: ['$count', 3600]}}}, //
        ],
        as: "lastshift"
      }
    });

    userQuery.push({
      $project: {
        createdAt: 1,
        updatedAt: 1,
        username: 1,
        email: 1,
        status: 1,
        address: 1,
        phone: 1,
        name: 1,
        job_type: 1,
        isverified: 1,
        locations: 1,
        avatar: 1,
        holiday_allowance: 1,
        joining_date: 1,
        final_date: 1,
        hours: '$hours.total',
        lastshift: '$lastshift.start_date'
      }
    });

    userQuery.push({
      $addFields: {
        lat: "$address.lat",
        lng: "$address.lon",
        label: { $substr: ["$name", 0, 1] }
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "job_data"
        }
      },
      { $addFields: { job_type: "$job_data.name" } }
    );

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("employees", userQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Name", "Job Role", "Tel", "Area", "Status", "Hours", "Last Shift"];
          const mydata = docdata;
          let finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            let temp = {};
            temp["Name"] = mydata[i].name || "";
            temp["Job Role"] = mydata[i].job_type.join() || "";
            temp["Tel"] = (mydata[i].phone && (mydata[i].phone.code || "")) + "-" + (mydata[i].phone && (mydata[i].phone.number || ""));
            temp["Area"] = mydata[i].locations.join() || "";
            temp["Status"] = mydata[i].status == 1 ? "Active" : "In-Active";
            temp["Hours"] = (mydata[i].hours && mydata[i].hours.length > 0) ? (mydata[i].hours[0] || "0") : "0";
            temp["Last Shift"] = (mydata[i].lastshift && mydata[i].lastshift.length > 0) ? (moment(mydata[i].lastshift[0]).format("DD-MM-YYYY") || "-") : "-";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */
  router.savefiles = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("filefor", "Invalid Input").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const Update_api = datas => {
      db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, datas, {}, (err, updatedoc) => {
        if (err || !updatedoc) {
          data.response = "Unable to save your file";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "File Saved";
          res.send(data);
        }
      });
    };
    if (req.files && req.files.uploadfiles && req.files.uploadfiles.length > 0) {
      const files_data = { path: attachment.get_attachment(req.files.uploadfiles[0].destination, req.files.uploadfiles[0].filename), name: req.files.uploadfiles[0].originalname };
      switch (req.body.filefor) {
        case "certificate":
          Update_api({ $push: { "onlineform.uploadcertificate": files_data } });
          break;
        case "passport":
          Update_api({ $push: { "onlineform.uploadpassport": files_data } });
          break;
        case "address":
          Update_api({ $push: { "onlineform.uploadaddress": files_data } });
          break;
        case "dbs":
          Update_api({ $push: { "onlineform.uploaddbs": files_data } });
          break;
        case "other":
          Update_api({ $push: { "onlineform.uploadothers": files_data } });
          break;
        default:
          break;
      }
    } else {
      data.response = "Unable to save your file";
      res.send(data);
    }
  };
  router.deletefiles = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("filefor", "Invalid Path").notEmpty();
    req.checkBody("filepath", "Invalid File").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const Update_api = (datas, data_path) => {
      db.UpdateDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, datas, {}, (err, updatedoc) => {
        if (err || !updatedoc) {
          data.response = "Unable to delete your file";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "File Deleted";
          res.send(data);
          unlinkAsync(data_path);
        }
      });
    };
    const file_p = req.body.filepath;
    if (req.body.filepath) {
      switch (req.body.filefor) {
        case "certificate":
          Update_api({ $pull: { "onlineform.uploadcertificate": { path: file_p } } }, file_p);
          break;
        case "passport":
          Update_api({ $pull: { "onlineform.uploadpassport": { path: file_p } } }, file_p);
          break;
        case "address":
          Update_api({ $pull: { "onlineform.uploadaddress": { path: file_p } } }, file_p);
          break;
        case "dbs":
          Update_api({ $pull: { "onlineform.uploaddbs": { path: file_p } } }, file_p);
          break;
        case "other":
          Update_api({ $pull: { "onlineform.uploadothers": { path: file_p } } }, file_p);
          break;
        default:
          break;
      }
    } else {
      data.response = "Unable to delete your file";
      res.send(data);
    }
  };
  router.refereeformdownload = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("referencefor", "Invalid Path").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
      const newkey = req.body.referencefor;
      db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, employeeData) => {
          if (employeeData) {
              let refereefirstname = "",
                  refereelastname = "",
                  refereeemail = "",
                  refereephone = "",
                  refereejobtitle = "",
                  refereecompany = "",
                  refereecapacity = "",
                  applicantfirstname = "",
                  applicantlastname = "",
                  applicantemail = "",
                  applicantphone = "",
                  applicantjobtitle = "",
                  applicantcompany = "",
                  employedfrom = "",
                  employedto = "",
                  remployee = "",
                  remployeenotes = "",
                  followcareplans = "",
                  reliability = "",
                  character = "",
                  dignity = "",
                  attitude = "",
                  communication = "",
                  relationships = "",
                  workunderinitiative = "",
                  disciplinaryaction = "",
                  disciplinaryactiondetails = "",
                  investigations = "",
                  investigationsdetails = "",
                  vulnerablepeople = "",
                  vulnerablepeopledetails = "",
                  criminaloffences = "",
                  criminaloffencedetails = "",
                  additionalcomments = "",
                  campanystamp = "",
                  form_reference1 = {},
                  form_reference2 = {};
              if (newkey === "reference1") {
                  form_reference1 = employeeData.onlineform.reference1.referee || {};
                  refereefirstname = form_reference1.refereefirstname;
                  refereelastname = form_reference1.refereelastname;
                  refereeemail = form_reference1.refereeemail;
                  refereephone = form_reference1.refereephone;
                  refereejobtitle = form_reference1.refereejobtitle;
                  refereecompany = form_reference1.refereecompany;
                  refereecapacity = form_reference1.refereecapacity;
                  applicantfirstname = form_reference1.applicantfirstname;
                  applicantlastname = form_reference1.applicantlastname;
                  applicantemail = form_reference1.applicantemail;
                  applicantphone = form_reference1.applicantphone;
                  applicantjobtitle = form_reference1.applicantjobtitle;
                  applicantcompany = form_reference1.applicantcompany;
                  employedfrom = form_reference1.employedfrom;
                  employedto = form_reference1.employedto;
                  remployee = form_reference1.remployee;
                  remployeenotes = form_reference1.remployeenotes;
                  followcareplans = form_reference1.followcareplans;
                  reliability = form_reference1.reliability;
                  character = form_reference1.character;
                  dignity = form_reference1.dignity;
                  attitude = form_reference1.attitude;
                  communication = form_reference1.communication;
                  relationships = form_reference1.relationships;
                  workunderinitiative = form_reference1.workunderinitiative;
                  disciplinaryaction = form_reference1.disciplinaryaction;
                  disciplinaryactiondetails = form_reference1.disciplinaryactiondetails;
                  investigations = form_reference1.investigations;
                  investigationsdetails = form_reference1.investigationsdetails;
                  vulnerablepeople = form_reference1.vulnerablepeople;
                  vulnerablepeopledetails = form_reference1.vulnerablepeopledetails;
                  criminaloffences = form_reference1.criminaloffences;
                  criminaloffencedetails = form_reference1.criminaloffencedetails;
                  additionalcomments = form_reference1.additionalcomments;
                  campanystamp = form_reference1.campanystamp;
              } else if (newkey === "reference2") {
                  form_reference2 = employeeData.onlineform.reference2.referee || {};
                  refereefirstname = form_reference2.refereefirstname;
                  refereelastname = form_reference2.refereelastname;
                  refereeemail = form_reference2.refereeemail;
                  refereephone = form_reference2.refereephone;
                  refereejobtitle = form_reference2.refereejobtitle;
                  refereecompany = form_reference2.refereecompany;
                  refereecapacity = form_reference2.refereecapacity;
                  applicantfirstname = form_reference2.applicantfirstname;
                  applicantlastname = form_reference2.applicantlastname;
                  applicantemail = form_reference2.applicantemail;
                  applicantphone = form_reference2.applicantphone;
                  applicantjobtitle = form_reference2.applicantjobtitle;
                  applicantcompany = form_reference2.applicantcompany;
                  employedfrom = form_reference2.employedfrom;
                  employedto = form_reference2.employedto;
                  remployee = form_reference2.remployee;
                  remployeenotes = form_reference2.remployeenotes;
                  followcareplans = form_reference2.followcareplans;
                  reliability = form_reference2.reliability;
                  character = form_reference2.character;
                  dignity = form_reference2.dignity;
                  attitude = form_reference2.attitude;
                  communication = form_reference2.communication;
                  relationships = form_reference2.relationships;
                  workunderinitiative = form_reference2.workunderinitiative;
                  disciplinaryaction = form_reference2.disciplinaryaction;
                  disciplinaryactiondetails = form_reference2.disciplinaryactiondetails;
                  investigations = form_reference2.investigations;
                  investigationsdetails = form_reference2.investigationsdetails;
                  vulnerablepeople = form_reference2.vulnerablepeople;
                  vulnerablepeopledetails = form_reference2.vulnerablepeopledetails;
                  criminaloffences = form_reference2.criminaloffence;
                  criminaloffencedetails = form_reference2.criminaloffencedetails;
                  additionalcomments = form_reference2.additionalcomments;
                  campanystamp = form_reference2.campanystamp;
              }
              const html = `  <style>html {
zoom: 0.55;
}</style>  <table style="margin: 0; padding: 0; color: #000; background: #fff;  padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                  <tr>
                    <td>
                      <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                        <tbody>
                          <tr>
						    <td>
							  <table style="margin: 0px auto; padding: 0 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
							    <tbody>
								  <tr>
								     <td width="30%">
									    <img src="logo.png">
									 </td>
									 <td width="70%" style="background-image:url(staff-profile.png);background-repeat: no-repeat;background-size: cover;height: 124px;font-size: 24px;color: #fff;    padding: 0px 0 0 70px; font-weight: 600;">
									     Referee Form
									 </td>
								  </tr>
								</tbody>
							  </table>
							</td>
						  </tr>
                          <tr>
                            <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                              Referee Details </td>
                          </tr>
                          <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>
                                  <tr>
                                    <td width="50%">
                                       <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">First name</label>
                                        
										<input type="text" value="${refereefirstname || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Email</label>
                                        
										<input type="text" value="${refereeemail || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Job Title</label>
                                        
										<input type="text" value="${refereejobtitle || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										
                                    </td>
                                    <td width="50%">
                                      <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; ">Last name</label>
                                        
										<input type="text" value="${refereelastname || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										 <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; ">Phone</label>
                                        
										<input type="text" value="${(refereephone.code || "") + "-" + (refereephone.number || "-")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										 <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; ">Company</label>
                                        
										<input type="text" value="${refereecompany || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
                                    </td>
                                   
                                  </tr>
								 
                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						  <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>

                                  <tr>
                                    <td width="100%">
                                       <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:20%;float:left;font-size: 12px; ">In what capacity did you know the applicant?:</label>
                                        
										<textarea type="text" value="" style="width:80%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${refereecapacity || ""}</textarea>
										</p>
										
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						  
                         <tr>
                            <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                              Applicant Details </td>
                          </tr>
						  
						   <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>

                                  <tr>
                                    <td width="50%" style="vertical-align:top;">
                                       <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">First name</label>
                                        
										<input type="text" value="${applicantfirstname || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										 <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Email </label>
                                        
										<input type="text" value="${applicantemail || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Job Title </label>
                                        
										<input type="text" value="${applicantjobtitle || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Date Employed from </label>
                                        
										<input type="text" value="${moment(new Date(employedfrom)).format("DD-MM-YYYY") || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Would you re-employ this person?</label>
                                        
										<span style=" width:50%; font-size: 12px; color:#7b7c80;">
										<label> <input type="radio" name="remployee" ${remployee === "yes" ? "checked" : ""}>  Yes</label>
										<label> <input type="radio" name="remployee" ${remployee === "no" ? "checked" : ""}>  No</label>
										</span>
										</p>
                                    </td>
                                    <td width="50%" style="vertical-align:top;">
                                       <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; ">Last name</label>
                                        
										<input type="text" value="${applicantlastname || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; "> Phone </label>
                                        
										<input type="text" value="${(applicantphone.code || "") + "-" + (applicantphone.number || "-")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; "> Company</label>
										<input type="text" value="${applicantcompany || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:50%;float:left;font-size: 12px; "> Date Employed to</label>
										<input type="text" value="${moment(new Date(employedto)).format("DD-MM-YYYY") || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
										</p>
                                    </td>
									
                                   
                                  </tr>

                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						  <tr>
                            <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                             SECTION 1 - HOW WOULD YOU ASSESS THE FOLLOWING?</td>
                          </tr>
						   <tr>
                            <td style="width: 100%; font-size: 13px; color: #585858; padding: 15px 30px;">
                             Please tick the relevant boxes Excellent, Good, Average, Poor</td>
                          </tr>
						  
						  <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>

                                  <tr>
                                    <td width="50%" style="vertical-align:top;">
                                       
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Ability to follow care plans</label>
                                        
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="followcareplans" ${followcareplans === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="followcareplans" ${followcareplans === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="followcareplans" ${followcareplans === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="followcareplans" ${followcareplans === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Character</label>
                                        
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="character" ${character === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="character" ${character === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="character" ${character === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="character" ${character === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Ability to ensure dignity is upheld</label>
                                        
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="dignity" ${dignity === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="dignity" ${dignity === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="dignity" ${dignity === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="dignity" ${dignity === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Relationships with colleagues</label>
                                        
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="relationships" ${relationships === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="relationships" ${relationships === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="relationships" ${relationships === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="relationships" ${relationships === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
                                    </td>
                                    <td width="50%" style="vertical-align:top;">
                                      <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Reliability, timekeeping,attendance</label>
                                        
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="reliability" ${reliability === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="reliability" ${reliability === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="reliability" ${reliability === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="reliability" ${reliability === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Attitude</label>
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="attitude" ${attitude === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="attitude" ${attitude === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="attitude" ${attitude === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="attitude" ${attitude === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Communication</label>
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="communication" ${communication === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="communication" ${communication === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="communication" ${communication === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="communication" ${communication === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Ability to work under own initiative</label>
										<span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "excellent" ? "checked" : ""}>  Excellent </label>
										<label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "good" ? "checked" : ""}>  Good</label>										
										<label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "average" ? "checked" : ""}>  Average</label>
										<label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "poor" ? "checked" : ""}>  Poor</label>
										</span>
										</p>
                                    </td>
									
                                  </tr>

                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						  <tr>
                            <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                             SECTION - 2 Please answer the following questions</td>
                          </tr>
						  
						  
						  <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>

                                  <tr>
                                    <td width="50%" style="vertical-align:top;">
                                       
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:100%;float:left;font-size: 12px; ">Has the applicant been subject to any disciplinary action?</label>
                                        
										<span style="width:100%;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="disciplinaryaction" ${disciplinaryaction === "yes" ? "checked" : ""}>  Yes</label>
										<label> <input type="radio" name="disciplinaryaction" ${disciplinaryaction === "no" ? "checked" : ""}>  No</label>
										
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                        
										<textarea type="text" value="${disciplinaryactiondetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${disciplinaryactiondetails || "-"}</textarea>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:100%;float:left;font-size: 12px; ">Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?</label>
                                        
										<span style="width:100%;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="vulnerablepeople" ${vulnerablepeople === "yes" ? "checked" : ""}>  Yes</label>
										<label> <input type="radio" name="vulnerablepeople" ${vulnerablepeople === "no" ? "checked" : ""}>  No</label>
										
										</span>
										</p>
										
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                        
										<textarea type="text" value="${vulnerablepeopledetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${vulnerablepeopledetails || "-"}</textarea>
										</p>
										
                                    </td>
                                    <td width="50%" style="vertical-align:top;">
                                      <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:100%;float:left;font-size: 12px; ">Are you aware of the applicants involvement in any safeguarding investigations previous or current .</label>
                                        
										<span style="width:100%;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="investigations" ${investigations === "yes" ? "checked" : ""}>  Yes</label>
										<label> <input type="radio" name="investigations" ${investigations === "no" ? "checked" : ""}>  No</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                        
										<textarea type="text" value="${investigationsdetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${investigationsdetails || "-"}</textarea>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:100%;float:left;font-size: 12px; ">To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?</label>
                                        
										<span style="width:100%;font-size: 12px; color:#7b7c80">
										<label> <input type="radio" name="criminaloffences" ${form_reference1.criminaloffence || form_reference2.criminaloffence === "yes" ? "checked" : ""}>  Yes</label>
										<label> <input type="radio" name="criminaloffences" ${form_reference1.criminaloffence || form_reference2.criminaloffence === "no" ? "checked" : ""}>  No</label>
										</span>
										</p>
										<p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                        
										<textarea type="text" value="${criminaloffencedetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${criminaloffencedetails || "-"}</textarea>
										</p>
										
                                    </td>
									
                                  </tr>

                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						    <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>

                                  <tr>
                                    <td width="100%">
                                       <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:20%;float:left;font-size: 12px; ">Additional Comments</label>
                                        
										<textarea type="text" value="${additionalcomments || "-"}" style="width:80%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${additionalcomments || "-"}</textarea>
										</p>
										
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						   <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                <tbody>

                                  <tr>
                                    <td width="100%">
                                       <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                        style="width:20%;float:left;font-size: 12px; ">Official company stamp</label>
                                        <span style="width:80%;float:left;">
										<img src="${`${CONFIG.LIVEURL}/${campanystamp}`}"/>
										<br>
										</p>
										
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						  
						  
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
              </table>`;
              console.log("refereephone",refereephone)
              console.log("FORMREFERENCE",form_reference1.criminaloffence)
              
            pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
          } else {
            data.response = "Unable to download";
            res.send(data);
          }
      });

  };
  return router;
};
