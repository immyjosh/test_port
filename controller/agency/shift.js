module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library");
  const attachment = require("../../model/attachments");
  const dashboard = require("../../model/dashboard");
  const mongoose = require("mongoose");
  const push = require("../../model/pushNotification.js")(io);
  const nodemailer = require("nodemailer");
  const mailcontent = require("../../model/mailcontent");
  const twilio = require("../../model/twilio.js");
  const moment = require("moment");
  const json2csv = require("json2csv").Parser;
  const fs = require("fs");
  const async = require("async");
  const Google = require("../../model/googleapis");
  const event = require("../../controller/events/events");

  const router = {};
  const data = {};
  data.status = 0;

  /* router.save = (req, res) => {
        var data = {};
        data.status = 0;

        //req.checkBody('title', "Title Required").notEmpty();
        req.checkBody('client', 'Client Required').notEmpty();
        req.checkBody('location', 'Location Required').notEmpty();
        req.checkBody('job_type', 'Job type Required').notEmpty();
        req.checkBody('shift_type', 'Job date Required').notEmpty();
        req.checkBody('starttime', 'Start time Required').notEmpty();
        req.checkBody('endtime', 'End time Required').notEmpty();
        req.checkBody('breaktime', 'Break time Required').notEmpty();
        req.checkBody('start_date', 'Start date Required').notEmpty();
        req.checkBody('status', 'Status Required').notEmpty();
        if (req.body.shift_type == "multiple") {
            req.checkBody('end_date', 'Job date Required').notEmpty();
            req.checkBody('shift_option', 'Shift options Required').notEmpty();
            req.checkBody('option_data', 'Shift options data Required').notEmpty();
        }

        console.log("req.body",req.body);

        var errors = req.validationErrors();
        if (errors) {
            data.response = errors[0].msg;
            return res.send(data);
        }

        var shift = {
            //title: req.body.title,
            location: req.body.location,
            job_type: req.body.job_type,
            starttime: req.body.starttime,
            endtime: req.body.endtime,
            breaktime: req.body.breaktime,
            start_date: req.body.start_date,
            end_date: req.body.end_date,
            shift_type: req.body.shift_type,
            shift_option: req.body.shift_option,
            option_data: req.body.option_data,
            status: req.body.status,
            client: req.body.client,
            agency: req.params.loginId,
        };

        console.log("req.body.start_date",req.body.start_date);
        console.log("req.body.end_date",req.body.end_date);

        if (req.body.shift_type == 'single') {
            shift.end_date = req.body.start_date;
        }

        shift.addedBy = "agency";
        shift.addedId = req.params.loginId;
        shift.shiftId = 'sh-' + Math.floor(100000 + Math.random() * 900000);
        shift.title = 'sh-' + Math.floor(100000 + Math.random() * 900000);

        if (req.body.id) {
            db.UpdateDocument('shifts', { _id: req.body.id }, shift, {}, (err, result) => {
                if (err) {
                    data.response = "Unable to Save Your Data Please try again";
                    res.send(data);
                } else {
                    data.status = 1;
                    data.response = "Shift updated successfully.";
                    data.shiftId = req.body.id;
                    data.shift_status = 1;
                    res.send(data);
                }

            });
        } else {

            var shiftArray = [];
            shiftArray.push(shift);
            db.InsertMultiple('shifts', shiftArray, (err, result) => {
                console.log("err, result",err, result);
                if (err || !result) {
                    data.response = 'Unable to Save Your Data Please try again';
                    res.send(data);
                } else {
                    data.status = 1;
                    data.result = result;
                    data.response = "Shift added successfully.";
                    data.shiftId = result[0]._id;
                    data.shift_status = result[0].status;
                    res.send(data);

                }
            });

        }
    };*/

  router.save = (req, res) => {
    const data = {};
    data.status = 0;
    // req.checkBody('title', "Title Required").notEmpty();
    req.checkBody("client", "Client Required").notEmpty();
    req.checkBody("branch", "Branch Required").notEmpty();
    req.checkBody("locations", "Location Required").notEmpty();
    req.checkBody("job_type", "Job type Required").notEmpty();
    req.checkBody("shift_type", "Job date Required").notEmpty();
    req.checkBody("starttime", "Start time Required").notEmpty();
    req.checkBody("endtime", "End time Required").notEmpty();
    req.checkBody("breaktime", "Break time Required").notEmpty();
    req.checkBody("start_date", "Start date Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();
    if (req.body.shift_type == "multiple") {
      req.checkBody("end_date", "Job date Required").notEmpty();
      req.checkBody("shift_option", "Shift options Required").notEmpty();
      req.checkBody("option_data", "Shift options data Required").notEmpty();
    }

    const shiftArray = [];
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    async.waterfall(
      [
        callback => {
          db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.params.loginId) }, {}, {}, (err, AgencyData) => {
            if (err || !AgencyData) {
              data.response = "Job details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, AgencyData);
            }
          });
        },
        (AgencyData, callback) => {
          db.GetCount("shifts", { agency: new mongoose.Types.ObjectId(req.params.loginId) }, (err, AgenciesCount) => {
            AgenciesCount = AgenciesCount == null || AgenciesCount == undefined ? 0 : AgenciesCount;
            callback(err, AgencyData, AgenciesCount);
          });
        }
      ],
      (err, AgencyData, AgenciesCount) => {
        if (AgencyData.settings && AgencyData.settings.shift && AgencyData.settings.shift.prefix && AgencyData.settings.shift.number) {
          const shift = {
            // title: req.body.title,
            locations: req.body.locations,
            branch: req.body.branch,
            job_type: req.body.job_type,
            starttime: req.body.starttime,
            endtime: req.body.endtime,
            breaktime: req.body.breaktime,
            start_date: req.body.start_date,
            end_date: req.body.end_date,
            shift_type: req.body.shift_type,
            shift_option: req.body.shift_option,
            option_data: req.body.option_data,
            status: req.body.status,
            client: req.body.client,
            notes: req.body.notes,
            latefee: req.body.latefee
          };

          if (!req.body.id) {
            const UniNumber = AgencyData.settings.shift.number + (AgenciesCount > 0 ? AgenciesCount + 1 : 0);
            shift.shiftId = `${AgencyData.settings.shift.prefix}-${UniNumber}`;
            // shift.shiftId = "sh-" + Math.floor(100000 + Math.random() * 900000);
            shift.title = shift.shiftId;
            shift.agency = req.params.loginId;
            shift.addedBy = "agency";
            shift.addedId = req.params.loginId;
          }

          if (req.body.shift_type == "single") {
            shift.end_date = req.body.start_date;
            shiftArray.push(shift);
          }

          if (req.body.shift_type == "multiple") {
            const shift_option = req.body.shift_option;
            const option_data = req.body.option_data;
            const dayList = [];
            const start_date = req.body.start_date;
            const end_date = req.body.end_date;

            if (shift_option == "daily") {
              for (var d = new Date(start_date); d <= new Date(end_date); d.setDate(d.getDate() + 1)) {
                dayList.push(new Date(d));
              }
            }

            if (shift_option == "weekday") {
              for (var d = new Date(start_date); d <= new Date(end_date); d.setDate(d.getDate() + 1)) {
                if (d.getDay() != 0 && d.getDay() != 6) {
                  // no sat and sun
                  dayList.push(new Date(d));
                }
              }
            }

            if (shift_option == "weekly") {
              for (var d = new Date(start_date); d <= new Date(end_date); d.setDate(d.getDate() + 1)) {
                const daynum = d.getDay();
                if (option_data.weekly[daynum] == true) {
                  dayList.push(new Date(d));
                }
              }
            }

            if (shift_option == "rotational") {
              const shiftdays = parseInt(option_data.rotational.x);
              const freedays = parseInt(option_data.rotational.y);
              for (var d = new Date(start_date); d <= new Date(end_date); d.setDate(d.getDate() + freedays)) {
                const shiftstart = new Date(d);
                const shiftend = new Date(d);
                shiftend.setDate(shiftstart.getDate() + shiftdays);
                for (let da = shiftstart; da < shiftend; da.setDate(da.getDate() + 1)) {
                  dayList.push(new Date(da));
                }
              }
            }

            if (shift_option == "monthly") {
              const monthly = option_data.monthly;
              for (var d = new Date(start_date); d <= new Date(end_date); d.setDate(d.getDate() + 1)) {
                if (monthly.indexOf(d.getDate().toString()) != -1) {
                  dayList.push(new Date(d));
                }
              }
            }
            let UniNumber = AgencyData.settings.shift.number + (AgenciesCount > 0 ? AgenciesCount + 1 : 0);
            dayList.map((a, i) => {
              shift.start_date = a;
              shift.end_date = a;
              shift.shift_type = "single";
              if (i > 0) {
                UniNumber = UniNumber + 1;
              }
              shift.shiftId = `${AgencyData.settings.shift.prefix}-${UniNumber}`;
              // shift.shiftId = "sh-" + Math.floor(100000 + Math.random() * 900000);
              shift.title = shift.shiftId;
              const upshift = Object.assign({}, shift);
              shiftArray.push(upshift);
            });
          }
          // console.log("shiftArray", shiftArray);
          // console.log("shift", shift);

          if (shiftArray.length == 0) {
            data.response = "No Date available between the selected dates for the selected Criteria.";
            res.send(data);
            return;
          }

          if (req.body.id) {
            db.UpdateDocument("shifts", { _id: req.body.id }, shift, {}, (err, result) => {
              if (err) {
                data.err = err;
                data.response = "Unable to Save Your Data Please try again";
                res.send(data);
              } else {
                data.status = 1;
                data.response = "Shift updated successfully.";
                data.shiftId = req.body.id;
                data.shift_status = 1;
                res.send(data);
              }
            });
          } else {
            db.InsertMultiple("shifts", shiftArray, (err, result) => {
              if (err || !result) {
                data.response = "Unable to Save Your Data Please try again";
                res.send(data);
              } else {
                data.status = 1;
                data.result = result;
                data.response = "Shift added successfully!";
                data.shiftId = result[0]._id;
                data.shift_status = result[0].status;
                res.send(data);
                // billing cycle dates
                event.emit("timesheet_dates_list", { agency: req.params.loginId});
                event.emit("invoice_dates_list", { agency: req.params.loginId});
                // // for shifts graph stats
                // shiftArray &&
                //   shiftArray.map(list => {
                //     event.emit("shiftinvoicestat", {
                //       agency: list.agency,
                //       client: list.client,
                //       employee: list.employee,
                //       shiftID: list.shiftID,
                //       timesheetID: list.timesheetID,
                //       invoiceID: list.invoiceID,
                //       locations: list.locations,
                //       branch: list.branch,
                //       job_type: list.job_type,
                //       status: 1,
                //       type: "shift"
                //     });
                //   });
              }
            });
          }
        } else {
          data.status = 0;
          data.response = "Please Set Shift Prefix & Number In Settings";
          res.send(data);
        }
      }
    );
  };

  router.list = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const locationQuery = [];
    locationQuery.push({ $match: { agency: { $eq: req.params.loginId } } });

    if (req.body.status) {
      locationQuery.push({ $match: { status: req.body.status } });
    } else {
      locationQuery.push({ $match: { status: { $gt: 0 } } });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      locationQuery.push({ $sort: sorting });
    } else {
      locationQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      locationQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      locationQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, branches: 1 } }
          ],
          as: "client_datas"
        }
      },
      { $addFields: { client: "$client_datas.companyname", client_data: "$client_datas.companyname" } },
      { $unwind: { path: "$client_datas", preserveNullAndEmptyArrays: true } },
      {
        $addFields: {
          branch_data: {
            $filter: {
              input: "$client_datas.branches",
              as: "bran",
              cond: {
                $eq: ["$$bran._id", "$branch"]
              }
            }
          }
        }
      }
    );

    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        locationQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        locationQuery.push({ $match: searching });
      }
    }
    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      locationQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      locationQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          groupcount: [
            {
              $match: {
                $or: [
                  { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } }
                  // {'employee_requested.employee':{ $eq: new mongoose.Types.ObjectId(req.params.loginId) } , 'status' : { $eq : 2} },
                  // {'employee_requested.employee':{ $eq: new mongoose.Types.ObjectId(req.params.loginId) } , 'status' : { $eq : 3} },
                ]
              }
            },
            { $group: { _id: "$status", count: { $sum: 1 } } },
            {
              $addFields: {
                status: {
                  $arrayElemAt: [["added", "added", "requests", "accepted", "assigned", "ongoing", "completed", "timehseet_approved", "invoice_approved", "Payment_completed", "expired"], "$_id"]
                }
              }
            },
            { $addFields: { nott: [["$status", "$count"]] } },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ],
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          groupcount: docData[0].groupcount[0]
        };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid Id").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("shifts", { _id: req.body.id, agency: req.params.loginId }, {}, {}, (err, shiftData) => {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: shiftData };
        res.send(data);
      }
    });
  };

  router.view = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Shift Id required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const shiftQuery = [];
    shiftQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    shiftQuery.push({ $match: { agency: { $eq: req.params.loginId } } });

    shiftQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { employeeid: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$employeeid"]
                }
              }
            },
            { $project: { name: 1, locations: 1 } }
          ],
          as: "employee_data"
        }
      },
      { $addFields: { employee: "$employee_data.name", employee_locations: "$employee_data.locations" } }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, email: 1, avatar: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    shiftQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch_data"
      }
    });
    shiftQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "locations"
        }
      },
      { $addFields: { locations: "$locations.name", location_data: "$locations" } }
    );
    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      shiftQuery.push({ $sort: sorting });
    }
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        shiftQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift_id: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        shiftQuery.push({ $match: searching });
      }
    }
    db.GetAggregation("shifts", shiftQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };

  router.shift_request = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("shiftId", "Shift Id required").notEmpty();
    req.checkBody("employees", "Select Employees").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, (err, shiftData) => {
      if (err || !shiftData) {
        data.response = err;
        return res.send(data);
      } else {
        if (shiftData.status === 1) {
          const userQuery = [];
          const employees = req.body.employees.map(function(id) {
            return new mongoose.Types.ObjectId(id);
          });
          userQuery.push({ $match: { _id: { $in: employees } } });

          db.GetAggregation("employees", userQuery, (err, employeeData) => {
            if (err || !employeeData) {
              data.response = "Unable to get your data, Please try again";
              res.send(data);
            } else {
              if (employeeData.length <= 0) {
                data.response = "Employees not found";
                res.send(data);
              } else {
                const employee_requested = [];
                employeeData.map(function(emp) {
                  const empobj = {
                    employee: emp._id,
                    status: 0
                    /* 'email': emp.email,
                    'name': emp.name,
                    'avatar': emp.avatar,
                    'employee_rate': emp.employee_rate,
                    'agency_rate': emp.agency_rate,
                    'client_rate': emp.client_rate,
                    'lat': emp.address.lat,
                    'lng': emp.address.lon,
                    'locations': emp.locations,
                    'job_type': emp.job_type,
                    'phone': emp.phone,*/
                  };
                  employee_requested.push(empobj);
                  const message = "New Job request";
                  const options = { shift: shiftData._id, emp_id: emp._id };
                  push.addnotification(emp._id, message, "job_request", options, "EMPLOYEE", (err, response, body) => {});
                  if (emp.settings && emp.settings.notifications.email) {
                    // Mail Sent To All
                    const mailData = {};
                    mailData.template = "newshiftrequesttoemployee";
                    mailData.from = req.params.loginData.company_email;
                    mailData.to = emp.email;
                    mailData.html = [];
                    mailData.html.push({ name: "username", value: emp.username });
                    mailData.html.push({ name: "role", value: emp.job_type });
                    mailcontent.sendmail(mailData, (err, response) => {
                      console.log("mail err,mail response", err, response);
                    });
                  }
                  if (emp.settings && emp.settings.notifications.sms) {
                    // SMS Send To All
                    const smsto = emp.phone.code + emp.phone.number;
                    const smsmessage = `Hi ${emp.username} , You Got New Shift For Role ${emp.job_type}`;
                    twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                      console.log("SMS err,SMS response", err, response);
                    });
                  }
                });

                db.UpdateDocument("shifts", { _id: req.body.shiftId }, { employee_requested: employee_requested, status: 2, requested_time: new Date() }, {}, function(err, docdata) {
                  data.status = 1;
                  data.response = { result: "Job Request sent to employees." };
                  res.send(data);
                });
              }
            }
          });
        } else {
          data.response = "Request sent already";
          res.send(data);
        }
      }
    });
  };
  router.open_request_to_all = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("shiftId", "Shift Id required").notEmpty();
    // req.checkBody("employees", "Select Employees").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, (err, shiftData) => {
      if (err || !shiftData) {
        data.response = err;
        return res.send(data);
      } else {
        if (shiftData.status === 1) {
          const userQuery = [];
          userQuery.push({ $match: { job_type: { $eq: shiftData.job_type } } });
          userQuery.push({ $match: { locations: { $eq: shiftData.locations } } });
          userQuery.push({ $match: { "onlineform.form_status": { $gte: 9 } } });
          userQuery.push({ $match: { "onlineform.final_verify_status": { $gte: 1 } } });

          db.GetAggregation("employees", userQuery, (err, employeeData) => {
            if (err || !employeeData) {
              data.response = "Unable to get your data, Please try again";
              res.send(data);
            } else {
              if (employeeData.length <= 0) {
                data.response = "Employees not found";
                res.send(data);
              } else {
                const employee_requested = [];
                employeeData.map(function(emp) {
                  const empobj = {
                    employee: emp._id,
                    status: 0
                    /* 'email': emp.email,
                    'name': emp.name,
                    'avatar': emp.avatar,
                    'employee_rate': emp.employee_rate,
                    'agency_rate': emp.agency_rate,
                    'client_rate': emp.client_rate,
                    'lat': emp.address.lat,
                    'lng': emp.address.lon,
                    'locations': emp.locations,
                    'job_type': emp.job_type,
                    'phone': emp.phone,*/
                  };
                  employee_requested.push(empobj);
                  const message = "New Job request";
                  const options = { shift: shiftData._id, emp_id: emp._id };
                  push.addnotification(emp._id, message, "job_request", options, "EMPLOYEE", (err, response, body) => {});
                  if (emp.settings && emp.settings.notifications.email) {
                    // Mail Sent To All
                    const mailData = {};
                    mailData.template = "newshiftrequesttoemployee";
                    mailData.from = req.params.loginData.company_email;
                    mailData.to = emp.email;
                    mailData.html = [];
                    mailData.html.push({ name: "username", value: emp.username });
                    mailData.html.push({ name: "role", value: emp.job_type });
                    mailcontent.sendmail(mailData, (err, response) => {
                      console.log("mail err,mail response", err, response);
                    });
                  }
                  if (emp.settings && emp.settings.notifications.sms) {
                    // SMS Send To All
                    const smsto = emp.phone.code + emp.phone.number;
                    const smsmessage = `Hi ${emp.username} , You Got New Shift`;
                    twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                      console.log("SMS err,SMS response", err, response);
                    });
                  }
                });

                db.UpdateDocument("shifts", { _id: req.body.shiftId }, { employee_requested: employee_requested, status: 2, requested_time: new Date() }, {}, function(err, docdata) {
                  data.status = 1;
                  data.response = { result: "Request sent to employees." };
                  res.send(data);
                });
              }
            }
          });
        } else {
          data.response = "Request sent already";
          res.send(data);
        }
      }
    });
  };
  router.get_employees = (req, res, next) => {
    let data = {};
    data.status = 0;
    req.checkBody("shiftId", "Shift Id required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, (err, shiftData) => {
      if (err || !shiftData) {
        data.response = err;
        return res.send(data);
      } else {
        const userQuery = [];
        let daystart, dayend;
        if (shiftData.status === 1) {
          userQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
          userQuery.push({ $match: { job_type: { $eq: shiftData.job_type } } });
          userQuery.push({ $match: { locations: { $eq: shiftData.locations } } });
          userQuery.push({ $match: { "onlineform.form_status": { $gte: 9 } } });
          userQuery.push({ $match: { "onlineform.final_verify_status": { $gte: 1 } } });
          const startdate = new Date(shiftData.start_date);
          const formatdate = moment(shiftData.start_date).format("YYYY-MM-DD");
          daystart = formatdate + " 00:00:00";
          dayend = formatdate + " 23:59:59";

          const obj = {};
          obj["available." + startdate.getDay() + ".status"] = { $eq: 1 };
          obj["available." + startdate.getDay() + ".from"] = { $lte: shiftData.starttime };
          obj["available." + startdate.getDay() + ".to"] = { $gte: shiftData.endtime };
          userQuery.push({ $match: obj });

          const unavailability = Object.assign([], userQuery);
          unavailability.push({
            $match: {
              $and: [
                { "timeoff.date": { $gte: new Date(daystart) } },
                { "timeoff.date": { $lte: new Date(dayend) } },
                {
                  $or: [
                    {
                      $and: [{ "timeoff.from": { $lte: shiftData.starttime } }, { "timeoff.to": { $gte: shiftData.starttime } }]
                    },
                    {
                      $and: [{ "timeoff.from": { $lte: shiftData.endtime } }, { "timeoff.to": { $gte: shiftData.endtime } }]
                    }
                  ]
                }
              ]
            }
          });

          userQuery.push(
            {
              $lookup: {
                from: "jobtypes",
                let: { job_type: "$job_type" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $in: ["$_id", "$$job_type"]
                      }
                    }
                  },
                  { $project: { name: 1 } }
                ],
                as: "jobtype_data"
              }
            },
            { $addFields: { job_type: "$jobtype_data.name" } }
          );

          userQuery.push(
            {
              $lookup: {
                from: "locations",
                let: { location_type: "$locations" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $in: ["$_id", "$$location_type"]
                      }
                    }
                  },
                  { $project: { name: 1 } }
                ],
                as: "locations"
              }
            },
            { $addFields: { locations: "$locations.name" } }
          );

          userQuery.push(
            {
              $lookup: {
                from: "shifts",
                let: { emp: "$_id" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $eq: ["$employee", "$$emp"]
                      }
                    }
                  },
                  { $project: { agency_rating: 1 } }
                ],
                as: "employee_rating_data"
              }
            },
            { $addFields: { employee_ratings: { $avg: "$employee_rating_data.agency_rating" } } },
            { $addFields: { employee_ratings: { $ifNull: ["$employee_ratings", 0] } } }
          );
          userQuery.push(
            {
              $lookup: {
                from: "clients",
                let: { client: shiftData.client },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $eq: ["$_id", "$$client"]
                      }
                    }
                  },
                  { $project: { companyname: 1, branches: 1 } }
                ],
                as: "client_datas"
              }
            },
            { $addFields: { client: "$client_datas.companyname", client_data: "$client_datas.companyname" } },
            { $unwind: { path: "$client_datas", preserveNullAndEmptyArrays: true } },
            {
              $addFields: {
                branch_data: {
                  $filter: {
                    input: "$client_datas.branches",
                    as: "bran",
                    cond: {
                      $eq: ["$$bran._id", shiftData.branch]
                    }
                  }
                }
              }
            }
          );

          if (req.body.field && req.body.order) {
            const sorting = {};
            sorting[req.body.field] = parseInt(req.body.order);
            userQuery.push({ $sort: sorting });
          }
          const withoutlimit = Object.assign([], userQuery);
          withoutlimit.push({ $count: "count" });
          if (req.body.skip >= 0) {
            userQuery.push({ $skip: parseInt(req.body.skip) });
          }

          if (req.body.limit >= 0) {
            userQuery.push({ $limit: parseInt(req.body.limit) });
          }
          if (req.body.search) {
            const searchs = req.body.search;

            if (req.body.filter === "all") {
              userQuery.push({
                $match: {
                  $or: [
                    { title: { $regex: searchs + ".*", $options: "si" } },
                    { shift_id: { $regex: searchs + ".*", $options: "si" } },
                    { client: { $regex: searchs + ".*", $options: "si" } },
                    { employee: { $regex: searchs + ".*", $options: "si" } },
                    { locations: { $regex: searchs + ".*", $options: "si" } },
                    { branch: { $regex: searchs + ".*", $options: "si" } },
                    { name: { $regex: searchs + ".*", $options: "si" } },
                    { email: { $regex: searchs + ".*", $options: "si" } },
                    { employee_ratings: { $regex: searchs + ".*", $options: "si" } },
                    { job_type: { $regex: searchs + ".*", $options: "si" } },
                    { distance: { $regex: searchs + ".*", $options: "si" } }
                  ]
                }
              });
            } else {
              const searching = {};
              searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
              userQuery.push({ $match: searching });
            }
          }
          const finalQuery = [
            {
              $facet: {
                overall: withoutlimit,
                employeeData: userQuery,
                unavailability: unavailability
              }
            }
          ];

          db.GetAggregation("employees", finalQuery, (err, result) => {
            if (err || !result) {
              data.err = err;
              data.response = "Unable to get employee data, Please try again";
              data.userQuery = finalQuery;
              return res.send(data);
            } else {
              let fullcount;
              if (result[0].overall[0] && result[0].overall[0].count) {
                fullcount = result[0].overall[0].count;
              } else {
                fullcount = result && result[0].result ? result[0].result.length : 0;
              }
              const employeeData = result[0].employeeData;
              const unavailability = result[0].unavailability;

              if (unavailability && unavailability.length > 0 || employeeData && employeeData.length <= 0) {
                // if (employeeData.length <= 0) {
                data.response = "Employees not found";
                return res.send(data);
              } else {
                async.mapSeries(
                  employeeData,
                  (emp, callback) => {
                    let distance = "Currently Not Available";
                    data = {
                      lat1: emp.address.lat,
                      lon1: emp.address.lon,
                      lat2: emp.branch_data[0].branchlat,
                      lon2: emp.branch_data[0].branchlng
                    };
                    Google.distancematrix(data, LResult => {
                      if (LResult && LResult.rows.length > 0 && LResult.rows[0].elements.length > 0) {
                        distance = LResult.rows[0].elements[0].distance && LResult.rows[0].elements[0].distance.text;
                      }
                      const empobj = {
                        employee: emp._id,
                        email: emp.email,
                        name: emp.name,
                        username: emp.username,
                        address: emp.address,
                        holiday_allowance: emp.holiday_allowance,
                        joining_date: emp.joining_date,
                        final_date: emp.final_date,
                        avatar: emp.avatar,
                        employee_rate: emp.employee_rate,
                        agency_rate: emp.agency_rate,
                        client_rate: emp.client_rate,
                        lat: emp.address.lat,
                        lng: emp.address.lon,
                        locations: emp.locations,
                        job_type: emp.job_type,
                        timeoff: emp.timeoff,
                        phone: emp.phone,
                        employee_ratings: emp.employee_ratings,
                        status: emp.status,
                        distance: distance
                      };
                      if (err) {
                        callback(err);
                      } else {
                        callback(null, empobj);
                      }
                    });
                  },
                  (err, results) => {
                    if (err) {
                      res.send(err);
                    } else {
                      data.status = 1;
                      data.response = {};
                      data.response.length = result[0].length;
                      data.response.fullcount = fullcount;
                      if (req.body.field === "distance") {
                        if (req.body.order === 1) {
                          data.response.result = results.sort((a, b) => b.distance - a.distance);
                        }
                        if (req.body.order === -1) {
                          data.response.result = results.sort((a, b) => a.distance - b.distance);
                        }
                      } else if (req.body.order && req.body.field) {
                        data.response.result = results;
                      } else {
                        data.response.result = results.sort((a, b) => b.distance - a.distance);
                      }
                      // data.response = {length: result[0].length, fullcount: fullcount, result: results};
                      return res.send(data);
                    }
                  }
                );
              }
            }
          });
        } else {
          const emprquested = shiftData.employee_requested;
          const emprquested11 = Object.assign([], shiftData.employee_requested);
          let employeesId = emprquested.map(function(emp) {
            return emp.employee;
          });
          if (shiftData.status >= 4) {
            employeesId = [shiftData.employee];
          }

          userQuery.push({ $match: { _id: { $in: employeesId } } });
          userQuery.push(
            {
              $lookup: {
                from: "shifts",
                let: { shiftid: new mongoose.Types.ObjectId(shiftData._id) },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $eq: ["$_id", "$$shiftid"]
                      }
                    }
                  },
                  { $project: { employee_requested: 1 } }
                ],
                as: "shift_data"
              }
            },
            { $unwind: { path: "$shift_data", preserveNullAndEmptyArrays: true } },
            {
              $addFields: {
                status_data: {
                  $filter: {
                    input: "$shift_data.employee_requested",
                    as: "shtda",
                    cond: {
                      $eq: ["$$shtda.employee", "$_id"]
                    }
                  }
                }
              }
            },
            { $unwind: { path: "$status_data", preserveNullAndEmptyArrays: true } },
            { $addFields: { status: "$status_data.status" } }
          );
          userQuery.push(
            {
              $lookup: {
                from: "jobtypes",
                let: { job_type: "$job_type" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $in: ["$_id", "$$job_type"]
                      }
                    }
                  },
                  { $project: { name: 1 } }
                ],
                as: "jobtype_data"
              }
            },
            { $addFields: { job_type: "$jobtype_data.name" } }
          );

          userQuery.push(
            {
              $lookup: {
                from: "locations",
                let: { location_type: "$locations" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $in: ["$_id", "$$location_type"]
                      }
                    }
                  },
                  { $project: { name: 1 } }
                ],
                as: "locations"
              }
            },
            { $addFields: { locations: "$locations.name" } }
          );
          userQuery.push(
            {
              $lookup: {
                from: "shifts",
                let: { emp: "$_id" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $eq: ["$employee", "$$emp"]
                      }
                    }
                  },
                  { $project: { agency_rating: 1 } }
                ],
                as: "employee_rating_data"
              }
            },
            { $addFields: { employee_ratings: { $avg: "$employee_rating_data.agency_rating" } } },
            { $addFields: { employee_ratings: { $ifNull: ["$employee_ratings", 0] } } }
          );
          userQuery.push(
            {
              $lookup: {
                from: "clients",
                let: { client: shiftData.client },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $eq: ["$_id", "$$client"]
                      }
                    }
                  },
                  { $project: { companyname: 1, branches: 1 } }
                ],
                as: "client_datas"
              }
            },
            { $addFields: { client: "$client_datas.companyname", client_data: "$client_datas.companyname" } },
            { $unwind: { path: "$client_datas", preserveNullAndEmptyArrays: true } },
            {
              $addFields: {
                branch_data: {
                  $filter: {
                    input: "$client_datas.branches",
                    as: "bran",
                    cond: {
                      $eq: ["$$bran._id", shiftData.branch]
                    }
                  }
                }
              }
            }
          );

          const withoutlimit = Object.assign([], userQuery);
          withoutlimit.push({ $count: "count" });
          if (req.body.skip >= 0) {
            userQuery.push({ $skip: parseInt(req.body.skip) });
          }

          if (req.body.limit >= 0) {
            userQuery.push({ $limit: parseInt(req.body.limit) });
          }
          if (req.body.search) {
            const searchs = req.body.search;

            if (req.body.filter === "all") {
              userQuery.push({
                $match: {
                  $or: [
                    { title: { $regex: searchs + ".*", $options: "si" } },
                    { shift_id: { $regex: searchs + ".*", $options: "si" } },
                    { client: { $regex: searchs + ".*", $options: "si" } },
                    { employee: { $regex: searchs + ".*", $options: "si" } },
                    { locations: { $regex: searchs + ".*", $options: "si" } },
                    { branch: { $regex: searchs + ".*", $options: "si" } },
                    { name: { $regex: searchs + ".*", $options: "si" } },
                    { job_type: { $regex: searchs + ".*", $options: "si" } },
                    { distance: { $regex: searchs + ".*", $options: "si" } }
                  ]
                }
              });
            } else {
              const searching = {};
              searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
              userQuery.push({ $match: searching });
            }
          }
          if (req.body.field && req.body.order) {
            const sorting = {};
            sorting[req.body.field] = parseInt(req.body.order);
            userQuery.push({ $sort: sorting });
          }
          db.GetAggregation("employees", userQuery, (err, employeeData) => {
            if (err || !employeeData) {
              data.err = err;
              data.response = "Unable to get employee data, Please try again";
              // return res.send(data);
            } else {
              if (employeeData.length <= 0) {
                data.response = "Employees not found";
                // return res.send(data);
              } else {
                // let fullcount;
                // if (employeeData[0].overall[0] && employeeData[0].overall[0].count) {
                //   fullcount = employeeData[0].overall[0].count;
                // } else {
                //   fullcount = employeeData[0].result.length;
                // }
                async.mapSeries(
                  employeeData,
                  (emp, callback) => {
                    let distance = "Currently Not Available";
                    data = {
                      lat1: emp.address.lat,
                      lon1: emp.address.lon,
                      lat2: emp.branch_data[0].branchlat,
                      lon2: emp.branch_data[0].branchlng
                    };
                    Google.distancematrix(data, LResult => {
                      if (LResult && LResult.rows.length > 0 && LResult.rows[0].elements.length > 0) {
                        distance = LResult.rows[0].elements[0].distance && LResult.rows[0].elements[0].distance.text;
                      }
                      const empobj = {
                        employee: emp._id,
                        email: emp.email,
                        name: emp.name,
                        username: emp.username,
                        address: emp.address,
                        holiday_allowance: emp.holiday_allowance,
                        joining_date: emp.joining_date,
                        final_date: emp.final_date,
                        avatar: emp.avatar,
                        employee_rate: emp.employee_rate,
                        agency_rate: emp.agency_rate,
                        client_rate: emp.client_rate,
                        lat: emp.address.lat,
                        lng: emp.address.lon,
                        locations: emp.locations,
                        job_type: emp.job_type,
                        timeoff: emp.timeoff,
                        phone: emp.phone,
                        employee_ratings: emp.employee_ratings,
                        status: emp.status,
                        distance: distance
                      };
                      if (err) {
                        callback(err);
                      } else {
                        callback(null, empobj);
                      }
                    });
                  },
                  (err, results) => {
                    if (err) {
                      res.send(err);
                    } else {
                      data.status = 1;
                      data.response = {};
                      // data.response.length = result[0].length;
                      // data.response.fullcount = fullcount;
                      if (req.body.field === "distance") {
                        if (req.body.order === 1) {
                          data.response.result = results.sort((a, b) => a.distance - b.distance);
                        }
                        if (req.body.order === -1) {
                          data.response.result = results.sort((a, b) => b.distance - a.distance);
                        }
                      } else if (req.body.order && req.body.field) {
                        data.response.result = results;
                      } else {
                        data.response.result = results.sort((a, b) => a.distance - b.distance);
                      }
                      // data.response = {length: result[0].length, fullcount: fullcount, result: results};
                      return res.send(data);
                    }
                  }
                );
              }
            }
          });
        }
      }
    });
  };

  router.assign_shift = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("shiftId", "Shift Id Required").notEmpty();
    req.checkBody("employeeId", "Employee Id Required").notEmpty();
    // req.checkBody("status", "Type Required").notEmpty();
    // req.checkBody("employeeData", "Type Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, (err, shiftData) => {
      if (err || !shiftData) {
        data.response = "Job details not available";
        return res.send(data);
      } else {
        const userQuery = [];
        userQuery.push({ $match: { employee: { $eq: req.body.employeeId } } });
        userQuery.push({
          $match: {
            $or: [
              { $and: [{ starttime: { $gte: shiftData.starttime } }, { endtime: { $lte: shiftData.endtime } }] },
              { $and: [{ starttime: { $gte: shiftData.starttime } }, { endtime: { $lte: shiftData.starttime } }] },
              { $and: [{ starttime: { $gte: shiftData.endtime } }, { endtime: { $lte: shiftData.endtime } }] }
            ]
          }
        });
        userQuery.push({ $match: { $or: [{ status: { $eq: 4 } }, { status: { $eq: 5 } }] } });
        db.GetAggregation("shifts", userQuery, (err, employeeData) => {
          if (err || !employeeData) {
            data.status = 0;
            data.err = err;
            data.response = "Unable to get employee data, Please try again";
            res.send(data);
          } else {
            const EmpDatas = employeeData && employeeData.filter(list => moment(list.start_date).format("DD-MM-YYYY") === moment(shiftData.start_date).format("DD-MM-YYYY"));
            if (EmpDatas && EmpDatas.length > 0) {
              data.status = 0;
              data.err = err;
              data.response = "Employee Already Assigned On Same Time";
              res.send(data);
            } else {
              let employee_rate = 0;
              let agency_rate = 0;
              let client_rate = 0;

              shiftData.employee_requested.map(a => {
                if (a.employee.toString() === req.body.employeeId.toString()) {
                  a.status = 2;
                  employee_rate = a.employee_rate;
                  client_rate = a.client_rate;
                  agency_rate = a.agency_rate;
                }
              });

              let shiftemployees = shiftData.employee_requested;

              if (req.body.status && req.body.status === 1) {
                const employee_requested = [];
                const employeeData = req.body.employeeData;
                employeeData.status = 2;
                employee_rate = employeeData.employee_rate;
                client_rate = employeeData.client_rate;
                agency_rate = employeeData.agency_rate;
                employee_requested.push(employeeData);
                shiftemployees = employee_requested;
              }
              const Shift_data = {
                employee_requested: shiftemployees,
                employee: req.body.employeeId,
                status: 4,
                employee_rate: employee_rate,
                agency_rate: agency_rate,
                client_rate: client_rate
              };
              db.UpdateDocument("shifts", { _id: req.body.shiftId }, Shift_data, {}, (err, docdata) => {
                if (err) {
                  data.err = err;
                  data.response = "Job not available";
                  res.send(data);
                } else {
                  const message = "Your have assigned for a shift.";
                  const options = { shiftid: req.body.shiftId };
                  push.addnotification(req.body.employeeId, message, "job_assigned", options, "EMPLOYEE", (err, response, body) => {});
                  db.GetOneDocument("employees", { _id: new mongoose.Types.ObjectId(req.body.employeeId) }, {}, {}, (err, EmployeeData) => {
                    if (EmployeeData) {
                      if (EmployeeData.settings && EmployeeData.settings.notifications.email) {
                        // Mail Sent To All
                        const mailData = {};
                        mailData.template = "newshiftassignedtoemployee";
                        mailData.from = req.params.loginData.company_email;
                        mailData.to = EmployeeData.email;
                        mailData.html = [];
                        mailData.html.push({ name: "username", value: EmployeeData.username });
                        mailData.html.push({ name: "role", value: EmployeeData.job_type });
                        mailcontent.sendmail(mailData, (err, response) => {
                          console.log("mail err,mail response", err, response);
                        });
                      }
                      if (EmployeeData.settings && EmployeeData.settings.notifications.sms) {
                        // SMS Send To All
                        const smsto = EmployeeData.phone.code + EmployeeData.phone.number;
                        const smsmessage = `Hi ${EmployeeData.username} , You Got New Shift.`;
                        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                          console.log("SMS err,SMS response", err, response);
                        });
                      }
                    }
                  });
                  data.status = 1;
                  data.response = "Shift assigned successfully.";
                  res.send(data);
                  event.emit("shiftstat", { id: req.body.shiftId, status: 4 });
                }
              });
            }
          }
        });
      }
    });
  };
  router.unassignShift = (req, res) => {
    // console.log(req.params.loginId,req.body.id);
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("employeeid", "Employee ID Required").notEmpty();

    db.GetOneDocument("shifts", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, shiftData) => {
      if (err) {
        res.send(err);
      } else {
        db.GetOneDocument("agencies", { _id: shiftData.agency }, {}, {}, (err, agencydata) => {
          if (err) {
            res.send(err);
          } else {
            db.UpdateDocument(
              "shifts",
              { _id: new mongoose.Types.ObjectId(req.body.id), employee_requested: { $elemMatch: { employee: new mongoose.Types.ObjectId(req.body.employeeid) } } },
              { status: 1, employee_requested: [], $unset: { employee: "" } },
              {},
              (err, result) => {
                if (err) {
                  data.status = 0;
                  data.response = "Unable to Save Your Data Please try again";
                  res.send(data);
                } else {
                  db.GetOneDocument("employees", { _id: shiftData.employee }, {}, {}, (err, EmployeeData) => {
                    if (EmployeeData) {
                      if (EmployeeData.settings && EmployeeData.settings.notifications.email) {
                        // Mail Sent To All
                        const mailData = {};
                        mailData.template = "shiftcancelnotificationtoagencyclient";
                        mailData.from = req.params.loginData.company_email;
                        mailData.to = EmployeeData.email;
                        mailData.html = [];
                        mailData.html.push({ name: "username", value: EmployeeData.username });
                        mailData.html.push({ name: "role", value: EmployeeData.job_type });
                        mailcontent.sendmail(mailData, (err, response) => {
                          // console.log("mail err,mail response", err, response);
                        });
                      }
                      if (EmployeeData.settings && EmployeeData.settings.notifications.sms) {
                        // SMS Send To All
                        const smsto = EmployeeData.phone.code + EmployeeData.phone.number;
                        const smsmessage = `Hi ${EmployeeData.username} , You Got New Shift Cancellation For Role ${EmployeeData.job_type}`;
                        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                          // console.log("SMS err,SMS response", err, response);
                        });
                      }
                    }
                  });
                  db.GetOneDocument("clients", { _id: shiftData.client }, {}, {}, (err, ClientData) => {
                    if (ClientData) {
                      if (ClientData.settings && ClientData.settings.notifications.email) {
                        // Mail Sent To All
                        const mailData = {};
                        mailData.template = "shiftcancelnotificationtoagencyclient";
                        mailData.from = req.params.loginData.company_email;
                        mailData.to = ClientData.email;
                        mailData.html = [];
                        mailData.html.push({
                          name: "username",
                          value: ClientData.username
                        });
                        mailData.html.push({
                          name: "role",
                          value: ClientData.job_type
                        });
                        mailcontent.sendmail(mailData, (err, response) => {
                          // console.log("mail err,mail response", err, response);
                        });
                      }
                      if (ClientData.settings && ClientData.settings.notifications.sms) {
                        // SMS Send To All
                        const smsto = ClientData.phone.code + ClientData.phone.number;
                        const smsmessage = `Hi ${ClientData.username} , You Got New Shift Cancellation For Role ${ClientData.job_type}`;
                        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                          // console.log("SMS err,SMS response", err, response);
                        });
                      }
                    }
                  });
                  data.status = 1;
                  data.response = "Shift Un-assigned";
                  res.send(data);
                  event.emit("shiftstatdelete", { id: shiftData._id, status: 4 });
                }
              }
            );
          }
        });
      }
    });
  };
  router.shiftexport = function(req, res) {
    const data = {};

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const shiftQuery = [];
    shiftQuery.push({ $match: { agency: { $eq: req.params.loginId } } });
    shiftQuery.push({ $sort: { createdAt: -1 } });

    shiftQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("shifts", shiftQuery, function(err, docData) {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          const fields = ["createdAt", "title", "locations[0]", "job_type[0]", "starttime", "endtime", "start_date", "end_date", "status"];
          const fieldNames = ["Date", "Shift", "locations", "Role", "Start Time", "End Time", "Start Date", "End Date", "Status"];

          for (let i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");
            if (docData[i].status == 1) {
              docData[i].status = "Shift Added";
            } else if (docData[i].status == 2) {
              docData[i].status = "Employee Requested";
            } else if (docData[i].status == 3) {
              docData[i].status = "Shift Accepted";
            } else if (docData[i].status == 4) {
              docData[i].status = "Shift Assigned";
            } else if (docData[i].status == 5) {
              docData[i].status = "Shift Ongoing";
            } else if (docData[i].status == 6) {
              docData[i].status = "Shift Completed";
            } else if (docData[i].status == 7) {
              docData[i].status = "Payment Completed";
            }
          }

          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(docData);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  router.deleteshift = function(req, res) {
    const data = {};
    data.status = 0;

    req.checkBody("id", "ID Required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const id = new mongoose.Types.ObjectId(req.body.id);

    db.GetOneDocument("shifts", { _id: id, status: { $lt: 4 } }, {}, {}, function(err, shifts) {
      if (err || !shifts) {
        data.response = err ? "Unable To Delete Shift" : "Shift Already Assigned";
        res.send(data);
      } else {
        db.UpdateDocument("shifts", { _id: id }, { status: 0 }, {}, function(err, shiftupdate) {
          if (err || !shiftupdate) {
            data.response = err ? "Unable To Delete Shift" : "Shift Already Assigned";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Shift Deleted";
            res.send(data);
            event.emit("shiftstatdelete", { id: id });
          }
        });
      }
    });
  };

  return router;
};
