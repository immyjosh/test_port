"use strict";
var db = require('../../model/mongodb');
var CONFIG = require('../../config/config');

module.exports = () =>{
    const router = {};
     
    var data = {};
    data.status = 0;

    router.save = (req,res) => {
        req.checkBody('name','Name is Required').notEmpty();
        req.checkBody('from','From Field is Required').notEmpty();
        req.checkBody('to','To Field is Required').notEmpty();
        req.checkBody('break','Break Field is Required').notEmpty();

        var error = req.validationErrors();
        if(error){
            data.response = error[0].msg;
            return res.send(data);
        }

        var shift = {
            name:req.body.name,
            from:req.body.from,
            to:req.body.to,
            break:req.body.break,
            status:req.body.status,
        };

        shift.addedBy = "client";
        shift.addedId = req.params.loginId;

        if(req.body._id){
            db.UpdateDocument('shifttemplates',{_id:req.body._id},shift,{}, (err ,result) => {
                if(err){
                    data.response = 'Unable to Save Your data Please try Again';
                    res.send(data);

                }else{
                    data.status = 1;
                    data.response = 'Successfully Updated';
                    res.send(data);
                }
            }); 
        }else{
            db.InsertDocument('shifttemplates',shift, (err,result) => {
                if(err || !result){
                    data.response = 'Unable to Save Your Data Please try Again';
                    res.send(data);
                }else{
                    data.status = 1;
                    data.response = { 'result': result};
                    res.send(data);
                }
            });

        }
    }

    router.list = (req,res) =>{

        var error = req.validationErrors();
        if(error){
            data.response = error[0].msg;
            return res.send(data);
        }

        var shiftQuery = [];

        shiftQuery.push({ '$match': { 'addedBy': { $eq: "client" } } });
        shiftQuery.push({ '$match': { 'addedId': { $eq: req.params.loginId } } });

        shiftQuery.push({
                    "$match": {
                        $or: [
                            { "addedId": { $eq: req.params.loginId } },
                            { "addedId": { $eq: req.params.loginData.agency } },
                        ]
                    }
                });
        
        shiftQuery.push({'$match':{'status':{$exists:true}}});
        if(req.body.status){
            shiftQuery.push({'$match':{'status':{$eq:req.body.status}}});
        }

        // if(req.body.search && req.body.search != ''){
        //     var searches = req.body.searches;
        //     var searching = {};
        //     searching[req.body.filter] = {$regex: searches + '.*', $options: 'si' };
        //     shiftQuery.push({'$match':searching});
        // }
        if (req.body.search) {
            var searchs = req.body.search;
            
            if(req.body.filter === 'all'){
                shiftQuery.push({
                    "$match": {
                        $or: [
                            { from: { $regex: searchs + '.*', $options: 'si' } },
                            { to: { $regex: searchs + '.*', $options: 'si' } },
                            { break: { $regex: searchs + '.*', $options: 'si' } },
                            { name: { $regex: searchs + '.*', $options: 'si' } }
                        ]
                    }
                });
            }else{
                var searching = {};
                searching[req.body.filter] = { $regex: searchs + '.*', $options: 'si' };
                shiftQuery.push({ "$match": searching });
            }

        }
        if(req.body.field && req.body.order){
            var sorting = {};
            sorting[req.body.field] = parseInt(req.body.order);
            shiftQuery.push({$sort:sorting});
        }else{
            shiftQuery.push({ $sort: { "createdAt" : -1 } });
        }

        shiftQuery.push({$project: { createdAt: 1, updatedAt: 1, status: 1, name: 1, from: 1, to: 1, break: 1 , addedId :1 }});

        var withoutlimit = Object.assign([], shiftQuery);
        withoutlimit.push({$count:'count'});


        if(req.body.skip >=0){
            shiftQuery.push({$skip: parseInt(req.body.skip)});
        }

        if(req.body.limit >=0){
            shiftQuery.push({$limit: parseInt(req.body.limit)});
        }

        var finalQuery = [
         {
             $facet:{
                 'overall':withoutlimit,
                 'result':shiftQuery,
             }
         }
        ];
        
       db.GetAggregation('shifttemplates',finalQuery, (err,docData) => {
           if(err || !docData){
               data.response = 'Unable to get Your Data Please Try Again';
               res.send(data);

           }else{
               data.status = 1;
               let fullcount;
             if(docData[0].overall[0] && docData[0].overall[0].count){
                  fullcount = docData[0].overall[0].count;
             }else{
                  fullcount = docData[0].result.length;
             }

           data.response = { 'result': docData[0].result, 'loginId' : req.params.loginId ,'length': docData[0].result.length, 'fullcount': fullcount };
           res.send(data);
           }
       });

    }

    router.edit = (req,res)=>{

        req.checkBody('id','Invalid id').notEmpty();

        var error = req.validationErrors();
        if(error){
            data.response = error[0].msg;
            return res.send(data);
        }

        db.GetDocument('shifttemplates',{'_id':req.body.id},{},{}, (err, shiftData) => {
            if(err || !shiftData){
                data.response = 'Unable to get Your Data Please try Again';
                res.send(data);
            }else{
                data.status = 1;
                data.response = {'result': shiftData};
                res.send(data);
            }
        });
    };

    return router;
};