module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library");
  const attachment = require("../../model/attachments");
  const mongoose = require("mongoose");
  const push = require("../../model/pushNotification.js")(io);
  const dashboard = require("../../model/dashboard.js");
  const nodemailer = require("nodemailer");
  const moment = require("moment");
  const json2csv = require("json2csv").Parser;
  const fs = require("fs");
  const mail = require("../../model/mail.js");
  const mailcontent = require("../../model/mailcontent");
  const pdf = require("html-pdf");
  const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };
  const async = require("async");
  // let RateCalculation = require("../calculation/rate");

  const router = {};
  const data = {};
  data.status = 0;

 

  router.list = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { client: { $eq: req.params.loginId } } });
    // invoiceQuery.push({$match: {timesheet_status: {$gte: 1}}});
    invoiceQuery.push({ $match: { status: { $eq: 2 } } });
    if (req.body.status) {
      invoiceQuery.push({ $match: { status: { $eq: req.body.status } } });
    }
    // else {
    //   invoiceQuery.push({
    //     $match: {
    //       $or: [{status: 6}, {status: 7}],
    //     },
    //   });
    // }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      invoiceQuery.push({ $sort: sorting });
    } else {
      invoiceQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        invoiceQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift_id: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        invoiceQuery.push({ $match: searching });
      }
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { client: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $group: {
                _id: "$status",
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "client", client: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        data.status = 1;
        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, earnings: docData[0].earnings, invoiceQuery: invoiceQuery };
        res.send(data);
      }
    });
  };

  router.view = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Shift Id required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const invoiceQuery = [];
    invoiceQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });

    invoiceQuery.push({
      $lookup: {
        from: "employees",
        let: { employeeid: "$employee" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$employeeid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "employee_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { clientid: "$client" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$clientid"]
              }
            }
          },
          { $project: { companyname: 1 } }
        ],
        as: "client_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "locations",
        let: { locationsid: "$locations" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$locationsid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "locations_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "shifts",
        let: { idd: "$shift_mid" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$idd"]
              }
            }
          }
        ],
        as: "shift_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "agencies",
        let: { agencyid: "$agency" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$agencyid"]
              }
            }
          },
          { $project: { company_name: 1 } }
        ],
        as: "agency_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch_data"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );
    db.GetAggregation("invoice", invoiceQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };
  router.groupbyemployee = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { client: { $eq: req.params.loginId } } });

    if (req.body.status) {
      invoiceQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      invoiceQuery.push({ $match: { status: { $gte: 2 } } });
    }
    // else {
    //   invoiceQuery.push({
    //     $match: {
    //       $or: [{status: 6}, {status: 7}],
    //     },
    //   });
    // }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gte: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lte: new Date(req.body.to_date) } } });
    }

    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "invoicecycle",
          let: { inv: "$invoiceID" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$invoiceID", "$$inv"]
                }
              }
            },
            { $project: { createdAt: 1, updatedAt: 1, status: 1, due_date: 1, payment_details: 1, invoiceID: 1 } }
          ],
          as: "inv_data"
        }
      },
      { $unwind: { path: "$inv_data", preserveNullAndEmptyArrays: true } },
      { $addFields: { payment_list: "$inv_data.payment_details" } },
      { $unwind: { path: "$payment_list", preserveNullAndEmptyArrays: true } },
      { $addFields: { payment_amount: { $sum: "$payment_list.amount" } } }
    );

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        invoiceQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift_id: { $regex: searchs + ".*", $options: "si" } },
              { "client.companyname": { $regex: searchs + ".*", $options: "si" } },
              { "employee.name": { $regex: searchs + ".*", $options: "si" } },
              { "locations.name": { $regex: searchs + ".*", $options: "si" } },
              { "branch.branches.branchname": { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { "job_type.name": { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        invoiceQuery.push({ $match: searching });
      }
    }
    invoiceQuery.push({
      $group: {
        // _id: {client: "$client", locations: "$locations", employee: "$employee", branch: "$branch", job_type: "$job_type"},
        _id: { client: "$client" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 },
        client_rate: { $sum: "$client_rate" },
        late_amount: { $sum: "$late_amount" },
        inv_data: { $push: "$inv_data" },
        payment_amount: { $push: "$payment_amount" }
      }
    });

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { client: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $group: {
                _id: "$status",
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        data.status = 1;
        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, earnings: docData[0].earnings, invoiceQuery: invoiceQuery };
        res.send(data);
      }
    });
  };
  router.listbyemployee = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { client: { $eq: req.params.loginId } } });
    // invoiceQuery.push({$match: {employee: {$eq: new mongoose.Types.ObjectId(req.body.employeeID)}}});
    // invoiceQuery.push({$match: {job_type: {$eq: new mongoose.Types.ObjectId(req.body.jobtypeID)}}});
    invoiceQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.clientID) } } });
    // invoiceQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // invoiceQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.branchID)}}});
    invoiceQuery.push({ $match: { status: { $gte: 2 } } });

    if (req.body.status) {
      invoiceQuery.push({ $match: { status: { $eq: req.body.status } } });
    }
    // else {
    //   invoiceQuery.push({
    //     $match: {
    //       $or: [{status: 6}, {status: 7}],
    //     },
    //   });
    // }

    if (req.body.search) {
      const searches = req.body.search;
      const searching = {};
      searching[req.body.filter] = { $regex: searches + ".*", $options: "si" };
      invoiceQuery.push({ $match: searching });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      invoiceQuery.push({ $sort: sorting });
    } else {
      invoiceQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, email: 1, avatar: 1, phone: 1, address: 1, company_logo: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "shifts",
          let: { shiftid: "$shift_mid" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$shiftid"]
                }
              }
            },
            { $project: { company_name: 1, timesheet: 1, breaktime: 1 } }
          ],
          as: "shift_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, email: 1, avatar: 1, phone: 1, address: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { client: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $group: {
                _id: "$status",
                agency_rate: { $sum: "$agency_rate" },
                client_rate: { $sum: "$client_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        data.status = 1;
        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, earnings: docData[0].earnings, invoiceQuery: invoiceQuery };
        res.send(data);
      }
    });
  };
  router.groupapproval = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("invoiceids", "Invoice Id Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetDocument("invoice", { _id: { $in: req.body.invoiceids } }, {}, {}, function(err, ShiftData) {
      if (err || !ShiftData) {
        data.response = "Invoice details not available";
        res.send(data);
        return;
      } else {
        const invoiceIDS = req.body.shiftIds;
        const SuccessData = [];
        ShiftData.map(ShiftArray => {
          InvoiceApprove(ShiftArray, invoiceIDS, (err, FinalResult) => {
            if (err) {
              data.err = err;
              data.response = err.response;
              res.send(data);
            } else {
              data.status = 1;
              data.response = { result: "Invoice Approved successfully." };
              SuccessData.push(FinalResult);
              if (ShiftData.length === SuccessData.length) {
                res.send(data);
              }
            }
          });
        });
      }
    });
  };

  router.approval = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("invoiceid", "Invoice Id Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetOneDocument("invoice", { _id: req.body.invoiceid }, {}, {}, function(err, shiftData) {
      if (err || !shiftData) {
        data.response = "Invoice details not available";
        return res.send(data);
      } else {
        db.UpdateDocument("shifts", { _id: shiftData.shift_mid }, { status: 7 }, {}, function(err, docdata) {
          if (err) {
            data.err = err;
            data.response = "Invoice not available";
            res.send(data);
          } else {
            db.UpdateDocument("invoice", { _id: shiftData._id }, { status: 2 }, {}, function(err, docdata) {
              if (err) {
                data.err = err;
                data.response = "Invoice not available";
                res.send(data);
              } else {
                data.status = 1;
                data.response = { result: "Invoice Approved successfully." };
                res.send(data);
              }
            });
          }
        });
      }
    });
  };
  router.export = (req, res) => {
    const invoiceQuery = [];
    invoiceQuery.push({ $match: { client: { $eq: req.params.loginId } } });
    invoiceQuery.push({ $match: { status: { $gte: 2 } } });

    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });

    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "invoicecycle",
          let: { inv: "$invoiceID" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$invoiceID", "$$inv"]
                }
              }
            },
            { $project: { createdAt: 1, updatedAt: 1, status: 1, due_date: 1, payment_details: 1, invoiceID: 1 } }
          ],
          as: "inv_data"
        }
      },
      { $unwind: { path: "$inv_data", preserveNullAndEmptyArrays: true } },
      { $addFields: { payment_list: "$inv_data.payment_details" } },
      { $unwind: { path: "$payment_list", preserveNullAndEmptyArrays: true } },
      { $addFields: { payment_amount: { $sum: "$payment_list.amount" } } }
    );

    invoiceQuery.push({
      $group: {
        // _id: {client: "$client", locations: "$locations", employee: "$employee", branch: "$branch", job_type: "$job_type"},
        _id: { client: "$client" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 },
        client_rate: { $sum: "$client_rate" },
        late_amount: { $sum: "$late_amount" },
        inv_data: { $push: "$inv_data" },
        payment_amount: { $push: "$payment_amount" }
      }
    });

    db.GetAggregation("invoice", invoiceQuery, (err, docdata) => {
      if (err || !docdata) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Number", "To", "Shifts", "Date", "Due Date", "Overdue by", "Paid", "Due"];
          const mydata = docdata;
          const finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            const due = mydata[i].client_rate + mydata[i].late_amount - mydata[i].payment_amount[0];
            const temp = {};
            temp["Number"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? mydata[i].inv_data[0].invoiceID || "" : "";
            temp["To"] = mydata[i].client && mydata[i].client.length > 0 ? mydata[i].client.join() || "" : "";
            temp["Shifts"] = mydata[i].count || 0;
            temp["Date"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? moment(mydata[i].inv_data[0].createdAt).format("DD-MM-YYYY") : "-";
            temp["Due Date"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? moment(mydata[i].inv_data[0].due_date).format("DD-MM-YYYY") : "-";
            temp["Overdue by"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? `${moment(mydata[i].inv_data[0].due_date).diff(moment(mydata[i].inv_data[0].createdAt), "days")} Days` : "0 Days";
            temp["Paid"] = mydata[i].payment_amount && mydata[i].payment_amount.length > 0 ? `£ ${mydata[i].payment_amount.join()}` : "£ 0";
            temp["Due"] = `£ ${due}` || "£ 0";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  router.downloadpdf = (req, res) => {
    console.log("test");
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const invoiceQuery = [];
    invoiceQuery.push({ $match: { client: { $eq: req.params.loginId } } });
    // invoiceQuery.push({$match: {employee: {$eq: new mongoose.Types.ObjectId(req.body.employeeID)}}});
    // invoiceQuery.push({$match: {job_type: {$eq: new mongoose.Types.ObjectId(req.body.jobtypeID)}}});
    invoiceQuery.push({ $match: { client: { $eq: new mongoose.Types.ObjectId(req.body.TabOpt.clientID) } } });
    // invoiceQuery.push({$match: {locations: {$eq: new mongoose.Types.ObjectId(req.body.locationID)}}});
    // invoiceQuery.push({$match: {branch: {$eq: new mongoose.Types.ObjectId(req.body.TabOpt.branchID)}}});
    invoiceQuery.push({ $match: { status: { $gte: 2 } } });

    if (req.body.search) {
      const searches = req.body.search;
      const searching = {};
      searching[req.body.filter] = { $regex: searches + ".*", $options: "si" };
      invoiceQuery.push({ $match: searching });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      invoiceQuery.push({ $sort: sorting });
    } else {
      invoiceQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      invoiceQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      invoiceQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, avatar: 1, address: 1, phone: 1, company_logo: 1, bank_details: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "shifts",
          let: { shiftid: "$shift_mid" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$shiftid"]
                }
              }
            },
            { $project: { company_name: 1, timesheet: 1, breaktime: 1 } }
          ],
          as: "shift_data"
        }
      },
      {
        $addFields: { agency: "$agency_data.company_name" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name", employee_avatar: "$employee_data.avatar" }
      }
    );

    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { client: "$client" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$client"]
              }
            }
          },
          { $project: { companyname: 1, avatar: 1, address: 1, phone: 1, email: 1, company_email: 1, company_logo: 1 } }
        ],
        as: "client_data"
      }
    });

    const withoutlimit = Object.assign([], invoiceQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      invoiceQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      invoiceQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            { $match: { agency: { $eq: new mongoose.Types.ObjectId(req.params.loginId) } } },
            {
              $group: {
                _id: "$status",
                agency_rate: { $sum: "$agency_rate" },
                employee_rate: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: invoiceQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        if (docData && docData[0].result && docData[0].result[0]) {
          db.GetOneDocument("invoicecycle", { invoiceID: req.body.invoice_invoicePrefix }, {}, {}, (err, INV_Data) => {
            if (err || !INV_Data) {
              data.status = 0;
              data.response = "Please approve Invocie";
              res.send(data);
            } else {
              const getshiftdata = [];
              // console.log("docData[0].result[0]", docData[0].result[0]);
              let totalrate,
                clienttotalrate,
                latetotalrate = 0;
              const clientAr = docData[0].result.map(list => list.client_rate);
              clienttotalrate = clientAr.reduce((a, b) => a + b);
              const lateAr = docData[0].result.map(list => list.late_amount);
              latetotalrate = lateAr.reduce((a, b) => a + b);
              // totalrate = clienttotalrate + latetotalrate;
              totalrate = clienttotalrate;
              docData[0].result.map((item, i) => {
                const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
                const evestarthr = item.starttime / 3600;
                const splithr = evestarthr.toString().split(".");
                const startsec = splithr[1];
                if (startsec === undefined) {
                  var startmins = "00";
                } else {
                  startmins = (+"0" + "." + startsec) * 60;
                }

                if (num.indexOf(splithr[0]) === -1) {
                  var starttimehr = splithr[0] + ":" + startmins;
                } else {
                  starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
                }
                const eveendhr = item.endtime / 3600;
                const splitendhr = eveendhr.toString().split(".");
                const endsec = splitendhr[1];
                if (endsec === undefined) {
                  var endmins = "00";
                } else {
                  endmins = (+"0" + "." + endsec) * 60;
                }

                if (num.indexOf(splitendhr[0]) === -1) {
                  var endtimehr = splitendhr[0] + ":" + endmins;
                } else {
                  endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
                }
                if (item.timesheet_status) {
                  var timesheet_status = item.timesheet_status;
                } else {
                  timesheet_status = "";
                }
                getshiftdata.push({
                  _id: item._id,
                  starttime: starttimehr,
                  endtime: endtimehr,
                  location: item.locations,
                  branch: item.branch[0].branches.branchname,
                  breaktime: item.breaktime,
                  start_date: item.start_date,
                  end_date: item.end_date,
                  client: item.client,
                  client_data: item.client_data,
                  agency_data: item.agency_data,
                  timesheet: item.timesheet,
                  job_type: item.job_type,
                  job_rate: item.rate_details.length > 0 ? item.rate_details[0].client_rate : "",
                  employee: item.employee,
                  employee_rate: item.employee_rate,
                  late_amount: item.late_amount,
                  client_rate: item.client_rate,
                  title: item.shift_id,
                  shiftId: item.shift_id,
                  shift_data: item.shift_data,
                  status: item.status,
                  timesheetid: item._id,
                  timesheet_status: timesheet_status,
                  employee_avatar: item.employee_avatar,
                  comment: item.agency_comment,
                  rating: item.agency_rating
                });
                return true;
              });
              let CurrentRate = 0;
              if (INV_Data) {
                const CurrAr = INV_Data.payment_details.map(list => list.amount);
                CurrentRate = CurrAr && CurrAr.length > 0 && CurrAr.reduce((a, b) => a + b);
              }
              const html = `<style>html {
zoom: 0.55;
}</style><table style="margin: 0; padding: 0; color: #000; background: #eee; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
          <tbody>
           <tr>
             <td>
               <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>
                  <tr>
                    <td>
                        <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                        <tr>
                          <td style="width: 40%; vertical-align: top;">
                           <h4 style="font-size: 18px; font-weight: bold;color: #000;">INVOICE</h4>
                         <p style="font-size: 15px; line-height: 26px;   color: #4e4c4c; width: 80%;">
                         ${getshiftdata[0].client_data[0].companyname} <br>
                         ${getshiftdata[0].client_data[0].address.formatted_address}, ${getshiftdata[0].client_data[0].address.zipcode} .
                          <br>Tel. :  ${getshiftdata[0].client_data[0].phone.code} -  ${getshiftdata[0].client_data[0].phone.number}</p>
                         
                        </td>
                         <td style="width: 30%; vertical-align: top;">
                           <span style="font-size: 17px; font-weight: bold; display: block;">Invoice Date</span>
                         
                           <span style="font-size: 14px; color: #4e4c4c; line-height: 30px;">${req.body.invoice_from_date} - ${req.body.invoice_to_date}</span>
                         <br> <br>
                         
                          <span style="font-size: 17px; font-weight: bold; display: block;">Invoice Number</span>
                         
                         <span style="font-size: 14px; color: #4e4c4c; line-height: 30px;">${req.body.invoice_invoicePrefix}</span>
                        </td>
                         <td style="width: 30%; vertical-align: top;">
                            <img src="${CONFIG.LIVEURL}/${getshiftdata[0].agency_data[0].company_logo}" style=" width: 120px;    height: 120px;">
                          <p style="font-size: 15px; line-height: 26px;   color: #4e4c4c; width: 90%;"> ${getshiftdata[0].agency_data[0].address.formatted_address}, ${getshiftdata[0].agency_data[0].address.zipcode} .
                           <br> Tel. : ${getshiftdata[0].agency_data[0].phone.code} -  ${getshiftdata[0].agency_data[0].phone.number}</p>
                        </td>
                        </tr>
                        </tbody>
                     </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                       <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <thead>
                        <tr>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Shift No. </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Work Location  </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Employee </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Shift </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Time </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Rate </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Hours </th>
                            <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;text-align: left;"> Total Price </th>
                        </tr>
                      </thead>
                      <tbody>
                        ${getshiftdata
                          .map(
                            item => `<tr>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${item.title}	 </td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${item.branch} </td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${item.employee} </td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${moment(item.start_date).format("DD-MM-YYYY")} </td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${item.starttime} - ${item.endtime} </td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> £ ${item.job_rate}	</td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> ${Math.round(item.shift_data[0].timesheet[0].workmins / 60)} Minutes </td>
                          <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;"> £ ${item.client_rate}</td>
                        </tr>`
                          )
                          .join(" ")}
                      </tbody>
                                  </table>							 
                    </td>
                  </tr>
                  <tr>
                   <td>
                   <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                     <tbody>
                       <tr>
                       <td width="80%" align="right" style="font-size:17px;color:#000;font-weight:bold;">
                        Late Booking Fee
                       </td>
                       <td width="20%" align="center" style="font-size:17px;color:#000;font-weight:bold;">
                       £ ${latetotalrate}
                       </td>
                       </tr> 
                       <tr>
                       <td width="80%" align="right" style="font-size:17px;color:#000;font-weight:bold;">
                        Total
                       </td>
                       <td width="20%" align="center" style="font-size:17px;color:#000;font-weight:bold;">
                       £ ${totalrate}
                       </td>
                       </tr>
                       <tr>
                       <td width="80%" align="right" style="font-size:17px;color:#000;font-weight:bold;">
                        Due Amount
                       </td>
                       <td width="20%" align="center" style="font-size:17px;color:#000;font-weight:bold;">
                       £ ${totalrate - CurrentRate}
                       </td>
                       </tr>
                      </tbody>
                    </table>				 
                   </td>
                      </tr>
                <tr>
                  <td>
                    <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;    border-top: 1px solid #ddd;">
                   <tbody>
                      <tr>
                       <td style="font-size: 18px; font-weight: bold; color: #000;">Due Date: ${moment(new Date(INV_Data.due_date)).format("DD-MM-YYYY")}</td>
                     </tr>
                     <tr>
                       <td style="font-size: 15px; font-weight: bold; color: #000; line-height: 40px;">PAYMENT ADVICE</td>
                     </tr>
                     <tr>
                       <td style="font-size: 14px; color: #4e4c4c;">Thank You for your Business!</td>
                     </tr>
                     
                     <tr>
                         <td style="line-height:30px;">
                         <span style="font-size: 14px; color: #4e4c4c;"> Bank: ${getshiftdata[0].agency_data[0].bank_details &&
                           getshiftdata[0].agency_data[0].bank_details.name} </span>  <span style="font-size: 14px; color: #4e4c4c;    padding: 0px 60px;"> Bank Acc No: ${getshiftdata[0].agency_data[0].bank_details &&
                getshiftdata[0].agency_data[0].bank_details.ac_no} </span>
                       </td>
                     </tr>
                     <tr>
                         <td style="line-height:30px;">
                         <span style="font-size: 14px; color: #4e4c4c;"> BECS: ${getshiftdata[0].agency_data[0].bank_details && getshiftdata[0].agency_data[0].bank_details.becs} </span>
                       </td>
                     </tr>
                     <tr>
                       <td style="font-size: 14px; color: #4e4c4c; line-height: 30px;">SEPA: ${getshiftdata[0].agency_data[0].bank_details && getshiftdata[0].agency_data[0].bank_details.sepa}</td>
                     </tr>
                     
                     <tr>
                        <td>
                          <a style="font-size: 13px; color: #00adf1; line-height: 30px; text-transform: uppercase;    font-weight: bold;" href="#">View and pay online now </a>
                        </td>
                     </tr>
                     
                   </tbody>
                  </table>		  
                  </td>
                </tr>
                <tr>
                   <td>
                    <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;    border-top: 1px solid #ddd;">
                      <tbody>
                          <tr>
                          <td style="font-size: 18px; font-weight: bold; color: #000;"> PAYMENT ADVICE </td>
                        </tr>
                        <tr>
                          <td style="font-size: 18px; line-height: 37px; color: #888888;"> To</td>
                        </tr>
                        <tr>
                           <td width="50%" style="vertical-align:top;" >
                           <p style="font-size: 15px; line-height: 26px;   color: #4e4c4c; width: 80%;">${getshiftdata[0].client_data[0].companyname},${getshiftdata[0].client_data[0].address.formatted_address},${
                getshiftdata[0].client_data[0].address.zipcode
              }. Tel. : ${getshiftdata[0].client_data[0].phone.code} -  ${getshiftdata[0].client_data[0].phone.number} .</p>
                         </td>
                         <td width="50%" style="vertical-align:top;">
                           <p style="font-size: 15px; line-height: 16px;   color: #4e4c4c;"> <span style="    font-weight: bold; color: #000;"> Customer :</span> <span> ${getshiftdata[0].client_data[0].companyname} </span></p>
                           <p style="font-size: 15px; line-height: 16px;   color: #4e4c4c;"> <span style="    font-weight: bold; color: #000;"> Invoice No :</span> <span> ${req.body.invoice_invoicePrefix} </span></p>
                           <p style="font-size: 15px; line-height: 16px;   color: #4e4c4c;"> <span style="    font-weight: bold; color: #000;"> Amount Due :</span> <span> £ ${totalrate - CurrentRate} </span></p>
                           <p style="font-size: 15px; line-height: 16px;   color: #4e4c4c;"> <span style="    font-weight: bold; color: #000;"> Due Date :</span> <span> ${moment(new Date(INV_Data.due_date)).format("DD-MM-YYYY")} </span></p>
                         </td>
                        </tr>
                      </body>
                     </table>				 
                   </td>
                 </tr>
                </tbody>
                </table>
             </td>
           </tr>
           </tbody>
       </table>   `;
       if (req.body.for === "download") {
                pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
              } else {
                pdf.create(html, pdfoptions).toFile("./uploads/pdf/invoices.pdf", function(err, document) {
                  if (err) {
                    res.send(err);
                  } else {
                    const mailOptions = {
                      from: docData[0].result[0].client_data[0].email,
                      to: docData[0].result[0].client_data[0].email,
                      subject: "Invoices",
                      text: "Please Download the attachment to see Invoices",
                      html: "<b>Please Download the attachment to see Invoices</b>",
                      attachments: [
                        {
                          filename: "invoices.pdf",
                          path: "./uploads/pdf/invoices.pdf",
                          contentType: "application/pdf"
                        }
                      ]
                    };
                    mail.send(mailOptions, function(err, response) {});
                    data.status = 1;
                    data.msg = "Mail Sent";
                    res.send(data);
                  }
                });
              }
            }
          });
        } else {
          data.status = 0;
          data.response = "Invoice Details Not Available";
          res.send(data);
        }
      }
    });
  };
  router.getinvoicecycle = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("invoiceID", "Invoice Id Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }
    db.GetOneDocument("invoicecycle", { invoiceID: req.body.invoiceID }, {}, {}, (err, InvoiceData) => {
      if (err || !InvoiceData) {
        data.status = 0;
        data.response = "Invoice details not available";
        res.send(data);
      } else {
        let result = 0;
        if (InvoiceData) {
          result = InvoiceData;
        }
        data.status = 1;
        data.response = result;
        res.send(data);
      }
    });
  };
  router.invociedates = (req, res) => {
    const data = {};
    async.waterfall(
      [
        callback => {
          db.GetDocument("clients", { _id: new mongoose.Types.ObjectId(req.params.loginId) }, { _id: 1 }, {}, (err, ClientData) => {
            if (err || !ClientData) {
              data.response = "Date details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, ClientData);
            }
          });
        },
        (ClientData, callback) => {
          db.GetDocument("billing", { agency: new mongoose.Types.ObjectId(ClientData.agency), type: "invoice" }, {}, {}, (err, Billing_Dates) => {
            if (err || !Billing_Dates) {
              data.response = "Date details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, ClientData, Billing_Dates);
            }
          });
        },
        (ClientData, Billing_Dates, callback) => {
          const options = {};
          options.sort = { createdAt: -1 };
          db.GetOneDocument("invoice", { agency: new mongoose.Types.ObjectId(ClientData.agency) }, { createdAt: 1, start_date: 1 }, options, (err, Invocies) => {
            if (err) {
              data.response = "Shift details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, ClientData, Billing_Dates, Invocies);
            }
          });
        }
      ],
      (err, ClientData, Billing_Dates, Invocies) => {
        if (err) {
          data.status = 0;
          data.response = "Unable to get Dates, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { datelist: Billing_Dates || [], lastinvoice: (Invocies && Invocies.createdAt) || new Date() };
          res.send(data);
        }
      }
    );
  };
  return router;
};
