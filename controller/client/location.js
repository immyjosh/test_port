"use strict";
const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");

module.exports = () => {
  const router = {};
  const data = {};
  data.status = 0;

  router.save = (req, res) => {
    req.checkBody("name", "Name is Required").notEmpty();
    req.checkBody("phone", "Phone Number is Required").notEmpty();
    req.checkBody("address", "Address is Required").notEmpty();
    // req.checkBody('color','Color is Required').notEmpty();
    req.checkBody("zipcode", "zipcode is Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const loc = {
      name: req.body.name,
      phone: req.body.phone,
      address: req.body.address,
      color: req.body.color,
      zipcode: req.body.zipcode,
      status: req.body.status,
      addedBy: "client",
      addedId: req.params.loginId
    };
    db.GetDocument("locations", { addedId: req.params.loginId, name: req.body.name }, {}, {}, (err, user) => {
      if (err) {
        data.response = "Invalid Credentials";
        res.send(data);
      } else {
        if (user && user.length > 0) {
          data.response = "Name is already Exists ";
          res.send(data);
        } else {
          if (req.body._id) {
            db.UpdateDocument("locations", { _id: req.body._id }, loc, {}, (err, result) => {
              if (err) {
                data.response = "Unable to Save Your Data Please Try Again";
                res.send(data);
              } else {
                data.status = 1;
                data.response = "Successfully Updated";
                res.send(data);
              }
            });
          } else {
            db.InsertDocument("locations", loc, (err, result) => {
              if (err || !result) {
                data.response = "Unable to Save Your Data Please Try Again";
                res.send(data);
              } else {
                data.status = 1;
                data.response = { result: result };
                res.send(data);
              }
            });
          }
        }
      }
    });
  };

  router.list = (req, res) => {
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const locationQuery = [];

    locationQuery.push({ $match: { status: { $exists: true } } });
    // locationQuery.push({ '$match': { 'addedBy': { $eq: "client" } } });
    // locationQuery.push({ '$match': { 'addedId': { $eq: req.params.loginId } } });
    // locationQuery.push({ '$match': { '_id': { $eq: req.params.loginData.location } } });
    // locationQuery.push({ '$match': { '_id': { $in: req.params.loginData.additional_locations } } });
    locationQuery.push({ $match: { _id: { $eq: req.params.loginId } } });
    // locationQuery.push({
    //             "$match": {
    //                 $or: [
    //                     { "_id": { $eq: req.params.loginData } },
    //                     { "_id": { $in: req.params.loginData.additional_locations } }
    //                 ]
    //             }
    //         });
    // locationQuery.push({
    //     $lookup: {
    //         from: "locations",
    //         let: { location_type: "$locations" },
    //         pipeline: [{
    //             $match: {
    //                 $expr: {
    //                     $in: ["$_id", "$$location_type"],
    //                 }
    //             }
    //         }],
    //         as: "locations",
    //     }
    // });
    locationQuery.push({
      $lookup: {
        from: "locations",
        let: { locations: "$locations" },
        pipeline: [
          {
            $match: { $expr: { $in: ["$_id", "$$locations"] } }
          },
          {
            $match: { $expr: { $eq: ["$status", 1] } }
          }
        ],
        as: "locations"
      }
    });
    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status || 1 } } });
    }

    // if(req.body.search){
    //     var searches = req.body.search;
    //     var searching = {};
    //     searching[req.body.filter] = {$regex: searches +'.*',$options:'si'};
    //     locationQuery.push({'$match':searching});
    // }
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        locationQuery.push({
          $match: {
            $or: [
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { address: { $regex: searchs + ".*", $options: "si" } },
              { color: { $regex: searchs + ".*", $options: "si" } },
              { zipcode: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        locationQuery.push({ $match: searching });
      }
    }
    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      locationQuery.push({ $sort: sorting });
    } else {
      locationQuery.push({ $sort: { createdAt: -1 } });
    }

    // locationQuery.push({$project:{ createdAt:1, updatedAt:1, status:1, name:1, color:1, phone:1, address:1, zipcode:1,}});
    locationQuery.push({ $project: { locations: 1, branches: 1, status: 1, name: 1, address: 1, zipcode: 1 } });

    // var withoutlimit = JSON.parse(JSON.stringify(locationQuery));
    // withoutlimit.push({
    //     $count:'count'
    // });

    const withoutlimit = Object.assign([], locationQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      locationQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      locationQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          result: locationQuery
        }
      }
    ];

    db.GetAggregation("clients", finalQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }

        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    req.checkBody("id", "Invalid id").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    db.GetDocument("locations", { _id: req.body.id }, {}, {}, (err, locationData) => {
      if (err || !locationData) {
        data.response = "Unable to get Your Data Please try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: locationData };
        res.send(data);
      }
    });
  };
  return router;
};
