const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library.js");
const attachment = require("../../model/attachments.js");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const moment = require("moment");
const dashboard = require("../../model/dashboard");
const nodemailer = require("nodemailer");
const mongoose = require("mongoose");
const mail = require("../../model/mail.js");
const mailcontent = require("../../model/mailcontent");
const pdf = require("html-pdf");
const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };

module.exports = () => {
  const router = {};

  router.downloadpdf = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid Id").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const userQuery = [];
    userQuery.push({ $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });

    userQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );
    userQuery.push({
      $lookup: {
        from: "clients",
        let: { client: new mongoose.Types.ObjectId(req.params.loginId) },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$client"]
              }
            }
          },
          { $project: { name: 1, email: 1 } }
        ],
        as: "client_data"
      }
    });

    userQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );
    db.GetAggregation("employees", userQuery, (err, employeeData) => {
      if (err) {
        res.send(err);
      } else {
        if (employeeData && employeeData[0]) {
          const html = `<style>html {
zoom: 0.55;
}</style><table style="margin: 0; padding: 0; color: #000; background: #eee; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
          <tbody>
           <tr>
             <td>
               <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>	 
                  <tr>
                    <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                        <tr>
                          <td style="width: 20%; vertical-align: top;">
                          <img src="${CONFIG.LIVEURL}/${employeeData[0].avatar}" style=" width: 120px; height: 120px; border-radius:50%;">
                         
                        </td>
                         <td style="width: 80%; vertical-align: top;">
                           <span style="font-size: 16px; font-weight: bold; display: block;line-height: 30px;">${employeeData[0].username}</span>
                         
                           <span style="font-size: 14px; color: #4e4c4c; line-height: 30px; display:block;">${employeeData[0].email}</span>
                         
                         <span style="font-size: 14px; color: #4e4c4c; line-height: 30px; display:block;">${employeeData[0].job_type.map(list => list)}</span>
                        </td>
                        
                        </tr>
                        </tbody>
                     </table>
                    </td>
                  </tr> 
                   <tr>
                    <td>
                                 <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                        <tr>
                           <td width="50%" style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Name:</span><span style="font-size: 14px; color: #4e4c4c;"> ${
                             employeeData[0].name
                           }</span> </td>
                           <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Area:</span><span style="font-size: 14px; color: #4e4c4c;"> ${employeeData[0].locations.map(
                             list => list
                           )} </span> </td>
                        </tr>
                         <tr>							     
                           <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Phone:</span><span style="font-size: 14px; color: #4e4c4c;"> ${
                             employeeData[0].phone.code
                           } - ${employeeData[0].phone.number}</span> </td>
                          <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Job Role:</span><span style="font-size: 14px; color: #4e4c4c;"> ${employeeData[0].job_type.map(
                            list => list
                          )}</td>
                        </tr>
                        <tr>							     
                           <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;" >UserName:</span><span style="font-size: 14px; color: #4e4c4c;">  ${
                             employeeData[0].username
                           } </span> </td>
                          <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;" >Holiday Allowance:</span><span style="font-size: 14px; color: #4e4c4c;"> £ ${
                            employeeData[0].holiday_allowance
                          }  </span> </td>
                        </tr>
                        <tr>							     
                           <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Email:</span><span style="font-size: 14px; color: #4e4c4c;"> ${
                             employeeData[0].email
                           }</span> </td>
                          <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Joining Date:</span><span style="font-size: 14px; color: #4e4c4c;"> ${moment(
                            employeeData[0].joining_date
                          ).format("DD-MM-YYYY")} </span> </td>
                        </tr>
                        <tr>							     
                           <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Address:</span><span style="font-size: 14px; color: #4e4c4c;"> ${
                             employeeData[0].address.formatted_address
                           } , ${employeeData[0].address.zipcode} </span> </td>
                          <td width="50%"  style="border-bottom:1px solid #ddd;padding:12px;"> <span style="font-size: 14px; font-weight: bold;">Last Working Date:</span><span style="font-size: 14px; color: #4e4c4c;"> ${moment(
                            employeeData[0].final_date
                          ).format("DD-MM-YYYY")}</span> </td>
                        </tr>
                      </tbody>
                                  </table>						 
                     </td>
                 </tr>
                </tbody>
                </table>
             </td>
           </tr>
          </tbody>
       </table>    `;

          if (req.body.for === "download") {
            pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
          } else {
            pdf.create(html, pdfoptions).toFile("./uploads/pdf/profile.pdf", (err, document) => {
              if (err) {
                res.send(err);
              } else {
                data.status = 1;
                data.msg = "Mail Sent";
                res.send(data);
                const mailOptions = {
                  from: employeeData[0].client_data[0].email,
                  to: employeeData[0].client_data[0].email,
                  subject: "Profile",
                  text: "Please Download the attachment to see Profile",
                  html: "<b>Please Download the attachment to see Profile</b>",
                  attachments: [
                    {
                      filename: "User Profile.pdf",
                      path: "./uploads/pdf/profile.pdf",
                      contentType: "application/pdf"
                    }
                  ]
                };
                mail.send(mailOptions, function(err, response) {});
              }
            });
          }
        } else {
          data.status = 0;
          data.response = "Employee Details Not Available";
          res.send(data);
        }
      }
    });
  };
  return router;
};
