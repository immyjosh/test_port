module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library.js");
  const nodemailer = require("nodemailer");
  const dashboard = require("../../model/dashboard.js");
  const push = require("../../model/pushNotification.js")(io);

  const router = {};
  router.clientdashboard = (req, res) => {
    const populate = { selectshift: req.body.selectshift, selecttimesheet: req.body.selecttimesheet, selectinvoice: req.body.selectinvoice, start: req.body.startdate, end: req.body.enddate };
    const data = {};
    data.status = 0;
    dashboard.clientdashboardCount({ type: "client", client: req.params.loginId, populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };
  router.notifications = (req, res) => {
    const data = {};
    data.status = 0;

    dashboard.notifications({ type: "client", client: req.params.loginId }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        res.send(response);
      }
    });
  };

  router.login = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("password", "Password Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    const email = req.body.email;
    const password = req.body.password;

    db.GetOneDocument("clients", { $or: [{ email: email, status: 1 }, { username: email, status: 1 }] }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Invalid Credentials";
        res.send(data);
      } else {
        if (library.validPassword(password, user.password)) {
          db.GetOneDocument("clients", { $or: [{ email: email, status: 1, isverified: 1 }, { username: email, status: 1, isverified: 1 }] }, {}, {}, (err, user) => {
            if (err || !user) {
              data.response = "Your account is not verified.";
              res.send(data);
            } else {
              const updata = { activity: {} };
              updata.activity.last_login = Date();
              db.UpdateDocument("clients", { _id: user._id }, updata, {}, (err, docdata) => {
                if (err) {
                  data.response = "Invalid Credentials";
                  res.send(data);
                } else {
                  // const auth_token = library.jwtSign({ username: user.username, role: "client" });
                  // data.status = 1;
                  // data.response = { auth_token: auth_token };
                  // res.send(data);
                  const options = {};
                  options.sort = { createdAt: -1 };
                  db.GetDocument("subscriptions", { agency: user.agency, status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
                    if (subscriptionData.length >= 1) {
                      data.status = 1;
                      const auth_token = library.jwtSign({
                        username: user.username,
                        role: "client",
                        subscription: 1,
                        employee_count: subscriptionData[0].plan.employees,
                        recruitment_module: subscriptionData[0].plan.recruitment_module
                      });
                      data.response = { auth_token: auth_token };
                      res.send(data);
                    } else {
                      data.status = 1;
                      const auth_token = library.jwtSign({ username: user.username, role: "client", subscription: 0, employee_count: 0, recruitment_module: 0 });
                      data.response = { auth_token: auth_token };
                      res.send(data);
                    }
                  });
                }
              });
            }
          });
        } else {
          data.response = "Invalid Credentials";
          res.send(data);
        }
      }
    });
  };

  router.logout = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Username Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.UpdateDocument("clients", { username: req.body.username }, { "activity.last_logout": new Date() }, {}, (err, response) => {
      if (err) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "logged out";
        res.send(data);
      }
    });
  };

  router.profilesave = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("name", "Name Required").notEmpty();
    if (!req.body._id || req.body.password != "") {
      req.checkBody("password", "Password Required").notEmpty();
      req.checkBody("confirm_password", "password do not match").equals(req.body.password);
    }
    req.checkBody("phone.code", "Phone Number Required").notEmpty();
    req.checkBody("phone.number", "Phone Number Required").notEmpty();
    req.checkBody("address.country", "Address Required").notEmpty();
    req.checkBody("address.zipcode", "Address Required").notEmpty();
    req.checkBody("address.formatted_address", "Address Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const client = {
      email: req.body.email,
      name: req.body.name,
      phone: req.body.phone,
      status: req.body.status,
      address: req.body.address,
      establishmenttype: req.body.establishmenttype,
      numberofbeds: req.body.numberofbeds,
      patienttype: req.body.patienttype,
      specialrequirements: req.body.specialrequirements,
      shiftpatens: req.body.shiftpatens,
      agreedstaffrates: req.body.agreedstaffrates,
      attain: req.body.attain,
      invoiceaddress: req.body.invoiceaddress,
      invoicephone: req.body.invoicephone,
      invoicefax: req.body.invoicefax,
      invoiceemail: req.body.invoiceemail,
      // additionallocations: req.body.additionallocations,
      companyname: req.body.companyname,
      company_email: req.body.company_email,
      fax: req.body.fax
    };

    if (req.body.password && req.body.confirm_password) {
      client.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && req.files && typeof req.files.avatar != "undefined") {
      if (req.files.avatar.length > 0) {
        client.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // client.avatar = req.body.avatar;
    }

    if (req.body._id) {
      db.UpdateDocument("clients", { _id: req.body._id }, client, {}, (err, result) => {
        if (err) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Client updated successfully.";
          res.send(data);
        }
      });
    } else {
      data.response = "Unable to Save Your Data Please try again";
      res.send(data);
    }
  };
  router.deactivatesave = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("status", "Status Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const client = {
      status: req.body.status
    };
    if (req.body._id) {
      db.UpdateDocument("clients", { _id: req.body._id }, client, {}, (err, result) => {
        if (err) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Client updated successfully.";
          res.send(data);
        }
      });
    } else {
      data.response = "Unable to Save Your Data Please try again";
      res.send(data);
    }
  };
  router.profile = (req, res) => {
    const data = {};
    data.status = 0;

    db.GetDocument("clients", { _id: req.params.loginId }, {}, {}, (err, clientData) => {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: clientData };
        res.send(data);
      }
    });
  };

  router.forgotpassword = (req, res) => {
    const data = {};
    const request = {};
    req.checkBody("email", "Invalid email").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    request.email = req.body.email;
    request.reset = library.randomString(8, "#A");

    async.waterfall(
      [
        callback => {
          db.GetOneDocument("clients", { email: request.email }, {}, {}, (err, client) => {
            callback(err, client);
          });
        },
        (client, callback) => {
          if (client) {
            db.UpdateDocument("clients", { _id: client._id }, { reset_code: request.reset }, {}, (err, response) => {
              callback(err, client);
            });
          } else {
            callback(null, client);
          }
        },
        (client, callback) => {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, settings) => {
            if (err) {
              callback(err, callback);
            } else {
              callback(err, client, settings);
            }
          });
        }
      ],
      (err, client, settings) => {
        if (err || !client) {
          data.status = "0";
          data.response = "Errror!";
          res.status(400).send(data);
        } else {
          let name;
          if (client.name) {
            name = client.name + " (" + client.username + ")";
          } else {
            name = client.username;
          }
          clientId = client._id;

          const mailData = {};
          mailData.template = "forgotpassword";
          mailData.to = client.email;
          mailData.html = [];
          mailData.html.push({ name: "name", value: name });
          mailData.html.push({ name: "site_url", value: settings.settings.site_url });
          mailData.html.push({ name: "logo", value: settings.settings.logo });
          mailData.html.push({ name: "email", value: client.email });
          mailData.html.push({ name: "url", value: settings.settings.site_url + "client/resetpassword" + "/" + clientId + "/" + request.reset });
          mailcontent.sendmail(mailData, (err, response) => {});
          data.status = "1";
          data.url = settings.settings.site_url + "client/resetpassword" + "/" + clientId + "/" + request.reset;
          data.response = "Reset Code Sent Successfully!";
          res.send(data);
        }
      }
    );
  };

  router.resetpassword = (req, res) => {
    const id = req.params.userid;
    const resetid = req.params.reset;
    const data = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

    db.UpdateDocument("clients", { _id: id, reset_code: resetid }, { password: data }, {}, (err, docdata) => {
      if (err || docdata.nModified == 0) {
        var data = {};
        data.status = "0";
        data.response = "Reset Password error!";
        res.send(data);
      } else {
        data = {};
        data.status = "1";
        data.response = "Reset Successfully!";
        res.send(data);
      }
    });
  };

  /* router.jobtypes = (req,res) => {
        var clientData = req.params.loginData;

         var data = {};
         data.status = 0;
         var error = req.validationErrors();
         if(error){
             data.response = error[0].msg;
             return res.send(data);
         }
        var jobQuery = [];
        jobQuery.push({ '$match': { 'addedBy': { $eq: "agency" } } });
        jobQuery.push({ '$match': { 'addedId': { $eq: clientData.agency } } });
        jobQuery.push({'$match':{'status':{$exists:true}}});
        if(req.body.status){
            jobQuery.push({'$match':{'status':{$eq: 1 }}});
        }
        jobQuery.push({ $project: { createdAt: 1, updatedAt: 1,  status : 1 , name : 1 }});

        db.GetAggregation('jobtypes', jobQuery, (err, docData) => {
            if(err || !docData){
                data.response = 'Unalbe to Get Your data Please try Again';
                res.send(err);
            }else{
                data.status = 1;
                data.response = {'result': docData , "jobQuery" : jobQuery };
                res.send(data);
            }
        });
    }

    router.job_requests = (req, res) => {

        var clientData = req.params.loginData;


        var data = {};
        data.status = 0;
        req.checkBody('shiftId', 'Shift Id required').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            data.response = errors[0].msg;
            return res.send(data);
        }

        db.GetOneDocument('shifts',{ 'shiftId' : req.body.shiftId },{},{}, (err, shiftData) => {

            if(err || !shiftData){
                data.response = err;
                return res.send(data);
            }else{

                var userQuery = [];
                userQuery.push({ '$match': { 'agency': { $eq: clientData.agency } } });
                userQuery.push({ '$match': { 'job_type': { $eq: shiftData.job_type } } });

                db.GetAggregation('employees', userQuery, (err, employeeData) => {

                    if (err || !employeeData) {
                        data.response = "Unable to get your data, Please try again";
                        res.send(data);
                    } else {

                        if(employeeData.length <= 0){

                            data.response = "Employees not found";
                            res.send(data);

                        }else{

                            employeeData.map((emp) => {
                                var message = "New Job request";
                                var options = { 'shiftid': shiftData.shiftid, 'emp_id': emp._id };
                                push.sendPushnotification(emp._id, message, 'job_request', 'ANDROID', options, 'EMPLOYEE', (err, response, body) => {});
                            });

                            db.UpdateDocument('shifts', { 'shiftId' : req.body.shiftId }, { 'status' : 2 }, {}, (err, docdata) => {
                                data.status = 1;
                                data.response = { "result" : "Job Request sent to employees." };
                                res.send(data);
                            });

                        }
                    }
                });
            }
        });
    };*/
  router.settingssave = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("mailstatus", "Required Email Status").optional();
    req.checkBody("smsstatus", "Required Name Status").optional();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    let Data;
    if (req.body.for === "notifications") {
      const notifications = {
        email: req.body.mailstatus,
        sms: req.body.smsstatus
      };
      Data = { $set: { "settings.notifications": notifications } };
    } else if (req.body.for === "jobrolesave") {
      Data = { $set: { "settings.job_roles": req.body.job_roles } };
    }

    db.UpdateDocument("clients", { _id: req.params.loginId }, Data, { upsert: true }, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Unable to save your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated successfully.";
        res.send(data);
      }
    });
  };
  router.job_types = (req, res) => {
    const data = {};
    data.status = 0;

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("clients", { _id: req.params.loginId }, {}, {}, (err, ClientData) => {
      if (err || !ClientData) {
        data.status = 0;
        data.response = "Unable to save your data, Please try again";
        res.send(data);
      } else {
        const jobQuery = [];
        jobQuery.push({ $match: { addedBy: { $eq: "agency" } } });
        jobQuery.push({ $match: { addedId: { $eq: ClientData.agency } } });

        jobQuery.push({ $match: { status: { $exists: true } } });
        jobQuery.push({ $match: { employee: { $exists: false } } });
        jobQuery.push({ $match: { client: { $exists: false } } });
        jobQuery.push({ $match: { job_id: { $exists: false } } });
        if (req.body.status) {
          jobQuery.push({ $match: { status: { $eq: 1 } } });
        }

        if (req.body.search) {
          const searchs = req.body.search;

          if (req.body.filter === "all") {
            jobQuery.push({
              $match: {
                $or: [{ name: { $regex: searchs + ".*", $options: "si" } }]
              }
            });
          } else {
            const searching = {};
            searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
            jobQuery.push({ $match: searching });
          }
        }

        if (req.body.filter && req.body.order) {
          const sorting = {};
          sorting[req.body.filter] = parseInt(req.body.order);
          jobQuery.push({ $sort: sorting });
        } else {
          jobQuery.push({ $sort: { createdAt: -1 } });
        }
        if (req.body.from_date) {
          jobQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
        }

        if (req.body.to_date) {
          jobQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
        }
        jobQuery.push({
          $project: { status: 1, name: 1, _id: 1 }
        });

        //  var withoutlimit = JSON.parse(JSON.stringify(jobQuery));
        //  withoutlimit.push({
        //      $count:'count'
        //  });

        const withoutlimit = Object.assign([], jobQuery);
        withoutlimit.push({ $count: "count" });

        if (req.body.skip >= 0) {
          jobQuery.push({ $skip: parseInt(req.body.skip) });
        }

        if (req.body.limit >= 0) {
          jobQuery.push({ $limit: parseInt(req.body.limit) });
        }

        const finalQuery = [
          {
            $facet: {
              overall: withoutlimit,
              result: jobQuery
            }
          }
        ];

        db.GetAggregation("jobtypes", finalQuery, (err, docData) => {
          if (err || !docData) {
            data.response = "Unalbe to Get Your data Please try Again";
            res.send(err);
          } else {
            data.status = 1;
            let fullcount;
            if (docData[0].overall[0] && docData[0].overall[0].count) {
              fullcount = docData[0].overall[0].count;
            } else {
              fullcount = docData[0].result.length;
            }

            data.response = {
              result: docData[0].result,
              length: docData[0].result.length,
              fullcount: fullcount
            };
            res.send(data);
          }
        });
      }
    });
  };
  router.clearnotification = (req, res) => {
    const data = {};
    db.UpdateMany("notifications", { client: req.params.loginId }, { status: 2 }, {}, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Please try again later";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Cleared";
        res.send(data);
      }
    });
  };
  return router;
};
