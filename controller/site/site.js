module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library.js");
  const dashboard = require("../../model/dashboard.js");
  const attachment = require("../../model/attachments.js");
  const push = require("../../model/pushNotification.js")(io);
  const mongoose = require("mongoose");
  const mailcontent = require("../../model/mailcontent");
  const twilio = require("../../model/twilio.js");
  const moment = require("moment");
  const async = require("async");
  const event = require("../../controller/events/events");
  const pdf = require("html-pdf");
  const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };

  const router = {};

  router.agency_registration = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Invalid Username").notEmpty();
    req
      .checkBody("email", "Invalid Email")
      .notEmpty()
      .isEmail();
    req.checkBody("name", "Invalid Name").notEmpty();
    if (!req.body._id) {
      req.checkBody("password", "Invalid Password").notEmpty();
      req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
    }
    req.checkBody("phone.code", "Invalid Number").notEmpty();
    req.checkBody("phone.number", "Invalid Number").notEmpty();
    req.checkBody("address.zipcode", "Invalid Address").notEmpty();
    req.checkBody("address.country", "Invalid Address").notEmpty();
    req.checkBody("address.formatted_address", "Invalid Address").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const agency = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      status: req.body.status,
      phone: req.body.phone,
      address: req.body.address,
      surname: req.body.surname,
      isverified: 0
    };

    if (req.body.password && req.body.confirm_password) {
      agency.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        agency.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // agency.avatar = req.body.avatar;
    }

    db.InsertDocument("agencies", agency, (err, result) => {
      if (err || !result) {
        if (err.code === 11000) {
          data.response = "Username/Email is already Exists ";
        } else {
          data.response = "Unable to Save Your Data Please try again";
        }
        res.send(err);
      } else {
        data.status = 1;

        const message = "New Agency Registered";
        const options = { agency_id: result._id, username: result.username };
        push.addnotification("", message, "agency_register", options, "ADMIN", (err, response, body) => {});

        const mailData = {};
        mailData.template = "signupsuccessmessage";
        mailData.to = result.email;
        mailData.html = [];
        mailData.html.push({ name: "username", value: result.username });
        mailData.html.push({ name: "role", value: "Agency" });
        mailcontent.sendmail(mailData, (err, response) => {
          console.log("mail err,mail response", err, response);
        });

        data.response = { result: result };
        res.send(data);
        event.emit("agencyemailtemplates", { agency: result._doc._id });
      }
    });
  };
  router.client_registration = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Username Required").notEmpty();
    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("name", "Name Required").notEmpty();
    if (!req.body._id) {
      req.checkBody("password", "Password Required").notEmpty();
      req.checkBody("confirm_password", "password do not match").equals(req.body.password);
    }
    req.checkBody("phone.code", "Phone Number Required").notEmpty();
    req.checkBody("phone.number", "Phone Number Required").notEmpty();
    req.checkBody("address.zipcode", "Address Required").notEmpty();
    req.checkBody("address.formatted_address", "Address Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();
    req.checkBody("location", "Location Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const client = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      phone: req.body.phone,
      status: req.body.status,
      address: req.body.address,
      establishmenttype: req.body.establishmenttype,
      numberofbeds: req.body.numberofbeds,
      patienttype: req.body.patienttype,
      specialrequirements: req.body.specialrequirements,
      shiftpatens: req.body.shiftpatens,
      agreedstaffrates: req.body.agreedstaffrates,
      attain: req.body.attain,
      invoiceaddress: req.body.invoiceaddress,
      invoicephone: req.body.invoicephone,
      invoicefax: req.body.invoicefax,
      invoiceemail: req.body.invoiceemail,
      additionallocations: req.body.additionallocations,
      companyname: req.body.companyname,
      fax: req.body.fax,
      agency: req.body.agency,
      isverified: 0,
      location: req.body.location,
      additional_locations: req.body.additional_locations
    };
    // console.log("req.body.agency", req.body.agency);

    if (req.body.password && req.body.confirm_password) {
      client.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        client.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // client.avatar = req.body.avatar;
    }

    db.InsertDocument("clients", client, function(err, result) {
      if (err || !result) {
        data.err = err;
        data.body = req.body;
        data.response = "Unable to Save Your Data Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Registered successfully.";

        const message = "New Client Registered";
        const options = { agency_id: req.body.agency, client_id: result._id, username: result.username };
        push.addnotification(req.body.agency, message, "client_register", options, "AGENCY", (err, response, body) => {});

        const mailData = {};
        mailData.template = "signupsuccessmessage";
        mailData.to = result.email;
        mailData.html = [];
        mailData.html.push({ name: "username", value: result.username });
        mailData.html.push({ name: "role", value: "Client" });
        mailcontent.sendmail(mailData, (err, response) => {
          // console.log("err, response",err, response);
        });

        res.send(data);
      }
    });
  };
  router.employee_registration = (req, res) => {
    console.log("Employee Registration", req.body);
    const data = {};
    data.status = 0;

    req.checkBody("agency", "Agency Required").notEmpty();
    req.checkBody("username", "Username Required").notEmpty();
    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("name", "Name Required").notEmpty();
    if (!req.body._id) {
      req.checkBody("password", "Password Required").notEmpty();
      req.checkBody("confirm_password", "password do not match").equals(req.body.password);
    }
    req.checkBody("phone.code", "Phone Number Required").notEmpty();
    req.checkBody("phone.number", "Phone Number Required").notEmpty();

    req.checkBody("address.zipcode", "Address Required").notEmpty();
    req.checkBody("address.formatted_address", "Address Required").notEmpty();
    // req.checkBody("employee_rate", "Employee rate Required").notEmpty();
    // req.checkBody('address.lat', 'Address Required').notEmpty();
    // req.checkBody('address.lon', 'Address Required').notEmpty();

    req.checkBody("status", "Status Required").notEmpty();
    req.checkBody("job_type", "Jobtype Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const employee = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      phone: req.body.phone,
      status: req.body.status,
      address: req.body.address,
      job_type: req.body.job_type,
      locations: req.body.locations,
      agency: req.body.agency,
      available: req.body.available,
      timeoff: req.body.timeoff,
      isverified: req.body.isverified,
      employee_rate: req.body.employee_rate
    };

    if (req.body.password && req.body.confirm_password) {
      employee.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        employee.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // employee.avatar = req.body.avatar;
    }

    const options = {};
    options.sort = { createdAt: -1 };
    db.GetDocument("subscriptions", { agency: new mongoose.Types.ObjectId(req.body.agency), status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
      if (subscriptionData.length >= 1) {
        if (subscriptionData[0].plan.recruitment_module === 0) {
          employee["onlineform.form_status"] = 9;
          employee["onlineform.final_verify_status"] = 1;
          employee["onlineform.recruitment_module"] = 0;
        } else if (subscriptionData[0].plan.recruitment_module === 1) {
          employee["onlineform.form_status"] = 0;
          employee["onlineform.recruitment_module"] = 1;
        }
        db.InsertDocument("employees", employee, (err, result) => {
          if (err || !result) {
            data.err = err;
            data.response = "Unable to Save Your Data Please try again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "employee added successfully.";

            const message = "New Employee Registered";
            const options = { agency_id: req.body.agency, employee_id: result._id, username: result.username };
            push.addnotification(req.body.agency, message, "employee_register", options, "AGENCY", (err, response, body) => {});
            const mailData = {};
            mailData.template = "signupsuccessmessage";
            mailData.to = result.email;
            mailData.agency = result.agency;
            mailData.html = [];
            mailData.html.push({ name: "username", value: result.username });
            mailData.html.push({ name: "role", value: "Employee" });
            mailcontent.sendcustommail(mailData, (err, response) => {
              // console.log("err, response",err, response);
            });
            res.send(data);
          }
        });
      }
    });
  };
  router.agencies = (req, res) => {
    const data = {};
    data.status = 0;

    const agencyQuery = [];
    agencyQuery.push({ $match: { status: { $eq: 1 } } });

    agencyQuery.push({
      $lookup: {
        from: "jobtypes",
        let: { agencyid: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$addedId", "$$agencyid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "jobtypes"
      }
    });

    agencyQuery.push({
      $lookup: {
        from: "locations",
        let: { agencyid: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$addedId", "$$agencyid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "locations"
      }
    });

    agencyQuery.push({ $project: { name: 1, jobtypes: 1, locations: 1 } });

    db.GetAggregation("agencies", agencyQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };
  router.site_settings = (req, res) => {
    const data = {};
    db.GetOneDocument("settings", { alias: "general" }, {}, {}, function(err, settings) {
      if (err) {
        data.status = 0;
        data.response = "Unable to get site data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { settings: settings.settings };
        res.send(data);
      }
    });
  };
  router.invociedates = (req, res) => {
    const data = {};
    async.waterfall(
      [
        function(callback) {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, Settings) => {
            if (err || !Settings) {
              data.response = "Settings details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, Settings);
            }
          });
        },
        function(Settings, callback) {
          const InvoiceQuery = [];
          InvoiceQuery.push({ $sort: { createdAt: 1 } }, { $limit: 1 });
          db.GetAggregation("invoice", InvoiceQuery, (err, Invoices) => {
            if (err || !Invoices) {
              data.response = "Shift details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, Settings, Invoices);
            }
          });
        }
      ],
      (err, Settings, Invoices) => {
        if (err) {
          data.status = 0;
          data.response = "Unable to get Dates data, Please try again";
          res.send(data);
        } else {
          const dayscount = Settings.settings.billingcycle || 7;
          const startDate = Invoices.length > 0 ? Invoices[0].createdAt : new Date();
          const endDate = new Date();
          const totaldays = Math.floor((moment(endDate) - moment(startDate)) / 86400000) + 1;
          // console.log('totaldays', totaldays);
          const today = new Date();
          const count = Math.round(totaldays / dayscount);
          // console.log('count', count);
          const sdate = [];
          const edate = [];
          const sdate1 = [];
          const edate1 = [];
          const datelist = [];
          for (let i = 0; i <= count; i++) {
            const sd = moment(today).format("DD/MM/YYYY");
            edate.push(sd);
            const sd1 = moment(today).format("YYYY-MM-DD 23:59:59");
            edate1.push(sd1);
            today.setDate(today.getDate() - dayscount);
            const ed = moment(today).format("DD/MM/YYYY");
            sdate.push(ed);
            const ed1 = moment(today).format("YYYY-MM-DD");
            sdate1.push(ed1);
          }
          for (let i = 0; i <= sdate.length; i++) {
            if (i != sdate.length) {
              datelist.push({ label: `${sdate[i]} - ${edate[i]}`, valuess: `${sdate1[i]} / ${edate1[i]}` });
            }
          }
          data.status = 1;
          data.response = { datelist };
          res.send(data);
        }
      }
    );
  };
  router.timesheetdates = (req, res) => {
    const data = {};
    async.waterfall(
      [
        function(callback) {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, Settings) => {
            if (err || !Settings) {
              data.response = "Settings details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, Settings);
            }
          });
        },
        function(Settings, callback) {
          const ShiftsQuery = [];
          ShiftsQuery.push({ $sort: { createdAt: 1 } }, { $limit: 1 });
          db.GetAggregation("shifts", ShiftsQuery, (err, Shifts) => {
            if (err || !Shifts) {
              data.response = "Shift details not available";
              data.status = 0;
              callback(data);
            } else {
              callback(err, Settings, Shifts);
            }
          });
        }
      ],
      (err, Settings, Shifts) => {
        if (err) {
          data.status = 0;
          data.response = "Unable to get Dates data, Please try again";
          res.send(data);
        } else {
          const dayscount = Settings.settings.billingcycle || 7;
          const startDate = Shifts.length > 0 ? Shifts[0].createdAt : new Date();
          const endDate = new Date();
          const totaldays = Math.floor((moment(endDate) - moment(startDate)) / 86400000) + 1;
          // console.log('totaldays', totaldays);
          const today = new Date();
          const count = Math.round(totaldays / dayscount);
          // console.log('count', count);
          const sdate = [];
          const edate = [];
          const sdate1 = [];
          const edate1 = [];
          const datelist = [];
          for (let i = 0; i <= count; i++) {
            const sd = moment(today).format("DD/MM/YYYY");
            edate.push(sd);
            const sd1 = moment(today).format("YYYY-MM-DD 23:59:59");
            edate1.push(sd1);
            today.setDate(today.getDate() - dayscount);
            const ed = moment(today).format("DD/MM/YYYY");
            sdate.push(ed);
            const ed1 = moment(today).format("YYYY-MM-DD");
            sdate1.push(ed1);
          }
          for (let i = 0; i <= sdate.length; i++) {
            if (i != sdate.length) {
              datelist.push({ label: `${sdate[i]} - ${edate[i]}`, valuess: `${sdate1[i]} / ${edate1[i]}` });
            }
          }
          data.status = 1;
          data.response = { datelist };
          res.send(data);
        }
      }
    );
  };
  router.timesheetprefixnumber = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("for", "For Required").notEmpty();
    req.checkBody("username", "For Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    if (req.body.for === "agency") {
      db.GetOneDocument("agencies", { username: req.body.username }, {}, {}, (err, AgencyData) => {
        if (err || !AgencyData) {
          data.response = "Details not available";
          data.status = 0;
          res.send(data);
        } else {
          if (AgencyData.settings && AgencyData.settings.timesheet && AgencyData.settings.timesheet.prefix && AgencyData.settings.timesheet.number) {
            db.GetCount("timesheetcycle", { agency: new mongoose.Types.ObjectId(AgencyData._id) }, (err, TimesheetCount) => {
              if (err) {
                data.response = "Details not available";
                data.status = 0;
                res.send(data);
              } else {
                let Result = "Not Available";
                if (AgencyData.settings.timesheet) {
                  const Count = TimesheetCount === 0 || TimesheetCount === "" || TimesheetCount === null ? 0 : TimesheetCount;
                  const Prefix = AgencyData.settings.timesheet.prefix;
                  const number = AgencyData.settings.timesheet.number + Count;
                  Result = `${Prefix}-${number}`;
                }
                data.response = Result;
                data.status = 1;
                res.send(data);
              }
            });
          } else {
            data.response = "Timesheet Settings Not Available";
            data.status = 0;
            res.send(data);
          }
        }
      });
    }
    if (req.body.for === "client") {
      db.GetOneDocument("clients", { username: req.body.username }, {}, {}, (err, ClientData) => {
        if (err || !ClientData) {
          data.response = "Client Details not available";
          data.status = 0;
          res.send(data);
        } else {
          db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(ClientData.agency) }, {}, {}, (err, AgencyData) => {
            if (err || !AgencyData) {
              data.response = "Agency Details not available";
              data.status = 0;
              res.send(data);
            } else {
              if (AgencyData.settings && AgencyData.settings.timesheet && AgencyData.settings.timesheet.prefix && AgencyData.settings.timesheet.number) {
                db.GetCount("timesheetcycle", { agency: new mongoose.Types.ObjectId(ClientData.agency) }, (err, TimesheetCount) => {
                  if (err) {
                    data.response = "Timesheet Details not available";
                    data.status = 0;
                    res.send(data);
                  } else {
                    let Result = "Not Available";
                    if (AgencyData.settings.timesheet) {
                      const Count = TimesheetCount === 0 ? 0 : TimesheetCount;
                      const Prefix = AgencyData.settings.timesheet.prefix;
                      const number = AgencyData.settings.timesheet.number + Count;
                      Result = `${Prefix}-${number}`;
                    }
                    data.response = Result;
                    data.status = 1;
                    res.send(data);
                  }
                });
              } else {
                data.response = "Timesheet Settings Not Available";
                data.status = 0;
                res.send(data);
              }
            }
          });
        }
      });
    }
    if (req.body.for === "employee") {
      db.GetOneDocument("employees", { username: req.body.username }, {}, {}, (err, EmployeeData) => {
        if (err || !EmployeeData) {
          data.response = "Details not available";
          data.status = 0;
          res.send(data);
        } else {
          db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(EmployeeData.agency) }, {}, {}, (err, AgencyData) => {
            if (err || !AgencyData) {
              data.response = "Details not available";
              data.status = 0;
              res.send(data);
            } else {
              if (AgencyData.settings && AgencyData.settings.timesheet && AgencyData.settings.timesheet.prefix && AgencyData.settings.timesheet.number) {
                db.GetCount("timesheetcycle", { agency: new mongoose.Types.ObjectId(EmployeeData.agency) }, (err, TimesheetCount) => {
                  if (err) {
                    data.response = "Details not available";
                    data.status = 0;
                    res.send(data);
                  } else {
                    let Result = "Not Available";
                    if (AgencyData.settings.timesheet) {
                      const Count = TimesheetCount === 0 ? 0 : TimesheetCount;
                      const Prefix = AgencyData.settings.timesheet.prefix;
                      const number = AgencyData.settings.timesheet.number + Count;
                      Result = `${Prefix}-${number}`;
                    }
                    data.response = Result;
                    data.status = 1;
                    res.send(data);
                  }
                });
              } else {
                data.response = "Timesheet Settings Not Available";
                data.status = 0;
                res.send(data);
              }
            }
          });
        }
      });
    }
  };
  router.invoiceprefixnumber = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("for", "For Required").notEmpty();
    req.checkBody("username", "For Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    if (req.body.for === "agency") {
      db.GetOneDocument("agencies", { username: req.body.username }, {}, {}, (err, AgencyData) => {
        if (err || !AgencyData) {
          data.response = "Agency Details not available";
          data.status = 0;
          res.send(data);
        } else {
          if (AgencyData.settings && AgencyData.settings.invoice && AgencyData.settings.invoice.prefix && AgencyData.settings.invoice.number) {
            db.GetCount("invoicecycle", { agency: new mongoose.Types.ObjectId(AgencyData._id) }, (err, invoiceCount) => {
              if (err) {
                data.response = "Invoice Settings not available";
                data.status = 0;
                res.send(data);
              } else {
                let Result = "Not Available";
                if (AgencyData.settings.invoice) {
                  const Count = invoiceCount === 0 || invoiceCount === "" || invoiceCount === null ? 0 : invoiceCount;
                  const Prefix = AgencyData.settings.invoice.prefix;
                  const number = AgencyData.settings.invoice.number + Count;
                  Result = `${Prefix}-${number}`;
                }
                data.response = Result;
                data.status = 1;
                res.send(data);
              }
            });
          } else {
            data.response = "invoice Settings Not Available";
            data.status = 0;
            res.send(data);
          }
        }
      });
    }
    if (req.body.for === "client") {
      db.GetOneDocument("clients", { username: req.body.username }, {}, {}, (err, ClientData) => {
        if (err || !ClientData) {
          data.response = "Client Details not available";
          data.status = 0;
          res.send(data);
        } else {
          db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(ClientData.agency) }, {}, {}, (err, AgencyData) => {
            if (err || !AgencyData) {
              data.response = "Agency Details not available";
              data.status = 0;
              res.send(data);
            } else {
              if (AgencyData.settings && AgencyData.settings.invoice && AgencyData.settings.invoice.prefix && AgencyData.settings.invoice.number) {
                db.GetCount("invoicecycle", { agency: new mongoose.Types.ObjectId(ClientData.agency) }, (err, invoiceCount) => {
                  if (err) {
                    data.response = "invoice Details not available";
                    data.status = 0;
                    res.send(data);
                  } else {
                    let Result = "Not Available";
                    if (AgencyData.settings.invoice) {
                      const Count = invoiceCount === 0 ? 0 : invoiceCount;
                      const Prefix = AgencyData.settings.invoice.prefix;
                      const number = AgencyData.settings.invoice.number + Count;
                      Result = `${Prefix}-${number}`;
                    }
                    data.response = Result;
                    data.status = 1;
                    res.send(data);
                  }
                });
              } else {
                data.response = "invoice Settings Not Available";
                data.status = 0;
                res.send(data);
              }
            }
          });
        }
      });
    }
    if (req.body.for === "employee") {
      db.GetOneDocument("employees", { username: req.body.username }, {}, {}, (err, EmployeeData) => {
        if (err || !EmployeeData) {
          data.response = "Details not available";
          data.status = 0;
          res.send(data);
        } else {
          db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(EmployeeData.agency) }, {}, {}, (err, AgencyData) => {
            if (err || !AgencyData) {
              data.response = "Details not available";
              data.status = 0;
              res.send(data);
            } else {
              if (AgencyData.settings && AgencyData.settings.invoice && AgencyData.settings.invoice.prefix && AgencyData.settings.invoice.number) {
                db.GetCount("invoicecycle", { agency: new mongoose.Types.ObjectId(EmployeeData.agency) }, (err, invoiceCount) => {
                  if (err) {
                    data.response = "Details not available";
                    data.status = 0;
                    res.send(data);
                  } else {
                    let Result = "Not Available";
                    if (AgencyData.settings.invoice) {
                      const Count = invoiceCount === 0 ? 0 : invoiceCount;
                      const Prefix = AgencyData.settings.invoice.prefix;
                      const number = AgencyData.settings.invoice.number + Count;
                      Result = `${Prefix}-${number}`;
                    }
                    data.response = Result;
                    data.status = 1;
                    res.send(data);
                  }
                });
              } else {
                data.response = "invoice Settings Not Available";
                data.status = 0;
                res.send(data);
              }
            }
          });
        }
      });
    }
  };

  router.referencesave = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Invalid Form").notEmpty();
    req.checkBody("referencefor", "Invalid Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const value = req.body;
    const newkey = req.body.referencefor;
    db.GetOneDocument("employees", { username: value.username }, {}, {}, (err, employeeData) => {
      if (err || !employeeData) {
        data.response = "Unable to get employee data";
        res.send(data);
      } else {
        if (newkey === "reference1") {
          if (employeeData.onlineform.reference1.referee && employeeData.onlineform.reference1.referee.status === 0) {
            data.response = "Invalid Form";
            return res.send(data);
          } else if (employeeData.onlineform.reference1.referee && employeeData.onlineform.reference1.referee.status === 2) {
            data.response = "Already Submited";
            return res.send(data);
          }
        } else if (newkey === "reference2") {
          if (employeeData.onlineform.reference2.referee && employeeData.onlineform.reference2.referee.status === 0) {
            data.response = "Invalid Form";
            return res.send(data);
          } else if (employeeData.onlineform.reference2.referee && employeeData.onlineform.reference2.referee.status === 2) {
            data.response = "Already Submited";
            return res.send(data);
          }
        }
        const attach = attachment.get_attachment(req.files.campanystamp[0].destination, req.files.campanystamp[0].filename);
        const update = {
          $set: {
            ["onlineform." + newkey + ".referee.refereefirstname"]: value.refereefirstname,
            ["onlineform." + newkey + ".referee.refereelastname"]: value.refereelastname,
            ["onlineform." + newkey + ".referee.refereeemail"]: value.refereeemail,
            ["onlineform." + newkey + ".referee.refereephone"]: value.refereephone,
            ["onlineform." + newkey + ".referee.refereejobtitle"]: value.refereejobtitle,
            ["onlineform." + newkey + ".referee.refereecompany"]: value.refereecompany,
            ["onlineform." + newkey + ".referee.refereecapacity"]: value.refereecapacity,
            ["onlineform." + newkey + ".referee.applicantfirstname"]: value.applicantfirstname,
            ["onlineform." + newkey + ".referee.applicantlastname"]: value.applicantlastname,
            ["onlineform." + newkey + ".referee.applicantemail"]: value.applicantemail,
            ["onlineform." + newkey + ".referee.applicantphone"]: value.applicantphone,
            ["onlineform." + newkey + ".referee.applicantjobtitle"]: value.applicantjobtitle,
            ["onlineform." + newkey + ".referee.applicantcompany"]: value.applicantcompany,
            ["onlineform." + newkey + ".referee.employedfrom"]: value.employedfrom,
            ["onlineform." + newkey + ".referee.employedto"]: value.employedto,
            ["onlineform." + newkey + ".referee.remployee"]: value.remployee,
            ["onlineform." + newkey + ".referee.remployeenotes"]: value.remployeenotes || "",
            ["onlineform." + newkey + ".referee.followcareplans"]: value.followcareplans,
            ["onlineform." + newkey + ".referee.reliability"]: value.reliability,
            ["onlineform." + newkey + ".referee.character"]: value.character,
            ["onlineform." + newkey + ".referee.dignity"]: value.dignity,
            ["onlineform." + newkey + ".referee.attitude"]: value.attitude,
            ["onlineform." + newkey + ".referee.communication"]: value.communication,
            ["onlineform." + newkey + ".referee.relationships"]: value.relationships,
            ["onlineform." + newkey + ".referee.workunderinitiative"]: value.workunderinitiative,
            ["onlineform." + newkey + ".referee.disciplinaryaction"]: value.disciplinaryaction,
            ["onlineform." + newkey + ".referee.disciplinaryactiondetails"]: value.disciplinaryactiondetails || "",
            ["onlineform." + newkey + ".referee.investigations"]: value.investigations,
            ["onlineform." + newkey + ".referee.investigationsdetails"]: value.investigationsdetails || "",
            ["onlineform." + newkey + ".referee.vulnerablepeople"]: value.vulnerablepeople,
            ["onlineform." + newkey + ".referee.vulnerablepeopledetails"]: value.vulnerablepeopledetails || "",
            ["onlineform." + newkey + ".referee.criminaloffence"]: value.criminaloffence,
            ["onlineform." + newkey + ".referee.criminaloffencedetails"]: value.criminaloffencedetails || "",
            ["onlineform." + newkey + ".referee.additionalcomments"]: value.additionalcomments || "",
            ["onlineform." + newkey + ".referee.confirm"]: value.confirm,
            ["onlineform." + newkey + ".referee.agree"]: value.agree,
            ["onlineform." + newkey + ".referee.campanystamp"]: attach,
            ["onlineform." + newkey + ".referee.status"]: 2
          }
        };
        db.UpdateDocument("employees", { _id: employeeData._id }, update, {}, (err, updatedoc) => {
          if (err || !updatedoc) {
            data.response = "Unable to save your data";
            res.send(data);
          } else {
            const message = "referee_form_completed";
            const options = {
              emp_id: employeeData._id
            };
            push.addnotification(employeeData.agency, message, "referee_form_completed", options, "AGENCY", (err, response, body) => {});
            db.GetOneDocument("agencies", { _id: employeeData.agency }, {}, {}, (err, AgencyData) => {
              if (AgencyData) {
                if (AgencyData.settings.notifications.email) {
                  // Mail Sent To All
                  db.GetOneDocument("employees", { _id: employeeData._id }, {}, {}, (err, employeeData) => {
                    if (employeeData) {
                      let refereefirstname = "",
                        refereelastname = "",
                        refereeemail = "",
                        refereephone = "",
                        refereejobtitle = "",
                        refereecompany = "",
                        refereecapacity = "",
                        applicantfirstname = "",
                        applicantlastname = "",
                        applicantemail = "",
                        applicantphone = "",
                        applicantjobtitle = "",
                        applicantcompany = "",
                        employedfrom = "",
                        employedto = "",
                        remployee = "",
                        remployeenotes = "",
                        followcareplans = "",
                        reliability = "",
                        character = "",
                        dignity = "",
                        attitude = "",
                        communication = "",
                        relationships = "",
                        workunderinitiative = "",
                        disciplinaryaction = "",
                        disciplinaryactiondetails = "",
                        investigations = "",
                        investigationsdetails = "",
                        vulnerablepeople = "",
                        vulnerablepeopledetails = "",
                        criminaloffences = "",
                        criminaloffencedetails = "",
                        additionalcomments = "",
                        campanystamp = "",
                        form_reference1 = {},
                        form_reference2 = {};
                      if (newkey === "reference1") {
                        form_reference1 = employeeData.onlineform.reference1.referee || {};
                        refereefirstname = form_reference1.refereefirstname;
                        refereelastname = form_reference1.refereelastname;
                        refereeemail = form_reference1.refereeemail;
                        refereephone = form_reference1.refereephone;
                        refereejobtitle = form_reference1.refereejobtitle;
                        refereecompany = form_reference1.refereecompany;
                        refereecapacity = form_reference1.refereecapacity;
                        applicantfirstname = form_reference1.applicantfirstname;
                        applicantlastname = form_reference1.applicantlastname;
                        applicantemail = form_reference1.applicantemail;
                        applicantphone = form_reference1.applicantphone;
                        applicantjobtitle = form_reference1.applicantjobtitle;
                        applicantcompany = form_reference1.applicantcompany;
                        employedfrom = form_reference1.employedfrom;
                        employedto = form_reference1.employedto;
                        remployee = form_reference1.remployee;
                        remployeenotes = form_reference1.remployeenotes;
                        followcareplans = form_reference1.followcareplans;
                        reliability = form_reference1.reliability;
                        character = form_reference1.character;
                        dignity = form_reference1.dignity;
                        attitude = form_reference1.attitude;
                        communication = form_reference1.communication;
                        relationships = form_reference1.relationships;
                        workunderinitiative = form_reference1.workunderinitiative;
                        disciplinaryaction = form_reference1.disciplinaryaction;
                        disciplinaryactiondetails = form_reference1.disciplinaryactiondetails;
                        investigations = form_reference1.investigations;
                        investigationsdetails = form_reference1.investigationsdetails;
                        vulnerablepeople = form_reference1.vulnerablepeople;
                        vulnerablepeopledetails = form_reference1.vulnerablepeopledetails;
                        criminaloffences = form_reference1.criminaloffences;
                        criminaloffencedetails = form_reference1.criminaloffencedetails;
                        additionalcomments = form_reference1.additionalcomments;
                        campanystamp = form_reference1.campanystamp;
                      } else if (newkey === "reference2") {
                        form_reference2 = employeeData.onlineform.reference2.referee || {};
                        refereefirstname = form_reference2.refereefirstname;
                        refereelastname = form_reference2.refereelastname;
                        refereeemail = form_reference2.refereeemail;
                        refereephone = form_reference2.refereephone;
                        refereejobtitle = form_reference2.refereejobtitle;
                        refereecompany = form_reference2.refereecompany;
                        refereecapacity = form_reference2.refereecapacity;
                        applicantfirstname = form_reference2.applicantfirstname;
                        applicantlastname = form_reference2.applicantlastname;
                        applicantemail = form_reference2.applicantemail;
                        applicantphone = form_reference2.applicantphone;
                        applicantjobtitle = form_reference2.applicantjobtitle;
                        applicantcompany = form_reference2.applicantcompany;
                        employedfrom = form_reference2.employedfrom;
                        employedto = form_reference2.employedto;
                        remployee = form_reference2.remployee;
                        remployeenotes = form_reference2.remployeenotes;
                        followcareplans = form_reference2.followcareplans;
                        reliability = form_reference2.reliability;
                        character = form_reference2.character;
                        dignity = form_reference2.dignity;
                        attitude = form_reference2.attitude;
                        communication = form_reference2.communication;
                        relationships = form_reference2.relationships;
                        workunderinitiative = form_reference2.workunderinitiative;
                        disciplinaryaction = form_reference2.disciplinaryaction;
                        disciplinaryactiondetails = form_reference2.disciplinaryactiondetails;
                        investigations = form_reference2.investigations;
                        investigationsdetails = form_reference2.investigationsdetails;
                        vulnerablepeople = form_reference2.vulnerablepeople;
                        vulnerablepeopledetails = form_reference2.vulnerablepeopledetails;
                        criminaloffences = form_reference2.criminaloffence;
                        criminaloffencedetails = form_reference2.criminaloffencedetails;
                        additionalcomments = form_reference2.additionalcomments;
                        campanystamp = form_reference2.campanystamp;
                      }
                      const html = `  <style>html {
                        zoom: 0.55;
                        }</style>  <table style="margin: 0; padding: 0; color: #000; background: #fff;  padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                          <tr>
                                            <td>
                                              <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                                                <tbody>
                                                  <tr>
                                        <td>
                                        <table style="margin: 0px auto; padding: 0 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                          <tbody>
                                          <tr>
                                             <td width="30%">
                                              <img src="logo.png">
                                           </td>
                                           <td width="70%" style="background-image:url(staff-profile.png);background-repeat: no-repeat;background-size: cover;height: 124px;font-size: 24px;color: #fff;    padding: 0px 0 0 70px; font-weight: 600;">
                                               Referee Form
                                           </td>
                                          </tr>
                                        </tbody>
                                        </table>
                                      </td>
                                      </tr>
                                                  <tr>
                                                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                                                      Referee Details </td>
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                                                          <tr>
                                                            <td width="50%">
                                                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">First name</label>
                                                                
                                            <input type="text" value="${refereefirstname || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Email</label>
                                                                
                                            <input type="text" value="${refereeemail || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Job Title</label>
                                                                
                                            <input type="text" value="${refereejobtitle || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            
                                                            </td>
                                                            <td width="50%">
                                                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; ">Last name</label>
                                                                
                                            <input type="text" value="${refereelastname || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; ">Phone</label>
                                                                
                                            <input type="text" value="${(refereephone.code || "") + "-" + (refereephone.number || "-")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; ">Company</label>
                                                                
                                            <input type="text" value="${refereecompany || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                                            </td>
                                                           
                                                          </tr>
                                         
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                      <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                        
                                                          <tr>
                                                            <td width="100%">
                                                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:20%;float:left;font-size: 12px; ">In what capacity did you know the applicant?:</label>
                                                                
                                            <textarea type="text" value="" style="width:80%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${refereecapacity || ""}</textarea>
                                            </p>
                                            
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                      
                                                 <tr>
                                                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                                                      Applicant Details </td>
                                                  </tr>
                                      
                                       <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                        
                                                          <tr>
                                                            <td width="50%" style="vertical-align:top;">
                                                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">First name</label>
                                                                
                                            <input type="text" value="${applicantfirstname || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                             <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Email </label>
                                                                
                                            <input type="text" value="${applicantemail || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Job Title </label>
                                                                
                                            <input type="text" value="${applicantjobtitle || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Date Employed from </label>
                                                                
                                            <input type="text" value="${moment(new Date(employedfrom)).format("DD-MM-YYYY") || "-"}" style="width:58%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Would you re-employ this person?</label>
                                                                
                                            <span style=" width:50%; font-size: 12px; color:#7b7c80;">
                                            <label> <input type="radio" name="remployee" ${remployee === "yes" ? "checked" : ""}>  Yes</label>
                                            <label> <input type="radio" name="remployee" ${remployee === "no" ? "checked" : ""}>  No</label>
                                            </span>
                                            </p>
                                                            </td>
                                                            <td width="50%" style="vertical-align:top;">
                                                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; ">Last name</label>
                                                                
                                            <input type="text" value="${applicantlastname || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; "> Phone </label>
                                                                
                                            <input type="text" value="${(applicantphone.code || "") + "-" + (applicantphone.number || "-")}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; "> Company</label>
                                            <input type="text" value="${applicantcompany || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:50%;float:left;font-size: 12px; "> Date Employed to</label>
                                            <input type="text" value="${moment(new Date(employedto)).format("DD-MM-YYYY") || "-"}" style="width:50%;height:38px;border:1px solid #e2e5f1;background-color:#e2e5f1;">
                                            </p>
                                                            </td>
                                          
                                                           
                                                          </tr>
                        
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                      <tr>
                                                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                                                     SECTION 1 - HOW WOULD YOU ASSESS THE FOLLOWING?</td>
                                                  </tr>
                                       <tr>
                                                    <td style="width: 100%; font-size: 13px; color: #585858; padding: 15px 30px;">
                                                     Please tick the relevant boxes Excellent, Good, Average, Poor</td>
                                                  </tr>
                                      
                                      <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                        
                                                          <tr>
                                                            <td width="50%" style="vertical-align:top;">
                                                               
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Ability to follow care plans</label>
                                                                
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="followcareplans" ${followcareplans === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="followcareplans" ${followcareplans === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="followcareplans" ${followcareplans === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="followcareplans" ${followcareplans === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Character</label>
                                                                
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="character" ${character === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="character" ${character === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="character" ${character === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="character" ${character === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Ability to ensure dignity is upheld</label>
                                                                
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="dignity" ${dignity === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="dignity" ${dignity === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="dignity" ${dignity === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="dignity" ${dignity === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Relationships with colleagues</label>
                                                                
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="relationships" ${relationships === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="relationships" ${relationships === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="relationships" ${relationships === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="relationships" ${relationships === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                                            </td>
                                                            <td width="50%" style="vertical-align:top;">
                                                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Reliability, timekeeping,attendance</label>
                                                                
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="reliability" ${reliability === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="reliability" ${reliability === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="reliability" ${reliability === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="reliability" ${reliability === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Attitude</label>
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="attitude" ${attitude === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="attitude" ${attitude === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="attitude" ${attitude === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="attitude" ${attitude === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Communication</label>
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="communication" ${communication === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="communication" ${communication === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="communication" ${communication === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="communication" ${communication === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Ability to work under own initiative</label>
                                            <span style="width:60%;float:left;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "excellent" ? "checked" : ""}>  Excellent </label>
                                            <label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "good" ? "checked" : ""}>  Good</label>										
                                            <label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "average" ? "checked" : ""}>  Average</label>
                                            <label> <input type="radio" name="workunderinitiative" ${workunderinitiative === "poor" ? "checked" : ""}>  Poor</label>
                                            </span>
                                            </p>
                                                            </td>
                                          
                                                          </tr>
                        
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                      <tr>
                                                    <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                                                     SECTION - 2 Please answer the following questions</td>
                                                  </tr>
                                      
                                      
                                      <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                        
                                                          <tr>
                                                            <td width="50%" style="vertical-align:top;">
                                                               
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:100%;float:left;font-size: 12px; ">Has the applicant been subject to any disciplinary action?</label>
                                                                
                                            <span style="width:100%;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="disciplinaryaction" ${disciplinaryaction === "yes" ? "checked" : ""}>  Yes</label>
                                            <label> <input type="radio" name="disciplinaryaction" ${disciplinaryaction === "no" ? "checked" : ""}>  No</label>
                                            
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                                                
                                            <textarea type="text" value="${disciplinaryactiondetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${disciplinaryactiondetails || "-"}</textarea>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:100%;float:left;font-size: 12px; ">Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?</label>
                                                                
                                            <span style="width:100%;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="vulnerablepeople" ${vulnerablepeople === "yes" ? "checked" : ""}>  Yes</label>
                                            <label> <input type="radio" name="vulnerablepeople" ${vulnerablepeople === "no" ? "checked" : ""}>  No</label>
                                            
                                            </span>
                                            </p>
                                            
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                                                
                                            <textarea type="text" value="${vulnerablepeopledetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${vulnerablepeopledetails || "-"}</textarea>
                                            </p>
                                            
                                                            </td>
                                                            <td width="50%" style="vertical-align:top;">
                                                              <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:100%;float:left;font-size: 12px; ">Are you aware of the applicants involvement in any safeguarding investigations previous or current .</label>
                                                                
                                            <span style="width:100%;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="investigations" ${investigations === "yes" ? "checked" : ""}>  Yes</label>
                                            <label> <input type="radio" name="investigations" ${investigations === "no" ? "checked" : ""}>  No</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                                                
                                            <textarea type="text" value="${investigationsdetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${investigationsdetails || "-"}</textarea>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:100%;float:left;font-size: 12px; ">To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?</label>
                                                                
                                            <span style="width:100%;font-size: 12px; color:#7b7c80">
                                            <label> <input type="radio" name="criminaloffences" ${form_reference1.criminaloffence || form_reference2.criminaloffence === "yes" ? "checked" : ""}>  Yes</label>
                                            <label> <input type="radio" name="criminaloffences" ${form_reference1.criminaloffence || form_reference2.criminaloffence === "no" ? "checked" : ""}>  No</label>
                                            </span>
                                            </p>
                                            <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:40%;float:left;font-size: 12px; ">Please provide details</label>
                                                                
                                            <textarea type="text" value="${criminaloffencedetails || "-"}" style="width:58%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${criminaloffencedetails || "-"}</textarea>
                                            </p>
                                            
                                                            </td>
                                          
                                                          </tr>
                        
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                        <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                        
                                                          <tr>
                                                            <td width="100%">
                                                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:20%;float:left;font-size: 12px; ">Additional Comments</label>
                                                                
                                            <textarea type="text" value="${additionalcomments || "-"}" style="width:80%;height:100px;border:1px solid #e2e5f1;background-color:#e2e5f1;">${additionalcomments || "-"}</textarea>
                                            </p>
                                            
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                       <tr>
                                                    <td>
                                                      <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                                                        <tbody>
                        
                                                          <tr>
                                                            <td width="100%">
                                                               <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;"> <label
                                                                style="width:20%;float:left;font-size: 12px; ">Official company stamp</label>
                                                                <span style="width:80%;float:left;">
                                            <img src="${`${CONFIG.LIVEURL}/${campanystamp}`}"/>
                                            <br>
                                            </p>
                                            
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                      
                                      
                                      
                                        </tbody>
                                        </table>
                                        </td>
                                        </tr>
                                        </tbody>
                                      </table>`;
                      pdf.create(html, pdfoptions).toFile("./uploads/pdf/onlineform.pdf", function(err, document) {
                        if (err) {
                          return res.send(err);
                        } else {
                          const mailData = {};
                          mailData.template = "refereeformcompleted";
                          mailData.to = AgencyData.company_email;
                          mailData.html = [];
                          mailData.html.push({ name: "agency", value: AgencyData.company_name });
                          mailData.html.push({ name: "employee", value: employeeData.username });
                          mailData.attachments = [
                            {
                              filename: "referenceform.pdf",
                              path: "./uploads/pdf/referenceform.pdf",
                              contentType: "application/pdf"
                            }
                          ];
                          mailcontent.sendmail(mailData, (err, response) => {
                            console.log("mail err,mail response", err, response);
                          });
                        }
                      });
                    } else {
                      const mailData = {};
                      mailData.template = "refereeformcompleted";
                      mailData.to = AgencyData.company_email;
                      mailData.html = [];
                      mailData.html.push({ name: "agency", value: AgencyData.company_name });
                      mailData.html.push({ name: "employee", value: employeeData.username });
                      mailcontent.sendmail(mailData, (err, response) => {
                        console.log("mail err,mail response", err, response);
                      });
                    }
                  });
                }
                if (AgencyData.settings.notifications.sms) {
                  // SMS Send To All
                  const smsto = AgencyData.company_phone.code + AgencyData.company_phone.number;
                  const smsmessage = `Hi ${AgencyData.company_name} , You have got Completed Referee Application from ${employeeData.username}.`;
                  twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                    console.log("SMS err,SMS response", err, response);
                  });
                }
              }
            });
            data.status = 1;
            data.response = "Form Submitted Successfully";
            res.send(data);
          }
        });
      }
    });
  };
  return router;
};
