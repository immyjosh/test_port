const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library");
const attachment = require("../../model/attachments");
const moment = require("moment");
const json2csv = require("json2csv").Parser;
const fs = require("fs");

// const superadminschemas = require("../schema/superadmin.schema");
const nodemailer = require("nodemailer");

module.exports = () => {
  const router = {};

  router.save = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("name", "Invalid Name").notEmpty();
    req.checkBody("freedays", "Invalid Email").notEmpty();
    req.checkBody("noofdays", "No of days required").notEmpty();
    req.checkBody("employees", "Maximum Employee count required").notEmpty();
    req.checkBody("amount", "Invalid Number").notEmpty();
    req.checkBody("description", "Invalid Address").notEmpty();
    req.checkBody("remainder", "Remainder required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    let type;
    if (req.body.freedays == 0) {
      type = 1; // subscription
    } else if (req.body.noofdays == 0) {
      type = 2; // Trial
    } else {
      type = 3; // Trial & Subscribe
    }

    const plan = {
      name: req.body.name,
      freedays: req.body.freedays,
      noofdays: req.body.noofdays,
      amount: req.body.amount,
      description: req.body.description,
      remainder: req.body.remainder,
      type: type,
      status: req.body.status,
      employees: req.body.employees,
      recruitment_module: req.body.recruitment_module,
      availability: req.body.availability
    };

    if (req.body._id) {
      db.UpdateDocument("plans", { _id: req.body._id }, plan, {}, (err, result) => {
        if (err || !result) {
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated successfully.";
          res.send(data);
        }
      });
    } else {
      db.InsertDocument("plans", plan, (err, result) => {
        if (err || !result) {
          data.err = err;
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { status: { $exists: true } } });

    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else if (req.body.status === 0) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });

    }
    if (req.body.availability) {
      userQuery.push({ $match: { availability: { $eq: req.body.availability } } });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { amount: { $regex: searchs + ".*", $options: "si" } },
              { noofdays: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { freedays: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: req.body.from_date } } });
    }

    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({ $project: { name: 1, freedays: 1, noofdays: 1, amount: 1, status: 1, description: 1, employees: 1, createdAt: 1, recruitment_module: 1 } });

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({
      $count: "count"
    });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery,
          dashboard: [
            { $group: { _id: "$status", count: { $sum: 1 } } },
            { $addFields: { status: { $arrayElemAt: [["inactive", "active"], "$_id"] } } },
            { $addFields: { nott: [["$status", "$count"]] } },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ]
        }
      }
    ];

    db.GetAggregation("plans", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.err = err;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          dashboard: docData[0].dashboard
        };
        if (docData[0].overall[0]) {
          data.response.fullcount = docData[0].overall[0].count;
        } else {
          data.response.fullcount = docData[0].result.length || 0;
        }
        res.send(data);
      }
    });
  };

  router.earnings = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { status: { $exists: true } } });

    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { amount: { $regex: searchs + ".*", $options: "si" } },
              { noofdays: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { freedays: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: req.body.from_date } } });
    }

    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({ $project: { name: 1, freedays: 1, noofdays: 1, amount: 1, status: 1, description: 1, planId: 1 } });

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({
      $count: "count"
    });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    userQuery.push(
      {
        $lookup: {
          from: "subscriptions",
          let: { plan_Id: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$planId", "$$plan_Id"] }, { $eq: ["$status", 1] }, { $eq: ["$payment_status", 1] }]
                }
              }
            }
          ],
          as: "members"
        }
      },
      {
        $match: { members: { $ne: [] } }
      },
      { $addFields: { subscriberscount: { $size: "$members" } } },
      { $addFields: { subscribersamount: { $sum: "$members.plan.amount" } } },
      { $project: { members: 0 } }
    );

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];

    db.GetAggregation("plans", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.err = err;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        const subquery = [];
        /* if (req.body.payment_status) {
          subquery.push({ $match: { $or: [{ payment_status: req.body.payment_status }, { status: 1 }] } });
        } else {
          subquery.push({ $match: { $or: [{ payment_status: 1 }, { status: 1 }] } });
        }

        if (req.body.type) {
          subquery.push({ $match: { type: req.body.type } });
        } */
        subquery.push(
          {
            $facet: {
              dashboardcounts: [
                { $match: { status: 1 } },
                { $group: { _id: "$type", count: { $sum: 1 } } },
                {
                  $addFields: {
                    nott: [["$_id", "$count"]]
                  }
                },
                { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
                { $group: { _id: null, res: { $push: "$$ROOT" } } },
                { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
              ],
              earnings: [{ $match: { payment_status: 1 } }, { $group: { _id: "$payment_status", earning: { $sum: "$plan.amount" } } }]
            }
          }
        );
        db.GetAggregation("subscriptions", subquery, (err, subData) => {
          let fullcount = docData[0].result.length;
          if (docData[0].overall[0] && docData[0].overall[0].count) {
            fullcount = docData[0].overall[0].count;
          }
          data.status = 1;
          data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, userQuery: userQuery };
          data.response.dashboardcounts = subData[0].dashboardcounts;
          data.response.earnings = subData[0].earnings;
          // data.response.dashboard = subData[0].dashboard;
          res.send(data);
        });
      }
    });
  };

  router.get = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("plans", { _id: req.body.id }, {}, {}, (err, planData) => {
      if (err || !planData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: planData };
        res.send(data);
      }
    });
  };

  router.subscribers = (req, res) => {
    const data = {};
    data.status = 0;
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const subscribers = [];

    if (req.body.payment_status) {
      subscribers.push({ $match: { $or: [{ payment_status: req.body.payment_status }, { status: 1 }] } });
    } else {
      subscribers.push({ $match: { $or: [{ payment_status: 1 }, { status: 1 }] } });
    }

    if (req.body.type) {
      subscribers.push({ $match: { type: req.body.type } });
    }

    subscribers.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1, company_name: 1, email: 1, phone: 1} }
          ],
          as: "agencyname"
        }
      },
      {
        $addFields: { agency: { $arrayElemAt: ["$agencyname.name", 0] } }
      },
        {
        $addFields: { agency_data: { $arrayElemAt: ["$agencyname", 0] } }
      },
      { $project: { agencyname: 0 } }
    );

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        subscribers.push({
          $match: {
            $or: [
              { amount: { $regex: searchs + ".*", $options: "si" } },
              { noofdays: { $regex: searchs + ".*", $options: "si" } },
              { agency: { $regex: searchs + ".*", $options: "si" } },
              { "agency_data.company_name": { $regex: searchs + ".*", $options: "si" } },
              { "agency_data.email": { $regex: searchs + ".*", $options: "si" } },
              { "agency_data.phone.number": { $regex: searchs + ".*", $options: "si" } },
              { "agency_data.name": { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { freedays: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        subscribers.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      subscribers.push({ $sort: sorting });
    } else {
      subscribers.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      subscribers.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      subscribers.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    const withoutlimit = Object.assign([], subscribers);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      subscribers.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      subscribers.push({ $limit: parseInt(req.body.limit) });
    }

    const finalquery = [
      {
        $facet: {
          dashboardcounts: [
            { $match: { status: 1 } },
            { $group: { _id: "$type", count: { $sum: 1 } } },
            {
              $addFields: {
                nott: [["$_id", "$count"]]
              }
            },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ],
          earnings: [{ $match: { payment_status: 1 } }, { $group: { _id: "$payment_status", earning: { $sum: "$plan.amount" } } }],
          result: subscribers,
          overall: withoutlimit
        }
      }
    ];

    db.GetAggregation("subscriptions", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.err = err;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = docData;

        let fullcount = docData[0].result.length;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        }

        let earnings = 0;
        if (docData[0].earnings[0] && docData[0].earnings[0].earning) {
          earnings = docData[0].earnings[0].earning;
        }

        data.response = { result: docData[0].result, dashboardcounts: docData[0].dashboardcounts, earnings: earnings, fullcount: fullcount };
        res.send(data);
      }
    });
  };

  /* router.userexport = (req, res) => {
    const data = {};
    const bannerQuery = [
      {
        $match: { status: { $exists: true } }
      }
    ];
    db.GetAggregation("plans", bannerQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata.length != 0) {
          const fields = ["createdAt", "freedays", "name", "noofdays", "employees", "amount", "status"];
          const fieldNames = ["Date", "Free Days", "Name", "No Of Days", "Employees Limit", "Amount", "Status"];
          const mydata = docdata;
          for (let i = 0; i < mydata.length; i++) {
            mydata[i].createdAt = moment(mydata[i].createdAt).format("DD/MM/YYYY");
          }
          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(mydata);
          // let filename = "uploads/csv/users-" + new Date().getTime() + ".csv";
          // fs.writeFile(filename, csv, (err, doc) => {
          //   if (err) {
          //     data.status = 0;
          //     data.response = "Exported Failed";
          //     res.send(data);
          //   } else {
          //     res.download(filename);
          //   }
          // });
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */

  router.userexport = (req, res) => {
    const data = {};
    const bannerQuery = [
      {
        $match: { status: { $exists: true } }
      }
    ];
    db.GetAggregation("plans", bannerQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata && docdata.length > 0) {
          const fieldNames = ["Date", "Free Days", "Name", "No Of Days", "Employees Limit", "Amount", "Status"];
          const mydata = docdata;
          let finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            let temp = {};
            temp["Name"] = mydata[i].name || "";
            temp["Trail Period"] = `${mydata[i].freedays} Days` || "0";
            temp["Billing Period"] = `${mydata[i].noofdays} Days` || "0";
            temp["Employee Limit"] = mydata[i].employees || "0";
            temp["Recruitment Module"] = mydata[i].recruitment_module == 1 ? "Yes" : "No";
            temp["Amount"] = (`£ ${mydata[i].amount}`) || "£ 0";
            temp["Created Date"] = mydata[i].createdAt ? (moment(mydata[i].createdAt).format("DD/MM/YYYY")) : "-";
            temp["Status"] = mydata[i].status == 1 ? "Active" : "In-Active";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  router.subscribersexport = (req, res) => {
    const data = {};

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const subscribers = [];
    subscribers.push({ $match: { $or: [{ payment_status: 1 }, { status: 1 }] } });

    subscribers.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "agencyname"
        }
      },
      {
        $addFields: { agency: { $arrayElemAt: ["$agencyname.name", 0] } }
      },
      { $project: { agencyname: 0 } }
    );

    db.GetAggregation("subscriptions", subscribers, (err, docData) => {
      if (err) {
        res.send(err);
      } else {
        if (docData && docData.length > 0) {
          const fieldNames = ["Agency", "Plan", "Amount", "Type", "Start Date", "End Date", "Subscribed Date", "Payment Status"];
          const mydata = docData;
          let finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            let temp = {};
            temp["Agency"] = mydata[i].agency || "";
            temp["Plan"] = mydata[i].plan ? (mydata[i].plan.name || "") : "";
            temp["Amount"] = mydata[i].plan ? (mydata[i].plan.amount ? `£ ${mydata[i].plan.amount}` : "£ 0") : "£ 0";
            temp["Type"] = mydata[i].type || "";
            temp["Start Date"] = mydata[i].start ? moment(mydata[i].start).format("DD/MM/YYYY") : "-";
            temp["End Date"] = mydata[i].end ? moment(mydata[i].end).format("DD/MM/YYYY") : "-";
            temp["Subscribed Date"] = mydata[i].createdAt ? moment(mydata[i].createdAt).format("DD/MM/YYYY") : "-";
            temp["Payment Status"] = mydata[i].payment_status == 1 ? "Paid" : "Trial";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);

          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  /*  router.subscribersexport = (req, res) => {
     const data = {};
 
     const errors = req.validationErrors();
     if (errors) {
       data.response = errors[0].msg;
       return res.send(data);
     }
 
     const subscribers = [];
     // subscribers.push({ "$match": { $or: [{ "payment_status": 1 }, { "status": 1 }] } });
     subscribers.push({ $match: { $or: [{ status: 1 }] } });
 
     subscribers.push(
       {
         $lookup: {
           from: "agencies",
           let: { agencyid: "$agency" },
           pipeline: [
             {
               $match: {
                 $expr: {
                   $eq: ["$_id", "$$agencyid"]
                 }
               }
             },
             { $project: { name: 1 } }
           ],
           as: "agencyname"
         }
       },
       {
         $addFields: { agency: { $arrayElemAt: ["$agencyname.name", 0] } }
       },
       { $project: { agencyname: 0 } }
     );
 
     if (req.body.field && req.body.order) {
       const sorting = {};
       sorting[req.body.field] = parseInt(req.body.order);
       subscribers.push({ $sort: sorting });
     } else {
       subscribers.push({ $sort: { createdAt: -1 } });
     }
 
     db.GetAggregation("subscriptions", subscribers, (err, docData) => {
       if (err) {
         res.send(err);
       } else {
         if (docData.length != 0) {
           const fields = ["createdAt", "agency", "amount", "plan.name", "start", "end", "type", "payment_status"];
           const fieldNames = ["Date", "Agency", "Amount", "Plan", "Start", "End", "Type", "Payment Status"];
 
           for (let i = 0; i < docData.length; i++) {
             docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");
             if (docData[i].payment_status == 1) {
               docData[i].payment_status = "Paid";
             } else if (docData[i].payment_status == 0) {
               docData[i].payment_status = "Trial";
             }
           }
 
           const json2csvParser = new json2csv({ fields, fieldNames });
           const csv = json2csvParser.parse(docData);
 
           // let filename = "uploads/csv/users-" + new Date().getTime() + ".csv";
           // fs.writeFile(filename, csv, (err) => {
           //   if (err) {
           //     data.status = 0;
           //     data.response = "Export Failed";
           //     res.send(data);
           //   } else {
           //     res.download(filename);
           //   }
           // });
           if (csv) {
             data.status = 1;
             data.response = csv;
             res.send(data);
           } else {
             data.status = 0;
             data.response = "Export Failed";
             res.send(data);
           }
         } else {
           res.send([0, 0]);
         }
       }
     });
   }; */

  router.earningsexport = (req, res) => {
    const data = {};

    const earningQuery = [];

    earningQuery.push({ $match: { status: { $exists: true } } });
    earningQuery.push({ $project: { name: 1, freedays: 1, noofdays: 1, amount: 1, status: 1, description: 1, planId: 1 } });

    earningQuery.push(
      {
        $lookup: {
          from: "subscriptions",
          let: { plan_Id: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$planId", "$$plan_Id"] }, { $eq: ["$status", 1] }]
                }
              }
            }
          ],
          as: "members"
        }
      },
      {
        $match: { members: { $ne: [] } }
      },
      { $addFields: { subscriberscount: { $size: "$members" } } },
      { $addFields: { subscribersamount: { $sum: "$members.plan.amount" } } },
      { $project: { members: 0 } }
    );

    db.GetAggregation("plans", earningQuery, (err, docData) => {
      if (err) {
        res.send(err);
      } else {
        if (docData && docData.length > 0) {
          const fieldNames = ["Name", "Trial Period", "Billing Period", "Amount", "Subscribers", "Earnings"];
          const mydata = docData;
          let finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            let temp = {};
            temp["Name"] = mydata[i].name || "";
            temp["Trail Period"] = `${mydata[i].freedays} Days` || "0";
            temp["Billing Period"] = `${mydata[i].noofdays} Days` || "0";
            temp["Amount"] = `£ ${mydata[i].amount}` || "£ 0";
            temp["Subscribers"] = mydata[i].subscriberscount || "0";
            temp["Earnings"] = `£ ${mydata[i].subscribersamount}` || "£ 0";
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);

          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  return router;
};
