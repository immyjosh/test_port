const express = require("express");
const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const library = require("../../model/library");
const fs = require("fs");
const path = require("path");
const multer = require("multer");
let attachment = require("../../model/attachments.js");

module.exports = () => {
  const router = {};

  let data = {};
  data.status = 0;

  router.list = (req, res) => {
    req.checkParams("name", "Invalid Setting").notEmpty();

    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("settings", {alias: req.params.name}, {}, {}, (err, docdata) => {
      if (err || !docdata || docdata.length <= 0) {
        data.response = "Unable to get " + req.params.name + " settings. Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = {settings: docdata[0].settings};
        res.send(data);
      }
    });
  };

  router.save = (req, res) => {
    req.checkParams("name", " Invalid Setting").notEmpty();

    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let setdata = {};
    setdata.settings = req.body.settings;

    db.UpdateDocument("settings", {alias: req.params.name}, setdata, {}, (err, docdata) => {
      if (err) {
        data.response = "Settings Cannot be updated...";
        res.send(data);
      } else {
        if (req.params.name == "general") {
          let config = JSON.parse(fs.readFileSync(path.join(__dirname, "../../config/config.json"), "utf8"));
          config.settings = data.settings;
          fs.writeFile(path.join(__dirname, "../../config/config.json"), JSON.stringify(config, null, 4), (err, respo) => {
            if (err) {
            } else {
            }
          });
        }
        data.status = 1;
        data.response = "Name Settings updated successfully...";
        res.send(data);
      }
    });
  };

  router.generalsave = (req, res) => {
    let data = {};
    data.status = 0;

    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    let generaldata = {};
    generaldata.settings = req.body.setting;

    if (req.files && typeof req.files.logo != "undefined") {
      if (req.files.logo.length > 0) {
        generaldata.settings.logo = attachment.get_attachment(req.files.logo[0].destination, req.files.logo[0].filename);
      }
    } else {
      generaldata.settings.logo = req.body.logo;
    }

    if (req.files && typeof req.files.favicon != "undefined") {
      if (req.files.favicon.length > 0) {
        generaldata.settings.favicon = attachment.get_attachment(req.files.favicon[0].destination, req.files.favicon[0].filename);
      }
    } else {
      generaldata.settings.favicon = req.body.favicon;
    }

    db.UpdateDocument("settings", {alias: "general"}, generaldata, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "Settings Cannot be updated...";
        res.send(data);
      } else {
        let config = JSON.parse(fs.readFileSync(path.join(__dirname, "../../config/config.json"), "utf8"));
        config.settings = data.settings;
        fs.writeFile(path.join(__dirname, "../../config/config.json"), JSON.stringify(config, null, 4), (err, respo) => {
          if (err) {
          } else {
          }
        });

        data.status = 1;
        data.response = "general Settings updated successfully...";
        res.send(data);
      }
    });
  };

  router.smtpsave = (req, res) => {
    let smtpdata = {};
    smtpdata.settings = req.body.settings;
    db.UpdateDocument("settings", {alias: "smtp"}, smtpdata, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "settings cannot be updated";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "smtp settings successfully updated";
        res.send(data);
      }
    });
  };

  router.saveseo = (req, res) => {
    let seodata = {};
    seodata.settings = req.body.settings;

    db.UpdateDocument("settings", {alias: "seo"}, seodata, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "Settings cannot be updated";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Seo Settings Successfully Updated";
        res.send(data);
      }
    });
  };

  router.savesms = (req, res) => {
    let smsdata = {};
    smsdata.settings = req.body.settings;

    db.UpdateDocument("settings", {alias: "sms"}, smsdata, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "Settings Cannot be Updated";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Sms Settings Successfully Updated";
        res.send(data);
      }
    });
  };

  router.savesocial_networks = (req, res) => {
    let socialdata = {};
    socialdata.settings = req.body.settings;

    db.UpdateDocument("settings", {alias: "social_networks"}, socialdata, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "Settings Cannot be Updated";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Social networks updated successfully";
        res.send(data);
      }
    });
  };
  router.savepayment_gateway = (req, res) => {
    let Data = {$set: {"settings.stripe": req.body.settings}};
    db.UpdateDocument("settings", {alias: "payment_gateway"}, Data, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "Settings Cannot be Updated";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Payment Gateway updated successfully";
        res.send(data);
      }
    });
  };

  router.savemobile = (req, res) => {
    let mobiledata = {};
    mobiledata.settings = req.body.settings;

    db.UpdateDocument("settings", {alias: "mobile"}, mobiledata, {upsert: true}, (err, docdata) => {
      if (err) {
        data.response = "Settings Cannot be Updated";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Mobile Settings Updated Successfully";
        res.send(data);
      }
    });
  };

  router.timezones = (req, res) => {
    let timezone = require("moment-timezone");
    let timezoneData = timezone.tz.names();
    res.send(timezoneData);
  };

  return router;
};
