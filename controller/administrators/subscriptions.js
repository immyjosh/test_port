const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library.js");
const nodemailer = require("nodemailer");
const dashboard = require("../../model/dashboard.js");
const stripe = require("stripe")("");
const moment = require("moment");
const mongoose = require("mongoose");
const mail = require("../../model/mail.js");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const pdf = require("html-pdf");
const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };
module.exports = () => {
  const router = {};
  const data = {};
  data.status = 0;

  router.subscribe = (req, res) => {

    req.checkBody("id", "ID Required").notEmpty();
    req.checkBody("planid", "Select a plan").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("plans", { _id: req.body.planid }, {}, {}, (err, planDatas) => {
      if (err) {
        data.response = "The plan is currently not available.";
        res.send(err);
      } else {
        const planData = JSON.parse(JSON.stringify(planDatas));
        const subscriptions = {
          agency: req.body.id,
          plan: planData,
          planId: planData._id
        };

        subscriptions.start = moment(Date.now());

        if (planData.freedays > 0) {
          subscriptions.end = moment(subscriptions.start).add(planData.freedays, "days");
        } else {
          subscriptions.end = moment(subscriptions.start).add(planData.noofdays, "days");
        }

        subscriptions.remainder = moment(Date.now()).add(planData.remainder, "days");
        subscriptions.payment_status = 0;

        if (parseInt(planData.freedays) > 0) {
          subscriptions.type = "trial";
          subscriptions.status = 1;
          var auth_token = library.jwtSign({
            username: req.params.loginData.username,
            role: "agency",
            subscription: 1,
            employee_count: planData.employees,
            recruitment_module: planData.recruitment_module
          });
          data.auth_token = auth_token;
          data.auth = { username: req.params.loginData.username, role: "agency", subscription: 1 };
        } else {
          /* req.checkBody('card_number','Card details required').notEmpty();
                    req.checkBody('exp_month','Card details required').notEmpty();
                    req.checkBody('exp_year','Card details required').notEmpty();
                    req.checkBody('cvc','Card details required').notEmpty();
                    var error = req.validationErrors();
                    if(error){
                        data.response = error[0].msg;
                        return res.send(data);
                    }*/
          subscriptions.type = "subscription";
          subscriptions.status = 0;
          var auth_token = library.jwtSign({ username: req.params.loginData.username, role: "agency", subscription: 0, employee_count: 0, recruitment_module: 0 });
          data.auth_token = auth_token;
        }

        db.InsertDocument("subscriptions", subscriptions, (err, result) => {
          if (err || !result) {
            data.response = "Unable to save your data, Please try again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = { result: result };
            res.send(data);
          }
        });
      }
    });
  };

  router.subscriptions = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const planQuery = [];
    planQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    planQuery.push({ $match: { status: { $gt: 0 } } });

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      planQuery.push({ $sort: sorting });
    } else {
      planQuery.push({ $sort: { end: -1 } });
    }

    const withoutlimit = Object.assign([], planQuery);
    withoutlimit.push({ $count: "count" });

    const current = Object.assign([], planQuery);
    current.push({ $match: { status: { $eq: 1 } } });
    current.push({ $limit: 1 });
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        planQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { "plan.name": { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        planQuery.push({ $match: searching });
      }
    }
    if (req.body.skip >= 0) {
      planQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      planQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          current: current,
          result: planQuery
        }
      }
    ];

    db.GetAggregation("subscriptions", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.err = err;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "agency", agency: new mongoose.Types.ObjectId(req.body.id), action: req.body.notification }, function(err, response) {});
        }
        data.status = 1;
        data.response = {};
        data.response.history = docData[0].result;
        data.response.current = docData[0].current;
        data.response.fullcount = 0;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          data.response.fullcount = docData[0].overall[0].count;
        }
        res.send(data);
      }
    });
  };

  router.accountDetails = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("settings", { alias: "payment_gateway" }, {}, {}, (err, paymentgateway) => {
      if (err || !paymentgateway.settings.stripe.secret_key) {
        res.status(400).send({ message: "Invalid payment method, Please contact the website administrator" });
      } else {
        stripe.setApiKey(paymentgateway.settings.stripe.secret_key);
        const project = { _id: 1, customer_id: 1, company_phone: 1, company_name: 1, postal_address: 1, company_email: 1, auto_renewal: 1 };
        db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id) }, project, {}, (err, agencyData) => {
          if (err || !agencyData) {
            data.response = "Agency Data Not Available";
            res.send(data);
          } else {
            if (agencyData && agencyData.customer_id) {
              stripe.customers.listCards(agencyData.customer_id, function(err, listCards) {
                if (err || !listCards) {
                  data.status = 0;
                  data.response = "Unable to Get Your Card Details, Please try Again";
                  res.send(data);
                } else {
                  data.status = 1;
                  data.response = { result: listCards, agency: agencyData };
                  res.send(data);
                }
              });
            } else {
              data.status = 1;
              data.response = { result: [], agency: agencyData };
              res.send(data);
            }
          }
        });
      }
    });
  };

  /*  router.edit = (req,res)=>{
        req.checkBody('id','Invalid id').notEmpty();
        var error = req.validationErrors();
        if(error){
            data.response = error[0].msg;
            return res.send(data);
        }
        db.GetDocument('subscriptions',{'_id':req.body.id},{},{},function(err, subscriptionData){
            if(err || !subscriptionData){
                data.response = 'Unable to get Your Data Please try Again';
                res.send(data);
            }else{
                data.status = 1;
                data.response = {'result': subscriptionData};
                res.send(data);
            }
        });
    };*/

  router.payment = (req, res) => {
    req.checkBody("id", "ID required").notEmpty();
    req.checkBody("subid", "Subscription id required").notEmpty();
    req.checkBody("plandetails", "Plandetails required").notEmpty();
    req.checkBody("account_details", "Plandetails required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    const plandetails = req.body.plandetails;
    const account = req.body.account_details;
    const metadata = {
      name: account.name,
      email: account.email,
      address: account.address,
      code: account.code,
      number: account.number,
      plans: plandetails._id,
      planname: plandetails.name,
      planamount: plandetails.amount,
      planemployee: plandetails.employees,
      plannoofdays: plandetails.noofdays,
      planfreedays: plandetails.freedays,
      plandesc: plandetails.description,
      planremainder: plandetails.remainder,
      planrecruitment_module: plandetails.recruitment_module,
      subid: req.body.subid
    };
    db.GetOneDocument("settings", { alias: "payment_gateway" }, {}, {}, (err, paymentgateway) => {
      if (err || !paymentgateway.settings.stripe.secret_key) {
        res.status(400).send({ message: "Invalid payment method, Please contact the website administrator" });
      } else {
        stripe.setApiKey(paymentgateway.settings.stripe.secret_key);
        if (req.body.payload) {
          const payload = req.body.payload;
          if (req.body.auto_save) {
            db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, agencyData) => {
              if (err || !agencyData) {
                data.response = "Agency Data Not Available";
                res.send(data);
              } else {
                if (!agencyData.customer_id || agencyData.customer_id === "") {
                  stripe.customers.create({ description: agencyData.username + " " + agencyData.email, source: payload.id }, (err, customer) => {
                    if (err || !customer) {
                      data.status = 0;
                      data.response = "Error in adding Card";
                      res.send(data);
                    } else {
                      db.UpdateDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id) }, { customer_id: customer.id }, { upsert: true }, (err, response) => {
                        if (err || response.nModified == 0) {
                          data.status = 0;
                          data.response = "Error in adding Card";
                          res.send(data);
                        } else {
                          stripe.customers.updateCard(customer.id, customer.default_source, { metadata }, (err, card) => {
                            if (err || !card) {
                              data.status = 0;
                              data.response = "Error in adding Card";
                              res.send(data);
                            } else {
                              const charge = {};
                              charge.amount = parseInt(plandetails.amount) * 100;
                              charge.currency = "gbp";
                              charge.customer = customer.id;
                              charge.source = customer.default_source;
                              charge.description = "Plan Subscription - ".subid;
                              const end = moment(Date.now()).add(plandetails.noofdays, "days");
                              stripe.charges.create(charge, (err, charges) => {
                                if (err || !charges) {
                                  data.status = 0;
                                  data.response = "Unable to capture amount from your card";
                                  data.err = err;
                                  res.send(data);
                                } else {
                                  if (charges) {
                                    const up_date = { payment_status: 1, payment_response: charges.id, status: 1, end: end };
                                    db.UpdateDocument("subscriptions", { _id: req.body.subid }, up_date, {}, (err, result) => {
                                      let auth_token;
                                      if (err) {
                                        data.status = 0;
                                        auth_token = library.jwtSign({
                                          username: req.params.loginData.username,
                                          role: "agency",
                                          subscription: 0,
                                          employee_count: 0,
                                          recruitment_module: 0
                                        });
                                        data.auth_token = auth_token;
                                        data.auth = { username: req.params.loginData.username, role: "agency", subscription: 0 };
                                        data.response = "Unable to Save Your data Please try Again";
                                        data.err = err;
                                        res.send(data);
                                      } else {
                                        auth_token = library.jwtSign({
                                          username: req.params.loginData.username,
                                          role: "agency",
                                          subscription: 1,
                                          employee_count: plandetails.employees,
                                          recruitment_module: plandetails.recruitment_module
                                        });
                                        data.auth = { username: req.params.loginData.username, role: "agency", subscription: 1 };
                                        data.auth_token = auth_token;
                                        data.status = 1;
                                        data.response = "Successfully Subscribed";
                                        res.send(data);
                                      }
                                    });
                                  }
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                } else {
                  stripe.customers.createSource(agencyData.customer_id, { source: payload.id }, (err, card) => {
                    if (err || !card) {
                      data.status = 0;
                      data.response = "Error in adding Card";
                      res.send(data);
                    } else {
                      stripe.customers.updateCard(agencyData.customer_id, card.id, { metadata }, (err, card) => {
                        if (err || !card) {
                          data.status = 0;
                          data.response = "Error in adding Card";
                          res.send(data);
                        } else {
                          const charge = {};
                          charge.amount = parseInt(plandetails.amount) * 100;
                          charge.currency = "gbp";
                          charge.customer = agencyData.customer_id;
                          charge.source = card.id;
                          charge.description = "Plan Subscription - ".subid;
                          const end = moment(Date.now()).add(plandetails.noofdays, "days");
                          stripe.charges.create(charge, (err, charges) => {
                            if (err || !charges) {
                              data.status = 0;
                              data.response = "Unable to capture amount from your card";
                              data.err = err;
                              res.send(data);
                            } else {
                              if (charges) {
                                const sub_update = { payment_status: 1, payment_response: charges.id, status: 1, end: end, account_details: req.body.account_details };
                                db.UpdateDocument("subscriptions", { _id: req.body.subid }, sub_update, {}, (err, result) => {
                                  let auth_token;
                                  if (err) {
                                    data.status = 0;
                                    auth_token = library.jwtSign({
                                      username: req.params.loginData.username,
                                      role: "agency",
                                      subscription: 0,
                                      employee_count: 0,
                                      recruitment_module: 0
                                    });
                                    data.auth_token = auth_token;
                                    data.auth = { username: req.params.loginData.username, role: "agency", subscription: 0 };
                                    data.response = "Unable to Save Your data Please try Again";
                                    data.err = err;
                                    res.send(data);
                                  } else {
                                    auth_token = library.jwtSign({
                                      username: req.params.loginData.username,
                                      role: "agency",
                                      subscription: 1,
                                      employee_count: plandetails.employees,
                                      recruitment_module: plandetails.recruitment_module
                                    });
                                    data.auth = { username: req.params.loginData.username, role: "agency", subscription: 1 };
                                    data.auth_token = auth_token;
                                    data.status = 1;
                                    data.response = "Successfully Subscribed";
                                    res.send(data);
                                  }
                                });
                              }
                            }
                          });
                        }
                      });
                    }
                  });
                }
              }
            });
          } else if (req.body.payaccount) {
            db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, (err, agencyData) => {
              if (err || !agencyData) {
                data.response = "Agency Data Not Available";
                res.send(data);
              } else {
                const charge = {};
                charge.amount = parseInt(plandetails.amount) * 100;
                charge.currency = "gbp";
                charge.customer = agencyData.customer_id;
                charge.source = payload.id;
                charge.description = "Plan Subscription - ".subid;
                const end = moment(Date.now()).add(plandetails.noofdays, "days");
                stripe.charges.create(charge, (err, charges) => {
                  if (err || !charges) {
                    data.status = 0;
                    data.response = "Unable to capture amount from your card";
                    data.err = err;
                    res.send(data);
                  } else {
                    if (charges) {
                      const sub_update = { payment_status: 1, payment_response: charges.id, status: 1, end: end, account_details: req.body.account_details };
                      db.UpdateDocument("subscriptions", { _id: req.body.subid }, sub_update, {}, (err, result) => {
                        let auth_token;
                        if (err) {
                          data.status = 0;
                          auth_token = library.jwtSign({ username: req.params.loginData.username, role: "agency", subscription: 0, employee_count: 0, recruitment_module: 0 });
                          data.auth_token = auth_token;
                          data.auth = { username: req.params.loginData.username, role: "agency", subscription: 0 };
                          data.response = "Unable to Save Your data Please try Again";
                          data.err = err;
                          res.send(data);
                        } else {
                          auth_token = library.jwtSign({
                            username: req.params.loginData.username,
                            role: "agency",
                            subscription: 1,
                            employee_count: plandetails.employees,
                            recruitment_module: plandetails.recruitment_module
                          });
                          data.auth = { username: req.params.loginData.username, role: "agency", subscription: 1 };
                          data.auth_token = auth_token;
                          data.status = 1;
                          data.response = "Successfully Subscribed";
                          res.send(data);
                        }
                      });
                    }
                  }
                });
              }
            });
          } else {
            const payload = req.body.payload;
            const charge = {};
            charge.amount = parseInt(plandetails.amount) * 100;
            charge.currency = "gbp";
            charge.source = payload.id;
            charge.description = "Plan Subscription - ".subid;
            const end = moment(Date.now()).add(plandetails.noofdays, "days");
            stripe.charges.create(charge, (err, charges) => {
              if (err || !charges) {
                data.status = 0;
                data.response = "Unable to capture amount from your card";
                data.err = err;
                res.send(data);
              } else {
                if (charges) {
                  const subs_update = { payment_status: 1, payment_response: charges.id, status: 1, end: end, account_details: req.body.account_details };
                  db.UpdateDocument("subscriptions", { _id: req.body.subid }, subs_update, {}, (err, result) => {
                    let auth_token;
                    if (err) {
                      data.status = 0;
                      auth_token = library.jwtSign({ username: req.params.loginData.username, role: "agency", subscription: 0, employee_count: 0, recruitment_module: 0 });
                      data.auth_token = auth_token;
                      data.auth = { username: req.params.loginData.username, role: "agency", subscription: 0 };
                      data.response = "Unable to Save Your data Please try Again";
                      data.err = err;
                      res.send(data);
                    } else {
                      auth_token = library.jwtSign({
                        username: req.params.loginData.username,
                        role: "agency",
                        subscription: 1,
                        employee_count: plandetails.employees,
                        recruitment_module: plandetails.recruitment_module
                      });
                      data.auth = { username: req.params.loginData.username, role: "agency", subscription: 1 };
                      data.auth_token = auth_token;
                      data.status = 1;
                      data.response = "Successfully Subscribed";
                      res.send(data);
                    }
                  });
                }
              }
            });
          }
        } else {
          data.status = 1;
          data.response = { admin_publishable_key: paymentgateway.settings.stripe.publishable_key };
          res.send(data);
        }
      }
    });
  };

  router.subscriptionexport = function(req, res) {
    const data = {};

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const historyQuery = [];

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      historyQuery.push({ $sort: sorting });
    } else {
      historyQuery.push({ $sort: { end: -1 } });
    }

    db.GetAggregation("subscriptions", historyQuery, (err, docData) => {
      if (err) {
        res.send(err);
      } else {
        if (docData.length != 0) {
          const fields = ["createdAt", "plan.name", "plan.freedays", "plan.noofdays", "plan.amount", "start", "end", "type"];
          const fieldNames = ["Date", "Name", "Trial Period", "Billing Period", "Amount", "Start Date", "End Date", "Type"];

          for (let i = 0; i < docData.length; i++) {
            docData[i].createdAt = moment(docData[i].createdAt).format("DD/MM/YYYY");

            if (docData[i].type == 0) {
              docData[i].type == "Subscription";
            } else if (docData[i].type == 1) {
              docData[i].type == "Trial";
            }
          }

          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(docData);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  router.downloadpdf = (req, res) => {
    db.GetOneDocument("settings", { alias: "general" }, {}, {}, function(err, setting) {
      if (err) {
        data.status = 0;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        const settings = setting.settings;
        const data = {};
        data.status = 0;
        req.checkBody("id", "ID Required").notEmpty();
        const errors = req.validationErrors();
        if (errors) {
          data.response = errors[0].msg;
          return res.send(data);
        }

        const planQuery = [];
        planQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.id)} } });

        if (req.body.field && req.body.order) {
          const sorting = {};
          sorting[req.body.field] = parseInt(req.body.order);
          planQuery.push({ $sort: sorting });
        } else {
          planQuery.push({ $sort: { end: -1 } });
        }
        planQuery.push({
          $lookup: {
            from: "agencies",
            let: { agencyid: "$agency" },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ["$_id", "$$agencyid"]
                  }
                }
              },
              { $project: { company_name: 1, company_email: 1, postal_address: 1 } }
            ],
            as: "agency_data"
          }
        });
        const withoutlimit = Object.assign([], planQuery);
        withoutlimit.push({ $count: "count" });

        const current = Object.assign([], planQuery);
        current.push({ $match: { status: { $eq: 1 } } });
        current.push({ $limit: 1 });

        if (req.body.skip >= 0) {
          planQuery.push({ $skip: parseInt(req.body.skip) });
        }
        if (req.body.limit >= 0) {
          planQuery.push({ $limit: parseInt(req.body.limit) });
        }

        const finalquery = [
          {
            $facet: {
              overall: withoutlimit,
              current: current,
              result: planQuery
            }
          }
        ];

        db.GetAggregation("subscriptions", finalquery, (err, docData) => {
          if (err || !docData || docData.length <= 0) {
            data.err = err;
            data.response = "Unable to get your data, Please try again";
            res.send(data);
          } else {
            if (req.body.page === "single") {
              const current = docData[0].current && docData[0].current[0];
              const html = `<style>html {
zoom: 0.55;
}</style><table style="margin: 0; padding: 0; color: #000; background: #fff; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;"
       border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>
                <tr>
                    <td>
                        <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;    border-bottom: 1px solid #ddd;">
                            <tbody>
                            <tr>
                                <td align="left"
                                    style="width: 50%;font-size: 15px;    font-weight: bold;text-transform: uppercase;">
                                    TAX INVOICE
                                </td>
                                <td align="right" style="width: 50%;">
                                    <img src="${CONFIG.LIVEURL}/${settings.logo}" width="240">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>
                            <tr>
                                <td style="width: 50%; vertical-align: top;">
                                    <p style="padding: 10px 0;margin:0; width:100%; float:left;"><span
                                            style="font-size: 15px;    font-weight: bold;color: #000;width:40%;float:left;">To</span><span
                                            style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;display:table;">${current.account_details.name}, ${
                current.account_details.address
              }</span>
                                    </p>
                                    <!--<p style="padding: 10px 0;margin:0 ;width:100%; float:left;"><span style="font-size: 15px;    font-weight: bold;color: #000;width:40%;float:left;">From Date</span><span style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;display:table;">1 November 2017</span></p>
                                    <p style="padding: 10px 0;margin:0; width:100%; float:left;"><span style="font-size: 15px;    font-weight: bold;color: #000;width:40%;float:left;">To Date</span><span style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;display:table;">1 November 2018</span></p>
                                    <p style="padding: 10px 0;margin:0; width:100%; float:left;"><span style="font-size: 15px;    font-weight: bold;color: #000;width:40%;float:left;">As At</span><span style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;display:table;">1 November 2018</span></p>-->


                                </td>
                                <td style="width: 50%; vertical-align: top;">
                                    <p style="padding: 10px 0;margin:0 ;width:100%;float:left;"><span
                                            style="font-size: 15px;    font-weight: bold;    color: #000;width:40%;float:left;">From</span><span
                                            style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;float:left;">${settings.siteaddress}.</span>
                                    </p>
                                    <!--<p style="padding: 10px 0;margin:0; width:100%;float:left;"><span style="font-size: 15px;    font-weight: bold;    color: #000;width:40%;float:left;">VAT Number</span><span style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;float:left;">80157985</span></p>-->
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <thead>
                            <tr>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Name
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Trial Period
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Billing Period
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Employees Limit
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Amount
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Date
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: right;">
                                    Type
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${current.plan.name}</td>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;">
        <a style="    color: #00adf1; text-decoration:none;" href="#">
            ${current.plan.freedays} Days
        </a>
      </td>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${current.plan.noofdays=== 30? "Monthly":current.plan.noofdays=== 365? "Yearly":""} Days</td>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${current.plan.employees}</td>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">£      ${current.plan.amount}</td>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${moment(current.start).format("DD-MM-YYYY")} - ${moment(
                current.end
              ).format("DD-MM-YYYY")}</td>
      <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: right; color: #545151;">${current.type}</td>
    </tr>
                            
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <thead>
                            <tr>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                    Description
                                </th>
                                <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: right;">
                                    Amount Amount
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">
                                    ${current.plan.description}
                                </td>
                                <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: right;color: #545151;">
                                £    ${current.plan.amount}
                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>   `;
              if (req.body.for === "download") {
                pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
              } else {
                pdf.create(html, pdfoptions).toFile("./uploads/pdf/statement.pdf", (err, document) => {
                  if (err) {
                    res.send(err);
                  } else {
                    data.status = 1;
                    data.msg = "Mail Sent";
                    res.send(data);
                    const mailOptions = {
                      from: current.account_details.email,
                      to: current.account_details.email,
                      subject: "Statements",
                      text: "Please Download the attachment to see Statement",
                      html: "<b>Please Download the attachment to see Statement</b>",
                      attachments: [
                        {
                          filename: "statement.pdf",
                          path: "./uploads/pdf/statement.pdf",
                          contentType: "application/pdf"
                        }
                      ]
                    };
                    mail.send(mailOptions, (err, response) => {});
                  }
                });
              }
            } else if (req.body.page === "statement") {
              const history = docData && docData[0].result;
              const agency = history && history[0].agency_data[0];
              const subscribedlistTotal = history.map(list => Number(list.plan.amount)).reduce((a, b) => a + b);
              const html = `<style>html {
zoom: 0.55;
}</style><table style="margin: 0; padding: 0; color: #000; background: #eee; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;"
              border="0" width="100%" cellspacing="0" cellpadding="0">
               <tbody>
                 <tr>
                   <td>
                     <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                       <tbody>
                         <tr height="10">
                           &nbsp;
                         </tr>
                         <tr>
                           <td>
                             <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;    border-bottom: 1px solid #ddd;">
                               <tbody>
                                 <tr>
                                   <td align="left" style="width: 50%;font-size: 15px;    font-weight: bold;text-transform: uppercase;">
                                     STATEMENT - <span style=" font-size: 14px; font-weight: normal; text-transform: capitalize;">Invoices</span>
                                   </td>
                                   <td align="right" style="width: 50%;">
                                     <img src="${CONFIG.LIVEURL}/${settings.logo}" width="240">
                                   </td>
                                 </tr>
                               </tbody>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td height="10">&nbsp;</td>
                         </tr>
                         <tr>
                           <td>
                             <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                               <tbody>
                                 <tr>
                                   <td style="width: 50%; vertical-align: top;">
                                     <p style="padding: 10px 0;margin:0; width:100%; float:left;"><span style="font-size: 15px;    font-weight: bold;color: #000;width:40%;float:left;">To</span><span
                                        style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;display:table;">${agency.company_name}, ${
                agency.postal_address
              }</span></p>
                                   </td>
                                   <td style="width: 50%; vertical-align: top;">
                                     <p style="padding: 10px 0;margin:0 ;width:100%;float:left;"><span style="font-size: 15px;    font-weight: bold;    color: #000;width:40%;float:left;">From</span><span
                                        style=" font-size: 15px; color: #5f5c5c; line-height: 23px;width:50%;float:left;">${settings.siteaddress}.</span></p>
                                   </td>
                                 </tr>
                               </tbody>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td>
                             <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                               <thead>
                                 <tr>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;"> Name
                                   </th>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                     Trial Period </th>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                     Billing Period </th>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;"> Due
                                     Employees Limit </th>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;"> Invoice
                                    Date  </th>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: left;">
                                     Type </th>
                                   <th style="padding: 10px 5px; font-size: 14px; border-bottom: 1px solid #ddd;    text-align: right;">
                                   Amount </th>
                                 </tr>
                               </thead>
                               <tbody>
                                 ${history
                                   .map(
                                     list =>
                                       `<tr>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">
                                    ${list.plan.name}</td>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left;color: #545151;">
                                    <a style="    color: #00adf1; text-decoration:none;" href="#"> ${list.plan.freedays} Days</a> </td>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;"> ${list.plan.noofdays=== 30? "Monthly":list.plan.noofdays=== 365? "Yearly":""}  Days</td>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">
                                    ${list.plan.employees}</td>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${moment(current.start).format(
                                    "DD-MM-YYYY"
                                  )} - ${moment(current.end).format("DD-MM-YYYY")}</td>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: left; color: #545151;">${list.type}</td>
                                  <td style="padding: 10px 5px; font-size: 13px; border-bottom: 1px solid #ddd;text-align: right; color: #545151;">£  ${list.plan.amount}</td>
                                </tr>`
                                   )
                                   .join(" ")}
                               </tbody>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td>
                             <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                               <tbody>
                                 <tr>
                                   <td width="80%" align="right" style="font-size:17px;color:#000;font-weight:bold;padding: 6px 0;">
                                     Total Amount
                                   </td>
                                   <td width="20%" align="right" style="font-size:17px;color:#000;font-weight:bold;padding: 6px 0;">
                                   £   ${subscribedlistTotal}
                                   </td>
                                 </tr>
                               </tbody>
                             </table>
                           </td>
                         </tr>
                       </tbody>
                     </table>
                   </td>
                 </tr>
               </tbody>
             </table>`;
              if (req.body.for === "download") {
                pdf.create(html, pdfoptions).toStream((err, stream) => stream.pipe(res));
              } else {
                pdf.create(html, pdfoptions).toFile("./uploads/pdf/statements.pdf", (err, document) => {
                  if (err) {
                    res.send(err);
                  } else {
                    data.status = 1;
                    data.msg = "Mail Sent";
                    res.send(data);
                    const mailOptions = {
                      from: agency && agency.company_email,
                      to: agency && agency.company_email,
                      subject: "Statements",
                      text: "Please Download the attachment to see Statements",
                      html: "<b>Please Download the attachment to see Statements</b>",
                      attachments: [
                        {
                          filename: "statements.pdf",
                          path: "./uploads/pdf/statements.pdf",
                          contentType: "application/pdf"
                        }
                      ]
                    };
                    mail.send(mailOptions, (err, response) => {});
                  }
                });
              }
            }
          }
        });
      }
    });
  };
  router.DeleteAccount = (req, res) => {
    req.checkBody("agencyid", "Agency Id required").notEmpty();
    req.checkBody("id", "Id required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("settings", { alias: "payment_gateway" }, {}, {}, (err, paymentgateway) => {
      if (err || !paymentgateway.settings.stripe.secret_key) {
        res.status(400).send({ message: "Invalid payment method, Please contact the website administrator" });
      } else {
        stripe.setApiKey(paymentgateway.settings.stripe.secret_key);
        db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.agencyid) }, {}, {}, (err, agencyData) => {
          if (err || !agencyData) {
            data.response = "Agency Data Not Available";
            res.send(data);
          } else {
            stripe.customers.deleteCard(agencyData.customer_id, req.body.id, (err, confirmation) => {
              if (err || !confirmation) {
                data.status = 0;
                data.response = "Unable to delete your card";
                res.send(data);
              } else {
                data.status = 1;
                data.response = confirmation;
                res.send(data);
              }
            });
          }
        });
      }
    });
  };
  router.UpdateAccount = (req, res) => {
    req.checkBody("agencyid", "Id required").notEmpty();
    req.checkBody("id", "Id required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("settings", { alias: "payment_gateway" }, {}, {}, (err, paymentgateway) => {
      if (err || !paymentgateway.settings.stripe.secret_key) {
        res.status(400).send({ message: "Invalid payment method, Please contact the website administrator" });
      } else {
        stripe.setApiKey(paymentgateway.settings.stripe.secret_key);
        db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.agencyid) }, {}, {}, (err, agencyData) => {
          if (err || !agencyData) {
            data.response = "Agency Data Not Available";
            res.send(data);
          } else {
            stripe.customers.update(agencyData.customer_id, { default_source: req.body.id }, (err, customer) => {
              if (err || !customer) {
                data.status = 0;
                data.response = "Unable to Update your card";
                res.send(data);
              } else {
                data.status = 1;
                data.response = customer;
                res.send(data);
              }
            });
          }
        });
      }
    });
  };

  router.Autorenewal = (req, res) => {
    req.checkBody("id", "ID required").notEmpty();
    req.checkBody("value", "Value required").notEmpty();
    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }
    db.UpdateDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id) }, { auto_renewal: Number(req.body.value) }, (err, resultss) => {
      if (err || !resultss) {
        data.response = "Unable to Update your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        if (Number(req.body.value) === 1) {
          data.response = "Auto Renewal Activated";
        } else {
          data.response = "Auto Renewal Deactivated";
        }
        res.send(data);
      }
    });
  };
  return router;
};
