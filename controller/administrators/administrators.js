const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const middlewares = require("../../model/middlewares");
const library = require("../../model/library");
const dashboard = require("../../model/dashboard");
const nodemailer = require("nodemailer");
const mailcontent = require("../../model/mailcontent");
const mongoose = require("mongoose");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const moment = require("moment");
const async = require("async");

module.exports = () => {
  const router = {};
  const data = {};

  router.admindashboard = (req, res) => {
    const data = {};
    data.status = 0;
    dashboard.admindashboardCount({}, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };

  router.notifications = (req, res) => {
    const data = {};
    data.status = 0;

    dashboard.notifications({ type: "admin" }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        res.send(response);
      }
    });
  };

  router.save = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("username", "Invalid Username").notEmpty();
    req.checkBody("name", "Invalid Name").notEmpty();
    req.checkBody("email", "Invalid Email").notEmpty();
    if (!req.body._id || req.body.password != "") {
      req.checkBody("password", "Invalid Password").notEmpty();
      req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
    }
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const administrator = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      status: req.body.status,
      role: "subadmin"
    };

    if (req.body.password && req.body.confirm_password) {
      administrator.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.params.loginData.role === "admin") {
      if (req.body._id) {
        db.UpdateDocument("administrators", { _id: req.body._id }, administrator, {}, (err, result) => {
          if (err) {
            data.response = "Unable to save your data, Please try again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Updated successfully.";
            res.send(data);
          }
        });
      } else {
        db.InsertDocument("administrators", administrator, (err, result) => {
          if (err || !result) {
            if (err.code === 11000) {
              data.response = "Username/Email is already Exists ";
            } else {
              data.response = "Unable to Save Your Data Please try again";
            }
            data.status = 0;
            res.send(data);
          } else {
            data.status = 1;
            data.response = { result: result };
            res.send(data);
          }
        });
      }
    } else {
      data.status = 0;
      data.response = "Permission Denied";
      res.send(data);
    }
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;
    
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { status: { $exists: true } } });
    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }else if(req.body.status === 0){
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }
    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    userQuery.push({ $project: { createdAt: 1, updatedAt: 1, username: 1, role: 1, email: 1, activity: 1, status: 1, phone: 1, name: 1 } });

    const finalquery = [
      {
        $facet: {
          overall: [
            {
              $bucket: {
                groupBy: "$_id",
                boundaries: [0, 100],
                default: "Other",
                output: {
                  count: { $sum: 1 }
                }
              }
            }
          ],
          result: userQuery,
          dashboard: [
            { $group: { _id: "$status", count: { $sum: 1 } } },
            { $addFields: { status: { $arrayElemAt: [["inactive", "active"], "$_id"] } } },
            { $addFields: { nott: [["$status", "$count"]] } },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ]
        }
      }
    ];

    db.GetAggregation("administrators", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount = docData[0].result.length;
        if (docData[0].overall[0]) {
          fullcount = docData[0].overall[0].count;
        }
        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, dashboard: docData[0].dashboard };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("administrators", { _id: req.body.id }, {}, {}, (err, adminData) => {
      if (err || !adminData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: adminData };
        res.send(data);
      }
    });
  };

  router.login = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("password", "Password Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    const email = req.body.email;
    const password = req.body.password;

    db.GetOneDocument("administrators", { $or: [{ email: email, status: 1 }, { username: email, status: 1 }] }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Invalid Credentials";
        res.send(data);
      } else {
        if (library.validPassword(password, user.password)) {
          const updata = { activity: {} };
          updata.activity.last_login = Date();
          db.UpdateDocument("administrators", { _id: user._id }, updata, {}, (err, docdata) => {
            if (err) {
              data.response = "Invalid Credentials";
              res.send(data);
            } else {
              const auth_token = library.jwtSign({ username: user.username, role: "admin" });
              data.status = 1;
              data.response = { auth_token: auth_token };
              res.send(data);
            }
          });
        } else {
          data.response = "Invalid Credentials";
          res.send(data);
        }
      }
    });
  };

  router.logout = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Username Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.UpdateDocument("administrators", { username: req.body.username }, { "activity.last_logout": new Date() }, {}, (err, response) => {
      if (err) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "logged out";
        res.send(data);
      }
    });
  };

  router.forgotpassword = (req, res) => {
    const data = {};
    const request = {};
    req.checkBody("email", "Invalid email").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    request.email = req.body.email;
    request.reset = library.randomString(8, "#A");

    async.waterfall(
      [
        callback => {
          db.GetOneDocument("administrators", { email: request.email }, {}, {}, (err, admin) => {
            callback(err, admin);
          });
        },
        (admin, callback) => {
          if (admin) {
            db.UpdateDocument("administrators", { _id: admin._id }, { reset_code: request.reset }, {}, (err, response) => {
              callback(err, admin);
            });
          } else {
            callback(null, admin);
          }
        },
        (admin, callback) => {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, settings) => {
            if (err) {
              callback(err, callback);
            } else {
              callback(err, admin, settings);
            }
          });
        }
      ],
      (err, admin, settings) => {
        if (err || !admin) {
          data.status = "0";
          data.response = "Errror!";
          res.status(400).send(data);
        } else {
          let name;
          if (admin.name) {
            name = admin.name + " (" + admin.username + ")";
          } else {
            name = admin.username;
          }
          const adminId = admin._id;

          const mailData = {};
          mailData.template = "forgotpassword";
          mailData.to = admin.email;

          mailData.html = [];
          mailData.html.push({ name: "name", value: name });
          mailData.html.push({ name: "site_url", value: settings.settings.site_url });
          mailData.html.push({ name: "logo", value: settings.settings.logo });
          mailData.html.push({ name: "email", value: admin.email });
          mailData.html.push({ name: "url", value: settings.settings.site_url + "portal/admin/resetpassword" + "/" + adminId + "/" + request.reset });

          mailcontent.sendmail(mailData, (err, response) => {
            // console.log("err, response",err, response);
          });

          data.status = "1";
          data.url = settings.settings.site_url + "portal/admin/resetpassword" + "/" + adminId + "/" + request.reset;
          data.response = "Reset Code Sent Successfully!";
          res.send(data);
        }
      }
    );
  };

  router.resetpassword = (req, res) => {
    const id = req.params.userid;
    const resetid = req.params.reset;
    const data = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

    db.UpdateDocument("administrators", { _id: id, reset_code: resetid }, { password: data }, {}, (err, docdata) => {
      if (err || docdata.nModified == 0) {
        var data = {};
        data.status = "0";
        data.response = "Reset Password error!";
        res.send(data);
      } else {
        data = {};
        data.status = "1";
        data.response = "Reset Successfully!";
        res.send(data);
      }
    });
  };

  router.userexport = (req, res) => {
    const adminQuery = [
      { $match: { status: { $exists: true } } },
      { $project: { username: 1, email: 1, name: 1, createdAt: 1, status: 1 } },
      { $project: { document: "$$ROOT" } },
      { $group: { _id: null, count: { $sum: 1 }, documentData: { $push: "$document" } } }
    ];
    db.GetAggregation("administrators", adminQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata.length != 0) {
          //const fields = ["createdAt", "username", "name", "email", "status"];
          const fieldNames = ["Date", "User Name", "Name", "User Mail", "Status"];
          const mydata = docdata[0].documentData;
          let finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            if (mydata[i].status == 0) {
              mydata[i].status = "In-active";
            } else if (mydata[i].status == 1) {
              mydata[i].status = "Active";
            }
            let temp = {};
            temp['Date'] = moment(mydata[i].createdAt).format("DD/MM/YYYY");
            temp['Username'] = mydata[i].username;
            temp['Name'] = mydata[i].name;
            temp['Usermail'] = mydata[i].email;
            temp['Status'] = mydata[i].status;
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  router.clearnotification = (req, res) => {
    db.UpdateMany("notifications", { type: "admin" }, { status: 2 }, {}, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Please try again later";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Cleared";
        res.send(data);
      }
    });
  };
  return router;
};
