const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library");
const attachment = require("../../model/attachments");

// const superadminschemas = require("../schema/superadmin.schema");
const nodemailer = require("nodemailer");

module.exports = () => {
  const router = {};

  router.save = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("name", "Invalid Name").notEmpty();
    req.checkBody("sender_name", "Invalid Sender Name").notEmpty();
    req.checkBody("sender_email", "Sender Email required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const plan = {
      name: req.body.name,
      description: req.body.description,
      email_subject: req.body.email_subject,
      sender_name: req.body.sender_name,
      sender_email: req.body.sender_email,
      email_content: req.body.email_content,
      status: req.body.status
    };

    if (req.body._id) {
      db.UpdateDocument("emailtemplate", { _id: req.body._id }, plan, {}, (err, result) => {
        if (err) {
          data.response = "Unable to save your data, Please try again";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated successfully.";
          res.send(data);
        }
      });
    } else {
      plan.slug = plan.name.replace(/\s+/g, "").toLowerCase();
      db.InsertDocument("emailtemplate", plan, (err, result) => {
        if (err || !result) {
          data.response = "Unable to save your data, Please try again";
          res.send(err);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
        }
      });
    }
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    userQuery.push({ $match: { status: { $exists: true } } });

    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    // if(req.body.search){
    //   var searchs = req.body.search;
    //   var searching = {};
    //   searching[req.body.filter] = { $regex: searchs + '.*', $options: 'si' };
    //   userQuery.push({"$match": searching});
    // }
    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });
    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { sender_email: { $regex: searchs + ".*", $options: "si" } },
              { sender_name: { $regex: searchs + ".*", $options: "si" } },
              { description: { $regex: searchs + ".*", $options: "si" } },
              { email_subject: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    userQuery.push({ $project: { createdAt: 1, updatedAt: 1, name: 1, description: 1, email_subject: 1, sender_name: 1, status: 1, sender_email: 1 } });

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery
        }
      }
    ];

    db.GetAggregation("emailtemplate", finalquery, (err, docData) => {
      if (err || !docData || docData.length <= 0) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        let fullcount = docData[0].result.length;
        if (docData[0].overall[0]) {
          fullcount = docData[0].overall[0].count;
        }

        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("emailtemplate", { _id: req.body.id }, {}, {}, (err, emailData) => {
      if (err || !emailData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: emailData };
        res.send(data);
      }
    });
  };

  return router;
};
