const db = require("../../model/mongodb");
const CONFIG = require("../../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const library = require("../../model/library");
const attachment = require("../../model/attachments");
const json2csv = require("json2csv").Parser;
const fs = require("fs");
const moment = require("moment");
const nodemailer = require("nodemailer");
const mailcontent = require("../../model/mailcontent");
const mongoose = require("mongoose");
const dashboard = require("../../model/dashboard");
const mail = require("../../model/mail.js");
const pdf = require("html-pdf");
const event = require("../../controller/events/events");
// const options = { format: "A4", orientation: "landscape" };
const options = { format: "A4" };

module.exports = () => {
  const router = {};

  router.save = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Invalid Username").notEmpty();
    req.checkBody("email", "Invalid Email").notEmpty();
    req.checkBody("name", "Invalid Name").notEmpty();
    if (!req.body._id || req.body.password != "") {
      req.checkBody("password", "Invalid Password").notEmpty();
      req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
    }
    req.checkBody("phone.code", "Invalid Number").notEmpty();
    req.checkBody("phone.number", "Invalid Number").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const agency = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      status: req.body.status,
      isverified: req.body.isverified,
      phone: req.body.phone,
      address: req.body.address,
      company_name: req.body.company_name,
      company_phone: req.body.company_phone,
      company_description: req.body.company_description,
      postal_address: req.body.postal_address,
      registration_number: req.body.registration_number,
      organisation_type: req.body.organisation_type,
      company_email: req.body.company_email,
      fax: req.body.fax,
      surname: req.body.surname,
      bank_details: {
        name: req.body.bank_name,
        ac_no: req.body.bank_ac,
        ac_name: req.body.bank_ac_name,
        sort_code: req.body.bank_sort_code
        // becs: req.body.bank_becs,
        // sepa: req.body.bank_sepa
      }
    };

    if (req.params.loginId) {
      agency.admin = req.params.loginId;
    }
    if (req.body.vat_number) {
      agency.vat_number = req.body.vat_number;
    }

    if (req.body.password && req.body.confirm_password) {
      agency.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        agency.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // agency.avatar = req.body.avatar;
    }

    if (req.files && typeof req.files.company_logo !== "undefined") {
      if (req.files.company_logo.length > 0) {
        agency.company_logo = attachment.get_attachment(req.files.company_logo[0].destination, req.files.company_logo[0].filename);
      }
    }

    if (req.body._id) {
      db.UpdateDocument("agencies", { _id: req.body._id }, agency, { upsert: true }, (err, result) => {
        if (err) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Updated successfully.";
          res.send(data);
        }
      });
    } else {
      db.InsertDocument("agencies", agency, (err, result) => {
        if (err || !result) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = { result: result };
          res.send(data);
          event.emit("agencyemailtemplates", { agency: result._doc._id });
        }
      });
    }
  };

  router.list = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const userQuery = [];
    if (req.body.isverified) {
      userQuery.push({ $match: { isverified: { $eq: parseInt(req.body.isverified) } } });
    }

    userQuery.push({ $match: { status: { $exists: true } } });
    if (req.body.status) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else if (req.body.status === 0) {
      userQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    if (req.body.from_date) {
      userQuery.push({ $match: { createdAt: { $gt: new Date(req.body.from_date) } } });
    }

    if (req.body.to_date) {
      userQuery.push({ $match: { createdAt: { $lt: new Date(req.body.to_date) } } });
    }

    userQuery.push({
      $lookup: {
        from: "shifts",
        let: { agencyid: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$agency", "$$agencyid"]
              }
            }
          },
          { $group: { _id: "$agency", count: { $sum: 1 } } }
        ],
        as: "shiftcount"
      }
    });
    userQuery.push({
      $lookup: {
        from: "clients",
        let: { agencyid: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$agency", "$$agencyid"]
              }
            }
          },
          { $group: { _id: null, count: { $sum: 1 } } }
        ],
        as: "clients"
      }
    });

    userQuery.push({
      $lookup: {
        from: "employees",
        let: { agencyid: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$agency", "$$agencyid"]
              }
            }
          },
          { $group: { _id: null, count: { $sum: 1 } } }
        ],
        as: "employees"
      }
    });

    userQuery.push({
      $lookup: {
        from: "stats",
        let: { agencyid: "$_id" },
        pipeline: [
          { $match: { $expr: { $and: [{ $eq: ["$agency", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
          { $project: { agency: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
          { $group: { _id: "$agency", count: { $sum: "$hours" } } },
          { $project: { total: { $divide: ["$count", 3600] } } }
        ],
        as: "hours"
      }
    });

    userQuery.push({
      $project: {
        createdAt: 1,
        updatedAt: 1,
        username: 1,
        company_name: 1,
        email: 1,
        activity: 1,
        status: 1,
        phone: 1,
        name: 1,
        isverified: 1,
        employees: "$employees.count",
        clients: "$clients.count",
        hours: "$hours.total",
        shiftcount: { $ifNull: [{ $arrayElemAt: ["$shiftcount.count", 0] }, 0] }
      }
    });

    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        userQuery.push({
          $match: {
            $or: [
              { username: { $regex: searchs + ".*", $options: "si" } },
              { email: { $regex: searchs + ".*", $options: "si" } },
              { company_name: { $regex: searchs + ".*", $options: "si" } },
              { employees: { $regex: searchs + ".*", $options: "si" } },
              { clients: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { phone: { $regex: searchs + ".*", $options: "si" } },
              { "phone.code": { $regex: searchs + ".*", $options: "si" } },
              { "phone.number": { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        userQuery.push({ $match: searching });
      }
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      userQuery.push({ $sort: sorting });
    } else {
      userQuery.push({ $sort: { createdAt: -1 } });
    }

    const withoutlimit = Object.assign([], userQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      userQuery.push({ $skip: parseInt(req.body.skip) });
    }
    if (req.body.limit >= 0) {
      userQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalquery = [
      {
        $facet: {
          overall: withoutlimit,
          result: userQuery,
          dashboard: [
            { $group: { _id: "$status", count: { $sum: 1 } } },
            { $addFields: { status: { $arrayElemAt: [["inactive", "active"], "$_id"] } } },
            { $addFields: { nott: [["$status", "$count"]] } },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ]
        }
      }
    ];

    db.GetAggregation("agencies", finalquery, (err, docData) => {
      if (err || !docData) {
        data.err = err;
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ type: "admin", action: req.body.notification }, function(err, response) {});
        }
        // console.log("notification_status", req.body.notification);

        data.status = 1;
        let fullcount = docData[0].result.length;
        if (docData[0].overall[0]) {
          fullcount = docData[0].overall[0].count;
        }

        data.response = { result: docData[0].result, length: docData[0].result.length, fullcount: fullcount, dashboard: docData[0].dashboard };
        res.send(data);
      }
    });
  };

  router.edit = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("id", "Invalid ID").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetDocument("agencies", { _id: req.body.id }, {}, {}, (err, agencyData) => {
      if (err || !agencyData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: agencyData };
        res.send(data);
      }
    });
  };

  /* router.userexport = (req, res) => {
    const data = {};
    const bannerQuery = [
      {
        // "$match": { status: { $ne: 0 } }
        $match: { status: { $exists: true } }
      },
      {
        $project: {
          company_name: 1,
          name: 1,
          phone: 1,
          email: 1,
          createdAt: 1,
          address: 1,
          status: 1
        }
      },
      {
        $project: {
          document: "$$ROOT"
        }
      },
      {
        $group: { _id: null, count: { $sum: 1 }, documentData: { $push: "$document" } }
      }
    ];
    db.GetAggregation("administrators", bannerQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata.length != 0) {
          const fields = [
            "createdAt",
            "username",
            "name",
            "email",
            "phone.code",
            "phone.number",
            "address.line1",
            "address.line2",
            "address.city",
            "address.state",
            "address.zipcode",
            "address.country",
            "address.formatted_address",
            "status"
          ];
          const fieldNames = ["Date", "User Name", "Name", "User mail", "Phone code", "Phone number", "Address 1", "Address 2", "city", "State", "zipcode", "Country", "Full Address", "Status"];
          const mydata = docdata[0].documentData;
          for (let i = 0; i < mydata.length; i++) {
            mydata[i].createdAt = moment(mydata[i].createdAt).format("DD/MM/YYYY");
          }
          const json2csvParser = new json2csv({ fields, fieldNames });
          const csv = json2csvParser.parse(mydata);
          // let filename = "uploads/csv/users-" + new Date().getTime() + ".csv";
          // fs.writeFile(filename, csv, (err, doc) => {
          //   if (err) {
          //     data.status = 0;
          //     data.response = "Exported Failed";
          //     res.send(data);
          //   } else {
          //     res.download(filename);
          //   }
          // });
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  }; */

  router.invoiceeexport = (req, res) => {
    const data = {};
    data.status = 0;

    const invoiceQuery = [];
    if (req.body.id) {
      invoiceQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    }
    if (req.body.status) {
      invoiceQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      invoiceQuery.push({ $match: { status: { $gte: 1 } } });
    }
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1, address: 1, avatar: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );
    invoiceQuery.push(
      {
        $lookup: {
          from: "invoicecycle",
          let: { inv: "$invoiceID" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$invoiceID", "$$inv"]
                }
              }
            },
            { $project: { createdAt: 1, updatedAt: 1, status: 1, due_date: 1, payment_details: 1, invoiceID: 1 } }
          ],
          as: "inv_data"
        }
      },
      { $unwind: { path: "$inv_data", preserveNullAndEmptyArrays: true } },
      { $addFields: { payment_list: "$inv_data.payment_details" } },
      { $unwind: { path: "$payment_list", preserveNullAndEmptyArrays: true } },
      { $addFields: { payment_amount: { $sum: "$payment_list.amount" } } }
    );
    invoiceQuery.push({
      $group: {
        // _id: {client: "$client", locations: "$locations", employee: "$employee", branch: "$branch", job_type: "$job_type"},
        _id: { client: "$client" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        client_rate: { $sum: "$client_rate" },
        late_amount: { $sum: "$late_amount" },
        inv_data: { $push: "$inv_data" },
        payment_amount: { $push: "$payment_amount" },
        count: { $sum: 1 }
      }
    });

    db.GetAggregation("invoice", invoiceQuery, (err, docData) => {
      if (err) {
        data.response = err;
        res.send(data);
      } else {
        if (docData && docData.length > 0) {
          const fieldNames = ["Number", "To", "Shifts", "Date", "Due Date", "Overdue by", "Paid", "Due"];
          const mydata = docData;
          const finaldate = [];
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Number"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? mydata[i].inv_data[0].invoiceID : "";
            temp["To"] = mydata[i].client && mydata[i].client.length > 0 ? mydata[i].client[0][0] : "";
            temp["Shifts"] = mydata[i].count || 0;
            temp["Date"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? moment(mydata[i].inv_data[0].createdAt).format("DD-MM-YYYY") : "-";
            temp["Due Date"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? moment(mydata[i].inv_data[0].due_date).format("DD-MM-YYYY") : "-";
            temp["Overdue by"] = mydata[i].inv_data && mydata[i].inv_data.length > 0 ? `${moment(mydata[i].inv_data[0].due_date || 0).diff(moment(mydata[i].inv_data[0].createdAt || 0), "days")} Days` : "-";
            temp["Paid"] = mydata[i].payment_amount && mydata[i].payment_amount.length > 0 ? `£ ${mydata[i].payment_amount[0]}` : "£ 0";
            temp["Due"] = `£ ${mydata[i].client_rate}` || "£ 0";
            finaldate.push(temp);
          }
          finaldate.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldate);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };
  router.timeexport = (req, res) => {
    const data = {};
    data.status = 0;

    const locationQuery = [];
    if (req.body.id) {
      locationQuery.push({ $match: { agency: { $eq: new mongoose.Types.ObjectId(req.body.id) } } });
    }

    locationQuery.push({ $match: { timesheet_status: { $gte: 1 } } });
    if (req.body.status) {
      locationQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      locationQuery.push({ $match: { status: { $gte: 6 } } });
    }
    locationQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data" }
      }
    );
    locationQuery.push(
      {
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { company_name: 1 } }
          ],
          as: "agency_data"
        }
      },
      {
        $addFields: { agency: "$agency_data" }
      }
    );
    locationQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { emp: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$emp"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data" }
      }
    );
    locationQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data" } }
    );
    locationQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    locationQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1, phone: 1, avatar: 1, address: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data" } }
    );
    locationQuery.push({ $project: { employee_requested: 0 } });
    locationQuery.push({
      $group: {
        _id: { employee: "$employee", client: "$client", job_type: "$job_type" },
        branch: { $push: "$branch.branches.branchname" },
        locations: { $push: "$locations.name" },
        client: { $push: "$client.companyname" },
        employee: { $push: "$employee.name" },
        job_type: { $push: "$job_type.name" },
        count: { $sum: 1 }
      }
    });

    db.GetAggregation("shifts", locationQuery, (err, docData) => {
      if (err || !docData) {
        data.response = err;
        res.send(data);
      } else {
        if (docData && docData.length > 0) {
          const fieldNames = ["Employee", "Client", "Role", "Shifts"];
          const mydata = docData;
          const finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Employee"] = mydata[i].employee ? (mydata[i].employee && mydata[i].employee.length > 0 ? mydata[i].employee[0][0] : "") : "";
            temp["Client"] = mydata[i].client ? (mydata[i].client && mydata[i].client.length > 0 ? mydata[i].client[0][0] : "") : "";
            temp["Role"] = mydata[i].job_type ? (mydata[i].job_type && mydata[i].job_type.length > 0 ? mydata[i].job_type[0][0] : "") : "";
            temp["Shifts"] = mydata[i].count || 0;
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  router.userexport = (req, res) => {
    const data = {};
    const bannerQuery = [
      {
        $match: { status: { $exists: true } }
      },
      {
        $lookup: {
          from: "shifts",
          let: { agencyid: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$agency", "$$agencyid"]
                }
              }
            },
            { $group: { _id: "$agency", count: { $sum: 1 } } }
          ],
          as: "shiftcount"
        }
      },
      {
        $lookup: {
          from: "clients",
          let: { agencyid: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$agency", "$$agencyid"]
                }
              }
            },
            { $group: { _id: null, count: { $sum: 1 } } }
          ],
          as: "clients"
        }
      },
      {
        $lookup: {
          from: "employees",
          let: { agencyid: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$agency", "$$agencyid"]
                }
              }
            },
            { $group: { _id: null, count: { $sum: 1 } } }
          ],
          as: "employees"
        }
      },
      {
        $lookup: {
          from: "stats",
          let: { agencyid: "$_id" },
          pipeline: [
            { $match: { $expr: { $and: [{ $eq: ["$agency", "$$agencyid"] }, { $eq: ["$status", 5] }] } } },
            { $project: { agency: 1, hours: { $subtract: ["$endtime", "$starttime"] } } },
            { $group: { _id: "$agency", count: { $sum: "$hours" } } },
            { $project: { total: { $divide: ["$count", 3600] } } }
          ],
          as: "hours"
        }
      },
      {
        $project: {
          company_name: 1,
          email: 1,
          status: 1,
          phone: 1,
          name: 1,
          employees: "$employees.count",
          clients: "$clients.count",
          hours: "$hours.total",
          shiftcount: { $ifNull: [{ $arrayElemAt: ["$shiftcount.count", 0] }, 0] }
        }
      },
      {
        $project: {
          document: "$$ROOT"
        }
      },
      {
        $group: { _id: null, count: { $sum: 1 }, documentData: { $push: "$document" } }
      }
    ];
    db.GetAggregation("agencies", bannerQuery, (err, docdata) => {
      if (err) {
        res.send(err);
      } else {
        if (docdata[0].documentData && docdata[0].documentData.length > 0) {
          const fieldNames = ["Company Name", "Contact Name", "Tel", "Email", "No of Employees", "Clients", "Hours", "Status"];
          const mydata = docdata[0].documentData;
          const finaldata = [];
          for (let i = 0; i < mydata.length; i++) {
            const temp = {};
            temp["Company Name"] = mydata[i].company_name || "";
            temp["Contact Name"] = mydata[i].name || "";
            temp["Tel"] = (mydata[i].phone.code || "") + "-" + (mydata[i].phone.number || "");
            temp["Email"] = mydata[i].email || "";
            temp["No of Employees"] = mydata[i].employees && mydata[i].employees.length > 0 ? mydata[i].employees[0] : "0";
            temp["Clients"] = mydata[i].clients && mydata[i].clients.length > 0 ? mydata[i].clients[0] : "0";
            temp["Hours"] = mydata[i].hours && mydata[i].hours.length > 0 ? mydata[i].hours[0] : "0:00";
            temp["Status"] = mydata[i].status && (mydata[i].status == 1 ? "Active" : "In-Active");
            finaldata.push(temp);
          }
          finaldata.reverse();
          const json2csvParser = new json2csv({ fieldNames });
          const csv = json2csvParser.parse(finaldata);
          if (csv) {
            data.status = 1;
            data.response = csv;
            res.send(data);
          } else {
            data.status = 0;
            data.response = "Export Failed";
            res.send(data);
          }
        } else {
          res.send([0, 0]);
        }
      }
    });
  };

  router.actasagency = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Id Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id), status: 1, isverified: 1 }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Agency is currently Inactive or not verified";
        res.send(data);
      } else {
        const options = {};
        options.sort = { createdAt: -1 };
        db.GetDocument("subscriptions", { agency: user._id, status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
          if (subscriptionData.length >= 1) {
            data.status = 1;
            const auth_token = library.jwtSign({
              username: user.username,
              role: "agency",
              subscription: 1,
              employee_count: subscriptionData[0].plan.employees,
              recruitment_module: subscriptionData[0].plan.recruitment_module
            });
            data.response = { auth_token: auth_token };
            data.auth = { username: user.username, role: "agency", subscription: 1, employee_count: subscriptionData[0].plan.employees, recruitment_module: subscriptionData[0].plan.recruitment_module };
            res.send(data);
          } else {
            data.status = 1;
            const auth_token = library.jwtSign({ username: user.username, role: "agency", subscription: 0, employee_count: 0, recruitment_module: 0 });
            data.response = { auth_token: auth_token };
            data.auth = { username: user.username, role: "agency", subscription: 0, employee_count: 0 };
            res.send(data);
          }
        });

        // var auth_token = library.jwtSign({ username: user.username , role : "agency" });
        // data.status = 1;
        // data.response = { 'auth_token' : auth_token  , 'subscription' : 1 };
        // res.send(data);
      }
    });
  };

  router.minidashboard = (req, res) => {
    req.checkBody("agency", "Invalid Details").notEmpty();
    req.checkBody("selectshift", "Invalid Details").notEmpty();
    req.checkBody("selecttimesheet", "Invalid Details").notEmpty();
    req.checkBody("selectinvoice", "Invalid Details").notEmpty();

    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const populate = { selectshift: req.body.selectshift, selecttimesheet: req.body.selecttimesheet, selectinvoice: req.body.selectinvoice };
    dashboard.agencydashboardCount({ type: "agency", agency: new mongoose.Types.ObjectId(req.body.agency), populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };

  router.profile = (req, res) => {
    req.checkBody("id", "Invalid Details").notEmpty();
    req.checkBody("purpose", "Invalid Details").notEmpty();
    req.checkBody("email", "Invalid Details").optional();

    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(req.body.id) }, {}, {}, function(err, agency) {
      if (err || !agency) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        const html = `<style>html {
zoom: 0.55;
}</style><table style="margin: 0; padding: 0; color: #000; background: #fff; padding-top: 40px; padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
          <!-- <tr>
            <td>
              <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                <tbody>
      <tr>
                     <td style="width: 100%; font-size: 16px;color: #119dcd;  padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">
                              Dashboard Details </td>
                          </tr>
                          <tr>
                            <td height="10">&nbsp;</td>
                          </tr>
                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                          
                          <tr>
                            <td width="49%">
                              <span style="width: 100%; font-size: 15px;color: #009688; padding: 18px 0;  font-weight: bold;"> Shifts</span>
                            </td>
                            <td width="49%">
                              <span style="width: 100%; font-size: 15px;color: #009688; padding: 18px 0;  font-weight: bold;"> Invoice
                              </span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr> -->
    <tr>
                            <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">
                              Personal Details </td>
                          </tr>
                          <tr>
                            <td height="10">&nbsp;</td>
                          </tr>
                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                          
                          <tr>
                            <td width="49%">
                              <img src='${CONFIG.LIVEURL}/${agency.avatar}' style=" width: 150px; height: 150px;">
                            </td>
                            <td width="49%">
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Name
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.name}</span> </p>
                             <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Surname
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.surname}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">Phone
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.phone.code}-${agency.phone.number}</span>
                              </p>
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0;font-size: 14px;"><span style="width: 50%;float: left; font-weight: bold;">Email
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.email}</span>
                              </p>
                             
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
  
      <tr>
                            <td style="width: 100%; font-size: 16px;color: #119dcd; padding: 15px 30px; border-bottom: 1px solid #ddd;    font-weight: bold;">Company
                              Information</td>
                          </tr>
                          <tr>
                            <td height="10">&nbsp;</td>
                          </tr>
      
                  <tr>
                    <td>
                      <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                        <tbody>
                          
                          <tr>
                            <td width="49%">
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  Name <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.company_name}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  Tel <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.company_phone.code}-${agency.company_phone.number}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  Location <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.address.formatted_address}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  State/Region <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">${agency.address.state}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  Postal Code <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%;float: left;">
                                  ${agency.address.zipcode}</span> </p>
                                  <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Company
                                  Number <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  ${agency.registration_number} </span> </p>
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  Type <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span
                                  style="width: 50%;float: left;"> ${agency.organisation_type}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0;margin: 0;font-size: 14px;"><span style="width: 50%;float: left;font-weight: bold;">
                                  Description <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span>
                                <span style="width: 50%;float: left;">${agency.company_description}</span> </p>
                             
                            </td>
                            <td width="49%">
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Email
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  ${agency.company_email}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Logo
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  <img src='${CONFIG.LIVEURL}/${agency.company_logo}' style=" width: 120px;"> </span> </p>
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Line1
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  ${agency.address.line1}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">City/Town
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  ${agency.address.line2}</span> </p>
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Country
                                  <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  ${agency.address.country} </span> </p>
                             
                              <p style="float:left;width:100%;padding: 5px 0; margin: 0; font-size: 14px;"><span style="width: 50%; float: left;     font-weight: bold;">Postal
                                  Address <span style="text-align: right; padding: 0px 10px; float: right;">:</span></span> <span style="width: 50%; float: left;">
                                  ${agency.postal_address} </span> </p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>`;
        if (req.body.purpose == "download") {
          pdf.create(html, options).toStream((err, stream) => stream.pipe(res));
        } else {
          pdf.create(html, options).toFile(`./uploads/pdf/${agency.name}.pdf`, function(err, document) {
            if (err) {
              data.response = "Unable to Send Mail Please Try Again";
              res.send(data);
            } else {
              if (req.body.email) {
                data.status = 1;
                data.response = "Mail Sent";
                res.send(data);
                const mailOptions = {
                  from: agency.email,
                  to: req.body.email,
                  subject: "Agency Profile",
                  text: "Please Download the attachment to see the Profile",
                  html: "<b>Please Download the attachment to see the Profile</b>",
                  attachments: [
                    {
                      filename: `${agency.name}.pdf`,
                      path: `./uploads/pdf/${agency.name}.pdf`,
                      contentType: "application/pdf"
                    }
                  ]
                };
                mail.send(mailOptions, function(err, response) {});
              } else {
                data.response = "Mail Recepient Required";
                res.send(data);
              }
            }
          });
        }
      }
    });
  };

  return router;
};
