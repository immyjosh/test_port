const db = require('../../model/mongodb');
const CONFIG = require('../../config/config');

module.exports =() =>{
    
    const router = {};
    var data = {};
    data.status = 0;

    router.save = (req,res) =>{

        if(req.body._id){

            req.checkBody('settings','Setting is Required').notEmpty();
            req.checkBody('status','Status is Required').notEmpty();

            var error = req.validationErrors();
            if(error){
                data.response = error[0].msg;
                return res.send(data);
            }

            var payment = {
                settings : req.body.settings,
                status : req.body.status,
            };

            db.UpdateDocument('paymentgateway',{_id:req.body._id},payment,{}, (err, result) => {
                if(err){
                    data.response = 'Unable to Save Your Data Please try Again';
                    res.send(data);

                }else{
                    data.status = 1;
                    data.response = 'Payment Updated Successfully ';
                    res.send(data);
                }
            });

        }else{

            req.checkBody('alias','Alias is Required').notEmpty();
            req.checkBody('gateway_name','Gateway Name is Required').notEmpty();
            req.checkBody('settings','Setting is Required').notEmpty();
            req.checkBody('status','Status is Required').notEmpty();

            error = req.validationErrors();
            if(error){
                data.response = error[0].msg;
                return res.send(data);
            }

            payment = {
                id:req.body.id,
                alias:req.body.alias,
                gateway_name:req.body.gateway_name,
                settings:req.body.settings,
                status:req.body.status,
            };


            db.InsertDocument('paymentgateway',payment, (err,result) => {
                if(err || !result){
                    data.response = 'Unable to Save Your Data Please Try Again';
                    res.send(data);
                }else{
                    data.status = 1;
                    data.response = {'result':result};
                    res.send(data);
                }
            });
        } 
    }


    router.list = (req,res) =>{
         
        var error = req.validationErrors();
        if(error){
            data.response = error[0].msg;
            return res.send(data);
        }

        var paymentQuery = [];
        paymentQuery.push({'$match':{'status':{$exists:true}}});
        if(req.body.status){
            paymentQuery.push({'$match':{'status':{$eq:req.body.status}}});
        }

        if(req.body.search){
            var searches = req.body.search;
            var searching = {};
            searching[req.body.filter] = {$regex: searches + '.*',$options:'si'};
            paymentQuery.push({'$match':searching});
        }

        if(req.body.field && req.body.order){
            var sorting = {};
            sorting[req.body.field] = parseInt(req.body.order);
            paymentQuery.push({$sort: sorting});
        }else{
            paymentQuery.push({ $sort: { "createdAt" : -1 } });
        }

        if(req.body.skip >=0){
        paymentQuery.push({$skip: parseInt(req.body.skip)});
        }

        if(req.body.limit >=0){
            paymentQuery.push({$limit: parseInt(req.body.limit)});
        }

        paymentQuery.push({$project: {createdAt:1, updatedAt:1, status:1, id:1, alias:1, gateway_name:1, settings:1, }});


        paymentQuery = [
        {
            $facet:{
                'overall':[{
                    $bucket:{
                        groupBy:'$_id',
                        boundaries:[0,100],
                        default:'other',
                        output:{
                            'count':{$sum:1}
                        }
                    }
                }],
                'result':paymentQuery,
            }
        }

        ];

        db.GetAggregation('paymentgateway',paymentQuery, (err, docData) => {
            if(err || !docData){
                data.response = 'Unable to Get Your Data Please Try Again';
                res.send(data);
            }else{
                data.status = 1;
                var fullCount = docData[0].result.length;
                if(docData[0].overall[0]){
                    fullCount = docData[0].overall[0].count;
                }
                data.response = {'result':docData[0].result, 'length':docData[0].result.length, 'fullcount':fullCount};
                res.send(data);
                
            }
        });
    }

    router.edit = (req,res) =>{

        req.checkBody('id','Invalid id').notEmpty();
        
        var error = req.validationErrors();
        if(error){
            data.response = error[0].msg;
            return res.send(data);
        }

        db.GetOneDocument('paymentgateway',{'_id':req.body.id},{},{}, (err, paymentData) => {
            if(err || !paymentData){
                data.response = 'Unable to Get Your Data Please try Again';
                res.send(data);
            }else{
                data.status = 1;
                data.response = {'result': paymentData};
                res.send(data);
            }
        });
    };
       
    return router;
};