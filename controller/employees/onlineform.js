module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library.js");
  const nodemailer = require("nodemailer");
  const mail = require("../../model/mail.js");
  const mailcontent = require("../../model/mailcontent");
  const twilio = require("../../model/twilio.js");
  const dashboard = require("../../model/dashboard.js");
  const push = require("../../model/pushNotification.js")(io);
  const moment = require("moment");
  const mongoose = require("mongoose");
  const attachment = require("../../model/attachments");
  const async = require("async");
  const json2csv = require("json2csv").Parser;
  const fs = require("fs");
  const Google = require("../../model/googleapis");
  const event = require("../../controller/events/events");
  const { promisify } = require("util");
  const unlinkAsync = promisify(fs.unlink);
  const pdf = require("html-pdf");
  const pdfoptions = { format: "A4", orientation: "portrait", border: { top: "2px", right: "0.1px", bottom: "2px", left: "0.1px" } };
  const router = {};

  const GetOnlineForm = (employeeID, callback) => {
    const data = {};
    data.status = 0;
    const Query = [
      { $match: { _id: { $eq: new mongoose.Types.ObjectId(employeeID) } } },
      {
        $lookup: {
          from: "jobtypes",
          let: { job_id: "$onlineform.postapplyfor" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_id"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "postapplyfor"
        }
      },
    ]
    db.GetAggregation("employees", Query, function (err, employee_data) {
      if (err || !employee_data) {
        data.response = "Employee Data Not Available";
        callback(data);
      } else {
        const employee = employee_data && employee_data.length > 0 ? employee_data[0] : []
        const form_data = employee.onlineform ? employee.onlineform : {};

        // console.log("form_data",form_data.postapplyfor)
        // console.log("employee", employee.postapplyfor[0].name)

        const form_recruitement_module = employee.onlineform ? employee.onlineform.recruitment_module : {};
        const form_personaldetails = employee.onlineform && employee.onlineform.personaldetails ? employee.onlineform.personaldetails : {};
        const form_driving = employee.onlineform && employee.onlineform.drivingdetails ? employee.onlineform.drivingdetails : {};
        const form_aboutwork = employee.onlineform && employee.onlineform.aboutyourwork ? employee.onlineform.aboutyourwork.abletowork : {};
        const form_kin = employee.onlineform && employee.onlineform.nextofkindetails ? employee.onlineform.nextofkindetails : {};
        const form_gp = employee.onlineform && employee.onlineform.gpdetails ? employee.onlineform.gpdetails : {};
        const form_status = employee.onlineform && employee.onlineform.form_status ? employee.onlineform.form_status : 0;
        if (form_recruitement_module) {
          if (form_data) {
            const online = {
              form_recruitement_module,
              // activeTab: form_status > 0 ? "5" : "1",
              postapplyfor: employee.postapplyfor[0].name,
              postapplydetails: form_data.postapplydetails,
              availablestartdate: moment(form_data.availablestartdate).format("DD/MM/YYYY"),
              title: form_personaldetails.title,
              firstname: form_personaldetails.firstname,
              middlename: form_personaldetails.middlename,
              lastname: form_personaldetails.lastname,
              homephone: form_personaldetails.homephone,
              mobileno: form_personaldetails.mobileno || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              email: form_personaldetails.email,
              dateofbirth: moment(form_personaldetails.dateofbirth).format("DD/MM/YYYY"),
              insuranceno: form_personaldetails.insuranceno,
              streetAddress: form_personaldetails.address && form_personaldetails.address.streetAddress,
              addressline2: form_personaldetails.address && form_personaldetails.address.addressline2,
              city: form_personaldetails.address && form_personaldetails.address.city,
              state: form_personaldetails.address && form_personaldetails.address.state,
              zip: form_personaldetails.address && form_personaldetails.address.zip,
              country: form_personaldetails.address && form_personaldetails.address.country,
              nationality: form_personaldetails.nationality,
              gender: form_personaldetails.gender,
              religion: form_personaldetails.religion,
              race: form_personaldetails.race,
              sexualorientation: form_personaldetails.sexualorientation,
              UploadIDPhoto: form_data.UploadIDPhoto,
              UploadIDName: form_data.UploadIDPhoto && form_data.UploadIDPhoto.substr(24, 30),
              visapermit: form_data.employmenteligibility && form_data.employmenteligibility.visapermit,
              visapermittype: form_data.employmenteligibility && form_data.employmenteligibility.visapermittype,
              drivinglicence: form_driving.drivinglicence,
              traveldetails: form_driving.traveldetails,
              drivinglicenceno: form_driving.drivinglicenceno,
              caraccess: form_driving.caraccess,
              bannedfromdriving: form_driving.bannedfromdriving,
              carinsurance: form_driving.carinsurance,
              UploadCV: form_data.UploadCV && form_data.UploadCV,
              UploadCVName: form_data.UploadCV && form_data.UploadCV.substr(24, 30),
              vehicledocumentsvalid: form_driving.vehicledocumentsvalid,
              englishspoken: form_data.langauages && form_data.langauages.englishspoken,
              englishwritten: form_data.langauages && form_data.langauages.englishwritten,
              otherlanguage: form_data.langauages && form_data.langauages.otherlanguage,
              IELTStest: form_data.langauages && form_data.langauages.IELTStest,
              work_mornings: form_aboutwork.work_mornings,
              work_evenings: form_aboutwork.work_evenings,
              work_afternoons: form_aboutwork.work_afternoons,
              work_occasional: form_aboutwork.work_occasional,
              work_fulltime: form_aboutwork.work_fulltime,
              work_parttime: form_aboutwork.work_parttime,
              work_nights: form_aboutwork.work_nights,
              work_weekends: form_aboutwork.work_weekends,
              work_anytime: form_aboutwork.work_anytime,
              kinprefix: form_kin.kinprefix,
              kinfirstname: form_kin.kinfirstname,
              kinlastname: form_kin.kinlastname,
              kinrelationship: form_kin.kinrelationship,
              kinhomephone: form_kin.kinhomephone,
              kinmobileno: form_kin.kinmobileno || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              kinemail: form_kin.kinemail,
              kinstreetAddress: form_kin.Address && form_kin.Address.kinstreetAddress,
              kinaddressline2: form_kin.Address && form_kin.Address.kinaddressline2,
              kincity: form_kin.Address && form_kin.Address.kincity,
              kinstate: form_kin.Address && form_kin.Address.kinstate,
              kinzip: form_kin.Address && form_kin.Address.kinzip,
              kincountry: form_kin.Address && form_kin.Address.kincountry,
              firstchoice: form_data.aboutyourwork && form_data.aboutyourwork.firstchoice,
              secondchoice: form_data.aboutyourwork && form_data.aboutyourwork.secondchoice,
              thirdchoice: form_data.aboutyourwork && form_data.aboutyourwork.thirdchoice,
              shortnotice: form_data.aboutyourwork && form_data.aboutyourwork.shortnotice,
              reduceworkflexibility: form_data.aboutyourwork && form_data.aboutyourwork.reduceworkflexibility,
              workstate: form_data.aboutyourwork && form_data.aboutyourwork.workstate,
              employmenthistorylist: form_data.employmenthistory,
              educationlist: form_data.education,
              firstrefrelationship: form_data.reference1 && form_data.reference1.firstrefrelationship,
              firstreffirstname: form_data.reference1 && form_data.reference1.firstreffirstname,
              firstreflastname: form_data.reference1 && form_data.reference1.firstreflastname,
              firstrefphone: (form_data.reference1 && form_data.reference1.firstrefphone) || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              
              firstrefemail: form_data.reference1 && form_data.reference1.firstrefemail,
              firstrefconfirmemail: form_data.reference1 && form_data.reference1.firstrefconfirmemail,
              firstrefstreetAddress: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefstreetAddress,
              firstrefcity: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefcity,
              firstrefstate: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefstate,
              firstrefzip: form_data.reference1 && form_data.reference1.Address && form_data.reference1.Address.firstrefzip,
              secondrefrelationship: form_data.reference2 && form_data.reference2.secondrefrelationship,
              secondreffirstname: form_data.reference2 && form_data.reference2.secondreffirstname,
              secondreflastname: form_data.reference2 && form_data.reference2.secondreflastname,
              secondrefphone: (form_data.reference2 && form_data.reference2.secondrefphone) || {
                number: "",
                code: "",
                dailcountry: "gb"
              },
              secondrefemail: form_data.reference2 && form_data.reference2.secondrefemail,
              secondrefconfirmemail: form_data.reference2 && form_data.reference2.secondrefconfirmemail,
              secondrefstreetAddress: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefstreetAddress,
              secondrefcity: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefcity,
              secondrefstate: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefstate,
              secondrefzip: form_data.reference2 && form_data.reference1.Address && form_data.reference2.Address.secondrefzip,
              mandatory_training: form_data.mandatory_training,
              movinghandling: form_data.movinghandling,
              basiclifesupport: form_data.basiclifesupport,
              healthsafety: form_data.healthsafety,
              firesafety: form_data.firesafety,
              firstaid: form_data.firstaid,
              infectioncontrol: form_data.infectioncontrol,
              foodsafety: form_data.foodsafety,
              medicationadmin: form_data.medicationadmin,
              safeguardingvulnerable: form_data.safeguardingvulnerable,
              trainingdates: form_data.trainingdates,
              healthcare_training: form_data.healthcare_training,
              diplomal3: form_data.diplomal3,
              diplomal2: form_data.diplomal2,
              personalsafetymental: form_data.personalsafetymental,
              intermediatelife: form_data.intermediatelife,
              advancedlife: form_data.advancedlife,
              complaintshandling: form_data.complaintshandling,
              handlingviolence: form_data.handlingviolence,
              doolsmental: form_data.doolsmental,
              coshh: form_data.coshh,
              dataprotection: form_data.dataprotection,
              equalityinclusion: form_data.equalityinclusion,
              loneworkertraining: form_data.loneworkertraining,
              resuscitation: form_data.resuscitation,
              interpretation: form_data.interpretation,
              trainindates2: form_data.trainindates2,
              sufferedlongtermillness: form_data.healthdeclaration && form_data.healthdeclaration.sufferedlongtermillness,
              leaveforneckinjury: form_data.healthdeclaration && form_data.healthdeclaration.leaveforneckinjury,
              neckinjuries: form_data.healthdeclaration && form_data.healthdeclaration.neckinjuries,
              sixweeksillness: form_data.healthdeclaration && form_data.healthdeclaration.sixweeksillness,
              communicabledisease: form_data.healthdeclaration && form_data.healthdeclaration.communicabledisease,
              medicalattention: form_data.healthdeclaration && form_data.healthdeclaration.medicalattention,
              healthdeclarationdetails: form_data.healthdeclaration && form_data.healthdeclaration.healthdeclarationdetails,
              registereddisabled: form_data.healthdeclaration && form_data.healthdeclaration.registereddisabled,
              absentfromwork: form_data.healthdeclaration && form_data.healthdeclaration.absentfromwork,
              statereason: form_data.healthdeclaration && form_data.healthdeclaration.statereason,
              GPname: form_gp.GPname,
              GPstreetAddress: form_gp.gpaddress && form_gp.gpaddress.GPstreetAddress,
              GPcity: form_gp.gpaddress && form_gp.gpaddress.GPcity,
              GPstate: form_gp.gpaddress && form_gp.gpaddress.GPstate,
              GPzip: form_gp.gpaddress && form_gp.gpaddress.GPzip,
              contactDoctor: form_gp.gpaddress && form_gp.gpaddress.contactDoctor,
              GPhealthdetails: form_gp.gpaddress && form_gp.gpaddress.GPhealthdetails,
              medicalillness: form_data.medicalhistory && form_data.medicalhistory.medicalillness,
              medicalillnesscaused: form_data.medicalhistory && form_data.medicalhistory.medicalillnesscaused,
              treatment: form_data.medicalhistory && form_data.medicalhistory.treatment,
              needanyadjustments: form_data.medicalhistory && form_data.medicalhistory.needanyadjustments,
              needanyadjustmentsnotes: form_data.medicalhistory && form_data.medicalhistory.needanyadjustmentsnotes,
              livedUK: form_data.tuberculosis && form_data.tuberculosis.livedUK,
              livedUKnotes: form_data.tuberculosis && form_data.tuberculosis.livedUKnotes,
              BCGvaccination: form_data.tuberculosis && form_data.tuberculosis.BCGvaccination,
              BCGvaccinationyes: form_data.tuberculosis && form_data.tuberculosis.BCGvaccination === "yes",
              BCGvaccinationdate: form_data.tuberculosis && moment(form_data.tuberculosis.BCGvaccinationdate).format("DD/MM/YYYY"),
              cough: form_data.tuberculosis && form_data.tuberculosis.cough,
              weightloss: form_data.tuberculosis && form_data.tuberculosis.weightloss,
              fever: form_data.tuberculosis && form_data.tuberculosis.fever,
              TB: form_data.tuberculosis && form_data.tuberculosis.TB,
              chickenpox: form_data.chickenpoxorshingles && form_data.chickenpoxorshingles.chickenpox,
              chickenpoxyes: form_data.chickenpoxorshingles && form_data.chickenpoxorshingles.chickenpox === "yes",
              chickenpoxdate: form_data.chickenpoxorshingles && moment(form_data.chickenpoxorshingles.chickenpoxdate).format("DD/MM/YYYY"),
              triplevaccination: form_data.immunisationhistory && form_data.immunisationhistory.triplevaccination,
              polio: form_data.immunisationhistory && form_data.immunisationhistory.polio,
              tetanus: form_data.immunisationhistory && form_data.immunisationhistory.tetanus,
              hepatitisB: form_data.immunisationhistory && form_data.immunisationhistory.hepatitisB,
              proneProcedures: form_data.immunisationhistory && form_data.immunisationhistory.proneProcedures,
              occupationalHealthServices: form_data.declaration && form_data.declaration.occupationalHealthServices,
              criminaloffence: form_data.declaration && form_data.declaration.criminaloffence,
              warningcriminaloffence: form_data.declaration && form_data.declaration.warningcriminaloffence,
              DBSdetails: form_data.declaration && form_data.declaration.DBSdetails,
              passport: form_data.righttowork && form_data.righttowork.passport,
              workhours: form_data.worktimedirectives && form_data.worktimedirectives.workhours,
              form_status: form_status,
              // uploadcertificate: form_data.uploadcertificate && form_data.uploadcertificate,
              // uploadcertificateName: form_data.uploadcertificate && form_data.uploadcertificate.substr(24, 30),
              // uploadaddress: form_data.uploadaddress && form_data.uploadaddress,
              // uploadaddressName: form_data.uploadaddress && form_data.uploadaddress.substr(24, 30),
              // uploadpassport: form_data.uploadpassport && form_data.uploadpassport,
              // uploadpassportName: form_data.uploadpassport && form_data.uploadpassport.substr(24, 30),
              // uploaddbs: form_data.uploaddbs && form_data.uploaddbs,
              // uploaddbsName: form_data.uploaddbs && form_data.uploaddbs.substr(24, 30),
              final_verify_status: form_data.final_verify_status,
              drivelicenceyes: form_driving.drivinglicence === "yes",
              caraccessyes: form_driving.caraccess === "yes",
              IELTStestyes: form_data.langauages && form_data.langauages.IELTStest === "yes",
              mandatorytrainingyes: form_data.mandatory_training === "yes",
              healthcaretrainingyes: form_data.healthcare_training === "yes",
              needanyadjustmentsyes: form_data.medicalhistory && form_data.medicalhistory.needanyadjustments === "yes",
              livedUKyes: form_data.tuberculosis && form_data.tuberculosis.livedUK === "no"
            };


            const html = `
            <style>
html {
zoom: 0.55;
}
</style>
    <table style="margin: 0; padding: 0; color: #000; background: #fff;  padding-bottom: 40px; font-family: SANS-SERIF;" border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                  <tr>
                    <td>
                      <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%; background: #fff;">
                        <tbody>

                          <tr>
						    <td>
							  <table style="margin: 0px auto; padding: 0 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
							    <tbody>
								  <tr>
								     <td width="30%">
									    <img src="${CONFIG.LIVEURL}/uploads/logo.png">
									 </td>
									 <td width="70%" style="background-image:url(staff-profile.png);background-repeat: no-repeat;background-size: cover;height: 124px;font-size: 24px;color: #fff;    padding: 0px 0 0 70px; font-weight: 600;">
									     Application Form (Internal)
									 </td>
								  </tr>
								</tbody>
							  </table>
							</td>
						  </tr>
						  
						  
						              <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="100%">
                                      <p style="font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;width:48%;float: left;border-top: 1px solid #119dcd; border-bottom: 1px solid #119dcd ;padding: 0; border-right: 1px solid #119dcd;"> 
                                      <label
                                        style="width:30%;float:left;font-size: 12px;    padding-top: 10px; ">Post Applying for
                                        </label>



                                        
										<select value=${online.postapplyfor || ""} style="background-color: #e2e5f1;border: none;width:70%;float:left; height: 36px; text-indent: 13px;  color: #333; font-size: 11px;
    ">
                                          <option value="Please Select" ${online.postapplyfor == "Please Select" ? "selected" : ""}>Please Select</option>
                                      <option value="doctor" ${online.postapplyfor == "Doctor" ? "selected" : ""}>Doctor</option>
                                        <option value="nurse" ${online.postapplyfor == "Nurse" ? "selected" : ""}>Nurse</option>
                                        <option value="rgn" ${online.postapplyfor == "rgn" ? "selected" : ""} >RGN</option>
                                        <option value="odps" ${online.postapplyfor == "odps" ? "selected" : ""}>ODPS</option>
                                        <option value="social_worker"  ${online.postapplyfor == "social_worker" ? "selected" : ""}>Social Worker</option>
                                        <option value="domestic_worker"  ${online.postapplyfor == "domestic_worker" ? "selected" : ""}>Domestic Worker</option>
                                        <option value="other" ${online.postapplyfor == "other" ? "selected" : ""}>Other</option>
                                        </select>
										</p>

                     <p style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:50%;float: left;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"> 
                     <label style="width:55%;float:left;font-size: 12px;    padding-top: 10px;    text-align: center; ">How did you hear about this us? 
                                        </label>
                                        
                      <select value=${online.postapplydetails || ""} style="background-color: #e2e5f1;border: none;width:45%;float:left;  height: 36px; text-indent: 13px;  color: #333;font-size: 11px;">
                                         <option value="Please Select" ${online.postapplydetails == "Please Select" ? "selected" : ""}>Please Select</option>
                                       <option value="internetsearch" ${online.postapplydetails == "internetsearch" ? "selected" : ""}>Internet Search</option>
                                        <option value="jobcentre" ${online.postapplydetails == "jobcentre" ? "selected" : ""}>Job Centre</option>
                                        <option value="friend" ${online.postapplydetails == "friend" ? "selected" : ""}>Friend</option>
                                        <option value="newspaper" ${online.postapplydetails == "newspaper" ? "selected" : ""}>Newspaper</option>
                                        <option value="other" ${online.postapplydetails == "other" ? "selected" : ""}>Other</option>
                                        </select>
                    </p>


										 <p style="font-size: 12px; font-weight: 600; float:left; width:98%; margin:0px; padding: 5px 0;    padding-top: 19px;padding-top: 19px;border-top: 1px solid #119dcd; border-bottom: 1px solid #119dcd; padding: 0;border-right: 1px solid #119dcd; margin-top: 13px;"><label style="width:40%;float:left;padding-top: 10px;    font-size: 12px;">Date available to start</label>
                                    <input style="background-color: #e2e5f1;    border: 1px solid #e2e5f1;
   border: none;width:60%;font-size: 12px;float:left; border: 1px solid #ccc; height: 38px; text-indent: 14px;  color: #333;border:none;"
                                    type="text" value=${online.availablestartdate || ""} placeholder=""></p>
                                    
                                    </td>



                                    <td width="24%" align="right">
                                     
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
						  
						            <tr>
                            <td>
                              <table style="margin: 0px auto; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 13px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Personal Details 1</p>  

                                    </td>
                                  </tr>    
                                </tbody>
						                  </table>  
                            </td>
                        </tr> 

                        <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;padding-top:0px;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Title</label>
                                    <select value=${online.title || ""} style="font-size: 12px;background-color: #e2e5f1;border: 1px solid #e2e5f1;
    width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;">
     <option value="Please Select">Please Select</option>
                                                                             <option value="mr" ${online.title == "mr" ? "selected" : ""}>Mr.</option>
                                      <option value="mrs" ${online.title == "mrs" ? "selected" : ""}>Mrs.</option>
                                      <option value="miss" ${online.title == "miss" ? "selected" : ""}>Miss.</option>
                                      <option value="ms" ${online.title == "ms" ? "selected" : ""}>Ms.</option>
                                      <option value="dr" ${online.title == "dr" ? "selected" : ""}>Dr.</option>
                                      <option value="prof" ${online.title == "prof" ? "selected" : ""}>Prof.</option>
                                      <option value="rev" ${online.title == "rev" ? "selected" : ""}>Rev.</option>
                                    </select></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"><label style="    font-size: 12px;width:28%;float:left;padding-bottom:10px;     padding-top: 13px;
    padding-left: 5px;">First Name</label>
                                    <input value=${online.firstname || ""} style="background-color: #e2e5f1;    border: 1px solid #e2e5f1;
   width:69%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Last Name</label>
                                    <input value=${online.lastname || ""} style="background-color: #e2e5f1;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Last Name"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>




                        <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Home Phone</label>
                                    
                                    <input value=${online.homephone || ""} style="background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name">
                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:28%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Mobile No</label>
                                    <input value=${(online.mobileno.code || "") + "-" + (online.mobileno.number || "")} style="font-size: 12px;background-color: #e2e5f1;border: 1px solid #e2e5f1;width:69%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Email</label>
                                    <input value=${online.email || ""} style="    background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Last Name"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>




                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Date Of Birth</label>
                                    
                                    <input value=${online.dateofbirth || ""} style="    background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Date">
                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"><label style="    font-size: 12px;width:57%;float:left;padding-bottom:10px;     padding-top: 13px;
    padding-left: 5px;">National Insurance No</label>
                                    <input value=${online.insuranceno || ""} style="    background-color: #e2e5f1;font-size: 12px;word-break: break-word;    border: 1px solid #e2e5f1;
   width:40%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Insurance No"></p>
                                </td>
                                <td width="33.33%">
                                 
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>







                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15pxs; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Address</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 

                        <tr>
                            <td style="width: 100%; font-size: 15px;color: #000; padding: 15px 30px; font-weight: bold;">
                              Your Current Address</td>
                          </tr>


                              <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Street Address</label>
                                    <input value=${online.streetAddress || ""} style="    background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;
   width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter Street address">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Address Line 2</label>
                                    <input value=${online.addressline2 || ""} style="    background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Address Line 2"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">City</label>
                                    <input value=${online.city || ""} style="    background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter City"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">County / State / Region</label>
                                    <input value=${online.state || ""} style="background-color: #e2e5f1;font-size: 12px;     border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter State">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">ZIP / Postal Code</label>
                                    <input value=${online.zip || ""} style="background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter ZIP / Postal Code"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Country</label>
                                    <input value=${online.country || ""} style="background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Country"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15pxs; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Personal Details 2</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 

                        <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Nationality</label>
                                    <select value=${online.nationality || ""} style="     background-color: #e2e5f1;font-size: 12px;   border: 1px solid #e2e5f1;
    width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;font-size: 12px;">
                                      <option value="Please Select" ${online.nationality == "Please Select" ? "selected" : ""}>Please Select</option>
                                        <option value="british" ${online.nationality == "british" ? "selected" : ""}>British</option>
                                      <option value="eucitizen" ${online.nationality == "eucitizen" ? "selected" : ""}>EU Citizen</option>
                                      <option value="other" ${online.nationality == "other" ? "selected" : ""}>Other</option>
                                    </select></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Gender</label>
                                    <select value=${online.gender || ""} style="    background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;
    width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;font-size: 12px;">
                                     <option value="Please Select" ${online.gender == "Please Select" ? "selected" : ""}>Please Select</option>
                                      <option value="female" ${online.gender == "female" ? "selected" : ""}>Female</option>
                        <option value="male" ${online.gender == "male" ? "selected" : ""}>Male</option>
                                    </select></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Religion</label>
                                    <select value=${online.religion || ""} style="    background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;
    width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;font-size: 12px;">
                                     <option value="Please Select" ${online.religion == "Please Select" ? "selected" : ""}>Please Select</option>
                                        <option value="bhudist" ${online.religion == "bhudist" ? "selected" : ""}>Bhudist</option>
                                        <option value="christian" ${online.religion == "christian" ? "selected" : ""}>Christian</option>
                                        <option value="jewish" ${online.religion == "jewish" ? "selected" : ""}>Jewish</option>
                                        <option value="hindu" ${online.religion == "hindu" ? "selected" : ""}>Hindu</option>
                                        <option value="muslim" ${online.religion == "muslim" ? "selected" : ""}>Muslim</option>
                                        <option value="sikh" ${online.religion == "sikh" ? "selected" : ""}>Sikh</option>
                                        <option value="other" ${online.religion == "other" ? "selected" : ""}>Other</option>
                                    </select></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>




                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Race/Ethnicity</label>
                                    <select value=${online.race || ""} style="background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;
    width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;font-size: 12px;">
                                      <option value="Please Select" ${online.race == "Please Select" ? "selected" : ""}>Please Select</option>
                                      <option value="whitebritish" ${online.race == "whitebritish" ? "selected" : ""}>White British</option>
                        <option value="whiteother" ${online.race == "whiteother" ? "selected" : ""}>White(Other)</option>
                        <option value="whiteirish" ${online.race == "whiteirish" ? "selected" : ""}>White Irish</option>
                        <option value="mixesrace" ${online.race == "mixesrace" ? "selected" : ""}>Mixes Race</option>
                        <option value="indian" ${online.race == "indian" ? "selected" : ""}>Indian</option>
                        <option value="pakistani" ${online.race == "pakistani" ? "selected" : ""}>Pakistani</option>
                        <option value="bangladeshi" ${online.race == "bangladeshi" ? "selected" : ""}>Bangladeshi</option>
                        <option value="otherasian" ${online.race == "otherasian" ? "selected" : ""}>Other Asian(non-Chinese)</option>
                        <option value="blackcaribbean" ${online.race == "blackcaribbean" ? "selected" : ""}>Black Caribbean</option>
                        <option value="blackafrican" ${online.race == "blackafrican" ? "selected" : ""}>Black African</option>
                        <option value="blackothers" ${online.race == "blackothers" ? "selected" : ""}>Black(others)</option>
                        <option value="chinese" ${online.race == "chinese" ? "selected" : ""}>Chinese</option>
                        <option value="other" ${online.race == "other" ? "selected" : ""}>Other</option>
                                    </select></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Sexual Orientation</label>
                                    <select value=${online.sexualorientation || ""} style="background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;font-size: 12px;">
                                      <option value="Please Select" ${online.sexualorientation == "Please Select" ? "selected" : ""}>Please Select</option>
                                    <option value="bisexual" ${online.sexualorientation == "bisexual" ? "selected" : ""}>Bisexual</option>
                        <option value="gayman" ${online.sexualorientation == "gayman" ? "selected" : ""}>Gay Man</option>
                        <option value="gaywoman" ${online.sexualorientation == "gaywoman" ? "selected" : ""}>Gay Woman/Lesbian</option>
                        <option value="straight" ${online.sexualorientation == "straight" ? "selected" : ""}>Straight/Heterosexual</option>
                        <option value="prefer" ${online.sexualorientation == "prefer" ? "selected" : ""}>Prefer not to answer</option>
                                    </select></p>
                                </td>
                                
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15pxs; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Employment Eligibility</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 


                         <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="font-size: 12px;width:99%;float:left;padding-bottom:10px;">What visa/permit/status do you currently hold?:</label>
                                    <label style="font-size: 12px;width:99%;float:left;padding-bottom:10px;"><input type="radio" ${online.visapermit == "workingholiday" ? "checked" : ""} name="visapermit"> Working Holiday</label>
                                    <label style="font-size: 12px;width:99%;float:left;padding-bottom:10px;"><input type="radio" ${online.visapermit == "workpermit" ? "checked" : ""} name="visapermit"> Work Permit</label>
                                    <label style="font-size: 12px;width:99%;float:left;padding-bottom:10px;"><input type="radio" ${online.visapermit == "leavetoremain" ? "checked" : ""} name="visapermit"> Leave to Remain</label>
                                    <label style="font-size: 12px;width:99%;float:left;padding-bottom:10px;"><input type="radio" ${online.visapermit == "other" ? "checked" : ""} name="visapermit"> Other</label>
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>

                      ${online.visapermit === "other" ?
                `<tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>

                         <p style="font-size: 12px; font-weight: 600; float:left; width:98%; margin:0px; padding: 5px 0;    padding-top: 19px;padding-top: 19px;border-top: 1px solid #119dcd; border-bottom: 1px solid #119dcd; padding: 0;border-right: 1px solid #119dcd; margin-top: 13px;"><label style="width:40%;float:left;padding-top: 10px;    font-size: 12px;">Please State what visa / permit / you hold</label>
                                    <input style="background-color: #e2e5f1;font-size: 12px;border: 1px solid #e2e5f1;
   border: none;width:60%;float:left; border: 1px solid #ccc; height: 38px; text-indent: 14px;  color: #333;border:none;"
                                    type="text" value=${online.visapermittype || "  - "} placeholder="Enter Visa / Permit"></p>

</td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>`: ''}


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Driving Details</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 



                        <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td width="49%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;font-size: 12px;">Do you have full Driving Licence that allows you
                                      to drive in the UK?</label>
                                    <label style="font-size: 12px;width:50%;float:left;padding-bottom:10px;"><input ${online.drivinglicence == "yes" ? "checked" : ""} type="radio" name="drivinglicence" >
                                      Yes</label>
                                    <label style="font-size: 12px;width:50%;float:left;padding-bottom:10px;"><input ${online.drivinglicence == "no" ? "checked" : ""} type="radio" name="drivinglicence" > No</label>
                                  </p>
                                </td>
                                <td width="49%">
                                  <p style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:100%;float: left;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"> 
                     <label style="width:55%;float:left;font-size: 12px;    padding-bottom: 10px;    padding-top: 10px;    text-align: center; ">How would you travel to work if assigned?
                                        </label>
                                        
                      <select value=${online.traveldetails || ""} style="background-color: #e2e5f1;border: none;width:45%;float:left;  height: 36px; text-indent: 13px;  color: #333;font-size: 12px;">
                                          <option value="Please Select" ${online.traveldetails == "Please Select" ? "selected" : ""}>Please Select</option>
                                        <option value="drive" ${online.traveldetails == "drive" ? "selected" : ""}>Drive</option>
                        <option value="public" ${online.traveldetails == "public" ? "selected" : ""}>Public Transport </option>
                        <option value="willgetalist" ${online.traveldetails == "willgetalist" ? "selected" : ""}>Will get a lift</option>
                        <option value="bicycle" ${online.traveldetails == "bicycle" ? "selected" : ""}>Bicycle</option>
                        <option value="other" ${online.traveldetails == "other" ? "selected" : ""}>Other</option>
                                        </select>
                    </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>

                      ${online.drivinglicence == "yes" ?


                ` <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td width="33.33%" style="vertical-align: top";>
                                 <p style="font-weight: 600; float:left; margin:0px; padding: 5px 0;width:100%;float: left;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"> 
                     <label style="width:50%;float:left;font-size: 12px;    padding-bottom: 10px;    padding-top: 10px;    text-align: center; ">Driving Licence No:
                                        </label>
                                        
                     <input style="background-color: #e2e5f1;font-size: 12px; border: 1px solid #e2e5f1;
   border: none;width:50%;float:left; border: 1px solid #ccc; height: 38px; text-indent: 14px;  color: #333;border:none;"
                                    type="text" value=${online.drivinglicenceno || " - "} placeholder="">
                    </p>
                                </td>
                                <td width="33.33%" style="vertical-align: top";>
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;font-size: 12px;padding-left: 10px;">Do you have access to a car that you can use for
                                      work?</label>
                                    <label style="padding-left: 10px;width:45%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.caraccess == "yes" ? "checked" : ""} type="radio" name="caraccess" > Yes</label>
                                    <label style="padding-left: 10px;width:45%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.caraccess == "no" ? "checked" : ""} type="radio" name="caraccess" > No</label>
                                  </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;font-size: 12px;">Have you been banned from driving. or do you have
                                      any current endorsements on you licence?</label>
                                    <label style="width:50%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.bannedfromdriving == "yes" ? "checked" : ""} type="radio" name="bannedfromdriving" > Yes</label>
                                    <label style="width:50%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.bannedfromdriving == "no" ? "checked" : ""} type="radio" name="bannedfromdriving" > No</label>
                                  </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>


                      ${online.caraccess == "yes" ?



                  `<tr>
                        <td>
                          <table style="margin: 0px auto; padding: 0 30px 15px; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>
                              <tr>
                                <td width="49%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;font-size: 12px;"> Does you car insurance include Class 1 business
                                      insurance? (in order to use you vehicle for work you must have class 1 business insurance)</label>
                                    <label style="font-size: 12px;width:50%;float:left;padding-bottom:10px;"><input ${online.carinsurance == "yes" ? "checked" : ""} type="radio" name="carinsurance"> Yes</label>
                                    <label style="font-size: 12px;width:50%;float:left;padding-bottom:10px;"><input ${online.carinsurance == "no" ? "checked" : ""} type="radio" name="carinsurance"> No</label>
                                  </p>
                                </td>
                                <td width="49%" style="vertical-align: top";>
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;font-size: 12px;">Are all your vehicle documents up to date and
                                      valid?</label>
                                    <label style="font-size: 12px;width:50%;float:left;padding-bottom:10px;"><input ${online.vehicledocumentsvalid == "yes" ? "checked" : ""} type="radio" name="vehicledocumentsvalid"> Yes</label>
                                    <label style="font-size: 12px;width:50%;float:left;padding-bottom:10px;"><input ${online.vehicledocumentsvalid == "no" ? "checked" : ""} type="radio" name="vehicledocumentsvalid"> No</label>
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>`: ''}` : ''
              }







                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Languages</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 


<tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td width="50%" style="vertical-align: top;">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;font-size: 12px;"> English - Spoken</label>
                                    <label style="width:33.3%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.englishspoken == "fluent" ? "checked" : ""}  type="radio" name="englishspoken"> Fluent </label>
                                    <label style="width:33.3%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.englishspoken == "good" ? "checked" : ""}  type="radio" name="englishspoken"> Good </label>
                                    <label style="width:33.3%;float:left;padding-bottom:10px;font-size: 12px;"><input ${online.englishspoken == "fair" ? "checked" : ""}  type="radio" name="englishspoken"> Fair </label>
                                  </p>
                                </td>
                                <td width="50%" style="vertical-align: top;">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;">English - Written</label>
                                    <label style="width:33.3%;float:left;padding-bottom:10px;"><input ${online.englishwritten == "fluent" ? "checked" : ""}  type="radio" name="englishwritten" > Fluent</label>
                                    <label style="width:33.3%;float:left;padding-bottom:10px;"><input ${online.englishwritten == "good" ? "checked" : ""}  type="radio" name="englishwritten" > Good</label>
                                    <label style="width:33.3%;float:left;padding-bottom:10px;"><input ${online.englishwritten == "fair" ? "checked" : ""}  type="radio" name="englishwritten" > Fair</label>
                                  </p>
                                </td>
                                

                              </tr>


                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 0 30px 15px; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>


                            <tr>
                            <td width="50%"  style="vertical-align: top;">
                                 <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:46%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Other Languages Spoken</label>
                                    <input value=${online.otherlanguage || " - "} style="background-color: #e2e5f1;width:52%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Other Language"></p>
                                </td>


                                <td width="50%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;padding-top: 0;padding-left: 10px;">
                                    <label style="width:99%;float:left;padding-bottom:10px;"> Have you passed each of the academic modules of
                                      the IELTS test?</label>
                                    <label style="padding-left: 6px;vertical-align:top;width:43%;float:left;padding-bottom:10px;"><input ${online.IELTStest == "yes" ? "checked" : ""} type="radio" name="IELTStest"> yes </label>
                                    <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.IELTStest == "no" ? "checked" : ""} type="radio" name="IELTStest"> No </label>
                                    ${online.IELTStest == "yes" ?
                `<span style="width: 100%; font-size: 12px;color: #000; font-weight: normal;">Please note: you have to
                                      provide copies of all IELTS certificates held by you</span>`: ''}
                                  </p>
                                </td>

                              </tr>


                              <tr>
                                

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>



                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Next of kin details</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 100%; font-size: 14px;color: #000; padding: 0px 30px; font-weight: bold;">Name</td>
                          </tr>


                          </tbody>
                          </table>
                          </td>
                          </tr>


                          <tr>
                            <td>
                              <table style="margin: 0px auto;     padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                          <tr>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:30%;float:left;padding-bottom:10px;padding-top: 10px;">Prefix</label>
                                    <select value=${online.kinprefix || ""} style="background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;
    width:70%;float:left; border: none; height: 38px; text-indent: 14px;  color: #333;">
                                     <option value="Please Select">Please Select</option>
                                       <option value="mr" ${online.title == "mr" ? "selected" : ""}>Mr.</option>
                                      <option value="mrs" ${online.title == "mrs" ? "selected" : ""}>Mrs.</option>
                                      <option value="miss" ${online.title == "miss" ? "selected" : ""}>Miss.</option>
                                      <option value="ms" ${online.title == "ms" ? "selected" : ""}>Ms.</option>
                                      <option value="dr" ${online.title == "dr" ? "selected" : ""}>Dr.</option>
                                      <option value="prof" ${online.title == "prof" ? "selected" : ""}>Prof.</option>
                                      <option value="rev" ${online.title == "rev" ? "selected" : ""}>Rev.</option>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"><label style="    font-size: 12px;width:28%;float:left;padding-bottom:10px;     padding-top: 13px;
    padding-left: 5px;">First Name</label>
                                    <input value=${online.kinfirstname || ""}  style="font-size: 12px; background-color: #e2e5f1;    border: 1px solid #e2e5f1;
   width:69%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Last Name</label>
                                    <input value=${online.kinlastname || ""} style="font-size: 12px; background-color: #e2e5f1;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Last Name"></p>
                                </td>
                              </tr>
                              </tbody>
                              </table>
                              </td>
                              </tr>




                            <tr>
                            <td>
                              <table style="margin: 0px auto;     padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                              <tr>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Relationship to you:</label>
                                    <input value=${online.kinrelationship || ""} style=" background-color: #e2e5f1;
   border: 1px solid #e2e5f1;
   width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Relationship to you"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"><label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;     padding-top: 10px;
    padding-left: 5px;">Home / Work Phone</label>
                                    <input value=${online.kinhomephone || ""} style="background-color: #e2e5f1;
 font-size: 12px;   border: 1px solid #e2e5f1;
   width:57%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Home / Work Phone"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Mobile No</label>
                                    <input value=${(online.kinmobileno.code || "") + "-" + (online.kinmobileno.number || "")} style="background-color: #e2e5f1;
font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder=""></p>
                                </td>
                              </tr>
                              </tbody>
                              </table>
                              </td>
                              </tr>

                              <tr>
                            <td>
                              <table style="margin: 0px auto;     padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                              <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Email</label>
                                    <input value=${online.kinemail || ""} style="background-color: #e2e5f1;    border: 1px solid #e2e5f1;
   width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Email"></p>
                                </td>
                                <td width="33.33%">
                                  
                                </td>
                                <td width="33.33%">
                                 
                                </td>
                              </tr>
                              </tbody>
                              </table>
                              </td>
                              </tr>

                              <tr>
                            <td>
                              <table style="margin: 0px auto;     padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                              <tr>
                              <td style="height: 10px">

                              </td>

                              </tr>
                              </tbody>
                              </table>
                              </td>
                              </tr>





                              <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Address</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>



                        <tr>
                            <td>
                              <table style="margin: 0px auto;     padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                              <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Street Address</label>
                                    <input value=${online.kinstreetAddress || ""}  style="background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Street Address"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;"><label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;     padding-top: 10px;
    padding-left: 5px;">Address Line 2</label>
                                    <input value=${online.kinaddressline2 || ""}  style="background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;
   width:57%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Address Line 2"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">City</label>
                                    <input value=${online.kincity || ""} style="background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter City"></p>
                                </td>
                              </tr>
                              </tbody>
                              </table>
                              </td>
                              </tr>


                              <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Country / State / Region</label>
                                    <input value=${online.kinstate || ""} style="     background-color: #e2e5f1;font-size: 12px;   border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter State">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">ZIP / Postal Code</label>
                                    <input value=${online.kinzip || ""} style="    background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter ZIP / Postal Code"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Country</label>
                                    <input value=${online.kincountry || ""} style="    background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Country"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>



                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Next of kin details</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>



                        <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;"> When are you able to work? </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_mornings ? "checked" : ""} type="checkbox" name="work_mornings"> Mornings
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_evenings ? "checked" : ""} type="checkbox" name="work_evenings"> Evenings
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_afternoons ? "checked" : ""} type="checkbox" name="work_afternoons"> Afternoons
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_occasional ? "checked" : ""} type="checkbox" name="work_occasional"> Occasional
                                      Weeks </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_fulltime ? "checked" : ""} type="checkbox" name="work_fulltime"> Full Times
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_parttime ? "checked" : ""} type="checkbox" name="work_parttime"> Part Time
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_nights ? "checked" : ""} type="checkbox" name="work_nights"> Nights
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_weekends ? "checked" : ""} type="checkbox" name="work_weekends"> Weekends
                                    </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.work_anytime ? "checked" : ""} type="checkbox" name="work_anytime"> Anytime
                                    </label>

                                  </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>



                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">1st Choice</label>
                                    <input value=${online.firstchoice || ""} style="    background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter 1st Choice">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">2nd Choice</label>
                                    <input value=${online.secondchoice || ""} style="    background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter 2nd Choice"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">3rd Choice</label>
                                    <input value=${online.thirdchoice || ""} style="    background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter 3rd Choice"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>



                      <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;"> If yes, please state: </label>
                                    <textarea value=${online.workstate || ""} style="    background-color: #e2e5f1;width:99%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;"
                                    type="text" placeholder="Enter State">${online.workstate || ""}</textarea>
                                  </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                        <td style="width: 100%; font-size: 18px;color: #0b5c77; padding: 15px 30px; border-bottom: 1px solid #119dcd; font-weight: bold; text-align: center;">
                          Employment & Education </td>
                      </tr>
                      <tr>
                        <td height="10">&nbsp;</td>
                      </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 35%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Education & Employment History</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


                        <tr>
                            <td style="width: 100%; font-size: 14px;color: #000; padding: 0px 30px; font-weight: bold;">Your History</td>
                        </tr>

                      

                           <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 13px; float:left; width:100%; margin:0px; padding: 5px 0; line-height: 26px; text-align: justify;">
                                    Please ensure you complete this section even if you have a CV. The NHS states that â€œEmployment history
                                    should be recorded on an Application Form which is signedâ€ Please ensure that you leave no gaps
                                    unaccounted for and it covers full work history including your education. Please use extra paper if
                                    required. Full work history including your education Dates to and from are shown in a mm/yy format Dates
                                    are continual with NO gaps Where there have been gaps in work history please state the reason for the gaps
                                    Lists all relevant training undertaken
                                  </p>
                                  <p style="font-size: 13px; float:left; width:100%; margin:0px 0px 20px; padding: 5px 0; line-height: 26px; text-align: justify;">
                                    Please ensure you complete this section even if you have a CV. The NHS states that â€œEmployment history
                                    should be recorded on an Application Form which is signedâ€ Please ensure that you leave no gaps
                                    unaccounted for and it covers full work history including your education. Please use extra paper if
                                    required.
                                  </p>

                                  <p style="font-size: 13px; float:left; width:100%; margin:0px; padding: 0; line-height: 26px; text-align: justify;">
                                    Full work history including your education</p>
                                  <p style="font-size: 13px; float:left; width:100%; margin:0px; padding: 0; line-height: 26px; text-align: justify;">
                                    Dates to and from are shown in a mm/yy format</p>
                                  <p style="font-size: 13px; float:left; width:100%; margin:0px; padding: 0; line-height: 26px; text-align: justify;">
                                    Dates are continual with NO gaps</p>
                                  <p style="font-size: 13px; float:left; width:100%; margin:0px; padding: 0; line-height: 26px; text-align: justify;">
                                    Where there have been gaps in work history please state the reason for the gaps</p>
                                  <p style="font-size: 13px; float:left; width:100%; margin:0px; padding: 0; line-height: 26px; text-align: justify;">
                                    Lists all relevant training undertaken</p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>


                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Employment History</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


                        ${online.employmenthistorylist
                .map(
                (item, i) =>
                  `<tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Employer</label>
                                    <input value=${item.employer || ""}  style="    background-color: #e2e5f1;
font-size: 12px;    border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter Employee name">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Position</label>
                                    <input value=${item.position || ""} style="font-size: 12px;    background-color: #e2e5f1;
    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter position"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Duties</label>
                                    <input value=${item.duties || ""} style="    background-color: #e2e5f1;
font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Duties"></p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Start Date</label>
                                    <input value=${item.start_date ? moment(item.start_date).format("DD-MM-YYYY") : "-"} style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter First Name">
                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">End Date</label>
                                    <input value=${item.start_date ? moment(item.end_date).format("DD-MM-YYYY") : "-"} style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:48%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Salary on Leaving</label>
                                    <input value=${item.salaryonleaving || ""} style="font-size: 12px;    background-color: #e2e5f1;width:49%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Last Name"></p>
                                </td>
                              </tr>`
                ).join("")}


                            </tbody>
                          </table>
                        </td>
                      </tr>
                       <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Education</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>

                        ${online.educationlist
                .map(
                (item, i) =>


                  `<tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="50%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Institution</label>

                                    <input value=${item.institution || ""} style="    background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter institution">

                                    </p>
                                </td>
                                <td width="50%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Course</label>
                                    <input value=${item.course || ""} style="font-size: 12px;    background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;width:61%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter course"></p>
                                </td>
                                
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>



                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="50%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Year</label>

                                    <input value=${item.year || ""} style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter Year">

                                    </p>
                                </td>
                                <td width="50%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Grade</label>
                                    <input value=${item.grade || ""} style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;width:61%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Grade"></p>
                                </td>
                                
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>`
                ).join("")}


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding-top: 15px; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">References</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


                        <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:40%;float:left;padding-bottom:10px;"> What visa/permit/status do you currently hold?:
                                    </label>

                                  </p>
                                  <p style="font-size: 12px; float:left; width:100%; margin:0px 0px 20px; padding: 5px 0; line-height: 26px; text-align: justify;">
                                    Please supply us with two professional referees. One must be from your present or most recent employer and
                                    must be a senior grade to yourself and you must have worked for that person for a period of not less than
                                    three months duration.

                                  </p>
                                </td>


                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Reference 1</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>



                        <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Relationship</label>
                                    <input value=${online.firstrefrelationship || ""} style="background-color: #e2e5f1; font-size: 12px;    border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter Relationship">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">First Name</label>
                                    <input value=${online.firstreffirstname || ""} style="background-color: #e2e5f1; font-size: 12px;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:48%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Last Name</label>
                                    <input value=${online.firstreflastname || ""} style="background-color: #e2e5f1; font-size: 12px;width:47%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter Last Name"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>

                      <tr>
                      <td style="height: 30px;">

                      </td>

                      </tr>



                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Mobile No</label>
                                    <input value=${(online.firstrefphone.code || "") + "-" + (online.firstrefphone.number || "")} style="background-color: #e2e5f1; font-size: 12px;border: 1px solid #e2e5f1;
    width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter number">

                                    </p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Email</label>
                                    <input value=${online.firstrefemail || ""} style="background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Email"></p>
                                </td>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:48%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Confirm Email</label>
                                    <input value=${online.firstrefconfirmemail || ""} style="background-color: #e2e5f1;font-size: 12px;width:47%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter email"></p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                            <td style="width: 100%; font-size: 15px;color: #000; padding: 0px 30px; padding-top: 15px; font-weight: bold;">Addresss</td>
                        </tr>



                        <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                              <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Street Address</label>
                                    <input value=${online.firstrefstreetAddress || ""} style="background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Street address"></p>
                                </td>

                                
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">City</label>
                                    <input value=${online.firstrefcity || ""} style="background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter City"></p>
                                </td>


                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="font-size: 12px;width:60%;float:left;padding-bottom:10px;padding-top: 10px;">Country / State / Region</label>
                                    <input value=${online.firstrefstate || ""} style="background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;
    width:36%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter First Name">

                                    </p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>



                       <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                               <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">ZIP / Postal Code</label>
                                    <input value=${online.firstrefzip || ""} style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter ZIP / Postal Code"></p>
                                </td>
                                  </tr>

                                  
                               <td width="33.33%">

                               </td>
                           
                               <td width="33.33%">

                               </td>
                              
                            </tbody>
                          </table>
                        </td>
                      </tr>



                      <tr>
                      <td>
                        <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                          <tbody>

                            <tr>
                              <td width="75%">

                                <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Reference 2</p>  

                              </td>
                            </tr>    
                          </tbody>
                        </table>  
                      </td>
                  </tr>



                  <tr>
                      <td>
                        <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                          <tbody>
                  <tr>
                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                              <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Relationship</label>
                              <input value=${online.secondrefrelationship || ""} style="background-color: #e2e5f1; font-size: 12px;    border: 1px solid #e2e5f1;
width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter Relationship">

                              </p>
                          </td>
                          <td width="33.33%">
                            <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">First Name</label>
                              <input value=${online.secondreffirstname || ""} style="background-color: #e2e5f1; font-size: 12px;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                              type="text" placeholder="Enter First Name"></p>
                          </td>
                          <td width="33.33%">
                            <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label
                              style="font-size: 12px;width:48%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Last Name</label>
                              <input value=${online.secondreflastname || ""} style="background-color: #e2e5f1; font-size: 12px;width:47%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                              type="text" placeholder="Enter Last Name"></p>
                          </td>
                        </tr>



                      </tbody>
                    </table>
                  </td>
                </tr>

                <tr>
                <td style="height: 30px;">

                </td>

                </tr>



                <tr>
                      <td>
                        <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                          <tbody>
                  <tr>
                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                              <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">Mobile No</label>
                              <input value=${(online.secondrefphone.code || "") + "-" + (online.secondrefphone.number || "")} style="background-color: #e2e5f1; font-size: 12px;border: 1px solid #e2e5f1;
width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter number">

                              </p>
                          </td>
                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Email</label>
                              <input value=${online.secondrefemail || ""} style="background-color: #e2e5f1;font-size: 12px;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                              type="text" placeholder="Enter Email"></p>
                          </td>
                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label
                              style="font-size: 12px;width:48%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">Confirm Email</label>
                              <input value=${online.secondrefconfirmemail || ""} style="background-color: #e2e5f1;font-size: 12px;width:47%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                              type="text" placeholder="Enter email"></p>
                          </td>
                        </tr>



                      </tbody>
                    </table>
                  </td>
                </tr>


                <tr>
                      <td style="width: 100%; font-size: 15px;color: #000; padding: 0px 30px; padding-top: 15px; font-weight: bold;">Addresss</td>
                  </tr>



                  <tr>
                      <td>
                        <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                          <tbody>
                        <tr>
                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label style="font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Street Address</label>
                              <input value=${online.secondrefstreetAddress || ""} style="background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                              type="text" placeholder="Enter Street address"></p>
                          </td>

                          
                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label
                              style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">City</label>
                              <input value=${online.secondrefcity || ""} style="background-color: #e2e5f1;font-size: 12px;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                              type="text" placeholder="Enter City"></p>
                          </td>


                          <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                              <label style="font-size: 12px;width:60%;float:left;padding-bottom:10px;padding-top: 10px;">Country / State / Region</label>
                              <input value=${online.secondrefstate || ""} style="background-color: #e2e5f1; font-size: 12px;   border: 1px solid #e2e5f1;
width:36%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter First Name">

                              </p>
                          </td>
                        </tr>



                      </tbody>
                    </table>
                  </td>
                </tr>



                 <td>
                        <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                          <tbody>
                  <tr>
                         <td width="33.33%">
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                            <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">ZIP / Postal Code</label>
                              <input value=${online.secondrefzip || ""} style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                              type="text" placeholder="Enter ZIP / Postal Code"></p>
                          </td>
                            </tr>

                            
                         <td width="33.33%">

                         </td>
                     
                         <td width="33.33%">

                         </td>
                        
                      </tbody>
                    </table>
                  </td>
                </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Skills & Training</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>

                        <tr>
                        <td style="width: 100%; font-size: 16px;color: #0b5c77; padding: 15px 30px; border-bottom: 1px solid #119dcd; font-weight: bold; text-align: center;">
                          Skills, Experience &amp; Training </td>
                      </tr>


                      <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;"> Have you completed mandatory training within the
                                      last year </label>
                                    <label style="width:20%;float:left;padding-bottom:10px;"><input ${online.mandatory_training == "yes" ? "checked" : ""} type="checkbox" name="mandatory_training"> Yes </label>
                                    <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.mandatory_training == "no" ? "checked" : ""} type="checkbox" name="mandatory_training"> No </label>
                                  </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>

                      ${online.mandatory_training == "yes" ?

                `<tr>
                      <td>
                        <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                          <tbody>

                            <tr>
                              <td width="75%">

                                <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Mandatory Training</p>  

                              </td>
                            </tr>    
                          </tbody>
                        </table>  
                      </td>
                  </tr>


                  <tr>
                  <td>
                    <table style="margin: 0px auto; padding: 15px 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                      <tbody>

                        <tr>
                          <td>
                            <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                              <label style="width:99%;float:left;padding-bottom:10px;">Please tick if you have completed the following
                                training within the last 12 months </label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.movingandhandling ? "checked" : ""} type="checkbox" name="movingandhandling"> Moving &
                                Handling </label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.basiclifesupport ? "checked" : ""} type="checkbox" name="basiclifesupport"> Basic life
                                support</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.healthsafety ? "checked" : ""} type="checkbox" name="healthsafety"> Health and
                                Safety</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.firesafety ? "checked" : ""} type="checkbox" name="firesafety"> Fire Safety</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.firstaid ? "checked" : ""} type="checkbox" name="firstaid"> First Aid</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.infectioncontrol ? "checked" : ""} type="checkbox" name="infectioncontrol"> Infection
                                Control</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.foodsafety ? "checked" : ""} type="checkbox" name="foodsafety"> Food Safety
                                & Nutrition</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.medicationadmin ? "checked" : ""} type="checkbox" name="medicationadmin"> Medication
                                Administration</label>
                              <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.safeguardingvulnerable ? "checked" : ""} type="checkbox" name="safeguardingvulnerable">
                                Safeguarding Vulnerable Adults & Children</label>
                            </p>
                          </td>

                        </tr>

                      </tbody>
                    </table>
                  </td>
                </tr>


                <tr>
                  <td>
                    <table style="margin: 0px auto; padding: 0 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                      <tbody>

                        <tr>
                          <td>
                            <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                              <label style="width:99%;float:left;padding-bottom:10px;">Training Dates: </label>
                              <textarea style="    background-color: #e2e5f1;width:99%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;"
                                                        value=${online.trainingdates || ""} type="text">${online.trainingdates || ""}</textarea>
                            </p>
                          </td>

                        </tr>

                      </tbody>
                    </table>
                  </td>
                </tr>`: ''
              }


                      


                      <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 0 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;">Have you completed other health care training?</label>
                                    <label style="width:20%;float:left;padding-bottom:10px;"><input  ${online.healthcare_training == "yes" ? "checked" : ""}  type="checkbox" name="healthcare_training"> Yes </label>
                                    <label style="width:50%;float:left;padding-bottom:10px;"><input  ${online.healthcare_training == "no" ? "checked" : ""}  type="checkbox" name="healthcare_training"> No </label>
                                  </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>



                      ${online.healthcare_training == "yes" ?

                ` <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 50%;border-top-left-radius: 0px;border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Other Training/Courses & Qualifications</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


                        <tr>
                        <td>
                          <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                            <tbody>

                              <tr>
                                <td>
                                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                    <label style="width:99%;float:left;padding-bottom:10px;">Please check which training you have completed
                                      and the date on the notes (certificates must be provided).</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.diplomal3 ? "checked" : ""} type="checkbox" name="gender"> Diploma in
                                      Health & Social Care L3 </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.diplomal2 ? "checked" : ""} type="checkbox" name="diplomal2"> Diploma in
                                      Health & Social Care L2 </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.personalsafetymental ? "checked" : ""} type="checkbox" name="personalsafetymental"> Personal
                                      Safety (Mental Health & Learning Disability) </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.intermediatelife ? "checked" : ""} type="checkbox" name="intermediatelife">
                                      Intermediate Life Support </label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.advancedlife ? "checked" : ""} type="checkbox" name="advancedlife"> Advanced
                                      Life Support</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.complaintshandling ? "checked" : ""} type="checkbox" name="complaintshandling"> Complaints
                                      Handling</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.handlingviolence ? "checked" : ""} type="checkbox" name="handlingviolence"> Handling
                                      Violence and Aggression</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.doolsmental ? "checked" : ""} type="checkbox" name="doolsmental"> DOLLS &
                                      Mental Capacity</label>

                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.coshh ? "checked" : ""} type="checkbox" name="coshh"> COSHH</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.dataprotection ? "checked" : ""} type="checkbox" name="dataprotection"> Data
                                      Protection</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.equalityinclusion ? "checked" : ""} type="checkbox" name="equalityinclusion"> Equality &
                                      Inclusion</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.loneworkertraining ? "checked" : ""} type="checkbox" name="loneworkertraining"> Lone Worker
                                      Training</label>

                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.resuscitation ? "checked" : ""} type="checkbox" name="resuscitation">
                                      Resuscitation of the Newborn (Midwifery)</label>
                                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.interpretation ? "checked" : ""} type="checkbox" name="interpretation">
                                      Interpretation of Cardiotocograph Traces (Midwifery)</label>
                                  </p>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </td>
                      </tr>
                      
                      <tr>
                      <td>
                        <table style="margin: 0px auto; padding: 0 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                          <tbody>

                            <tr>
                              <td>
                                <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                                  <label style="width:99%;float:left;padding-bottom:10px;">Training Dates: </label>
                                  <textarea value=${online.trainindates2 || ""} style="    background-color: #e2e5f1;width:99%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;"
                                  type="text">${online.trainindates2 || ""}</textarea>
                                </p>
                              </td>

                            </tr>

                          </tbody>
                        </table>
                      </td>
                    </tr>`: ''}

                       

                      <tr>
                        <td style="width: 100%; font-size: 18px;color: #0b5c77; padding: 15px 30px; border-bottom: 1px solid #119dcd; font-weight: bold; text-align: center;">
                          Declarations & Terms </td>
                      </tr>
                               

                      <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Health Declaration</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 



                        <tr>
                <td>
                  <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>
                        <td width="33.33%" style="vertical-align:top;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Do you or have ever suffered from long term illness?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.sufferedlongtermillness == "yes" ? "checked" : ""} style="font-size: 12px;" type="radio" name="sufferedlongtermillness" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.sufferedlongtermillness == "no" ? "checked" : ""} style="font-size: 12px;" type="radio" name="sufferedlongtermillness" > No</label>
                          </p>
                        </td>
                        <td width="33.33%" style="vertical-align:top;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Have you ever required sick leave for a back or neck
                              injury?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.leaveforneckinjury == "yes" ? "checked" : ""} style="font-size: 12px;" type="radio" name="leaveforneckinjury" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.leaveforneckinjury == "no" ? "checked" : ""} style="font-size: 12px;" type="radio" name="leaveforneckinjury" > No</label>
                          </p>
                        </td>
                        <td width="33.33%" style="vertical-align:top;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Do you suffer with any back or neck injuries?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.neckinjuries == "yes" ? "checked" : ""} style="font-size: 12px;" type="radio" name="neckinjuries" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.neckinjuries == "no" ? "checked" : ""} style="font-size: 12px;" type="radio" name="neckinjuries" > No</label>
                          </p>
                        </td>

                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>


              <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>
                        <td width="33.33%" >
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Have you been in contact with anyone who is suffering
                              from a contagious illness within the last six weeks?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.sixweeksillness == "yes" ? "checked" : ""} type="radio" style="font-size: 12px;" name="sixweeksillness" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.sixweeksillness == "no" ? "checked" : ""} type="radio" style="font-size: 12px;" name="sixweeksillness" > No</label>
                          </p>
                        </td>
                        <td width="33.33%" style="vertical-align: top;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Do you suffer with a communicable disease?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.communicabledisease == "yes" ? "checked" : ""} type="radio" name="communicabledisease" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.communicabledisease == "no" ? "checked" : ""} type="radio" name="communicabledisease" > No</label>
                          </p>
                        </td>
                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Are you currently receiving active medical
                              attention?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.medicalattention == "yes" ? "checked" : ""} type="radio" name="medicalattention" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.medicalattention == "no" ? "checked" : ""} type="radio" name="medicalattention" > No</label>
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>
                      <tr>
                        <td>
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> If you have answered 'yes' to any of the above,
                              please give details: </label>
                            <textarea value=${online.healthdeclarationdetails || ""} style="background-color: #e2e5f1;width:99%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;"
                            type="text">${online.healthdeclarationdetails || ""}</textarea>
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              


              <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>
                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Are you registered disabled?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.registereddisabled == "yes" ? "checked" : ""} type="radio" name="registereddisabled" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.registereddisabled == "no" ? "checked" : ""} type="radio" name="registereddisabled" > No</label>
                          </p>
                        </td>
                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">How many days have you been absent from work due to
                              illness in the last 12 months?:</label>
                            <input style="font-size: 12px;    background-color: #e2e5f1;width:99%;float:left; height: 38px; text-indent: 14px;  color: #333;border:1px solid #ccc;" type="text"
                                              value=${online.absentfromwork || ""} placeholder="Enter Field"> </p>
                        </td>


                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
              <td style="height: 50px;">

              </td>
              </tr>


              <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0 30px; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>
                        <td>
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> State reason(s) for absence: </label>
                            <textarea style="font-size: 12px;    background-color: #e2e5f1;width:99%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;" value=${online.statereason || ""} type="text">${online.statereason || ""}</textarea>
                          </p>
                        </td>

                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>


              <tr>
                            <td style="width: 100%; font-size: 15px;color: #119dcd; padding: 0px 30px; padding-top: 15px; font-weight: bold;">GP Details</td>
                        </tr>

                         <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                        <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:40%;float:left;padding-bottom:10px;padding-top: 10px;">GP Name</label>
                                    
                                    <input value=${online.GPname || ""} style=" font-size: 12px;    border: 1px solid #e2e5f1;
    background-color: #e2e5f1;   border: 1px solid #e2e5f1; width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter GP Name">
                                    </p>
                                </td>
                                </tr>
                                </tbody>
                                </table>
                                </td>
                                </tr>



                                <tr>
                            <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 0px 30px; padding-top: 15px; font-weight: bold;">GP Address</td>
                        </tr>



                        <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                              <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">Street Address</label>
                                    <input value=${online.GPstreetAddress || ""}  style="font-size: 12px;    background-color: #e2e5f1;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter Street Address"></p>
                                </td>

                                
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;    border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label
                                    style="font-size: 12px;width:27%;float:left;padding-bottom: 10px;padding-top: 12px;padding-left: 5px;">City</label>
                                    <input value=${online.GPcity || ""} style="font-size: 12px; background-color: #e2e5f1;width:70%;float:left; border: none;height: 38px; text-indent: 14px;color: #333;"
                                    type="text" placeholder="Enter City"></p>
                                </td>


                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                    <label style="    font-size: 12px;width:60%;float:left;padding-bottom:10px;padding-top: 10px;">Country / State / Region</label>
                                    <input value=${online.GPstate || ""} style="font-size: 12px; background-color: #e2e5f1;    border: 1px solid #e2e5f1;
    width:40%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;" type="text" placeholder="Enter State">

                                    </p>
                                </td>
                              </tr>



                            </tbody>
                          </table>
                        </td>
                      </tr>


                      <tr>
                            <td>
                              <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>
                              <tr>
                                <td width="33.33%">
                                  <p style="font-size: 12px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;border-top: 1px solid #119dcd;border-bottom: 1px solid #119dcd;padding: 0;border-right: 1px solid #119dcd;">
                                  <label style="    font-size: 12px;width:37%;float:left;padding-bottom:10px;     padding-top: 13px;padding-left: 5px;">ZIP / Postal Code</label>
                                    <input value=${online.GPzip || ""} style=" font-size: 12px;background-color: #e2e5f1;    border: 1px solid #e2e5f1;width:60%;float:left; border: none;height: 38px; text-indent: 14px;  color: #333;"
                                    type="text" placeholder="Enter First Name"></p>
                                </td>

                                <td width="33.33%" style="padding-left: 10px;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">May we contact your Doctor for health check?:</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.contactDoctor == "yes" ? "checked" : ""} type="radio" name="contactDoctor" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.contactDoctor == "no" ? "checked" : ""} type="radio" name="contactDoctor" > No</label>
                          </p>
                          </p>
                        </td width="33.33%">

                        <td>

                        </td>

                                </tr>
                                </tbody>
                                </table>
                                </td>
                                </tr>



                                <tr>
                <td>
                  <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>
                        <td>
                          <p style="font-size: 13px; float:left; width:100%; margin:0px 0px 20px; padding: 5px 0; line-height: 26px; text-align: justify;">
                            The above information will be held in strict confidence. If you aware of any health issue that you feel may
                            affect your ability to undertake responsibilities of the post it is your responsibility to inform the Care
                            Manager immediately. Again any details discussed in the meeting will be held in strict confidence.
                          </p>
                          <p style="font-size: 13px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="float:left;padding-bottom:10px;"><input type="radio" name="GPhealthdetails" ${online.GPhealthdetails ? "checked" : ""}> I understand that
                              my GP may be contacted for details about my health. </label>

                          </p>

                        </td>


                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>


              <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Medical History</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr> 


                        <tr>
                            <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 10px 30px;font-weight: bold;">All staff groups complete this section</td>
                        </tr>


                         <tr>
                <td>
                  <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>


                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Do you have any illness/impairment/disability
                              (physical or psychological) which may affect your work? </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="medicalillness" ${online.medicalillness == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="medicalillness" ${online.medicalillness == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Have you ever had any illness/impairment/disability
                              which may have been caused or made worse by your work? </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="medicalillnesscaused" ${online.medicalillnesscaused == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="medicalillnesscaused" ${online.medicalillnesscaused == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>


                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>


                <tr>
                 <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>


                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Are you having, or waiting for treatment (including
                              medication) or investigations at present? If your answer is yes, please provide further details of the
                              condition, treatment and dates? </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="treatment" ${online.treatment == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="treatment" ${online.treatment == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="50%" style="vertical-align: top;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Do you think you may need any adjustments or
                              assistance to help you to do the job? </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="needanyadjustments" ${online.needanyadjustments == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="needanyadjustments" ${online.needanyadjustments == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>


                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>

              ${online.needanyadjustments == "yes" ?

              `<tr>
                 <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>
                      <tr>
                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                             <textarea style="font-size: 12px;    background-color: #e2e5f1;width:60%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;" value=${online.needanyadjustmentsnotes || ""} type="text">${online.needanyadjustmentsnotes || ""}</textarea>
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>`:''}


              <tr>
                            <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 10px 30px;font-weight: bold;">Tuberculosis</td>
                        </tr>


                         <tr>
                            <td style="width: 100%; font-size: 13px;padding: 10px 30px;"><p>Clinical diagnosis and management of tuberculosis, and measures for its prevention and control (NICE 2006)</p></td>
                        </tr>


              <tr>
                <td>
                  <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>


                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Have you lived continuously in the UK for the last 5
                              years </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="livedUK" ${online.livedUK == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="livedUK" ${online.livedUK == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>

                        ${online.livedUK == "no" ?
                `<td width="33.33%">
                        <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                        <label style="width:99%;float:left;padding-bottom:10px;"> Countries Listed </label>
                        <textarea style="font-size: 12px;    background-color: #e2e5f1;width:60%;float:left; height: 100px; text-indent: 14px;  color: #333;border:1px solid #119dcd;" value=${online.livedUKnotes || ""} type="text">${online.livedUKnotes || ""}</textarea>
                        </p>
                        </td>`: ''}

                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Have you had a BCG vaccination in relation to
                              Tuberculosis? </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="BCGvaccination" ${online.BCGvaccination == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="BCGvaccination" ${online.BCGvaccination == "no" ? "checked" : ""}> No</label>
                          </p>
                       </td>
                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>






              <tr>
              <td>
                <table style="margin: 0px auto; padding: 15px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                  <tbody>
                    <tr>

                    ${online.BCGvaccination == "yes" ?
                `  <td width="30%">
                        <label style="width:40%;float:left;padding-top: 10px;    font-size: 12px;">BCG Date Stated</label>
                                      <input style="background-color: #e2e5f1;    border: 1px solid #e2e5f1;
    border: none;width:60%;font-size: 12px;float:left; border: 1px solid #ccc; height: 38px; text-indent: 14px;  color: #333;border:none;"
                                      type="text" value=${online.BCGvaccinationdate || ""}>
                          </td>`: ''}

                      

                      <td width="33.33%">
                        <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                        <label style="width:99%;float:left;padding-bottom:10px;"> A cough which has lasted for more than 3 weeks</label>
                        <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="cough" ${online.cough == "yes" ? "checked" : ""}> Yes</label>
                        <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="cough" ${online.cough == "no" ? "checked" : ""}> No</label>
                       </p>
                     </td>
                    </tr>

                  </tbody>
                </table>
              </td>
            </tr>














               <tr>
              <td style="height: 60px;">

              </td>
              </tr>


               <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>


                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Unexplained weight loss </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="weightloss" ${online.weightloss == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="weightloss" ${online.weightloss == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Unexplained fever</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="fever" ${online.fever == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="fever" ${online.fever == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;">Have you had tuberculosis (TB) or been in recent
                              contact with open TB</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="TB" ${online.TB == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="TB" ${online.TB == "no" ? "checked" : ""}> No</label>
                          </p>  
                        </td>

                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>

             <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Chicken Pox or Shingles</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


             <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>


                        <td width="33.33%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Have you ever had chicken pox or shingles? </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="chickenpox" ${online.chickenpox == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="chickenpox" ${online.chickenpox == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>

                        ${online.chickenpox == "yes" ?
                `  <td width="33.33%">
                        <label style="width:40%;float:left;padding-top: 10px;    font-size: 12px;">Date Stated</label>
                                      <input style="background-color: #e2e5f1;    border: 1px solid #e2e5f1;
    border: none;width:60%;font-size: 12px;float:left; border: 1px solid #ccc; height: 38px; text-indent: 14px;  color: #333;border:none;"
                                      type="text" value=${online.chickenpoxdate || ""}>
                          </td>`: ''}

                        <td width="33.33%">

                        </td>

                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                            <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 10px 30px;font-weight: bold;">Immunisation History</td>
                        </tr>

                        <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> The
                  Disclosure and Barring Service (DBS formerly Criminal Records Bureau CRB) is the executive agency of the Home
                  Office responsible for conducting checks on criminal records. We are registered body for receipt of DBS disclosure
                  information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information
                  recruitment decisions which require DBS checks to be made on all staff. It is a condition of proceeding with your
                  application that you apply for a DBS disclosure check. The disclosure will be compared with the information given
                  below and any inconsistencies could invalidate your application or lead to the cancellation of your registration
                  with us. </td>
              </tr>


                <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>

                      <tr>


                        <td width="25%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Triple vaccination as a child (Diptheria / Tetanus /
                              Whooping cough)? </label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="triplevaccination" ${online.triplevaccination == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="triplevaccination" ${online.triplevaccination == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="25%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Polio</label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="polio" ${online.polio == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="polio" ${online.polio == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="25%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Tetanus </label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="tetanus" ${online.tetanus == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="tetanus" ${online.tetanus == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>
                        <td width="25%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Hepatitis B </label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="hepatitisB" ${online.hepatitisB == "yes" ? "checked" : ""}> Yes</label>
                            <label style="width:99%;float:left;padding-bottom:10px;"><input type="radio" name="hepatitisB" ${online.hepatitisB == "no" ? "checked" : ""}> No</label>
                          </p>
                        </td>

                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>


              <tr>
                            <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 10px 30px;font-weight: bold;">Varicella</td>
                        </tr>

                        <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> You
                  must provide a written statement to confirm that you have had chicken pox or shingles however we strongly advise
                  that you provide serology test result showing varicella immunity </td>
              </tr>


<tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Tuberculosis </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> We
                  require an occupational health/GP certificate of a positive scar or a record of a positive skin test result (Do not
                  Self Declare)</td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Rubella, Measles &
                  Mumps </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;">
                  Certificate of â€œtwoâ€ MMR vaccinations or proof of a positive antibody for Rubella Measles & Mumps</td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;">Hepatitis B </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> You
                  must provide a copy of the most recent pathology report showing titre levels of 100lu/l or above </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Hepatitis B Surface
                  Antigen </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;">
                  Evidence of a negative Surface Antigen Test Report must be an identified validated sample. (IVS) </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Hepatitis C </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;">
                  Evidence of a negative antibody test Report must be an identified validated sample. (IVS) </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> HIV </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;">
                  Evidence of a negative antibody test Report must be an identified validated s ample. (IVS) </td>
              </tr>
              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Exposure Prone
                  Procedures </td>
              </tr>
              <tr>
                <td width="33.3%" style="float: left; padding: 15px 30px;">
                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                    <label style="width:99%;float:left;padding-bottom:10px;"> Will your role involve Exposure Prone Procedures </label>
                    <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="proneProcedures" ${online.proneProcedures == "yes" ? "checked" : ""}> Yes</label>
                    <label style="width:50%;float:left;padding-bottom:10px;"><input type="radio" name="proneProcedures" ${online.proneProcedures == "no" ? "checked" : ""}> No</label>
                  </p>
                </td>

                <td width="33.3%">
                </td>
              </tr>


              <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Chicken Pox or Shingles</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


                      

              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Disclosure Barring
                  Service (DBS) </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> The
                  Disclosure and Barring Service (DBS â€“ formerly Criminal Records Bureau CRB) is the executive agency of the Home
                  Office responsible for conducting checks on criminal records. We are registered body for receipt of DBS disclosure
                  information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information
                  recruitment decisions which require DBS checks to be made on all staff. It is a condition of proceeding with your
                  application that you apply for a DBS disclosure check. The disclosure will be compared with the information given
                  below and any inconsistencies could invalidate your application or lead to the cancellation of your registration
                  with us </td>
              </tr>


              <tr>
                <td>
                  <table style="margin: 0px auto; padding: 0px 30px 0; border-spacing: 0; border-collapse: initial; width: 100%;">
                    <tbody>
                      <tr>

                        <td width="50%" style="vertical-align: top;">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Have you been convicted of a criminal offence?
                            </label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.criminaloffence == "yes" ? "checked" : ""} type="radio" name="criminaloffence" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.criminaloffence == "no" ? "checked" : ""} type="radio" name="criminaloffence" > No</label>
                          </p>
                        </td>
                        <td width="50%">
                          <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                            <label style="width:99%;float:left;padding-bottom:10px;"> Have you ever been cautioned or issued with a formal
                              warning for a criminal offence?</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.warningcriminaloffence == "yes" ? "checked" : ""} type="radio" name="warningcriminaloffence" > Yes</label>
                            <label style="width:50%;float:left;padding-bottom:10px;"><input ${online.warningcriminaloffence == "no" ? "checked" : ""} type="radio" name="warningcriminaloffence" > No</label>
                          </p>
                        </td>


                      </tr>

                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style=" padding: 15px 30px;">
                  <p style="font-size: 13px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                    <label style="width:99%;float:left;padding-bottom:10px;"> <input ${online.DBSdetails ? "checked" : ""} type="checkbox" name=""> I confirm the above is
                      true and I understand that a DBS check will be sort in the event of a successful application. </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Rehabilitation of
                  Offenders Act 1974 and Criminal Records </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> By the
                  virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section
                  4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is concerned with the
                  provision of health services and which is of such a kind to enable the holder to have access to persons in receipt
                  of such services in the course of his/her normal duties. You should there force list all offences below even if you
                  believe them to be â€˜spentâ€™ or â€˜out of dateâ€™ for some other reason. </td>
              </tr>


              <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Right To Work</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>
<tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;">It is a
                  legal requirement that before any offer of work can be made all candidates provide the company with confirmation of
                  their eligibility to work in the UK by providing one of the original documents detailed below.</td>
              </tr>
<tr>
                <td style=" padding: 15px 30px;">
                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">
                    <label style="width:99%;float:left;padding-bottom:10px;"> A passport which describes the holder as a British
                      Citizen or as having a right of abode in the United Kingdom or a passport or other </label>
                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.passport == "britishcitizen" ? "checked" : ""} type="radio" name="british" > travel
                      document to show that the holder has IDENFINITE LEAVE TO REMAIN in the United Kingdom and is not precluded from
                      taking the work in question.</label>
                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.passport == "state" ? "checked" : ""} type="radio" name="british" > A passport or
                      identity card issued by a State which is party to the European Union and EEA Agreement and which describes the
                      holder as a national or a state which is a arty to that agreement.</label>
                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.passport == "homeoffice" ? "checked" : ""} type="radio" name="british" > A letter
                      issued by the Home Office or the Department of Education and Employment indicating that the person named in the
                      letter has permission to take agency work in question or a biometric residence permit. </label>
                  </p>
                </td>
              </tr>

              <tr>
                            <td>
                              <table style="margin: 0px auto;border-spacing: 0; border-collapse: initial; width: 100%;vertical-align:top;">
                                <tbody>

                                  <tr>
                                    <td width="75%">

                                      <p style="font-size: 14px;text-transform: uppercase;    background: #119dcd;padding: 12px;border-radius: 20px;width: 30%;border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;color: white;    margin: 15px 0;">Work Time Directives</p>  

                                    </td>
                                  </tr>    
                                </tbody>
                              </table>  
                            </td>
                        </tr>


              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> By the
                  virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section
                  4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is concerned with the
                  provision of health services and which is of such a kind to enable the holder to have access to persons in receipt
                  of such services in the course of his/her normal duties. You should there force list all offences below even if you
                  believe them to be â€˜spentâ€™ or â€˜out of dateâ€™ for some other reason.</td>
              </tr>
               <tr>
                <td style=" padding: 15px 30px;">
                  <p style="font-size: 14px; font-weight: 600; float:left; width:100%; margin:0px; padding: 5px 0;">

                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.workhours == "no" ? "checked" : ""} type="radio" name="gender" > I DO NOT wish
                      to work more than 48 hours per week</label>
                    <label style="width:99%;float:left;padding-bottom:10px;"><input ${online.workhours == "yes" ? "checked" : ""} type="radio" name="gender" > I DO wish to
                      work more than 48 hours per week</label>

                  </p>
                </td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 14px;color: #119dcd; padding: 15px 30px;font-weight: bold;"> Registration form
                  Declaration</td>
              </tr>

              <tr>
                <td style="width: 100%; font-size: 13px; padding: 15px 30px;    line-height: 24px;     text-align: justify;"> I
                  declare that all information given in this registration form is to the best of my knowledge complete and accurate
                  in all respects and that I am eligible to work in the UK.</td>
              </tr>


						  

                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
              </table>`;
            data.status = 1;
            data.response = html;
            callback(data);
          } else {
            data.response = "Employee ID Not Available";
            callback(data);
          }
        } else {
          data.response = "Employee ID Not Available";
          callback(data);
        }
      }
    });
  };

  router.formupdate = (req, res) => {
    const employeeid = req.params.loginId;
    const data = {};
    data.status = 0;
    req.checkBody("forpage", "Invalid Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const forpage = Number(req.body.forpage);
    const det = req.body;
    let tab;
    const update = tab => {
      if (tab) {
        db.UpdateDocument("employees", { _id: employeeid }, tab, {}, (err, updatedoc) => {
          if (err || !updatedoc) {
            console.log("ERROR",err)
            data.response = "Unable to save your form data";
            res.send(data);
          } else {
            data.status = 1;
            data.response = "Data Submitted";
            res.send(data);
          }
        });
      }
    };
    if (employeeid) {
      if (forpage === 1) {
        if (det.postapplyfor && det.availablestartdate && det.postapplydetails && det.personaldetails && det.employmenteligibility && det.drivingdetails && det.langauages && det.nextofkindetails && det.aboutyourwork) {
          let attach;
          if (req.files && typeof req.files.UploadIDPhoto !== "undefined") {
            if (req.files.UploadIDPhoto.length > 0) {
              attach = attachment.get_attachment(req.files.UploadIDPhoto[0].destination, req.files.UploadIDPhoto[0].filename);
              tab = {
                $set: {
                  "onlineform.postapplyfor": det.postapplyfor,
                  "onlineform.availablestartdate": det.availablestartdate,
                  "onlineform.postapplydetails": det.postapplydetails,
                  "onlineform.personaldetails": det.personaldetails,
                  "onlineform.employmenteligibility": det.employmenteligibility,
                  "onlineform.drivingdetails": det.drivingdetails,
                  "onlineform.langauages": det.langauages,
                  "onlineform.nextofkindetails": det.nextofkindetails,
                  "onlineform.aboutyourwork": det.aboutyourwork,
                  "onlineform.UploadIDPhoto": attach
                }
              };
            }
          } else {
            tab = {
              $set: {
                "onlineform.postapplyfor": det.postapplyfor,
                "onlineform.availablestartdate": det.availablestartdate,
                "onlineform.postapplydetails": det.postapplydetails,
                "onlineform.personaldetails": det.personaldetails,
                "onlineform.employmenteligibility": det.employmenteligibility,
                "onlineform.drivingdetails": det.drivingdetails,
                "onlineform.langauages": det.langauages,
                "onlineform.nextofkindetails": det.nextofkindetails,
                "onlineform.aboutyourwork": det.aboutyourwork
              }
            };
          }
          update(tab);
        } else {
          data.response = "Form Fields Incomplete";
          res.send(data);
        }
      } else if (forpage === 2) {
        let attachCV;
        if (det.reference1 && det.reference2 && det.employmenthistory && det.employmenthistory.length > 0 && det.education && det.education.length > 0) {
          if (req.files && typeof req.files.UploadCV !== "undefined") {
            if (req.files.UploadCV.length > 0) {
              attachCV = attachment.get_attachment(req.files.UploadCV[0].destination, req.files.UploadCV[0].filename);
              tab = {
                $set: {
                  "onlineform.reference1": det.reference1,
                  "onlineform.reference2": det.reference2,
                  "onlineform.employmenthistory": det.employmenthistory,
                  "onlineform.education": det.education,
                  "onlineform.UploadCV": attachCV
                }
              };
            }
          } else {
            tab = {
              $set: {
                "onlineform.reference1": det.reference1,
                "onlineform.reference2": det.reference2,
                "onlineform.employmenthistory": det.employmenthistory,
                "onlineform.education": det.education
              }
            };
          }
          update(tab);
        } else {
          data.response = "Form Fields Incomplete";
          res.send(data);
        }
      } else if (forpage === 3) {
        tab = {
          $set: {
            "onlineform.mandatory_training": det.mandatory_training || "",
            "onlineform.movinghandling": det.movinghandling || 0,
            "onlineform.basiclifesupport": det.basiclifesupport || 0,
            "onlineform.healthsafety": det.healthsafety || 0,
            "onlineform.firesafety": det.firesafety || 0,
            "onlineform.firstaid": det.firstaid || 0,
            "onlineform.infectioncontrol": det.infectioncontrol || 0,
            "onlineform.foodsafety": det.foodsafety || 0,
            "onlineform.medicationadmin": det.medicationadmin || 0,
            "onlineform.safeguardingvulnerable": det.safeguardingvulnerable || 0,
            "onlineform.trainingdates": det.trainingdates || "",
            "onlineform.healthcare_training": det.healthcare_training || "",
            "onlineform.diplomal3": det.diplomal3 || 0,
            "onlineform.diplomal2": det.diplomal2 || 0,
            "onlineform.personalsafetymental": det.personalsafetymental || 0,
            "onlineform.intermediatelife": det.intermediatelife || 0,
            "onlineform.advancedlife": det.advancedlife || 0,
            "onlineform.complaintshandling": det.complaintshandling || 0,
            "onlineform.handlingviolence": det.handlingviolence || 0,
            "onlineform.doolsmental": det.doolsmental || 0,
            "onlineform.coshh": det.coshh || 0,
            "onlineform.dataprotection": det.dataprotection || 0,
            "onlineform.equalityinclusion": det.equalityinclusion || 0,
            "onlineform.loneworkertraining": det.loneworkertraining || 0,
            "onlineform.resuscitation": det.resuscitation || 0,
            "onlineform.interpretation": det.interpretation || 0,
            "onlineform.trainindates2": det.trainindates2 || ""
          }
        };
        update(tab);
      } else if (forpage === 4) {
        /* let attach1, attach2, attach3, attach4, attach5;
        // if (det.uploadcertificate && det.uploadpassport && det.uploadaddress && det.uploaddbs) {
        if (req.files && typeof req.files.uploadcertificate !== "undefined" && typeof req.files.uploadpassport !== "undefined" && typeof req.files.uploadaddress !== "undefined" && typeof req.files.uploaddbs !== "undefined") {
          if (req.files.uploadcertificate.length > 0 && req.files.uploadpassport.length > 0 && req.files.uploadaddress.length > 0 && req.files.uploaddbs.length > 0) {
            console.log("req.files ", req.files);
            attach1 = attachment.get_attachment(req.files.uploadcertificate[0].destination, req.files.uploadcertificate[0].filename);
            attach2 = attachment.get_attachment(req.files.uploadpassport[0].destination, req.files.uploadpassport[0].filename);
            attach3 = attachment.get_attachment(req.files.uploadaddress[0].destination, req.files.uploadaddress[0].filename);
            attach4 = attachment.get_attachment(req.files.uploaddbs[0].destination, req.files.uploaddbs[0].filename);
            attach5 = attachment.get_attachment(req.files.uploadothers[0].destination, req.files.uploadothers[0].filename);
            tab = {
              $set: {
                "onlineform.uploadcertificate": attach1,
                "onlineform.uploadpassport": attach2,
                "onlineform.uploadaddress": attach3,
                "onlineform.uploaddbs": attach4,
                "onlineform.uploadothers": attach5
              }
            };
            // update(tab);
          } else {
            data.response = "Form Fields Incomplete";
            res.send(data);
          }
        } else {
          data.response = "Form Fields Incomplete";
          res.send(data);
        } */
      } else if (forpage === 5) {
        if (det.healthdeclaration && det.gpdetails && det.medicalhistory && det.tuberculosis && det.chickenpoxorshingles && det.immunisationhistory && det.declaration && det.righttowork && det.worktimedirectives) {
          tab = {
            $set: {
              "onlineform.healthdeclaration": det.healthdeclaration,
              "onlineform.gpdetails": det.gpdetails,
              "onlineform.medicalhistory": det.medicalhistory,
              "onlineform.tuberculosis": det.tuberculosis,
              "onlineform.chickenpoxorshingles": det.chickenpoxorshingles,
              "onlineform.immunisationhistory": det.immunisationhistory,
              "onlineform.declaration": det.declaration,
              "onlineform.righttowork": det.righttowork,
              "onlineform.worktimedirectives": det.worktimedirectives
            }
          };
          update(tab);
        } else {
          data.response = "Form Fields Incomplete";
          res.send(data);
        }
      }
    } else {
      data.response = "Employee ID Not Available";
      res.send(data);
    }
  };
  router.savefiles = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("filefor", "Invalid Input").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const Update_api = datas => {
      db.UpdateDocument("employees", { _id: req.params.loginId }, datas, {}, (err, updatedoc) => {
        if (err || !updatedoc) {
          data.response = "Unable to save your file";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "File Saved";
          res.send(data);
        }
      });
    };
    if (req.files && req.files.uploadfiles && req.files.uploadfiles.length > 0) {
      const files_data = { path: attachment.get_attachment(req.files.uploadfiles[0].destination, req.files.uploadfiles[0].filename), name: req.files.uploadfiles[0].originalname };
      switch (req.body.filefor) {
        case "certificate":
          Update_api({ $push: { "onlineform.uploadcertificate": files_data } });
          break;
        case "passport":
          Update_api({ $push: { "onlineform.uploadpassport": files_data } });
          break;
        case "address":
          Update_api({ $push: { "onlineform.uploadaddress": files_data } });
          break;
        case "dbs":
          Update_api({ $push: { "onlineform.uploaddbs": files_data } });
          break;
        case "other":
          Update_api({ $push: { "onlineform.uploadothers": files_data } });
          break;
        default:
          break;
      }
    } else {
      data.response = "Unable to save your file";
      res.send(data);
    }
  };
  router.deletefiles = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("filefor", "Invalid Path").notEmpty();
    req.checkBody("filepath", "Invalid File").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const Update_api = (datas, data_path) => {
      db.UpdateDocument("employees", { _id: req.params.loginId }, datas, {}, (err, updatedoc) => {
        if (err || !updatedoc) {
          data.response = "Unable to delete your file";
          res.send(data);
        } else {
          data.status = 1;
          data.response = "File Deleted";
          res.send(data);
          unlinkAsync(data_path);
        }
      });
    };
    const file_p = req.body.filepath;
    if (req.body.filepath) {
      switch (req.body.filefor) {
        case "certificate":
          Update_api({ $pull: { "onlineform.uploadcertificate.path": { path: file_p } } }, file_p);
          break;
        case "passport":
          Update_api({ $pull: { "onlineform.uploadpassport.path": { path: file_p } } }, file_p);
          break;
        case "address":
          Update_api({ $pull: { "onlineform.uploadaddress.path": { path: file_p } } }, file_p);
          break;
        case "dbs":
          Update_api({ $pull: { "onlineform.uploaddbs.path": { path: file_p } } }, file_p);
          break;
        case "other":
          Update_api({ $pull: { "onlineform.uploadothers.path": { path: file_p } } }, file_p);
          break;
        default:
          break;
      }
    } else {
      data.response = "Unable to delete your file";
      res.send(data);
    }
  };
  router.formsubmit = (req, res) => {
    const employeeid = req.params.loginId;
    const employeename = req.params.loginData.username;
    const agency = req.params.loginData ? req.params.loginData.agency : false;
    const data = {};
    data.status = 0;
    req.checkBody("form_status", "Invalid Form").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const form_status = req.body.form_status;
    if (employeeid && agency) {
      db.UpdateDocument("employees", { _id: employeeid }, { $set: { "onlineform.submitted_date": new Date(), "onlineform.form_status": 1 } }, {}, (err, updateDoc) => {
        if (err || !updateDoc) {
          data.response = "Unable to submit your data";
          return res.send(data);
        } else {
          const message = "new_online_form";
          const options = {
            emp_id: employeeid
          };
          push.addnotification(agency, message, "new_online_form", options, "AGENCY", (err, response, body) => { });
          db.GetOneDocument("agencies", { _id: agency }, {}, {}, (err, AgencyData) => {
            if (AgencyData) {
              if (AgencyData.settings.notifications.email) {
                // Mail Sent To All
                const EID = req.params.loginId;
                GetOnlineForm(EID, result => {
                  if (result.status === 1) {
                    pdf.create(result.response, pdfoptions).toFile("./uploads/pdf/onlineform.pdf", function (err, document) {
                      if (err) {
                        return res.send(err);
                      } else {
                        data.status = 1;
                        data.response = "Form Submitted Successfully";
                        res.send(data);
                        const mailData = {};
                        mailData.template = "newformrequest";
                        mailData.to = AgencyData.company_email;
                        mailData.html = [];
                        mailData.html.push({ name: "username", value: AgencyData.company_name });
                        mailData.html.push({ name: "employee", value: employeename });
                        mailData.attachments = [
                          {
                            filename: "OnlineForm.pdf",
                            path: "./uploads/pdf/onlineform.pdf",
                            contentType: "application/pdf"
                          }
                        ];
                        mailcontent.sendmail(mailData, (err, response) => {
                          // console.log("mail err,mail response", err, response);
                        });
                      }
                    });
                  } else {
                    data.response = result.response;
                    res.send(data);
                  }
                });
              }
              if (AgencyData.settings.notifications.sms) {
                // SMS Send To All
                const smsto = AgencyData.company_phone.code + AgencyData.company_phone.number;
                const smsmessage = `Hi ${AgencyData.company_name} , You have got New Application Request from ${employeename}.`;
                twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                  // console.log("SMS err,SMS response", err, response);
                });
              }
            }
          });
        }
      });
    } else {
      data.response = "Unable to submit your data";
      res.send(data);
    }
  };

  router.downloadpdf = (req, res) => {
    const data = {};
    data.status = 0;

    if (req.params && req.params.loginId) {
      const EID = req.params.loginId;
      GetOnlineForm(EID, result => {
        if (result.status === 1) {
          pdf.create(result.response, pdfoptions).toStream((err, stream) => stream.pipe(res));
        } else {
          data.response = result.response;
          res.send(data);
        }
      });
    } else {
      data.response = "Employee ID Not Available";
      res.send(data);
    }
  };

  return router;
};
