module.exports = (app, io) => {
  const db = require("../../model/mongodb");
  const CONFIG = require("../../config/config");
  const jwt = require("jsonwebtoken");
  const bcrypt = require("bcrypt-nodejs");
  const library = require("../../model/library.js");
  const nodemailer = require("nodemailer");
  const mail = require("../../model/mail.js");
  const mailcontent = require("../../model/mailcontent");
  const twilio = require("../../model/twilio.js");
  const dashboard = require("../../model/dashboard.js");
  const push = require("../../model/pushNotification.js")(io);
  const moment = require("moment");
  const mongoose = require("mongoose");
  const attachment = require("../../model/attachments");
  const async = require("async");
  const json2csv = require("json2csv").Parser;
  const fs = require("fs");
  const Google = require("../../model/googleapis");
  const event = require("../../controller/events/events");
  const router = {};

  router.employeenotifications = (req, res) => {
    const data = {};
    data.status = 0;
    dashboard.notifications(
      {
        type: "employee",
        employee: new mongoose.Types.ObjectId(req.params.loginId)
      },
      function(err, response) {
        if (err || !response) {
          data.response = err;
          res.send(data);
        } else {
          /* data.status = 1;
                data.response = response;*/
          res.send(response);
        }
      }
    );
  };
  router.employeedashboard = (req, res) => {
    const populate = {
      selectshift: req.body.selectshift,
      selecttimesheet: req.body.selecttimesheet,
      selectinvoice: req.body.selectinvoice,
      start: req.body.startdate,
      end: req.body.enddate
    };
    const data = {};
    data.status = 0;
    dashboard.employeedashboardCount({ type: "employee", employee: new mongoose.Types.ObjectId(req.params.loginId), populate }, (err, response) => {
      if (err || !response) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = response;
        res.send(data);
      }
    });
  };
  router.login = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("password", "Password Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    const email = req.body.email;
    const password = req.body.password;

    db.GetOneDocument("employees", { $or: [{ email: email, status: 1 }, { username: email, status: 1 }] }, {}, {}, (err, user) => {
      if (err || !user) {
        data.response = "Invalid Credentials";
        res.send(data);
      } else {
        if (library.validPassword(password, user.password)) {
          db.GetOneDocument("employees", { $or: [{ email: email, status: 1, isverified: 1 }, { username: email, status: 1, isverified: 1 }] }, {}, {}, (err, user) => {
            if (err || !user) {
              data.response = "Your account is not verified.";
              res.send(data);
            } else {
              const updata = { activity: {} };
              updata.activity.last_login = Date();
              db.UpdateDocument("employees", { _id: user._id }, updata, {}, function(err, docdata) {
                if (err) {
                  data.response = "Invalid Credentials";
                  res.send(data);
                } else {
                  // const auth_token = library.jwtSign({
                  //   username: user.username,
                  //   role: "employee"
                  // });
                  // data.status = 1;
                  // data.response = { auth_token: auth_token };
                  // res.send(data);
                  const options = {};
                  options.sort = { createdAt: -1 };
                  db.GetDocument("subscriptions", { agency: user.agency, status: 1, end: { $gte: new Date() } }, {}, options, function(err, subscriptionData) {
                    if (subscriptionData.length >= 1) {
                      data.status = 1;
                      const auth_token = library.jwtSign({
                        username: user.username,
                        role: "employee",
                        subscription: 1,
                        employee_count: subscriptionData[0].plan.employees,
                        recruitment_module: subscriptionData[0].plan.recruitment_module
                      });
                      data.response = { auth_token: auth_token };
                      res.send(data);
                    } else {
                      data.status = 1;
                      const auth_token = library.jwtSign({ username: user.username, role: "employee", subscription: 0, employee_count: 0, recruitment_module: 0 });
                      data.response = { auth_token: auth_token };
                      res.send(data);
                    }
                  });
                }
              });
            }
          });
        } else {
          data.response = "Invalid Credentials";
          res.send(data);
        }
      }
    });
  };
  router.logout = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("username", "Username Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.UpdateDocument("employees", { username: req.body.username }, { "activity.last_logout": new Date() }, {}, function(err, response) {
      if (err) {
        data.response = err;
        res.send(data);
      } else {
        data.status = 1;
        data.response = "logged out";
        res.send(data);
      }
    });
  };
  router.job_requests = (req, res) => {
    const data = {};
    data.status = 0;
    const options = {};

    /* //options.populate = 'employee';
        options.populate = 'shift';
        db.GetDocument('notifications', { "shift" : { $exists: true } , 'type' : 'employee' , 'employee' : req.params.loginId , 'action' : 'job_request' }, {}, options, function(err, notificationData){
            if(err || !notificationData){
                data.response = "No new Job requests.";
                res.send(data);
            }else{
                db.UpdateDocument('notifications', { employee : req.params.loginId }, { status : 2 } , { multi : true }, function(err, result) {
                    data.status = 1;
                    data.response = { "job_requests" : notificationData };
                    res.send(data);
                });
            }
        });*/

    db.GetDocument("shifts", { "employee_requested.employee": req.params.loginId, "status": { $lte: 7 } }, {}, options, function(err, notificationData) {
      if (err || !notificationData) {
        data.response = "No new Job requests.";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { job_requests: notificationData };
        res.send(data);
      }
    });
  };
  /* router.accept_job = (req, res) => {
    let data = {};
    data.status = 0;

    req.checkBody("shiftId", "JOb Id Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    let employeeData = req.params.loginData;

    db.GetOneDocument("shifts", {_id: req.body.shiftId}, {}, {}, function(err, shiftData) {
      if (err || !shiftData) {
        data.response = "Job details not available";
        return res.send(data);
      } else {
        db.UpdateDocument("shifts", {"_id": req.body.shiftId, "employee_requested.employee": req.params.loginId}, {$set: {"employee_requested.$.status": 1}, status: 3}, {}, function(err, docdata) {
          if (err) {
            data.err = err;
            data.response = "Job not available";
            res.send(data);
          } else {
            let message = "Your job has been accepted";
            let options = {shift: req.body.shiftId};
            var addedby = "AGENCY";

            if (shiftData.addedBy == "agency") {
              var addedby = "AGENCY";
            } else if (shiftData.addedBy == "client") {
              var addedby = "CLIENT";
            }

            push.addnotification(shiftData.addedId, message, "job_accepted", options, addedby, function(err, response, body) {});
            data.status = 1;
            data.response = {result: "Job accepted successfully."};
            res.send(data);
          }
        }
        );
      }
    });
  }; */
  router.assign_shift = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("shiftId", "Shift Id Required").notEmpty();
    req.checkBody("employeeId", "Employee Id Required").optional();
    // req.checkBody("status", "Type Required").notEmpty();
    // req.checkBody("employeeData", "Type Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, (err, shiftData) => {
      if (err || !shiftData) {
        data.response = "Job details not available";
        return res.send(data);
      } else {
        // let start = moment(shiftData.start_date).format("YYYY-MM-DD") + " 00:00:00";
        // let end = moment(shiftData.start_date).format("YYYY-MM-DD") + " 23:59:59";
        const userQuery = [];
        userQuery.push({ $match: { employee: { $eq: req.params.loginId } } });
        // userQuery.push({$match: {$and: [{start_date: {$gte: start}}, {start_date: {$lte: end}}]}});
        userQuery.push({
          $match: {
            $or: [
              {
                $and: [{ starttime: { $gte: shiftData.starttime } }, { endtime: { $lte: shiftData.endtime } }]
              },
              {
                $and: [{ starttime: { $gte: shiftData.starttime } }, { endtime: { $lte: shiftData.starttime } }]
              },
              {
                $and: [{ starttime: { $gte: shiftData.endtime } }, { endtime: { $lte: shiftData.endtime } }]
              }
            ]
          }
        });
        userQuery.push({
          $match: { $or: [{ status: { $eq: 4 } }, { status: { $eq: 5 } }] }
        });
        db.GetAggregation("shifts", userQuery, (err, employeeData) => {
          if (err || !employeeData) {
            data.status = 0;
            data.err = err;
            data.response = "Unable to get employee data, Please try again";
            res.send(data);
          } else {
            const EmpDatas = employeeData && employeeData.filter(list => moment(list.start_date).format("DD-MM-YYYY") === moment(shiftData.start_date).format("DD-MM-YYYY"));
            if (EmpDatas && EmpDatas.length > 0) {
              data.status = 0;
              data.err = err;
              data.response = "Employee Already Assigned On Same Time";
              res.send(data);
            } else {
              let employee_rate = 0;
              let agency_rate = 0;
              let client_rate = 0;

              shiftData.employee_requested.map(a => {
                if (a.employee.toString() === req.params.loginId.toString()) {
                  a.status = 2;
                  employee_rate = a.employee_rate;
                  client_rate = a.client_rate;
                  agency_rate = a.agency_rate;
                }
              });

              let shiftemployees = shiftData.employee_requested;

              if (req.body.status && req.body.status === 1) {
                const employee_requested = [];
                const employeeData = req.body.employeeData;
                employeeData.status = 2;
                employee_rate = employeeData.employee_rate;
                client_rate = employeeData.client_rate;
                agency_rate = employeeData.agency_rate;
                employee_requested.push(employeeData);
                shiftemployees = employee_requested;
              }
              const shift_update = {
                employee_requested: shiftemployees,
                employee: req.params.loginId,
                status: 4,
                employee_rate: employee_rate,
                agency_rate: agency_rate,
                client_rate: client_rate
              };
              db.UpdateDocument("shifts", { _id: req.body.shiftId }, shift_update, {}, (err, docdata) => {
                if (err) {
                  data.err = err;
                  data.response = "Job not available";
                  res.send(data);
                } else {
                  // let message = "Your have assigned for a shift.";
                  // let options = {shiftid: req.body.shiftId};
                  // push.addnotification(req.body.employeeId, message, "job_assigned", options, "EMPLOYEE", (err, response, body) => {});
                  // data.status = 1;
                  // data.response = {result: "Shift assigned successfully."};
                  // res.send(data);
                  const message = "Your job has been accepted";
                  const options = { shift: req.body.shiftId };
                  // let addedby = "AGENCY";
                  // if (shiftData.addedBy === "agency") {
                  //   addedby = "AGENCY";
                  // } else if (shiftData.addedBy === "client") {
                  //   addedby = "CLIENT";
                  // }
                  push.addnotification(shiftData.agency, message, "job_accepted", options, "AGENCY", (err, response, body) => {});
                  push.addnotification(shiftData.client, message, "job_accepted", options, "CLIENT", (err, response, body) => {});
                  db.GetOneDocument("agencies", { _id: shiftData.agency }, {}, {}, (err, AgencyData) => {
                    if (AgencyData) {
                      if (AgencyData.settings.notifications.email) {
                        // Mail Sent To All
                        const mailData = {};
                        mailData.template = "shiftcancelnotificationtoagencyclient";
                        mailData.to = AgencyData.email;
                        mailData.html = [];
                        mailData.html.push({
                          name: "username",
                          value: AgencyData.username
                        });
                        mailData.html.push({
                          name: "role",
                          value: AgencyData.job_type
                        });
                        mailcontent.sendmail(mailData, (err, response) => {
                          console.log("mail err,mail response", err, response);
                        });
                      }
                      if (AgencyData.settings.notifications.sms) {
                        // SMS Send To All
                        const smsto = AgencyData.phone.code + AgencyData.phone.number;
                        const smsmessage = `Hi ${AgencyData.username} , You Got New Shift For Role ${AgencyData.job_type}`;
                        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                          console.log("SMS err,SMS response", err, response);
                        });
                      }
                    }
                  });
                  db.GetOneDocument("clients", { _id: shiftData.client }, {}, {}, (err, ClientData) => {
                    if (ClientData) {
                      if (ClientData.settings && ClientData.settings.notifications.email) {
                        // Mail Sent To All
                        const mailData = {};
                        mailData.template = "shiftcancelnotificationtoagencyclient";
                        mailData.to = ClientData.email;
                        mailData.html = [];
                        mailData.html.push({
                          name: "username",
                          value: ClientData.username
                        });
                        mailData.html.push({
                          name: "role",
                          value: ClientData.job_type
                        });
                        mailcontent.sendmail(mailData, (err, response) => {
                          console.log("mail err,mail response", err, response);
                        });
                      }
                      if (ClientData.settings && ClientData.settings.notifications.sms) {
                        // SMS Send To All
                        const smsto = ClientData.phone.code + ClientData.phone.number;
                        const smsmessage = `Hi ${ClientData.username} , You Got New Shift For Role ${ClientData.job_type}`;
                        twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                          console.log("SMS err,SMS response", err, response);
                        });
                      }
                    }
                  });
                  data.status = 1;
                  data.response = { result: "Job accepted successfully." };
                  res.send(data);
                  event.emit("shiftstat", { id: req.body.shiftId, status: 4 });
                }
              });
            }
          }
        });
      }
    });
  };
  router.profile = function(req, res) {
    const data = {};
    data.status = 0;

    db.GetOneDocument("employees", { _id: req.params.loginId }, {}, {}, function(err, clientData) {
      if (err) {
        res.send(err);
      } else {
        data.status = 1;
        data.response = { result: clientData };
        res.send(data);
      }
    });
  };
  router.profileDetails = function(req, res) {
    const data = {};
    data.status = 0;

    const clientQuery = [];
    clientQuery.push({ $match: { _id: { $eq: req.params.loginId } } });

    clientQuery.push({
      $lookup: {
        from: "jobtypes",
        let: { job_type: "$job_type" },
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ["$_id", "$$job_type"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "jobtypes"
      }
    });

    clientQuery.push({
      $lookup: {
        from: "locations",
        let: { locations: "$locations" },
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ["$_id", "$$locations"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "locations"
      }
    });

    // clientQuery.push({ $project: { name: 1, jobtypes: 1, locations: 1 } });

    db.GetAggregation("employees", clientQuery, (err, clientData) => {
      // console.log(err, clientData);
      if (err || !clientData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: clientData };
        res.send(data);
      }
    });
  };
  router.saveprofile = function(req, res) {
    const data = {};
    data.status = 0;
    // req.checkBody('client', "Client Required").notEmpty();
    req.checkBody("email", "Email Required").notEmpty();
    req.checkBody("name", "Name Required").notEmpty();
    if (req.body.password !== undefined || req.body.confirm_password !== undefined) {
      if (!req.body._id || req.body.password != "") {
        req.checkBody("password", "Invalid Password").notEmpty();
        req.checkBody("confirm_password", "Passwords do not match.").equals(req.body.password);
      }
    }
    req.checkBody("phone.code", "Phone Number Required").notEmpty();
    req.checkBody("phone.number", "Phone Number Required").notEmpty();
    req.checkBody("address.zipcode", "Address Required").notEmpty();
    req.checkBody("address.country", "Address Required").notEmpty();
    req.checkBody("address.formatted_address", "Address Required").notEmpty();
    req.checkBody("status", "Status Required").notEmpty();
    req.checkBody("job_type", "Jobtype Required").notEmpty();
    // req.checkBody('locations','Locations Required').notEmpty();
    // req.checkBody('available','Available Required').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const employee = {
      username: req.body.username,
      email: req.body.email,
      name: req.body.name,
      phone: req.body.phone,
      status: req.body.status,
      address: req.body.address,
      job_type: req.body.job_type,
      available: req.body.available,
      // timeoff:req.body.timeoff,
      joining_date: req.body.joining_date,
      final_date: req.body.final_date,
      // holiday_allowance: req.body.holiday_allowance,
      employee_rate: req.body.employee_rate,
      locations: req.body.locations
      // client_rate:req.body.client_rate,
    };

    if (req.body.password && req.body.confirm_password) {
      employee.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }

    if (req.files && typeof req.files.avatar !== "undefined") {
      if (req.files.avatar.length > 0) {
        employee.avatar = attachment.get_attachment(req.files.avatar[0].destination, req.files.avatar[0].filename);
      }
    } else {
      // employee.avatar = req.body.avatar;
    }

    if (req.body._id) {
      db.UpdateDocument("employees", { _id: req.body._id }, employee, { upsert: true }, function(err, result) {
        if (err) {
          if (err.code === 11000) {
            data.response = "Username/Email is already Exists ";
          } else {
            data.response = "Unable to Save Your Data Please try again";
          }
          data.status = 0;
          res.send(data);
        } else {
          data.status = 1;
          data.response = "Employee updated successfully.";
          res.send(data);
        }
      });
    } else {
      data.response = "Unable to Save Your Data Please try again";
      res.send(data);
    }
  };
  router.change_shift = function(req, res) {
    const data = {};
    data.status = 0;

    req.checkBody("shiftId", "shiftId Required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const updata = {};

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, function(err, shiftData) {
      if (err || !shiftData) {
        data.response = "Job details not available.";
        res.send(data);
      } else {
        // console.log("req.params.action", req.params.action);
        let timesheetobj = {};
        let status = shiftData.status;
        const timesheetarr = JSON.parse(JSON.stringify(shiftData.timesheet));

        if (timesheetarr.length > 0) {
          timesheetobj = timesheetarr[0];
        }

        if (req.params.action == "start") {
          status = 5;
          timesheetobj.date = new Date();
          timesheetobj.start = new Date();
          event.emit("shiftstat", { id: req.body.shiftId, status: 5 });
        } else if (req.params.action == "hold") {
          status = shiftData.status;
          timesheetobj.hold = new Date();
        } else if (req.params.action == "restart") {
          if (timesheetobj.hold != "") {
            status = shiftData.status;
            var a = moment(new Date());
            var b = moment(timesheetobj.hold);
            var c = a.diff(b, "seconds");
            const hold = {
              hold: timesheetobj.hold,
              restart: new Date(),
              holdedmins: parseInt(c)
            };
            timesheetobj.hold = "";
            if (timesheetobj.holdedmins) {
              timesheetobj.holdedmins = parseInt(hold.holdedmins) + parseInt(timesheetobj.holdedmins);
            } else {
              timesheetobj.holdedmins = parseInt(hold.holdedmins);
            }
            timesheetobj.breaks.push(hold);
          }
        } else if (req.params.action == "end") {
          status = 6;
          timesheetobj.end = new Date();
          var a = moment(timesheetobj.end);
          var b = moment(timesheetobj.start);
          var c = a.diff(b, "seconds");
          timesheetobj.jobmins = c;
          timesheetobj.workmins = timesheetobj.jobmins - (timesheetobj.holdedmins || 0);

          const workedhours = Math.ceil(timesheetobj.workmins / 60);
          // console.log("workedhours", workedhours);

          // updata.employee_fare = workedhours * shiftData.employee_rate;
          // updata.agency_fare = workedhours * shiftData.agency_rate;
          // updata.client_fare = workedhours * shiftData.client_rate;
          event.emit("shiftstat", { id: req.body.shiftId, status: 6 });
        } else {
          data.response = "Shift not available";
          return res.send(data);
        }

        // console.log("timesheetobj",timesheetobj);
        const newtimesheet = [];
        newtimesheet[0] = timesheetobj;

        updata.status = status;
        updata.timesheet = newtimesheet;

        db.UpdateDocument("shifts", { _id: req.body.shiftId }, updata, {}, function(err, result) {
          if (err) {
            data.response = "Unable to Save Your Data Please try again";
            data.err = err;
            res.send(data);
          } else {
            db.GetDocument("shifts", { _id: req.body.shiftId }, {}, {}, function(err, shiftData) {
              if (err || !shiftData) {
                data.response = "Shift details not available.";
                res.send(data);
              } else {
                data.status = 1;
                data.response = "";
                if (req.params.action == "start") {
                  data.response = "Job started!";
                  const message = data.response;
                  const options = {
                    shift: shiftData[0]._id,
                    emp_id: shiftData[0].employee
                  };
                  push.addnotification(shiftData[0].agency, message, "job_started", options, "AGENCY", (err, response, body) => {});
                  push.addnotification(shiftData[0].client, message, "job_started", options, "CLIENT", (err, response, body) => {});
                } else if (req.params.action == "hold") {
                  data.response = "Job holded";
                } else if (req.params.action == "restart") {
                  data.response = "Job restarted.";
                } else if (req.params.action == "end") {
                  data.response = "Job completed!.";
                  const message = data.response;
                  const options = {
                    shift: shiftData[0]._id,
                    emp_id: shiftData[0].employee
                  };
                  push.addnotification(shiftData[0].agency, message, "job_completed", options, "AGENCY", (err, response, body) => {});
                  push.addnotification(shiftData[0].client, message, "job_completed", options, "CLIENT", (err, response, body) => {});
                  event.emit("timesheet_dates_list", { agency: shiftData[0].agency });
                  event.emit("invoice_dates_list", { agency: shiftData[0].agency });
                }
                data.shiftstatus = parseInt(status);
                data.newtimesheet = newtimesheet;
                res.send(data);
              }
            });
          }
        });
      }
    });
  };
  router.shiftlist = (req, res) => {
    let data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const listQuery = [];

    listQuery.push({
      $match: {
        $or: [
          // { "status" : 2 },
          {
            employee: { $eq: new mongoose.Types.ObjectId(req.params.loginId) }
          },
          {
            employee_requested: {
              $elemMatch: {
                employee: new mongoose.Types.ObjectId(req.params.loginId),
                status: 1
              }
            }
          },
          {
            "employee_requested.employee": {
              $eq: new mongoose.Types.ObjectId(req.params.loginId)
            },
            "status": 2
          }
          // {"employee_requested.employee": {$eq: new mongoose.Types.ObjectId(req.params.loginId)}, "status": 3},
        ]
      }
    });
    listQuery.push({ $match: { status: { $ne: 10 } } });
    if (req.body.status) {
      req.body.status = parseInt(req.body.status);
      listQuery.push({ $match: { status: { $eq: req.body.status } } });
    }

    if (req.body.from_date) {
      listQuery.push({
        $match: { createdAt: { $gt: new Date(req.body.from_date) } }
      });
    }

    if (req.body.to_date) {
      listQuery.push({
        $match: { createdAt: { $lt: new Date(req.body.to_date) } }
      });
    }

    const withoutlimit = Object.assign([], listQuery);
    withoutlimit.push({ $count: "count" });

    listQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { agencyid: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      {
        $addFields: { job_type: "$jobtype_data.name" }
      }
    );

    listQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { agencyid: req.params.loginId },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1, address: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: { employee: "$employee_data.name" }
        // $addFields: { "employee": { $arrayElemAt: ["$employee_data.name", 0] } }
      }
    );
    listQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { locations: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$locations"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "locations_data"
        }
      },
      {
        $addFields: { locations: "$locations_data.name" }
      }
    );

    listQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { clientid: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$clientid"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    listQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$location" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { location: "$location_data.name" } }
    );
    listQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch_data"
      }
    });
    if (req.body.from_date) {
      listQuery.push({
        $match: { createdAt: { $gt: new Date(req.body.from_date) } }
      });
    }

    if (req.body.to_date) {
      listQuery.push({
        $match: { createdAt: { $lt: new Date(req.body.to_date) } }
      });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      listQuery.push({ $sort: sorting });
    } else {
      listQuery.push({ $sort: { createdAt: -1 } });
    }
    if (req.body.search) {
      const searchs = req.body.search;
      if (req.body.filter === "all") {
        listQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } },
              { distance: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        listQuery.push({ $match: searching });
      }
    }
    if (req.body.skip >= 0) {
      listQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      listQuery.push({ $limit: parseInt(req.body.limit) });
    }

    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          groupcount: [
            {
              $match: {
                $or: [
                  {
                    employee: {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    }
                  },
                  {
                    "employee_requested.employee": {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    },
                    "status": { $eq: 2 }
                  },
                  {
                    "employee_requested.employee": {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    },
                    "status": { $eq: 3 }
                  }
                ]
              }
            },
            { $group: { _id: "$status", count: { $sum: 1 } } },
            {
              $addFields: {
                status: {
                  $arrayElemAt: [["added", "added", "requests", "accepted", "assigned", "ongoing", "completed", "timehseet_approved", "invoice_approved", "Payment_completed", "expired"], "$_id"]
                }
              }
            },
            { $addFields: { nott: [["$status", "$count"]] } },
            { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
            { $group: { _id: null, res: { $push: "$$ROOT" } } },
            { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
          ],
          result: listQuery
        }
      }
    ];

    db.GetAggregation("shifts", finalQuery, function(err, docData) {
      if (err || !docData) {
        data.err = err;
        data.response = "unable to get Your Data Please Try Again";
        res.send(data);
      } else {
        if (req.body.notification) {
          dashboard.notification_status({ employee: req.params.loginId, action: req.body.notification }, function(err, response) {});
        }
        async.mapSeries(
          docData && docData[0].result,
          (emp, callback) => {
            let distance = "Currently Not Available";
            const bran_data = emp.branch_data && emp.branch_data.length > 0 && emp.branch_data[0].branches;
            data = {
              lat1: emp.employee_data[0].address.lat,
              lon1: emp.employee_data[0].address.lon,
              lat2: bran_data.branchlat,
              lon2: bran_data.branchlng
            };
            Google.distancematrix(data, LResult => {
              if (LResult && LResult.rows.length > 0 && LResult.rows[0].elements.length > 0) {
                distance = LResult.rows[0].elements[0].distance && LResult.rows[0].elements[0].distance.text;
              }
              const empobj = {
                _id: emp._id,
                title: emp.title,
                shift_id: emp.shift_id,
                shiftId: emp.shiftId,
                endtime: emp.endtime,
                starttime: emp.starttime,
                client: emp.client,
                breaktime: emp.breaktime,
                end_date: emp.end_date,
                start_date: emp.start_date,
                locations: emp.locations,
                job_type: emp.job_type,
                employee_data: emp.employee_data,
                branch: emp.branch,
                branch_data: emp.branch_data,
                jobtype_data: emp.jobtype_data,
                distance: distance,
                createdAt: emp.createdAt,
                status: emp.status
              };
              if (err) {
                callback(err);
              } else {
                callback(null, empobj);
              }
            });
          },
          (err, results) => {
            if (err) {
              res.send(err);
            } else {
              data.status = 1;
              let fullcount;
              if (docData[0].overall[0] && docData[0].overall[0].count) {
                fullcount = docData[0].overall[0].count;
              } else {
                fullcount = docData[0].result.length;
              }
              // data.response = docData;
              data.response = {};
              if (req.body.field === "distance") {
                if (req.body.order === 1) {
                  data.response.result = results.sort((a, b) => a.distance - b.distance);
                }
                if (req.body.order === -1) {
                  data.response.result = results.sort((a, b) => b.distance - a.distance);
                }
              } else if (req.body.order && req.body.field) {
                data.response.result = results;
              } else {
                data.response.result = results.sort((a, b) => b.createdAt - a.createdAt);
              }
              (data.response.length = docData[0].result.length), (data.response.fullcount = fullcount), (data.response.groupcount = docData[0].groupcount[0]);
              return res.send(data);
            }
          }
        );
      }
    });
  };
  router.view_shift = (req, res) => {
    let data = {};
    data.status = 0;

    req.checkBody("shiftId", "shiftId Required").notEmpty();

    const error = req.validationErrors();
    if (error) {
      data.response = error[0].msg;
      return res.send(data);
    }

    const shiftQuery = [];
    shiftQuery.push({
      $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.shiftId) } }
    });

    // console.log('req.params.loginId', req.params.loginId);
    shiftQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { employeeid: req.params.loginId },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$employeeid"]
                }
              }
            },
            { $project: { name: 1, address: 1, avatar: 1 } }
          ],
          as: "employee_data"
        }
      },
      {
        $addFields: {
          employee_avatar: "$employee_data.avatar",
          employee: "$employee_data.name",
          employeeaddress: "$employee_data.address"
        }
      }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );
    shiftQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    shiftQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );

    shiftQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { location_type: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$location_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      { $addFields: { locations: "$location_data.name" } }
    );

    db.GetAggregation("shifts", shiftQuery, function(err, shiftData) {
      if (err || !shiftData) {
        data.err = err;
        data.shiftData = shiftData;
        data.response = "Job details not available";
        return res.send(data);
      } else {
        async.mapSeries(
          shiftData,
          (item, callback) => {
            let distance = "Currently Not Available";
            const bran_data = item.branch && item.branch.length > 0 && item.branch[0].branches;
            data = {
              lat1: item.employeeaddress[0].lat,
              lon1: item.employeeaddress[0].lon,
              lat2: bran_data.branchlat,
              lon2: bran_data.branchlng
            };
            Google.distancematrix(data, LResult => {
              if (LResult && LResult.rows.length > 0 && LResult.rows[0].elements.length > 0) {
                distance = LResult.rows[0].elements[0].distance && LResult.rows[0].elements[0].distance.text;
              }
              const empobj = {
                _id: item._id,
                distance: distance,
                option_data: item.option_data,
                latefee: item.latefee,
                timesheet_status: item.timesheet_status,
                agency_rating: item.agency_rating,
                locations: item.locations,
                job_type: item.job_type,
                branch: item.branch,
                title: item.title,
                shiftId: item.shiftId,
                notes: item.notes,
                status: item.status,
                starttime: item.starttime,
                endtime: item.endtime,
                breaktime: item.breaktime,
                start_date: item.start_date,
                end_date: item.end_date,
                shift_type: item.shift_type,
                shift_option: item.shift_option,
                client: item.client,
                agency: item.agency,
                employee_requested: item.employee_requested,
                timesheet: item.timesheet,
                location_data: item.location_data,
                jobtype_data: item.jobtype_data,
                client_data: item.client_data,
                employee_avatar: item.employee_avatar,
                employeeaddress: item.employeeaddress,
                employee_data: item.employee_data,
                employee: item.employee,
                employee_rate: item.employee_rate
              };
              if (err) {
                callback(err);
              } else {
                callback(null, empobj);
              }
            });
          },
          (err, results) => {
            if (err) {
              res.send(err);
            } else {
              data.status = 1;
              data.response = results && results[0];
              return res.send(data);
            }
          }
        );
      }
    });
  };
  router.agencies = (req, res) => {
    const data = {};
    data.status = 0;

    const agencyQuery = [];
    agencyQuery.push({ $match: { status: { $eq: 1 } } });
    agencyQuery.push({ $match: { _id: { $eq: req.params.loginData.agency } } });

    agencyQuery.push({
      $lookup: {
        from: "jobtypes",
        let: { agencyid: "$_id" },
        pipeline: [{ $match: { $expr: { $eq: ["$addedId", "$$agencyid"] } } }, { $match: { "job_id": { $exists: false } } }, { $project: { name: 1 } }],
        as: "jobtypes"
      }
    });

    agencyQuery.push({
      $lookup: {
        from: "locations",
        let: { agencyid: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$addedId", "$$agencyid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "locations"
      }
    });

    agencyQuery.push({ $project: { name: 1, jobtypes: 1, locations: 1 } });

    db.GetAggregation("agencies", agencyQuery, (err, docData) => {
      if (err || !docData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };
  router.earnings = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const listQuery = [];
    listQuery.push({
      $match: {
        employee: { $eq: new mongoose.Types.ObjectId(req.params.loginId) }
      }
    });
    listQuery.push({ $match: { status: { $eq: 2 } } });

    if (req.body.status) {
      listQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      listQuery.push({ $match: { status: 2 } });
    }
    listQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );
    listQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    // listQuery.push({
    //     $lookup: {
    //         from: 'invoice',
    //         let: { idd: "$_id" },
    //         pipeline: [{
    //             $match: { $expr: { $eq: [ '$shift_mid', '$$idd' ] } }
    //         },{ $project: {_id:0, "employee_rate": 1}}],
    //         as: 'invoice'
    //     }
    // }, {$addFields: {employee_rate: "$invoice.employee_rate"}});
    listQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { locid: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$locid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      {
        $addFields: { locations: { $arrayElemAt: ["$location_data.name", 0] } }
      }
    );

    listQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { agencyid: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobname"
        }
      },
      {
        $addFields: { job_type: { $arrayElemAt: ["$jobname.name", 0] } }
      },
      { $project: { jobname: 0 } }
    );

    listQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { agencyid: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "empname"
        }
      },
      {
        $addFields: { employee: { $arrayElemAt: ["$empname.name", 0] } }
      },
      { $project: { empname: 0 } }
    );

    // var withoutlimit = JSON.parse(JSON.stringify(listQuery));
    // withoutlimit.push({ $count:'count'});

    const withoutlimit = Object.assign([], listQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      listQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      listQuery.push({ $limit: parseInt(req.body.limit) });
    }

    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        listQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        listQuery.push({ $match: searching });
      }
    }
    if (req.body.from_date) {
      listQuery.push({
        $match: { createdAt: { $gt: new Date(req.body.from_date) } }
      });
    }

    if (req.body.to_date) {
      listQuery.push({
        $match: { createdAt: { $lt: new Date(req.body.to_date) } }
      });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      listQuery.push({ $sort: sorting });
    } else {
      listQuery.push({ $sort: { createdAt: -1 } });
    }
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            {
              $match: {
                $or: [
                  {
                    employee: {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    }
                  },
                  {
                    "employee_requested.employee": {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    }
                  }
                ]
              }
            },
            {
              $match: {
                $or: [{ status: 1 }, { status: 2 }]
              }
            },
            {
              $group: {
                _id: "$status",
                totalamount: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: listQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, function(err, docData) {
      if (err || !docData) {
        data.err = err;
        data.response = "unable to get Your Data Please Try Again";
        res.send(data);
      } else {
        const obj = {
          employee: req.params.loginId,
          action: req.body.notification
        };
        // console.log("==obj", obj);
        if (req.body.notification) {
          dashboard.notification_status(obj, function(err, response) {});
        }
        if (req.body.notification) {
          dashboard.notification_status(
            {
              type: "employee",
              employee: req.params.loginId,
              action: req.body.notification
            },
            function(err, response) {}
          );
        }

        data.status = 1;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          var fullcount = docData[0].overall[0].count;
        } else {
          var fullcount = docData[0].result.length;
        }

        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          listQuery: listQuery
        };
        res.send(data);
      }
    });
  };
  router.earningsview = (req, res) => {
    const data = {};
    data.status = 0;
    req.checkBody("id", "Shift Id required").notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    const invoiceQuery = [];
    invoiceQuery.push({
      $match: { _id: { $eq: new mongoose.Types.ObjectId(req.body.id) } }
    });

    invoiceQuery.push({
      $lookup: {
        from: "employees",
        let: { employeeid: "$employee" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$employeeid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "employee_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { clientid: "$client" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$clientid"]
              }
            }
          },
          { $project: { companyname: 1 } }
        ],
        as: "client_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "locations",
        let: { locationsid: "$locations" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$locationsid"]
              }
            }
          },
          { $project: { name: 1 } }
        ],
        as: "locations_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "shifts",
        let: { idd: "$shift_mid" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$idd"]
              }
            }
          }
        ],
        as: "shift_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "agencies",
        let: { agencyid: "$agency" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$agencyid"]
              }
            }
          },
          { $project: { company_name: 1 } }
        ],
        as: "agency_details"
      }
    });
    invoiceQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch_data"
      }
    });
    invoiceQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { job_type: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$job_type"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobtype_data"
        }
      },
      { $addFields: { job_type: "$jobtype_data.name" } }
    );
    db.GetAggregation("invoice", invoiceQuery, function(err, docData) {
      if (err || !docData) {
        data.response = "Unable to Get Your Data Please Try Again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = { result: docData };
        res.send(data);
      }
    });
  };
  router.forget_password = (req, res) => {
    const data = {};
    const request = {};
    req.checkBody("email", "Invalid email").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    request.email = req.body.email;
    request.reset = library.randomString(6, "#a");

    async.waterfall(
      [
        function(callback) {
          db.GetOneDocument("employees", { email: request.email }, {}, {}, function(err, employee) {
            callback(err, employee);
          });
        },
        function(employee, callback) {
          if (employee) {
            db.UpdateDocument("employees", { _id: employee._id }, { reset_code: request.reset }, {}, function(err, response) {
              callback(err, employee);
            });
          } else {
            callback(null, employee);
          }
        },
        function(employee, callback) {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, function(err, settings) {
            if (err) {
              callback(err, callback);
            } else {
              callback(err, employee, settings);
            }
          });
        }
      ],
      function(err, employee, settings) {
        if (err || !employee) {
          data.status = 0;
          data.response = "User Not Found";
          res.send(data);
        } else {
          let name;
          if (employee.name) {
            name = employee.name + " (" + employee.username + ")";
          } else {
            name = employee.username;
          }
          const mailData = {};
          mailData.template = "forgotpassword";
          mailData.to = employee.email;
          mailData.html = [];
          mailData.html.push({ name: "name", value: name });
          mailData.html.push({
            name: "site_url",
            value: settings.settings.site_url
          });
          mailData.html.push({ name: "logo", value: settings.settings.logo });
          mailData.html.push({ name: "email", value: employee.email });
          mailData.html.push({
            name: "url",
            value: settings.settings.site_url + "resetpassword" + "/" + employee._id + "/" + request.reset
          });
          // console.log("mailData", mailData);
          mailcontent.sendmail(mailData, function(err, response) {
            console.log("err, response", err, response);
          });
          data.status = 1;
          data.url = settings.settings.site_url + "resetpassword" + "/" + employee._id + "/" + request.reset;
          data.response = "Please Check Your Mail!";
          res.send(data);
        }
      }
    );
  };
  router.resetpassword = (req, res) => {
    const id = req.params.userid;
    const resetid = req.params.reset;
    const data = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

    db.UpdateDocument("employees", { _id: id, reset_code: resetid }, { password: data }, {}, function(err, docdata) {
      console.log("err, docdata ", err, docdata);
      if (err || docdata.nModified == 0) {
        var data = {};
        data.status = 0;
        data.response = "Reset Password error!";
        res.send(data);
      } else {
        data = {};
        data.status = 1;
        data.response = "Reset Successfully!";
        res.send(data);
      }
    });
  };
  router.timesheet_request = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("shiftId", "Job Id Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      res.send(data);
      return;
    }

    const employeeData = req.params.loginData;

    db.GetOneDocument("shifts", { _id: req.body.shiftId }, {}, {}, function(err, shiftData) {
      if (err || !shiftData) {
        data.response = "Job details not available";
        return res.send(data);
      } else {
        db.UpdateDocument("shifts", { _id: req.body.shiftId }, { timesheet_status: 1 }, {}, function(err, docdata) {
          if (err) {
            data.err = err;
            data.response = "Job not available";
            res.send(data);
          } else {
            const message = "Timesheet request from employee";
            const options = { shift: req.body.shiftId };
            // if (shiftData.addedBy == "agency") {
            //   var addedby = "AGENCY";
            // } else if (shiftData.addedBy == "client") {
            //   var addedby = "CLIENT";
            // }
            push.addnotification(shiftData.agency, message, "timesheet_request", options, "AGENCY", function(err, response, body) {});
            push.addnotification(shiftData.client, message, "timesheet_request", options, "CLIENT", function(err, response, body) {});
            data.status = 1;
            data.response = { result: "Your request sent successfully." };
            res.send(data);
          }
        });
      }
    });
  };
  router.update_availabilty = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("UnavailableData", "Date Required").notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const unavailability = [];
    req.body.UnavailableData.map(list => {
      const dayTemp = new Date(list.date);
      const dayCheck = dayTemp.getDay();
      let day = "";
      if (dayCheck == 0) {
        day = "Sunday";
      } else if (dayCheck == 1) {
        day = "Monday";
      } else if (dayCheck == 2) {
        day = "Tuesday";
      } else if (dayCheck == 3) {
        day = "Wednesday";
      } else if (dayCheck == 4) {
        day = "Thursday";
      } else if (dayCheck == 5) {
        day = "Friday";
      } else if (dayCheck == 6) {
        day = "Saturday";
      }
      unavailability.push({
        day: day,
        from: list.from,
        to: list.to,
        date: list.date
      });
    });
    const ResultData = [];
    unavailability &&
      unavailability.length > 0 &&
      unavailability.map((list, key) => {
        db.UpdateDocument("employees", { _id: req.params.loginId }, { $push: { timeoff: list } }, {}, (err, result) => {
          if (err) {
            data.response = "Unable to Save Your Data Please try again";
            data.err = err;
            data.request = req.body;
            res.send(data);
          } else {
            ResultData.push(result);
            if (unavailability.length === ResultData.length) {
              data.status = 1;
              data.response = "employee updated successfully.";
              res.send(data);
            }
          }
        });
      });
  };
  router.delete_availabilty = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("index", "index Required").notEmpty();

    db.GetOneDocument("employees", { _id: req.params.loginId }, {}, {}, function(err, employeeData) {
      if (err) {
        res.send(err);
      } else {
        const timeoff = Object.assign([], employeeData.timeoff);
        delete timeoff[req.body.index];
        const newtimeoff = timeoff.filter(x => x != null);

        db.UpdateDocument("employees", { _id: req.params.loginId }, { timeoff: newtimeoff }, {}, (err, result) => {
          // console.log("err, result", err, result);
          if (err) {
            data.response = "Unable to Save Your Data Please try again";
            data.err = err;
            data.request = req.body;
            res.send(data);
          } else {
            data.status = 1;
            data.response = "employee updated successfully.";
            res.send(data);
          }
        });
      }
    });
  };
  /* router.unassignShift = (req, res) => {
    // console.log(req.params.loginId,req.body.id);
    let data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();

    db.GetOneDocument("shifts", {_id: req.body.id}, {}, {}, function(err, shiftData) {
      if (err) {
        res.send(err);
      } else {
        // console.log(shiftData);
        if (shiftData.employee_requested.length === 1) {
          db.UpdateDocument(
            "shifts",
            {_id: req.body.id, employee_requested: {$elemMatch: {employee: req.params.loginId}}},
            {status: 1, employee_requested: [], $unset: {employee: ""}},
            {},
            (err, result) => {
              // console.log("1","err, result",err, result);
              if (err) {
                data.response = "Unable to Save Your Data Please try again";
                data.err = err;
                data.request = req.body;
                res.send(data);
              } else {
                data.status = 1;
                data.response = "employee Unassigned successfully.";
                let message = "shift_unassigned";
                let options = {shift: shiftData._id, emp_id: shiftData.employee};
                push.addnotification(shiftData.agency, message, "shift_unassigned", options, "AGENCY", (err, response, body) => {});
                push.addnotification(shiftData.client, message, "shift_unassigned", options, "CLIENT", (err, response, body) => {});
                res.send(data);
              }
            }
          );
        }
        if (shiftData.employee_requested.length > 1) {
          db.UpdateDocument(
            "shifts",
            {_id: req.body.id, employee_requested: {$elemMatch: {employee: req.params.loginId}}},
            {"status": 2, "employee_requested.$.status": 0, "$unset": {employee: ""}},
            {},
            (err, result) => {
              // console.log("err, result",err, result);
              if (err) {
                data.response = "Unable to Save Your Data Please try again";
                data.err = err;
                data.request = req.body;
                res.send(data);
              } else {
                data.status = 1;
                data.response = "employee Unassigned successfully.";
                let message = "shift_unassigned";
                let options = {shift: shiftData._id, emp_id: shiftData.employee};
                push.addnotification(shiftData.agency, message, "shift_unassigned", options, "AGENCY", (err, response, body) => {});
                push.addnotification(shiftData.client, message, "shift_unassigned", options, "CLIENT", (err, response, body) => {});
                res.send(data);
              }
            }
          );
        }
      }
    });
  };*/
  router.unassignShift = (req, res) => {
    // console.log(req.params.loginId,req.body.id);
    const data = {};
    data.status = 0;
    req.checkBody("id", "ID Required").notEmpty();

    db.GetOneDocument("shifts", { _id: req.body.id }, {}, {}, (err, shiftData) => {
      if (err) {
        res.send(err);
      } else {
        db.GetOneDocument("agencies", { _id: shiftData.agency }, {}, {}, (err, agencydata) => {
          if (err) {
            res.send(err);
          } else {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = shiftData.starttime / 3600;
            const splithr = evestarthr.toString().split(".");
            const startsec = splithr[1];
            const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
            const JobTimeinhrs = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            const shiftDate_Time = moment(shiftData.start_date).format(`YYYY-MM-DD ${JobTimeinhrs}:00`);
            const Cancel_Time = moment(shiftDate_Time).subtract(agencydata.settings.general.cancel_shift, "seconds");
            const CurrentTime = moment(new Date());
            if (CurrentTime > Cancel_Time) {
              data.status = 0;
              data.response = "Not Allowed To Cancel Shift";
              res.send(data);
            } else {
              if (shiftData.employee_requested.length === 1) {
                db.UpdateDocument("shifts", { _id: req.body.id, employee_requested: { $elemMatch: { employee: req.params.loginId } } }, { status: 1, employee_requested: [], $unset: { employee: "" } }, {}, (err, result) => {
                  if (err) {
                    data.response = "Unable to Save Your Data Please try again";
                    data.err = err;
                    data.request = req.body;
                    res.send(data);
                  } else {
                    db.GetOneDocument("agencies", { _id: shiftData.agency }, {}, {}, (err, AgencyData) => {
                      if (AgencyData) {
                        if (AgencyData.settings && AgencyData.settings.notifications.email) {
                          // Mail Sent To All
                          const mailData = {};
                          mailData.template = "shiftcancelnotificationtoagencyclient";
                          mailData.to = AgencyData.email;
                          mailData.html = [];
                          mailData.html.push({ name: "username", value: AgencyData.username });
                          mailData.html.push({ name: "role", value: AgencyData.job_type });
                          mailcontent.sendmail(mailData, (err, response) => {
                            console.log("mail err,mail response", err, response);
                          });
                        }
                        if (AgencyData.settings && AgencyData.settings.notifications.sms) {
                          // SMS Send To All
                          const smsto = AgencyData.phone.code + AgencyData.phone.number;
                          const smsmessage = `Hi ${AgencyData.username} , You Got New Shift Cancellation For Role ${AgencyData.job_type}`;
                          twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                            console.log("SMS err,SMS response", err, response);
                          });
                        }
                      }
                    });
                    db.GetOneDocument("clients", { _id: shiftData.client }, {}, {}, (err, ClientData) => {
                      if (ClientData) {
                        if (ClientData.settings && ClientData.settings.notifications.email) {
                          // Mail Sent To All
                          const mailData = {};
                          mailData.template = "shiftcancelnotificationtoagencyclient";
                          mailData.to = ClientData.email;
                          mailData.html = [];
                          mailData.html.push({
                            name: "username",
                            value: ClientData.username
                          });
                          mailData.html.push({
                            name: "role",
                            value: ClientData.job_type
                          });
                          mailcontent.sendmail(mailData, (err, response) => {
                            console.log("mail err,mail response", err, response);
                          });
                        }
                        if (ClientData.settings && ClientData.settings.notifications.sms) {
                          // SMS Send To All
                          const smsto = ClientData.phone.code + ClientData.phone.number;
                          const smsmessage = `Hi ${ClientData.username} , You Got New Shift Cancellation For Role ${ClientData.job_type}`;
                          twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                            console.log("SMS err,SMS response", err, response);
                          });
                        }
                      }
                    });
                    db.GetOneDocument("shifts", { _id: shiftData._id }, {}, {}, (err, shiftData) => {
                      if (err || !shiftData) {
                        data.response = err;
                        return res.send(data);
                      } else {
                        if (shiftData.status) {
                          const userQuery = [];
                          userQuery.push({
                            $match: {
                              job_type: { $eq: shiftData.job_type }
                            }
                          });
                          userQuery.push({
                            $match: {
                              locations: { $eq: shiftData.locations }
                            }
                          });

                          db.GetAggregation("employees", userQuery, (err, employeeData) => {
                            if (err || !employeeData) {
                              data.response = "Unable to get your data, Please try again";
                              res.send(data);
                            } else {
                              if (employeeData.length <= 0) {
                                data.response = "Employees not found";
                                res.send(data);
                              } else {
                                const employee_requested = [];
                                employeeData.map(function(emp) {
                                  const empobj = {
                                    employee: emp._id,
                                    status: 0
                                  };
                                  employee_requested.push(empobj);
                                  const message = "New Job request";
                                  const options = {
                                    shift: shiftData._id,
                                    emp_id: emp._id
                                  };
                                  push.addnotification(emp._id, message, "job_request", options, "EMPLOYEE", (err, response, body) => {});
                                  if (emp.settings && emp.settings.notifications.email) {
                                    // Mail Sent To All
                                    const mailData = {};
                                    mailData.template = "newshiftrequesttoemployee";
                                    mailData.to = emp.email;
                                    mailData.html = [];
                                    mailData.html.push({
                                      name: "username",
                                      value: emp.username
                                    });
                                    mailData.html.push({
                                      name: "role",
                                      value: emp.job_type
                                    });
                                    mailcontent.sendmail(mailData, (err, response) => {
                                      console.log("mail err,mail response", err, response);
                                    });
                                  }
                                  if (emp.settings && emp.settings.notifications.sms) {
                                    // SMS Send To All
                                    const smsto = emp.phone.code + emp.phone.number;
                                    const smsmessage = `Hi ${emp.username} , You Got New Shift For Role ${emp.job_type}`;
                                    twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                                      console.log("SMS err,SMS response", err, response);
                                    });
                                  }
                                });

                                db.UpdateDocument(
                                  "shifts",
                                  { _id: shiftData._id },
                                  {
                                    employee_requested: employee_requested,
                                    status: 2
                                  },
                                  {},
                                  function(err, docdata) {
                                    data.status = 1;
                                    data.response = "employee Unassigned successfully.";
                                    const message = "shift_unassigned";
                                    const options = {
                                      shift: shiftData._id,
                                      emp_id: shiftData.employee
                                    };
                                    push.addnotification(shiftData.agency, message, "shift_unassigned", options, "AGENCY", (err, response, body) => {});
                                    push.addnotification(shiftData.client, message, "shift_unassigned", options, "CLIENT", (err, response, body) => {});
                                    res.send(data);
                                    event.emit("shiftstatdelete", { id: shiftData._id, status: 4 });
                                  }
                                );
                              }
                            }
                          });
                        } else {
                          data.response = "Request sent already";
                          res.send(data);
                        }
                      }
                    });
                  }
                });
              }
              if (shiftData.employee_requested.length > 1) {
                db.UpdateDocument(
                  "shifts",
                  {
                    _id: req.body.id,
                    employee_requested: {
                      $elemMatch: { employee: req.params.loginId }
                    }
                  },
                  {
                    "status": 2,
                    "employee_requested.$.status": 0,
                    "$unset": { employee: "" }
                  },
                  {},
                  (err, result) => {
                    // console.log("err, result",err, result);
                    if (err) {
                      data.response = "Unable to Save Your Data Please try again";
                      data.err = err;
                      data.request = req.body;
                      res.send(data);
                    } else {
                      db.GetOneDocument("agencies", { _id: shiftData.agency }, {}, {}, (err, AgencyData) => {
                        if (AgencyData) {
                          if (AgencyData.settings && AgencyData.settings.notifications.email) {
                            // Mail Sent To All
                            const mailData = {};
                            mailData.template = "shiftcancelnotificationtoagencyclient";
                            mailData.to = AgencyData.email;
                            mailData.html = [];
                            mailData.html.push({
                              name: "username",
                              value: AgencyData.username
                            });
                            mailData.html.push({
                              name: "role",
                              value: AgencyData.job_type
                            });
                            mailcontent.sendmail(mailData, (err, response) => {
                              console.log("mail err,mail response", err, response);
                            });
                          }
                          if (AgencyData.settings && AgencyData.settings.notifications.sms) {
                            // SMS Send To All
                            const smsto = AgencyData.phone.code + AgencyData.phone.number;
                            const smsmessage = `Hi ${AgencyData.username} , You Got New Shift For Role ${AgencyData.job_type}`;
                            twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                              console.log("SMS err,SMS response", err, response);
                            });
                          }
                        }
                      });
                      db.GetOneDocument("clients", { _id: shiftData.client }, {}, {}, (err, ClientData) => {
                        if (ClientData) {
                          if (ClientData.settings && ClientData.settings.notifications.email) {
                            // Mail Sent To All
                            const mailData = {};
                            mailData.template = "shiftcancelnotificationtoagencyclient";
                            mailData.to = ClientData.email;
                            mailData.html = [];
                            mailData.html.push({
                              name: "username",
                              value: ClientData.username
                            });
                            mailData.html.push({
                              name: "role",
                              value: ClientData.job_type
                            });
                            mailcontent.sendmail(mailData, (err, response) => {
                              console.log("mail err,mail response", err, response);
                            });
                          }
                          if (ClientData.settings && ClientData.settings.notifications.sms) {
                            // SMS Send To All
                            const smsto = ClientData.phone.code + ClientData.phone.number;
                            const smsmessage = `Hi ${ClientData.username} , You Got New Shift For Role ${ClientData.job_type}`;
                            twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                              console.log("SMS err,SMS response", err, response);
                            });
                          }
                        }
                      });
                      db.GetOneDocument("shifts", { _id: shiftData._id }, {}, {}, (err, shiftData) => {
                        if (err || !shiftData) {
                          data.response = err;
                          return res.send(data);
                        } else {
                          const userQuery = [];
                          userQuery.push({
                            $match: {
                              job_type: { $eq: shiftData.job_type }
                            }
                          });
                          userQuery.push({
                            $match: {
                              locations: { $eq: shiftData.locations }
                            }
                          });

                          db.GetAggregation("employees", userQuery, (err, employeeData) => {
                            if (err || !employeeData) {
                              data.response = "Unable to get your data, Please try again";
                              res.send(data);
                            } else {
                              if (employeeData.length <= 0) {
                                data.response = "Employees not found";
                                res.send(data);
                              } else {
                                const employee_requested = [];
                                employeeData.map(function(emp) {
                                  const empobj = {
                                    employee: emp._id,
                                    status: 0
                                  };
                                  employee_requested.push(empobj);
                                  const message = "New Job request";
                                  const options = {
                                    shift: shiftData._id,
                                    emp_id: emp._id
                                  };
                                  push.addnotification(emp._id, message, "job_request", options, "EMPLOYEE", (err, response, body) => {});
                                  if (emp.settings && emp.settings.notifications.email) {
                                    // Mail Sent To All
                                    const mailData = {};
                                    mailData.template = "newshiftrequesttoemployee";
                                    mailData.to = emp.email;
                                    mailData.html = [];
                                    mailData.html.push({
                                      name: "username",
                                      value: emp.username
                                    });
                                    mailData.html.push({
                                      name: "role",
                                      value: emp.job_type
                                    });
                                    mailcontent.sendmail(mailData, (err, response) => {
                                      console.log("mail err,mail response", err, response);
                                    });
                                  }
                                  if (emp.settings && emp.settings.notifications.sms) {
                                    // SMS Send To All
                                    const smsto = emp.phone.code + emp.phone.number;
                                    const smsmessage = `Hi ${emp.username} , You Got New Shift For Role ${emp.job_type}`;
                                    twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                                      console.log("SMS err,SMS response", err, response);
                                    });
                                  }
                                });

                                db.UpdateDocument(
                                  "shifts",
                                  { _id: shiftData._id },
                                  {
                                    employee_requested: employee_requested,
                                    status: 2
                                  },
                                  {},
                                  function(err, docdata) {
                                    data.status = 1;
                                    data.response = "employee Unassigned successfully.";
                                    const message = "shift_unassigned";
                                    const options = {
                                      shift: shiftData._id,
                                      emp_id: shiftData.employee
                                    };
                                    push.addnotification(shiftData.agency, message, "shift_unassigned", options, "AGENCY", (err, response, body) => {});
                                    push.addnotification(shiftData.client, message, "shift_unassigned", options, "CLIENT", (err, response, body) => {});
                                    res.send(data);
                                  }
                                );
                              }
                            }
                          });
                        }
                      });
                    }
                  }
                );
              }
            }
          }
        });
      }
    });
  };
  router.earningsexport = (req, res) => {
    const data = {};
    data.status = 0;

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const listQuery = [];
    listQuery.push({
      $match: {
        employee: { $eq: new mongoose.Types.ObjectId(req.params.loginId) }
      }
    });
    listQuery.push({ $match: { status: { $eq: 2 } } });

    if (req.body.status) {
      listQuery.push({ $match: { status: { $eq: req.body.status } } });
    } else {
      listQuery.push({ $match: { status: 2 } });
    }
    listQuery.push(
      {
        $lookup: {
          from: "clients",
          let: { client: "$client" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$client"]
                }
              }
            },
            { $project: { companyname: 1 } }
          ],
          as: "client_data"
        }
      },
      { $addFields: { client: "$client_data.companyname" } }
    );
    listQuery.push({
      $lookup: {
        from: "clients",
        let: { branch: "$branch" },
        pipeline: [
          {
            $unwind: "$branches"
          },
          {
            $match: { $expr: { $eq: ["$branches._id", "$$branch"] } }
          },
          { $project: { _id: 0, branches: 1 } }
        ],
        as: "branch"
      }
    });
    // listQuery.push({
    //     $lookup: {
    //         from: 'invoice',
    //         let: { idd: "$_id" },
    //         pipeline: [{
    //             $match: { $expr: { $eq: [ '$shift_mid', '$$idd' ] } }
    //         },{ $project: {_id:0, "employee_rate": 1}}],
    //         as: 'invoice'
    //     }
    // }, {$addFields: {employee_rate: "$invoice.employee_rate"}});
    listQuery.push(
      {
        $lookup: {
          from: "locations",
          let: { locid: "$locations" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$locid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "location_data"
        }
      },
      {
        $addFields: { locations: { $arrayElemAt: ["$location_data.name", 0] } }
      }
    );

    listQuery.push(
      {
        $lookup: {
          from: "jobtypes",
          let: { agencyid: "$job_type" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "jobname"
        }
      },
      {
        $addFields: { job_type: { $arrayElemAt: ["$jobname.name", 0] } }
      },
      { $project: { jobname: 0 } }
    );

    listQuery.push(
      {
        $lookup: {
          from: "employees",
          let: { agencyid: "$employee" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1 } }
          ],
          as: "empname"
        }
      },
      {
        $addFields: { employee: { $arrayElemAt: ["$empname.name", 0] } }
      },
      { $project: { empname: 0 } }
    );

    // var withoutlimit = JSON.parse(JSON.stringify(listQuery));
    // withoutlimit.push({ $count:'count'});

    const withoutlimit = Object.assign([], listQuery);
    withoutlimit.push({ $count: "count" });

    if (req.body.skip >= 0) {
      listQuery.push({ $skip: parseInt(req.body.skip) });
    }

    if (req.body.limit >= 0) {
      listQuery.push({ $limit: parseInt(req.body.limit) });
    }

    if (req.body.search) {
      const searchs = req.body.search;

      if (req.body.filter === "all") {
        listQuery.push({
          $match: {
            $or: [
              { title: { $regex: searchs + ".*", $options: "si" } },
              { shift: { $regex: searchs + ".*", $options: "si" } },
              { client: { $regex: searchs + ".*", $options: "si" } },
              { employee: { $regex: searchs + ".*", $options: "si" } },
              { locations: { $regex: searchs + ".*", $options: "si" } },
              { branch: { $regex: searchs + ".*", $options: "si" } },
              { name: { $regex: searchs + ".*", $options: "si" } },
              { job_type: { $regex: searchs + ".*", $options: "si" } }
            ]
          }
        });
      } else {
        const searching = {};
        searching[req.body.filter] = { $regex: searchs + ".*", $options: "si" };
        listQuery.push({ $match: searching });
      }
    }
    if (req.body.from_date) {
      listQuery.push({
        $match: { createdAt: { $gt: new Date(req.body.from_date) } }
      });
    }

    if (req.body.to_date) {
      listQuery.push({
        $match: { createdAt: { $lt: new Date(req.body.to_date) } }
      });
    }

    if (req.body.field && req.body.order) {
      const sorting = {};
      sorting[req.body.field] = parseInt(req.body.order);
      listQuery.push({ $sort: sorting });
    } else {
      listQuery.push({ $sort: { createdAt: -1 } });
    }
    const finalQuery = [
      {
        $facet: {
          overall: withoutlimit,
          earnings: [
            {
              $match: {
                $or: [
                  {
                    employee: {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    }
                  },
                  {
                    "employee_requested.employee": {
                      $eq: new mongoose.Types.ObjectId(req.params.loginId)
                    }
                  }
                ]
              }
            },
            {
              $match: {
                $or: [{ status: 1 }, { status: 2 }]
              }
            },
            {
              $group: {
                _id: "$status",
                totalamount: { $sum: "$employee_rate" },
                count: { $sum: 1 }
              }
            }
          ],
          result: listQuery
        }
      }
    ];

    db.GetAggregation("invoice", finalQuery, function(err, docData) {
      if (err || !docData) {
        data.err = err;
        data.response = "unable to get Your Data Please Try Again";
        res.send(data);
      } else {
        const obj = {
          employee: req.params.loginId,
          action: req.body.notification
        };
        // console.log("==obj", obj);
        if (req.body.notification) {
          dashboard.notification_status(obj, function(err, response) {});
        }

        data.status = 1;
        let fullcount;
        if (docData[0].overall[0] && docData[0].overall[0].count) {
          fullcount = docData[0].overall[0].count;
        } else {
          fullcount = docData[0].result.length;
        }
        data.response = {
          result: docData[0].result,
          length: docData[0].result.length,
          fullcount: fullcount,
          earnings: docData[0].earnings,
          listQuery: listQuery
        };
        const fields = ["createdAt", "shift_id", "employee", "client", "branch[0].branches.branchname", "employee_rate"];
        const fieldNames = ["Date", "Shift", "Employee", "Client", "Branch", "Employee Rate"];
        const mydata = docData[0].result;
        for (let i = 0; i < mydata.length; i++) {
          mydata[i].createdAt = moment(mydata[i].createdAt).format("DD/MM/YYYY");
        }
        const json2csvParser = new json2csv({ fields, fieldNames });
        const csv = json2csvParser.parse(mydata);
        if (csv) {
          data.status = 1;
          data.response = csv;
          res.send(data);
        } else {
          data.status = 0;
          data.response = "Export Failed";
          res.send(data);
        }
      }
    });
  };
  router.notificationssettings = (req, res) => {
    const data = {};
    data.status = 0;

    req.checkBody("mailstatus", "Required Email Status").optional();
    req.checkBody("smsstatus", "Required Name Status").optional();

    const errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }

    const employee = {
      settings: {
        notifications: {
          email: req.body.mailstatus,
          sms: req.body.smsstatus
        }
      }
    };

    db.UpdateDocument("employees", { _id: req.params.loginId }, employee, { upsert: true }, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Unable to save your data, Please try again";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Updated successfully.";
        res.send(data);
      }
    });
  };
  router.clearnotification = (req, res) => {
    const data = {};
    db.UpdateMany("notifications", { employee: req.params.loginId }, { status: 2 }, {}, (err, result) => {
      if (err) {
        data.status = 0;
        data.response = "Please try again later";
        res.send(data);
      } else {
        data.status = 1;
        data.response = "Cleared";
        res.send(data);
      }
    });
  };
  return router;
};
