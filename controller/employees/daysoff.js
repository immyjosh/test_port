let db = require("../../model/mongodb");
let CONFIG = require("../../config/config");
let mongoose = require("mongoose");

module.exports = () => {
  const router = {};

  let data = {};
  data.status = 0;

  router.forshiftslist = (req, res) => {
    let errors = req.validationErrors();
    if (errors) {
      data.response = errors[0].msg;
      return res.send(data);
    }
    db.GetOneDocument("employees", {_id: req.params.loginId}, {}, {}, function(err, EmployeeData) {
      if (err || !EmployeeData) {
        data.response = "Unable to get your data, Please try again";
        res.send(data);
      } else {
        let daysQuery = [];
        daysQuery.push({$match: {addedBy: {$eq: "agency"}}});
        daysQuery.push({$match: {addedId: {$eq: new mongoose.Types.ObjectId(EmployeeData.agency)}}});
        daysQuery.push({$match: {status: {$exists: true}}});
        daysQuery.push({$match: {status: {$eq: 1}}}, {$unwind: "$dates"});
        daysQuery.push({$sort: {createdAt: -1}});
        daysQuery.push({$project: {createdAt: 1, updatedAt: 1, name: 1, activity: 1, status: 1, reason: 1, numberofday: 1, dates: 1}});

        db.GetAggregation("daysoff", daysQuery, function(err, docData) {
          if (err || !docData) {
            data.response = "Unable to Get Your Data Please Try Again";
            res.send(data);
          } else {
            data.status = 1;
            data.response = {result: docData, daysQuery: daysQuery};
            res.send(data);
          }
        });
      }
    });
  };

  return router;
};
