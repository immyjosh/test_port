module.exports = io => {
  const moment = require("moment");
  const db = require("../model/mongodb.js");
  const mail = require("../model/mail.js");
  const stripe = require("stripe")("");
  const mailcontent = require("../model/mailcontent");
  const twilio = require("../model/twilio.js");
  const exec = require("child_process").exec;
  const CronJob = require("cron").CronJob;
  const CONFIG = require("../config/config.json");
  const mongoose = require("mongoose");
  const push = require("../model/pushNotification.js")(io);
  const Cycle_Dates = (agency, type, schema) => {
    db.GetOneDocument("settings", { alias: "general" }, {}, {}, (err, settings) => {
      if (err) {
        console.log("err ", err);
      } else {
        const billingcycle = parseInt(settings.settings.billingcycle);
        const ext = {};
        ext.sort = { createdAt: -1 };
        db.GetOneDocument("billing", { agency: new mongoose.Types.ObjectId(agency), type: type }, {}, ext, (err, billingcycyle) => {
          if (err) {
            console.log("err ", err);
          } else {
            if (!billingcycyle) {
              const ext = {};
              ext.sort = { createdAt: -1 };
              db.GetOneDocument(schema, { agency: new mongoose.Types.ObjectId(agency) }, {}, ext, (err, docdata) => {
                if (err) {
                  console.log("err ", err);
                } else {
                  if (docdata) {
                    const date = docdata.createdAt;
                    const startDate = moment(date).format("YYYY/MM/DD 00:00:00.000");
                    const endDate = moment().format("YYYY/MM/DD 00:00:00.000");
                    const diffDays = moment(new Date(endDate)).diff(moment(new Date(startDate)), "days");
                    if (diffDays >= billingcycle) {
                      const data = {};
                      data.agency = agency;
                      data.type = type;
                      data.start_date = moment(new Date(startDate));
                      data.end_date = moment(new Date(endDate));
                      data.billingcycyle = data.start_date.format("DD/MM/YYYY") + "-" + data.end_date.format("DD/MM/YYYY");
                      db.InsertDocument("billing", data, (err, result) => {});
                    }
                  }
                }
              });
            } else {
              const date = billingcycyle.end_date;
              const startDate = moment(date).format("YYYY/MM/DD 00:00:00.000");
              const endDate = moment(date)
                .add(billingcycle, "day")
                .format("YYYY/MM/DD 00:00:00.000");
              const currentDate = moment().format("YYYY/MM/DD 00:00:00.000");
              const endDateUnix = moment(new Date(endDate)).unix();
              const currentDateUnix = moment(new Date(currentDate)).unix();
              if (endDateUnix <= currentDateUnix) {
                const data = {};
                data.agency = agency;
                data.type = type;
                data.start_date = moment(new Date(startDate));
                data.end_date = moment(new Date(endDate));
                data.billingcycyle = data.start_date.format("DD/MM/YYYY") + "-" + data.end_date.format("DD/MM/YYYY");
                db.GetOneDocument("billing", { billingcycyle: data.billingcycyle, agency: new mongoose.Types.ObjectId(agency), type: type }, {}, {}, (err, billingcycyle) => {
                  if (err) {
                    console.log("err ", err);
                  } else {
                    if (!billingcycyle) {
                      db.InsertDocument("billing", data, (err, result) => {});
                    }
                  }
                });
              }
            }
          }
        });
      }
    });
  };
  /*
var job = new CronJob({
    cronTime: '0 0 * * *', //Daily Cron Check @ 00:00
    onTick: function () {
        db.GetOneDocument('settings', { "alias": 'general' }, {}, {}, function (err, settings) {
            if (err) {
                res.send(err);
            } else {
                var billingcycle = parseInt(settings.settings.billingcycle);
                var ext = {};
                ext.sort = { createdAt: -1 };
                db.GetOneDocument('billing', {}, {}, ext, function (err, billingcycyle) {
                    if (err) {
                    } else {
                        if (!billingcycyle) {
                            var ext = {};
                            ext.sort = { createdAt: -1 };
                            db.GetOneDocument('task', { 'status': 7 }, {}, ext, function (err, docdata) {
                                if (err) {
                                } else {
                                    if (docdata) {
                                        var date = docdata.createdAt;
                                        var startDate = moment(date).format("YYYY/MM/DD 00:00:00.000");
                                        var endDate = moment().format("YYYY/MM/DD 00:00:00.000");
                                        var diffDays = moment(new Date(endDate)).diff(moment(new Date(startDate)), 'days');
                                        if (diffDays >= billingcycle) {
                                            var data = {};
                                            data.start_date = moment(new Date(startDate));
                                            data.end_date = moment(new Date(endDate));
                                            data.billingcycyle = data.start_date.format("YYYY/MM/DD") + '-' + data.end_date.format("YYYY/MM/DD");
                                            db.InsertDocument('billing', data, function (err, result) { });
                                        }
                                    }
                                }
                            });
                        } else {
                            var date = billingcycyle.end_date;
                            var startDate = moment(date).format("YYYY/MM/DD 00:00:00.000");
                            var endDate = moment(date).add(billingcycle, 'day').format("YYYY/MM/DD 00:00:00.000");
                            var currentDate = moment().format("YYYY/MM/DD 00:00:00.000");
                            var endDateUnix = moment(new Date(endDate)).unix();
                            var currentDateUnix = moment(new Date(currentDate)).unix();
                            if (endDateUnix <= currentDateUnix) {
                                var data = {};
                                data.start_date = moment(new Date(startDate));
                                data.end_date = moment(new Date(endDate));
                                data.billingcycyle = data.start_date.format("YYYY/MM/DD") + '-' + data.end_date.format("YYYY/MM/DD");
                                db.GetOneDocument('billing', { 'billingcycyle': data.billingcycyle }, {}, {}, function (err, billingcycyle) {
                                    if (err) {
                                    } else {
                                        if (!billingcycyle) {
                                            db.InsertDocument('billing', data, function (err, result) { });
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    },
    start: false,
    //timeZone: 'America/Los_Angeles'
});
job.start();*/

  const subscriptionRemainder = new CronJob({
    cronTime: "* * */1 * * *", // Daily Cron Check @ 00:00
    onTick: function() {
      const date = moment(new Date()).format("YYYY-MM-DD");
      const start = moment(new Date()).format("YYYY-MM-DD") + " 00:00:00";
      const end = moment(new Date()).format("YYYY-MM-DD") + " 23:59:59";

      const finalquery = [{ $match: { remainder: { $gte: new Date(start) } } }, { $match: { remainder: { $lte: new Date(end) } } }, { $match: { status: { $eq: 1 } } }];

      finalquery.push({
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1, email: 1 } }
          ],
          as: "agency_data"
        }
      });

      db.GetAggregation("subscriptions", finalquery, (err, docData) => {
        if (docData && docData.length > 0) {
          docData.map(function(sub) {
            // console.log("Remainder",sub.agency_data);  //Mail or notification;
            const end_date = moment(new Date(sub.end)).format("YYYY-MM-DD");
            const message = "Your Plan Subscription going to be expired on " + end_date;
            const options = { agency: sub.agency, remainder: sub.remainder, end: sub.end };
            // push.addnotification(sub.agency, message, 'subscription_remaider', options, 'AGENCY', (err, response, body) => {});
          });
        }
      });
    },
    start: false
    // timeZone: 'America/Los_Angeles'
  });
  subscriptionRemainder.start();

  const inactivateSubscription = new CronJob({
    cronTime: "* * */1 * * *", // Daily Cron Check @ 00:00
    onTick: function() {
      const finalquery = [{ $match: { status: { $eq: 1 } } }, { $match: { end: { $lt: new Date() } } }];

      finalquery.push({
        $lookup: {
          from: "agencies",
          let: { agencyid: "$agency" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$_id", "$$agencyid"]
                }
              }
            },
            { $project: { name: 1, email: 1 } }
          ],
          as: "agency_data"
        }
      });

      db.GetAggregation("subscriptions", finalquery, (err, docData) => {
        if (docData && docData.length > 0) {
          docData.map(function(sub) {
            // console.log("InactiveSubscription", sub.agency_data); // Mail or notification;
            db.UpdateDocument("subscriptions", { _id: sub._id }, { status: 0 }, {}, (suberr, result) => {
              // console.log("Inactive", sub.agency_data); // Mail or notification;
              const end_date = moment(new Date(sub.end)).format("YYYY-MM-DD");
              const message = "Your Plan is expired";
              const options = { agency: sub.agency, subscription: sub._id, end: sub.end };
              push.addnotification(sub.agency, message, "subscription_expired", options, "AGENCY", (err, response, body) => {});
            });
          });
        }
      });
    },
    start: false
    // timeZone: 'America/Los_Angeles'
  });
  inactivateSubscription.start();
  const ShiftStartingReminder = new CronJob({
    cronTime: "0 0 */1 * * *", // Run Cron Check every 1hr
    onTick: function() {
      const date = moment(new Date()).format("YYYY-MM-DD");
      const start = moment(new Date()).format("YYYY-MM-DD") + " 00:00:00";
      const end = moment(new Date()).format("YYYY-MM-DD") + " 23:59:59";
      db.GetDocument("shifts", { status: 4, start_date: { $gte: start, $lte: end } }, {}, {}, (err, docData) => {
        // console.log("err, unity", err, docData.length);
        if (err || !docData) {
          // console.log("err || !docData", err || !docData);
        } else {
          const getshiftdata = [];
          docData &&
            docData.map(list => {
              const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
              const evestarthr = (list.starttime - 3600) / 3600;
              const splithr = evestarthr.toString().split(".");
              const startsec = splithr[1];
              let startmins;
              let StartTime;
              if (startsec === undefined) {
                startmins = "00";
              } else {
                startmins = (+"0" + "." + startsec) * 60;
              }
              if (num.indexOf(splithr[0]) === -1) {
                StartTime = splithr[0] + ":" + startmins;
              } else {
                StartTime = +"0" + "" + splithr[0] + ":" + startmins;
              }
              getshiftdata.push({
                start_date: list.start_date,
                starttimes: StartTime,
                employee: list.employee
              });
            });
          const FdocData =
            getshiftdata &&
            getshiftdata.length > 0 &&
            getshiftdata.filter(list => {
              const SMSTime = moment(list.start_date).format(`DD-MM-YYYY ${list.starttimes}:00`);
              const Stime = moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
              const today = new Date();
              today.setMinutes(today.getMinutes() + 60);
              const Etime = moment(today).format("DD-MM-YYYY HH:mm:ss");
              return SMSTime >= Stime && SMSTime <= Etime;
            });
          // console.log("FdocData", FdocData);
          FdocData &&
            FdocData.length > 0 &&
            FdocData.map(list => {
              db.GetOneDocument("employees", { _id: list.employee }, {}, {}, (err, EmployeeData) => {
                if (EmployeeData) {
                  if (EmployeeData.settings && EmployeeData.settings.notifications.email) {
                    // Mail Sent To All
                    const mailData = {};
                    mailData.template = "shiftremainder";
                    mailData.to = EmployeeData.email;
                    mailData.html = [];
                    mailData.html.push({ name: "username", value: EmployeeData.username });
                    mailData.html.push({ name: "role", value: EmployeeData.job_type });
                    mailcontent.sendmail(mailData, (err, response) => {
                      console.log("mail err,mail response", err, response);
                    });
                  }
                  if (EmployeeData.settings && EmployeeData.settings.notifications.sms) {
                    // SMS Send To All
                    const smsto = EmployeeData.phone.code + EmployeeData.phone.number;
                    const smsmessage = `Hi ${EmployeeData.username} , Shift Remainder For Role ${EmployeeData.job_type}`;
                    twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                      console.log("SMS err,SMS response", err, response);
                    });
                  }
                }
              });
            });
        }
      });
    },
    start: false
    // timeZone: 'America/Los_Angeles'
  });
  ShiftStartingReminder.start();

  const ShiftRequestExpiry = new CronJob({
    cronTime: "0 0 */1 * * *", // Run Cron Check every 1hr
    onTick: function() {
      db.GetDocument("shifts", { status: 2 }, {}, {}, (err, Shift_data) => {
        if (err || !Shift_data) {
          console.log("err", err);
        } else {
          Shift_data.map(list => {
            db.GetOneDocument("agencies", { _id: list.agency }, {}, {}, (err, AgencyData) => {
              const Expiry_time = moment(list.requested_time).add(AgencyData.settings.general.shift_request_expiry, "seconds");
              const CurrentTime = moment(new Date());
              if (CurrentTime > Expiry_time) {
                db.UpdateDocument("shifts", { _id: list._id }, { status: 10 }, {}, function(err, docdata) {
                  // console.log("err, Expiry", err, docdata);
                  if (docdata) {
                    db.GetOneDocument("agencies", { _id: list.agency }, {}, {}, (err, AgencyData) => {
                      if (AgencyData) {
                        if (AgencyData.settings.notifications.email) {
                          // Mail Sent To All
                          const mailData = {};
                          mailData.template = "shiftexpired";
                          mailData.to = AgencyData.email;
                          mailData.html = [];
                          mailData.html.push({ name: "username", value: AgencyData.username });
                          mailData.html.push({ name: "role", value: AgencyData.job_type });
                          mailcontent.sendmail(mailData, (err, response) => {
                            console.log("mail err,mail response", err, response);
                          });
                        }
                        if (AgencyData.settings.notifications.sms) {
                          // SMS Send To All
                          const smsto = AgencyData.phone.code + AgencyData.phone.number;
                          const smsmessage = `Hi ${AgencyData.username} , Shift Expired For Role ${AgencyData.job_type}`;
                          twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                            console.log("SMS err,SMS response", err, response);
                          });
                        }
                      }
                    });
                    db.GetOneDocument("clients", { _id: list.client }, {}, {}, (err, ClientData) => {
                      if (ClientData) {
                        if (ClientData.settings && ClientData.settings.notifications.email) {
                          // Mail Sent To All
                          const mailData = {};
                          mailData.template = "shiftexpired";
                          mailData.to = ClientData.email;
                          mailData.html = [];
                          mailData.html.push({ name: "username", value: ClientData.username });
                          mailData.html.push({ name: "role", value: ClientData.job_type });
                          mailcontent.sendmail(mailData, (err, response) => {
                            console.log("mail err,mail response", err, response);
                          });
                        }
                        if (ClientData.settings && ClientData.settings.notifications.sms) {
                          // SMS Send To All
                          const smsto = ClientData.phone.code + ClientData.phone.number;
                          const smsmessage = `Hi ${ClientData.username} , Shift Expired For Role ${ClientData.job_type}`;
                          twilio.createMessage(smsto, "", smsmessage, (err, response) => {
                            console.log("SMS err,SMS response", err, response);
                          });
                        }
                      }
                    });
                  }
                });
              }
            });
          });
        }
      });
    },
    start: false
    // timeZone: 'America/Los_Angeles'
  });
  ShiftRequestExpiry.start();

  const Subscription_Auto_Renewal = new CronJob({
    cronTime: "0 0 1 * * *", // Run Cron At 01:00 Everyday
    onTick: function() {
      db.GetDocument("agencies", { auto_renewal: 1, customer_id: { $exists: true } }, { _id: 1 }, {}, (err, Agencydata) => {
        if (err || !Agencydata) {
          console.log("err", err);
        } else {
          if (Agencydata && Agencydata.length > 0) {
            Agencydata.map(items => {
              const planQuery = [
                { $match: { agency: { $eq: items._id }, status: 1, payment_status: 1 } },
                { $sort: { createdAt: -1 } },
                { $limit: 1 },
                {
                  $lookup: {
                    from: "agencies",
                    let: { agency_id: items._id },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $eq: ["$_id", "$$agency_id"]
                          }
                        }
                      },
                      {
                        $project: { customer_id: 1, auto_renewal: 1 }
                      }
                    ],
                    as: "agency_data"
                  }
                }
              ];
              db.GetAggregation("subscriptions", planQuery, (err, docData) => {
                if (err) {
                  console.log("err", err);
                } else {
                  const Subscription_data = docData && docData.length > 0 && docData.filter(list => moment(list.end).format("DD-MM-YYYY") === moment(new Date()).format("DD-MM-YYYY"));
                  if (Subscription_data && Subscription_data.length > 0) {
                    const CustomerID = Subscription_data[0].agency_data[0].customer_id;
                    const AgencyID = Subscription_data[0].agency_data[0]._id;
                    db.GetOneDocument("settings", { alias: "payment_gateway" }, {}, {}, (err, paymentgateway) => {
                      if (err || !paymentgateway.settings.stripe.secret_key) {
                        console.log("TCL: err", err);
                      } else {
                        stripe.setApiKey(paymentgateway.settings.stripe.secret_key);
                        stripe.customers.retrieve(CustomerID, (err, customer) => {
                          if (err || !customer) {
                            console.log("TCL: err", err);
                          } else {
                            stripe.customers.retrieveCard(customer.id, customer.default_source, (err, card) => {
                              if (err || !card) {
                                console.log("TCL: err", err);
                              } else {
                                const meta = card.metadata;
                                const account_details = { name: meta.name, email: meta.email, address: meta.address, number: meta.number, code: meta.code };
                                const planData = {
                                  _id: meta.plans,
                                  name: meta.planname,
                                  freedays: meta.planfreedays,
                                  noofdays: meta.plannoofdays,
                                  amount: meta.planamount,
                                  description: meta.plandesc,
                                  remainder: meta.planremainder,
                                  employees: meta.planemployee,
                                  recruitment_module: meta.planrecruitment_module
                                };
                                const subscriptions = { agency: AgencyID, plan: planData, planId: planData._id };
                                subscriptions.start = moment(Date.now());
                                subscriptions.end = moment(subscriptions.start).add(Number(planData.noofdays), "days");
                                subscriptions.remainder = moment(Date.now()).add(Number(planData.remainder), "days");
                                subscriptions.type = "subscription";
                                db.InsertDocument("subscriptions", subscriptions, (err, Subresult) => {
                                  if (err || !Subresult) {
                                    console.log("TCL: err || !Subresult", err || !Subresult);
                                  } else {
                                    const subid = Subresult._id;
                                    const charge = {};
                                    charge.amount = parseInt(planData.amount) * 100;
                                    charge.currency = "gbp";
                                    charge.customer = card.customer;
                                    charge.source = card.id;
                                    charge.description = "Plan Subscription - ".subid;
                                    const end = moment(Date.now()).add(planData.noofdays, "days");
                                    stripe.charges.create(charge, (err, charges) => {
                                      if (err || !charges) {
                                        console.log("TCL: err", err);
                                      } else {
                                        if (charges) {
                                          const state_update = { payment_status: 1, payment_response: charges.id, status: 1, end: end, account_details: account_details };
                                          db.UpdateDocument("subscriptions", { _id: subid }, state_update, {}, (err, Finalresult) => {
                                            if (err) {
                                              console.log("TCL: err", err);
                                            } else {
                                              console.log("TCL: Finalresult", Finalresult);
                                            }
                                          });
                                        }
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                }
              });
            });
          }
        }
      });
    },
    start: false
    // timeZone: 'America/Los_Angeles'
  });
  Subscription_Auto_Renewal.start();

  const billing_cycle_dates = new CronJob({
    cronTime: "0 0 1 * * *", // Daily Cron Check @ 00:00
    onTick: function() {
      db.GetDocument("agencies", {}, { _id: 1 }, {}, (err, AgencyData) => {
        if (err) {
          console.log(err);
        } else {
          if (AgencyData && AgencyData.length > 0) {
            AgencyData.map(list => {
              Cycle_Dates(list.agency, "timesheet", "shifts");
              Cycle_Dates(list.agency, "invoice", "invoice");
            });
          }
        }
      });
    },
    start: false
    // timeZone: 'America/Los_Angeles'
  });
  billing_cycle_dates.start();
};
