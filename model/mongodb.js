const administrators = require("../schema/administrators.js");
const agencies = require("../schema/agencies.js");
const clients = require("../schema/clients.js");
const employees = require("../schema/employees.js");
const settings = require("../schema/settings.js");
const plans = require("../schema/plans.js");
const subscriptions = require("../schema/subscriptions.js");
const jobtypes = require("../schema/jobtypes.js");
// const clientjobtypes = require("../schema/clientjobtypes.js");
// const employeejobtypes = require("../schema/employeejobtypes.js");
const shifttemplates = require("../schema/shifttemplates.js");
const shifts = require("../schema/shifts.js");
const locations = require("../schema/locations.js");
const paymentgateway = require("../schema/paymentgateway.js");
const emailtemplate = require("../schema/emailtemplate.js");
const agencyemailtemplate = require("../schema/agencyemailtemplate.js");
const notifications = require("../schema/notification.js");
const daysoff = require("../schema/daysoff.js");
const invoice = require("../schema/invoice.js");
const timesheetcycle = require("../schema/timesheetcycle");
const invoicecycle = require("../schema/invoicecycle");
const stats = require("../schema/stats");
const billing = require("../schema/billing");

const db = {
  administrators: administrators,
  agencies: agencies,
  clients: clients,
  employees: employees,
  settings: settings,
  plans: plans,
  subscriptions: subscriptions,
  jobtypes: jobtypes,
  // clientjobtypes: clientjobtypes,
  // employeejobtypes: employeejobtypes,
  shifttemplates: shifttemplates,
  shifts: shifts,
  locations: locations,
  paymentgateway: paymentgateway,
  emailtemplate: emailtemplate,
  agencyemailtemplate: agencyemailtemplate,
  notifications: notifications,
  daysoff: daysoff,
  invoice: invoice,
  timesheetcycle: timesheetcycle,
  invoicecycle: invoicecycle,
  stats: stats,
  billing: billing,
};

const GetDocument = (model, query, projection, extension, callback) => {
  let Query = db[model].find(query, projection, extension.options);
  if (extension.populate) {
    // Query.populate(extension.populate);
    let popcond = {};
    if (extension.popcond) {
      popcond = extension.popcond;
    }
    Query.populate(extension.populate, null, popcond);
  }
  if (extension.sort) {
    Query.sort(extension.sort);
  }
  if (extension.limit) {
    Query.limit(extension.limit);
  }
  Query.exec((err, docs) => {
    if (extension.count) {
      Query.countDocuments((err, docs) => callback(err, docs));
    } else {
      callback(err, docs);
    }
  });
};

const GetOneDocument = (model, query, projection, extension, callback) => {
  let Query = db[model].findOne(query, projection, extension.options);
  if (extension.populate) {
    // Query.populate(extension.populate);
    let popcond = {};
    let poppro = {};
    if (extension.condition) {
      popcond = extension.condition;
    }
    if (extension.project) {
      poppro = extension.project;
    }
    Query.populate(extension.populate, poppro, popcond);
  }
  if (extension.sort) {
    Query.sort(extension.sort);
  }
  Query.exec((err, docs) => callback(err, docs));
};

const GetAggregation = (model, query, callback) => {
  db[model].aggregate(query).exec((err, docs) => callback(err, docs));
};

const InsertDocument = (model, docs, callback) => {
  let doc_obj = new db[model](docs);
  doc_obj.save((err, numAffected) => callback(err, numAffected));
};

const InsertMultiple = (model, docs, callback) => {
  db[model].insertMany(docs, (err, numAffected) => callback(err, numAffected));
};

const DeleteDocument = (model, criteria, callback) => {
  db[model].deleteOne(criteria, (err, docs) => callback(err, docs));
};

const UpdateDocument = (model, criteria, doc, options, callback) => {
  db[model].updateOne(criteria, doc, options, (err, docs) => callback(err, docs));
};
const UpdateMany = (model, criteria, doc, options, callback) => {
  db[model].updateMany(criteria, doc, options, (err, docs) => callback(err, docs));
};

const GetCount = (model, conditions, callback) => {
  db[model].countDocuments(conditions, (err, count) => callback(err, count));
};

const PopulateDocument = (model, docs, options, callback) => {
  db[model].populate(docs, options, (err, docs) => callback(err, docs));
};

const RemoveDocument = (model, criteria, callback) => {
  db[model].deleteOne(criteria, (err, docs) => callback(err, docs));
};
// ------------------------------------------------------------------------------------------
const GetAggregationDoc = (model, query) => {
  return new Promise((resolve, reject) => {
    db[model].aggregate(query).exec((err, docs) => {
      if (err) {
        reject(err);
      } else {
        resolve(docs);
      }
    });
  });
};

const GetOneDoc = (model, query, projection, extension) => {
  const Query = db[model].findOne(query, projection, extension.options);
  return new Promise((resolve, reject) => {
    if (extension.populate) {
      Query.populate(extension.populate);
    }
    if (extension.sort) {
      Query.sort(extension.sort);
    }
    Query.exec((err, docs) => {
      if (err) {
        reject(err);
      } else {
        resolve(docs);
      }
    });
  });
};

const GetDocs = (model, query, projection, extension) => {
  const Query = db[model].find(query, projection, extension.options);
  return new Promise((resolve, reject) => {
    if (extension.populate) {
      Query.populate(extension.populate);
    }
    if (extension.sort) {
      Query.sort(extension.sort);
    }
    Query.exec((err, docs) => {
      if (extension.count) {
        Query.countDocuments((err, docs) => {
          if (err) {
            reject(err);
          } else {
            resolve(docs);
          }
        });
      } else {
        if (err) {
          reject(err);
        } else {
          resolve(docs);
        }
      }
    });
  });
};

const UpdateDoc = (model, criteria, doc, options) => {
  return new Promise((resolve, reject) => {
    db[model].updateOne(criteria, doc, options, (err, docs) => {
      if (err) {
        reject(err);
      } else {
        resolve(docs);
      }
    });
  });
};

const InsertDoc = (model, docs) => {
  const doc_obj = new db[model](docs);
  return new Promise((resolve, reject) => {
    doc_obj.save((err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
};

module.exports = {
  GetDocument: GetDocument,
  GetOneDocument: GetOneDocument,
  InsertDocument: InsertDocument,
  DeleteDocument: DeleteDocument,
  UpdateDocument: UpdateDocument,
  UpdateMany: UpdateMany,
  GetAggregation: GetAggregation,
  PopulateDocument: PopulateDocument,
  RemoveDocument: RemoveDocument,
  GetCount: GetCount,
  GetAggregationDoc: GetAggregationDoc,
  GetOneDoc: GetOneDoc,
  GetDocs: GetDocs,
  UpdateDoc: UpdateDoc,
  InsertDoc: InsertDoc,
  InsertMultiple: InsertMultiple
};
