module.exports = function(io) {
  let path = require("path");
  let db = require("../model/mongodb.js");
  let apn = require("apn");
  let gcm = require("node-gcm");

  return {
    sendPushnotification: function(regIds, message, action, type, urlval, app, callback) {
      let send_message = {};
      let data = {};
      send_message["message"] = message || "";
      send_message["action"] = action;

      if (urlval instanceof Array) {
        for (let i = 0; i < urlval.length; i++) {
          send_message["key" + i] = urlval[i];
          data["key" + i] = urlval[i];
        }
      } else {
        Object.keys(urlval).forEach(function(key, i) {
          send_message["key" + i] = urlval[key];
          data["key" + i] = urlval[key];
        });
      }

      let collection = "";

      if (app == "EMPLOYEE") {
        collection = "employees";
        type = "employee";
      } else if (app == "AGENCY") {
        collection = "agencies";
        type = "agency";
      } else if (app == "CLIENT") {
        collection = "clients";
        type = "client";
      }

      db.GetDocument("settings", {alias: "mobile"}, {}, {}, function(err, docdata) {
        if (docdata) {
          if (docdata[0].settings.apns.mode == 1) {
            docdata[0].settings.apns.mode = true;
          } else {
            docdata[0].settings.apns.mode = false;
          }
          db.GetDocument(collection, {_id: regIds}, {}, {}, function(pushErr, pushRespo) {
            if (!pushErr && pushRespo[0]) {
              let notification = {};
              if (type == "employee") {
                notification.employee = regIds;
              } else if (type == "agency") {
                notification.agency = regIds;
              } else if (type == "client") {
                notification.client = regIds;
              }

              notification.action = action;

              notification.type = type;
              notification.message = send_message.message;
              notification.raw_data = send_message;

              db.InsertDocument("notifications", notification, function(err, upNotify) {
                if (!err) {
                  let username = pushRespo[0]._id;
                  if (pushRespo[0].device_info) {
                    if (pushRespo[0].device_info.device_token) {
                      if (pushRespo[0].device_info.ios_notification_mode == "apns") {
                        let options = {};
                        let topic = "";
                        if (collection == "users") {
                          options = {cert: docdata[0].settings.apns.user_pem, key: docdata[0].settings.apns.user_pem, production: docdata[0].settings.apns.mode};
                          topic = docdata[0].settings.apns.user_bundle_id;
                        } else if (collection == "tasker") {
                          options = {cert: docdata[0].settings.apns.tasker_pem, key: docdata[0].settings.apns.tasker_pem, production: docdata[0].settings.apns.mode};
                          topic = docdata[0].settings.apns.tasker_bundle_id;
                        }

                        let apnProvider = new apn.Provider(options);
                        let deviceToken = pushRespo[0].device_info.device_token;
                        let note = new apn.Notification();
                        note.expiry = Math.floor(Date.now() / 1000) + 3600;
                        note.sound = "ping.aiff";
                        note.alert = send_message.message;
                        note.payload = send_message;
                        note.topic = topic;
                        note.threadId = docdata._id;
                        apnProvider.send(note, deviceToken).then((result) => {});
                      } else {
                        io.of("/notify")
                            .in(regIds)
                            .emit("notification", {username: username, message: send_message});
                      }
                    }

                    if (pushRespo[0].device_info.gcm) {
                      if (pushRespo[0].device_info.android_notification_mode == "gcm") {
                        let gcmid = {};
                        if (collection == "users") {
                          gcmid = docdata[0].settings.gcm.user;
                        } else if (collection == "tasker") {
                          gcmid = docdata[0].settings.gcm.tasker;
                        }
                        data.title = send_message.message;
                        let message = new gcm.Message({data: {data: data}});
                        let sender = new gcm.Sender(gcmid);
                        let regTokens = [pushRespo[0].device_info.gcm];
                        sender.send(message, {registrationTokens: regTokens}, function(err, response) {});
                      } else {
                        io.of("/notify")
                            .in(regIds)
                            .emit("notification", {username: username, message: send_message});
                      }
                    }
                    io.of("/notify")
                        .in(regIds)
                        .emit("web notification", {username: username, message: send_message});
                  }
                  callback("err", "httpResponse", send_message);
                }
              });
            }
          });
        }
      });
    },

    addnotification: (regIds, message, action, urlval, app, callback) => {
      //   console.log("TCL: regIds, message, action, urlval, app", regIds, message, action, urlval, app);

      let send_message = {};
      let notification = {};

      send_message["message"] = message || "";
      send_message["action"] = action;

      if (urlval instanceof Array) {
        for (let i = 0; i < urlval.length; i++) {
          send_message["key" + i] = urlval[i];
        }
      } else {
        Object.keys(urlval).forEach((key, i) => {
          send_message["key" + i] = urlval[key];
          send_message[key] = urlval[key];
          notification[key] = urlval[key];
        });
      }
      let type;
      if (app == "EMPLOYEE") {
        type = "employee";
      } else if (app == "AGENCY") {
        type = "agency";
      } else if (app == "CLIENT") {
        type = "client";
      } else if (app == "ADMIN") {
        type = "admin";
      }

      if (regIds && regIds != "") {
        if (type == "employee") {
          notification.employee = regIds;
        } else if (type == "agency") {
          notification.agency = regIds;
        } else if (type == "client") {
          notification.client = regIds;
        } else if (type == "admin") {
          notification.admin = regIds;
        }
      }

      notification.action = action;
      notification.type = type;
      notification.message = message;
      notification.raw_data = send_message;

      db.InsertDocument("notifications", notification, (err, upNotify) => {
        if (!err) {
          callback("err", "httpResponse", send_message);
          io.of("/notify").emit("web_notification", {type: type, message: message});
        }
      });
    }
  };
};
