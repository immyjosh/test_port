const CONFIG = require("../config/config");
const async = require("async");
const mail = require("../model/mail.js");
const db = require("../model/mongodb.js");
const mongoose = require("mongoose");

function sendmail(data, callback) {
  async.waterfall(
    [
      function(callback) {
        db.GetOneDocument("settings", { alias: "general" }, {}, {}, function(err, settings) {
          if (err || !settings) {
            callback("Configure your website settings");
          } else {
            callback(err, settings.settings);
          }
        });
      },
      function(settings, callback) {
        db.GetOneDocument("emailtemplate", { slug: data.template, status: { $ne: 0 } }, {}, {}, function(err, template) {
          if (err || !template) {
            callback("Unable to get email template", settings);
          } else {
            callback(err, settings, template);
          }
        });
      }
    ],
    function(err, settings, template) {
      if (err) {
        callback(err);
      } else {
        let html = template.email_content;
        html = html.replace(/{{privacy}}/g, settings.site_url + "page/privacypolicy");
        html = html.replace(/{{terms}}/g, settings.site_url + "page/termsandconditions");
        html = html.replace(/{{contactus}}/g, settings.site_url + "contact_us");
        html = html.replace(/{{senderemail}}/g, template.sender_email);
        html = html.replace(/{{sendername}}/g, template.sender_name);
        html = html.replace(/{{logo}}/g, settings.site_url + settings.logo);
        html = html.replace(/{{site_title}}/g, settings.site_title);
        html = html.replace(/{{email_title}}/g, settings.site_title);
        html = html.replace(/{{email_address}}/g, settings.email_address);
        html = html.replace(/{{site_url}}/g, settings.site_url);

        for (i = 0; i < data.html.length; i++) {
          const regExp = new RegExp("{{" + data.html[i].name + "}}", "g");
          html = html.replace(regExp, data.html[i].value);
        }
        const tomail = data.to ? data.to : template.sender_email;

        const mailOptions = {
          // from: template.sender_email,
          from: template.sender_name + " <" + template.sender_email + ">",
          to: tomail,
          subject: template.email_subject,
          text: template.email_subject,
          html: html
        };

        mail.send(mailOptions, function(err, response) {
          const data = {};
          data.mailOptions = mailOptions;
          data.response = response;
          callback(err, data);
        });
      }
    }
  );
}
function sendcustommail(data, callback) {
  async.waterfall(
    [
      function(callback) {
        db.GetOneDocument("settings", { alias: "general" }, {}, {}, function(err, settings) {
          if (err || !settings) {
            callback("Configure your website settings");
          } else {
            callback(err, settings.settings);
          }
        });
      },
      function(settings, callback) {
        db.GetOneDocument("agencyemailtemplate", { type: data.template, agency: new mongoose.Types.ObjectId(data.agency), default_mail: 1 }, {}, {}, function(err, template) {
          if (err || !template) {
            callback("Unable to get email template", settings);
          } else {
            callback(err, settings, template);
          }
        });
      },
      function(settings, template, callback) {
        db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(data.agency) }, {}, {}, function(err, agency) {
          if (err || !agency) {
            callback("Unable to get agency details", agency);
          } else {
            callback(err, settings, template, agency);
          }
        });
      }
    ],
    function(err, settings, template, agency) {
      if (err) {
        callback(err);
      } else {
        let html = template.email_content;
        html = html.replace(/{{privacy}}/g, settings.site_url + "page/privacypolicy");
        html = html.replace(/{{terms}}/g, settings.site_url + "page/termsandconditions");
        html = html.replace(/{{contactus}}/g, settings.site_url + "contact_us");
        html = html.replace(/{{senderemail}}/g, agency.company_email || template.sender_email);
        html = html.replace(/{{sendername}}/g, template.sender_name);
        html = html.replace(/{{logo}}/g, settings.site_url + (agency.company_logo ||settings.logo));
        html = html.replace(/{{site_title}}/g, settings.site_title);
        html = html.replace(/{{email_title}}/g, settings.site_title);
        html = html.replace(/{{email_address}}/g, settings.email_address);
        html = html.replace(/{{site_url}}/g, settings.site_url);

        for (i = 0; i < data.html.length; i++) {
          const regExp = new RegExp("{{" + data.html[i].name + "}}", "g");
          html = html.replace(regExp, data.html[i].value);
        }
        const tomail = data.to ? data.to : template.sender_email;

        const mailOptions = {
          // from: template.sender_email,
          from: template.sender_name + " <" + (agency.company_email || template.sender_email) + ">",
          to: tomail,
          subject: template.email_subject,
          text: template.email_subject,
          html: html
        };

        mail.send(mailOptions, function(err, response) {
          const data = {};
          data.mailOptions = mailOptions;
          data.response = response;
          callback(err, data);
        });
      }
    }
  );
}
const getmaildata = data => {
  // console.log('data ', data)
  return new Promise((resolve, reject) => {
    async.waterfall(
      [
        function(callback) {
          db.GetOneDocument("settings", { alias: "general" }, {}, {}, function(err, settings) {
            if (err || !settings) {
              callback("Configure your website settings");
            } else {
              callback(err, settings.settings);
            }
          });
        },
        function(settings, callback) {
          db.GetOneDocument("agencyemailtemplate", { _id: new mongoose.Types.ObjectId(data.template) }, {}, {}, function(err, template) {
            if (err || !template) {
              callback("Unable to get email template", settings);
            } else {
              callback(err, settings, template);
            }
          });
        },
        function(settings, template, callback) {
          db.GetOneDocument("agencies", { _id: new mongoose.Types.ObjectId(template.agency) }, {}, {}, function(err, agency) {
            if (err || !agency) {
              callback("Unable to get agency details", agency);
            } else {
              callback(err, settings, template, agency);
            }
          });
        }
      ],
      function(err, settings, template, agency) {
        if (err) {
          reject(err);
        }
        let html = template.email_content;
        html = html.replace(/{{privacy}}/g, settings.site_url + "page/privacypolicy");
        html = html.replace(/{{terms}}/g, settings.site_url + "page/termsandconditions");
        html = html.replace(/{{contactus}}/g, settings.site_url + "contact_us");
        html = html.replace(/{{senderemail}}/g, agency.company_email || template.sender_email);
        html = html.replace(/{{sendername}}/g, template.sender_name);
        html = html.replace(/{{logo}}/g, settings.site_url + (agency.company_logo || settings.logo));
        html = html.replace(/{{site_title}}/g, settings.site_title);
        html = html.replace(/{{email_title}}/g, settings.site_title);
        html = html.replace(/{{email_address}}/g, settings.email_address);
        html = html.replace(/{{site_url}}/g, settings.site_url);
        for (i = 0; i < data.html.length; i++) {
          const regExp = new RegExp("{{" + data.html[i].name + "}}", "g");
          html = html.replace(regExp, data.html[i].value);
        }
        const tomail = data.to ? data.to : template.sender_email;
        const datas = {
          from: template.sender_name + " <" + (agency.company_email || template.sender_email) + ">",
          to: tomail,
          subject: template.email_subject,
          text: template.email_subject,
          html: html
        };
        // callback(err, data);
        resolve(datas);
      }
    );
  });
};

module.exports = {
  sendmail: sendmail,
  sendcustommail: sendcustommail,
  getmaildata: getmaildata
};
