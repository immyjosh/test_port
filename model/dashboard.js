const CONFIG = require("../config/config");
const async = require("async");
const moment = require("moment");
const path = require("path");
const mongoose = require("mongoose");
const db = require(path.resolve(__dirname, "./mongodb.js"));

// var db = require('mongodb.js');

function admindashboardCount(condition, callback) {
  const data = {};
  async.parallel(
    {
      subadminCount: function(callback) {
        db.GetCount("administrators", {}, function(err, count) {
          callback(err, count);
        });
      },
      agencyCount: function(callback) {
        db.GetCount("agencies", {}, function(err, count) {
          callback(err, count);
        });
      },
      agencyRegisteredCount: function(callback) {
        db.GetCount("agencies", { isverified: 0 }, function(err, count) {
          callback(err, count);
        });
      },
      clientCount: function(callback) {
        db.GetCount("clients", {}, function(err, count) {
          callback(err, count);
        });
      },
      employeeCount: function(callback) {
        db.GetCount("employees", {}, function(err, count) {
          callback(err, count);
        });
      }
    },
    function(err, result) {
      if (err || !result) {
        callback(err, {});
      } else {
        data.result = [];
        data.result.push({
          agencies: result.agencyCount,
          clients: result.clientCount,
          employees: result.employeeCount,
          subadmin: result.subadminCount,
          newagencies: result.agencyRegisteredCount
        });

        /* data.response.push({ 'agencies': result.agencyCount });
            data.response.push({ 'clients': result.clientCount });
            data.response.push({ 'employees': result.employeeCount });
            data.response.push({ 'subadmin': result.subadminCount });*/
        callback(err, data);
      }
    }
  );
}

const agencydashboardCount = (condition, callback) => {
  const data = {};
  async.parallel(
    {
      clientCount: callback => {
        db.GetCount("clients", { agency: condition.agency }, (err, count) => callback(err, count));
      },
      employeeCount: callback => {
        db.GetCount("employees", { agency: condition.agency }, (err, count) => callback(err, count));
      },
      activeclientCount: callback => {
        db.GetCount("clients", { agency: condition.agency, status: 1 }, (err, count) => callback(err, count));
      },
      inactiveclientCount: callback => {
        db.GetCount("clients", { agency: condition.agency, status: 0 }, (err, count) => callback(err, count));
      },
      activeemployeeCount: callback => {
        db.GetCount("employees", { agency: condition.agency, status: 1 }, (err, count) => callback(err, count));
      },
      inactiveemployeeCount: callback => {
        db.GetCount("employees", { agency: condition.agency, status: 0 }, (err, count) => callback(err, count));
      },
      timesheetpendingCount: callback => {
        db.GetCount("shifts", { agency: condition.agency, timesheet_status: 1 }, (err, count) => callback(err, count));
      },
      timesheetapprovedCount: callback => {
        db.GetCount("shifts", { agency: condition.agency, timesheet_status: 2 }, (err, count) => callback(err, count));
      },
      totalhours: callback => {
        const finalQuery = [{ $match: { agency: { $eq: condition.agency } } }];
        if (condition.populate.start && condition.populate.end) {
          const from = condition.populate.start;
          const to = condition.populate.end;
          finalQuery.push({
            $match: {
              $and: [{ start_date: { $gte: new Date(from) } }, { start_date: { $lte: new Date(to) } }]
            }
          });
        }
        finalQuery.push(
          { $match: { status: { $eq: 5 } } },
          { $project: { starttime: 1, endtime: 1 } },
          { $addFields: { total: { $subtract: ["$starttime", "$endtime"] } } },
          { $project: { _id: 0, total: 1 } },
          { $group: { _id: "null", hours: { $sum: "$total" } } },
          { $project: { _id: 0, hours: 1 } }
        );
        db.GetAggregation("stats", finalQuery, (err, count) => callback(err, count));
      },
      shiftsCount: callback => {
        const finalQuery = [{ $match: { agency: { $eq: condition.agency } } }];
        if (condition.populate.selectshift !== "all") {
          const calc = moment(new Date()).subtract(condition.populate.selectshift, "days");
          const from = calc.toISOString();
          const to = new Date().toISOString();
          finalQuery.push({
            $match: {
              $and: [{ createdAt: { $gte: new Date(from) } }, { createdAt: { $lte: new Date(to) } }]
            }
          });
        }
        finalQuery.push(
          { $group: { _id: "$status", count: { $sum: 1 } } },
          {
            $addFields: {
              status: {
                $arrayElemAt: [["added", "added", "requests", "accepted", "assigned", "ongoing", "completed", "timehseet_approved", "invoice_approved", "Payment_completed", "expired"], "$_id"]
              }
            }
          },
          { $addFields: { nott: [["$status", "$count"]] } },
          { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
          { $group: { _id: null, res: { $push: "$$ROOT" } } },
          { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
        );
        db.GetAggregation("stats", finalQuery, (err, count) => callback(err, count));
      },
      invoiceCount: callback => {
        const finalQuery = [{ $match: { agency: { $eq: condition.agency } } }];
        if (condition.populate.selectinvoice !== "all") {
          const calc = moment(new Date()).subtract(condition.populate.selectinvoice, "days");
          const from = calc.toISOString();
          const to = new Date().toISOString();
          finalQuery.push({ $match: { $and: [{ createdAt: { $gte: new Date(from) } }, { createdAt: { $lte: new Date(to) } }] } });
        }
        finalQuery.push(
          { $group: { _id: "$status", client_rate: { $sum: "$client_rate" }, employee_rate: { $sum: "$employee_rate" }, count: { $sum: 1 } } },
          {
            $addFields: {
              status: {
                $arrayElemAt: [["start", "draft", "approved", "part_paid", "paid"], "$_id"]
              },
              rate: {
                $arrayElemAt: [["start_rate", "draft_rate", "approved_rate", "part_paid_rate", "paid_rate"], "$_id"]
              }
            }
          },
          { $addFields: { nott: [["$status", "$count"], ["$rate", "$client_rate"]] } },
          { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
          { $group: { _id: null, res: { $push: "$$ROOT" } } },
          { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
        );
        db.GetAggregation("invoice", finalQuery, (err, count) => callback(err, count));
      }
    },
    (err, result) => {
      if (err || !result) {
        callback(err, {});
      } else {
        data.result = [];
        data.result.push({
          clients: result.clientCount,
          employees: result.employeeCount,
          activeclient: result.activeclientCount,
          inactiveclient: result.inactiveclientCount,
          activeemployee: result.activeemployeeCount,
          inactiveemployee: result.inactiveemployeeCount,
          timesheetpending: result.timesheetpendingCount,
          timesheetapproved: result.timesheetapprovedCount,
          shifts: result.shiftsCount,
          invoices: result.invoiceCount,
          totalhours: result.totalhours && result.totalhours[0]
        });
        /* data.response.push({ 'agencies': result.agencyCount });
            data.response.push({ 'clients': result.clientCount });
            data.response.push({ 'employees': result.employeeCount });
            data.response.push({ 'subadmin': result.subadminCount });*/
        callback(err, data);
      }
    }
  );
};

const clientdashboardCount = (condition, callback) => {
  const data = {};
  async.parallel(
    {
      employeeCount: callback => {
        db.GetCount("employees", { client: condition.client }, (err, count) => callback(err, count));
      },
      activeemployeeCount: callback => {
        db.GetCount("employees", { client: condition.client, status: 1 }, (err, count) => callback(err, count));
      },
      inactiveemployeeCount: callback => {
        db.GetCount("employees", { client: condition.client, status: 0 }, (err, count) => callback(err, count));
      },
      timesheetpendingCount: callback => {
        db.GetCount("shifts", { client: condition.client, timesheet_status: 1 }, (err, count) => callback(err, count));
      },
      timesheetapprovedCount: callback => {
        db.GetCount("shifts", { client: condition.client, timesheet_status: 2 }, (err, count) => callback(err, count));
      },
      totalhours: callback => {
        const finalQuery = [{ $match: { client: { $eq: condition.client } } }];
        if (condition.populate.start && condition.populate.end) {
          const from = condition.populate.start;
          const to = condition.populate.end;
          finalQuery.push({
            $match: {
              $and: [{ start_date: { $gte: new Date(from) } }, { start_date: { $lte: new Date(to) } }]
            }
          });
        }
        finalQuery.push(
          { $match: { status: { $eq: 5 } } },
          { $project: { starttime: 1, endtime: 1 } },
          { $addFields: { total: { $subtract: ["$starttime", "$endtime"] } } },
          { $project: { _id: 0, total: 1 } },
          { $group: { _id: "null", hours: { $sum: "$total" } } },
          { $project: { _id: 0, hours: 1 } }
        );
        db.GetAggregation("stats", finalQuery, (err, count) => callback(err, count));
      },
      shiftsCount: callback => {
        const finalQuery = [{ $match: { client: { $eq: condition.client } } }];
        if (condition.populate.selectshift !== "all") {
          const calc = moment(new Date()).subtract(condition.populate.selectshift, "days");
          const from = calc.toISOString();
          const to = new Date().toISOString();
          finalQuery.push({
            $match: {
              $and: [{ createdAt: { $gte: new Date(from) } }, { createdAt: { $lte: new Date(to) } }]
            }
          });
        }
        finalQuery.push(
          { $group: { _id: "$status", count: { $sum: 1 } } },
          {
            $addFields: {
              status: {
                $arrayElemAt: [["added", "added", "requests", "accepted", "assigned", "ongoing", "completed", "timehseet_approved", "invoice_approved", "Payment_completed", "expired"], "$_id"]
              }
            }
          },
          { $addFields: { nott: [["$status", "$count"]] } },
          { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
          { $group: { _id: null, res: { $push: "$$ROOT" } } },
          { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
        );
        db.GetAggregation("stats", finalQuery, (err, count) => callback(err, count));
      },
      invoiceCount: callback => {
        const finalQuery = [{ $match: { client: { $eq: condition.client } } }];
        if (condition.populate.selectinvoice !== "all") {
          const calc = moment(new Date()).subtract(condition.populate.selectinvoice, "days");
          const from = calc.toISOString();
          const to = new Date().toISOString();
          finalQuery.push({ $match: { $and: [{ createdAt: { $gte: new Date(from) } }, { createdAt: { $lte: new Date(to) } }] } });
        }
        finalQuery.push(
          { $group: { _id: "$status", client_rate: { $sum: "$client_rate" }, employee_rate: { $sum: "$employee_rate" }, count: { $sum: 1 } } },
          {
            $addFields: {
              status: {
                $arrayElemAt: [["start", "draft", "approved", "part_paid", "paid"], "$_id"]
              },
              rate: {
                $arrayElemAt: [["start_rate", "draft_rate", "approved_rate", "part_paid_rate", "paid_rate"], "$_id"]
              }
            }
          },
          { $addFields: { nott: [["$status", "$count"], ["$rate", "$client_rate"]] } },
          { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
          { $group: { _id: null, res: { $push: "$$ROOT" } } },
          { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
        );
        db.GetAggregation("invoice", finalQuery, (err, count) => callback(err, count));
      },
      earnings: callback => {
        const finalQuery = [{ $match: { client: { $eq: condition.client } } }, { $match: { status: { $eq: 7 } } }];

        db.GetAggregation("shifts", finalQuery, (err, docData) => callback(err, docData));
      }
    },
    (err, result) => {
      if (err || !result) {
        callback(err, {});
      } else {
        data.result = [];

        const newresult = {
          employees: result.employeeCount,
          active: result.activeemployeeCount,
          inactive: result.inactiveemployeeCount,
          timesheetpending: result.timesheetpendingCount,
          timesheetapproved: result.timesheetapprovedCount,
          shifts: result.shiftsCount,
          invoices: result.invoiceCount,
          totalhours: result.totalhours && result.totalhours[0]
        };

        if (result.earnings[0] && result.earnings[0].client_rate) {
          newresult.clientearnings = result.earnings[0].client_rate;
        }

        if (result.earnings[0] && result.earnings[0].agency_rate) {
          newresult.agencyearnings = result.earnings[0].agency_rate;
        }

        if (result.earnings[0] && result.earnings[0].employee_rate) {
          newresult.employeeearnings = result.earnings[0].employee_rate;
        }

        data.result.push(newresult);

        /* data.response.push({ 'agencies': result.agencyCount });
            data.response.push({ 'clients': result.clientCount });
            data.response.push({ 'employees': result.employeeCount });
            data.response.push({ 'subadmin': result.subadminCount });*/
        callback(err, data);
      }
    }
  );
};
const employeedashboardCount = (condition, callback) => {
  const data = {};
  async.parallel(
    {
      timesheetpendingCount: callback => {
        db.GetCount("shifts", { employee: condition.employee, timesheet_status: 1 }, (err, count) => callback(err, count));
      },
      timesheetapprovedCount: function(callback) {
        db.GetCount("shifts", { employee: condition.employee, timesheet_status: 2 }, (err, count) => callback(err, count));
      },
      totalshiftCount: callback => {
        db.GetCount("shifts", { employee: condition.employee }, (err, count) => callback(err, count));
      },
      shiftsCount: callback => {
        const finalQuery = [{ $match: { employee: { $eq: condition.employee } } }];
        if (condition.populate.selectshift !== "all") {
          const calc = moment(new Date()).subtract(condition.populate.selectshift, "days");
          const from = calc.toISOString();
          const to = new Date().toISOString();
          finalQuery.push({
            $match: {
              $and: [{ createdAt: { $gte: new Date(from) } }, { createdAt: { $lte: new Date(to) } }]
            }
          });
        }
        finalQuery.push(
          { $group: { _id: "$status", count: { $sum: 1 } } },
          {
            $addFields: {
              status: {
                $arrayElemAt: [["added", "added", "requests", "accepted", "assigned", "ongoing", "completed", "timehseet_approved", "invoice_approved", "Payment_completed", "expired"], "$_id"]
              }
            }
          },
          { $addFields: { nott: [["$status", "$count"]] } },
          { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
          { $group: { _id: null, res: { $push: "$$ROOT" } } },
          { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
        );
        db.GetAggregation("stats", finalQuery, (err, count) => callback(err, count));
      },
      invoiceCount: callback => {
        const finalQuery = [{ $match: { employee: { $eq: condition.employee } } }];
        if (condition.populate.selectinvoice !== "all") {
          const calc = moment(new Date()).subtract(condition.populate.selectinvoice, "days");
          const from = calc.toISOString();
          const to = new Date().toISOString();
          finalQuery.push({ $match: { $and: [{ createdAt: { $gte: new Date(from) } }, { createdAt: { $lte: new Date(to) } }] } });
        }
        finalQuery.push(
          { $group: { _id: "$status", client_rate: { $sum: "$client_rate" }, employee_rate: { $sum: "$employee_rate" }, count: { $sum: 1 } } },
          {
            $addFields: {
              status: {
                $arrayElemAt: [["start", "draft", "approved", "part_paid", "paid"], "$_id"]
              },
              rate: {
                $arrayElemAt: [["start_rate", "draft_rate", "approved_rate", "part_paid_rate", "paid_rate"], "$_id"]
              }
            }
          },
          { $addFields: { nott: [["$status", "$count"], ["$rate", "$employee_rate"]] } },
          { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
          { $group: { _id: null, res: { $push: "$$ROOT" } } },
          { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
        );
        db.GetAggregation("invoice", finalQuery, (err, count) => callback(err, count));
      },
      totalhours: callback => {
        const finalQuery = [{ $match: { employee: { $eq: condition.employee } } }];
        if (condition.populate.start && condition.populate.end) {
          const from = condition.populate.start;
          const to = condition.populate.end;
          finalQuery.push({
            $match: {
              $and: [{ start_date: { $gte: new Date(from) } }, { start_date: { $lte: new Date(to) } }]
            }
          });
        }
        finalQuery.push(
          { $match: { status: { $eq: 5 } } },
          { $project: { starttime: 1, endtime: 1 } },
          { $addFields: { total: { $subtract: ["$starttime", "$endtime"] } } },
          { $project: { _id: 0, total: 1 } },
          { $group: { _id: "null", hours: { $sum: "$total" } } },
          { $project: { _id: 0, hours: 1 } }
        );
        db.GetAggregation("stats", finalQuery, (err, count) => callback(err, count));
      }
    },
    (err, result) => {
      if (err || !result) {
        callback(err, {});
      } else {
        data.result = [];
        data.result.push({
          clients: result.clientCount,
          employees: result.employeeCount,
          activeclient: result.activeclientCount,
          inactiveclient: result.inactiveclientCount,
          activeemployee: result.activeemployeeCount,
          inactiveemployee: result.inactiveemployeeCount,
          timesheetpending: result.timesheetpendingCount,
          timesheetapproved: result.timesheetapprovedCount,
          shifts: result.shiftsCount,
          invoices: result.invoiceCount,
          totalshiftCount: result.totalshiftCount,
          totalhours: result.totalhours && result.totalhours[0]
        });
        /* data.response.push({ 'agencies': result.agencyCount });
              data.response.push({ 'clients': result.clientCount });
              data.response.push({ 'employees': result.employeeCount });
              data.response.push({ 'subadmin': result.subadminCount });*/
        callback(err, data);
      }
    }
  );
};
function dashboardCount(condition, callback) {
  const data = {};
  async.parallel(
    {
      subadminCount: function(callback) {
        db.GetCount("administrators", {}, function(err, count) {
          callback(err, count);
        });
      },
      agencyCount: function(callback) {
        db.GetCount("agencies", {}, function(err, count) {
          callback(err, count);
        });
      },
      agencyRegisteredCount: function(callback) {
        db.GetCount("agencies", { isverified: 0 }, function(err, count) {
          callback(err, count);
        });
      },
      clientCount: function(callback) {
        db.GetCount("clients", {}, function(err, count) {
          callback(err, count);
        });
      },
      employeeCount: function(callback) {
        db.GetCount("employees", {}, function(err, count) {
          callback(err, count);
        });
      }
    },
    function(err, result) {
      if (err || !result) {
        callback(err, {});
      } else {
        data.result = [];
        data.result.push({
          agencies: result.agencyCount,
          clients: result.clientCount,
          employees: result.employeeCount,
          subadmin: result.subadminCount,
          newagencies: result.agencyRegisteredCount
        });

        /* data.response.push({ 'agencies': result.agencyCount });
            data.response.push({ 'clients': result.clientCount });
            data.response.push({ 'employees': result.employeeCount });
            data.response.push({ 'subadmin': result.subadminCount });*/
        callback(err, data);
      }
    }
  );
}

const notification_status = (condition, callback) => {
  db.UpdateDocument("notifications", condition, { status: 2 }, { multi: true }, (err, result) => {
    callback("err", result);
  });
};

function notifications(condition, callback) {
  const data = {};
  const groupQuery = [
    { $match: condition },
    { $match: { status: 1 } },
    { $group: { _id: "$action", count: { $sum: 1 } } },
    {
      $addFields: { nott: [["$_id", "$count"]] }
    },
    { $replaceRoot: { newRoot: { $arrayToObject: "$nott" } } },
    { $group: { _id: null, res: { $push: "$$ROOT" } } },
    { $replaceRoot: { newRoot: { $mergeObjects: "$res" } } }
  ];

  db.GetAggregation("notifications", groupQuery, function(err, docData) {
    if (err || !docData) {
      callback(err, {});
    } else {
      data.status = 1;
      let overall = 0;
      if (docData[0]) {
        Object.keys(docData[0]).forEach(function(key, i) {
          overall = overall + docData[0][key];
        });
      }
      data.response = { Notifications: docData, overall: overall };

      callback(err, data);
    }
  });
}

module.exports = {
  dashboardCount: dashboardCount,
  admindashboardCount: admindashboardCount,
  agencydashboardCount: agencydashboardCount,
  clientdashboardCount: clientdashboardCount,
  employeedashboardCount: employeedashboardCount,
  notification_status: notification_status,
  notifications: notifications
};
