var mongoose = require("mongoose");
var bcrypt = require("bcrypt-nodejs");

/*---new db schema----*/
// var config_admin_schema = require("../schema/administrators.schema.js");
// var config_settings_schema = require("../schema/setting.schema.js");

const SUPER_ADMIN_SCHEMA = require("../schema/superadmin.schema");
const AGENCY_ADMIN_SCHEMA = require("../schema/agencyadmin.schema");
const CLIENT_ADMIN_SCHEMA = require("../schema/clientadmin.schema");
const EMPLOYEE_SCHEMA = require("../schema/employee.schema");
const SUBSCRIBTION_SCHEMA = require("../schema/subscribe.schema");
const SETTINGS_SCHEMA = require("../schema/settings.schema");
const SHIFTS_SCHEMA = require("../schema/shifts.schema");


// define the schema for our user model
// var adminSchema = mongoose.Schema(config_admin_schema.ADMIN, {
//   timestamps: true,
//   versionKey: false
// });
const super_admin_schema = mongoose.Schema(SUPER_ADMIN_SCHEMA, { timestamps: true, versionKey: false });
const agency_admin_schema = mongoose.Schema(AGENCY_ADMIN_SCHEMA, { timestamps: true, versionKey: false });
const client_admin_schema = mongoose.Schema(CLIENT_ADMIN_SCHEMA, { timestamps: true, versionKey: false });
const employee_schema = mongoose.Schema(EMPLOYEE_SCHEMA, { timestamps: true, versionKey: false });
const subscribtion_schema = mongoose.Schema(SUBSCRIBTION_SCHEMA, { timestamps: true, versionKey: false });
const settings_schema = mongoose.Schema(SETTINGS_SCHEMA, { timestamps: true, versionKey: false });
const shifts_schemas = mongoose.Schema(SHIFTS_SCHEMA, { timestamps: true, versionKey: false });

// adminSchema.methods.generateHash = function(password) {
//   return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };

// // checking if password is valid
// adminSchema.methods.validPassword = function(password) {
//   return bcrypt.compareSync(password, this.password);
// };

// create the model for users and expose it to our app
const administrators = mongoose.model("administrators", super_admin_schema, "administrators");

const superadminschemas = mongoose.model("superadminschemas", super_admin_schema, "superadminschemas");
const agencyadminschemas = mongoose.model("agencyadminschemas", agency_admin_schema, "agencyadminschemas");
const clientadminschemas = mongoose.model("clientadminschemas", client_admin_schema, "clientadminschemas");
const employeeschemas = mongoose.model("employeeschemas", employee_schema, "employeeschemas");
const subscribtionschemas = mongoose.model("subscribtionschemas", subscribtion_schema, "subscribtionschemas");
const settingsschemas = mongoose.model("settingsschemas", settings_schema, "settingsschemas");
const shiftsschemas = mongoose.model("shiftsschemas", shifts_schemas, "shiftsschemas");



module.exports = {
  administrators: administrators,
  superadminschemas: superadminschemas,
  agencyadminschemas: agencyadminschemas,
  clientadminschemas: clientadminschemas,
  employeeschemas: employeeschemas,
  subscribtionschemas: subscribtionschemas,
  settingsschemas: settingsschemas,
  shiftsschemas: shiftsschemas,

};
