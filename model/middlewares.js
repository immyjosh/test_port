const json2csv = require("json2csv");
const multer = require("multer");
const fs = require("fs");
const CONFIG = require("../config/config");

function jsontocsv(column_header, data, path, callback) {
  json2csv({ data: data, fields: column_header }, function(err, csv) {
    if (err);
    fs.writeFile(path, csv, function(err) {
      if (err) {
        callback(err);
      }
      callback("file saved");
    });
  });
}

function commonUpload(destinationPath) {
  const storage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, destinationPath);
    },
    filename: function(req, file, callback) {
      const uploadName = file.originalname.split(".");
      const extension = "." + uploadName[uploadName.length - 1];
      const fileName = Date.now().toString();
      fs.readFile(destinationPath + file.originalname, function(err, res) {
        if (!err) {
          callback(null, fileName + extension);
        } else {
          callback(null, fileName + extension);
        }
      });
    }
  });

  const uploaded = multer({ storage: storage }); /** ----{limits : {fieldNameSize : 100}}---*/
  return uploaded;
}

function customUpload(destinationPath) {
  const storage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, destinationPath);
    },
    filename: function(req, file, callback) {
      const uploadName = file.originalname.split(".");
      const extension = "." + uploadName[uploadName.length - 1];
      const fileName = file.fieldname;
      fs.readFile(destinationPath + file.originalname, function(err, res) {
        if (!err) {
          callback(null, fileName + extension);
        } else {
          callback(null, fileName + extension);
        }
      });
    }
  });

  const uploaded = multer({ storage: storage }); /** ----{limits : {fieldNameSize : 100}}---*/
  return uploaded;
}

module.exports = {
  jsontocsv: jsontocsv,
  commonUpload: commonUpload,
  customUpload: customUpload
};
