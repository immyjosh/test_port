"use strict";

const nodemailer = require('nodemailer');
const db = require("../model/mongodb");

function send(data, callback) {
    db.GetOneDocument('settings', { 'alias': 'smtp' }, {}, {}, function(err, settings) {
        if (err || !settings) {
            data.response = 'Error in settings';
            callback(data);
        } else {
            const smtp_host = settings.settings.smtp_host;
            const smtp_port = settings.settings.smtp_port;
            const smtp_username = settings.settings.smtp_username;
            const smtp_password = settings.settings.smtp_password;

            // console.log("smtp_host",smtp_host);
            // console.log("smtp_port",smtp_port);
            // console.log("smtp_username",smtp_username);
            // console.log("smtp_password",smtp_password);

            const transporter = nodemailer.createTransport({
                host: smtp_host,
                port: smtp_port,
                secure: true,
                auth: {
                    user: smtp_username,
                    pass: smtp_password
                },
                tls: {
                    rejectUnauthorized: false
                }
            });

            transporter.sendMail(data, function(error, info) {
                callback(error, info);
            });
        }
    });
}

module.exports = {
    "send": send
};
