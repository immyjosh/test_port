let http = require("http");
let https = require("https");
let CONFIG = require("../config/config");

function geocode(data, callback) {
  return https.get(
    {
      protocol: "https:",
      host: "maps.googleapis.com",
      path: "/maps/api/geocode/json?latlng=" + data.latitude + "," + data.longitude + "&sensor=false&key=" + CONFIG.GOOGLE_MAP_API_KEY
    },
    function(response) {
      let body = "";
      response.on("data", function(d) {
        body += d;
      });
      response.on("end", function() {
        let parsed = JSON.parse(body);
        callback(parsed.results);
      });
    }
  );
}

function directions(data, callback) {
  return http.get(
    {
      protocol: "http:",
      host: "maps.googleapis.com",
      path: "/maps/api/directions/json?origin=" + data.from + "&destination=" + data.to + "&alternatives=true&sensor=false&mode=driving"
    },
    function(response) {
      let body = "";
      response.on("data", function(d) {
        body += d;
      });
      response.on("end", function() {
        let parsed = JSON.parse(body);
        callback(parsed);
      });
    }
  );
}
function distancematrix(data, callback) {
  return https.get({
    protocol: 'https:',
    host: 'maps.googleapis.com',
    path: '/maps/api/distancematrix/json?units=imperial&origins='+data.lat1+','+data.lon1+'&destinations='+data.lat2+','+data.lon2+'&key='+CONFIG.GOOGLE_MAP_API_KEY,
    travelMode: 'driving'
  }, function(response) {
    let body = '';
    response.on('data', function(d) {
      body += d;
    });
    response.on('end', function() {
      let parsed = JSON.parse(body);
      callback(parsed);
    });
  });
}
module.exports = {
  geocode: geocode,
  directions: directions,
  distancematrix: distancematrix
};
