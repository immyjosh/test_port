import axios from "axios";
import jwt_decode from "jwt-decode";

const tokens = localStorage.getItem("APUS");
const tokensAgency = localStorage.getItem("APUSA");
const tokensClient = localStorage.getItem("APUSC");
let token, tokenAgency, tokenClient, AUTH_TOKEN;
if (tokens) {
  token = jwt_decode(tokens);
  if (window.location.pathname.slice(0, 14) === "/portal/admin/" && token.role === "admin") {
    AUTH_TOKEN = tokens;
  }
}
if (tokensAgency) {
  tokenAgency = jwt_decode(tokensAgency);
  if (window.location.pathname.slice(0, 14) === "/portal/agency" && tokenAgency.role === "agency") {
    AUTH_TOKEN = tokensAgency;
  }
}
if (tokensClient) {
  tokenClient = jwt_decode(tokensClient);
  if (window.location.pathname.slice(0, 14) === "/portal/client" && tokenClient.role === "client") {
    AUTH_TOKEN = tokensClient;
  }
}
export const NodeURL = "http://localhost:3006";
// export const NodeURL = "http://agency.casperon.co";
export const client = axios.create({
  baseURL: NodeURL
});
client.defaults.headers.common["Authorization"] = AUTH_TOKEN;
client.defaults.responseType = "json";
const request = options => {
  const onSuccess = response => {
    return response.data;
  };
  const onError = error => {
    if (error.response) {
      // Request was made but server responded with something other than 2xx
    } else {
      // Something else happened while setting up the request triggered the error
    }
    return Promise.reject(error.response || error.message);
  };
  return client(options)
    .then(onSuccess)
    .catch(onError);
};
export default request;
