import React, { Fragment } from "react";
import Context from "./context";
import jwt_decode from "jwt-decode";

class Gettoken extends React.Component {
  componentDidMount() {}

  tokenagain() {
    this.componentDidMount();
  }
  render() {
    const token = localStorage.getItem("APUS");
    const tokenAgency = localStorage.getItem("APUSA");
    const tokenClient = localStorage.getItem("APUSC");
    let decoded, decodedAgency, decodedClient;
    if (token) {
      decoded = jwt_decode(token);
    } else {
      decoded = "No Token Available";
    }
    if (tokenAgency) {
      decodedAgency = jwt_decode(tokenAgency);
    } else {
      decodedAgency = "No Token Available";
    }
    if (tokenClient) {
      decodedClient = jwt_decode(tokenClient);
    } else {
      decodedClient = "No Token Available";
    }
    return (
      <Fragment>
        <Context.Provider
          value={{
            Authtoken: decoded,
            AuthtokenAgency: decodedAgency,
            AuthtokenClient: decodedClient,
            Token: token,
            TokenAgency: tokenAgency,
            TokenClient: tokenClient
          }}
        >
          {this.props.children}
        </Context.Provider>
      </Fragment>
    );
  }
}
export default Gettoken;
