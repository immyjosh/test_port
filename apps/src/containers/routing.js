import React, { Component, Fragment } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Container } from "reactstrap";

import Header from "../components/Header/";
import Sidebar from "../components/Sidebar/";
import Breadcrumb from "../components/Breadcrumb/";
import Aside from "../components/Aside/";
import Footer from "../components/Footer/";

// Admin Routes import
import AddAgency from "../views/admin/agency/addagency";
import AgencyList from "../views/admin/agency/agencylist";
import EditAgency from "../views/admin/agency/editagency";
import ViewAgency from "../views/admin/agency/viewagency";
import AgencyTimesheet from "../views/admin/agency/timesheet/employeelist";
import AgencyTimesheetDetails from "../views/admin/agency/timesheet/timesheetdetails";
import AgencyInvoice from "../views/admin/agency/invoice/clientlist";
import AgencyInvoiceGroup from "../views/admin/agency/invoice/invoicegroupdetails";
import AgencyInvoiceDetails from "../views/admin/agency/invoice/invoicedetails";
import AddSubadmin from "../views/admin/subadmin/addsubadmin";
import SubadminList from "../views/admin/subadmin/subadminlist";
import EditSubadmin from "../views/admin/subadmin/editsubadmin";
import addemailtemplate from "../views/admin/emailtemplate/addemailtemplate";
import listemailtemplate from "../views/admin/emailtemplate/listemailtemplate";
import editemailtemplate from "../views/admin/emailtemplate/editemailtemplate";
import General from "../views/admin/settings/general";
import Smtp from "../views/admin/settings/smtp";
import Seo from "../views/admin/settings/seo";
import Mobile from "../views/admin/settings/mobile";
import Sms from "../views/admin/settings/sms";
import Paymentgetway from "../views/admin/settings/paymentgateway";
import Socialnetwork from "../views/admin/settings/socialnetwork";
import Addsubscription from "../views/admin/subscription/addsubscription";
import Editsubscription from "../views/admin/subscription/editsubscription";
import Subscriptionlist from "../views/admin/subscription/subscriptionlist";
import Subscriberslist from "../views/admin/subscription/subscriberslist";
import Earningslist from "../views/admin/earnings";
import Admindashboard from "../views/admin/admindashboard";
import AdminSubscribeAccount from '../views/admin/agency/subscribe/account'
import AdminSubscriptionPayment from '../views/admin/agency/subscribe/subscriptionpayment'
import AdminSubscribeEditAccount from "../views/admin/agency/subscribe/editaccount";
import AdminSubscribeInvoicelist from "../views/admin/agency/subscribe/invoicelist";
import AdminSubscribeSingleinvocie from "../views/admin/agency/subscribe/singleinvoice";

//Agency Routes import
import Addclient from "../views/agency/client/addclient";
import Clientlist from "../views/agency/client/clientlist";
import Editclient from "../views/agency/client/editclient";
import Viewclient from "../views/agency/client/viewclient";
import ClientTimesheet from "../views/agency/client/timesheet/employeelist";
import ClientTimesheetDetails from "../views/agency/client/timesheet/timesheetdetails";
import ClientInvoice from "../views/agency/client/invoice/clientlist";
import ClientInvoiceGroup from "../views/agency/client/invoice/invoicegroupdetails";
import SubscriptionPayment from "../views/agency/subscribe/subscriptionpayment";
import Addemployee from "../views/agency/employees/addemployee";
import Employeelist from "../views/agency/employees/employeelist";
import Editemployee from "../views/agency/employees/editemployee";
import Viewemployee from "../views/agency/employees/viewemployee";
import Staffprofileview from "../views/agency/employees/staffprofileview";
import EmployeeTimesheet from "../views/agency/employees/timesheets/timesheetlist";
import EmployeeTimesheetDetails from "../views/agency/employees/timesheets/timesheetdetails";
import EmployeeInvoice from "../views/agency/employees/invoice/clientlist";
import EmployeeInvoiceGroup from "../views/agency/employees/invoice/invoicegroup";
import Agencydashboard from "../views/agency/agencydashboard";
import ShiftList from "../views/agency/shift/shiftlist";
import Myshift from "../views/agency/shift/myshift";
import Allshifts from "../views/agency/shift/allshifts";
import Showemployees from "../views/agency/shift/showemployees";
import Requestmap from "../views/agency/shift/requestmap";
import RequestEmployees from "../views/agency/shift/requestemployees";
import Addjobtype from "../views/agency/jobtype/addjobtype";
import Editjobtype from "../views/agency/jobtype/editjobtype";
import EditjobtypeClient from "../views/agency/jobtype/editjobtypeclient";
import EditjobtypeEmployee from "../views/agency/employees/editjobtypeemployee";
import Listjobtype from "../views/agency/jobtype/listjobtype";
// import timesheet from "../views/agency/timesheet/timesheet";
import timesheetemployee from "../views/agency/timesheet/employeelist";
import timesheetdetails from "../views/agency/timesheet/timesheetdetails";
import Addworklocation from "../views/agency/worklocation/addworklocation";
import Editworklocation from "../views/agency/worklocation/editworklocation";
import Listworklocation from "../views/agency/worklocation/listworklocation";
import Adddaysoff from "../views/agency/daysoff/adddaysoff";
import Editdaysoff from "../views/agency/daysoff/editdaysoff";
import Listdaysoff from "../views/agency/daysoff/listdaysoff";
import Addshifttemplate from "../views/agency/shifttemplate/addshifttemplate";
import Editshifttemplate from "../views/agency/shifttemplate/editshifttemplate";
import Listshifttemplate from "../views/agency/shifttemplate/listshifttemplate";
import Subscribe from "../views/agency/subscribe/subscribe";
import SubscribeAccount from "../views/agency/subscribe/account";
import SubscribeEditAccount from "../views/agency/subscribe/editaccount";
import SubscriptionInvoices from "../views/agency/subscribe/invoicelist";
import SubscriptionInvoice from "../views/agency/subscribe/singleinvoice";
// import Invoice from "../views/agency/invoice/invoice";
import InvoiceList from "../views/agency/invoice/invoicedetails";
import invoiceClientAgency from "../views/agency/invoice/clientlist";
import invoicegrouptdetails from "../views/agency/invoice/invoicegroupdetails";
import EditAgencyprofile from "../views/agency/editprofile";
import Recruitment from "../views/agency/recruitment/candidatelist";
import Recruitment_online_form from "../views/agency/recruitment/onlineform";
import Recruitment_online_form_view from "../views/agency/recruitment/onlineformview";
import AgencyCompany from "../views/agency/settings/editcompany";
import Agencyaddemailtemplate from "../views/agency/emailtemplate/addemailtemplate";
import Agencylistemailtemplate from "../views/agency/emailtemplate/listemailtemplate";
import Agencyeditemailtemplate from "../views/agency/emailtemplate/editemailtemplate";

// client Routes import
import Clientdashboard from "../views/clients/clientdashboard";
import clientEditshift from "../views/clients/shift/myshift";
import ClientAllshifts from "../views/clients/shift/allshifts";
// import ClientInvoice from "../views/clients/invoice/invoice";
import ClientInvoiceList from "../views/clients/invoice/invoicedetails";
import invoiceClientlist from "../views/clients/invoice/clientlist";
import invoicegrouptdetailsClient from "../views/clients/invoice/invoicegroupdetails";
import ClientEditProfile from "../views/clients/editprofile";
import Addworkclientlocation from "../views/clients/worklocation/addworklocation";
import Editworkclientlocation from "../views/clients/worklocation/editworklocation";
import Listworkclientlocation from "../views/clients/worklocation/listworklocation";
import Addshiftclienttemplate from "../views/clients/shifttemplate/addshifttemplate";
import Editshiftclienttemplate from "../views/clients/shifttemplate/editshifttemplate";
import Listshiftclienttemplate from "../views/clients/shifttemplate/listshifttemplate";
// import clienttimesheet from "../views/clients/timesheet/timesheet";
import timesheetemployeeClient from "../views/clients/timesheet/employeelist";
import timesheetdetailsClient from "../views/clients/timesheet/timesheetdetails";
import clientShiftList from "../views/clients/shift/shiftlist";
import clientRequestEmployees from "../views/clients/shift/requestemployees";
// import ClientNotifications from "../views/clients/settings/notifications";
// import ClientGeneral from "../views/clients/settings/general";
import ClientViewRates from "../views/clients/settings/editjobtypeclient";
import ClientListJobType from "../views/clients/settings/listjobtypeclient";

// import Template from "./Template";

import Gettoken from "../api/gettoken";
import Context from "../api/context";
// import Page404 from "../views/Template/Pages/Page404";

class Routing extends Component {
  state = {
    TokenToComponent: "",
    Token: ""
  };
  componentDidMount() {
    window.scrollTo(0, 0);

    if (this.props.location.pathname.slice(0, 7) === "/admin/" && this.props.theme.Authtoken.role === "admin") {
      this.setState({
        TokenToComponent: this.props.theme.Authtoken,
        Token: this.props.theme.Token
      });
    } else if (this.props.location.pathname.slice(0, 8) === "/agency/" && this.props.theme.AuthtokenAgency.role === "agency") {
      this.setState({
        TokenToComponent: this.props.theme.AuthtokenAgency,
        Token: this.props.theme.TokenAgency
      });
    } else if (this.props.location.pathname.slice(0, 8) === "/client/" && this.props.theme.AuthtokenClient.role === "client") {
      this.setState({
        TokenToComponent: this.props.theme.AuthtokenClient,
        Token: this.props.theme.TokenClient
      });
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              {this.state.TokenToComponent.role === "admin" ? (
                <Switch>
                  <PrivateRoute path="/admin/dashboard" name="AdminDashboard" component={Admindashboard} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/addagency" name="AddAdmin" component={AddAgency} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/agencylist" name="AdminList" component={AgencyList} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/editagency" component={EditAgency} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/viewagency" component={ViewAgency} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/agency/timesheet" component={AgencyTimesheet} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/agency/viewtimesheet" component={AgencyTimesheetDetails} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/agency/invoice" component={AgencyInvoice} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/agency/invoicegroupdetails" component={AgencyInvoiceGroup} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/agency/viewinvoicedetails" component={AgencyInvoiceDetails} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/addsubadmin" name="AddAdmin" component={AddSubadmin} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/subadminlist" name="AdminList" component={SubadminList} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/editsubadmin" component={EditSubadmin} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/general" component={General} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/smtp" component={Smtp} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/seo" component={Seo} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/mobile" component={Mobile} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/sms" component={Sms} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/paymentgetway" component={Paymentgetway} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/settings/socialnetwork" component={Socialnetwork} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/addsubscription" component={Addsubscription} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/editsubscription" component={Editsubscription} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/subscriptionlist" component={Subscriptionlist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/subscriberslist" component={Subscriberslist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/earningslist" component={Earningslist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/addemailtemplate" component={addemailtemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/listemailtemplate" component={listemailtemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/admin/editemailtemplate" component={editemailtemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                   <PrivateRoute path="/admin/subscription/account" component={AdminSubscribeAccount} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                   <PrivateRoute path="/admin/subscriptionpayment" component={AdminSubscriptionPayment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                   <PrivateRoute path="/admin/subscription/editaccount" component={AdminSubscribeEditAccount} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                   <PrivateRoute path="/admin/subscription/invoicelist" component={AdminSubscribeInvoicelist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                   <PrivateRoute path="/admin/subscription/invoice" component={AdminSubscribeSingleinvocie} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                    {/* <Template /> */}
                    {/*<PrivateRoute component={Page404} />*/}
                </Switch>
              ) : null}
              {this.state.TokenToComponent.role === "agency" ? (
                <Switch>
                  {this.state.TokenToComponent.subscription === 0 ? (
                    <Fragment>
                      <PrivateRoute path="/agency/subscribe" component={Subscribe} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscription/account" component={SubscribeAccount} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscriptionpayment" component={SubscriptionPayment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editprofile" component={EditAgencyprofile} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                    </Fragment>
                  ) : null}
                  {this.state.TokenToComponent.subscription === 1 ? (
                    <Fragment>
                      <PrivateRoute path="/agency/dashboard" component={Agencydashboard} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/addclient" component={Addclient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/clientlist" component={Clientlist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editclient" component={Editclient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/viewclient" component={Viewclient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/client/timesheet" component={ClientTimesheet} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/client/timesheetdetails" component={ClientTimesheetDetails} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/client/invoice" component={ClientInvoice} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/client/invoicegroup" component={ClientInvoiceGroup} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/addemployee" component={Addemployee} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/employeelist" component={Employeelist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editemployee" component={Editemployee} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/viewemployee" component={Viewemployee} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/employee/staffprofile/view" component={Staffprofileview} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/employee/timesheet" component={EmployeeTimesheet} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/employee/timesheetdetails" component={EmployeeTimesheetDetails} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/employee/invoice" component={EmployeeInvoice} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/employee/invoicegroup" component={EmployeeInvoiceGroup} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editshift" component={Myshift} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/shifts" component={Allshifts} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/shiftlist" component={ShiftList} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/addjobtype" component={Addjobtype} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editjobtype" component={Editjobtype} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editjobtypeclient" component={EditjobtypeClient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editjobtypeemployee" component={EditjobtypeEmployee} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/listjobtype" component={Listjobtype} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/addworklocation" component={Addworklocation} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editworklocation" component={Editworklocation} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/listworklocation" component={Listworklocation} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/addshifttemplate" component={Addshifttemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editshifttemplate" component={Editshifttemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/listshifttemplate" component={Listshifttemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/showemployees" component={Showemployees} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/requestmap" component={Requestmap} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/requestemployees" component={RequestEmployees} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editprofile" component={EditAgencyprofile} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      {/* <PrivateRoute path="/agency/timesheet" component={timesheet} value={this.state.TokenToComponent} tokenid={this.state.Token} /> */}
                      <PrivateRoute path="/agency/timesheetemployee" component={timesheetemployee} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/timesheetdetails" component={timesheetdetails} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscribe" component={Subscribe} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscription/account" component={SubscribeAccount} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscription/editaccount" component={SubscribeEditAccount} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscription/invoicelist" component={SubscriptionInvoices} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscription/invoice" component={SubscriptionInvoice} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      {/* <PrivateRoute path="/agency/invoice" component={Invoice} value={this.state.TokenToComponent} tokenid={this.state.Token} /> */}
                      <PrivateRoute path="/agency/invoicedetails" component={InvoiceList} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/invoiceclientlist" component={invoiceClientAgency} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/invoicegroupdetails" component={invoicegrouptdetails} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/subscriptionpayment" component={SubscriptionPayment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/addpublicholidays" component={Adddaysoff} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editpublicholidays" component={Editdaysoff} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/listpublicholidays" component={Listdaysoff} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                      <PrivateRoute path="/agency/editcompany" component={AgencyCompany} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                        <PrivateRoute path="/agency/settings/email/add" component={Agencyaddemailtemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                        <PrivateRoute path="/agency/settings/email/list" component={Agencylistemailtemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                        <PrivateRoute path="/agency/settings/email/edit" component={Agencyeditemailtemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                        {/*<PrivateRoute component={Page404} />*/}
                        {this.state.TokenToComponent.recruitment_module === 1 ? (
                        <>
                          <PrivateRoute path="/agency/recruitment/candidates" component={Recruitment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/application" component={Recruitment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/interview" component={Recruitment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/files" component={Recruitment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/references" component={Recruitment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/summary" component={Recruitment} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitmentform/candidates" component={Recruitment_online_form} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitmentform/application" component={Recruitment_online_form} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitmentform/interview" component={Recruitment_online_form} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitmentform/files" component={Recruitment_online_form} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitmentform/references" component={Recruitment_online_form} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitmentform/summary" component={Recruitment_online_form} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/form/candidates/view" component={Recruitment_online_form_view} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/form/application/view" component={Recruitment_online_form_view} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/form/interview/view" component={Recruitment_online_form_view} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/form/files/view" component={Recruitment_online_form_view} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/form/references/view" component={Recruitment_online_form_view} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                          <PrivateRoute path="/agency/recruitment/form/summary/view" component={Recruitment_online_form_view} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                            {/**/}
                        </>
                      ) : null}
                    </Fragment>
                  ) : null}
                  {/* <Template /> */}
                </Switch>
              ) : null}
              {this.state.TokenToComponent.role === "client" ? (
                <Switch>
                  <PrivateRoute path="/client/shiftlist" component={clientShiftList} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/editprofile" component={ClientEditProfile} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/editshift" component={clientEditshift} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/shifts" component={ClientAllshifts} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/dashboard" component={Clientdashboard} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/myshift" component={Myshift} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/addworklocation" component={Addworkclientlocation} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  {/* <PrivateRoute path="/client/invoice" component={ClientInvoice} value={this.state.TokenToComponent} tokenid={this.state.Token} /> */}
                  <PrivateRoute path="/client/invoicedetails" component={ClientInvoiceList} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/invoiceclientlist" component={invoiceClientlist} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/invoicegroupdetails" component={invoicegrouptdetailsClient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/editworklocation" component={Editworkclientlocation} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/listworklocation" component={Listworkclientlocation} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/addshifttemplate" component={Addshiftclienttemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/editshifttemplate" component={Editshiftclienttemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/listshifttemplate" component={Listshiftclienttemplate} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  {/* <PrivateRoute path="/client/timesheet" component={clienttimesheet} value={this.state.TokenToComponent} tokenid={this.state.Token} /> */}
                  <PrivateRoute path="/client/timesheetemployee" component={timesheetemployeeClient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/timesheetdetails" component={timesheetdetailsClient} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/showemployees" component={Showemployees} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/requestemployees" component={clientRequestEmployees} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  {/* <PrivateRoute path="/client/notifications" component={ClientNotifications} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/general" component={ClientGeneral} value={this.state.TokenToComponent} tokenid={this.state.Token} /> */}
                  <PrivateRoute path="/client/jobrates" component={ClientListJobType} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                  <PrivateRoute path="/client/viewjobrates" component={ClientViewRates} value={this.state.TokenToComponent} tokenid={this.state.Token} />
                    {/*<PrivateRoute component={Page404} />*/}
                </Switch>
              ) : null}
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}
const PrivateRoute = ({ component: Component, value, tokenid, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const token = localStorage.getItem("APUS");
      const tokenAgency = localStorage.getItem("APUSA");
      const tokenClient = localStorage.getItem("APUSC");
      const decoded = !!(token || tokenAgency || tokenClient);
      /* if (token || tokenAgency || tokenClient) {
        decoded = true;
      } else {
        decoded = false;
      }*/
      if (decoded) {
        return <Component {...props} token_role={value} token={tokenid} />;
      } else {
        return <Redirect to={{ pathname: "/404", state: { from: props.location } }} />;
      }
    }}
  />
);
// export default Routing;
export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Routing {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
