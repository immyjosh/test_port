import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import {Container} from 'reactstrap';

import Dashboard from '../views/Template/Dashboard/';

import Colors from '../views/Template/Theme/Colors/';
import Typography from '../views/Template/Theme/Typography/';

import Charts from '../views/Template/Charts/';
import Widgets from '../views/Template/Widgets/';


// Base
import Cards from '../views/Template/Base/Cards/';
import Switches from '../views/Template/Base/Switches/';
import Tabs from '../views/Template/Base/Tabs/';
import Breadcrumbs from '../views/Template/Base/Breadcrumbs/';
import Carousels from '../views/Template/Base/Carousels/';
import Collapses from '../views/Template/Base/Collapses/';
import Dropdowns from '../views/Template/Base/Dropdowns/';
import Jumbotrons from '../views/Template/Base/Jumbotrons/';
import ListGroups from '../views/Template/Base/ListGroups/';
import Navbars from '../views/Template/Base/Navbars/';
import Navs from '../views/Template/Base/Navs/';
import Paginations from '../views/Template/Base/Paginations/';
import Popovers from '../views/Template/Base/Popovers/';
import ProgressBar from '../views/Template/Base/ProgressBar/';
import Tooltips from '../views/Template/Base/Tooltips/';

// Buttons
import Buttons from '../views/Template/Buttons/Buttons/';
import ButtonDropdowns from '../views/Template/Buttons/ButtonDropdowns/';
import ButtonGroups from '../views/Template/Buttons/ButtonGroups/';
import LoadingButtons from '../views/Template/Buttons/LoadingButtons/';
import SocialButtons from '../views/Template/Buttons/SocialButtons/';

// Editors
import TextEditors from '../views/Template/Editors/TextEditors';
import CodeEditors from '../views/Template/Editors/CodeEditors';

// Forms
import BasicForms from '../views/Template/Forms/BasicForms/';
import AdvancedForms from '../views/Template/Forms/AdvancedForms';

import GoogleMaps from '../views/Template/GoogleMaps/';

// Icons
import Flags from '../views/Template/Icons/Flags/';
import FontAwesome from '../views/Template/Icons/FontAwesome/';
import SimpleLineIcons from '../views/Template/Icons/SimpleLineIcons/';

// Notifications
import Alerts from '../views/Template/Notifications/Alerts/';
import Badges from '../views/Template/Notifications/Badges/';
import Modals from '../views/Template/Notifications/Modals/';
import Toastr from '../views/Template/Notifications/Toastr/';

// Plugins
import Calendar from '../views/Template/Plugins/Calendar/';
import Spinners from '../views/Template/Plugins/Spinners/';

// Tables
import DataTable from '../views/Template/Tables/DataTable/';
import Tables from '../views/Template/Tables/Tables/';

// UI Kits
import Invoice from '../views/Template/UI-Kits/Invoicing/';
import Inbox from '../views/Template/UI-Kits/Email/Inbox/';
import Message from '../views/Template/UI-Kits/Email/Message/';
import Compose from '../views/Template/UI-Kits/Email/Compose/';

class Template extends Component {  
  render() {
    return (
      <div className="app">
            <Container fluid>
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/theme/colors" name="Colors" component={Colors}/>
                <Route path="/theme/typography" name="Typography" component={Typography}/>
                <Route path="/base/cards" name="Cards" component={Cards}/>
                <Route path="/agency/base/switches" name="Swithces" component={Switches}/>
                <Route path="/base/tabs" name="Tabs" component={Tabs}/>
                <Route path="/base/breadcrumbs" name="Breadcrumbs" component={Breadcrumbs}/>
                <Route path="/base/carousels" name="Carousels" component={Carousels}/>
                <Route path="/base/collapses" name="Collapses" component={Collapses}/>
                <Route path="/base/dropdowns" name="Dropdowns" component={Dropdowns}/>
                <Route path="/base/jumbotrons" name="Jumbotrons" component={Jumbotrons}/>
                <Route path="/base/list-groups" name="ListGroups" component={ListGroups}/>
                <Route path="/base/navbars" name="Navbars" component={Navbars}/>
                <Route path="/base/navs" name="Navs" component={Navs}/>
                <Route path="/base/paginations" name="Paginations" component={Paginations}/>
                <Route path="/base/popovers" name="Popovers" component={Popovers}/>
                <Route path="/base/progress-bar" name="Progress Bar" component={ProgressBar}/>
                <Route path="/base/tooltips" name="Tooltips" component={Tooltips}/>
                <Route path="/buttons/buttons" name="Buttons" component={Buttons}/>
                <Route path="/buttons/button-dropdowns" name="ButtonDropdowns" component={ButtonDropdowns}/>
                <Route path="/buttons/button-groups" name="ButtonGroups" component={ButtonGroups}/>
                <Route path="/buttons/loading-buttons" name="Loading Buttons" component={LoadingButtons}/>
                <Route path="/buttons/social-buttons" name="Social Buttons" component={SocialButtons}/>
                <Route path="/charts" name="Charts" component={Charts}/>
                <Route path="/editors/text-editors" name="Text Editors" component={TextEditors}/>
                <Route path="/editors/code-editors" name="Code Editors" component={CodeEditors}/>
                <Route path="/agency/forms/basic-forms" name="Basic Forms" component={BasicForms}/>
                <Route path="/agency/forms/advanced-forms" name="Advanced Forms" component={AdvancedForms}/>
                <Route path="/google-maps" name="Google Maps" component={GoogleMaps}/>
                <Route path="/icons/flags" name="Flags" component={Flags}/>
                <Route path="/icons/font-awesome" name="Font Awesome" component={FontAwesome}/>
                <Route path="/icons/simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
                <Route path="/notifications/alerts" name="Alerts" component={Alerts}/>
                <Route path="/notifications/badges" name="Badges" component={Badges}/>
                <Route path="/notifications/modals" name="Modals" component={Modals}/>
                <Route path="/notifications/toastr" name="Toastr" component={Toastr}/>
                <Route path="/plugins/calendar" name="Calendar" component={Calendar}/>
                <Route path="/plugins/spinners" name="Loading Buttons" component={Spinners}/>
                <Route path="/tables/datatable" name="Data Table" component={DataTable}/>
                <Route path="/tables/tables" name="Tables" component={Tables}/>
                <Route path="/widgets" name="Widgets" component={Widgets}/>
                 <Route path="/ui-kits/invoicing/invoice" name="Invoice" component={Invoice}/>
                <Route path="/ui-kits/email/inbox" name="Invoice" component={Inbox}/>
                <Route path="/ui-kits/email/message" name="Message" component={Message}/>
                <Route path="/ui-kits/email/compose" name="Compose" component={Compose}/>                                   
                {/* <Redirect from="/" to="/dashboard"/> */}
              </Switch>
            </Container>
      </div>
    );
  }
}
export default Template;
