const routes = {
  "/admin": "Home",
  "/admin/dashboard": "Dashboard",
  "/admin/addagency": "Add Agency",
  "/admin/agencylist": "List of Agency",
  "/admin/editagency": "Edit Agency",
  "/admin/addsubadmin": "Add Administrators",
  "/admin/subadminlist": "List of Administrators",
  "/admin/editsubadmin": "Edit Administrator",
  "/admin/settings/general": "General Settings",
  "/admin/settings/smtp": "SMTP Settings",
  "/admin/settings/seo": "SEO Settings",
  "/admin/settings/sms": "SMS Settings",
  "/admin/settings/mobile": "Mobile Settings",
  "/admin/settings/paymentgetway": "Payment Gateway Settings",
  "/admin/settings/socialnetwork": "Social Network Settings",
  "/admin/addsubscription": "Add Subscriptions",
  "/admin/subscriptionlist": "List of Subscriptions",
  "/admin/subscriberslist": "List of Subscribers",
  "/admin/earningslist": "List of Earnings",
  "/admin/editsubscription": "Edit Subscriptions",
  "/admin/addemailtemplate": "Add Email Temlate",
  "/admin/editemailtemplate": "Edit Email Temlate",
  "/admin/listemailtemplate": "List of Email Temlate",
  "/admin/viewagency": "View Agency Details",
  "/admin/agency/timesheet": "Timesheet List",
  "/admin/agency/viewtimesheet": "View Timesheet Details",
  "/admin/agency/invoice": "Invoice List",
  "/admin/agency/invoicegroupdetails": "Invoice Details",
  "/admin/agency/viewinvoicedetails": "Invoice Details",
  "/admin/subscriptionpayment": "Subscription - Payment",
  "/admin/subscription/invoice": "Invoice",
  "/admin/subscription/invoicelist": "Invoice - Statement",
  "/admin/subscription/account": "Billing Accounts",
  "/admin/subscription/editaccount": "Edit Billing Accounts",
};
export const routesagency = {
  "/agency": "Home",
  "/agency/dashboard": "Dashboard",
  "/agency/editprofile": "Edit Profile",
  "/agency/addclient": "Add Client",
  "/agency/clientlist": "List of Clients",
  "/agency/editclient": "Edit Client",
  "/agency/viewclient": "View Client Details",
  "/agency/client/timesheet": "Timesheet List",
  "/agency/client/timesheetdetails": "View Timesheet Details",
  "/agency/client/invoice": "Invoice List",
  "/agency/client/invoicegroup": "Invoice Details",
  "/agency/addemployee": "Add Employees",
  "/agency/employeelist": "List of Employees",
  "/agency/editemployee": "Edit Employee",
  "/agency/viewemployee": "View Employee Deatils",
  "/agency/employee/timesheet": "Timesheet List",
  "/agency/employee/timesheetdetails": "View Timesheet Details",
  "/agency/employee/invoice": "Earning List",
  "/agency/employee/invoicegroup": "Earning Details",
  "/agency/editshift": "Add Shifts",
  "/agency/shiftlist": "Shifts List",
  "/agency/showemployees": "God View",
  "/agency/requestemployees": "Shift Details - Assign Employee",
  "/agency/timesheet": "Time Sheet List",
  "/agency/timesheetemployee": "Time Sheet List (Group By Employee)",
  "/agency/timesheetdetails": "Time Sheet Detaits",
  "/agency/addjobtype": "Add Job Role",
  "/agency/listjobtype": "List of Job Roles",
  "/agency/editjobtype": "Edit Job Role",
  "/agency/editjobtypeclient": "Edit Job Role - ( Client )",
  "/agency/editjobtypeemployee": "Edit Job Role - ( Employee )",
  "/agency/addworklocation": "Add Area",
  "/agency/listworklocation": "List of Areas",
  "/agency/editworklocation": "Edit Area",
  "/agency/addshifttemplate": "Add Shift Template",
  "/agency/listshifttemplate": "List Of Shift Templates",
  "/agency/editshifttemplate": "Edit Shift Template",
  "/agency/invoice": "Invoice List (Shifts)",
  "/agency/invoicedetails": "Invoice Details",
  "/agency/invoiceclientlist": "Invoice List (Clients)",
  "/agency/invoicegroupdetails": "Invoice Details (Client)",
  "/agency/addpublicholidays": "Add Public Holidays",
  "/agency/listpublicholidays": "List Of Public Holidays",
  "/agency/editpublicholidays": "Edit Public Holidays",
  "/agency/notifications": "Settings - Notifications",
  "/agency/editcompany": "Settings - General",
  "/agency/subscribe": "Subscription",
  "/agency/subscriptionpayment": "Subscription - Payment",
  "/agency/subscription/invoice": "Invoice",
  "/agency/subscription/invoicelist": "Invoice - Statement",
  "/agency/subscription/account": "Billing Accounts",
  "/agency/subscription/editaccount": "Edit Billing Accounts",
  "/agency/recruitment": "Recruitment - Candidates",
  "/agency/recruitmentform": "Recruitment - Online Application Form",
  "/agency/employee/staffprofile/view": "Staff Profile",
};
export const routesclient = {
  "/client": "Home",
  "/client/dashboard": "Dashboard",
  "/client/editprofile": "Edit Profile",
  "/client/shiftlist": "Shift List",
  "/client/editshift": "Edit Shift",
  "/client/showemployees": "God View",
  "/client/timesheet": "Time Sheet List",
  "/client/addshifttemplate": "Add Shift Template",
  "/client/listshifttemplate": "List Of Shift Templates",
  "/client/editshifttemplate": "Edit Shift Template",
  "/client/addworklocation": "Add Area",
  "/client/listworklocation": "List of Areas",
  "/client/editworklocation": "Edit Area",
  "/client/invoice": "List of Invoices",
  "/client/invoicedetails": "Invoice Details",
  "/client/invoiceclientlist": "Invoice List",
  "/client/invoicegroupdetails": "Invoice Details",
  "/client/requestemployees": "Shift Details - Assign Employee",
  "/client/timesheetemployee": "Time Sheet List (Group By Employee)",
  "/client/timesheetdetails": "Time Sheet Detaits",
  "/client/notifications": "Settings - Notifications",
  "/client/general": "Settings - General",
  "/client/jobrates": "List of Job Rates",
  "/client/viewjobrates": "View Job Rates",
};
export default routes;
