import React, { Component, Fragment } from "react";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { withRouter } from "react-router-dom";
import { Nav, NavbarBrand, NavbarToggler, NavItem, NavLink, Modal, ModalHeader, ModalBody, ModalFooter, Button, Col, Label, Row, TabContent, TabPane, Input } from "reactstrap";
import HeaderDropdown from "./HeaderDropdown";
import request, { NodeURL } from "../../api/api";
import { ToastContainer, toast } from "react-toastify";
import Select from "react-select";
import "react-select/dist/react-select.css";
import { breaklist } from "../../views/common/utils";
import classnames from "classnames";
import jwt_decode from "jwt-decode";

class Header extends Component {
  state = {
    sitesettings: "",
    user: "",
    agencysettingmodal: false,
    activeTab: "1",
    activeTabClient: "C2",
    shift_request_expiry: "",
    cancel_shift: "",
    shiftprefix: "",
    shiftprefixCond: "",
    shiftnumber: "",
    timesheetprefix: "",
    timesheetprefixCond: "",
    timesheetnumber: "",
    invoiceprefix: "",
    invoiceprefixCond: "",
    invoicenumber: "",
    invoice_due_days: "",
    Job_List: [],
    job_roles: [],
    job_roles_list: [],
    client_mailstatus: 0,
    client_smsstatus: 0
  };
  componentDidMount() {
    this.setState({ breaklist });
    const tokens = localStorage.getItem("APUS");
    const tokensAgency = localStorage.getItem("APUSA");
    const tokensClient = localStorage.getItem("APUSC");
    let token; let tokenAgency; let tokenClient;
    if (tokens) {
      token = jwt_decode(tokens);
    }
    if (tokensAgency) {
      tokenAgency = jwt_decode(tokensAgency);
    }
    if (tokensClient) {
      tokenClient = jwt_decode(tokensClient);
    }
    this.setState({ updateNotifi: true });
    request({
      url: "/site/settings",
      method: "POST"
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            sitesettings: res.response.settings || {}
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });

    if (tokenAgency && (this.props.location.pathname.slice(0, 8) === "/agency/" && tokenAgency.role === "agency")) {
      this.setState({
        user: tokenAgency
      });
    }
    if (tokenClient && (this.props.location.pathname.slice(0, 8) === "/client/" && tokenClient.role === "client")) {
      this.setState({
        user: tokenClient
      });
    }
    if (token && (this.props.location.pathname.slice(0, 7) === "/admin/" && token.role === "admin")) {
      this.setState({
        user: token
      });
    }
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle("sidebar-hidden");
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle("sidebar-minimized");
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle("sidebar-mobile-show");
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle("aside-menu-hidden");
  }

  AgencySettingOpen = () => {
    this.setState({
      agencysettingmodal: !this.state.agencysettingmodal
    });
    this.getAgencyprofile();
  };
  AgencySettingClose = () => {
    this.setState({
      agencysettingmodal: !this.state.agencysettingmodal,
      activeTab: "1",
      shift_request_expiry: "",
      cancel_shift: "",
      shiftprefix: "",
      shiftprefixCond: "",
      shiftnumber: "",
      timesheetprefix: "",
      timesheetprefixCond: "",
      timesheetnumber: "",
      invoiceprefix: "",
      invoiceprefixCond: "",
      invoicenumber: "",
      invoice_due_days: "",
      mailstatus: 0,
      smsstatus: 0
    });
  };
  getAgencyprofile() {
    request({
      url: "/agency/profile",
      method: "POST",
      data: { id: this.state.user.username }
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            shift_request_expiry: res.response.result ? res.response.result[0].settings.general.shift_request_expiry : "",
            cancel_shift: res.response.result ? res.response.result[0].settings.general.cancel_shift : "",
            invoice_due_days: res.response.result ? res.response.result[0].settings.general.invoice_due_days : "",
            shiftprefix: res.response.result && res.response.result[0].settings.shift ? res.response.result[0].settings.shift.prefix : "",
            shiftprefixCond: res.response.result && res.response.result[0].settings.shift ? res.response.result[0].settings.shift.prefix : "",
            shiftnumber: res.response.result && res.response.result[0].settings.shift ? res.response.result[0].settings.shift.number : "",
            timesheetprefix: res.response.result && res.response.result[0].settings.timesheet ? res.response.result[0].settings.timesheet.prefix : "",
            timesheetprefixCond: res.response.result && res.response.result[0].settings.timesheet ? res.response.result[0].settings.timesheet.prefix : "",
            timesheetnumber: res.response.result && res.response.result[0].settings.timesheet ? res.response.result[0].settings.timesheet.number : "",
            invoiceprefix: res.response.result && res.response.result[0].settings.invoice ? res.response.result[0].settings.invoice.prefix : "",
            invoiceprefixCond: res.response.result && res.response.result[0].settings.invoice ? res.response.result[0].settings.invoice.prefix : "",
            invoicenumber: res.response.result && res.response.result[0].settings.invoice ? res.response.result[0].settings.invoice.number : "",
            mailstatus: res.response.result && res.response.result[0].settings.notifications.email,
            smsstatus: res.response.result && res.response.result[0].settings.notifications.sms
          });
        } else if (res.status === 0) {
          toast.error(res);
        }
      })
      .catch(error => {
        return console.log(error);
      });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  selectChangeexpiry = value => {
    if (value) {
      this.setState({ shift_request_expiry: value.value });
    }
  };
  selectChangecancel = value => {
    if (value) {
      this.setState({ cancel_shift: value.value });
    }
  };
  statusChangeemail = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      mailstatus: e.target.checked || value
    });
  };
  statusChangesms = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      smsstatus: e.target.checked || value
    });
  };
  AgencySettingForm = (e, values) => {
    let data;
    if (this.state.activeTab === "1") {
      data = {
        shift_request_expiry: this.state.shift_request_expiry,
        cancel_shift: this.state.cancel_shift,
        invoice_due_days: this.state.invoice_due_days,
        for: "general"
      };
      this.settings(data);
    } else if (this.state.activeTab === "2") {
      data = {
        prefix: this.state.shiftprefix,
        number: this.state.shiftnumber,
        for: "shift"
      };
      if (this.state.shiftprefixCond === "") {
        this.settings(data);
      } else {
        toast.warning("Not allowed To Change");
      }
    } else if (this.state.activeTab === "3") {
      data = {
        prefix: this.state.timesheetprefix,
        number: this.state.timesheetnumber,
        for: "timesheet"
      };
      if (this.state.timesheetprefixCond === "") {
        this.settings(data);
      } else {
        toast.warning("Not allowed To Change");
      }
    } else if (this.state.activeTab === "4") {
      data = {
        prefix: this.state.invoiceprefix,
        number: this.state.invoicenumber,
        for: "invoice"
      };
      if (this.state.invoiceprefixCond === "") {
        this.settings(data);
      } else {
        toast.warning("Not allowed To Change");
      }
    } else if (this.state.activeTab === "5") {
      data = {
        mailstatus: this.state.mailstatus,
        smsstatus: this.state.smsstatus,
        for: "notifications"
      };
      this.settings(data);
      this.setState({
        agencysettingmodal: !this.state.agencysettingmodal});
    }
  };
  settings(data) {
    request({
      url: "/agency/settings/save",
      method: "POST",
      data: data
    })
      .then(response => {
        if (response.status === 1) {
          this.componentDidMount();
          toast.success("Updated");
        } else if (response.status === 0) {
          toast.error(response.response);
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  }

  ClientSettingOpen = () => {
    this.setState({
      clientsettingmodal: !this.state.clientsettingmodal
    });
    this.GetJobDetails();
  };

  GetJobDetails() {
    request({
      url: "/client/settings/job_types",
      method: "POST"
    })
      .then(res => {
        if (res.status === 1) {
          const job_roles_list = res.response.result.map(list => ({
            value: list._id,
            label: list.name
          }));
          this.setState({ job_roles_list });
          setTimeout(() => {
            this.GetSelectedJob();
          }, 1000);
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }
  GetSelectedJob() {
    request({
      url: "/client/profile",
      method: "POST",
      data: { id: this.state.user.username }
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            job_roles: res.response.result && res.response.result[0].settings && res.response.result[0].settings.job_roles,
            client_mailstatus: res.response.result ? res.response.result[0].settings.notifications.email : "",
            client_smsstatus: res.response.result ? res.response.result[0].settings.notifications.sms : ""
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }
  Job_select = value => {
    if (value) {
      this.setState({ job_roles: value });
    }
  };
  clientstatusChangeemail = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      client_mailstatus: e.target.checked || value
    });
  };
  clientstatusChangesms = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      client_smsstatus: e.target.checked || value
    });
  };
  ClientSettingForm = (e, values) => {
    if (this.state.activeTabClient === "C2") {
      const data = {
        mailstatus: this.state.client_mailstatus,
        smsstatus: this.state.client_smsstatus,
        for: "notifications"
      };
      this.ClientSettingSave(data);
    }
  };
  ClientSettingSave(data) {
    request({
      url: "/client/settings/save",
      method: "POST",
      data: data
    })
      .then(response => {
        if (response.status === 1) {
          toast.success("Updated");
          this.componentDidMount();
        } else if (response.status === 0) {
          toast.error(response);
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  }
  ClientSettingClose = () => {
    this.setState({
      clientsettingmodal: false,
      activeTabClient: "C2",
      Job_List: [],
      job_roles: [],
      job_roles_list: [],
      client_mailstatus: 0,
      client_smsstatus: 0
    });
  };

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  toggleClient(tab) {
    if (this.state.activeTabClient !== tab) {
      this.setState({
        activeTabClient: tab
      });
    }
  }

  render() {
    return (
      <Fragment>
        <header className="app-header navbar">
          <ToastContainer position="top-right" autoClose={2500} />
          <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
            <span className="navbar-toggler-icon" />
          </NavbarToggler>
          <NavbarBrand style={{ backgroundImage: "url(" + NodeURL + "/" + this.state.sitesettings.logo + ")" }} />
          <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>
            <span className="navbar-toggler-icon" />
          </NavbarToggler>
          <Nav className="d-md-down-none" navbar>
            {this.state.user.role === "admin" ? (
              <NavItem className="px-3">
                <NavLink>
                  <strong>Admin</strong>: {this.state.user.username}
                </NavLink>
              </NavItem>
            ) : null}
            {this.state.user.role === "agency" ? (
              <>
                {this.state.user.subscription === 1 ? (
                  <NavItem className="px-3">
                    <NavLink>
                      <strong>Agency</strong>: {this.state.user.username}
                    </NavLink>
                  </NavItem>
                ) : null}
              </>
            ) : null}
            {this.state.user.role === "client" ? (
              <NavItem className="px-3">
                <NavLink>
                  <strong>Client</strong>: {this.state.user.username}
                </NavLink>
              </NavItem>
            ) : null}
            {/* <NavItem className="px-3">
            <NavLink>Users</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink>Settings</NavLink>
          </NavItem> */}
          </Nav>
          <Nav className="ml-auto mr-3" navbar>
            {this.state.user && this.state.user.role === "agency" ? (
              <>
                {this.state.user.subscription === 1 ? (
                  <NavItem className="d-md-down-none cursor-pointer">
                    <NavLink onClick={this.AgencySettingOpen}>
                      <i className="icon-settings" />
                    </NavLink>
                  </NavItem>
                ) : null}
              </>
            ) : null}
            {this.state.user && this.state.user.role === "client" ? (
              <NavItem className="d-md-down-none cursor-pointer">
                <NavLink onClick={this.ClientSettingOpen}>
                  <i className="icon-settings" />
                </NavLink>
              </NavItem>
            ) : null}
            <HeaderDropdown notif />
            <HeaderDropdown tasks />
            {/* <HeaderDropdown mssgs/> */}
            <HeaderDropdown accnt />
          </Nav>
          {/* <NavbarToggler className="d-md-down-none" onClick={this.asideToggle}>*/}
          {/* <span className="navbar-toggler-icon"></span>*/}
          {/* </NavbarToggler>*/}
        </header>
        <Modal isOpen={this.state.agencysettingmodal} className={"modal-lg " + this.props.className}>
          <AvForm onValidSubmit={this.AgencySettingForm}>
            <ModalHeader toggle={this.AgencySettingClose}>
              {" "}
              <i className="fa fa-cogs" /> Settings
            </ModalHeader>
            <ModalBody className="p-0">
              <Nav tabs className="settings-admins">
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "1" })}
                    onClick={() => {
                      this.toggle("1");
                    }}
                  >
                    <i className="fa fa-dot-circle-o" /> General
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "2" })}
                    onClick={() => {
                      this.toggle("2");
                    }}
                  >
                    <i className="fa fa-file-text-o" /> Shift
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "3" })}
                    onClick={() => {
                      this.toggle("3");
                    }}
                  >
                    <i className="fa fa-clock-o" /> Timesheet
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "4" })}
                    onClick={() => {
                      this.toggle("4");
                    }}
                  >
                    <i className="fa fa-money" /> Invoice
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "5" })}
                    onClick={() => {
                      this.toggle("5");
                    }}
                  >
                    <i className="fa fa-bell" /> Notifications
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                  <Row>
                    <Col xs="12" md="4">
                      <Label>
                        Shift Request Expiry Duration <span>(In Minutes)</span>
                      </Label>
                      <Select name="shift_request_expiry" value={this.state.shift_request_expiry} options={this.state.breaklist} onChange={this.selectChangeexpiry} />
                    </Col>

                    <Col xs="12" md="4">
                      <Label>
                        Cancel Shift Duration <span>(In Minutes)</span>
                      </Label>
                      <Select name="cancel_shift" value={this.state.cancel_shift} options={this.state.breaklist} onChange={this.selectChangecancel} />
                    </Col>

                    <Col xs="12" md="4">
                      <AvField
                        name="invoice_due_days"
                        onChange={this.onChange}
                        value={this.state.invoice_due_days}
                        placeholder="Enter No. Of Days"
                        label="Invoice Due Days"
                        type="number"
                        errorMessage="This is required"
                        validate={{ required: { value: true }, min: { value: 0 } }}
                      />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2">
                  <h5>Shift</h5>
                  <Row>
                    {this.state.activeTab === "2" ? (
                      <Fragment>
                        <Col xs="12" md="6">
                          <AvField
                            name="shiftprefix"
                            onChange={this.onChange}
                            value={this.state.shiftprefix}
                            placeholder="Enter prefix"
                            label="Prefix"
                            type="text"
                            errorMessage="This is required (Minimun 3 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 3 } }}
                          />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField
                            name="shiftnumber"
                            onChange={this.onChange}
                            value={this.state.shiftnumber}
                            placeholder="Enter Number"
                            label="Number"
                            type="number"
                            errorMessage="This is required (Minimun 3 Numbers)"
                            validate={{ required: { value: true }, minLength: { value: 3 } }}
                          />
                        </Col>
                      </Fragment>
                    ) : null}
                  </Row>
                </TabPane>
                <TabPane tabId="3">
                  <h5>Timesheet</h5>
                  <Row>
                    {this.state.activeTab === "3" ? (
                      <Fragment>
                        <Col xs="12" md="6">
                          <AvField
                            name="timesheetprefix"
                            onChange={this.onChange}
                            value={this.state.timesheetprefix}
                            placeholder="Enter prefix"
                            label="Prefix"
                            type="text"
                            errorMessage="This is required (Minimun 4 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 4 } }}
                          />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField
                            name="timesheetnumber"
                            onChange={this.onChange}
                            value={this.state.timesheetnumber}
                            placeholder="Enter Number"
                            label="Number"
                            type="number"
                            errorMessage="This is required (Minimun 4 Numbers)"
                            validate={{ required: { value: true }, minLength: { value: 4 } }}
                          />
                        </Col>
                      </Fragment>
                    ) : null}
                  </Row>
                </TabPane>
                <TabPane tabId="4">
                  <h5>Invoice</h5>
                  <Row>
                    {this.state.activeTab === "4" ? (
                      <Fragment>
                        <Col xs="12" md="6">
                          <AvField
                            name="invoiceprefix"
                            onChange={this.onChange}
                            value={this.state.invoiceprefix}
                            placeholder="Enter prefix"
                            label="Prefix"
                            type="text"
                            errorMessage="This is required (Minimun 5 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 5 } }}
                          />
                        </Col>
                        <Col xs="12" md="6">
                          <AvField
                            name="invoicenumber"
                            onChange={this.onChange}
                            value={this.state.invoicenumber}
                            placeholder="Enter Number"
                            label="Number"
                            type="number"
                            errorMessage="This is required (Minimun 5 Number)"
                            validate={{ required: { value: true }, minLength: { value: 5 } }}
                          />
                        </Col>
                      </Fragment>
                    ) : null}
                  </Row>
                </TabPane>
                <TabPane tabId="5">
                  <h5>Notifications</h5>
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>Mail</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.mailstatus} onChange={this.statusChangeemail} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>

                    <Col xs="12" md="4">
                      <div>
                        <label>SMS</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.smsstatus} onChange={this.statusChangesms} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </ModalBody>
            <ModalFooter>
              <Button color="success" className="pull-tight" type="submit">
                Save
              </Button>{" "}
              <Button color="secondary" className="pull-left" type="button" onClick={this.AgencySettingClose}>
                Close
              </Button>
            </ModalFooter>
          </AvForm>
        </Modal>
        <Modal isOpen={this.state.clientsettingmodal} className={"modal-lg " + this.props.className}>
          <AvForm onValidSubmit={this.ClientSettingForm}>
            <ModalHeader toggle={this.ClientSettingClose}>
              {" "}
              <i className="fa fa-cogs" /> Settings
            </ModalHeader>
            <ModalBody className="p-0">
              <Nav tabs className="settings-admins">
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTabClient === "C2" })}
                    onClick={() => {
                      this.toggleClient("C2");
                    }}
                  >
                    <i className="fa fa-bell" /> Notifications
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTabClient}>
                <TabPane tabId="C2">
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>Mail</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.client_mailstatus} onChange={this.clientstatusChangeemail} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>SMS</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.client_smsstatus} onChange={this.clientstatusChangesms} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </ModalBody>
            <ModalFooter>
              <Button color="success" className="pull-tight" type="submit">
                Save
              </Button>{" "}
              <Button color="secondary" className="pull-left" type="button" onClick={this.ClientSettingClose}>
                Close
              </Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </Fragment>
    );
  }
}

export default withRouter(Header);
