import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
// import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Dropdown } from "reactstrap";
import request, { NodeURL, client } from "../../api/api";
import jwt_decode from "jwt-decode";
import io from "socket.io-client";

const propTypes = {
  notif: PropTypes.bool,
  accnt: PropTypes.bool,
  tasks: PropTypes.bool,
  mssgs: PropTypes.bool
};
const defaultProps = {
  notif: false,
  accnt: false,
  tasks: false,
  mssgs: false
};

class HeaderDropdown extends Component {
  Logout = e => {
    if (this.state.user) {
      switch (this.state.user.role) {
        case "admin":
          this.adminlogoutstatus(this.state.user);
          break;
        case "agency":
          this.agencylogoutstatus(this.state.user);
          break;
        case "client":
          this.clientlogoutstatus(this.state.user);
          break;
        default:
          localStorage.clear();
          this.props.history.replace("/");
      }
    }
  };

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      token: "",
      dropdownOpen: false,
      admindashboard: "",
      notificationcount: "",
      agenydashboard: "",
      tokenAgency: "",
      tokenClient: "",
      agency_avatar: "",
      client_avatar: "",
      user: "",
      agenyoverall: ""
    };
  }
  componentDidMount() {
    // const tokenforprofile = this.props.token_role;
    const tokens = localStorage.getItem("APUS");
    const tokensAgency = localStorage.getItem("APUSA");
    const tokensClient = localStorage.getItem("APUSC");
    let token, tokenAgency, tokenClient;
    if (tokens) {
      token = jwt_decode(tokens);
    }
    if (tokensAgency) {
      tokenAgency = jwt_decode(tokensAgency);
    }
    if (tokensClient) {
      tokenClient = jwt_decode(tokensClient);
    }
   /*    const socket = io(`${NodeURL}/notify`);
    socket.on("web_notification", data => {
      switch (data.type) {
        case "admin":
          if (token && (this.props.location.pathname.slice(0, 7) === "/admin/" && token.role === "admin")) {
            this.getAdminnotify();
          }
          break;
        case "agency":
          if (tokenAgency && (this.props.location.pathname.slice(0, 8) === "/agency/" && tokenAgency.role === "agency")) {
            this.getAgencynotify();
          }
          break;
        case "client":
          if (tokenClient && (this.props.location.pathname.slice(0, 8) === "/client/" && tokenClient.role === "client")) {
            this.getClientnotify();
          }
          break;
        default:
          break;
      }
    });*/
    if (token && (this.props.location.pathname.slice(0, 7) === "/admin/" && token.role === "admin")) {
      this.setState({
        user: token
      });
      this.getAdminnotify();
    }
    if (tokenAgency && (this.props.location.pathname.slice(0, 8) === "/agency/" && tokenAgency.role === "agency")) {
      this.setState({
        user: tokenAgency
      });
      this.getAgencynotify();
      request({
        url: "/agency/profile",
        method: "POST",
        data: { id: "" }
      }).then(res => {
        if (res && res.status === 1) {
          this.setState({ agency_avatar: res.response.result[0].avatar });
        }
      });
    }
    if (tokenClient && (this.props.location.pathname.slice(0, 8) === "/client/" && tokenClient.role === "client")) {
      this.setState({
        user: tokenClient
      });
      this.getClientnotify();
      request({
        url: "/client/profile",
        method: "POST",
        data: { id: this.state.user.username }
      }).then(res => {
        if (res && res.status === 1) {
          this.setState({ client_avatar: res.response.result[0].avatar });
        }
      });
    }
  }
  getAdminnotify() {
    const tokens = localStorage.getItem("APUS");
    client.defaults.headers.common["Authorization"] = tokens;
    request({
      url: "/administrators/notifications",
      method: "post"
    }).then(res => {
      // console.log("res", res);
      if (res && res.status === 1) {
        this.setState({ admindashboard: res.response.Notifications[0] || {} });
        this.setState({ notificationcount: res.response.overall });
      }
    });
  }
  getAgencynotify() {
    const tokensAgency = localStorage.getItem("APUSA");
    client.defaults.headers.common["Authorization"] = tokensAgency;
    request({
      url: "/agency/notifications",
      method: "post"
    }).then(res => {
      // console.log("res", res);
      if (res && res.status === 1) {
        this.setState({ agenydashboard: res.response.Notifications ? res.response.Notifications[0] : {} });
        this.setState({ agenyoverall: res.response.overall });
      }
    });
  }
  getClientnotify() {
    const tokensClient = localStorage.getItem("APUSC");
    client.defaults.headers.common["Authorization"] = tokensClient;
    request({
      url: "/client/notifications",
      method: "post"
    }).then(res => {
      // console.log("res", res);
      if (res && res.status === 1) {
        this.setState({ clientdashboard: res.response.Notifications ? res.response.Notifications[0] : {} });
        this.setState({ clientoverall: res.response.overall });
      }
    });
  }
  adminlogoutstatus(token) {
    request({
      url: "/adminlogout",
      method: "POST",
      data: { username: token.username }
    }).then(res => {
      if (res.status === 1) {
      } else if (res.status === 0) {
        // toast.error("please try again");
      }
    });
      localStorage.removeItem("APUS");
      this.props.history.replace({ pathname: "/admin", state: "notify" });
  }

  agencylogoutstatus(token) {
    request({
      url: "/agencylogout",
      method: "POST",
      data: { username: token.username }
    }).then(res => {
      if (res.status === 1) {
      } else if (res.status === 0) {
        // toast.error("please try again");
      }
    });
      localStorage.removeItem("APUSA");
      this.props.history.replace({ pathname: "/agency", state: "notify" });
  }

  clientlogoutstatus(token) {
    request({
      url: "/clientlogout",
      method: "POST",
      data: { username: token.username }
    }).then(res => {
      if (res.status === 1) {
      } else if (res.status === 0) {
        // toast.error("please try again");
      }
    });
      localStorage.removeItem("APUSC");
      this.props.history.replace({ pathname: "/client", state: "notify" });
  }
  AdminClearNotification = () => {
    request({
      url: "/admin/clearnotification",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
      }
    });
  };
  AgencyClearNotification = () => {
    request({
      url: "/agency/clearnotification",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
      }
    });
  };
  ClientClearNotification = () => {
    request({
      url: "/client/clearnotification",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
      }
    });
  };
  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  dropNotif() {
    // const itemsCount = 0;
    return (
      <Fragment>
        <div>
          {this.state.user.role === "agency" ? (
            <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle nav>
                <i className="icon-bell" />
                <Badge pill color="danger">
                  {this.state.agenyoverall ? <Fragment>{this.state.agenyoverall}</Fragment> : 0}
                </Badge>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem header tag="div" className="text-center">
                  <strong>You have {this.state.agenyoverall ? <Fragment>{this.state.agenyoverall}</Fragment> : 0} notifications</strong>
                </DropdownItem>
                {this.state.agenydashboard && this.state.agenydashboard.job_accepted ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/shiftlist",
                        state: { notificationName: "job_accepted" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Accepted
                    <Badge pill color="primary">
                      {this.state.agenydashboard.job_accepted}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.shift_unassigned ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/shiftlist",
                        state: { notificationName: "shift_unassigned" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Cancelled
                    <Badge pill color="primary">
                      {this.state.agenydashboard.shift_unassigned}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.timesheet_request ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/timesheetemployee",
                        state: { notificationName: "timesheet_request" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Time Sheet Request
                    <Badge pill color="primary">
                      {this.state.agenydashboard.timesheet_request}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.job_started ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/shiftlist",
                        state: { notificationName: "job_started" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Started
                    <Badge pill color="primary">
                      {this.state.agenydashboard.job_started}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.job_completed ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/shiftlist",
                        state: { notificationName: "job_completed" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Completed
                    <Badge pill color="primary">
                      {this.state.agenydashboard.job_completed}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.client_register ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/clientlist",
                        state: { notificationName: "client_register" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-user-follow text-info" /> New Clients
                    <Badge pill color="info">
                      {this.state.agenydashboard.client_register}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.employee_register ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/employeelist",
                        state: { notificationName: "employee_register" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-user-follow text-success" /> New Employees
                    <Badge pill color="success">
                      {this.state.agenydashboard.employee_register}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.subscription_expired ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/subscribe",
                        state: { notificationName: "subscription_expired" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-user-follow text-success" /> Subscription Notice
                    <Badge pill color="success">
                      {this.state.agenydashboard.subscription_expired}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.new_online_form ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/recruitment/candidates",
                        state: { notificationName: "new_online_form" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-user-follow text-success" /> New Candidate Request
                    <Badge pill color="success">
                      {this.state.agenydashboard.new_online_form}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.agenydashboard && this.state.agenydashboard.referee_form_completed ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/agency/recruitment",
                        state: { notificationName: "referee_form_completed" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-user-follow text-success" /> Referee Form Completed
                    <Badge pill color="success">
                      {this.state.agenydashboard.referee_form_completed}
                    </Badge>
                  </DropdownItem>
                ) : null}
                <DropdownItem onClick={this.AgencyClearNotification} className="text-center">
                  <i className="fa fa-remove text-success" /> Clear All Notification
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          ) : null}
        </div>
        <div>
          {this.state.user.role === "client" ? (
            <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle nav>
                <i className="icon-bell" />
                <Badge pill color="danger">
                  {this.state.clientoverall ? <Fragment>{this.state.clientoverall}</Fragment> : 0}
                </Badge>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem header tag="div" className="text-center">
                  <strong>You have {this.state.clientoverall ? <Fragment>{this.state.clientoverall}</Fragment> : 0} notifications</strong>
                </DropdownItem>
                {this.state.clientdashboard && this.state.clientdashboard.job_accepted ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/shiftlist",
                        state: { notificationName: "job_accepted" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Accepted
                    <Badge pill color="primary">
                      {this.state.clientdashboard.job_accepted}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.clientdashboard && this.state.clientdashboard.shift_unassigned ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/shiftlist",
                        state: { notificationName: "shift_unassigned" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Cancelled
                    <Badge pill color="primary">
                      {this.state.clientdashboard.shift_unassigned}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.clientdashboard && this.state.clientdashboard.timesheet_request ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/timesheetemployee",
                        state: { notificationName: "timesheet_request" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Time Sheet Request
                    <Badge pill color="primary">
                      {this.state.clientdashboard.timesheet_request}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.clientdashboard && this.state.clientdashboard.job_started ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/shiftlist",
                        state: { notificationName: "job_started" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Started
                    <Badge pill color="primary">
                      {this.state.clientdashboard.job_started}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.clientdashboard && this.state.clientdashboard.job_completed ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/shiftlist",
                        state: { notificationName: "job_completed" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Shifts Completed
                    <Badge pill color="primary">
                      {this.state.clientdashboard.job_completed}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.clientdashboard && this.state.clientdashboard.invoice_approved ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/timesheetemployee",
                        state: { notificationName: "timesheet_approved" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Timesheet Approved
                    <Badge pill color="primary">
                      {this.state.clientdashboard.timesheet_approved}
                    </Badge>
                  </DropdownItem>
                ) : null}
                {this.state.clientdashboard && this.state.clientdashboard.invoice_approved ? (
                  <DropdownItem
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/client/invoiceclientlist",
                        state: { notificationName: "invoice_approved" }
                      });
                      setTimeout(() => {
                        this.componentDidMount();
                      }, 3000);
                    }}
                  >
                    <i className="icon-clock text-primary" /> Invoice Approved
                    <Badge pill color="primary">
                      {this.state.clientdashboard.invoice_approved}
                    </Badge>
                  </DropdownItem>
                ) : null}
                <DropdownItem onClick={this.ClientClearNotification} className="text-center">
                  <i className="fa fa-remove text-success" /> Clear All Notification
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          ) : null}
        </div>
      </Fragment>
    );
  }

  dropAccnt() {
    return (
      <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
          {this.props.location.pathname.slice(0, 7) === "/admin/" ? (
            <div>
              <img width="40px" height="40px" src={"../../../img/avatars/9.jpg"} className="img-avatar" alt="profile" />
            </div>
          ) : null}
          {this.props.location.pathname.slice(0, 8) === "/agency/" ? (
            <div>
              {this.state.agency_avatar ? (
                <img
                  width="40px"
                  height="40px"
                  src={NodeURL + "/" + this.state.agency_avatar}
                  className="img-avatar"
                  alt="profile"
                  onError={() => {
                    this.setState({ avatar_return: "../../img/user-profile.png" });
                  }}
                />
              ) : (
                <img
                  width="40px"
                  height="40px"
                  src={"../../img/avatars/9.jpg"}
                  className="img-avatar"
                  alt="profile"
                  onError={() => {
                    this.setState({ avatar_return: "../../img/user-profile.png" });
                  }}
                />
              )}
            </div>
          ) : null}
          {this.props.location.pathname.slice(0, 8) === "/client/" ? (
            <div>
              {this.state.client_avatar ? (
                <img
                  width="40px"
                  height="40px"
                  src={NodeURL + "/" + this.state.client_avatar}
                  className="img-avatar"
                  alt="profile"
                  onError={() => {
                    this.setState({ avatar_return: "../../img/user-profile.png" });
                  }}
                />
              ) : (
                <img
                  width="40px"
                  height="40px"
                  src={"../../img/avatars/9.jpg"}
                  className="img-avatar"
                  alt="profile"
                  onError={() => {
                    this.setState({ avatar_return: "../../img/user-profile.png" });
                  }}
                />
              )}
            </div>
          ) : null}
        </DropdownToggle>
        <DropdownMenu right>
          {this.props.location.pathname.slice(0, 7) === "/admin/" ? (
            <DropdownItem
              onClick={() => {
                this.props.history.push("/admin/settings/general");
              }}
            >
              <i className="fa fa-cog" /> Settings
            </DropdownItem>
          ) : null}
          {this.state.user.role === "agency" ? (
            <div>
              <DropdownItem
                onClick={() => {
                  this.props.history.push("/agency/editprofile");
                }}
              >
                <i className="fa fa-user" /> Edit Profile
              </DropdownItem>
              <DropdownItem
                onClick={() => {
                  this.props.history.push("/agency/subscribe");
                }}
              >
                <i className="fa fa-gift" /> Subscription
              </DropdownItem>
            </div>
          ) : null}
          {this.state.user.role === "client" ? (
            <div>
              <DropdownItem
                onClick={() => {
                  this.props.history.push("/client/editprofile");
                }}
              >
                <i className="fa fa-user" /> Edit Profile
              </DropdownItem>
            </div>
          ) : null}
          <DropdownItem onClick={this.Logout.bind(this)}>
            <i className="fa fa-lock" /> Logout
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    );
  }

  dropTasks() {
    // const itemsCount = 0;
    return (
      <div>
        {this.state.user.role === "admin" && this.props.location.pathname.slice(0, 7) === "/admin/" ? (
          <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle nav>
              <i className="icon-bell" />
              <Badge pill color="warning">
                {this.state.notificationcount ? this.state.notificationcount : 0}
              </Badge>
            </DropdownToggle>
            <DropdownMenu right className="dropdown-menu-lg">
              <DropdownItem header tag="div" className="text-center">
                <strong>You have {this.state.notificationcount || 0} notifications</strong>
              </DropdownItem>
              {this.state.admindashboard.agency_register ? (
                <DropdownItem
                  onClick={() => {
                    this.props.history.push({
                      pathname: "/admin/agencylist",
                      state: { notificationName: "agency_register" }
                    });
                    setTimeout(() => {
                      this.componentDidMount();
                    }, 3000);
                  }}
                >
                  <i className="icon-user-follow text-success" /> New Agencies
                  <Badge pill color="success">
                    {this.state.admindashboard.agency_register}
                  </Badge>
                </DropdownItem>
              ) : null}
              <DropdownItem onClick={this.AdminClearNotification} className="text-center">
                <i className="fa fa-remove text-success" /> Clear All Notification
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        ) : null}
      </div>
    );
  }

  render() {
    const { notif, accnt, tasks, mssgs, /* ...attributes */} = this.props;
    return notif ? this.dropNotif() : accnt ? this.dropAccnt() : tasks ? this.dropTasks() : mssgs ? this.dropMssgs() : null;
  }
}

HeaderDropdown.propTypes = propTypes;
HeaderDropdown.defaultProps = defaultProps;

export default withRouter(HeaderDropdown);
