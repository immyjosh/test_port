export default {
  items: [
    // {
    //   role: "admin",
    //   title: true,
    //   name: "Dashboard",
    //   wrapper: {
    //     // optional wrapper object
    //     element: "", // required valid HTML5 element tag
    //     attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: "" // optional class names space delimited list for title item ex: "text-center"
    // },
    // {
    //   role: "agency",
    //   title: true,
    //   name: "Dashboard",
    //   wrapper: {
    //     // optional wrapper object
    //     element: "", // required valid HTML5 element tag
    //     attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: "" // optional class names space delimited list for title item ex: "text-center"
    // },
    // {
    //   role: "client",
    //   title: true,
    //   name: "Dashboard",
    //   wrapper: {
    //     // optional wrapper object
    //     element: "", // required valid HTML5 element tag
    //     attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: "" // optional class names space delimited list for title item ex: "text-center"
    // },
    // {
    //   role: "theme",
    //   name: "Dashboard",
    //   url: "/dashboard",
    //   icon: "icon-speedometer",
    //   badge: {
    //     variant: "info",
    //     text: "NEW"
    //   }
    // },
    {
      role: "admin",
      name: "Dashboard",
      url: "/admin/dashboard",
      icon: "icon-layers"
    },
    {
      role: "admin",
      name: "Admin",
      icon: "icon-user",
      children: [
        {
          name: "All Admins",
          url: "/admin/subadminlist",
          icon: "icon-list"
        },
        {
          name: "Add New",
          url: "/admin/addsubadmin",
          icon: "icon-user-follow"
        }
      ]
    },

    {
      role: "admin",
      name: "Agencies",
      icon: "icon-user",
      children: [
        {
          name: "All Agencies",
          url: "/admin/agencylist",
          icon: "icon-list"
        },
        {
          name: "Add New",
          url: "/admin/addagency",
          icon: "icon-user-follow"
        }
      ]
    },

    {
      role: "admin",
      name: "Subscription",
      icon: "icon-trophy",
      children: [
        {
          name: "All Subscriptions",
          url: "/admin/subscriptionlist",
          icon: "icon-list"
        },
        {
          name: "Add New",
          url: "/admin/addsubscription",
          icon: "icon-trophy"
        }
      ]
    },
    {
      role: "admin",
      name: "Subscribers",
      url: "/admin/subscriberslist",
      icon: "fa fa-gift"
    },

    {
      role: "admin",
      name: "Earnings",
      url: "/admin/earningslist",
      icon: "fa fa-money"
    },

    {
      role: "admin",
      name: "Email Templates",
      icon: "fa fa-envelope",
      children: [
        {
          name: "All Email Templates",
          url: "/admin/listemailtemplate",
          icon: "icon-list"
        },
        {
          name: "Add New",
          url: "/admin/addemailtemplate",
          icon: "fa fa-envelope-o"
        }
      ]
    },
    {
      role: "admin",
      name: "Settings",
      url: "/settings",
      icon: "fa fa-cogs",
      children: [
        {
          name: "General",
          url: "/admin/settings/general",
          icon: "icon-settings"
        },
        {
          name: "SMTP",
          url: "/admin/settings/smtp",
          icon: "fa fa-comments-o"
        },
        // {
        //   name: "SEO",
        //   url: "/admin/settings/seo",
        //   icon: "icon-globe"
        // }
        {
          name: "SMS",
          url: "/admin/settings/sms",
          icon: "fa fa-envelope-o"
        },
        {
          name: "Payment Gateway",
          url: "/admin/settings/paymentgetway",
          icon: "fa fa-money"
        }
        // {
        //   name: "Mobile",
        //   url: "/admin/settings/mobile",
        //   icon: "fa fa-mobile"
        // },
        // {
        //   name: "Socialnetwork",
        //   url: "/admin/settings/socialnetwork",
        //   icon: "fa fa-share-square"
        // }
      ]
    },
    {
      role: "agency",
      name: "Dashboard",
      url: "/agency/dashboard",
      icon: "icon-star",
      recruitment_module: 0
    },
    {
      role: "agency",
      name: "Shifts",
      url: "/agency/editshift",
      icon: "fa fa-calendar",
      recruitment_module: 0
    },
    {
      role: "agency",
      name: "Timesheets",
      url: "/agency/timesheetemployee",
      icon: "icon-clock",
      recruitment_module: 0
    },
    {
      role: "agency",
      name: "Invoices",
      url: "/agency/invoiceclientlist",
      icon: "fa fa-money",
      recruitment_module: 0
    },

    // {
    //   role: "agency",
    //   name: "God View",
    //   url: "/agency/showemployees",
    //   icon: "icon-map"
    // },
    {
      role: "agency",
      name: "Employees",
      icon: "icon-people",
      recruitment_module: 0,
      children: [
        {
          name: "All Employees",
          url: "/agency/employeelist",
          icon: "icon-list"
        },
        {
          name: "Add New",
          url: "/agency/addemployee",
          icon: "icon-user-follow"
        }
      ]
    },
    {
      role: "agency",
      name: "Clients",
      icon: "icon-user-following",
      recruitment_module: 0,
      children: [
        {
          name: "All Clients",
          url: "/agency/clientlist",
          icon: "icon-list"
        },
        {
          name: "Add New",
          url: "/agency/addclient",
          icon: "icon-user-follow"
        }
      ]
    },
    {
      role: "agency",
      name: "Recruitment",
      // url: "/agency/recruitment",
      icon: "fa fa-bell",
      recruitment_module: 1,
      children: [
       /*  {
          name: "Candidates",
          url: "/agency/recruitment/candidates",
          icon: "icon-list"
        }, */
        {
          name: "Candidates",
          url: "/agency/recruitment/application",
          icon: "fa fa-check-square-o"
        },
       /* {
          name: "Files",
          url: "/agency/recruitment/files",
          icon: "fa fa-files-o"
        },*/
        {
          name: "Interviews",
          url: "/agency/recruitment/interview",
          icon: "fa fa-users"
        },
        {
          name: "References",
          url: "/agency/recruitment/references",
          icon: "fa fa-share-alt"
        },
        {
          name: "Summary",
          url: "/agency/recruitment/summary",
          icon: "fa fa-info-circle"
        }
      ]
    },
    {
      role: "agency",
      name: "File Manager",
      url: "/agency/recruitment/files",
      icon: "fa fa-folder-open-o",
      recruitment_module: 1,
    },
    {
      role: "agency",
      name: "Settings",
      url: "/settings",
      icon: "fa fa-cogs",
      recruitment_module: 0,
      children: [
        {
          role: "agency",
          name: "General",
          url: "/agency/editcompany",
          icon: "fa fa-user-md"
        },

        {
          role: "agency",
          name: "Subscription",
          url: "/agency/subscribe",
          icon: "fa fa-gift"
        },
        {
          role: "agency",
          name: "Shift Templates",
          url: "/settings",
          icon: "fa fa-calendar-o",
          children: [
            {
              name: "All Shift Templates",
              url: "/agency/listshifttemplate",
              icon: "icon-list"
            },
            {
              name: "Add New",
              url: "/agency/addshifttemplate",
              icon: "icon-list"
            }
          ]
        },

        {
          name: "Job Roles",
          url: "/settings/",
          icon: "fa fa-user-md",
          children: [
            {
              name: "All Job Role",
              url: "/agency/listjobtype",
              icon: "icon-list"
            },
            {
              name: "Add New",
              url: "/agency/addjobtype",
              icon: "icon-list"
            }
          ]
        },
        {
          role: "agency",
          url: "/settings",
          name: "Coverage",
          icon: "icon-location-pin",
          children: [
            {
              name: "All Areas",
              url: "/agency/listworklocation",
              icon: "icon-list"
            },
            {
              name: "Add New",
              url: "/agency/addworklocation",
              icon: "icon-user-follow"
            }
          ]
        },
        {
          role: "agency",
          url: "/settings/",
          name: "Public Holidays",
          icon: "fa fa-eercast",
          children: [
            {
              name: "All Public Holidays",
              url: "/agency/listpublicholidays",
              icon: "icon-list"
            },
            {
              name: "Add New",
              url: "/agency/addpublicholidays",
              icon: "icon-user-follow"
            }
          ]
        },
        {
          role: "agency",
          url: "/settings/",
          name: "Email Templates",
          icon: "fa fa-envelope",
          children: [
            {
              name: "All Mail Templates",
              url: "/agency/settings/email/list",
              icon: "icon-list"
            },
            {
              name: "Add New",
              url: "/agency/settings/email/add",
              icon: "icon-user-follow"
            }
          ]
        }
      ]
    },
    {
      role: "client",
      name: "Dashboard",
      url: "/client/dashboard",
      icon: "icon-star"
    },
    {
      role: "client",
      name: "Shifts",
      url: "/client/editshift",
      icon: "icon-calendar"
    },
    // {
    //   role: "client",
    //   name: "God View",
    //   url: "/client/showemployees",
    //   icon: "icon-map"
    // },
    {
      role: "client",
      name: "Timesheets",
      url: "/client/timesheetemployee",
      icon: "icon-clock"
    },
    {
      role: "client",
      name: "Invoices",
      url: "/client/invoiceclientlist",
      icon: "fa fa-money"
    },
    {
      role: "client",
      name: "Settings",
      url: "/settings",
      icon: "fa fa-cogs",
      children: [
        // {
        //   role: "client",
        //   name: "General",
        //   url: "/client/general",
        //   icon: "fa fa-user-md"
        // },
        // {
        //   role: "client",
        //   name: "Notifications",
        //   url: "/client/notifications",
        //   icon: "fa fa-bell"
        // },
        {
          role: "client",
          name: "View Rates",
          url: "/client/jobrates",
          icon: "fa fa-money"
        },
        {
          role: "client",
          url: "/settings",
          name: "Shift Templates",
          icon: "icon-layers",
          children: [
            {
              name: "All Shift Templates",
              url: "/client/listshifttemplate",
              icon: "icon-list"
            },
            {
              name: "Add New",
              url: "/client/addshifttemplate",
              icon: "icon-list"
            }
          ]
        }
      ]
    },
    // {
    //   role: "client",
    //   name: "Work Location",
    //   icon: "icon-location-pin",
    //   children: [
    //     // {
    //     //   name: "Add Work Location",
    //     //   url: "/client/addworklocation",
    //     //   icon: "icon-list"
    //     // },
    //     {
    //       name: "Work Location List",
    //       url: "/client/listworklocation",
    //       icon: "icon-list"
    //     }
    //   ]
    // },
    {
      role: "theme",
      title: true,
      name: "Theme"
    },
    {
      role: "theme",
      name: "Colors",
      url: "/theme/colors",
      icon: "icon-drop"
    },
    {
      role: "theme",
      name: "Typography",
      url: "/theme/typography",
      icon: "icon-pencil"
    },
    {
      role: "theme",
      name: "Base",
      url: "/base",
      icon: "icon-puzzle",
      children: [
        {
          name: "Breadcrumbs",
          url: "/base/breadcrumbs",
          icon: "icon-puzzle"
        },
        {
          name: "Cards",
          url: "/base/cards",
          icon: "icon-puzzle"
        },
        {
          name: "Carousels",
          url: "/base/carousels",
          icon: "icon-puzzle"
        },
        {
          name: "Collapses",
          url: "/base/collapses",
          icon: "icon-puzzle"
        },
        {
          name: "Dropdowns",
          url: "/base/dropdowns",
          icon: "icon-puzzle"
        },
        {
          name: "Jumbotrons",
          url: "/base/jumbotrons",
          icon: "icon-puzzle"
        },
        {
          name: "List groups",
          url: "/base/list-groups",
          icon: "icon-puzzle"
        },
        {
          name: "Navs",
          url: "/base/navs",
          icon: "icon-puzzle"
        },
        {
          name: "Paginations",
          url: "/base/paginations",
          icon: "icon-puzzle"
        },
        {
          name: "Popovers",
          url: "/base/popovers",
          icon: "icon-puzzle"
        },
        {
          name: "Progress Bar",
          url: "/base/progress-bar",
          icon: "icon-puzzle"
        },
        {
          name: "Switches",
          url: "/base/switches",
          icon: "icon-puzzle"
        },
        {
          name: "Tabs",
          url: "/base/tabs",
          icon: "icon-puzzle"
        },
        {
          name: "Tooltips",
          url: "/base/tooltips",
          icon: "icon-puzzle"
        }
      ]
    },
    {
      role: "theme",
      name: "Buttons",
      url: "/buttons",
      icon: "icon-cursor",
      children: [
        {
          name: "Buttons",
          url: "/buttons/buttons",
          icon: "icon-cursor"
        },
        {
          name: "Button dropdowns",
          url: "/buttons/button-dropdowns",
          icon: "icon-cursor"
        },
        {
          name: "Button groups",
          url: "/buttons/button-groups",
          icon: "icon-cursor"
        },
        {
          name: "Loading Buttons",
          url: "/buttons/loading-buttons",
          icon: "icon-cursor"
        },
        {
          name: "Social Buttons",
          url: "/buttons/social-buttons",
          icon: "icon-cursor"
        }
      ]
    },
    {
      role: "theme",
      name: "Charts",
      url: "/charts",
      icon: "icon-pie-chart"
    },
    {
      role: "theme",
      name: "Editors",
      url: "/editors",
      icon: "fa fa-code",
      children: [
        {
          name: "Text Editors",
          url: "/editors/text-editors",
          icon: "icon-note"
        },
        {
          name: "Code Editors",
          url: "/editors/code-editors",
          icon: "fa fa-code"
        }
      ]
    },
    {
      role: "theme",
      name: "Forms",
      url: "/forms",
      icon: "icon-note",
      children: [
        {
          name: "Basic Forms",
          url: "/forms/basic-forms",
          icon: "icon-note"
        },
        {
          name: "Advanced Forms",
          url: "/forms/advanced-forms",
          icon: "icon-note"
        }
      ]
    },
    {
      role: "theme",
      name: "Google Maps",
      url: "/google-maps",
      icon: "icon-map",
      badge: {
        variant: "info",
        text: "NEW"
      }
    },
    {
      role: "theme",
      name: "Icons",
      url: "/icons",
      icon: "icon-star",
      children: [
        {
          name: "Flags",
          url: "/icons/flags",
          icon: "icon-star",
          badge: {
            variant: "success",
            text: "NEW"
          }
        },
        {
          name: "Font Awesome",
          url: "/icons/font-awesome",
          icon: "icon-star",
          badge: {
            variant: "secondary",
            text: "4.7"
          }
        },
        {
          name: "Simple Line Icons",
          url: "/icons/simple-line-icons",
          icon: "icon-star"
        }
      ]
    },
    {
      role: "theme",
      name: "Notifications",
      url: "/notifications",
      icon: "icon-bell",
      children: [
        {
          name: "Alerts",
          url: "/notifications/alerts",
          icon: "icon-bell"
        },
        {
          name: "Badges",
          url: "/notifications/badges",
          icon: "icon-bell"
        },
        {
          name: "Modals",
          url: "/notifications/modals",
          icon: "icon-bell"
        },
        {
          name: "Toastr",
          url: "/notifications/toastr",
          icon: "icon-bell"
        }
      ]
    },
    {
      role: "theme",
      name: "Plugins",
      url: "/plugins",
      icon: "icon-energy",
      children: [
        {
          name: "Calendar",
          url: "/plugins/calendar",
          icon: "icon-calendar"
        },
        {
          name: "Spinners",
          url: "/plugins/spinners",
          icon: "fa fa-spinner"
        }
      ]
    },
    {
      role: "theme",
      name: "Tables",
      url: "/tables",
      icon: "icon-list",
      children: [
        {
          name: "DataTable",
          url: "/tables/datatable",
          icon: "icon-list"
        },
        {
          name: "Tables",
          url: "/tables/tables",
          icon: "icon-list"
        }
      ]
    },
    {
      role: "theme",
      name: "Widgets",
      url: "/widgets",
      icon: "icon-calculator",
      badge: {
        variant: "info",
        text: "NEW"
      }
    },
    {
      role: "theme",
      name: "Pages",
      url: "/pages",
      icon: "icon-star",
      children: [
        {
          name: "Login",
          url: "/login",
          icon: "icon-star"
        },
        {
          name: "Register",
          url: "/register",
          icon: "icon-star"
        },
        {
          name: "Error 404",
          url: "/404",
          icon: "icon-star"
        },
        {
          name: "Error 500",
          url: "/500",
          icon: "icon-star"
        }
      ]
    },
    {
      role: "theme",
      name: "UI Kits",
      url: "/ui-kits",
      icon: "icon-layers",
      children: [
        {
          name: "Invoicing",
          url: "/ui-kits/invoicing",
          icon: "icon-speech",
          children: [
            {
              name: "Invoice",
              url: "/ui-kits/invoicing/invoice",
              icon: "icon-speech"
            }
          ]
        },
        {
          name: "Email",
          url: "/ui-kits/email",
          icon: "icon-speech",
          children: [
            {
              name: "Inbox",
              url: "/ui-kits/email/inbox",
              icon: "icon-speech"
            },
            {
              name: "Message",
              url: "/ui-kits/email/message",
              icon: "icon-speech"
            },
            {
              name: "Compose",
              url: "/ui-kits/email/compose",
              icon: "icon-speech"
            }
          ]
        }
      ]
    }
  ]
};
