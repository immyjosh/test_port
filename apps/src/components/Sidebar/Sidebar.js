/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Badge, Nav, NavItem, NavLink as RsNavLink } from "reactstrap";
import classNames from "classnames";
import nav from "./_nav";
import SidebarFooter from "./../SidebarFooter";
import SidebarForm from "./../SidebarForm";
import SidebarHeader from "./../SidebarHeader";
import SidebarMinimizer from "./../SidebarMinimizer";
import { Switch, Route, Redirect, BrowserRouter } from "react-router-dom";
import jwt_decode from "jwt-decode";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.activeRoute = this.activeRoute.bind(this);
    this.hideMobile = this.hideMobile.bind(this);
    this.state = {
      token: "",
      tokenAgency: "",
      tokenClient: "",
      unauthredirect: false,
      unauthredirectA: false,
      unauthredirectC: false,
      user: "",
      url: ""
    };
  }
  componentDidMount() {
    const tokens = localStorage.getItem("APUS");
    const tokensAgency = localStorage.getItem("APUSA");
    const tokensClient = localStorage.getItem("APUSC");
    let token;
    let tokenAgency;
    let tokenClient;
    if (tokens) {
      token = jwt_decode(tokens);
    }
    if (tokensAgency) {
      tokenAgency = jwt_decode(tokensAgency);
    }
    if (tokensClient) {
      tokenClient = jwt_decode(tokensClient);
    }
    if (!token && !tokenAgency && !tokenClient) {
      this.setState({
        unauthredirect: true
      });
    }
    if (!token && this.props.location.pathname.slice(0, 7) === "/admin/") {
      this.setState({
        unauthredirect: true
      });
    }
    if (!tokenAgency && this.props.location.pathname.slice(0, 8) === "/agency/") {
      this.setState({
        unauthredirectA: true
      });
    }
    if (!tokenClient && this.props.location.pathname.slice(0, 8) === "/client/") {
      this.setState({
        unauthredirectC: true
      });
    }
    this.setState({
      token,
      tokenAgency,
      tokenClient
    });
    if (token && (this.props.location.pathname.slice(0, 7) === "/admin/" && token.role === "admin")) {
      this.setState({
        user: token
      });
    }
    if (tokenAgency && (this.props.location.pathname.slice(0, 8) === "/agency/" && tokenAgency.role === "agency")) {
      this.setState({
        user: tokenAgency
      });
    }
    if (tokenClient && (this.props.location.pathname.slice(0, 8) === "/client/" && tokenClient.role === "client")) {
      this.setState({
        user: tokenClient
      });
    }
  }
  // handleClick(e) {
  //   e.preventDefault();
  //   var elems = document.querySelectorAll("nav ul.nav li.nav-dropdown");

  //   if (e.target.parentElement.classList[2] === "open") {
  //     [].forEach.call(elems, function(el) {
  //       el.classList.remove("open");
  //     });
  //     e.target.parentElement.classList.remove("open");
  //   } else {
  //     elems = document.querySelectorAll("nav ul.nav li.nav-dropdown");
  //     [].forEach.call(elems, function(el) {
  //       el.classList.remove("open");
  //     });
  //     e.target.parentElement.classList.toggle("open");
  //   }
  // }
  // handleClick(e) {
  //   e.preventDefault();
  //   var elems = document.querySelectorAll("nav ul.nav li.nav-dropdown");
  //   [].forEach.call(elems, function(el) {
  //     el.classList.remove("open");
  //   });
  //   e.target.parentElement.classList.toggle("open");
  // }
  handleClick(e) {
    e.preventDefault();
    let elems = document.querySelectorAll("nav ul.nav li.nav-dropdown");
    if (e.target.parentElement.classList[2] === "open") {
      // [].forEach.call(elems, function(el) {
      //   el.classList.remove("open");
      // });
      e.target.parentElement.classList.remove("open");
    } else {
      elems = document.querySelectorAll("nav ul.nav li.nav-dropdown");
      [].forEach.call(elems, function(el) {
        // el.classList.remove("open");
      });
      e.target.parentElement.classList.toggle("open");
    }
  }
  // handleClick(e) {
  //   e.preventDefault();
  //   e.target.parentElement.classList.toggle("open");
  // }

  activeRoute(routeName, props) {
    const getrouteurl = [];
    routeName.children.map((item, i) => {
      const navurl = item.url.toString().split("/");
      const navurldata = "/" + navurl[2];
      const routeurl = this.props.location.pathname.toString().split("/");
      const routeurldata = "/" + routeurl[2];
      if (routeurldata === navurldata) {
        const pushurl = navurldata;
        getrouteurl.push(pushurl);
      }
      return true;
    });
    return getrouteurl.length === 1 ? "nav-item nav-dropdown open" : "nav-item nav-dropdown";
  }

  hideMobile(e) {
    const checkClass = e.target.parentElement.parentElement.getAttribute("class");
    if (checkClass !== "nav-dropdown-items") {
      const elems = document.querySelectorAll("nav ul.nav li.nav-dropdown");
      [].forEach.call(elems, function(el) {
        el.classList.remove("open");
      });
    }

    if (document.body.classList.contains("sidebar-mobile-show")) {
      document.body.classList.toggle("sidebar-mobile-show");
    }
  }

  // todo Sidebar nav secondLevel
  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  render() {
    if (this.state.unauthredirect) {
      return <Redirect to="/" />;
    }
    if (this.state.unauthredirectA) {
      return <Redirect to="/" />;
    }
    if (this.state.unauthredirectC) {
      return <Redirect to="/" />;
    }
    const props = this.props;

    // badge addon to NavItem
    const badge = badge => {
      if (badge) {
        const classes = classNames(badge.class);
        return (
          <Badge className={classes} color={badge.variant}>
            {badge.text}
          </Badge>
        );
      }
    };

    // simple wrapper for nav-title item
    const wrapper = item => {
      return item.wrapper && item.wrapper.element ? React.createElement(item.wrapper.element, item.wrapper.attributes, item.name) : item.name;
    };

    // nav list section title
    const title = (title, key) => {
      const classes = classNames("nav-title", title.class);
      return (
        <li key={key} className={classes}>
          {wrapper(title)}{" "}
        </li>
      );
    };

    // nav list divider
    const divider = (divider, key) => {
      const classes = classNames("divider", divider.class);
      return <li key={key} className={classes} />;
    };

    // nav label with nav link
    const navLabel = (item, key) => {
      const classes = {
        item: classNames("hidden-cn", item.class),
        link: classNames("nav-label", item.class ? item.class : ""),
        icon: classNames(!item.icon ? "fa fa-circle" : item.icon, item.label.variant ? `text-${item.label.variant}` : "", item.label.class ? item.label.class : "")
      };
      return navLink(item, key, classes);
    };

    // nav item with nav link
    const navItem = (item, key) => {
      const classes = {
        item: classNames(item.class),
        link: classNames("nav-link", item.variant ? `nav-link-${item.variant}` : ""),
        icon: classNames(item.icon)
      };
      return navLink(item, key, classes);
    };

    // nav link
    const navLink = (item, key, classes) => {
      const url = item.url ? item.url : "";
      return (
        <NavItem key={key} className={classes.item}>
          {isExternal(url) ? (
            <RsNavLink href={url} className={classes.link} active>
              <i className={classes.icon} />
              {item.name}
              {badge(item.badge)}
            </RsNavLink>
          ) : (
            <NavLink to={url} className={classes.link} activeClassName="active" onClick={this.hideMobile}>
              <i className={classes.icon} />
              {item.name}
              {badge(item.badge)}
            </NavLink>
          )}
        </NavItem>
      );
    };

    // nav dropdown
    const navDropdown = (item, key) => {
      return (
        <li key={key} className={this.activeRoute(item, props)}>
          <button className="nav-link nav-dropdown-toggle" onClick={this.handleClick}>
            <i className={item.icon} />
            {item.name}
          </button>
          <ul className="nav-dropdown-items">{navList(item.children)}</ul>
        </li>
      );
    };

    // nav type
    const navType = (item, idx) =>
      item.title ? title(item, idx) : item.divider ? divider(item, idx) : item.label ? navLabel(item, idx) : item.children ? navDropdown(item, idx) : navItem(item, idx);

    // nav list
    const navList = items => {
      return items.map((item, index) => navType(item, index));
    };

    const isExternal = url => {
      const link = url ? url.substring(0, 4) : "";
      return link === "http";
    };
    // const navListCondition = items => {
    //   return items.filter(item => item.role === this.state.user.role);
    // };
    const navListCondition = items => {
      if (this.state.user.subscription === 0 && this.state.user.role === "agency") {
        return items.filter(item => item.sub === 0 && item.role === "agency");
      } else {
        if (this.state.user.role === "agency") {
          if (this.state.user.recruitment_module === 1) {
            return items.filter(item => (item.recruitment_module === 1 || item.recruitment_module === 0) && item.role === this.state.user.role);
          } else {
            return items.filter(item => item.recruitment_module === 0 && item.role === this.state.user.role);
          }
        } else {
          return items.filter(item => item.role === this.state.user.role);
        }
      }
    };
    const newBar = navListCondition(nav.items);
    // sidebar-nav root
    return (
      <div className="sidebar">
        <SidebarHeader />
        <SidebarForm />
        <nav className={this.state.user.role === "client" ? "sidebar-nav" : "sidebar-nav"}>
          <Nav>{navList(newBar)}</Nav>
        </nav>
        <SidebarFooter />
        <SidebarMinimizer />
      </div>
    );
  }
}

export default Sidebar;
