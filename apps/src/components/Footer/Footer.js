import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span className="ml-auto">&copy; 2019 Agency Portal</span>
      </footer>
    );
  }
}

export default Footer;
