import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import moment from "moment";
import { Row, Col, Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import Widget02 from "../../Template/Widgets/Widget02";
import request from "../../../api/api";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "120px"
};

class Subscriberslist extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    earnings: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    dashboardcounts: "",
    tableOptions: {
      payment_status: "",
      type: "",
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0
    }
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/agency");
    }
    this.populateData();
  }
  // editpage = id => {
  //   return this.props.history.push({
  //     pathname: "/admin/editsubscription",
  //     state: { rowid: id }
  //   });
  // };

  ClickableTabs = type => {
    if (type === "payment") {
      this.setState(state => {
        state.tableOptions.payment_status = 1;
        state.tableOptions.type = "";
        this.populateData();
      });
    } else if (type === "subscription") {
      this.setState(state => {
        state.tableOptions.type = type;
        state.tableOptions.payment_status = "";
        this.populateData();
      });
    } else if (type === "trial") {
      this.setState(state => {
        state.tableOptions.type = type;
        state.tableOptions.payment_status = "";
        this.populateData();
      });
    } else {
      this.setState(state => {
        state.tableOptions.type = "";
        state.tableOptions.payment_status = "";
        this.populateData();
      });
    }
  };

  populateData() {
    request({
      url: "/administrators/plans/subscribers",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          dashboardcounts: res.response.dashboardcounts[0] || {},
          earnings: res.response.earnings,
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["agency_data.company_name","agency_data.name", "plan.name", "agency_data.phone.number", "plan.amount", "type", "start", "end", "payment_status"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Company") {
        state.tableOptions.filter = "company_name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/plans/subscribersexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("New Subscription Added");
  }
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row className="sub-ranges" >
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs("payment")} header={this.state.earnings ? <Fragment>£ {this.state.earnings}</Fragment> : "0"} mainText="Earnings" icon="fa fa-money" color="primary" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02
              className="cursor-pointer"
              onClick={() => this.ClickableTabs("subscription")}
              header={this.state.dashboardcounts.subscription ? this.state.dashboardcounts.subscription : "0"}
              mainText="SubScriptions"
              icon="fa fa-users"
              color="info"
            />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs("trial")} header={this.state.dashboardcounts.trial ? this.state.dashboardcounts.trial : "0"} mainText="Trial" icon="fa fa-user-o" color="warning" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            {/* <Widget02
              header={0}
              mainText="Income"
              icon="fa fa-users"
              color="danger"
            /> */}
          </Col>
        </Row>
        <Card>
          {/* <CardHeader>
            <i className="icon-list" />Subscribers List
          </CardHeader> */}
          <CardHeader>
            <i className="icon-list" />
            <span className="cursor-pointer" onClick={() => this.ClickableTabs("")}>
              Subscribers List
            </span>
            <div className="card-actions" style={tStyle} onClick={this.export}>
              <button style={tStyles}>
                <i className="fa fa-upload" /> Export
                {/*<small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
             {/* <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Company</option>
                </Input>
              </div>*/}
              <div className="col-lg-12">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search..." name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive size="sm">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("agency_data.company_name");
                      }}
                    >
                      Company <i style={{ paddingLeft: "25px" }} className={sorticondef} id="agency_data.company_name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("agency_data.name");
                      }}
                    >
                      Contact <i style={{ paddingLeft: "25px" }} className={sorticondef} id="agency_data.name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("agency_data.phone.number");
                      }}
                    >
                      Tel <i style={{ paddingLeft: "25px" }} className={sorticondef} id="agency_data.phone.number" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("plan.name");
                      }}
                    >
                      Plan <i style={{ paddingLeft: "25px" }} className={sorticondef} id="plan.name" />
                    </th>
                    <th
                        onClick={() => {
                          this.sort("type");
                        }}
                    >
                      Type <i style={{ paddingLeft: "25px" }} className={sorticondef} id="type" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("plan.amount");
                      }}
                    >
                      Amount <i style={{ paddingLeft: "25px" }} className={sorticondef} id="plan.amount" />
                    </th>

                    <th
                      onClick={() => {
                        this.sort("start");
                      }}
                    >
                      Start Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("end");
                      }}
                    >
                      End Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="end" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("end");
                      }}
                    >
                      Renewal Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="renewal" />
                    </th>

                    <th
                      onClick={() => {
                        this.sort("payment_status");
                      }}
                    >
                      Amount Payment Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="payment_status" />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.agency_data && item.agency_data.company_name}</td>
                        <td><p className={"mb-0"}>{item.agency_data && item.agency_data.name}</p><small className="total-amount">{item.agency_data && item.agency_data.email}</small></td>
                        <td>{item.agency_data.phone && item.agency_data.phone.code} - {item.agency_data.phone && item.agency_data.phone.number}</td>
                        <td>{item.plan.name}</td>
                        <td>{item.type}</td>
                        <td className="total-amount">£ {item.plan.amount}</td>
                        <td>{moment(item.start).format("DD-MM-YYYY")}</td>
                        <td>{moment(item.end).format("DD-MM-YYYY")}</td>
                        <td>{moment(item.end).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.payment_status === 1 ? <Badge color="success">Paid</Badge> : null}
                          {item.payment_status === 0 ? <Badge color="danger">Trial</Badge> : null}
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={14}>
                        <h5>No record available</h5>
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              {/*<Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Subscriberslist;
