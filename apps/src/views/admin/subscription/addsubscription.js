/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Input } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import request from "../../../api/api";
import addnodify from "./subscriptionlist";

class Addsubscription extends Component {
  state = {
    name: "",
    freedays: 0,
    noofdays: 0,
    employees: 0,
    amount: 0,
    description: "",
    remainder: 0,
    status: "",
    recruitment_module: "",
    availability: ""
  };
  flash = new addnodify();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
  }
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      recruitment_module: value
    });
  };
  availableChange = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      availability: value
    });
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    request({
      url: "/administrators/plans/save",
      method: "POST",
      data: {
        name: this.state.name,
        freedays: this.state.freedays,
        noofdays: this.state.noofdays,
        employees: this.state.employees,
        description: this.state.description,
        amount: this.state.amount,
        remainder: this.state.remainder,
        recruitment_module: this.state.recruitment_module,
        availability: this.state.availability,
        status: 1
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.saveuser();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(function(error) {});
  };
  saveuser = () => {
    return this.props.history.push("/admin/subscriptionlist"), this.flash.adminsavenodify(), this.form && this.form.reset();
  };
  render() {
    let totaldays = parseInt(this.state.freedays, Number) + parseInt(this.state.noofdays, Number) || 0;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardHeader>
                  <strong>Add</strong> Subscription
                </CardHeader>
                <CardBody>
                  {/* <p className="h5">Basic Information</p> */}
                  <Row> 
				   <Col xs="12" md="12">
				    <div className="cus-design edited-section">
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Subscription Name</Label>
                        <AvInput type="text" label="Subscription Name" name="name" placeholder="Enter Subscription Name" onChange={this.onChange} minLength="4" maxLength="30" value={this.state.name} message="error" required autoFocus />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>
                          Trial Period <small>(In Days)</small>
                        </Label>
                        <AvInput type="number" name="freedays" placeholder="Enter Trial Period" onChange={this.onChange} value={this.state.freedays} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
                    <Col xs="12" md="4">
                      <Row>
                        <Col xs="12" md="12">
                          <AvGroup>
                            <Label>
                              Billing Period
                            </Label>
                            <AvField type="select" name="noofdays" onChange={this.onChange} value={this.state.noofdays} required>
                              <option>Please Select</option>
                              <option value={30}>Monthly</option>
                              <option value={365}>Yearly</option>
                          </AvField>
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="12">
                          <p className="total-period">
                            Total Period <small>(In Days)</small>
                            <span className="total-days">{totaldays}</span>
                          </p>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Maximum Employees count for Plan</Label>
                        <AvInput type="number" min="0" name="employees" placeholder="Employee count" value={this.state.employees} onChange={this.onChange} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Amount (£)</Label>
                        <AvInput type="number" name="amount" placeholder="Amount" onChange={this.onChange} value={this.state.amount} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>
                          Remainder <small>(In Days)</small>
                        </Label>
                        <AvInput type="number" name="remainder" placeholder="Day" onChange={this.onChange} value={this.state.remainder} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="4">
                      <div>
                        <label>Recruitment Module</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.recruitment_module} onChange={this.statusChangeses} />
                        <span className="switch-label" data-on="Yes" data-off="No" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  
                    <Col xs="12" md="4">
                      <div>
                        <label>Available to All</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.availability} onChange={this.availableChange} />
                        <span className="switch-label" data-on="Yes" data-off="No" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
					
					   <Col xs="12" md="12">
                      <AvGroup>
                        <Label>Description</Label>
                        <AvInput type="textarea" label="Description " name="description" placeholder="Enter details" onChange={this.onChange} minLength="4" maxLength="100" value={this.state.description} message="error" required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="success pull-right mb-3" title="Add New Subscription">
                    Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addsubscription;
