import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Widget02 from "../../Template/Widgets/Widget02";
import { Row, Col, Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import Loader from "../../common/loader";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
import moment from "moment";

const tStyle = {
  cursor: "pointer"
};

class Subscriptionlist extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    isLoader: false,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      status: "",
      limit: 10,
      skip: 0
    },
    dashboardCount: {}
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    request({
      url: "/administrators/plans/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res) {
        this.setState({
          isLoader: false,
          adminlist: res.response.result,
          dashboardCount: res.response.dashboard[0] || {},
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  ClickableTabs = (status) => {
    if (status) {
      this.setState(state => {
        state.tableOptions.status = status;
        this.populateData();
      })
    } else if (status === 0) {
      this.setState(state => {
        state.tableOptions.status = status;
        this.populateData();
      })
    } else {
      this.setState(state => {
        state.tableOptions.status = "";
        this.populateData();
      })
    }
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/admin/editsubscription",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/administrators/plans/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (!res.response.result && !res.response.fullcount) {
        this.setState({ adminlist: [], pages: "" });
      } else {
        this.setState({
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["noofdays", "freedays", "name", "createdAt", "status", "employees", "amount", "recruitment_module"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Free days") {
        state.tableOptions.filter = "freedays";
      } else if (value === "No of days") {
        state.tableOptions.filter = "noofdays";
      } else if (value === "Amount") {
        state.tableOptions.filter = "amount";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("Saved!");
  }

  export() {
    request({
      url: "/administrators/plans/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <Row className="sub-ranges">
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs("")} header={this.state.dashboardCount? (this.state.dashboardCount.active ? this.state.dashboardCount.active : 0) + (this.state.dashboardCount.inactive ? this.state.dashboardCount.inactive : 0): 0} mainText="Total Susbcriptions" icon="fa fa-users" color="primary" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs(1)} header={this.state.dashboardCount.active ? this.state.dashboardCount.active : "0"} mainText="Active Susbcriptions" icon="fa fa-users" color="success" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs(0)} header={this.state.dashboardCount.inactive ? this.state.dashboardCount.inactive : "0"} mainText="Inactive Susbcriptions" icon="fa fa-user-secret" color="secondary" />
          </Col>
        </Row>
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            <span className="cursor-pointer" onClick={() => this.ClickableTabs("")}>Subscriptions List</span>
            <div className="card-actions" style={tStyle}>
              <button onClick={()=>this.props.history.push("/admin/addsubscription")}>
                <i className="fa fa-plus" /> Add New
              </button>
              <button onClick={this.export}>
                <i className="fa fa-upload" /> Export
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
             {/* <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Name</option>
                   <option>Free days</option>
                   <option>No of days</option>
                   <option>Amount</option>
                </Input>
              </div>*/}
              <div className="col-lg-12">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive size="sm">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("freedays");
                      }}
                    >
                      Trial Period <i style={{ paddingLeft: "25px" }} className={sorticondef} id="freedays" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("noofdays");
                      }}
                    >
                      Billing Period <i style={{ paddingLeft: "25px" }} className={sorticondef} id="noofdays" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("employees");
                      }}
                    >
                      Employee Limit <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employees" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("recruitment_module");
                      }}
                    >
                      Recruitment Module <i style={{ paddingLeft: "25px" }} className={sorticondef} id="recruitment_module" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("amount");
                      }}
                    >
                      Amount <i style={{ paddingLeft: "25px" }} className={sorticondef} id="amount" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("amount");
                      }}
                    >
                      Creadted Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>
                          {item.freedays} <small>Days</small>
                        </td>
                        <td>
                          {item.noofdays=== 30? "Monthly":item.noofdays=== 365? "Yearly":""}
                        </td>
                        <td>{item.employees}</td>
                        <td>{item.recruitment_module === 1 ? "Yes" : "No"}</td>
                        <td className="total-amount">£ {item.amount}</td>
                        <td>{moment(item.createdAt).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                        </td>
                        <td>
                          <button type="button" title="Edit" className="btn table-edit" color="info" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                            <i className="fa fa-edit" />
                          </button>
                          <UncontrolledTooltip placement="top" target={`edit${i}`}>
                            Edit
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              {/*<Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Subscriptionlist;
