/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, CardBody, CardFooter, Input, CardHeader, Col, Label, Row } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput , AvField} from "availity-reactstrap-validation";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";

class Editsubscription extends Component {
  state = {
    name: "",
    freedays: "",
    noofdays: "",
    employees: "",
    amount: "",
    description: "",
    status: "",
    remainder: "",
    stausactive: false,
    subscriptionlist: [],
    recruitment_module: "",
      availability: "",
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
    availableChange = e => {
        const value = e.target.checked ? 1 : 0;
        this.setState({
            availability: value
        });
    };
  statusChangeses1 = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      recruitment_module: value
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/plans/get",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ adminlist: res.response.result[0] });
      this.setState({
        name: this.state.adminlist.name,
        freedays: this.state.adminlist.freedays,
        noofdays: this.state.adminlist.noofdays,
        employees: this.state.adminlist.employees,
        amount: this.state.adminlist.amount,
        description: this.state.adminlist.description,
        remainder: this.state.adminlist.remainder,
        recruitment_module: this.state.adminlist.recruitment_module,
          availability: this.state.adminlist.availability,
        status: this.state.adminlist.status
      });
      if (this.state.adminlist.status === 1) {
        this.setState({
          stausactive: true
        });
      } else {
        this.setState({
          stausactive: false
        });
      }
    });
  }
  onEditAdmin = (e, values) => {
    e.persist();
    request({
      url: "/administrators/plans/save",
      method: "POST",
      data: {
        _id: this.props.location.state.rowid,
        name: this.state.name,
        freedays: this.state.freedays,
        noofdays: this.state.noofdays,
        employees: this.state.employees,
        amount: this.state.amount,
        description: this.state.description,
        remainder: this.state.remainder,
        recruitment_module: this.state.recruitment_module,
          availability: this.state.availability,
        status: this.state.status
      }
    }).then(res => {
      if (res.status === 1) {
        this.editnodify();
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  toastId = "SubscriptionEdit";
  editnodify() {
    if (!toast.isActive(this.toastId)) {
      this.toastId = toast.success("Updated");
    }
    this.componentDidMount();
  }
  render() {
    console.log(this.state.noofdays)
    const totaldays = parseInt(this.state.freedays, Number) + parseInt(this.state.noofdays, Number) || 0;
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <strong>Edit</strong> Subscription
                </CardHeader>
                <CardBody>
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Subscription Name</Label>
                        <AvInput
                          type="text"
                          label="Subscription Name"
                          name="name"
                          placeholder="Enter Subscription Name"
                          onChange={this.onChange}
                          minLength="4"
                          maxLength="30"
                          value={this.state.name}
                          message="error"
                          required
                          autoFocus
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>
                          Trial Period <small>(In Days)</small>
                        </Label>
                        <AvInput type="number" min="0" name="freedays" placeholder="Enter Trial Period" onChange={this.onChange} value={this.state.freedays} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
                    <Col xs="12" md="4">
                      <Row>
                        <Col xs="12" md="12">
                          <AvGroup>
                            <Label>
                              Billing Period
                            </Label>
                            {/* <AvInput type="number" min="0" name="noofdays" placeholder="Enter Billing Period" onChange={this.onChange} value={this.state.noofdays} required /> */}
                            <AvField type="select" name="noofdays" onChange={this.onChange} value={this.state.noofdays} required>
                              <option>Please Select</option>
                              <option value={30}>Monthly</option>
                              <option value={365}>Yearly</option>
                          </AvField>
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="12">
                          <p className="total-period">
                            {" "}
                            Total Period <small>(In Days)</small>
                            <span className="total-days">{totaldays}</span>
                          </p>
                        </Col>
						
                      </Row>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Maximum Employees count for Plan</Label>
                        <AvInput type="number" min="0" name="employees" placeholder="Employee count" value={this.state.employees} onChange={this.onChange} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Amount (£)</Label>
                        <AvInput type="number" min="0" name="amount" placeholder="Amount in £" onChange={this.onChange} value={this.state.amount} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>
                          Remainder in days <small>(In Days)</small>
                        </Label>
                        <AvInput type="number" name="remainder" placeholder="Day.." onChange={this.onChange} value={this.state.remainder} />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
                    <Col xs="12" md="4">
                      <div>
                        <label>Recruitment Module</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.recruitment_module} onChange={this.statusChangeses1} />
                        <span className="switch-label" data-on="Yes" data-off="No" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                      <Col xs="12" md="4">
                          <div>
                              <label>Available to All</label>
                          </div>
                          <Label className="switch switch-text switch-success new-switch">
                              <Input type="checkbox" className="switch-input" checked={this.state.availability} onChange={this.availableChange} />
                              <span className="switch-label" data-on="Yes" data-off="No" />
                              <span className="switch-handle new-handle" />
                          </Label>
                      </Col>
                    <Col xs="12" md="4">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
					<Col xs="12" md="12">
                      <AvGroup>
                        <Label>Description</Label>
                        <AvInput
                          type="textarea"
                          label="Description "
                          name="description"
                          placeholder="Enter details"
                          onChange={this.onChange}
                          minLength="4"
                          maxLength="100"
                          value={this.state.description}
                          message="error"
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i className="fa fa-dot-circle-o" /> Update
                  </Button>
                  <Link to="/admin/subscriptionlist">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editsubscription;
