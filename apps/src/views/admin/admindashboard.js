import React, { Component } from "react";
import { toast } from "react-toastify";
import { Card, CardBody, CardHeader, Col, Row, Table } from "reactstrap";
import request, { client } from "../../api/api";
import Widget02 from "../../views/Template/Widgets/Widget02";
import Loader from "../common/loader";

class Admindashboard extends Component {
  state = {
    admindashboard: "",
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    sortOrder: true,
    loading: false,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: ""
    },
    start_date: null,
    end_date: null
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (this.props.location.state) {
      if (this.props.location.state === true) {
        let toastId = "AdminLogin21";
        if (!toast.isActive(toastId)) {
          toastId = toast.success(`Welcome  ${token.username}`);
          this.props.history.push({ state: false });
        }
      }
    }

    client.defaults.headers.common["Authorization"] = this.props.token;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/agency/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          isLoader: false,
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/admindashboard",
      method: "post"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ admindashboard: res.response.result[0] });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        {/*<ToastContainer position="top-right" autoClose={2500} />*/}
        <Row className="sub-ranges">
          <Col xs="12" sm="6" lg="3">
            <Widget02
              onClick={() => {
                this.props.history.push("/admin/agencylist");
              }}
              header={this.state.admindashboard.agencies ? this.state.admindashboard.agencies : "0"}
              mainText="Agencies"
              icon="fa fa-user"
              color="primary"
              className="cursor-pointer"
            />
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Widget02
              header={this.state.admindashboard.clients ? this.state.admindashboard.clients : "0"}
              mainText="Clients"
              icon="fa fa-user-o"
              color="info"
              className="cursor-pointer"
            />
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Widget02 header={this.state.admindashboard.employees ? this.state.admindashboard.employees : "0"} mainText="Employees" icon="fa fa-user-md" color="warning" className="cursor-pointer"/>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Widget02
              onClick={() => {
                this.props.history.push("/admin/subadminlist");
              }}
              header={this.state.admindashboard.subadmin ? this.state.admindashboard.subadmin : "0"}
              mainText="Admin"
              icon="fa fa-users"
              color="danger"
              className="cursor-pointer"
            />
          </Col>
        </Row>
        <Row>
          <Col xs="12" md="12">
            <div className="animated">
              <Card>
                <CardHeader
                  onClick={() => {
                    this.props.history.push("/admin/agencylist");
                  }}
                  className="cursor-pointer"
                >
                  <i className="icon-list" />
                  Recent Agency List
                </CardHeader>
                <CardBody>
                  <div className="table-responsive mt-2">
                    <Table hover bordered responsive>
                      <thead>
                        <tr>
                          <th>S.No.</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Username</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.adminlist.length > 0 ? (
                          this.state.adminlist.map((item, i) => (
                            <tr key={item._id}>
                              <td>{this.state.tableOptions.skip + i + 1}</td>
                              <td>{item.name}</td>
                              <td>{item.email}</td>
                              <td>{item.username}</td>
                            </tr>
                          ))
                        ) : (
                          <tr className="text-center">
                            <td colSpan={9}>{!this.state.isLoader && <h6>No record available</h6>}</td>
                          </tr>
                        )}
                        {this.state.isLoader && <Loader />}
                      </tbody>
                    </Table>
                  </div>
                </CardBody>
              </Card>
            </div>
          </Col>
          <Col xs="12" md="12" />
        </Row>
      </div>
    );
  }
}

export default Admindashboard;
