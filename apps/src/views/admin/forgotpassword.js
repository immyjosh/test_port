import React, { Component } from "react";
import { Alert, Button, Card, CardBody, CardGroup, Col, Container, InputGroupAddon, InputGroupText, Row } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import { Link } from "react-router-dom";
import request from "../../api/api";

class adminforgotpassword extends Component {
  state = {
    email: "",
    mailalert: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    request({
      url: "/administrators/forgotpassword",
      method: "POST",
      data: { email: this.state.email }
    })
      .then(res => {
        this.setState({
          mailalert: true
        });
        setTimeout(() => {
          this.setState({
            mailalert: false
          });
        }, 3000);
      })
      .catch(err => {
        this.logerror();
      });
  };

  componentDidMount() {
    if (localStorage.getItem("notify")) {
      this.logoutmsg();
      localStorage.clear();
    }
  }

  logerror = () => {
    return toast.error("No User Found!");
  };

  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <CardBody>
                    <h1 className="login-infos" >Forgot Password</h1>
                    <AvForm onValidSubmit={this.OnFormSubmit}>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="text" name="email" placeholder="Enter email" onChange={this.onChange} value={this.state.email} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" type="submit">
                            Send
                          </Button>
                        </Col>
                        <Col xs="6" className="bac-log">
                          {" "}
                          <Button type="button" color="link" className="px-0 ">
                            <Link to="/admin">Back to Login</Link>
                          </Button>
                        </Col>
                      </Row>
                    </AvForm>
                    <Row>
                      <Col className="mt-3">{this.state.mailalert ? <Alert color="danger">Check your email!</Alert> : null}</Col>
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default adminforgotpassword;