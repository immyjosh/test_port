/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row
} from "reactstrap";
import {
  AvFeedback,
  AvField,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";

import request from "../../../api/api";

class Smtp extends Component {
  state = {
    smtphost: "",
    smtpport: "",
    smtpusername: "",
    smtppassword: "",
    mode: "",
    success: false,
    settingslist: []
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/settings/smtp",
      method: "POST"
    }).then(res => {      
      if (res.status === 1) {
        this.setState({ settingslist: res.response.settings });
        if(res.response.settings !==null){
           this.setState({
              smtphost: res.response.settings.smtp_host,
              smtpport: res.response.settings.smtp_port,
              smtpusername: res.response.settings.smtp_username,
              smtppassword: res.response.settings.smtp_password,
              mode: res.response.settings.mode
        });
        }
       
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    const settings = {
      
    };
    request({
      url: "/administrators/settings/smtp/save",
      method: "POST",
      data: {
         settings: {
            smtp_host: this.state.smtphost,
            smtp_port: this.state.smtpport,
            smtp_username: this.state.smtpusername,
            smtp_password: this.state.smtppassword,
            mode: this.state.mode
         }
      }
    }).then(response => {   
       if(response.status === 1){    
        this.savedsettings();
        this.componentDidMount();
       }
    });
  };
  savedsettings() {
    return toast.success("Saved!");
  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>SMTP</strong> Settings
                 {/* <div className="card-actions">
                    <a>
                      <i className="fa fa-cogs" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody>
				<Row>
				 <Col xs="12" md="12">
				  <div className="cus-design edited-section">
                  <Col xs="12" md="4">
                    <AvGroup>
                      <Label>SMTP Host</Label>
                      <AvInput
                        type="text"
                        label="SMTP Host"
                        name="smtphost"
                        placeholder="Enter SMTP Host"
                        onChange={this.onChange}
                        minLength="4"
                        maxLength="30"
                        value={this.state.smtphost}
                        message="error"
                        autoFocus
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="4">
                    <AvGroup>
                      <Label>SMTP Port</Label>
                      <AvInput
                        type="number"
                        name="smtpport"
                        placeholder="Enter SMTP Port"
                        onChange={this.onChange}                        
                        value={this.state.smtpport}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
				 
                  <Col xs="12" md="4">
                    <AvGroup>
                      <Label>SMTP Username</Label>
                      <AvInput
                        type="text"
                        name="smtpusername"
                        placeholder="Enter SMTP Username"
                        onChange={this.onChange}
                        value={this.state.smtpusername}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="4">
                    <AvGroup>
                      <Label>SMTP Password</Label>
                      <AvInput
                        type="password"
                        name="smtppassword"
                        placeholder="Enter SMTP Password"
                        onChange={this.onChange}
                        value={this.state.smtppassword}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
				  
                  <Col xs="12" md="4">
                    <AvField
                      type="select"
                      name="mode"
                      label="Mode"
                      placeholder="Select Mode"
                      onChange={this.onChange}
                      value={this.state.mode}
                    >
                      <option>Select</option>
                      <option>Development</option>
                      <option>Production</option>
                    </AvField>
                  </Col>
				  </div>
				  </Col>
				  </Row>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit" 
                    color="success pull-right mb-3"
                     title="Save"
                  >
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Smtp;
