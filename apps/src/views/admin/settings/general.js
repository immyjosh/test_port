/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, CustomInput, FormGroup } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import Select from "react-select";
import "react-select/dist/react-select.css";
import request, { NodeURL } from "../../../api/api";

class General extends Component {
  state = {
    sitetitle: "",
    email: "",
    siteurl: "",
    sitelocation: "",
    siteaddress: "",
    distanceby: "",
    billingcycle: "",
    logoName: "",
    logo: {},
    image: {},
    favicon: {},
    favName: "",
    timezones: [],
    success: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/timezones",
      method: "POST"
    })
      .then(res => {
        if (res) {
          const timezones = res.map((list, key) => ({
            value: list,
            label: list
          }));
          this.setState({ timezones });
        }
      })
      .catch(error => {
        return toast.error(error);
      });
    request({
      url: "/administrators/settings/general",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ settingslist: res.response.settings });
        if (this.state.settingslist) {
          this.setState({
            sitetitle: res.response.settings.site_title,
            email: res.response.settings.email_address,
            siteurl: res.response.settings.site_url,
            sitelocation: res.response.settings.location,
            siteaddress: res.response.settings.siteaddress,
            billingcycle: res.response.settings.billingcycle,
            timezone: res.response.settings.time_zone,
            distanceby: res.response.settings.distanceby,
            logo: res.response.settings.logo,
            logoName: res.response.settings.logo.substr(24, 30),
            favicon: res.response.settings.favicon,
            favName: res.response.settings.favicon.substr(24, 30)
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    const data = new FormData();
    data.append("setting[site_title]", this.state.sitetitle);
    data.append("setting[site_url]", this.state.siteurl);
    data.append("setting[location]", this.state.sitelocation);
    data.append("setting[siteaddress]", this.state.siteaddress);
    data.append("setting[email_address]", this.state.email);
    data.append("setting[billingcycle]", this.state.billingcycle);
    data.append("setting[distanceby]", this.state.distanceby);
    data.append("setting[time_zone]", this.state.timezone);
    data.append("logo", this.state.logo);
    data.append("favicon", this.state.favicon);
    request({
      url: "/administrators/settings/general/save",
      method: "POST",
      data: data
    })
      .then(response => {
        if (response.status === 1) {
          this.savedsettings();
          this.componentDidMount();
          window.location.reload(true);
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  };
  savedsettings() {
    return toast.success("Saved!");
  }
  logoChangedHandler = evt => {
    var file = evt.target.files[0];
    var lname = evt.target.files[0].name;
    this.setState({
      logo: file,
      logoName: lname
    });
    setTimeout(function() {}, 1000);
  };
  sendTimezone = value => {
    if (value) {
      this.setState({ timezone: value.value });
    }
  };
  faviconChangedHandler = evt => {
    var file = evt.target.files[0];
    var fname = evt.target.files[0].name;
    this.setState({
      favicon: file,
      favName: fname
    });
    setTimeout(function() {}, 1000);
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>General</strong> Settings
                </CardHeader>
                <CardBody>
                  <Row>
				  <Col xs="12" md="12">
				   <div className="cus-design edited-section">
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Site Title</Label>
                        <AvInput
                          type="text"
                          label="Site Tittle"
                          name="sitetitle"
                          placeholder="Enter Site Tittle"
                          onChange={this.onChange}
                          minLength="4"
                          maxLength="30"
                          value={this.state.sitetitle}
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>

                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Email</Label>
                        <AvInput type="email" name="email" placeholder="Enter Email" onChange={this.onChange} minLength="4" maxLength="50" value={this.state.email} />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
					 <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Site URL</Label>
                        <AvInput type="text" name="siteurl" placeholder="Enter Site URL" onChange={this.onChange} value={this.state.siteurl} />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					 <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Timesheet/Invoice Cycle</Label>
                        <AvInput
                          type="number"
                          min="6"
                          max="31"
                          label="Invoice Cycle"
                          name="billingcycle"
                          placeholder="Enter Invoice Cycle"
                          onChange={this.onChange}
                          value={this.state.billingcycle}
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
					 <Col xs="12" md="6">
                     
                        <Col xs="12" md="12">
                          <FormGroup>
                            <Label for="exampleCustomFileBrowser">Site Logo</Label>
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser"
                              name="logo"
                              onChange={this.logoChangedHandler}
                              label={this.state.logoName ? this.state.logoName : "Choose Logo"}
                            />
                          </FormGroup>
                        </Col>
                        <Col xs="6" md="12" className="mt-4 logos_heres">
                          {this.state.logo ? (
                            <img width="160px" height="60px" src={NodeURL + "/" + this.state.logo} alt="Profile" />
                          ) : (
                            <img width="140px" height="60px" src={"../../../img/avatars/sample.png"} alt="profile" />
                          )}
                        </Col>
						</Col>
                       <Col xs="12" md="6">
                        <Col xs="12" md="12">
                          <FormGroup>
                            <Label for="exampleCustomFileBrowser">Fav Icon</Label>
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser"
                              name="favicon"
                              onChange={this.faviconChangedHandler}
                              label={this.state.favName ? this.state.favName : "Choose Logo"}
                            />
                          </FormGroup>
                        </Col>
                        <Col xs="6" md="12" className="mt-4 logos_heres">
                          {this.state.favicon ? (
                            <img width="60px" height="60px" src={NodeURL + "/" + this.state.favicon} alt="Profile" />
                          ) : (
                            <img width="60px" height="60px" src={"../../../img/avatars/sample.png"} alt="profile" />
                          )}
                        </Col>
                     
                    </Col>
					
					 <Col xs="12" md="12">
                      <AvGroup>
                        <Label>Site Address</Label>
                        <AvInput type="textarea" name="siteaddress" placeholder="Enter Site Address" onChange={this.onChange} value={this.state.siteaddress} />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  </div>
				   </Col>
                  </Row>
				  
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="success pull-right mb-3" title="Save">
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default General;
