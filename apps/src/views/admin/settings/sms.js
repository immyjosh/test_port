/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row
} from "reactstrap";
import {
  AvFeedback,
  AvField,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";

import request from "../../../api/api"; 
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
class Sms extends Component {
  state = {
    smsaccount_id: "",
    smsauthentication_token: "",
    code: "",
    default_phone_number: "",
    smsmode: "",
    success: false,
    settingslist: []
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    
    request({
      url: "/administrators/settings/sms",
      method: "POST"
    }).then(res => {           
      if (res.status === 1) {
        this.setState({ settingslist: res.response.settings });
        if(res.response.settings.twilio !==null){            
           this.setState({              
              smsaccount_id: res.response.settings.twilio.account_sid,
              smsauthentication_token: res.response.settings.twilio.authentication_token,              
              default_phone_number: res.response.settings.twilio.default_phone_number,
              smsmode: res.response.settings.twilio.mode
        });
        }      
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    const settings = {
      
    };    
     if (this.state.number === "") {
      this.setState({
        phoneerror: true      });
    } else {
    request({
      url: "/administrators/settings/sms/save",
      method: "POST",
      data: {
         settings: {
           twilio : {
            account_sid: this.state.smsaccount_id,
            authentication_token: this.state.smsauthentication_token,           
            default_phone_number: this.state.default_phone_number,  
            mode: this.state.smsmode      
           }      
         }
      }
    }).then(response => {   
       if(response.status === 1){    
        this.savedsettings();
        this.componentDidMount();
       }
    });
    }
  };
  savedsettings() {
    return toast.success("Saved!");
  }
handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  render() {       
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>SMS</strong> Settings
                {/*  <div className="card-actions">
                    <a>
                      <i className="fa fa-cogs" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody> 
                 <Row>	
				  <Col xs="12" md="12">
                   <div className="cus-design edited-section">				 
                  <Col xs="12" md="4">
                    <AvGroup>
                      <Label>Account Id</Label>
                      <AvInput
                        type="text"
                        label="Account Id"
                        name="smsaccount_id"
                        placeholder="Enter Account Id"
                        onChange={this.onChange}                       
                        value={this.state.smsaccount_id}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="4">
                    <AvGroup>
                      <Label>Authentication Token</Label>
                      <AvInput
                        type="text"
                        name="smsauthentication_token"
                        placeholder="Enter Authentication Token"
                        onChange={this.onChange}                        
                        value={this.state.smsauthentication_token}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                   <Col xs="12" md="4">
                   <AvGroup>
                      <Label>Phone No.</Label>
                      <AvInput
                        type="text"
                        name="default_phone_number"
                        placeholder="Enter Phone No."
                        onChange={this.onChange}                        
                        value={this.state.default_phone_number}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                    </Col>
                  <Col xs="12" md="4">
                    <AvField
                      type="select"
                      name="smsmode"
                      label="Mode"
                      placeholder="Select Mode"
                      onChange={this.onChange}
                      value={this.state.smsmode}                      
                    >
                      <option>Select</option>
                      <option>Development</option>
                      <option>Production</option>
                    </AvField>
                  </Col>
				  </div>
				  </Col>
				 </Row> 
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    onClick={() => {
                      this.phonefield(this.state.number);
                    }}
                     title="Save"
                  >
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Sms;
