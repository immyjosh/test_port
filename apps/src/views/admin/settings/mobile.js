/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row,
  FormGroup,
  CustomInput
} from "reactstrap";
import {
  AvFeedback,
  AvField,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";

import request from "../../../api/api";

class Mobile extends Component {
  state = {
    user_bundle_id: "",
    tasker_bundle_id: "",
    mode: "",
    user_gcm_api_key: "",  
    tasker_gcm_api_key:"", 
    tasker_pem_files: "",
    user_pem_files: "",
    success: false,
    settingslist: []
   
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/settings/mobile",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {       
        this.setState({ settingslist: res.response.settings });
        this.setState({
          tasker_pem_files: res.response.settings.apns.tasker_pem,
          user_pem_files: res.response.settings.apns.user_pem,
          user_bundle_id: res.response.settings.apns.user_bundle_id,
          tasker_bundle_id: res.response.settings.apns.tasker_bundle_id,
          mode: res.response.settings.apns.mode,
          user_gcm_api_key: res.response.settings.gcm.user,
          tasker_gcm_api_key: res.response.settings.gcm.tasker         
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  
  onEditAdmin = (e, values) => {    
    const data = new FormData();
    data.append("settings[gcm][user]", this.state.user_gcm_api_key);
    data.append("settings[gcm][tasker]", this.state.tasker_gcm_api_key);
    data.append("settings[apns][user_pem]",  this.state.user_pem_files);
    data.append("settings[apns][tasker_pem]", this.state.tasker_pem_files);
    data.append("settings[apns][tasker_bundle_id]", this.state.tasker_bundle_id);
    data.append("settings[apns][user_bundle_id]",this.state.user_bundle_id);
    data.append("settings[apns][mode]", this.state.mode);   
    
    request({
      url: "/administrators/settings/mobile/save",
      method: "POST",
      data: data
    }).then(response => {         
      if(response.status === 1){       
        this.savedsettings();
        this.componentDidMount();
      }     
    });
  };
  savedsettings() {
    return toast.success("Saved!");
  }
  user_pem_filesChangedHandler = evt => {      
    var file = evt.target.files[0];       
      this.setState({
        user_pem_files: file
      });
  
    setTimeout(function() {}, 1000);
  };
  tasker_pem_filesChangedHandler = evt => {    
    var file = evt.target.files[0];  
    this.setState({
        tasker_pem_files: file
      });         
    setTimeout(function() {}, 1000);
  };
  render() {
     return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>Mobile</strong> Settings                  
                 {/* <div className="card-actions">
                    <a>
                      <i className="fa fa-cogs" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody>                  
                    <p className="h5">APNS Settings (For IOS Mobile)</p>
                    <hr />
                    <Row>
                        <Col xs="12" md="6">
                            <FormGroup>
                            <Label for="exampleCustomFileBrowser">User PEM Files | Current File: user.pem</Label>
                            <CustomInput 
                                type="file"
                                id="exampleCustomFileBrowser" 
                                name="user_pem_files" 
                                label="User PEM Files" 
                                onChange={this.user_pem_filesChangedHandler}                               
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="12" md="6">
                            <FormGroup>
                            <Label for="exampleCustomFileBrowser">Tasker PEM Files | Current File:tasker.pem</Label>
                            <CustomInput
                               type="file"
                               id="exampleCustomFileBrowser" 
                               name="tasker_pem_files"
                               onChange={this.tasker_pem_filesChangedHandler}                                                          
                               label="Tasker PEM Files"
                               />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="6">
                            <AvGroup>
                            <Label>User Bundle ID</Label>
                            <AvInput
                                type="text"
                                label="User Bundle ID"
                                name="user_bundle_id"
                                placeholder="Enter User Bundle ID"
                                onChange={this.onChange}
                                minLength="4"
                                maxLength="150"
                                value={this.state.user_bundle_id}                                
                            />
                            <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                        </Col>
                        <Col xs="12" md="6">
                            <AvGroup>
                            <Label>Tasker Bundle ID</Label>
                            <AvInput
                                type="text"
                                name="tasker_bundle_id"
                                placeholder="Enter Tasker Bundle ID"
                                onChange={this.onChange}
                                minLength="4"
                                maxLength="150"
                                value={this.state.tasker_bundle_id}                                
                            />
                            <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="6">
                            <AvField
                            type="select"
                            name="mode"
                            label="Mode"
                            placeholder="Select Mode"
                            onChange={this.onChange}
                            value={this.state.mode}
                            >
                            <option>Select</option>
                            <option>Development</option>
                            <option>Production</option>
                            </AvField>
                        </Col>
                    </Row>
                     <hr />
                    <p className="h5">GCM Settings (For Android Mobiles)</p>
                    <hr />
                     <Row>
                        <Col xs="12" md="6">
                            <AvGroup>
                            <Label>User GCM API Key</Label>
                            <AvInput
                                type="text"
                                label="User GCM API Key"
                                name="user_gcm_api_key"
                                placeholder="Enter User GCM API Key"
                                onChange={this.onChange}
                                minLength="4"
                                maxLength="150"
                                value={this.state.user_gcm_api_key}                                
                            />
                            <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                        </Col>
                        <Col xs="12" md="6">
                            <AvGroup>
                            <Label>Tasker GCM API Key</Label>
                            <AvInput
                                type="text"
                                name="tasker_gcm_api_key"
                                placeholder="Enter Tasker GCM API Key"
                                onChange={this.onChange}
                                minLength="4"
                                maxLength="150"
                                value={this.state.tasker_gcm_api_key}                                
                            />
                            <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                        </Col>
                    </Row>             
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    size="md"
                    color="success pull-right mb-3"
                     title="Save"
                  >
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Mobile;
