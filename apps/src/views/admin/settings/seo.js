/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row
} from "reactstrap";
import {
  AvFeedback,
  AvField,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";

import request from "../../../api/api"; 
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
class Seo extends Component {
  state = {
    seotitle: "",
    seofocuskeyword: "",
    seometa_description: "",
    seogoogle_analytics: "",   
    success: false,
    settingslist: []
  }; 
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }    
    request({
      url: "/administrators/settings/seo",
      method: "POST"
    }).then(res => {    
      if (res.status === 1) {
        this.setState({ settingslist: res.response.settings });
        if(res.response.settings !==null){                     
           this.setState({              
              seotitle: res.response.settings.seo_title,
              seofocuskeyword: res.response.settings.focus_keyword,
              seometa_description: res.response.settings.meta_description,
              seogoogle_analytics: res.response.settings.webmaster.google_analytics,              
               
        });
        }      
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    const settings = {
      
    };    
    
    request({
      url: "/administrators/settings/seo/save",
      method: "POST",
      data: {
         settings: {
            seo_title: this.state.seotitle,
            focus_keyword: this.state.seofocuskeyword,
            meta_description: this.state.seometa_description,   
            webmaster:{
              google_analytics: this.state.seogoogle_analytics
            }        
         }
      }       
    }).then(response => {   
       if(response.status === 1){    
        this.savedsettings();
        this.componentDidMount();
       }
    });    
  };
  savedsettings() {
    return toast.success("Saved!");
  }
handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode
    });
  }; 
  render() {       
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>SEO</strong> Settings  
                  {/*<div className="card-actions">
                    <a>
                      <i className="fa fa-cogs" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>            
                <CardBody>    
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Title</Label>
                      <AvInput
                        type="text"
                        label="Title"
                        name="seotitle"
                        placeholder="Enter title"
                        onChange={this.onChange}                       
                        value={this.state.seotitle}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Focus Keyword</Label>
                      <AvInput
                        type="text"
                        name="seofocuskeyword"
                        placeholder="Enter Focus Keyword"
                        onChange={this.onChange}                        
                        value={this.state.seofocuskeyword}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                   <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Meta Description</Label>
                      <AvInput
                        type="textarea"
                        name="seometa_description"
                        placeholder="Enter Focus Keyword"
                        onChange={this.onChange}                        
                        value={this.state.seometa_description}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                   <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Google Analytics</Label>
                      <AvInput
                        type="text"
                        name="seogoogle_analytics"
                        placeholder="Enter  Google Analytics"
                        onChange={this.onChange}                        
                        value={this.state.seogoogle_analytics}                        
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>                 
                 
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    size="md"
                    color="success pull-right mb-3"
                     title="Save"
                  >
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Seo;
