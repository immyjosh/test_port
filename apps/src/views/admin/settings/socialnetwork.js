/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";

import request from "../../../api/api";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
class Socialnetwork extends Component {
  state = {
    facebook_name: "",
    facebook_url: "",
    facebook_status: "",
    twiltter_name: "",
    twiltter_url: "",
    twiltter_status: "",
    linkedin_name: "",
    linkedin_url: "",
    linkedin_status: "",
    pinterest_name: "",
    pinterest_url: "",
    pinterest_status: "",
    youtube_name: "",
    youtube_url: "",
    youtube_status: "",
    googleplus_name: "",
    googleplus_url: "",
    googleplus_status: "",
    googleplay_name: "",
    googleplay_url: "",
    googleplay_status: "",
    applestore_name: "",
    applestore_url: "",
    applestore_status: "",
    smsmode: "",
    success: false,
    settingslist: []
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }

    request({
      url: "/administrators/settings/social_networks",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ settingslist: res.response.settings });
        if (this.state.settingslist !== null) {
          this.setState({
            facebook_name: res.response.settings.link[0].name,
            facebook_url: res.response.settings.link[0].url,
            facebook_status: res.response.settings.link[0].status,
            twiltter_name: res.response.settings.link[1].name,
            twiltter_url: res.response.settings.link[1].url,
            twiltter_status: res.response.settings.link[1].status,
            linkedin_name: res.response.settings.link[2].name,
            linkedin_url: res.response.settings.link[2].url,
            linkedin_status: res.response.settings.link[2].status,
            pinterest_name: res.response.settings.link[3].name,
            pinterest_url: res.response.settings.link[3].url,
            pinterest_status: res.response.settings.link[3].status,
            youtube_name: res.response.settings.link[4].name,
            youtube_url: res.response.settings.link[4].url,
            youtube_status: res.response.settings.link[4].status,
            googleplus_name: res.response.settings.link[5].name,
            googleplus_url: res.response.settings.link[5].url,
            googleplus_status: res.response.settings.link[5].status,
            googleplay_name: res.response.settings.mobileapp[0].name,
            googleplay_url: res.response.settings.mobileapp[0].url,
            googleplay_status: res.response.settings.mobileapp[0].status,
            applestore_name: res.response.settings.mobileapp[1].name,
            applestore_url: res.response.settings.mobileapp[1].url,
            applestore_status: res.response.settings.mobileapp[1].status,
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    const settings = {};
    request({
      url: "/administrators/settings/social_networks/save",
      method: "POST",
      data: {
        settings: {
          link: [
            {
              img: "",
              name: this.state.facebook_name,
              url: this.state.facebook_url,
              status: this.state.facebook_status
            },
            {
              img: "",
              name: this.state.twiltter_name,
              url: this.state.twiltter_url,
              status: this.state.twiltter_status
            },
            {
              img: "",
              name: this.state.linkedin_name,
              url: this.state.linkedin_url,
              status: this.state.linkedin_status
            },
            {
              img: "",
              name: this.state.pinterest_name,
              url: this.state.pinterest_url,
              status: this.state.pinterest_status
            },
            {
              img: "",
              name: this.state.youtube_name,
              url: this.state.youtube_url,
              status: this.state.youtube_status
            },
            {
              img: "",
              name: this.state.googleplus_name,
              url: this.state.googleplus_url,
              status: this.state.googleplus_status
            }
          ],
          mobileapp: [
            {
              img: "",
              name: this.state.googleplay_name,
              url: this.state.googleplay_url,
              status: this.state.googleplay_status
            },
            {
              img: "",
              name: this.state.applestore_name,
              url: this.state.applestore_url,
              status: this.state.applestore_status
            }
          ],
          facebook_api: {
            application_id: "",
            application_secret: ""
          },
          google_api: {
            client_id: "",
            secret_key: "",
            developer_key: "",
            callback: ""
          },
          twitter_api: {
            consumer_key: "",
            secret_key: ""
          }
        }
      }
    }).then(response => {
      if (response.status === 1) {
        this.savedsettings();
        this.componentDidMount();
      }
    });
  };
  savedsettings() {
    return toast.success("Saved!");
  }
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>Social Network</strong> Settings
                  {/*   <div className="card-actions">
                    <a>
                      <i className="fa fa-cogs" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody>
                  <h5>Social Network URL / Links</h5>
                  <hr />
                  <strong>Facebook Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Facebook Name</Label>
                      <AvInput
                        type="text"
                        label="Facebook Name"
                        name="facebook_name"
                        placeholder="Enter Facebook Name"
                        onChange={this.onChange}
                        value={this.state.facebook_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Facebook Url</Label>
                      <AvInput
                        type="text"
                        label="Facebook Url"
                        name="facebook_url"
                        placeholder="Enter Facebook Url"
                        onChange={this.onChange}
                        value={this.state.facebook_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="facebook_status"
                      label="Facebook Status"
                      placeholder="Select Facebook Status"
                      onChange={this.onChange}
                      value={this.state.facebook_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>
                      <option value="0">Unpublish</option>
                    </AvField>
                  </Col>
                  <strong>Twiltter Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Twiltter Name</Label>
                      <AvInput
                        type="text"
                        label="Twiltter Name"
                        name="twiltter_name"
                        placeholder="Enter Twiltter Name"
                        onChange={this.onChange}
                        value={this.state.twiltter_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Twiltter Url</Label>
                      <AvInput
                        type="text"
                        label="Twiltter Url"
                        name="twiltter_url"
                        placeholder="Enter Twiltter Url"
                        onChange={this.onChange}
                        value={this.state.twiltter_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="twiltter_status"
                      label="Twiltter Status"
                      placeholder="Select Twiltter Status"
                      onChange={this.onChange}
                      value={this.state.twiltter_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>
                      <option value="0">Unpublish</option>
                    </AvField>
                  </Col>
                  <strong>Linked In Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Linked In Name</Label>
                      <AvInput
                        type="text"
                        label="Linked In Name"
                        name="linkedin_name"
                        placeholder="Enter Linked In Name"
                        onChange={this.onChange}
                        value={this.state.linkedin_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Twiltter Url</Label>
                      <AvInput
                        type="text"
                        label="Linked In Url"
                        name="linkedin_url"
                        placeholder="Enter Linked In Url"
                        onChange={this.onChange}
                        value={this.state.linkedin_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="linkedin_status"
                      label="Linked In Status"
                      placeholder="Select Linked In Status"
                      onChange={this.onChange}
                      value={this.state.linkedin_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>
                      <option value="0">Unpublish</option>
                    </AvField>
                  </Col>
                  <strong>Pinterest Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Pinterest Name</Label>
                      <AvInput
                        type="text"
                        label="Pinterest Name"
                        name="pinterest_name"
                        placeholder="Enter Pinterest Name"
                        onChange={this.onChange}
                        value={this.state.pinterest_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Twiltter Url</Label>
                      <AvInput
                        type="text"
                        label="Pinterest Url"
                        name="pinterest_url"
                        placeholder="Enter Pinterest Url"
                        onChange={this.onChange}
                        value={this.state.pinterest_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="pinterest_status"
                      label="Pinterest Status"
                      placeholder="Select Pinterest Status"
                      onChange={this.onChange}
                      value={this.state.pinterest_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>                      
                      <option value="0">Unpublish</option>    
                    </AvField>
                  </Col>
                  <strong>You Tube Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Youtube Name</Label>
                      <AvInput
                        type="text"
                        label="Youtube Name"
                        name="youtube_name"
                        placeholder="Enter Youtube Name"
                        onChange={this.onChange}
                        value={this.state.youtube_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Youtube Url</Label>
                      <AvInput
                        type="text"
                        label="Youtube Url"
                        name="youtube_url"
                        placeholder="Enter Youtube Url"
                        onChange={this.onChange}
                        value={this.state.youtube_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="youtube_status"
                      label="Youtube Status"
                      placeholder="Select Youtube Status"
                      onChange={this.onChange}
                      value={this.state.youtube_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>                      
                      <option value="0">Unpublish</option>    
                    </AvField>
                  </Col>
                  <strong>Google Plus Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Google Plus Name</Label>
                      <AvInput
                        type="text"
                        label="Google Plus Name"
                        name="googleplus_name"
                        placeholder="Enter Google Plus Name"
                        onChange={this.onChange}
                        value={this.state.googleplus_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Google Plus Url</Label>
                      <AvInput
                        type="text"
                        label="Google Plus Url"
                        name="googleplus_url"
                        placeholder="Enter Google Plus Url"
                        onChange={this.onChange}
                        value={this.state.googleplus_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="googleplus_status"
                      label="Google Plus Status"
                      placeholder="Select Google Plus Status"
                      onChange={this.onChange}
                      value={this.state.googleplus_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>                      
                      <option value="0">Unpublish</option>    
                    </AvField>
                  </Col>
                  <hr />
                  <h5>Mobile Download URL / Links</h5>
                  <hr />
                  <strong>Google Play Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Google Play Name</Label>
                      <AvInput
                        type="text"
                        label="Google Play Name"
                        name="googleplay_name"
                        placeholder="Enter Google Play Name"
                        onChange={this.onChange}
                        value={this.state.googleplay_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Google Play Url</Label>
                      <AvInput
                        type="text"
                        label="Google Play Url"
                        name="googleplay_url"
                        placeholder="Enter Google Play Url"
                        onChange={this.onChange}
                        value={this.state.googleplay_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="googleplay_status"
                      label="Google Play Status"
                      placeholder="Select Google Play Status"
                      onChange={this.onChange}
                      value={this.state.googleplay_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>                      
                      <option value="0">Unpublish</option>    
                    </AvField>
                  </Col>
                  <strong>Apple Store Details</strong>
                  <Col xs="12" md="6" className="mt-2">
                    <AvGroup>
                      <Label>Apple Store Name</Label>
                      <AvInput
                        type="text"
                        label="Apple Store Name"
                        name="applestore_name"
                        placeholder="Enter Apple Store Name"
                        onChange={this.onChange}
                        value={this.state.applestore_name}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Apple Store Url</Label>
                      <AvInput
                        type="text"
                        label="Apple Store Url"
                        name="applestore_url"
                        placeholder="Enter Apple Store Url"
                        onChange={this.onChange}
                        value={this.state.applestore_url}
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <AvField
                      type="select"
                      name="applestore_status"
                      label="Apple Store Status"
                      placeholder="Select Apple Store Status"
                      onChange={this.onChange}
                      value={this.state.applestore_status}
                    >
                      <option>Select</option>
                      <option value="1">Publish</option>                      
                      <option value="0">Unpublish</option>    
                    </AvField>
                  </Col>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    size="md"
                    color="success pull-right mb-3"
                    onClick={() => {
                      this.phonefield(this.state.number);
                    }}
                    title="Save"
                  >
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Socialnetwork;
