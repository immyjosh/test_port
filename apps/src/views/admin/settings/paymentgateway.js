/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";

import request from "../../../api/api";

class Paymentgateway extends Component {
  state = {
    publishable_key: "",
    secret_key: "",
    settingslist: []
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/settings/payment_gateway",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ settingslist: res.response.settings });
        if (res.response.settings !== null) {
          this.setState({
            secret_key: res.response.settings.stripe.secret_key,
            publishable_key: res.response.settings.stripe.publishable_key
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    const settings = {};
    request({
      url: "/administrators/settings/payment_gateway/save",
      method: "POST",
      data: {
        settings: {
          secret_key: this.state.secret_key,
          publishable_key: this.state.publishable_key
        }
      }
    }).then(response => {
      if (response.status === 1) {
        this.savedsettings();
        this.componentDidMount();
      }
    });
  };
  savedsettings() {
    return toast.success("Saved!");
  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>Payment Gateway</strong> Settings
                  {/* <div className="card-actions">
                    <a>
                      <i className="fa fa-cogs" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody>
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
                    <Col xs="12" md="12">
                      <p className="h5">Stripe</p>
                    </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Publishable Key</Label>
                        <AvInput
                          type="text"
                          label="Publishable Key"
                          name="publishable_key"
                          placeholder="Enter SMTP Host"
                          onChange={this.onChange}
                          value={this.state.publishable_key}
                          message="error"
                          autoFocus
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Secret Key</Label>
                        <AvInput type="text" name="secret_key" placeholder="Enter Key" onChange={this.onChange} value={this.state.secret_key} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					</div>
				   </Col>	
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="success pull-right mb-3" title="Save">
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Paymentgateway;
