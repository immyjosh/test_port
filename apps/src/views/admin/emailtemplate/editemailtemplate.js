/*eslint no-sequences: 0*/
import {
  AvFeedback,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row,
} from "reactstrap";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import { Editor } from "@tinymce/tinymce-react";
import {Settings} from "../../../api/key";

class editemailtemplate extends Component {
  state = {
    description: "",
    name: "",
    email_subject: "",
    subscriptionmailoption: "",
    sender_name: "",
    sender_email: "",
    email_content: "",
    tokenusername: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  subChange = e => {
    this.setState({ subscriptionmailoption: e.target.checked });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    this.setState({
      tokenusername: token.username
    });
    request({
      url: "/administrators/emailtemplate/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ adminlist: res.response.result[0] });
      this.setState({
        name: this.state.adminlist.name,
        description: this.state.adminlist.description,
        email_subject: this.state.adminlist.email_subject,
        sender_name: this.state.adminlist.sender_name,
        sender_email: this.state.adminlist.sender_email,
        email_content: this.state.adminlist.email_content,
        status: this.state.adminlist.status
      });
    });
  }
  onEditAdmin = (e, values) => {
    request({
      url: "/administrators/emailtemplate/save",
      method: "POST",
      data: {
        _id: this.props.location.state.rowid,
        name: this.state.name,
        description: this.state.description,
        email_subject: this.state.email_subject,
        sender_name: this.state.sender_name,
        sender_email: this.state.sender_email,
        email_content: this.state.email_content,
        status: this.state.status
      }
    }).then(res => {
      if (res.status === 1) {
        this.editnodify();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  editnodify() {
    return (
      toast.success("Updated"),
      this.componentDidMount()
    );
  }
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-note" />Edit Email Template
                 {/* <div className="card-actions">
                    <a>
                      <i className="fa fa-user" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody>
                  <Row>
				    <Col xs="12" md="12">
				    <div className="cus-design edited-section">
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Template Name</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter templatename"
                          onChange={this.onChange}
                          value={this.state.name}
                          disabled
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Email Subject</Label>
                        <AvInput
                          type="text"
                          name="email_subject"
                          placeholder="Enter Email Subject"
                          onChange={this.onChange}
                          value={this.state.email_subject}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Sender Name</Label>
                        <AvInput
                          type="text"
                          name="sender_name"
                          placeholder="Enter Sender Name"
                          onChange={this.onChange}
                          value={this.state.sender_name}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Sender Email Address</Label>
                        <AvInput
                          type="email"
                          name="sender_email"
                          placeholder="Enter Sender Email Address"
                          onChange={this.onChange}
                          value={this.state.sender_email}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="12">
                      <label>Email Content</label>
                      <Editor
                        apiKey={Settings.tinymceKey}
                        initialValue={this.state.email_content}
                        init={{
                          plugins: "link image code",
                          toolbar:
                            "undo redo | bold italic | alignleft aligncenter alignright | code",
                            height: 420
                        }}
                        onChange={this.handleEditorChange}
                        value={this.state.email_content}
                      />
                    </Col>
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right"  title="Update">
                    <i className="fa fa-dot-circle-o" /> Update
                  </Button>
                  <Link to="/admin/listemailtemplate">
                    <Button type="submit" color="secondary"  title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default editemailtemplate;
