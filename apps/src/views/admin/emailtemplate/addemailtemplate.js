import {
  AvFeedback,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row
} from "reactstrap";
import request from "../../../api/api";
import { Editor } from "@tinymce/tinymce-react";
import savenodify from "./listemailtemplate";
import {Settings} from "../../../api/key";

class addemailtemplate extends Component {
  state = {
    description: "",
    name: "",
    email_subject: "",
    subscriptionmailoption: "",
    sender_name: "",
    sender_email: "",
    email_content: ""
  };
  flash = new savenodify();
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  subChange = e => {
    this.setState({ subscriptionmailoption: e.target.checked });
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
  }
  OnFormSubmit = (e, values) => {
    e.persist();
    const adminform = {
      name: this.state.name,
      description: this.state.description,
      email_subject: this.state.email_subject,
      sender_name: this.state.sender_name,
      sender_email: this.state.sender_email,
      email_content: this.state.email_content,
      status: 1
    };
    request({
      url: "/administrators/emailtemplate/save",
      method: "POST",
      data: adminform
    })
      .then(res => {
        if (res.status === 1) {
          this.saveuser();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
    this.form && this.form.reset();
  };
  saveuser = () => {
    return (
      this.props.history.push("/admin/listemailtemplate"),
      this.flash.adminsavenodify()
    );
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />Email Template
               {/* <div className="card-actions">
                  <a>
                    <i className="fa fa-user-plus" />
                    <small className="text-muted" />
                  </a>
                </div>*/}
              </CardHeader>
              <AvForm
                ref={clear => (this.form = clear)}
                onValidSubmit={this.OnFormSubmit}
              >
                <CardBody>
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Template Name</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter templatename"
                          onChange={this.onChange}
                          value={this.state.name}
                          required
                          autoFocus
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Email Subject</Label>
                        <AvInput
                          type="text"
                          name="email_subject"
                          placeholder="Enter Email Subject"
                          onChange={this.onChange}
                          value={this.state.email_subject}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Sender Name</Label>
                        <AvInput
                          type="text"
                          name="sender_name"
                          placeholder="Enter Sender Name"
                          onChange={this.onChange}
                          value={this.state.sender_name}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Sender Email Address</Label>
                        <AvInput
                          type="email"
                          name="sender_email"
                          placeholder="Enter Sender Email Address"
                          onChange={this.onChange}
                          value={this.state.sender_email}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
				 
                    <Col xs="12" md="12">
                      <label>Email Content</label>
                      <Editor
                          apiKey={Settings.tinymceKey}
                        initialValue={this.state.email_content}
                        init={{
                          plugins: "link image code",
                          toolbar:
                            "undo redo | bold italic | alignleft aligncenter alignright | code"
                        }}
                        onChange={this.handleEditorChange}
                        value={this.state.email_content}
                      />
                    </Col>
					 </div>	
					</Col> 
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    title="Add Email Template"
                  >
                    Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default addemailtemplate;
