import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import Loader from "../../common/loader";
import Pagination from "react-js-pagination";
// import moment from "moment";

class listemailtemplate extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    isLoader: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0
    }
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    this.setState({ isLoader: true });
    request({
      url: "/administrators/emailtemplate/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res) {
        this.setState({
          isLoader: false,
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/admin/editemailtemplate",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/administrators/emailtemplate/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (!res.response.result && !res.response.fullcount) {
        this.setState({ adminlist: [], pages: "" });
      } else {
        this.setState({
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["name", "email_subject"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Template Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Sender Name") {
        state.tableOptions.filter = "sender_name";
      } else if (value === "Sender Email") {
        state.tableOptions.filter = "sender_email";
      } else if (value === "Email Subject") {
        state.tableOptions.filter = "email_subject";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };

  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("New Template Added");
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Email Template List
            <div className="card-actions">
              <button onClick={() => this.props.history.push("/admin/addemailtemplate")}>
                <i className="fa fa-plus" /> Add New
                <small className="text-muted" />
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
              {/* <div className="col-lg-7 pl-0">
                <Button size="md" color="primary" onClick={this.export}>
                  Export
                </Button>
              </div> */}
            {/*  <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Template Name</option>
                  <option>Email Subject</option>
                </Input>
              </div>*/}
              <div className="col-lg-12">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive size="sm">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Template Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                    </th>
                    {/*<th
                      onClick={() => {
                        this.sort("sender_name");
                      }}
                    >
                      Sender Name
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="sender_name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("sender_email");
                      }}
                    >
                      Sender Email
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="sender_email" />
                    </th>*/}
                    <th
                      onClick={() => {
                        this.sort("email_subject");
                      }}
                    >
                      Email Subject <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email_subject" />
                    </th>
                   {/* <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>*/}
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        {/*<td>{item.sender_name}</td>
                        <td>{item.sender_email}</td>*/}
                        <td>{item.email_subject}</td>
                      {/*  <td>
                          {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                        </td>*/}
                        <td>
                          <button type="button" title="Edit" className="btn view-table" color="info" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                            <i className="fa fa-edit" />
                          </button>
                          <UncontrolledTooltip placement="top" target={`edit${i}`}>
                            Edit
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              {/* <Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default listemailtemplate;
