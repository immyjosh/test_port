/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardGroup, Col, Container, InputGroupAddon, InputGroupText, Row } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import request, { client } from "../../api/api";
import { Link } from "react-router-dom";
import Gettoken from "../../api/gettoken";
import Context from "../../api/context";

class Adminlogin extends Component {
  state = {
    email: "",
    password: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    const loginform = {
      email: this.state.email,
      password: this.state.password
    };
    request({
      url: "/adminlogin",
      method: "POST",
      data: loginform
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUS", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          this.props.history.push({ pathname: "/admin/dashboard", state: true });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };
  componentDidMount() {
    const token = this.props.theme.Authtoken;
    if (token.role) {
      switch (token.role) {
        case "admin":
          this.props.history.replace("/admin/dashboard");
          break;
        default:
          this.props.history.replace("/");
      }
    }
    if (this.props.location.state === "notify") {
      toast.info("Loged Out!");
      this.props.history.push({ state: "" });
    }
  }

  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <CardBody>
                    <h1 className="login-infos"  >Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <AvForm onValidSubmit={this.OnFormSubmit}>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="text" name="email" placeholder="Enter email or username" onChange={this.onChange} value={this.state.email} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="password" name="password" placeholder="Enter password" onChange={this.onChange} value={this.state.password} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>

                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" type={"submit"}>
                            Login
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">
                            <Link className="for-pass" to="/admin/forgotpassword">Forgot password?</Link>
                          </Button>
                        </Col>
                      </Row>
                    </AvForm>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Adminlogin {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
