import React, { Component } from "react";
import { Button, Card, CardBody, CardGroup, Col, Container, InputGroupAddon, InputGroupText, Row } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import { Link } from "react-router-dom";
import request from "../../api/api";

class adminchangepassword extends Component {
  state = {
    newpassword: "",
    confirmnewpassword: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    request({
      url: `/admin/resetpassword/${this.props.match.params.id}/${this.props.match.params.resetcode}`,
      method: "POST",
      data: {
        password: this.state.newpassword,
        confirmpassword: this.state.confirmnewpassword
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.passwordnodify();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error(err);
      });
  };
  toastId = "ChangePassword";
  passwordnodify() {
    if (!toast.isActive(this.toastId)) {
      this.toastId = toast.success("Password Changed!");
    }
  }

  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <CardBody>
                      <h1 className="login-infos">Change Password</h1>
                      {/* <p className="text-muted">Sign In to your account</p> */}
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput
                          lable="New Password"
                          type="password"
                          name="newpassword"
                          placeholder="Enter New Password"
                          onChange={this.onChange}
                          value={this.state.newpassword}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput
                          type="password"
                          name="confirmnewpassword"
                          placeholder="Confirm new password"
                          onChange={this.onChange}
                          value={this.state.confirmnewpassword}
                          required
                          validate={{ match: { value: "newpassword" } }}
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">
                            Submit
                          </Button>
                        </Col>
                        <Col xs="6" className="bac-log">
                          {" "}
                          <Button color="link" className="px-0">
                            <Link to="/admin">Back to Login</Link>
                          </Button>
                        </Col>
                      </Row>
                    </CardBody>
                  </AvForm>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default adminchangepassword;
