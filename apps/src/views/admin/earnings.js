import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Col, Row, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request, { client } from "../../api/api";
import Widget02 from "../Template/Widgets/Widget02";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
// import moment from 'moment';

const tStyle = {
  cursor: "pointer"
};


class Earnings extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    earnings: "",
    subscription: "",
    dashboardcounts: "",
    tableOptions: {
      // payment_status : "",
      // type : "",
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0
    }
  };
  componentDidMount() {
    const token = this.props.token_role;
    client.defaults.headers.common["Authorization"] = this.props.token;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/agency");
    }
    this.populateData();
  }
  // editpage = id => {
  //   return this.props.history.push({
  //     pathname: "/admin/editsubscription",
  //     state: { rowid: id }
  //   });
  // };
  /* ClickableTabs = (type) => {
    if (type == "payment") {
      this.setState(state => {
        state.tableOptions.payment_status = 1;
        state.tableOptions.type = "";
        this.populateData();
      })
    } else if (type == "subscription") {
      this.setState(state => {
        state.tableOptions.type = type;
        state.tableOptions.payment_status = "";
        this.populateData();
      })
    } else if (type == "trial") {
      this.setState(state => {
        state.tableOptions.type = type;
        state.tableOptions.payment_status = "";
        this.populateData();
      })
    } else {
      this.setState(state => {
        state.tableOptions.type = "";
        state.tableOptions.payment_status = "";
        this.populateData();
      })
    }
  } */
  populateData() {
    request({
      url: "/administrators/plans/earnings",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          dashboardcounts: res.response.dashboardcounts[0] || {},
          earnings: res.response.earnings ? res.response.earnings[0] : [],
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["subscribersamount", "subscriberscount", "name", "createdAt", "amount", "noofdays", "freedays"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Free days") {
        state.tableOptions.filter = "freedays";
      } else if (value === "No of days") {
        state.tableOptions.filter = "noofdays";
      } else if (value === "Amount") {
        state.tableOptions.filter = "amount";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/plans/earningsexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("Saved!");
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row className="sub-ranges">
          <Col xs="12" sm="6" lg="4">
            <Widget02 header={this.state.earnings ? <Fragment>£ {this.state.earnings.earning}</Fragment> : "0"} mainText="Amount" icon="fa fa-money" color="primary" />
            {/* <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs("payment")} header={this.state.earnings ? <Fragment>£ {this.state.earnings.earning}</Fragment> : "0"} mainText="Amount" icon="fa fa-money" color="primary" /> */}
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 header={this.state.dashboardcounts.subscription ? this.state.dashboardcounts.subscription : "0"} mainText="SubScriptions" icon="fa fa-users" color="info" />
            {/* <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs("subscription")} header={this.state.dashboardcounts.subscription ? this.state.dashboardcounts.subscription : "0"} mainText="SubScriptions" icon="fa fa-users" color="info" /> */}
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 header={this.state.dashboardcounts.trial ? this.state.dashboardcounts.trial : "0"} mainText="Trial" icon="fa fa-user-o" color="warning" />
            {/* <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs("trial")} header={this.state.dashboardcounts.trial ? this.state.dashboardcounts.trial : "0"} mainText="Trial" icon="fa fa-user-o" color="warning" /> */}
          </Col>
          <Col xs="12" sm="6" lg="4">
            {/* <Widget02
              header={0}
              mainText="Income"
              icon="fa fa-users"
              color="danger"
            /> */}
          </Col>
        </Row>
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Subscription Earnings List
            {/* <span className="cursor-pointer" onClick={() => this.ClickableTabs("")}>Subscription Earnings List</span> */}
            <div className="card-actions" style={tStyle} onClick={this.export}>
              <button>
                <i className="fa fa-upload" /> Export
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
             {/* <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Name</option>
                   <option>Free days</option>
                   <option>No of days</option>
                   <option>Amount</option>
                   <option>Subscribers Amount</option>
                  <option>Subscribers Count</option>
                </Input>
              </div>*/}
              <div className="col-lg-12">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                    </th>

                    <th
                      onClick={() => {
                        this.sort("freedays");
                      }}
                    >
                      Trial Period <i style={{ paddingLeft: "25px" }} className={sorticondef} id="freedays" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("noofdays");
                      }}
                    >
                      Billing Period <i style={{ paddingLeft: "25px" }} className={sorticondef} id="noofdays" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("amount");
                      }}
                    >
                      Amount <i style={{ paddingLeft: "25px" }} className={sorticondef} id="amount" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("subscriberscount");
                      }}
                    >
                      Subscribers <i style={{ paddingLeft: "25px" }} className={sorticondef} id="subscriberscount" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("subscribersamount");
                      }}
                    >
                      Earnings <i style={{ paddingLeft: "25px" }} className={sorticondef} id="subscribersamount" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("subscribersamount");
                      }}
                      className="d-none"
                    >
                      Created Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>
                          {item.freedays} <small>Days</small>
                        </td>
                        <td>{item.noofdays === 30 ? "Monthly" : item.noofdays === 365 ? "Yearly" : ""}</td>
                        <td>£ {item.amount}</td>
                        <td>{item.subscriberscount}</td>
                        <td>£ {item.subscribersamount}</td>
                        <td className="d-none">{item.createdAt}</td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>
                        <h5>No record available</h5>
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              {/* <Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Earnings;
