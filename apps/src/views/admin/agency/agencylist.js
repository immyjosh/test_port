/* eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Col, Row, Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request, { client } from "../../../api/api";
// import { DateRangePicker } from "react-dates";
import fileDownload from "js-file-download";
import loginnodify from "../../agency/agencydashboard";
import loginnodifyNoSub from "../../agency/subscribe/subscribe";
import Loader from "../../common/loader";
import jwt_decode from "jwt-decode";
import Pagination from "react-js-pagination";
import Widget02 from "../../Template/Widgets/Widget02";
// import moment from "moment";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "120px"
};

class Agencylist extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    isLoader: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      to_date: "",
      status: "",
      from_date: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    dashboardCount: {}
  };
  actagency = React.createRef();
  flash = new loginnodify();
  flash1 = new loginnodifyNoSub();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      // return this.props.history.replace("/agency");
    }
    client.defaults.headers.common["Authorization"] = this.props.token;
    this.setState({ isLoader: true });
    request({
      url: "/administrators/agency/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          isLoader: false,
          adminlist: res.response.result || {},
          dashboardCount: res.response.dashboard[0] || {},
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.resposnse);
      }
    });
  }
  ClickableTabs = status => {
    if (status) {
      this.setState(state => {
        state.tableOptions.status = status;
        this.populateData();
      });
    } else if (status === 0) {
      this.setState(state => {
        state.tableOptions.status = status;
        this.populateData();
      });
    } else {
      this.setState(state => {
        state.tableOptions.status = "";
        this.populateData();
      });
    }
  };
  editpage = (e, id) => {
    e.stopPropagation();
    return this.props.history.push({
      pathname: "/admin/editagency",
      state: { rowid: id }
    });
  };
  viewpage = id => {
    return this.props.history.push({
      pathname: "/admin/viewagency",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/administrators/agency/list",
      method: "POST",
      data: this.state.tableOptions
    })
      .then(res => {
        if (res.status === 1) {
          if (!res.response.result && !res.response.fullcount) {
            this.setState({ adminlist: [], pages: "" });
          } else {
            this.setState({
              adminlist: res.response.result,
              pages: res.response.fullcount,
              currPage: res.response.length
            });
          }
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["company_name", "email", "name", "status", "phone", "employees", "clients", "hours"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Company Name") {
        state.tableOptions.filter = "company_name";
      } else if (value === "Email") {
        state.tableOptions.filter = "email";
      } else if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Tel") {
        state.tableOptions.filter = "phone.number";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/agency/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d || "";
        state.tableOptions.to_date = this.state.end_date._d || "";
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  nodifytoastId = "Agencylist";
  nodify() {
    if (!toast.isActive(this.nodifytoastId)) {
      this.nodifytoastId = toast.warn("Updated");
    }
  }
  adminsavenodifytoastId = "Agencylist";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifytoastId)) {
      this.adminsavenodifytoastId = toast.success("New Agency Added!");
    }
  }
  actAsAgency = (e, id) => {
    e.stopPropagation();
    const tabOpen = window.open("about:blank", "newtab");
    request({
      url: "/administrators/agency/actasagency",
      method: "POST",
      data: { id: id }
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUSA", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          let tokenAgency;
          if (res.response.auth_token) {
            tokenAgency = jwt_decode(res.response.auth_token);
          }
          if (tokenAgency.subscription === 1) {
            return (
              (tabOpen.location = "/portal/agency/dashboard"),
              setTimeout(() => {
                window.location.reload();
              }, 800)
            );
          } else if (tokenAgency.subscription === 0) {
            return (
              (tabOpen.location = "/portal/agency/subscribe"),
              setTimeout(() => {
                window.location.reload();
              }, 800)
            );
          }
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    const hours = hour => {
      if (hour && hour[0]) {
        const total = hour[0].toFixed(2);
        const split = total.split(".");
        const totalhours = `${split[0]}:` + split[1] || `00`;
        return totalhours;
      } else {
        return "0:00";
      }
    };
    return (
      <div className="animated">
        <Row className="sub-ranges">
          <Col xs="12" sm="6" lg="4">
            <Widget02
              className="cursor-pointer"
              onClick={() => this.ClickableTabs("")}
              header={this.state.dashboardCount ? (this.state.dashboardCount.active ? this.state.dashboardCount.active : 0) + (this.state.dashboardCount.inactive ? this.state.dashboardCount.inactive : 0) : 0}
              mainText="Total Agencies"
              icon="fa fa-users"
              color="primary"
            />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ClickableTabs(1)} header={this.state.dashboardCount.active ? this.state.dashboardCount.active : "0"} mainText="Active Agencies" icon="fa fa-users" color="success" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02
              className="cursor-pointer"
              onClick={() => this.ClickableTabs(0)}
              header={this.state.dashboardCount.inactive ? this.state.dashboardCount.inactive : "0"}
              mainText="Inactive Agencies"
              icon="fa fa-user-secret"
              color="secondary"
            />
          </Col>
        </Row>
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          {this.props.location.pathname === "/admin/dashboard" ? (
            <CardHeader>
              <i className="icon-list" />
              <span className="cursor-pointer" onClick={() => this.ClickableTabs("")}>
                Recent Agency List
              </span>
            </CardHeader>
          ) : (
            <CardHeader>
              <i className="icon-list" />
              <span className="cursor-pointer" onClick={() => this.ClickableTabs("")}>
                Agency List
              </span>
              <div className="card-actions" style={tStyle}>
                <button style={tStyles} onClick={() => this.props.history.push("/admin/addagency")}>
                  <i className="fa fa-plus" /> Add New
                  {/* <small className="text-muted" />*/}
                </button>
                <button style={tStyles} onClick={this.export}>
                  <i className="fa fa-upload" /> Export
                  {/* <small className="text-muted" />*/}
                </button>
              </div>
            </CardHeader>
          )}
          <CardBody>
            {this.props.location.pathname !== "/admin/dashboard" ? (
              <div className="row">
                {/* <div className="col-lg-8">
                  <DateRangePicker
                    showClearDates={true}
                    startDate={this.state.start_date}
                    startDateId="start_date"
                    endDate={this.state.end_date}
                    endDateId="end_date"
                    onDatesChange={({ startDate, endDate }) => {
                      this.setState({
                        start_date: startDate,
                        end_date: endDate
                      });
                    }}
                    isOutsideRange={day => day.isBefore(this.state.start_date)}
                    focusedInput={this.state.focusedInput}
                    onFocusChange={focusedInput => this.setState({ focusedInput })}
                    displayFormat="DD-MM-YYYY"
                  />
                  <button
                    className="btn btn-primary rounded-0"
                    onClick={() => {
                      this.fromTo();
                    }}
                  >
                    <i className="fa fa-search" />
                  </button>
                </div> */}
                <div className="col-lg-12">
                  <InputGroup>
                   {/* <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-2">
                      <option>All</option>
                      <option>Company</option>
                      <option>Email</option>
                      <option>Name</option>
                      <option>Tel</option>
                    </Input>*/}
                    <Input type="text" ref="search" placeholder="Search" name="search..." onChange={e => this.search(e.target.value)} className="rounded-0" />
                    <Button
                      className="rounded-0"
                      color="primary"
                      id="clear"
                      onClick={() => {
                        ReactDOM.findDOMNode(this.refs.search).value = "";
                        this.search("");
                      }}
                    >
                      <i className="fa fa-remove" />
                    </Button>
                    <UncontrolledTooltip placement="top" target="clear">
                      Clear
                    </UncontrolledTooltip>
                  </InputGroup>
                </div>
              </div>
            ) : null}

            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    {this.props.location.pathname !== "/admin/dashboard" ? (
                      <th
                        onClick={() => {
                          this.sort("company_name");
                        }}
                      >
                        Company Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="company_name" />
                      </th>
                    ) : null}

                    {this.props.location.pathname !== "/admin/dashboard" ? (
                      <Fragment>
                        <th
                          onClick={() => {
                            this.sort("name");
                          }}
                        >
                          Contact Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("phone");
                          }}
                        >
                          Tel <i style={{ paddingLeft: "25px" }} className={sorticondef} id="phone" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("email");
                          }}
                        >
                          Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("employees");
                          }}
                        >
                          No of Employees <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employees" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("clients");
                          }}
                        >
                          Clients <i style={{ paddingLeft: "25px" }} className={sorticondef} id="clients" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("hours");
                          }}
                        >
                          Hours <i style={{ paddingLeft: "25px" }} className={sorticondef} id="hours" />
                        </th>
                        {/* <th
                          onClick={() => {
                            this.sort("createdAt");
                          }}
                        >
                          Created Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                        </th> */}
                        <th
                          onClick={() => {
                            this.sort("status");
                          }}
                        >
                          Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                        </th>
                        <th>Actions</th>
                      </Fragment>
                    ) : (
                      <Fragment>
                        <th>Name</th>
                        <th>Email</th>
                      </Fragment>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.viewpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.company_name}</td>
                        <td className="client-email">{item.name}</td>
                        <td>{item.phone.number}</td>
                        <td>{item.email}</td>
                        <td className="client-email">{Math.round(item.employees)}</td>
                        <td className="client-email">{Math.round(item.clients)}</td>
                        <td className="client-email">{hours(item.hours)}</td>
                        {/* <td>{moment(item.createdAt).format("DD-MM-YYYY")}</td> */}
                        <td>
                          {item.status === 1 && item.isverified === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 && item.isverified === 1 ? <Badge color="danger">In-active</Badge> : null}
                          {item.status === 1 && item.isverified === 0 ? <Badge color="danger">Not Verified</Badge> : null}
                        </td>
                        <td>
                          <Fragment>
                            <button type="button" title="Edit" className="btn table-edit" id={`edit${i}`} onClick={(e, id) => this.editpage(e, item._id)}>
                              <i className="fa fa-edit" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit${i}`}>
                              Edit
                            </UncontrolledTooltip>
                            {item.status === 1 && item.isverified === 1 ? (
                              <Fragment>
                                <button type="button" title="Edit" className="btn table-edit ml-2" id={`act${i}`} onClick={(e, id) => this.actAsAgency(e, item._id)}>
                                  <i className="fa fa-sign-in" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`act${i}`}>
                                  Act As Agency
                                </UncontrolledTooltip>
                              </Fragment>
                            ) : null}
                          </Fragment>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            {this.props.location.pathname !== "/admin/dashboard" ? (
              <Fragment>
                <nav className="float-left">
                  <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                  </Input>
                </nav>
                <nav className="float-right">
                  <div>
                    <Pagination
                      prevPageText="Prev"
                      nextPageText="Next"
                      firstPageText="First"
                      lastPageText="Last"
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.tableOptions.limit}
                      totalItemsCount={this.state.pages}
                      pageRangeDisplayed={this.state.pageRangeDisplayed}
                      onChange={this.paginate}
                    />
                  </div>
                </nav>
              </Fragment>
            ) : null}
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Agencylist;
