/* eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import classnames from "classnames";
import { Button, Card, Input, CardBody, CardFooter, CardHeader, Col, Label, Row, CustomInput, NavItem, Nav, NavLink, TabContent, TabPane, Badge, UncontrolledTooltip } from "reactstrap";
import request, { client, NodeURL } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import Avatar from "react-avatar-edit";
import moment from "moment";
import Pagination from "react-js-pagination";
// import ReactCrop from "react-image-crop";
// import "react-image-crop/dist/ReactCrop.css";

const subh4 = {
  color: "rgb(177, 50, 50)"
};

class Editagency extends Component {
  state = {
    username: "",
    password: "",
    name: "",
    email: "",
    confirm_password: "",
    conform: "",
    number: "",
    code: "",
    number1: "",
    code1: "",
    company_email: "",
    company_logo: "",
    logo_file: "",
    company_logo_name: "",
    company_name: "",
    logo_return: "",
    postal_address: "",
    company_description: "",
    registration_number: "",
    organisation_type: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    phoneerror: false,
    stausactive: false,
    isverifiedstaus: false,
    avatar: "",
    status: "",
    avatar_return: "",
    isverifiedCheck: "",
    isverified: "",
    avatarName: "",
    bank_name: "",
    bank_ac: "",
    bank_becs: "",
    bank_sepa: "",
    bank_ac_name: "",
    bank_sort_code: "",
    dailcountry: "",
    dailcountry1: "",
    propreview: null,
    src: null,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 5,
      skip: 0,
      status: 1,
      id: this.props && this.props.location && this.props.location.state ? this.props.location.state.rowid : ""
    },
    subscriptiontableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 9,
      skip: 0,
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : "",
      id: this.props && this.props.location && this.props.location.state ? this.props.location.state.rowid : ""
    },
    crop: {
      x: 120,
      y: 120,
      aspect: 16 / 9,
      maxWidth: 180,
      maxHeight: 180
    },
    activeTab: this.props && this.props.location && this.props.location.state && this.props.location.state.activetab ? this.props.location.state.activetab : "1"
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  isverifiedChanges = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      isverifiedstaus: e.target.checked,
      isverified: value
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/agency/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          const data = res.response.result[0];
          this.setState({
            username: data.username,
            name: data.name,
            email: data.email,
            status: data.status,
            number: data.phone.number,
            code: data.phone.code,
            dailcountry: data.phone ? data.phone.dailcountry : "gb",
            line1: data.address.line1,
            line2: data.address.line2,
            city: data.address.city,
            state: data.address.state,
            country: data.address.country,
            zipcode: data.address.zipcode,
            address: data.address.formatted_address,
            company_name: data.company_name,
            company_description: data.company_description,
            number1: data.company_phone ? data.company_phone.number : "07410",
            code1: data.company_phone ? data.company_phone.code : "44",
            dailcountry1: data.company_phone ? data.company_phone.dailcountry : "gb",
            postal_address: data.postal_address,
            organisation_type: data.organisation_type,
            company_email: data.company_email,
            registration_number: data.registration_number,
            avatar_return: data.avatar,
            bank_name: data.bank_details ? data.bank_details.name : "",
            bank_ac: data.bank_details ? data.bank_details.ac_no : "",
            bank_ac_name: data.bank_details ? data.bank_details.ac_name : "",
            bank_sort_code: data.bank_details ? data.bank_details.sort_code : "",
            logo_return: data.company_logo,
            fax: data.fax,
            vat_number: data.vat_number,
            surname: data.surname,
            isverifiedCheck: data.isverified,
            isverified: data.isverified,
            stausactive: data.status === 1 ? true : false,
            isverifiedstaus: data.isverified === 1 ? true : false
            // bank_becs: data.bank_details ? data.bank_details.becs : "",
            // bank_sepa: data.bank_details ? data.bank_details.sepa : "",
            // company_logo_name: data.company_logo && data.company_logo.substr(24, 30),
            // avatarName: data.avatar && data.avatar.substr(24, 30),
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/admin/agency/subscription/subscriptions",
      method: "POST",
      data: this.state.subscriptiontableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          currentlist: res.response.current,
          subscribedlist: res.response.history,
          // subscribedstartdate: res.response.history[0].start,
          // subscribedplan: res.response.history[0].plan,
          subscribedpages: res.response.fullcount,
          subscribedcurrPage: res.response.length
        });
      } else if (res.status === 0) {
        this.setState({ subscribedmsg: res.response });
      }
    });
    this.populateData();
  }
  populateData() {
    request({
      url: "/administrators/plans/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  handleChange = address => {
    this.setState({
      line1: "",
      line2: "",
      city: "",
      state: "",
      country: "",
      zipcode: ""
    });
    this.setState({ address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: ""
      });
    }
  };

  handleSelect = address => {
    this.setState({ address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name + " " + results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        this.setState({ address: results[0].formatted_address });
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };

  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2
    });
  };
  fileChangedHandler1 = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          company_logo: file_data,
          logo_file: URL.createObjectURL(file_data),
          company_logo_name: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  onEditAdmin = (e, values) => {
    if (this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else if (this.state.number1 === "") {
      this.setState({
        phoneerror1: true
      });
    } else {
      const data = new FormData();
      data.append("_id", this.props.location.state.rowid);
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      data.append("company_name", this.state.company_name);
      data.append("company_phone[code]", this.state.code1);
      data.append("company_phone[number]", this.state.number1);
      data.append("company_phone[dailcountry]", this.state.dailcountry1);
      data.append("company_email", this.state.company_email);
      data.append("company_logo", this.state.company_logo);
      data.append("postal_address", this.state.postal_address);
      data.append("company_description", this.state.company_description);
      data.append("registration_number", this.state.registration_number);
      data.append("organisation_type", this.state.organisation_type);
      data.append("fax", this.state.fax);
      data.append("vat_number", this.state.vat_number);
      data.append("surname", this.state.surname);
      data.append("bank_name", this.state.bank_name);
      data.append("bank_ac", this.state.bank_ac);
      data.append("bank_ac_name", this.state.bank_ac_name);
      data.append("bank_sort_code", this.state.bank_sort_code);
      // data.append("bank_becs", this.state.bank_becs);
      // data.append("bank_sepa", this.state.bank_sepa);
      data.append("status", this.state.status);
      data.append("isverified", this.state.isverified);
      request({
        url: "/administrators/agency/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            const toastId = "AgencyEdit";
            if (!toast.isActive(toastId)) {
              this.toastId = toast.success("Updated");
            }
            this.componentDidMount();
            this.setState({ src: null, croppedImageUrl: null });
            this.onClose();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  onClose = () => {
    this.setState({ propreview: null });
  };

  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };

  /* onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => this.setState({ src: reader.result }));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
  };

  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, "newFile.jpeg");
    this.setState({ croppedImageUrl: croppedImageUrl.url, company_logo: croppedImageUrl.file });
  };

  onCropChange = crop => {
    this.setState({ crop });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);
    return new Promise((resolve, reject) => {
      canvas.toBlob(file => {
        if (file) {
          file.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(file);
          resolve({ url: this.fileUrl, file: file });
        }
      }, "image/png");
    });
  }*/
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  paymentNow = list => {
    this.props.history.push({
      pathname: "/admin/subscription/account",
      state: {
        data: {
          agency_id: this.props.location.state.rowid,
          subid: list._id,
          plandetails: list.plan,
          username: this.state.token_username
        }
      }
    });
  };
  subscribeNow = id => {
    const sub = {
      id: this.props.location.state.rowid,
      planid: id
    };
    request({
      url: "/admin/agency/subscription/subscribe",
      method: "POST",
      data: sub
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result.status === 1) {
          if (res.auth_token) {
            localStorage.setItem("APUSA", res.auth_token);
            client.defaults.headers.common["Authorization"] = res.auth_token;
            // this.call.tokenagain();
            window.location.reload(true);
          }
          return toast.success("Free Trial Activated"), this.componentDidMount();
        } else if (res.response.result.status === 0) {
          return this.props.history.push({
            pathname: "/admin/subscription/account",
            state: {
              data: {
                agency_id: this.props.location.state.rowid,
                subid: res.response.result._id,
                plandetails: res.response.result.plan,
                username: this.state.token_username
              }
            }
          });
        }
      } else if (res.status === 2) {
        return toast.success("res.response");
      }
    });
  };
  viewpage = id => {
    return this.props.history.push({
      pathname: "/admin/viewagency",
      state: { rowid: id }
    });
  };
  render() {
    const current_date = new Date();
    // const { croppedImageUrl } = this.state;
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-note" />
                  Edit Agency
                  <div className="card-actions">
                    <button onClick={id => this.viewpage(this.props.location.state.rowid)}>
                      <i className="fa fa-eye" /> View Details
                      {/* <small className="text-muted" />*/}
                    </button>
                  </div>
                </CardHeader>
                <CardBody className="p-0">
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "1" })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Details
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "2" })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Subscriptions
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Contact Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Name</Label>
                              <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} minLength="2" value={this.state.name} required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Surname</Label>
                              <AvInput type="text" name="surname" placeholder="Enter " onChange={this.onChange} minLength="2" value={this.state.surname} required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvField name="email" label="Email" type="email" placeholder="Enter Email" value={this.state.email} onChange={this.onChange} required />
                          </Col>
                          <Col xs="12" md="4" className={"for-phone-right"}>
                            <Label>Tel</Label>
                            {this.state && this.state.dailcountry ? (
                                <IntlTelInput
                                    style={{ width: "100%" }}
                                    defaultCountry={this.state.dailcountry}
                                    utilsScript={libphonenumber}
                                    css={["intl-tel-input", "form-control"]}
                                    onPhoneNumberChange={this.handler}
                                    value={this.state.number}
                                />
                            ) : null}
                            {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                          </Col>

                          <Col xs="6" md="4">
                            {/* <Label for="exampleCustomFileBrowser">Profile Image</Label>
                      <CustomInput
                        type="file"
                        id="exampleCustomFileBrowser"
                        name="avatar"
                        onChange={this.fileChangedHandler}
                        label={this.state.avatarName ? this.state.avatarName : "Upload  Image"}
                        encType="multipart/form-data"
                      />*/}
                            <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                          </Col>
                          {this.state.propreview ? (
                              <Col xs="6" md="3">
                                <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                              </Col>
                          ) : null}
                          <Col xs="6" md="3">
                            <Label>Profile Image</Label>
                            <img
                                width="180px"
                                height="180px"
                                src={NodeURL + "/" + this.state.avatar_return}
                                alt="Profile"
                                onError={() => {
                                  this.setState({ avatar_return: "../../img/user-profile.png" });
                                }}
                            />
                          </Col>
                        </div>
                      </Row>
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Login Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Username</Label>
                              <AvInput type="text" name="username" placeholder="Enter Username" onChange={this.onChange} minLength="4" value={this.state.username} required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Password</Label>
                              <AvInput type="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password} />
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Confirm Password</Label>
                              <AvInput type="password" name="confirm_password" placeholder="Enter Password" value={this.state.confirm_password} onChange={this.onChange} validate={{ match: { value: "password" } }} />
                              <AvFeedback>Match Password!</AvFeedback>
                            </AvGroup>
                          </Col>
                        </div>
                      </Row>
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Company Information</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Company/Organisation Name</Label>
                              <AvInput name="company_name" type="text" placeholder="Enter Name" onChange={this.onChange} minlength="3" value={this.state.company_name} autoComplete="company_name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Email</Label>
                              <AvInput name="company_email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.company_email} autoComplete="company_email" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4" className={"for-phone-right"}>
                            <Label>Tel</Label>
                            {this.state && this.state.dailcountry1 ? (
                              <IntlTelInput
                                style={{ width: "100%" }}
                                defaultCountry={this.state.dailcountry1.toString()}
                                utilsScript={libphonenumber}
                                css={["intl-tel-input", "form-control"]}
                                onPhoneNumberChange={this.handler1}
                                value={this.state.number1}
                              />
                            ) : null}
                            {this.state.phoneerror1 ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Fax</Label>
                              <AvInput name="fax" type="number" placeholder="Enter Fax" onChange={this.onChange} minLength="3" value={this.state.fax} autoComplete="fax" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>

                          <Col xs="6" md="4">
                            <Label for="exampleCustomFileBrowser1">Logo</Label>
                            <CustomInput
                              type="file"
                              accept=".png, .jpg, .jpeg"
                              id="exampleCustomFileBrowser189"
                              name="avatar"
                              onChange={this.fileChangedHandler1}
                              label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                              encType="multipart/form-data"
                            />
                          </Col>
                          {this.state.logo_file ? (
                            <Col xs="6" md="3">
                              <Label>Preview</Label> <img className="img-fluid" src={this.state.logo_file} alt="Preview" />{" "}
                            </Col>
                          ) : null}
                          {this.state.logo_return ? (
                            <Col xs="6" md="4">
                              <img className="prof-image img-fluid" width="180px" height="140px" src={NodeURL + "/" + this.state.logo_return} alt="Preview" />
                            </Col>
                          ) : null}

                          <Col xs="12" md="12" className="mb-2">
                            <Label>Business Address</Label>
                            <PlacesAutocomplete value={this.state.address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                              {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                                <Fragment>
                                  <input
                                    {...getInputProps({
                                      placeholder: "Search Places",
                                      className: "form-control"
                                    })}
                                    required
                                  />
                                  <div className="autocomplete-dropdown-container absolute">
                                    {suggestions.map(suggestion => {
                                      const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                      // inline style for demonstration purpose
                                      const style = suggestion.active
                                        ? {
                                            backgroundColor: "#fafafa",
                                            cursor: "pointer"
                                          }
                                        : {
                                            backgroundColor: "#ffffff",
                                            cursor: "pointer"
                                          };
                                      return (
                                        <div
                                          {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                          })}
                                        >
                                          <span>{suggestion.description}</span>
                                        </div>
                                      );
                                    })}
                                  </div>
                                </Fragment>
                              )}
                            </PlacesAutocomplete>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Line1</Label>
                              <AvInput type="text" name="line1" placeholder="Enter Line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>City/Town</Label>
                              <AvInput type="text" name="line2" placeholder="Enter City/Town" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>State/Region</Label>
                              <AvInput type="text" name="state" placeholder="Enter State" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Country</Label>
                              <AvInput type="text" name="country" placeholder="Enter Country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Post Code</Label>
                              <AvInput type="text" name="zipcode" placeholder="Enter Post Code" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                        </div>
                      </Row>
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>
                                VAT No : <small>(If Available)</small>
                              </Label>
                              <AvInput type="number" name="vat_number" placeholder="Enter Number" onChange={this.onChange} min="0" value={this.state.vat_number} autoComplete="vat_number" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvField type="select" name="organisation_type" label="Organisation Type" onChange={this.onChange} value={this.state.organisation_type} required>
                              <option>Select</option>
                              <option value="limited_company">Limited Company</option>
                              <option value="not_for_profit">Not For Profit</option>
                              <option value="partnership">Partnership</option>
                              <option value="soletrader">Soletrader</option>
                            </AvField>
                          </Col>
                          {this.state.organisation_type === "limited_company" ? (
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Company Number</Label>
                                <AvInput type="number" name="registration_number" placeholder="Enter Number" onChange={this.onChange} min="0" value={this.state.registration_number} required autoComplete="registration_number" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                          ) : null}
                          <Col xs="12" md="6">
                            <AvGroup>
                              <Label>Organisation Description</Label>
                              <AvInput type="textarea" name="company_description" placeholder="Enter Description" onChange={this.onChange} value={this.state.company_description} required autoComplete="company_description" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="6">
                            <AvGroup>
                              <Label>Postal Address</Label>
                              <AvInput type="textarea" name="postal_address" placeholder="Enter Address" onChange={this.onChange} value={this.state.postal_address} required autoComplete="postal_address" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Bank Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvField
                              name="bank_name"
                              onChange={this.onChange}
                              value={this.state.bank_name}
                              placeholder="Enter Name"
                              label="Bank Name"
                              type="text"
                              errorMessage="This is required (Minimun 4 Letters)"
                              validate={{ required: { value: true }, minLength: { value: 4 } }}
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField
                              name="bank_ac_name"
                              onChange={this.onChange}
                              value={this.state.bank_ac_name}
                              placeholder="Enter AC.Name"
                              label="Account Name"
                              type="text"
                              errorMessage="This is required (Minimun 2 Letters)"
                              validate={{ required: { value: true }, minLength: { value: 2 } }}
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField
                              name="bank_sort_code"
                              onChange={this.onChange}
                              value={this.state.bank_sort_code}
                              placeholder="Enter Sort Code"
                              label="Sort Code"
                              type="number"
                              errorMessage="This is required (Minimun 6 Numbers)"
                              validate={{ required: { value: true }, minLength: { value: 6 } }}
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField
                              name="bank_ac"
                              onChange={this.onChange}
                              value={this.state.bank_ac}
                              placeholder="Enter AC.No"
                              label="Account Number"
                              type="number"
                              errorMessage="This is required (Minimun 8 Numbers)"
                              validate={{ required: { value: true }, minLength: { value: 8 } }}
                            />
                          </Col>

                          <Col xs="12" md="4">
                            <div>
                              <label>Status</label>
                            </div>
                            <Label className="switch switch-text switch-success new-switch">
                              <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                              <span className="switch-label" data-on="active" data-off="inactive" />
                              <span className="switch-handle new-handle" />
                            </Label>
                          </Col>
                          {this.state.isverifiedCheck === 0 ? (
                            <Col xs="12" md="4">
                              <div>
                                <label>Verified</label>
                              </div>
                              <Label className="switch switch-text switch-success new-switch">
                                <Input type="checkbox" className="switch-input" checked={this.state.isverifiedstaus} onChange={this.isverifiedChanges} />
                                <span className="switch-label" data-on="yes" data-off="no" />
                                <span className="switch-handle new-handle" />
                              </Label>
                            </Col>
                          ) : null}
                        </div>
                      </Row>
                    </TabPane>
                    <TabPane tabId="2">
                      <h4>Current Subscription</h4>
                      <Fragment>
                        {this.state.currentlist && this.state.currentlist.length > 0 ? (
                          <Row>
                            <Col md="12" xs="12">
                              <button className="btn btn-primary pull-right hover-drop" type="button">
                                Options <i className="fa fa-caret-down" />
                                <ul className="rel-postion">
                                  <li
                                    onClick={() => {
                                      this.props.history.push({
                                        pathname: "/admin/subscription/invoice",
                                        state: { rowid: this.props.location.state.rowid }
                                      });
                                    }}
                                  >
                                    <button type="button" className="btn ch-views">
                                      View Invoice
                                    </button>
                                  </li>
                                  <li
                                    onClick={() => {
                                      this.props.history.push({
                                        pathname: "/admin/subscription/editaccount",
                                        state: { rowid: this.props.location.state.rowid }
                                      });
                                    }}
                                  >
                                    <button type="button" className="btn ch-views">
                                      Billing Accounts
                                    </button>
                                  </li>
                                  <li
                                    onClick={() => {
                                      this.props.history.push({
                                        pathname: "/admin/subscription/invoicelist",
                                        state: { rowid: this.props.location.state.rowid }
                                      });
                                    }}
                                  >
                                    <button type="button" className="btn ch-views">
                                      View Statement
                                    </button>
                                  </li>
                                </ul>
                              </button>
                            </Col>
                            <Col md="12" xs="12">
                              <div className="table-responsive mt-2">
                                <table className="table table-hover subsc-table" size="sm">
                                  <thead>
                                    <tr>
                                      <th>Plan</th>
                                      <th>Trial Period</th>
                                      <th>Billing Period</th>
                                      <th>Dates</th>
                                      <th>Employees Limit</th>
                                      <th>Recruitment Module</th>
                                      <th>Amount</th>
                                      <th>Description</th>
                                      <th>Status</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {this.state.currentlist.map(list => (
                                      <tr key={list._id}>
                                        <td className="plan-power">{list.plan.name}</td>
                                        <td>{list.plan.freedays} Days</td>
                                        <td> {list.plan.noofdays === 30 ? "Monthly" : list.plan.noofdays === 365 ? "Yearly" : ""}</td>
                                        <td>
                                          {moment(list.start).format("DD-MM-YYYY")} - {moment(list.end).format("DD-MM-YYYY")}
                                        </td>
                                        <td>{list.plan.employees}</td>
                                        <td>{list.plan.recruitment_module === 1 ? "Yes" : "No"}</td>
                                        <td className="plan-power">£ {list.plan.amount}</td>
                                        <td>{list.plan.description}</td>
                                        <td>
                                          {" "}
                                          {list.payment_status === 0 || current_date >= list.end ? (
                                            <Button color="info" onClick={id => this.paymentNow(list)}>
                                              Pay Now
                                            </Button>
                                          ) : null}
                                          {list.payment_status === 1 ? <Badge color="success">Paid</Badge> : null}
                                        </td>
                                      </tr>
                                    ))}
                                  </tbody>
                                </table>
                              </div>
                            </Col>
                          </Row>
                        ) : (
                          <div className="text-center">
                            <div>
                              <h4 style={subh4}>
                                <p>
                                  <strong>Please Subscribe Any Plan.</strong>
                                </p>
                              </h4>
                            </div>
                          </div>
                        )}
                      </Fragment>
                      <hr />
                      <h4>Available Subscriptions</h4>
                      <div className="payment-headings">
                        <ul>
                          <li className="active">
                            {" "}
                            <span> 1 </span> Slect Plan{" "}
                          </li>
                          <li>
                            {" "}
                            <span> 2 </span> Slect Billing Account{" "}
                          </li>
                          <li>
                            {" "}
                            <span> 3 </span> Review & Pay{" "}
                          </li>
                        </ul>
                      </div>
                      <div className="price-subscribe">
                        <div className="price-category">
                          {this.state.adminlist && this.state.adminlist.length > 0 ? (
                            this.state.adminlist.map((item, i) => (
                              <div className="subscribe-lists" key={item._id}>
                                <div className="sub-design">
                                  <h4>
                                    {item.name}{" "}
                                    <p>
                                      Trial Period - <span>{item.freedays} days</span>
                                    </p>
                                  </h4>
                                  <h2>$ {item.amount}</h2>
                                  <div className="subs-lists">
                                    <p>
                                      Billing Period - <span> {item.noofdays === 30 ? "Monthly" : item.noofdays === 365 ? "Yearly" : ""}</span>
                                    </p>
                                  </div>
                                  <div className="subs-lists">
                                    <p>
                                      Employees Limit - <span>{item.employees}</span>
                                    </p>
                                  </div>
                                  <div className="subs-lists">
                                    <p>
                                      Recruitment Module - <span>{item.recruitment_module === 1 ? "Yes" : "No"}</span>
                                    </p>
                                  </div>
                                  <div className="subs-des">
                                    <p>{item.description}</p>
                                  </div>
                                  <div className="subs-buttons">
                                    <p>
                                      <button type="button" title="Subscribe Now" className="btn btn-primary subscribe-btn" color="info" id={`edit${i}`} onClick={id => this.subscribeNow(item._id)}>
                                        Subscribe Now
                                      </button>
                                      <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                        Subscribe Now
                                      </UncontrolledTooltip>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <tr className="text-center">
                              <td colSpan={9}>
                                <h2>
                                  <strong>
                                    <h4>No record available</h4>
                                  </strong>
                                </h2>
                              </td>
                            </tr>
                          )}
                        </div>
                      </div>
                      <nav className="float-right">
                        <div>
                          <Pagination
                            prevPageText="Prev"
                            nextPageText="Next"
                            firstPageText="First"
                            lastPageText="Last"
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.tableOptions.limit}
                            totalItemsCount={this.state.pages}
                            pageRangeDisplayed={this.state.pageRangeDisplayed}
                            onChange={this.paginate}
                          />
                        </div>
                      </nav>
                    </TabPane>
                  </TabContent>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i
                      className="fa fa-dot-circle-o"
                      onClick={() => {
                        this.phonefield(this.state.number);
                      }}
                    />{" "}
                    Update
                  </Button>
                  <Button type="button" color="secondary" title="Back" onClick={() => window.history.go(-1)}>
                    <i className="fa fa-arrow-left" /> Back
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editagency;
