/* eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, CustomInput } from "reactstrap";
import request from "../../../api/api";
import savenodify from "./agencylist";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress } from "react-places-autocomplete";
import Avatar from "react-avatar-edit";
// import ReactCrop from "react-image-crop";
// import "react-image-crop/dist/ReactCrop.css";

class Addagency extends Component {
  state = {
    username: "",
    name: "",
    password: "",
    confirm_password: "",
    email: "",
    number: "",
    code: "",
    number1: "",
    code1: "",
    company_email: "",
    company_logo: "",
    logo_file: "",
    company_logo_name: "",
    company_name: "",
    postal_address: "",
    company_description: "",
    registration_number: "",
    organisation_type: "",
    address: "",
    phoneerror: false,
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    loading: false,
    avatar: "",
    bank_name: "",
    bank_ac: "",
    bank_becs: "",
    bank_sepa: "",
    bank_ac_name: "",
    bank_sort_code: "",
    status: 1,
    isverified: 1,
    dailcountry: "",
    dailcountry1: "",
    countryCode: "",
    avatarfile: "",
    propreview: null,
    src: null,
    crop: {
      x: 120,
      y: 120,
      aspect: 16 / 9,
      maxWidth: 180,
      maxHeight: 180
    }
  };
  flash = new savenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
  }
  handleChange = address => {
    this.setState({
      line1: "",
      line2: "",
      city: "",
      state: "",
      country: "",
      zipcode: ""
    });
    this.setState({ address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: ""
      });
    }
  };

  handleSelect = address => {
    this.setState({ address: address, formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        const res_address = results[0].address_components;
        // console.log(results);
        if (res_address.length === 1) {
          this.setState({
            country: res_address[0].long_name
          });
        } else if (res_address.length === 2) {
          this.setState({
            state: res_address[0].long_name,
            country: res_address[1].long_name
          });
        } else if (res_address.length === 3) {
          this.setState({
            city: res_address[0].long_name,
            state: res_address[1].long_name,
            country: res_address[2].long_name
          });
        } else if (res_address.length === 4) {
          this.setState({
            city: res_address[0].long_name,
            state: res_address[2].long_name,
            country: res_address[3].long_name
          });
        } else if (res_address.length === 5) {
          this.setState({
            line1: res_address[0].long_name,
            line2: res_address[1].long_name,
            city: res_address[2].long_name,
            state: res_address[3].long_name,
            country: res_address[4].long_name
          });
        } else if (res_address.length === 6) {
          this.setState({
            line1: res_address[0].long_name,
            line2: res_address[1].long_name,
            city: res_address[2].long_name,
            state: res_address[3].long_name,
            country: res_address[4].long_name,
            zipcode: res_address[5].long_name
          });
        } else if (res_address.length === 7) {
          this.setState({
            line1: res_address[0].long_name,
            line2: res_address[1].long_name,
            city: res_address[3].long_name,
            state: res_address[4].long_name,
            country: res_address[5].long_name,
            zipcode: res_address[6].long_name
          });
        } else if (res_address.length === 8) {
          this.setState({
            line1: res_address[0].long_name + " " + res_address[1].long_name,
            line2: res_address[2].long_name,
            city: res_address[3].long_name,
            state: res_address[4].long_name,
            country: res_address[5].long_name,
            zipcode: res_address[6].long_name
          });
        } else {
          this.setState({
            line1: res_address[0].long_name,
            line2: res_address[1].long_name,
            city: res_address[res_address.length - 3].long_name,
            state: res_address[res_address.length - 2].long_name,
            country: res_address[res_address.length - 1].long_name
          });
        }
        this.setState({ formatted_address: results[0].formatted_address });
        this.setState({ lat: results[0].geometry.location.lat });
        this.setState({ lon: results[0].geometry.location.lon });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2
    });
  };

  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2
    });
  };
  fileChangedHandler1 = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          company_logo: file_data,
          logo_file: URL.createObjectURL(file_data),
          company_logo_name: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    if (this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else if (this.state.number1 === "") {
      this.setState({
        phoneerror1: true
      });
    } else {
      const data = new FormData();
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("surname", this.state.surname);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      data.append("company_name", this.state.company_name);
      data.append("company_phone[code]", this.state.code1);
      data.append("company_phone[number]", this.state.number1);
      data.append("company_phone[dailcountry]", this.state.dailcountry1);
      data.append("company_email", this.state.company_email);
      data.append("company_logo", this.state.company_logo);
      data.append("postal_address", this.state.postal_address);
      data.append("company_description", this.state.company_description);
      data.append("registration_number", this.state.registration_number);
      data.append("organisation_type", this.state.organisation_type);
      data.append("fax", this.state.fax);
      data.append("vat_number", this.state.vat_number);
      data.append("bank_name", this.state.bank_name);
      data.append("bank_ac", this.state.bank_ac);
      data.append("bank_ac_name", this.state.bank_ac_name);
      data.append("bank_sort_code", this.state.bank_sort_code);
      // data.append("bank_becs", this.state.bank_becs);
      // data.append("bank_sepa", this.state.bank_sepa);
      data.append("status", this.state.status);
      data.append("isverified", this.state.isverified);
      request({
        url: "/administrators/agency/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.error(error);
        });
    }
  };
  saveuser = () => {
    return this.setState({ number: "", address: "" }), this.form && this.form.reset(), this.props.history.push("/admin/agencylist"), this.flash.adminsavenodify();
  };

  /* onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => this.setState({ src: reader.result }));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
  };

  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, "newFile.jpeg");
    this.setState({ croppedImageUrl: croppedImageUrl.url, company_logo: croppedImageUrl.file });
  };

  onCropChange = crop => {
    this.setState({ crop });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);
    return new Promise((resolve, reject) => {
      canvas.toBlob(file => {
        if (file) {
          file.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(file);
          resolve({ url: this.fileUrl, file: file });
        }
      }, "image/jpeg");
    });
  }*/

  render() {
    // const { croppedImageUrl } = this.state;
    return (
      <div className="animated fadeIn">
        {this.state.loading ? <div className="loading">Loading&#8230;</div> : null}
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Add Agency
              </CardHeader>

              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
                  <Row>
                    <Col xs="12" md="12">
                      <div className="cus-design edited-section">
                        <Col xs="12" md="12">
                          <p className="h5">Contact Details</p>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="name"
                            onChange={this.onChange}
                            value={this.state.name}
                            placeholder="Enter name"
                            label="First Name"
                            type="text"
                            errorMessage="This is required (Minimun 4 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 4 } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="surname"
                            onChange={this.onChange}
                            value={this.state.surname}
                            placeholder="Enter"
                            label="Surname"
                            type="text"
                            errorMessage="This is required (Minimun 4 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 4 } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Email</Label>
                            <AvInput name="email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4" className={"for-phone-right"}>
                          <Label>Tel</Label>
                          <IntlTelInput style={{ width: "100%" }} defaultCountry={"gb"} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler} value={this.state.number} />
                          {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                        </Col>

                        <Col xs="6" md="4">
                          <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                        </Col>
                        {this.state.propreview ? (
                          <Col xs="6" md="3">
                            <Label>Preview</Label> <img width="180px" height="180px" className="img-fluid" src={this.state.propreview} alt="Preview" />{" "}
                          </Col>
                        ) : null}
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="12">
                      <div className="cus-design edited-section">
                        <Col xs="12" md="12">
                          <p className="h5">Login Details</p>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="username"
                            onChange={this.onChange}
                            value={this.state.username}
                            placeholder="Enter username"
                            label="Username"
                            type="text"
                            errorMessage="This is required (Minimun 4 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 4 } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Password</Label>
                          <AvInput type="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password} required autoComplete="off" />
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Confirm Password</Label>
                            <AvInput
                              type="password"
                              name="confirm_password"
                              placeholder="Enter Password"
                              onChange={this.onChange}
                              value={this.state.confirm_password}
                              required
                              validate={{ match: { value: "password" } }}
                              autoComplete="off"
                            />
                            <AvFeedback>Match Password!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="12">
                      <div className="cus-design edited-section">
                        <Col xs="12" md="12">
                          <p className="h5">Company Information</p>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Company/Organisation Name</Label>
                            <AvInput name="company_name" type="text" placeholder="Enter Name" onChange={this.onChange} minLength="3" value={this.state.company_name} autoComplete="company_name" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Email Address</Label>
                            <AvInput name="company_email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.company_email} autoComplete="company_email" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4" className={"for-phone-right"}>
                          <Label>Tel</Label>
                          <IntlTelInput style={{ width: "100%" }} defaultCountry={"gb"} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler1} value={this.state.number1} />
                          {this.state.phoneerror1 ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Fax</Label>
                            <AvInput name="fax" type="number" placeholder="Enter Fax" onChange={this.onChange} minLength="3" value={this.state.fax} autoComplete="fax" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>

                        <Col xs="6" md="4">
                          <Label for="exampleCustomFileBrowser1">Logo</Label>
                          {/* <CustomInput type="file" id="exampleCustomFileBrowser189" name="avatar" onChange={this.onSelectFile} label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"} encType="multipart/form-data" />*/}
                          <CustomInput
                            type="file"
                            accept=".png, .jpg, .jpeg"
                            id="exampleCustomFileBrowser189"
                            name="avatar"
                            onChange={this.fileChangedHandler1}
                            label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                            encType="multipart/form-data"
                          />
                        </Col>
                        {/* <Col xs="6" md="4">
                      {this.state.src && <ReactCrop src={this.state.src} crop={this.state.crop} onImageLoaded={this.onImageLoaded} onComplete={this.onCropComplete} onChange={this.onCropChange} />}
                    </Col>
                    <Col xs="6" md="4">
                      {croppedImageUrl && <img width="100%" height="180px" alt="Crop" src={croppedImageUrl} />}{" "}
                    </Col>*/}
                        {this.state.logo_file ? (
                          <Col xs="6" md="3">
                            <Label>Preview</Label> <img className="img-fluid" src={this.state.logo_file} alt="Preview" />{" "}
                          </Col>
                        ) : null}

                        <Col xs="12" md="12" className="mb-2">
                          <Label>Business Address</Label>
                          <PlacesAutocomplete value={this.state.address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                            {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                              <Fragment>
                                <input
                                  {...getInputProps({
                                    placeholder: "Search Places",
                                    className: "form-control"
                                  })}
                                  required
                                />
                                <div className="autocomplete-dropdown-container absolute">
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                          backgroundColor: "#fafafa",
                                          cursor: "pointer"
                                        }
                                      : {
                                          backgroundColor: "#ffffff",
                                          cursor: "pointer"
                                        };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </Fragment>
                            )}
                          </PlacesAutocomplete>
                        </Col>

                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Line1</Label>
                            <AvInput type="text" name="line1" placeholder="Enter Line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>City/Town</Label>
                            <AvInput type="text" name="line2" placeholder="Enter City/Town" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>State/Region</Label>
                            <AvInput type="text" name="state" placeholder="Enter State" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>

                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Country</Label>
                            <AvInput type="text" name="country" placeholder="Enter Country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Post Code</Label>
                            <AvInput type="text" name="zipcode" placeholder="Enter Post Code" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </div>
                    </Col>
                  </Row><Row>
                    <Col xs="12" md="12">
                      <div className="cus-design edited-section">
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>
                              VAT No : <small>(If Available)</small>
                            </Label>
                            <AvInput type="number" name="vat_number" placeholder="Enter Number" onChange={this.onChange} min="0" value={this.state.vat_number} autoComplete="vat_number" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="select" name="organisation_type" label="Organisation Type" onChange={this.onChange} value={this.state.organisation_type} required>
                            <option>Select</option>
                            <option value="limited_company">Limited Company</option>
                            <option value="not_for_profit">Not For Profit</option>
                            <option value="partnership">Partnership</option>
                            <option value="soletrader">Soletrader</option>
                          </AvField>
                        </Col>
                        {this.state.organisation_type === "limited_company" ? (
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Company Number</Label>
                              <AvInput type="number" name="registration_number" placeholder="Enter Number" onChange={this.onChange} min="0" value={this.state.registration_number} required autoComplete="registration_number" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                        ) : null}
                        <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Organisation Description</Label>
                            <AvInput type="textarea" name="company_description" placeholder="Enter Description" onChange={this.onChange} value={this.state.company_description} required autoComplete="company_description" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="6">
                          <AvGroup>
                            <Label>Postal Address</Label>
                            <AvInput type="textarea" name="postal_address" placeholder="Enter Address" onChange={this.onChange} value={this.state.postal_address} required autoComplete="postal_address" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </div>
                    </Col>
                  </Row>

                  <Row>
                    <Col xs="12" md="12">
                      <div className="cus-design edited-section">
                        <Col xs="12" md="12">
                          <Label>Bank Details</Label>
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="bank_name"
                            onChange={this.onChange}
                            value={this.state.bank_name}
                            placeholder="Enter Name"
                            label="Bank Name"
                            type="text"
                            errorMessage="This is required (Minimun 4 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 4 } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="bank_ac_name"
                            onChange={this.onChange}
                            value={this.state.bank_ac_name}
                            placeholder="Enter AC.Name"
                            label="Account Name"
                            type="text"
                            errorMessage="This is required (Minimun 2 Letters)"
                            validate={{ required: { value: true }, minLength: { value: 2 } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="bank_sort_code"
                            onChange={this.onChange}
                            value={this.state.bank_sort_code}
                            placeholder="Enter Sort Code"
                            label="Sort Code"
                            type="number"
                            errorMessage="This is required (Minimun 6 Numbers)"
                            validate={{ required: { value: true }, minLength: { value: 6 } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            name="bank_ac"
                            onChange={this.onChange}
                            value={this.state.bank_ac}
                            placeholder="Enter AC.No"
                            label="Account Number"
                            type="number"
                            errorMessage="This is required (Minimun 8 Numbers)"
                            validate={{ required: { value: true }, minLength: { value: 8 } }}
                          />
                        </Col>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    title="Add Agency"
                    onClick={() => {
                      this.phonefield(this.state.number);
                    }}
                  >
                    Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addagency;
