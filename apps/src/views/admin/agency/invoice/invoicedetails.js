/*eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { toast, ToastContainer } from "react-toastify";
// import { Link } from "react-router-dom";
import {
  Row,
  Col,
  Button,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Label
  //  Table,
  //   UncontrolledTooltip
} from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import moment from "moment";
import request from "../../../../api/api";

class Invoicedetails extends Component {
  state = {
    client_rate: "",
    agency_details: "",
    branch: "",
    client_details: "",
    employee_details: "",
    employee_rate: "",
    job_type: "",
    locations_details: "",
    starttime: "",
    endtime: "",
    late_amount: "",
    notes: {},
    rate_details: [],
    start_date: "",
    shift_id: "",
    breaktime: "",
    holded: "",
    total_duration: "",
    started_time: "",
    _id: "",
    ended_time: "",
    ForEdit: false
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/admin/agencyinvoice/view",
      method: "POST",
      data: { id: this.props.location.state.id }
    }).then(res => {
        if (res.status === 1) {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          let evestarthr = res.response.result[0].starttime / 3600;
          let splithr = evestarthr.toString().split(".");
          let startsec = splithr[1];
          let startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
          let starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          let eveendhr = res.response.result[0].endtime / 3600;
          let splitendhr = eveendhr.toString().split(".");
          let endsec = splitendhr[1];
          let endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          let endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          let ConvertedRateDetails = res.response.result[0].rate_details.map(list => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            let evestarthr = list.from / 3600;
            let splithr = evestarthr.toString().split(".");
            let startsec = splithr[1];
            let startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
            let starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            let eveendhr = list.to / 3600;
            let splitendhr = eveendhr.toString().split(".");
            let endsec = splitendhr[1];
            let endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
            let endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            let day,
              period = "";
            let Shift_week = list.name.slice(0, 3).toLowerCase();
            let words = list.name;
            let Shift_period = words.substr(words.length - 3);
            if (Shift_period === "day") {
              period = "Morning";
            } else if (Shift_period === "ght") {
              period = "Night";
            }
            switch (Shift_week) {
              case "mon":
                day = "Monday";
                break;
              case "tue":
                day = "Tuesday";
                break;
              case "wed":
                day = "Wednesday";
                break;
              case "thu":
                day = "Thursday";
                break;
              case "fri":
                day = "Friday";
                break;
              case "sat":
                day = "Saturday";
                break;
              case "sun":
                day = "Sunday";
                break;
              case "pub":
                day = "Public Holiday";
                break;
              default:
                day = "";
                break;
            }
            return {
              _id: list._id,
              from: starttimehr,
              to: endtimehr,
              day: day,
              period: period,
              name: list.name,
              client_rate: list.client_rate,
              employee_rate: list.employee_rate
            };
          });
          this.setState({
            _id: res.response.result[0]._id,
            status: res.response.result[0].status,
            agency_details: res.response.result[0].agency_details[0].name,
            shift_id: res.response.result[0].shift_id,
            branch: res.response.result[0].branch_data[0].branches.branchname,
            client_details: res.response.result[0].client_details[0].companyname,
            employee_details: res.response.result[0].employee_details[0].name,
            client_rate: res.response.result[0].client_rate,
            employee_rate: res.response.result[0].employee_rate,
            job_type: res.response.result[0].job_type,
            late_amount: res.response.result[0].late_amount,
            notes: res.response.result[0].notes,
            rate_details: ConvertedRateDetails,
            starttime: starttimehr,
            endtime: endtimehr,
            start_date: res.response.result[0].start_date,
            started_time: moment(res.response.result[0].shift_details[0].timesheet[0].start).format("HH:mm"),
            ended_time: moment(res.response.result[0].shift_details[0].timesheet[0].end).format("HH:mm"),
            breaktime: res.response.result[0].shift_details[0].breaktime / 60,
            holded: res.response.result[0].shift_details[0].timesheet[0].holded ? res.response.result[0].shift_details[0].timesheet[0].holded : 0,
            total_duration: res.response.result[0].shift_details[0].timesheet[0].workmins / 60,
            locations_details: res.response.result[0].locations_details[0].name 
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => toast.error(err));
  }
  
  backTopage = () => {
    window.history.go(-1);
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    const adminform = {
      id: this.state._id,
      employee_rate: this.state.employee_rate,
      client_rate: this.state.client_rate,
      late_amount: this.state.late_amount
    };
    request({
      url: "/admin/agencyinvoicecycle/update",
      method: "POST",
      data: adminform
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({ ForEdit: false });
          this.componentDidMount();
          toast.success("Updated !");
          setTimeout(() => {
            this.componentDidMount();
          }, 200);
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  render() {
    return (
      <Fragment>
        <div className="animated">
          <ToastContainer position="top-right" autoClose={2500} />
          <Card>
            <CardHeader>
              <i className="icon-list" />
              Invoice Details
            </CardHeader>
            <CardBody>
              <Fragment>
                <ul className="invoice-view">
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Shift <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.shift_id} </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Client <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.client_details} </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Location <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.locations_details} </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Branch <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.branch} </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Employee <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.employee_details} </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Job Role <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.job_type} </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Date
                      <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc">
                      {" "}
                      {moment(this.state.start_date).format("DD-MM-YYYY")} ({moment(this.state.start_date).format("dddd")})
                    </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Time <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc">
                      {" "}
                      {this.state.starttime} - {this.state.endtime}{" "}
                    </span>
                  </li>
                  <li>
                    <span className="inv-heading">
                      {" "}
                      Break <span className="mvr-space"> : </span>{" "}
                    </span>{" "}
                    <span className="inv-desc"> {this.state.breaktime} Minutes </span>
                  </li>
                  {/* <li><span className="inv-heading"> Duration <span className="mvr-space"> : </span> </span> <span className="inv-desc">  05:00 PM </span></li> */}
                  <li className="invoice-widths">
                    <div className="days-durations">
                      <h5>Job Rate Details</h5>
                      {this.state.rate_details.map(list => (
                        <Fragment>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              Day <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              {" "}
                              {list.day} ({list.period}){" "}
                            </span>
                          </div>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              From - To <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              {" "}
                              {list.from} - {list.to}
                            </span>
                          </div>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              Client Rate <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              £ {list.client_rate} <small> (per hour)</small>
                            </span>
                          </div>
                          <div className="days-dura">
                            <span className="inv-heading">
                              {" "}
                              Employee Rate <span className="mvr-space"> : </span>{" "}
                            </span>{" "}
                            <span className="inv-desc">
                              £ {list.employee_rate} <small> (per hour)</small>
                            </span>
                          </div>
                        </Fragment>
                      ))}
                    </div>
                  </li>
                  <li className="invoice-widths">
                    <div className="days-durations">
                      <h5>Worked Details</h5>
                      <div className="days-dura">
                        <span className="inv-heading">
                          {" "}
                          Time <span className="mvr-space"> : </span>{" "}
                        </span>{" "}
                        <span className="inv-desc">
                          {" "}
                          {this.state.started_time} - {this.state.ended_time}{" "}
                        </span>
                      </div>
                      <div className="days-dura">
                        <span className="inv-heading">
                          {" "}
                          Holded <span className="mvr-space"> : </span>{" "}
                        </span>{" "}
                        <span className="inv-desc"> {this.state.holded} Minutes</span>
                      </div>
                      <div className="days-dura">
                        <span className="inv-heading">
                          {" "}
                          Duration <span className="mvr-space"> : </span>{" "}
                        </span>{" "}
                        <span className="inv-desc"> {Math.round(this.state.total_duration)} Minutes</span>
                      </div>
                      {/* <div className="days-dura"><span className="inv-heading"> Duration <span className="mvr-space"> : </span> </span> <span className="inv-desc">  05:00 PM </span></div> */}
                    </div>
                  </li>
                </ul>
                <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                  <Row>
                    <Col xs="12" md="3">
                      <Label>Client Rate :</Label>
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.ForEdit && this.state.status === 1 ? (
                        <AvGroup>
                          <AvInput
                            type="number"
                            min="0"
                            name="client_rate"
                            placeholder="Enter Client Rate"
                            onChange={this.onChange}
                            value={this.state.client_rate}
                            required
                            autoComplete="name"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      ) : (
                        <Fragment>£ {this.state.client_rate}</Fragment>
                      )}
                    </Col>
                    <Col xs="12" md="3">
                      <Label>Employee Rate :</Label>
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.ForEdit && this.state.status === 1 ? (
                        <AvGroup>
                          <AvInput
                            type="number"
                            min="0"
                            name="employee_rate"
                            placeholder="Enter Late Amount"
                            onChange={this.onChange}
                            value={this.state.employee_rate}
                            required
                            autoComplete="name"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      ) : (
                        <Fragment>£ {this.state.employee_rate} </Fragment>
                      )}
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="3">
                      <Label>Late Booking Fee :</Label>
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.ForEdit && this.state.status === 1 ? (
                        <AvGroup>
                          <AvInput
                            type="number"
                            min="0"
                            name="late_amount"
                            placeholder="Enter Late Amount"
                            onChange={this.onChange}
                            value={this.state.late_amount}
                            required
                            autoComplete="name"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      ) : (
                        <Fragment>£ {this.state.late_amount} </Fragment>
                      )}
                    </Col>
                  </Row>
                  {this.state.ForEdit && this.state.status === 1 ? (
                    <Row>
                      <Col className="d-flex justify-content-end">
                        <Button type="submit" color="success" title="Back" onClick={this.EditInvoice}>
                          Update
                        </Button>
                      </Col>
                    </Row>
                  ) : null}
                </AvForm>
              </Fragment>
            </CardBody>
            <CardFooter>
                <Button type="button" color="secondary" title="Back" onClick={this.backTopage}>
                  <i className="fa fa-arrow-left" /> Back
                </Button>
              {this.state.status === 1 ? (
                <Fragment>
                  {/* <Button type="button" size="md" color="success" className={"pull-right"} title="Back" onClick={this.approve}>
                    <i className="fa fa-check" /> Approve
                  </Button> */}
                  <Button
                    type="button"
                    color="info"
                    className={"pull-right mr-5"}
                    title="Back"
                    onClick={() => {
                      this.setState({ ForEdit: !this.state.ForEdit });
                    }}
                  >
                    <i className="fa fa-edit" /> Edit
                  </Button>
                </Fragment>
              ) : null}
            </CardFooter>
          </Card>
        </div>
      </Fragment>
    );
  }
}

export default Invoicedetails;
