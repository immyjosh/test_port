/* eslint no-sequences: 0*/
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button, Card, CardBody, CardFooter, CardHeader, Col, Row, Input, Popover,
  PopoverHeader,
  PopoverBody,
  InputGroup,
  InputGroupAddon
} from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import { Bar } from "react-chartjs-2";
import Widget02 from "../../Template/Widgets/Widget02";
import FileSaver from "file-saver";
import jwt_decode from 'jwt-decode';

class Viewagency extends Component {
  state = {
    username: "",
    password: "",
    name: "",
    email: "",
    confirm_password: "",
    conform: "",
    number: "",
    code: "",
    number1: "",
    code1: "",
    company_email: "",
    company_logo: "",
    logo_file: "",
    company_logo_name: "",
    company_name: "",
    logo_return: "",
    postal_address: "",
    company_description: "",
    registration_number: "",
    organisation_type: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    phoneerror: false,
    stausactive: false,
    isverifiedstaus: false,
    avatar: "",
    status: "",
    avatar_return: "",
    isverifiedCheck: "",
    isverified: "",
    avatarName: "",
    bank_name: "",
    bank_ac: "",
    bank_becs: "",
    bank_sepa: "",
    dailcountry: "",
    dailcountry1: "",
    propreview: null,
    src: null,
    agency: this.props.location.state.rowid,
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all",
    agencydashboard: false,
    added: 0,
    accepted: 0,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    timehseet_approved: 0,
    invoice_approved: 0,
    Payment_completed: 0,
    expired: 0,
    totalshiftCount: 0,
    draft: 0,
    approved: 0,
    part_paid: 0,
    paid: 0,
    draft_rate: 0,
    approved_rate: 0,
    part_paid_rate: 0,
    paid_rate: 0,
    mailpopover: false,
    recepient: ""
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    request({
      url: "/administrators/agency/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.response.result && res.response.result.length > 0) {
      this.setState({ adminlist: res.response.result[0] });
        this.setState({
          username: res.response.result[0].username,
          name: res.response.result[0].name,
          email: res.response.result[0].email,
          status: res.response.result[0].status,
          number: res.response.result[0].phone.number,
          code: res.response.result[0].phone.code,
          dailcountry: res.response.result[0].phone ? res.response.result[0].phone.dailcountry : "gb",
          line1: res.response.result[0].address.line1,
          line2: res.response.result[0].address.line2,
          city: res.response.result[0].address.city,
          state: res.response.result[0].address.state,
          country: res.response.result[0].address.country,
          zipcode: res.response.result[0].address.zipcode,
          address: res.response.result[0].address.formatted_address,
          company_name: res.response.result[0].company_name,
          company_description: res.response.result[0].company_description,
          number1: res.response.result[0].company_phone ? res.response.result[0].company_phone.number : "07410",
          code1: res.response.result[0].company_phone ? res.response.result[0].company_phone.code : "44",
          dailcountry1: res.response.result[0].company_phone ? res.response.result[0].company_phone.dailcountry : "gb",
          postal_address: res.response.result[0].postal_address,
          organisation_type: res.response.result[0].organisation_type,
          company_email: res.response.result[0].company_email,
          registration_number: res.response.result[0].registration_number,
          avatar_return: res.response.result[0].avatar,
          bank_name: res.response.result[0].bank_details ? res.response.result[0].bank_details.name : "",
          bank_ac: res.response.result[0].bank_details ? res.response.result[0].bank_details.ac_no : "",
          bank_becs: res.response.result[0].bank_details ? res.response.result[0].bank_details.becs : "",
          bank_sepa: res.response.result[0].bank_details ? res.response.result[0].bank_details.sepa : "",
          logo_return: res.response.result[0].company_logo,
          company_logo_name: res.response.result[0].company_logo && res.response.result[0].company_logo.substr(24, 30),
          isverifiedCheck: res.response.result[0].isverified,
          isverified: res.response.result[0].isverified,
          avatarName: res.response.result[0].avatar && res.response.result[0].avatar.substr(24, 30)
        });
      }
    });
    this.dashboard();
  }
  editpage = (e, id) => {
    e.stopPropagation();
    return this.props.history.push({
      pathname: "/admin/editagency",
      state: {
        rowid: this.props.location.state.rowid
      }
    });
  };
  dashboard() {
    const { agency } = this.state;
    if (agency) {
      request({
        url: "/administrators/agency/dashboard",
        method: "POST",
        data: {
          agency,
          selectshift: this.state.selectshift,
          selecttimesheet: this.state.selecttimesheet,
          selectinvoice: this.state.selectinvoice
        }
      }).then(res => {
        if (res.status === 1) {
          if (res.response.result && res.response.result.length > 0) {
            const agencydashboard = res.response.result[0] || false;
            this.setState({ agencydashboard });
            if (agencydashboard && agencydashboard.shifts) {
              const data = agencydashboard.shifts.length > 0 ? agencydashboard.shifts[0] : {};
              this.setState({
                added: data.added || 0,
                accepted: data.accepted || 0,
                assigned: data.assigned || 0,
                completed: data.completed || 0,
                ongoing: data.ongoing || 0,
                timehseet_approved: data.timehseet_approved || 0,
                invoice_approved: data.invoice_approved || 0,
                Payment_completed: data.Payment_completed || 0,
                expired: data.expired || 0,
                totalshiftCount: (data.added ? data.added : 0) + (data.accepted ? data.accepted : 0) + (data.assigned ? data.assigned : 0) + (data.completed ? data.completed : 0) + (data.ongoing ? data.ongoing : 0) + (data.timehseet_approved ? data.timehseet_approved : 0) + (data.invoice_approved ? data.invoice_approved : 0) + (data.Payment_completed ? data.Payment_completed : 0) + (data.expired ? data.expired : 0)
              });
            }
            if (agencydashboard && agencydashboard.invoices) {
              const data = agencydashboard.invoices.length > 0 ? agencydashboard.invoices[0] : {};
              this.setState({
                draft: data.draft || 0,
                approved: data.approved || 0,
                part_paid: data.part_paid || 0,
                paid: data.paid || 0,
                draft_rate: data.draft_rate || 0,
                approved_rate: data.approved_rate || 0,
                part_paid_rate: data.part_paid_rate || 0,
                paid_rate: data.paid_rate || 0
              });
            }
          }
        }
      });
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.dashboard());
  };
  newPage = page => {
    if (page === "Invoice") {
      this.props.history.push({
        pathname: "/admin/agency/invoice",
        state: {
          rowid: this.props.location.state.rowid
        }
      });
    }
    if (page === "Time") {
      this.props.history.push({
        pathname: "/admin/agency/timesheet",
        state: {
          rowid: this.props.location.state.rowid
        }
      });
    }
  };
  downloadPDF = purpose => {
    client.defaults.responseType = "blob";
    let toastId = "downmail0";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/administrators/agency/profile",
      method: "POST",
      data: { id: this.props.location.state.rowid, purpose }
    }).then(res => {
      if (res && res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      } else if (res) {
        const file = new Blob([res], { type: "application/pdf" });
        const name = this.state.name ? `${this.state.name} Profile.pdf` : "Profile.pdf";
        FileSaver(file, name);
        toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      }
      client.defaults.responseType = "json";
    }).catch(error => {
      toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      client.defaults.responseType = "json";
    });
  };
  mailPDF = purpose => {
    if (this.state.recepient) {
      let toastId = "downmail";
      toastId = toast.info("Sending......", { autoClose: false });
      request({
        url: "/administrators/agency/profile",
        method: "POST",
        data: { id: this.props.location.state.rowid, purpose, email: this.state.recepient }
      }).then(res => {
        if (res && res.status === 1) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.setState({
            mailpopover: false
          });
          this.componentDidMount();
        } else {
          toast.update(toastId, { render: res.response || "Unable To Send Mail, Try Again Later", type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      }).catch(error => {
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
    } else {
      toast.error("Recepient Mail ID Required");
    }
  };
  MailPopOver = () => {
    this.setState({
      mailpopover: !this.state.mailpopover
    });
  };
  actAsAgency = (e, id) => {
    e.stopPropagation();
    const tabOpen = window.open("about:blank", "newtab");
    request({
      url: "/administrators/agency/actasagency",
      method: "POST",
      data: { id: id }
    })
        .then(res => {
          if (res.status === 1) {
            localStorage.setItem("APUSA", res.response.auth_token);
            client.defaults.headers.common["Authorization"] = res.response.auth_token;
            let tokenAgency;
            if (res.response.auth_token) {
              tokenAgency = jwt_decode(res.response.auth_token);
            }
            if (tokenAgency.subscription === 1) {
              return (
                  (tabOpen.location = "/portal/agency/dashboard"),
                      setTimeout(() => {
                        window.location.reload();
                      }, 800)
              );
            } else if (tokenAgency.subscription === 0) {
              return (
                  (tabOpen.location = "/portal/agency/subscribe"),
                      setTimeout(() => {
                        window.location.reload();
                      }, 800)
              );
            }
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(err => {
          toast.error("No User Found!");
        });
  };
  render() {
    const { state } = this;
    const { agencydashboard } = this.state;
    const bar = {
      labels: ["Shifts"],
      datasets: [
        {
          label: "Total",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.totalshiftCount]
        },
        {
          label: "Assigned",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.assigned]
        },
        {
          label: "Ongoing",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.ongoing]
        },
        {
          label: "Completed",
          backgroundColor: "#4dbd74",
          borderColor: "#4dbd74",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#4dbd74",
          data: [this.state && this.state.completed]
        }
      ]
    };
    const bar1 = {
      labels: ["Invoices"],
      datasets: [
        {
          label: "Draft",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.draft_rate]
        },
        {
          label: "Approved Rate",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.approved_rate]
        },
        {
          label: "Part Paid",
          backgroundColor: "#20a8d8b8",
          borderColor: "#1382a9",
          borderWidth: 1,
          hoverBackgroundColor: "#20a8d8",
          hoverBorderColor: "#1382a9",
          data: [this.state && this.state.part_paid_rate]
        },
        {
          label: "Paid",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.paid_rate]
        }
      ]
    };
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="fa fa-desktop" />
                Agency Details
                <div className="card-actions">
                  <button onClick={() => this.newPage("Time")}>
                    <i className="icon-clock" /> Timesheets
                  <small className="text-muted" />
                  </button>
                  <button onClick={() => this.newPage("Invoice")}>
                    <i className="fa fa-money" /> Invoice
                  <small className="text-muted" />
                  </button>
                  <button onClick={this.editpage}>
                    <i className="fa fa-edit" /> Edit
                    <small className="text-muted" />
                  </button>
                </div>
              </CardHeader>
              <CardBody>
                <div className="views-information">
                  <div className="info-categories">
				  
				  <div className="row">
                      
					  <div className="emp-full">
                      <div className="cus-design">
					  <div className="cate-ranges">
                          <h4>Personal Details</h4>
                        </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <img
                            width="180px"
                            height="180px"
                            src={NodeURL + "/" + this.state.avatar_return}
                            alt="Profile"
                            onError={() => {
                              this.setState({ avatar_return: "../../img/user-profile.png" });
                            }}
                          />
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                         {/* <p>
                            <span className="left-views">
                              Username <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.username}</span>
                          </p>*/}
                          <p>
                            <span className="left-views">
                              Name <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.name}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Surname <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.surname}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Tel <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{`${state.code} ${state.number}`}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Email <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.email}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Status <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.status === 1 ? "Active" : "In-Active"}</span>
                          </p>
                        </div>
                      </div>
					   </div>
                      </div>
                    </div>
					
				  
                    <div className="row">
					
                      <div className="col-md-12">
					  <div className="cus-design">
                        <div className="cate-ranges">
                          <h4 className="dash-foheading no-bar"><span className="pull-left left-dash"> Dashboard Details </span>
                          <span className="pull-right right-dash">
						  <Button  title="Act As Agency" className="pull-right ml-3 btn-ask" onClick={(e, id) => this.actAsAgency(e, this.props.location.state.rowid)}>
                            <i className="fa fa-sign-in" /> Act As Agency
                          </Button>
                            <Button title="Download" className="pull-right btn-download ml-3" onClick={() => this.downloadPDF("download")}>
                            <i className="fa fa-download" /> Download
                          </Button>
                          <Button title="Mail" className="pull-right  btn-mail ml-3" id="mailPopover" onClick={this.MailPopOver}>
                            <i className="fa fa-envelope-o" /> Mail
                          </Button></span>
                          </h4>

                          <Popover placement="left" isOpen={this.state.mailpopover} target="mailPopover" toggle={this.MailPopOver}>
                            <PopoverHeader>Send Mail</PopoverHeader>
                            <PopoverBody>
                              <Row className="m-2">
                                <InputGroup>
                                  <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                                  <Input placeholder="E-mail" name="recepient" value={this.state.recepient} onChange={this.onChange} required />
                                </InputGroup>
                                <button className="btn btn-success mt-2 pull-right" onClick={() => this.mailPDF("mail")}>
                                  Send
                                </button>
                              </Row>
                            </PopoverBody>
                          </Popover>
                        </div>
						</div>
                      </div>
					  
                      <div className="col-md-6">
					  <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges">
                            <h5>Shifts</h5>
                            <Row className="d-flex justify-content-end">
                              <Col md="4" xs="12">
                                <Input type="select" name="selectshift" value={this.state.selectshift} onChange={this.onChange}>
                                  <option value="all">All</option>
                                  <option value="1">Today</option>
                                  <option value="8">Last 7 Days</option>
                                  <option value="31">Last 30 Days</option>
                                  <option value="366">Last 12 Months</option>
                                </Input>
                              </Col>
                            </Row>
                            <div className="chart-wrapper">
                              <Bar
                                data={bar}
                                options={{
                                  maintainAspectRatio: false
                                }}
                              />
                            </div>
                          </div>
                        </div>
						</div>
                      </div>
                      <div className="col-md-6">
					    <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges">
                            <h5>Invoice</h5>
                            <Row className="d-flex justify-content-end">
                              <Col md="4" xs="12">
                                <Input type="select" name="selectinvoice" value={this.state.selectinvoice} onChange={this.onChange}>
                                  <option value="all">All</option>
                                  <option value="1">Today</option>
                                  <option value="8">Last 7 Days</option>
                                  <option value="31">Last 30 Days</option>
                                  <option value="366">Last 12 Months</option>
                                </Input>
                              </Col>
                            </Row>
                            <div className="chart-wrapper">
                              <Bar
                                data={bar1}
                                options={{
                                  maintainAspectRatio: false
                                }}
                              />
                            </div>
                          </div>
                        </div>
						</div>
                      </div>
                      <div className="col-md-12">
					  <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges wg-class">
                            <h5>Timesheets</h5>
                            <Row>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard ? agencydashboard.timesheetapproved + agencydashboard.timesheetpending : "0"}
                                  mainText="Total"
                                  icon="fa fa-users"
                                  color="warning"
                                  variant="1"
                                />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.timesheetapproved ? agencydashboard.timesheetapproved : "0"}
                                  mainText="Approved"
                                  icon="fa fa-users"
                                  color="success"
                                  variant="1"
                                />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.timesheetpending ? agencydashboard.timesheetpending : "0"}
                                  mainText="Pending"
                                  icon="fa fa-user-plus"
                                  color="warning"
                                  variant="1"
                                />
                              </Col>
                            </Row>
                          </div>
                        </div>
						</div>
                      </div>
                      <div className="col-md-12">
					  <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges wg-class">
                            <h5>Clients</h5>
                            <Row>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.clients ? agencydashboard.clients : "0"}
                                  mainText="Total"
                                  icon="fa fa-users"
                                  color="primary"
                                  variant="1"
                                />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.activeclient ? agencydashboard.activeclient : "0"}
                                  mainText="Active"
                                  icon="fa fa-user-plus"
                                  color="success"
                                  variant="1"
                                />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.inactiveclient ? agencydashboard.inactiveclient : "0"}
                                  mainText="In-Active"
                                  icon="fa fa-user-times"
                                  color="danger"
                                  variant="1"
                                />
                              </Col>
                            </Row>
                          </div>
                        </div>
						</div>
                      </div>
                      <div className="col-md-12">
					  <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges wg-class">
                            <h5>Employees</h5>
                            <Row>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.employees ? agencydashboard.employees : "0"}
                                  mainText="Total"
                                  icon="fa fa-users"
                                  color="info"
                                  variant="1"
                                />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.activeemployee ? agencydashboard.activeemployee : "0"}
                                  mainText="Active"
                                  icon="fa fa-user-plus"
                                  color="success"
                                  variant="1"
                                />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02
                                  header={agencydashboard.inactiveemployee ? agencydashboard.inactiveemployee : "0"}
                                  mainText="In-Active"
                                  icon="fa fa-user-times"
                                  color="danger"
                                  variant="1"
                                />
                              </Col>
                            </Row>
                          </div>
						  </div>
                        </div>
                      </div>
                    </div>
                    
                    <div className="row">
                      
					  <div className="emp-full">
                      <div className="cus-design">
					  <div className="cate-ranges">
                          <h4>Company Information</h4>
                        </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Name <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.company_name}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Tel <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{`${state.code1} ${state.number1}`}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Address <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.address}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              State/Region <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.state}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Post Code <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.zipcode}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Company Number <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.registration_number}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Organisation Type <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.organisation_type === "limited_company"? "Limited Company":""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Organisation Description <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.company_description}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Email <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.company_email}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Logo <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">
                              {" "}
                              <img
                                width="100px"
                                src={NodeURL + "/" + this.state.avatar_return}
                                alt="Profile"
                                onError={() => {
                                  this.setState({ avatar_return: "../../img/user-profile.png" });
                                }}
                              />
                            </span>
                          </p>
                          <p>
                            <span className="left-views">
                              Line1 <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.line1}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              City/Town <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.line2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Country <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.country}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Postal Address <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{state.postal_address}</span>
                          </p>
                        </div>
                      </div>
                    </div>
					 </div>
                    </div>
                  </div>
                </div>
              </CardBody>
              <CardFooter>
                <Link to="/admin/agencylist">
                  <Button type="submit" color="secondary" title="Back">
                    <i className="fa fa-arrow-left" /> Back
                  </Button>
                </Link>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Viewagency;
