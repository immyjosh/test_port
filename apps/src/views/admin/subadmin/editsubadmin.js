/*eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, Input, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";


class Editsubadmin extends Component {
  state = {
    username: "",
    password: "",
    name: "",
    email: "",
    confirm_password: "",
    conform: "",
    tokenusername: "",
    status: "",
    adminlist:[],
    stausactive: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
    this.setState({
      tokenusername: token.username
    });
    request({
      url: "/administrators/administrators/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ adminlist: res.response.result[0]});
      this.setState({
        username: this.state.adminlist.username,
        name: this.state.adminlist.name,
        email: this.state.adminlist.email,
        status: this.state.adminlist.status
      });
      if (this.state.adminlist.status === 1) {
        this.setState({
          stausactive: true
        });
      } else {
        this.setState({
          stausactive: false
        });
      }
    });
  }
  onEditAdmin = (e, values) => {
    request({
      url: "/administrators/administrators/save",
      method: "POST",
      data: {
        _id: this.props.location.state.rowid,
        username: this.state.username,
        name: this.state.name,
        email: this.state.email,
        status: this.state.status,
        password: this.state.password,
        confirm_password: this.state.confirm_password
      }
    }).then(res => {
      if (res.status === 1) {
        this.editnodify();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  toastId = "SubAdminEdit";
  editnodify() {
      if (!toast.isActive(this.toastId)) {
        this.toastId = toast.success("Updated");
      }
      this.componentDidMount();
    }
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-note" />Edit Admin
                  {/* <div className="card-actions">
                    <a>
                      <i className="fa fa-user" />
                      <small className="text-muted" />
                    </a>
                  </div> */}
                </CardHeader>
                <CardBody>
                  
                  <Row>
				  <Col xs="12" md="12">
				   <div className="cus-design edited-section">
				     <Col xs="12" md="12">
				     <p className="h5">Basic Details</p>
					 </Col>
                    {this.state.username !== this.state.tokenusername ? (
                      <Col xs="12" md="4">
                        <AvGroup>
                          <Label>Username</Label>
                          <AvInput
                            type="text"
                            name="username"
                            placeholder="Enter Username"
                            onChange={this.onChange}
                            value={this.state.username}
                            required
                            autoComplete="name"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                    ) : (
                      <Col xs="12" md="4">
                        <AvGroup>
                          <Label>Username</Label>
                          <AvInput
                            type="text"
                            name="username"
                            placeholder="Enter Username"
                            onChange={this.onChange}
                            value={this.state.username}
                            disabled
                            autoComplete="name"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                    )}
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Name</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter Name"
                          onChange={this.onChange}
                          value={this.state.name}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					</div>
					</Col>
                  </Row>
                  
                  <Row>
				  <Col xs="12" md="12">
				  <div className="cus-design edited-section">
				    <Col xs="12" md="12">
				      <p className="h5">Login Details</p>
				    </Col> 
                    <Col xs="12" md="4">
                      <AvField
                        name="email"
                        label="Email"
                        placeholder="Enter Email"
                        onChange={this.onChange}
                        value={this.state.email}
                        validate={{required: {value: true, errorMessage: "This is required!"}}}
                        autoComplete="email"
                      />
                    </Col>

                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Password</Label>
                        <AvInput
                          type="password"
                          name="password"
                          placeholder="Enter Password"
                          onChange={this.onChange}
                          value={this.state.password}
                          autoComplete="off"
                        />
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Confirm Password</Label>
                        <AvInput
                          type="password"
                          name="confirm_password"
                          placeholder="Enter Password"
                          onChange={this.onChange}
                          value={this.state.confirm_password}
                          validate={{ match: { value: "password" } }}
                          autoComplete="off"
                        />

                        <AvFeedback>Match Password!</AvFeedback>
                      </AvGroup>
                    </Col>
					
                
                  {this.state.username !== this.state.tokenusername ? (
                      <Col xs="12" md="4">
                        <div>
                          <label>Status</label>
                        </div>
                        <Label className="switch switch-text switch-success new-switch">
                          <Input
                            type="checkbox"
                            className="switch-input"
                            checked={this.state.stausactive}
                            onChange={this.statusChangeses}
                          />
                          <span className="switch-label" data-on="active" data-off="inactive" />
                          <span className="switch-handle new-handle" />
                        </Label>
                      </Col>
                    ) : null}
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i className="fa fa-dot-circle-o" /> Update
                  </Button>
                  <Link to="/admin/subadminlist">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editsubadmin;
