import {
  AvFeedback,
  AvField,
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row
} from "reactstrap";
import request from "../../../api/api";
import savenodify from "./subadminlist";

class Addsubadmin extends Component {
  state = {
    username: "",
    name: "",
    password: "",
    confirm_password: "",
    email: ""
  };
  flash = new savenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "admin"))) {
      return this.props.history.replace("/admin");
    }
  }
  OnFormSubmit = (e, values) => {
    e.persist();
    const adminform = {
      username: this.state.username,
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      confirm_password: this.state.confirm_password,
      status: 1,
    };    
    request({
      url: "/administrators/administrators/save",
      method: "POST",
      data: adminform
    })
      .then(res => {
        if (res.status === 1) {
          this.saveuser();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
    
  };
  saveuser = () => {
    return (
      this.props.history.push("/admin/subadminlist"),this.flash.adminsavenodify(),this.form && this.form.reset()
    );
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />Add Admin
              {/*  <div className="card-actions">
                  <a>
                    <i className="fa fa-user-plus" />
                    <small className="text-muted" />
                  </a>
                </div>*/}
              </CardHeader>
              <AvForm
                ref={clear => (this.form = clear)}
                onValidSubmit={this.OnFormSubmit}
              >
                <CardBody>
                  
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
				     <Col xs="12" md="12">
				       <p className="h5">Basic Details</p>
					  </Col> 
                      <Col xs="12" md="4">
                          <AvGroup>
                              <Label>Username</Label>
                              <AvInput
                                  type="text"
                                  name="username"
                                  placeholder="Enter Username"
                                  onChange={this.onChange}
                                  value={this.state.username}
                                  required
                                  autoComplete="name"
                              />
                              <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                      </Col>
                      <Col xs="12" md="4">
                          <AvGroup>
                              <Label>Name</Label>
                              <AvInput
                                  type="text"
                                  name="name"
                                  placeholder="Enter Name"
                                  onChange={this.onChange}
                                  value={this.state.name}
                                  required
                                  autoComplete="name"
                              />
                              <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                      </Col>
					  </div>
					  </Col>
                  </Row>
                  
                  <Row>
				  <Col xs="12" md="12">
				  <div className="cus-design edited-section">
				    <Col xs="12" md="12">
		             <p className="h5">Login Details</p>
					</Col> 
                    <Col xs="12" md="12">
                      <AvGroup>
                        <Row>
						  <Col xs="12" md="4">
                      <AvField
                        name="email"
                        label="Email"
                        type="email"
                        placeholder="Enter Email"
                        onChange={this.onChange}
                        value={this.state.email}
                        autoComplete="email"
                        validate={{required: {value: true, errorMessage: "This is required!"}}}
                      />
                    </Col>
					
                          <Col xs="12" md="4">
                            <Label>Password</Label>
                            <AvInput
                              type="password"
                              name="password"
                              placeholder="Enter Password"
                              onChange={this.onChange}
                              value={this.state.password}
                              required
                              autoComplete="off"
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <Label>Confirm Password</Label>
                            <AvInput
                              type="password"
                              name="confirm_password"
                              placeholder="Enter Password"
                              onChange={this.onChange}
                              value={this.state.confirm_password}
                              required
                              validate={{ match: { value: "password" } }}
                              autoComplete="off"
                            />
                            <AvFeedback>Match Password!</AvFeedback>
                          </Col>
						  
                        </Row>
                      </AvGroup>
                    </Col>
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    title="Add New Admin"
                  >
                   Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addsubadmin;
