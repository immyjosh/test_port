/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, {Component} from "react";
import {Button, Card, CardBody, CardGroup, CardHeader, Col, Container, Row} from "reactstrap";
import {toast, ToastContainer} from "react-toastify";

import {Link} from 'react-router-dom';
import axios from "axios";
import {NodeURL} from "../../api/api";

class Alllogin extends Component {
  state = {
    email: "",
    password: ""
  };
  componentDidMount() { 
    if (localStorage.getItem("notify")) {
      this.logoutmsg();
      localStorage.clear();
    }
  }
    logoutmsg = () => {
    return toast.info("Loged Out!");
  };
  render() {
    return (
      <div className="app flex-row align-items-center fullimage">
        <ToastContainer position="top-right" autoClose={2500} />
                <Container>
                <Row className="justify-content-center">
                    <Col md="5">
                    <CardGroup className="mb-4">
                        <Card>
                        <CardHeader className="d-flex justify-content-center login-header">
                        <img src="./img/logo.png" className="align-center" alt="agency portal" />
                        </CardHeader>
                        <CardBody className="login-functions">
                        <Link to="agency"> <Button outline color="success mb-4 mt-2" size="lg" block>Agency Login</Button></Link>
                        <Link to="client"> <Button outline color="primary mb-2" size="lg" block>Client Login</Button></Link>
                       </CardBody>
                    </Card>
                </CardGroup>
            </Col>
        </Row>
    </Container>
    </div>
    );
  }
}

export default Alllogin;
