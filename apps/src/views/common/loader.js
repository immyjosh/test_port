import React, { Component } from "react";
export default class Loader extends Component {
  render() {
    return (
      <tr className="text-center">
        <td colSpan={18}>
          <h2>
            <strong>
              <i className="fa fa-spinner fa-spin fa-2x" />
            </strong>
          </h2>
        </td>
      </tr>
    );
  }
}
