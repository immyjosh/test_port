/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component, Fragment } from "react";
import { Card, CardHeader, CardBody, NavLink, Col, button } from "reactstrap";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps";
import { Settings } from "../../api/key";
import request, { NodeURL } from "../../api/api";

const defaultZoom = 6;
const defaultOptions = { gestureHandling: "greedy" };
const defaultCenter = { lat: 53.50853, lng: -0.12574 };
const locations = [
  {
    lat: 51.50853,
    lng: -0.12574,
    label: "S",
    draggable: false
  }
];

class MarkerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      godlist: [],
      noPopUp: ""
    };
  }
  componentDidMount() {
    this.setState({
      godlist: this.props && this.props.mapListDetails && this.props.mapListDetails.locationDetails ? this.props.mapListDetails.locationDetails : [],
    });
  }
  // componentWillUnmount() {
  //   this.setState({ godlist: [] });
  // }

  render() {
    return this.state.godlist.map((locations, index) => {
      return <MarkerWithInfoWindow key={index.toString()} location={locations} noPopUp={this.state.noPopUp} />;
    });
  }
}

class MarkerWithInfoWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      nopops: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const { location } = this.props;

    return (
      <Marker onClick={this.toggle} position={location} title={location.title} label={location.label}>
        <Fragment>
          {this.state.isOpen && (
            <InfoWindow onCloseClick={this.toggle}>
              <Col className="full-mapdetails">
                <Col className="left-mapdetails">
                  <img width="80px" height="80px" alt="" src={NodeURL + "/" + location.avatar} />
                </Col>
                <Col className="right-mapdetails">
                  <p className="view-det map-names">
                    {/* <span>
                    <strong>Name <sapn className="mid-arr">:</sapn></strong>
                  </span>{" "} */}
                    <span>{location.title}</span>
                  </p>
                  <p className="view-det">
                    {/*  <span>
                    <strong>Email<sapn className="mid-arr">:</sapn></strong>
                  </span>{" "} */}
                    <span>{location.email}</span>
                  </p>
                  <p className="view-det">
                    {/*<span>
                    <strong>Roll<sapn className="mid-arr">:</sapn></strong>
                  </span>*/}
                    <span> {location.job_type}</span>
                  </p>
                  <p className="view-det">
                    {/*<span>
                    <strong>Location<sapn className="mid-arr">:</sapn></strong>
                  </span>{" "}*/}
                    <span>{location.locations}</span>
                  </p>
                  <p className="view-det">
                    {/*<span>
                    <strong>Phone<sapn className="mid-arr">:</sapn></strong>
                  </span>{" "}*/}
                    <span>
                      {location.phone.code} - {location.phone.number}
                    </span>
                  </p>
                </Col>
              </Col>
            </InfoWindow>
          )}
        </Fragment>
      </Marker>
    );
  }
}

const GoogleMapsComponent = withScriptjs(
  withGoogleMap(props => {
    return (
      <GoogleMap defaultZoom={defaultZoom} defaultCenter={defaultCenter} defaultOptions={defaultOptions}>
        <MarkerList locations={props.locations} mapListDetails={props.mapListDetails}>


        </MarkerList>
      </GoogleMap>
    );
  })
);

class Maplist extends Component {
  render() {
    return (
      <GoogleMapsComponent
        mapListDetails={this.props.mapListDetails}
        key="map"
        googleMapURL={Settings.googleMapURL + Settings.googleMapKey}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    );
  }
}

export default Maplist;
