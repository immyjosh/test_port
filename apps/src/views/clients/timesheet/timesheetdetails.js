import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import Rating from "react-rating";
import fileDownload from "js-file-download";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import moment from "moment";
import { Badge, Button, Card, CardBody, CardHeader, CardFooter, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table, UncontrolledTooltip, Label } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import { breaklist, timelist } from "../../common/utils";
import { Link } from "react-router-dom";
import FileSaver from "file-saver";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "125px"
};

class timesheet extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    singlecheck: false,
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    timesheet_endtimeview: "",
    timesheet_starttimeview: "",
    status_timesheet: "",
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 100,
      skip: 0,
      to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : "",
      employeeID: this.props && this.props.location.state ? this.props.location.state.rowid._id.employee[0]._id : "",
      clientID: this.props && this.props.location.state ? this.props.location.state.rowid._id.client[0]._id : "",
      // locationID: this.props && this.props.location.state ? this.props.location.state.rowid._id.locations[0]._id : "",
      // branchID: this.props && this.props.location.state ? this.props.location.state.rowid._id.branch[0].branches._id : "",
      jobtypeID: this.props && this.props.location.state ? this.props.location.state.rowid._id.job_type[0]._id : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    viewtimesheetid: "",
    timesheet: [],
    ForediState: false,
    ShowApprovebtn: true,
    agency_data: "",
    agency_avatar: "",
    timesheet_to_date: "",
    timesheet_from_date: "",
    TimesheetPrefix: "",
    currentusername: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({
      timelist,
      breaklist,
      currentusername: token && token.username,
      isLoader: true,
      timesheet_to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      timesheet_from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : ""
    });
    this.populateData();
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  populateData() {
    request({
      url: "/client/timesheet/employee/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            if (earn._id === 7) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 6) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            }
            return true;
          });
          const getshiftdata = [];
          this.setState({
            timesheetlists: res.response.result
          });
          res.response.result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = item.starttime / 3600;
            const splithr = evestarthr.toString().split(".");
            const startsec = splithr[1];
            const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
            const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            const eveendhr = item.endtime / 3600;
            const splitendhr = eveendhr.toString().split(".");
            const endsec = splitendhr[1];
            const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
            const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            const timesheet_status = item.timesheet_status ? item.timesheet_status : "";
            getshiftdata.push({
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.title,
              shiftId: item.shiftId,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            });
            this.setState({
              adminlist: getshiftdata,
              pages: res.response.fullcount,
              currPage: res.response.length,
              agency_data: res.response.result.length > 0 ? res.response.result[0].agency_data[0] : {},
              agency_avatar: res.response.result.length > 0 ? res.response.result[0].agency_data[0].company_logo : {}
            });
            return true;
          });
        }
        let TimesheetPrefix = "";
        const TimeID = res.response.result.filter(list => list.timesheetID);
        if (TimeID.length > 0) {
          TimesheetPrefix = TimeID[0].timesheetID;
        } else {
          request({
            url: "/site/timesheetprefixnumber",
            method: "POST",
            data: { for: "client", username: this.state.currentusername }
          }).then(res => {
            if (res.status === 1) {
              TimesheetPrefix = res.response;
            } else if (res.status === 0) {
              if (res.response === "Timesheet Settings Not Available") {
                this.props.history.push("/client/timesheetemployee");
                toast.error("Please Contact Agency");
              } else {
                toast.error(res.response);
              }
            }
          });
        }
        setTimeout(() => {
          this.setState({ TimesheetPrefix });
        }, 1000);
        const time_Sta = res.response.result.filter(list => list.timesheet_status === 1);
        if (time_Sta.length === 0) {
          this.setState({ ShowApprovebtn: false });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["starttime", "start_date", "branch", "title", "status", "breaktime"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/administrators/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  TimesheettoastId = "Timesheet";
  nodify() {
    if (!toast.isActive(this.TimesheettoastId)) {
      this.TimesheettoastId = toast.warn("Updated");
    }
  }
  adminsavenodifytoastId = "Timesheet";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifytoastId)) {
      this.adminsavenodifytoastId = toast.success("Successfully Saved!");
    }
  }
  viewpageclose = e => {
    this.setState({
      success: false,
      edit: false
    });
  };
  viewpage = e => {
    this.state.timesheetlists.filter((value, key) => {
      if (value._id === e.timesheetid) {
        this.setState({
          timesheetviewdata: value,
          viewtimesheetid: e.timesheetid
        });
        if (e.timesheet_status === 2) {
          this.setState({
            status_timesheet: e.timesheet_status
          });
        } else {
          this.setState({
            status_timesheet: ""
          });
        }
        const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
        const evestarthr = value.starttime / 3600;
        const splithr = evestarthr.toString().split(".");
        const startsec = splithr[1];
        const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
        const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
        const eveendhr = value.endtime / 3600;
        const splitendhr = eveendhr.toString().split(".");
        const endsec = splitendhr[1];
        const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
        const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
        this.setState({
          breaktimesheet: value.breaktime,
          employee_rate_sheet: value.employee_rate,
          time_start_date: moment(value.start_date),
          time_end_date: moment(value.end_date),
          client_rate_sheet: value.client_rate,
          timesheetcommand: value.comment || e.comment || "",
          rating: value.rating || e.rating || "",
          timesheet_starttime: value.starttime,
          timesheet_endtime: value.endtime,
          timesheet: value.timesheet,
          timesheet_starttimeview: starttimehr,
          timesheet_endtimeview: endtimehr
        });
      }
      return true;
    });
    if (e.update !== "update") {
      this.setState({
        success: !this.state.success,
        edit: false
      });
    } else {
      this.setState({
        edit: false
      });
    }
  };
  EditnestedModal = () => {
    this.setState({
      edit: true
    });
  };
  EditnestedModalview = () => {
    this.setState({
      edit: false
    });
  };
  approve() {
    let toastId = "ApproveD_01_Time_02";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/client/timesheet/approval",
      method: "POST",
      data: {
        shiftId: this.state.timesheetviewdata._id,
        comment: this.state.timesheetcommand,
        rating: this.state.rating,
        timesheetID: this.state.TimesheetPrefix,
        to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
        from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
      }
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        toast.update(toastId, { render: "Timesheet Approved", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.setState({
          success: !this.state.success
        });
        setTimeout(() => {
          this.componentDidMount();
        }, 3500);
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  editapprove(value) {
    const timesheet = {
      shiftId: this.state.timesheetviewdata._id,
      employee_rate: this.state.employee_rate_sheet,
      client_rate: this.state.client_rate_sheet,
      start_date: this.state.time_start_date,
      end_date: this.state.time_end_date,
      starttime: this.state.timesheet_starttime,
      endtime: this.state.timesheet_endtime,
      breaktime: this.state.breaktimesheet,
      comment: this.state.timesheetcommand,
      rating: this.state.rating
    };
    request({
      url: "/client/timesheet/save",
      method: "POST",
      data: timesheet
    }).then(res => {
      if (res.status === 1) {
        toast.success("Updated!");
        this.setState({
          success: !this.state.success
        });
        this.componentDidMount();
      } else {
        toast.error(res.response);
      }
    });
  }
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ timesheet_starttime: value.value, selecterror: false });
    } else {
      this.setState({ timesheet_starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ timesheet_endtime: value.value, selectenderror: false });
    } else {
      this.setState({ timesheet_endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktimesheet: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktimesheet: "", selectbreakerror: true });
    }
  };
  checkbox(index, id) {
    const c = document.getElementById(index);
    this.setState({
      singlecheck: true
    });
    const ca = document.getElementById("checkall");
    let count = this.state.count;
    const len = this.state.currPage;
    const bulk = this.state.bulk;
    if (c.checked === true) {
      count++;
      bulk.push(id);
      this.setState({ count, bulk });
    } else {
      count--;
      const i = bulk.indexOf(id);
      bulk.splice(i, 1);
      this.setState({ count, bulk });
    }
    if (count === len) {
      ca.checked = true;
      this.checkall("checkall", len);
    } else {
      ca.checked = false;
    }
  }
  checkall(id, len) {
    const c = document.getElementById(id);
    const val = [];
    if (c.checked) {
      this.setState({ count: len });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = true;
        val.push(document.getElementById(i).value);
      }
      this.setState({ bulk: val });
    } else {
      this.setState({ count: 0 });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = false;
      }
      this.setState({ bulk: [] });
    }
  }
  ApproveAll() {
    let toastId = "Approve_Time_02";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const valued = this.state.adminlist.filter(list => list.timesheet_status === 1);
    const values = valued.map(list => list._id);
    request({
      url: "/client/timesheet/groupapproval",
      method: "POST",
      data: {
        shiftIds: values,
        comment: this.state.timesheetcommand,
        rating: this.state.rating,
        timesheetID: this.state.TimesheetPrefix,
        to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
        from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
      }
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        this.setState({ ForediState: false });
        setTimeout(() => {
          this.componentDidMount();
        }, 3500);
        toast.update(toastId, { render: "Timesheet Approved!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
      client.defaults.responseType = "json";
    });
  }
  TimesheetMailPDF = value => {
    let toastId = "Down_Time_Mail_02";
    client.defaults.responseType = "json";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/client/timesheet/downloadpdf",
      method: "POST",
      data: {
        TabOpt: this.state.tableOptions,
        for: "mail",
        timesheet_timesheetID: this.state.TimesheetPrefix,
        timesheet_to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
        timesheet_from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  TimesheetDownloadPDF = value => {
    let toastId = "Down_Time_02";
    client.defaults.responseType = "blob";
    toastId = toast.info("Downloading......", { autoClose: false });
    if (this.state.TimesheetPrefix) {
      request({
        url: "/client/timesheet/downloadpdf",
        method: "POST",
        data: {
          TabOpt: this.state.tableOptions,
          for: "download",
          timesheet_timesheetID: this.state.TimesheetPrefix,
          timesheet_to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
          timesheet_from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
        }
      })
        .then(res => {
          if (res) {
            const file = new Blob([res], { type: "application/pdf" });
            const name = `Timesheet_${this.state.TimesheetPrefix}.pdf`;
            FileSaver(file, name);
            toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
          } else {
            toast.update(toastId, { render: "Please try again later", type: toast.TYPE.ERROR, autoClose: 2500 });
          }
          client.defaults.responseType = "json";
        })
        .catch(error => {
          client.defaults.responseType = "json";
          toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        });
    } else {
      client.defaults.responseType = "json";
      toast.update(toastId, { render: "Unable to Download", type: toast.TYPE.ERROR, autoClose: 2500 });
    }
  };
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    if (this.state.timesheetviewdata.start_date) {
      var timesheetstart = moment(this.state.timesheetviewdata.start_date).format("DD-MM-YYYY");
    }

    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />

        <Card>
          <CardHeader>
            <i className="icon-list" />
            Time Sheet List
            <div className="card-actions" style={tStyle}>
              <button style={tStyles} onClick={() => this.TimesheetDownloadPDF()}>
                <i className="fa fa-download" /> Download
                <small className="text-muted" />
              </button>
              <button style={tStyles} onClick={() => this.TimesheetMailPDF()}>
                <i className="fa fa-envelope" /> Mail
                <small className="text-muted" />
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row">
              <div className="new_invoice_pages">
                <div className="invoice_logos" />
                <div className="top_address_wraps">
                  <div className="col-md-7">
                    <div className="left_invoive_add">
                      {this.state.adminlist.length &&
                        this.state.adminlist.slice(0, 1).map((list, i) => (
                          <div className="agency-arra pt-0 col-md-12" key={i}>
                            {/*<div className="left-agency-profile">
                              <img
                                width="180px"
                                height="180px"
                                src={`${NodeURL}/${list.employee_avatar}`}
                                alt="Profile"
                                onError={() => {
                                  this.setState({ employee_avatar: "../../img/user-profile.png" });
                                }}
                              />
                            </div>*/}
                            <div className="right-agency-profile time-news">
                              <div className="upper-range">
                                <h5>
                                  <span className="times-heading"> Employee Name : </span>
								  <span className="times-para">{list.employee}</span>{" "}
                                </h5>

                                <h5>
                                  <span className="times-heading"> Job Role: </span>
								  <span className="times-para">{list.job_type}</span>{" "}
                                </h5>
								
								<h5>
								  <span className="times-heading"> Timesheet Date </span>
                                 <span className="times-para">
                                   {moment(this.state.timesheet_from_date).format("DD-MM-YYYY")} - {moment(this.state.timesheet_to_date).format("DD-MM-YYYY")}</span> {" "}
                                </h5>
								<h5>
								<span className="times-heading">Timesheet Number</span>
								<span className="times-para">{this.state.TimesheetPrefix}</span>
						        </h5>
						
                              </div>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                  <div className="col-md-5">
                    <div className="col-md-12 crop-img">
                      <img
                        width="250px"
                        src={`${NodeURL}/${this.state.agency_avatar}`}
                        alt="Profile"
                        onError={() => {
                          this.setState({ agency_avatar: "../../img/user-profile.png" });
                        }}
                      />
                    </div>
                    <div className="right_invoive_add">
                      <div className="col-md-6">
                        
                      </div>
                      <div className="col-md-6">
                        <p>{this.state.agency_data && this.state.agency_data.company_name}</p>
                        <p>
                          {" "}
                          {this.state.agency_data && this.state.agency_data.address ? (
                            <Fragment>
                              {this.state.agency_data.address.formatted_address},{this.state.agency_data.address.zipcode} .
                            </Fragment>
                          ) : null}{" "}
                          <br />
                          {this.state.agency_data && this.state.agency_data.address ? (
                            <Fragment>
                              <strong> Tel. </strong> : {this.state.agency_data.phone.code} - {this.state.agency_data.phone.number}
                            </Fragment>
                          ) : null}{" "}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="table-responsive times-tables mt-2">
              <Table hover responsive>
                <thead>
                  <tr>
                    {/* <th>
                                        <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.currPage)} />
                                    </th>*/}
                    <th
                      onClick={() => {
                        this.sort("title");
                      }}
                    >
                      Shift No. <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" />
                    </th>

                    <th
                      onClick={() => {
                        this.sort("branch");
                      }}
                    >
                      Work Location {/* Branch*/} <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("start_date");
                      }}
                    >
                      Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("starttime");
                      }}
                    >
                      Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("breaktime");
                      }}
                    >
                      Break <i style={{ paddingLeft: "25px" }} className={sorticondef} id="breaktime" />
                    </th>

                    <th>Time Worked</th>
                    <th>
                      Status
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    {this.state.ForediState ? <th>Actions</th> : null}
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item.shiftId}>
                        {/* <td>
                                                <input type="checkbox" className="checkbox1" id={i} value={item._id} onClick={() => this.checkbox(i, item._id)} />
                                            </td>*/}
                        <td>{item.title}</td>
                        <td>{item.branch}</td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        {/* <td>{moment(item.end_date).format("DD-MM-YYYY")}</td> */}

                        <td>{item.breaktime / 60} Minutes</td>
                        <td>{Math.round(item.timesheet[0].workmins / 60)} Minutes</td>
                        <td>{item.timesheet_status === 2 ? <Badge color="success">Approved</Badge> : <Badge color="danger">Pending</Badge>}</td>
                        {this.state.ForediState ? (
                          <td>
                            {item.timesheet_status === 2 ? null : (
                              <div>
                                <button type="button" title="Edit" className="btn btn-primary btn-sm" id={`edit${i}`} onClick={e => this.viewpage(item)}>
                                  <i className="fa fa-edit" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                  Edit
                                </UncontrolledTooltip>
                              </div>
                            )}
                          </td>
                        ) : null}
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>

            {/* <nav className="float-left">
                            <Label>Show no.of items : </Label>
                            <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                                <option>10</option>
                                <option>25</option>
                                <option>50</option>
                                <option>100</option>
                                <option>200</option>
                            </Input>
                        </nav>
                        <nav className="float-right">
                            <div>
                                <Pagination
                                    prevPageText="Prev"
                                    nextPageText="Next"
                                    firstPageText="First"
                                    lastPageText="Last"
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.tableOptions.limit}
                                    totalItemsCount={this.state.pages}
                                    pageRangeDisplayed={this.state.pageRangeDisplayed}
                                    onChange={this.paginate}
                                />
                            </div>
                        </nav>*/}
          </CardBody>
          <CardFooter>
            <Link to="/client/timesheetemployee">
              <Button type="button" color="secondary" title="Back">
                <i className="fa fa-arrow-left" /> Back
              </Button>
            </Link>
            {/* {this.state.bulk.length !== 0 ? (
                                <Button id="approve" size="md" color="success" title="Approve"className={"pull-right"} onClick={() => this.approveselected(this.state.bulk)}>
                                    Approve <i className="fa fa-check" />
                                </Button>
                        ) : (
                            <Button id="approve" size="md" color="success" title="Approve" className={"pull-right"} onClick={() => this.approveselected(this.state.bulk)} disabled>
                                Approve <i className="fa fa-check" />
                            </Button>
                        )}*/}
            {this.state.ShowApprovebtn ? (
              <Fragment>
                <Button id="approve" size="md" color="success" title="Approve" className={"pull-right"} onClick={() => this.ApproveAll()}>
                  Approve
                </Button>
                <Button id="approve" size="md" color="danger" title="Approve" className={"pull-right mr-4"} onClick={() => this.setState({ ForediState: !this.state.ForediState })}>
                  Edit
                </Button>
              </Fragment>
            ) : (
              <Badge color="success" className="pull-right">
                Approved
              </Badge>
            )}
          </CardFooter>
        </Card>
        <Modal isOpen={this.state.success} toggle={this.viewpage} className={"modal-lg " + this.props.className}>
          {!this.state.edit ? (
            <Fragment>
              <ModalHeader>EDIT Time Sheet</ModalHeader>
              {/* <AvForm onValidSubmit={this.OnFormEdit}> */}
              <ModalBody>
                <Row>
                  <Col xs="12" md="4">
                    <Label>Rating</Label>
                    <Rating initialRating={this.state.rating} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} onChange={rate => this.setState({ rating: rate })} value={this.state.rating} />
                  </Col>
                  <Col xs="12" md="4">
                    <Label>Comments</Label>
                    <Input
                      name="timesheetcommand"
                      // onChange={(text) => this.ratingComment(text)}
                      placeholder="Comments"
                      type="textarea"
                      onChange={this.onChange}
                      value={this.state.timesheetcommand}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between">
                <div>
                  <Button color="secondary" onClick={this.viewpageclose} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                  {/* <Button color="secondary" className="ml-3" onClick={this.EditnestedModalview} title="Back To View">
                    <i className="fa fa-arrow-left" />
                  </Button> */}
                </div>

                <Button color="success" id="updateapprove" onClick={e => this.editapprove(this.state.timesheetviewdata.shiftId)}>
                  <i className="fa fa-check" />
                </Button>
                <UncontrolledTooltip placement="top" target="updateapprove">
                  Update Time Sheet
                </UncontrolledTooltip>
                {/* <Button color="success" type="submit">
                        {" "}
                        <i className="fa fa-check" />
                      </Button>{" "} */}
              </ModalFooter>
              {/* </AvForm> */}
            </Fragment>
          ) : (
            <Fragment>
              <ModalHeader>Time Sheet Details</ModalHeader>
              <ModalBody>
                <Row>
                  <Col xs="12" md="4">
                    <b>Tittle:</b> {this.state.timesheetviewdata.title}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Added By:</b> {this.state.timesheetviewdata.addedBy}
                  </Col>

                  <Col xs="12" md="4">
                    <b>Role:</b> {this.state.timesheetviewdata.job_type}
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Client:</b> {this.state.timesheetviewdata.client}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Location:</b> {this.state.timesheetviewdata.locations}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Branch:</b> {this.state.timesheetviewdata && this.state.timesheetviewdata.branch.map((list, key) => list.branches.branchname)}
                  </Col>
                </Row>
                {/* <br />
                    <Row>
                      <Col xs="12" md="4">
                        <b>Employee Name:</b> {this.state.timesheetviewdata.employee}
                      </Col>
                      <Col xs="12" md="4">
                        <b>client Rate:</b> {this.state.timesheetviewdata.client_rate}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Employee Rate:</b> {this.state.timesheetviewdata.employee_rate}
                      </Col>
                    </Row> */}
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Date:</b>
                    {timesheetstart}
                    {/* {timesheetend} */}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Time:</b> {this.state.timesheet_starttimeview} - {this.state.timesheet_endtimeview}
                  </Col>

                  <Col xs="12" md="4">
                    <b>Break:</b> {this.state.timesheetviewdata.breaktime / 60 + " mins"}
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Employee Name:</b> {this.state.timesheetviewdata.employee}
                  </Col>
                </Row>
                <br />
                {this.state.timesheet ? (
                  <Row>
                    <Col xs="12" md="4">
                      {this.state.timesheet.length > 0 && this.state.timesheet[0].start ? (
                        <Fragment>
                          <b>Worked Time : </b> {moment(this.state.timesheet[0].start).format("HH:mm")} - {moment(this.state.timesheet[0].end).format("HH:mm")}
                        </Fragment>
                      ) : null}
                    </Col>
                    <Col xs="12" md="4">
                      {this.state.timesheet.length > 0 && this.state.timesheet[0].holdedmins ? (
                        <Fragment>
                          <b>Holded Duration : </b> {Math.round(this.state.timesheet[0].holdedmins / 60)} Minutes
                        </Fragment>
                      ) : null}
                    </Col>
                    <Col xs="12" md="4">
                      {this.state.timesheet.length > 0 && this.state.timesheet[0].workmins ? (
                        <Fragment>
                          <b> Total Duration : </b> {Math.round(this.state.timesheet[0].workmins / 60)} Minutes
                        </Fragment>
                      ) : null}
                    </Col>
                  </Row>
                ) : null}
                <br />
                {this.state.status_timesheet === "" ? (
                  <Row>
                    <Col xs="12" md="4">
                      <b>Rating : </b>
                      <Rating initialRating={this.state.rating} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} onChange={rate => this.setState({ rating: rate })} value={this.state.rating} />
                    </Col>
                    <Col xs="12" md="4">
                      <b>Comments :</b>
                      <Input
                        name="timesheetcommand"
                        // onChange={(text) => this.ratingComment(text)}
                        placeholder="Comments"
                        type="textarea"
                        onChange={this.onChange}
                        value={this.state.timesheetcommand}
                      />
                    </Col>
                  </Row>
                ) : (
                  <Row>
                    <Col xs="12" md="4">
                      <b>Rating : </b>
                      <Rating initialRating={this.state.rating} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} value={this.state.rating} readonly />
                    </Col>
                    <Col xs="12" md="4">
                      <b>Comments : </b>
                      {this.state.timesheetcommand}
                    </Col>
                  </Row>
                )}
              </ModalBody>
              <ModalFooter className="justify-content-between">
                <Button color="secondary" onClick={this.viewpageclose} title="Close">
                  <i className="fa fa-close" />
                </Button>
                {this.state.status_timesheet === "" ? (
                  <Fragment>
                    <Button id="approve" color="success" onClick={e => this.approve(this.state.timesheetviewdata.shiftId)}>
                      <i className="fa fa-calendar-check-o" aria-hidden="true" />
                    </Button>
                    <UncontrolledTooltip placement="top" target="approve">
                      Approve Time Sheet
                    </UncontrolledTooltip>
                    <Button color="primary" className="ml-2" onClick={this.EditnestedModal} title="Edit">
                      <i className="fa fa-edit" />
                    </Button>
                  </Fragment>
                ) : null}
              </ModalFooter>
            </Fragment>
          )}
          {/* <ModalFooter>
                {this.state.status_timesheet === "" ? (
                  <Button color="success" onClick={e => this.approve(this.state.timesheetviewdata.shiftId)}>
                    Approve
                  </Button>
                ) : null}
                <Button color="secondary" onClick={this.viewpage}>
                  Close
                </Button>
              </ModalFooter> */}
        </Modal>
      </div>
    );
  }
}

export default timesheet;
