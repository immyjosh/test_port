import React, { Component, Fragment } from "react";
// import FacebookLogin from "react-facebook-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import { withRouter } from "react-router-dom";
import { Settings } from "../../../api/key";
import { Button, Col, Row } from "reactstrap";
var google = {
  backgroundImage: "url('./img/google-plus.png')",
  backgroundRepeat: "no-repeat"
};
var facebook = {
  backgroundImage: "url('./img/facebook.png')",
  backgroundRepeat: "no-repeat"
};
class SocialLogin extends Component {
  state = {
    loginError: false,
    redirect: true,
    name: "",
    provider: "",
    email: "",
    provider_id: "",
    token: "",
    provider_pic: ""
  };
  responseGoogle = response => {
    this.signup(response, "google");
  };
  responseFacebook = response => {
    this.signup(response, "facebook");
  };
  signup = (res, type) => {
    if (type === "facebook" && res.email) {
      this.setState({
        name: res.name,
        provider: type,
        email: res.email,
        provider_id: res.id,
        token: res.accessToken,
        provider_pic: res.picture.data.url
      });
      this.props.history.push("/client/register", {
        ...this.state
      });
    }
    if (type === "google" && res.w3.U3) {
      this.setState({
        name: res.w3.ig,
        email: res.w3.U3,
        provider_id: res.El,
        token: res.Zi.access_token,
        provider_pic: res.w3.Paa
      });
      this.props.history.push("/client/register", {
        ...this.state
      });
    }
  };
  register = () => {
    this.props.history.push("/client/register", {
      ...this.state
    });
  };
  render() {
    return (
      <Fragment>
        <Row>
          <Col xs="12" md="12">
            <Button color="primary" className="mt-3" active onClick={this.register}>
              Register Now!
            </Button>
          </Col>
          <Col xs="12" md="12">
            <GoogleLogin
              className="btn btn-danger google-login"
              clientId={Settings.googleClientId}
              scope="profile"
              buttonText="Login with Google"
              onSuccess={this.responseGoogle}
              onFailure={this.responseGoogle}
              style={google}
            />
          </Col>
          <Col xs="12" md="12">
            <FacebookLogin
              appId={Settings.fbAppId}
              autoLoad={false}
              fields="name,email,picture"
              callback={this.responseFacebook}
              render={renderProps => (
                <Button color="danger" size="md" className="facebook-login" onClick={renderProps.onClick} style={facebook}>
                  Login with Facebook
                </Button>
              )}
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}
export default withRouter(SocialLogin);
