import React, {Component} from "react";
import {Button, Card, CardBody, CardGroup, Col, Container, InputGroupAddon, InputGroupText, Row,} from "reactstrap";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {toast, ToastContainer} from "react-toastify";

import request from "../../../api/api";


class clientchangepassword extends Component {
  state = {
    newpassword: "",
    confirmnewpassword: "",
    
  };
   onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    request({
      url: `/client/resetpassword/${this.props.match.params.id}/${this.props.match.params.resetcode}`,
      method: "POST",
      data: {
        password: this.state.newpassword,
        confirmpassword: this.state.confirmnewpassword
      }
    })
      .then(res => {
       this.passwordnodify();
      })
      .catch(err => {
        this.logerror();
      });
  };
 
  logerror = () => {
    return toast.error("No User Found!");
  };
   passwordnodify() {   
    return toast.success("Password Changed!");
  }
  render() {
    return (
    
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-4">
                <Card className="p-4">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <CardBody>
                      <h3>Change Password</h3>
                      {/* <p className="text-muted">Sign In to your account</p> */}
                     <AvGroup className="input-group">
                         <InputGroupAddon addonType="prepend">
                          <InputGroupText>  
                          <i className="icon-lock"></i>
                          </InputGroupText>
                          </InputGroupAddon>
                        <AvInput
                        lable="New Password"
                          type="password"
                          name="newpassword"
                          placeholder="Enter new password"
                          onChange={this.onChange}
                          value={this.state.newpassword}
                          required
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup> 
                        <AvGroup className="input-group">
                         <InputGroupAddon addonType="prepend">
                          <InputGroupText>  
                          <i className="icon-lock"></i>
                          </InputGroupText>
                          </InputGroupAddon>
                        <AvInput
                          type="password"
                          name="confirmnewpassword"
                          placeholder="Enter confirm new password"
                          onChange={this.onChange}
                          value={this.state.confirmnewpassword}
                          required
                           validate={{ match: { value: "newpassword" } }}
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup> 
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">
                            Submit
                          </Button>
                        </Col>
                       
                      </Row>
                    </CardBody>
                  </AvForm>
                </Card>
                <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: 44 + "%" }}
                >
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua.
                      </p>
                      <Button color="primary" className="mt-3" active>
                        Register Now!
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default clientchangepassword;
