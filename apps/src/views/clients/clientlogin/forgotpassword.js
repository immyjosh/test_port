import React, {Component} from "react";
import {Alert,Button, Card, CardBody, CardGroup, Col, Container, InputGroupAddon, InputGroupText, Row,} from "reactstrap";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {toast, ToastContainer} from "react-toastify";

import request from "../../../api/api";


class clientforgotpassword extends Component {
  state = {
    email: "",
    mailalert:false
  };
   onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => { 
     request({
      url: "/client/forgotpassword",
      method: "POST",
      data: {email:this.state.email}
    })
      .then(res => {
      this.setState({
          mailalert: true
        });
        setTimeout(() => {
           this.setState({
          mailalert: false
        });
        }, 3000)
       
      })
      .catch(err => {
        this.logerror();
      });
  };
 
  componentDidMount() {
   
    if (localStorage.getItem("notify")) {
      this.logoutmsg();
      localStorage.clear();
    }
  }
 
  logerror = () => {
    return toast.error("No User Found!");
  };
  
  render() {
    return (
    
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <CardBody>
                      <h1 className="login-infos" >Forgot Password</h1>
                      <p className="text-muted">Enter your Email</p>
                     <AvGroup className="input-group"> 
                         <InputGroupAddon addonType="prepend">
                          <InputGroupText>  
                          <i className="icon-user"></i>
                          </InputGroupText>
                          </InputGroupAddon> 
                        <AvInput
                          type="text"
                          name="email"
                          placeholder="Enter email"
                          onChange={this.onChange}
                          value={this.state.email}
                          required
                        />
                        <AvFeedback>This is invalid!</AvFeedback>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">
                            Send
                          </Button>
                        </Col>
                       {this.state.mailalert ? (
                          <Alert color="danger">
                         Check your email!
                          </Alert>) : null}
                      </Row>
                    </CardBody>
                  </AvForm>
                </Card>
                <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: 44 + "%" }}
                >
                  <CardBody className="text-center">
                    <div className="for-text">
                      <h2>Sign up</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua.
                      </p>
                      <Button color="primary" className="mt-3" active>
                        Register Now!
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default clientforgotpassword;
