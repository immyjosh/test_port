/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { Container, Row, Col, CardGroup, Card, CardBody, Button, Alert, InputGroupAddon, InputGroupText } from "reactstrap";
import { AvForm, AvGroup, AvInput, AvFeedback } from "availity-reactstrap-validation";
import loginnodify from "../clientdashboard";
import { ToastContainer, toast } from "react-toastify";
import request, { client } from "../../../api/api";
import Gettoken from "../../../api/gettoken";
import Context from "../../../api/context";
import { Link } from "react-router-dom";
import jwt_decode from "jwt-decode";
// import SocialLogin from "./sociallogin" //google

class Clientlogin extends Component {
  state = {
    email: "",
    password: "",
    emaillists: [],
    invalidemail: false,
    error: false
  };
  flash = new loginnodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    if (this.props.location.state === true) {
      return toast.success("Registration Completed!"), this.props.history.push({ state: false });
    }
    const token = this.props.theme.AuthtokenClient;
    if (token.role) {
      switch (token.role) {
        case "client":
          this.props.history.replace("/client/dashboard");
          break;
        default:
          this.props.history.replace("/");
      }
    }
    if (this.props.location.state === "notify") {
      toast.info("Loged Out!");
      this.props.history.push({ state: "" });
    }
  }
  OnFormSubmit = e => {
    const loginform = {
      email: this.state.email,
      password: this.state.password
    };
    request({
      url: "/clientlogin",
      method: "POST",
      data: loginform
    })
      .then(res => {
        if (res.status === 1) {
          let tokenClient;
          if (res.response.auth_token) {
            tokenClient = jwt_decode(res.response.auth_token);
          }
          if (tokenClient.subscription === 1) {
            localStorage.setItem("APUSC", res.response.auth_token);
            client.defaults.headers.common["Authorization"] = res.response.auth_token;
            this.loginnodify();
          } else if (tokenClient.subscription === 0) {
            toast.error("Agency Subscription Expired, Please Contact Agency.");
          }
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };
  agencynodify() {
    return toast.success("Registration Completed!");
  }
  loginnodify() {
    return this.props.history.push({ pathname: "/client/dashboard", state: true });
    // return this.props.history.replace("/client/dashboard"),this.flash.nodify()
  }
  deactivatenodify() {
    return toast.error("Deactivated!");
  }
  logoutmsg = () => {
    return toast.info("Loged Out!");
  };
  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <CardBody>
                      <h1 className="login-infos" >Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput name="email" label="Email" placeholder="Enter email or username" onChange={this.onChange} value={this.state.email} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="password" name="password" placeholder="Enter password" onChange={this.onChange} value={this.state.password} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button type={"submit"} color="primary" className="px-4">
                            Login
                          </Button>
                        </Col>
                        {this.state.error ? <Alert color="danger">Password Incorrect.Enter Correct Password!</Alert> : null}
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">
                            <Link className="for-pass" to="/client/forgotpassword">Forgot password?</Link>
                          </Button>
                        </Col>
                      </Row>
                    </CardBody>
                  </AvForm>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + "%" }}>
                  <CardBody className="text-center">
                    <div className="portal-admin">
                      <h2>Agency Portal</h2>
                    </div>
                    {/*<SocialLogin/>*/}
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Clientlogin {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
