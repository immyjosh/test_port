/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from "availity-reactstrap-validation";
import savenodify from "../clientlogin/clientlogin";
import { toast, ToastContainer } from "react-toastify";

import { Container, Row, Col, Card, CardBody, CardFooter, Button, InputGroup, CustomInput, Label } from "reactstrap";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import request from "../../../api/api";
import IntlTelInput from "react-intl-tel-input";
import { withRouter } from "react-router-dom";
import Select from "react-select";
import "react-select/dist/react-select.css";

class clientRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.history.location !== undefined || this.props.history.location.state !== undefined ? this.props.history.location.state.name : "",
      email: this.props.history.location !== undefined || this.props.history.location.state !== undefined ? this.props.history.location.state.email : "",
      password: "",
      confirm_password: "",
      number: "",
      username: "",
      code: "",
      address: "",
      line1: "",
      line2: "",
      city: "",
      state: "",
      country: "",
      zipcode: "",
      formatted_address: "",
      phoneerror: false,
      avatar: null,
      status: 1,
      isverified: 0,
      agencylist: [],
      agency: "",
      location: "",
      worklist: [],
      additional_locations: [],
      avatarfile: ""
    };
  }
  flash1 = new savenodify();
  componentDidMount() {
    request({
      url: "/site/agencies",
      method: "POST"
    }).then(res => {
      this.setState({ agencylist: res.response.result });
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  selectjoblocation = e => {
    const filteredlist = this.state.agencylist.filter(list => list._id === e.target.value);
    const worklist = filteredlist[0].locations.map((emp, key) => ({
      value: emp._id,
      label: emp.name
    }));
    this.setState({
      agency: e.target.value,
      joblist: filteredlist[0].jobtypes,
      locationlist: filteredlist[0].locations,
      worklist: worklist
    });
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode
    });
  };

  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: ""
      });
    }
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarfile: URL.createObjectURL(evt.target.files[0]),
      avatarName: evt.target.files[0].name
    });
  };
  saveChanges = value => {
    this.setState({
      additional_locations: value
    });
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    if (this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else {
      const data = new FormData();
      this.state.additional_locations.map((item, i) => {
        return data.append("additional_locations", item.value);
      });
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("agency", this.state.agency);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      data.append("location", this.state.location);
      data.append("status", this.state.status);
      data.append("isverified", this.state.isverified);
      request({
        url: "/register/client",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(err => {
          toast.error("error");
        });
    }
  };
  saveuser() {
    return this.setState({ number: "", address: "" }), this.form && this.form.reset(), this.props.history.push({ pathname: "/client", state: true });
  }
  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <AvForm onValidSubmit={this.OnFormSubmit}>
            <Row className="justify-content-center">
              <Col md="6">
                <Card className="mx-4">
                  <CardBody className="p-4">
                    <h1>Register</h1>
                    <InputGroup className="mb-3 for-select">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-user" />
                        </span>
                      </div>
                      <AvField type="select" name="agency" placeholder="Select Agency" onChange={this.selectjoblocation} value={this.state.agency} required>
                        <option>Select Agency</option>
                        {this.state.agencylist
                          ? this.state.agencylist.map(list => (
                              <option key={list._id} value={list._id}>
                                {list.name}
                              </option>
                            ))
                          : null}
                      </AvField>
                    </InputGroup>
                    <InputGroup className="mb-3 for-select">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-map" />
                        </span>
                      </div>
                      <AvField type="select" name="location" placeholder="Select Location" onChange={this.onChange} value={this.state.location} required>
                        <option>Select Location</option>
                        {this.state.locationlist
                          ? this.state.locationlist.map(list => (
                              <option key={list._id} value={list._id}>
                                {list.name}
                              </option>
                            ))
                          : null}
                      </AvField>
                    </InputGroup>
                    <InputGroup className="mb-3 for-select">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fa fa-map-o" />
                        </span>
                      </div>
                      <AvGroup>
                        <Select
                          name="form-field-name2"
                          value={this.state.additional_locations}
                          options={this.state.worklist}
                          onChange={this.saveChanges}
                          multi
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </InputGroup>
                    <p className="text-muted">Personel Details</p>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-user-following" />
                        </span>
                      </div>
                      <AvInput type="text" name="username" placeholder="User Name" onChange={this.onChange} value={this.state.username} required />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-user" />
                        </span>
                      </div>
                      <AvInput type="text" name="name" placeholder="Name" onChange={this.onChange} value={this.state.name} required />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-phone" />
                        </span>
                      </div>

                      <IntlTelInput
                        style={{ width: "91%" }}
                        defaultCountry={"gb"}
                        utilsScript={libphonenumber}
                        css={["intl-tel-input", "form-control"]}
                        onPhoneNumberChange={this.handler}
                        value={this.state.number}
                      />
                      {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                    </InputGroup>

                    <Label for="exampleCustomFileBrowser">Profile Image</Label>
                    <Row>
                      <Col md="8">
                        <CustomInput
                          type="file"
                          id="exampleCustomFileBrowser"
                          name="avatar"
                          onChange={this.fileChangedHandler}
                          label={this.state.avatarName ? this.state.avatarName : "Upload  Image"}
                          encType="multipart/form-data"
                        />
                      </Col>
                      {this.state.avatarfile ? <img className="prof-image img-fluid" width="120px" height="100px" src={this.state.avatarfile} alt="Preview" /> : null}
                    </Row>
                    <p className="text-muted">Search address</p>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-map" />
                        </span>
                      </div>
                      <PlacesAutocomplete
                        style={{ width: "100%" }}
                        value={this.state.address}
                        onChange={this.handleChange}
                        onSelect={this.handleSelect}
                        onFocus={this.geolocate}
                      >
                        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                          <div style={{ width: "91%" }}>
                            <input
                              {...getInputProps({
                                placeholder: "Search Your Location",
                                className: "form-control"
                              })}
                            />
                            <div className="autocomplete-dropdown-container">
                              {suggestions.map(suggestion => {
                                const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                  ? {
                                      backgroundColor: "#fafafa",
                                      cursor: "pointer"
                                    }
                                  : {
                                      backgroundColor: "#ffffff",
                                      cursor: "pointer"
                                    };
                                return (
                                  <div
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                      style
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        )}
                      </PlacesAutocomplete>
                    </InputGroup>
                    <InputGroup className="mb-3">
					 <div className="reg-widths">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput type="text" name="line1" placeholder="Line 1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
					  <div>
					  
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput type="text" name="line2" placeholder="Line 2" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      {/* <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput
                        type="text"
                        name="city"
                        placeholder="City"
                        onChange={this.onChange}
                        value={this.state.city}
                        required
                        autoComplete="name"
                      /> */}
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput type="text" name="state" placeholder="State" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput
                        type="text"
                        name="country"
                        placeholder="Country"
                        onChange={this.onChange}
                        value={this.state.country}
                        required
                        autoComplete="name"
                      />
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput
                        type="text"
                        name="zipcode"
                        placeholder="Zipcode"
                        onChange={this.onChange}
                        value={this.state.zipcode}
                        required
                        autoComplete="name"
                      />
                    </InputGroup>
                    <p className="text-muted">Login Details</p>
                    <AvGroup className="input-group">
                      <InputGroup className="mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-envelope" />{" "}
                          </span>
                        </div>
                        <AvInput type="email" name="email" placeholder="Enter Your Mail Id" onChange={this.onChange} value={this.state.email} required />{" "}
                        <AvFeedback>Enter Valid Email!</AvFeedback>
                      </InputGroup>
                    </AvGroup>
                    <AvGroup className="input-group">
                      <InputGroup className="mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock" />
                          </span>
                        </div>
                        <AvInput
                          type="password"
                          name="password"
                          placeholder="Enter Password"
                          onChange={this.onChange}
                          value={this.state.password}
                          required
                          autoComplete="off"
                        />
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock" />
                          </span>
                        </div>
                        <AvInput
                          type="password"
                          name="confirm_password"
                          placeholder="Re-Enter Password"
                          onChange={this.onChange}
                          value={this.state.confirm_password}
                          required
                          validate={{ match: { value: "password" } }}
                          autoComplete="off"
                        />
                        <AvFeedback>Password Doesn't match!</AvFeedback>
                      </InputGroup>
                    </AvGroup>
                  </CardBody>
                  <CardFooter className="p-4">
                    <Button
                      type="submit"
                      block
                      color="success pull-right mb-3"
                      title="Ok"
                      onClick={() => {
                        this.phonefield(this.state.number);
                      }}
                    >
                      REGISTER
                    </Button>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </AvForm>
        </Container>
      </div>
    );
  }
}

export default withRouter(clientRegister);
