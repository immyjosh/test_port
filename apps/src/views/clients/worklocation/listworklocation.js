import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import { Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import Pagination from "react-js-pagination";

class Listworklocation extends Component {
  state = {
    adminredirect: false,
    sortOrder: true,
    isLoader: false,
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0
    },
    worklist: []
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ isLoader: true });
    request({
      url: "/client/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        if (res) {
          this.setState({
            isLoader: false,
            worklist: res.response.result,
            pages: res.response.fullcount,
            currPage: res.response.length
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/client/editworklocation",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/client/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (!res.response.result && !res.response.fullcount) {
        this.setState({ worklist: [], pages: "" });
      } else {
        this.setState({
          worklist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Address") {
        state.tableOptions.filter = "address";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  render() {
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Work Location List
            {/* <div className="card-actions">
              <a>
                <i className="fa fa-users" />
                <small className="text-muted" />
              </a>
            </div> */}
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
              <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Name</option>
                  <option>Address</option>
                </Input>
              </div>
              <div className="col-lg-3 pl-0">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Branch Name <i className="fa fa-sort" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("address");
                      }}
                    >
                      Address <i className="fa fa-sort" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("zipcode");
                      }}
                    >
                      Zipcode
                      <i className="fa fa-sort" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("phone");
                      }}
                    >
                      Phone Number
                      <i className="fa fa-sort" />
                    </th>
                    <th>
                      Status <i className="fa fa-sort" />
                    </th>
                    {/* <th>Actions</th> */}
                  </tr>
                </thead>
                <tbody>
                  {this.state.worklist ? (
                    this.state.worklist.map((item, i) => (
                      <tr key={item._id}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.address}</td>
                        <td>{item.zipcode}</td>
                        <td>{item.phone.number}</td>
                        <td>
                          {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                        </td>
                        {/* <td>
                          <button
                            type="button"
                            title="Edit"
                            className="btn btn-primary btn-sm"
                            id={`edit${i}`}
                            onClick={id => this.editpage(item._id)}
                          >
                            <i className="fa fa-edit" />
                          </button>
                          <UncontrolledTooltip
                            placement="top"
                            target={`edit${i}`}
                          >
                            Edit
                          </UncontrolledTooltip>
                        </td> */}
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td />
                      <td />
                      {!this.state.isLoader && <td className="float-right">No Work Location Added Yet</td>}
                      <td />
                      <td />
                      <td />
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Listworklocation;
