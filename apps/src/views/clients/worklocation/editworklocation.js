/*eslint no-sequences: 0*/
import {
  AvFeedback, 
  AvForm,
  AvGroup,
  AvInput
} from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Label,
  Row
} from "reactstrap";
import request  from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";

import PlacesAutocomplete, {
    geocodeByAddress
} from "react-places-autocomplete";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";

class Editworklocation extends Component {
  state = {
   name: "",
    zipcode: "",
    formatted_address: "",
    color:"",
    number: "",
    status: "",   
    code: "",   
    address: "",
    phoneerror: false,
    phonestatus: "",
    colorerror: false,
    worklist: "",
    
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangecolor = e => {   
    this.setState({ [e.target.name]: e.target.value ,
       colorerror: false
    });   
  };
  componentDidMount() {
      const token = this.props.token_role;
      if (!token || (token && (!token.username || token.role !== "client"))) {
          return this.props.history.replace("/client");
      }
    request({
      url: "/client/location/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ worklist: res.response.result[0] });
      this.setState({
        name: this.state.worklist.name,
        number: this.state.worklist.phone.number,
        formatted_address: this.state.worklist.address,
        color: this.state.worklist.color,
        zipcode: this.state.worklist.zipcode      
      });
    });
  }

    OnFormSubmit = (e, values) => {
     if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    }else if(this.state.color === ""){
      this.setState({
        colorerror: true
      });

    } else { 
        request({
          url: "/client/location/save",
          method: "POST",
          data: {
              _id: this.props.location.state.rowid,
              name:this.state.name,
              phone:{
                code:this.state.code,
                number:this.state.number,
              },
              address:this.state.formatted_address,
              color:this.state.color,
              zipcode:this.state.zipcode,
              status:1,
          }
      }).then(res => {
          if (res.status === 1) {
              this.editnodify();
          } else if (res.status === 0) {
              toast.error(res.response);
          }
      });
    }
  };

   handleChange = address => {
    this.setState({ formatted_address: address });  
    this.setState({ address });    
  };

  handleSelect = address => {
    this.setState({ address: address });
     this.setState({ formatted_address: address });  
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            zipcode: results[0].address_components[6].long_name
          });
        }      
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      phonestatus: status
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
toastId = "shiftlocationEdit";
     editnodify() {
       if (!toast.isActive(this.toastId)) {
       this.toastId = toast.success("Updated")
       }
      this.componentDidMount()
   
  }

  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
          <Row>
              <Col xs="12" md="12">
                  <Card>
                      <CardHeader>
                          <i className="icon-plus" />Edit Work Location
                          {/* <div className="card-actions">
                              <a>
                                  <i className="fa fa-user-plus" />
                                  <small className="text-muted" />
                              </a>
                          </div> */}
                      </CardHeader>
                      <AvForm
                ref={clear => (this.form = clear)}
                onValidSubmit={this.OnFormSubmit}
              >
                      <CardBody>
                        <p className="h5">Work Details</p>
                        <hr />                 
                            <Row>
                            <Col xs="12" md="5">
                                <AvGroup>
                                <Label>Name</Label>
                                <AvInput
                                    type="text"
                                    name="name"
                                    placeholder="Enter username"
                                     value={this.state.name} 
                                    onChange={this.onChange}
                                    required
                                    autoComplete="name"
                                />
                                <AvFeedback>This is required!</AvFeedback>
                                </AvGroup>
                            </Col>   
                            <Col xs="12" md="5">                        
                                <AvGroup>
                                <p>Phone</p>
                                <IntlTelInput
                                onPhoneNumberBlur={this.handler}
                                style={{ width: "100%" }}
                                defaultCountry={"gb"}
                                utilsScript={libphonenumber}
                                css={["intl-tel-input", "form-control"]}
                                onPhoneNumberChange={this.handler}
                                value={this.state.number}
                                />
                                {this.state.phoneerror ? (
                                <div style={{ color: "red" }}>
                                    {" "}
                                    Enter Valid Phone Number!
                                </div>
                                ) : null}
                            </AvGroup> 
                            </Col>                   
                            </Row>  
                            <div>
                            <Label>Search address</Label>
                            </div> 
                            <Row>
                            <Col xs="12" md="10">
                            <PlacesAutocomplete
                                value={this.state.formatted_address}
                                onChange={this.handleChange}
                                onSelect={this.handleSelect}
                                onFocus={this.geolocate}
                            >
                                {({
                                getInputProps,
                                suggestions,
                                getSuggestionItemProps
                                }) => (
                                <div>
                                    <input
                                    {...getInputProps({
                                        placeholder: "Search Places",
                                        className: "form-control"
                                    })}
                                    />
                                    <div className="autocomplete-dropdown-container absolute">
                                    {suggestions.map(suggestion => {
                                        const className = suggestion.active
                                        ? "suggestion-item--active"
                                        : "suggestion-item";
                                        // inline style for demonstration purpose
                                        const style = suggestion.active
                                        ? {
                                            backgroundColor: "#fafafa",
                                            cursor: "pointer"
                                            }
                                        : {
                                            backgroundColor: "#ffffff",
                                            cursor: "pointer"
                                            };
                                        return (
                                        <div
                                            {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                            })}
                                        >
                                            <span>{suggestion.description}</span>
                                        </div>
                                        );
                                    })}
                                    </div>
                                </div>
                                )}
                            </PlacesAutocomplete>
                            </Col>
                        </Row>
                            <Row className="mt-3">                           
                            <Col xs="12" md="5">
                            <AvGroup>
                                <Label>Zipcode</Label>
                                <AvInput
                                type="text"
                                name="zipcode"
                                placeholder="Enter zipcode"
                                onChange={this.onChange}
                                value={this.state.zipcode}
                                required
                                autoComplete="name"
                                />
                                <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                            </Col>   
                            <Col xs="12" md="5">
                            <AvGroup>
                                <Label>Color</Label>
                                <AvInput
                                type="color"
                                name="color"
                                placeholder="Enter color"
                                onChange={this.onChangecolor}
                                value={this.state.color}
                                required
                                autoComplete="name"
                                />
                                <AvFeedback>This is required!</AvFeedback>
                                {this.state.colorerror ? (
                                <div style={{ color: "red" }}>
                                    {" "}
                                    Enter Valid color!
                                </div>
                                ) : null}
                            </AvGroup>
                            </Col>                  
                            </Row>       
                                   
                        </CardBody>
                    <CardFooter>
                        <Button type="submit" size="md" color="primary pull-right" title="Update">
                            <i
                                className="fa fa-dot-circle-o"
                            />
                            Update
                        </Button>
                        <Link to="/client/listworklocation">
                            <Button type="submit" size="md" color="secondary" title="Back">
                                <i className="fa fa-arrow-left" /> Back
                            </Button>
                        </Link>
                    </CardFooter>
                    </AvForm>
                  </Card>
              </Col>
          </Row>
      </div>
    );
  }
}

export default Editworklocation;
