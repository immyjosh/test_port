import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { AvField, AvInput, AvForm, AvRadio, AvRadioGroup, AvFeedback, AvGroup } from "availity-reactstrap-validation";
import Loader from "../../common/loader";
import { Badge, Label, Modal, ModalBody, ModalHeader, ModalFooter, Row, Col, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
import moment from "moment";
import { breaklist, timelist, monthlydates } from "../../common/utils";
import "react-dates/initialize";
import { DateRangePicker, SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import Widget02 from "../../../views/Template/Widgets/Widget02";

var tStyle = {
  cursor: "pointer"
};
var tStyles = {
  width: "150px"
};

class Shiftlist extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    isLoader: false,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      status: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    start_date1: null,
    end_date1: null,
    emplist: [],
    job_type1: "",
    jobname: "",
    jobrate: "",
    jobmodel: false,
    jobtypelistselect: [],
    eventlarge: false,
    nestedeventlarge: false,
    closeAll: false,
    large: false,
    allday: true,
    title: "",
    location1: "",
    starttime: "",
    endtime: "",
    breaktime: "",
    slotdata: [],
    shift_type: "",
    multiselecteddays: "",
    job_type: "",
    timelist: [],
    breaklist: [],
    monthlydates: [],
    eventlist: [],
    worklocationlist: [],
    eventdata: "",
    id: "",
    weekly: false,
    rotational: false,
    monthly: false,
    monthlydate: [],
    isGoing: true,
    sun: false,
    mon: false,
    tue: false,
    wed: false,
    thu: false,
    fri: false,
    sat: false,
    jobtypelist: [],
    option_data: {},
    shift_option: "",
    shifttemplatelist: [],
    shifttemplatelistselect: [],
    date_list: [],
    Weekly_select_date_list: [],
    montofri: 0,
    Weekly_select_date_count: 0,
    monthly_date_count: 0,
    shift_template: "",
    template: "",
    values: "",
    status: "",
    start_date_time: null,
    end_date_time: null,
    edit: false,
    clientlist: [],
    client1: "",
    clientview: "",
    locationview: "",
    job_typeview: "",
    xday: "",
    yday: "",
    shifttemplatemodel: false,
    shifttemplatename: "",
    shifttemplatefrom: "",
    shifttemplateto: "",
    shifttemplatebreak: "",
    radiodata: "",
    selecterror: false,
    selectbreakerror: false,
    selectenderror: false,
    requests: 0,
    accepted: 0,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    finished: 0,
    branchlist: [],
    branchview: "",
    branch: "",
    notes: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ isLoader: true });
    this.setState({ timelist, breaklist, monthlydates });
    this.populateData();
    request({
      url: "/client/job_types",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
        let job_role_selected = [];
        request({
          url: "/client/profile",
          method: "POST",
          data: { id: token.username }
        }).then(res => {
          if (res.status === 1) {
            job_role_selected = res.response.result && res.response.result[0] && res.response.result[0].settings.job_roles;
          }
          let jobtypelistselect = [];
          if (job_role_selected.length > 0) {
            this.setState({
              jobtypelistselect: job_role_selected
            });
          } else {
            jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
              value: emp._id,
              label: emp.name
            }));
            this.setState({
              jobtypelistselect
            });
          }
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/client/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        let branchlist = res.response.result.length ? res.response.result.map(list => list.branches) : [];
        let branchlistselected =
          branchlist &&
          branchlist[0].map((branch, key) => ({
            value: branch._id,
            label: branch.branchname,
            locationID: branch.branchlocation
          }));
        this.setState({ branchlistselected });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/client/shifttemplate/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ shifttemplatelist: res.response.result });
        const shifttemplatelistselect = this.state.shifttemplatelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          shifttemplatelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  editpage = id => {
    if (id.status !== 10) {
      return this.props.history.push({
        pathname: "/client/requestemployees",
        state: { rowid: id }
      });
    }
  };
  onChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    e.persist();
    this.setState({ [e.target.name]: value });
  };
  populateData() {
    request({
      url: "/client/shift/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        let getshiftdata = res.response.result.map((item, i) => {
          var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          var evestarthr = item.starttime / 3600;
          var splithr = evestarthr.toString().split(".");
          var startsec = splithr[1];
          if (startsec === undefined) {
            var startmins = "00";
          } else {
            startmins = (+"0" + "." + startsec) * 60;
          }

          if (num.indexOf(splithr[0]) === -1) {
            var starttimehr = splithr[0] + ":" + startmins;
          } else {
            starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
          }
          var eveendhr = item.endtime / 3600;
          var splitendhr = eveendhr.toString().split(".");
          var endsec = splitendhr[1];
          if (endsec === undefined) {
            var endmins = "00";
          } else {
            endmins = (+"0" + "." + endsec) * 60;
          }

          if (num.indexOf(splitendhr[0]) === -1) {
            var endtimehr = splitendhr[0] + ":" + endmins;
          } else {
            endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
          }
          return {
            _id: item._id,
            starttime: starttimehr,
            endtime: endtimehr,
            locations: item.locations,
            branch: item.branch,
            breaktime: item.breaktime,
            start_date: item.start_date,
            end_date: item.end_date,
            job_type: item.job_type,
            title: item.title,
            shiftId: item.shiftId,
            branch1: item.branch_data[0].branchname,
            status: item.status,
            notes: item.notes
          };
        });
        this.setState({
          adminlist: getshiftdata,
          pages: res.response.fullcount,
          currPage: res.response.length,
          added: res.response.groupcount.added,
          accepted: res.response.groupcount.accepted,
          assigned: res.response.groupcount.assigned,
          completed: res.response.groupcount.completed,
          ongoing: res.response.groupcount.ongoing,
          timehseet_approved: res.response.groupcount.timehseet_approved,
          invoice_approved: res.response.groupcount.invoice_approved,
          Payment_completed: res.response.groupcount.Payment_completed,
          expired: res.response.groupcount.expired
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["start_date", "starttime", "job_type", "start_date", "branch", "location", "title", "status"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }

  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Name") {
        state.tableOptions.filter = "name";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/client/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  shifttemplateChange = value => {
    this.setState({ shift_template: value });
    this.state.shifttemplatelist.filter((shift, key) => {
      if (value && shift._id === value.value) {
        this.setState({
          starttime: shift.from,
          endtime: shift.to,
          breaktime1: shift.break
        });
      }
      return true;
    });
  };
  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("Saved!");
  }
  sendRequest(item) {
    request({
      //url: "/client/job_requests",
      url: "/client/shift_request",
      method: "POST",
      data: {
        shiftId: item._id
      }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response.result);
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  EditnestedModalview = () => {
    this.setState({
      edit: false,
      starttime: this.state.starttime.label || this.state.starttime_view,
      endtime: this.state.endtime.label || this.state.endtime_view,
      breaktime1: this.state.breaktime.label || 0,
      shift_type: this.state.shift_type,
      title: this.state.title,
      id: this.state.id,
      start_date1: moment(this.state.start_date_time).format("DD-MM-YYYY"),
      end_date1: moment(this.state.end_date_time).format("DD-MM-YYYY"),
      shift_option: this.state.shift_option,
      status: this.state.status,
      notes: this.state.notes,
      clientview: this.state.clientview,
      client1: this.state.client1,
      job_typeview: this.state.job_typeview,
      job_type1: this.state.job_type1,
      locationview: this.state.locationview,
      branchview: this.state.branchview,
      branch: this.state.branch,
      location1: this.state.location1
    });
  };

  EditModelOpen = (e, event) => {
    e.stopPropagation();
    this.setState({
      eventlarge: !this.state.eventlarge
    });
    this.eventset(event);
    this.EditModelClose();
  };
  eventset = event => {
    if (event.start_date) {
      request({
        url: "/client/shift/view",
        method: "POST",
        data: { id: event._id }
      }).then(res => {
        if (res.status === 1) {
          this.setState({
            emplist: res.response.result[0],
            job_type1: res.response.result[0].job_type
          });
          var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          var evestarthr = this.state.emplist.starttime / 3600;
          var splithr = evestarthr.toString().split(".");
          var startsec = splithr[1];
          if (startsec === undefined) {
            var startmins = "00";
          } else {
            startmins = (+"0" + "." + startsec) * 60;
          }

          if (num.indexOf(splithr[0]) === -1) {
            var starttimehr = splithr[0] + ":" + startmins;
          } else {
            starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
          }

          var eveendhr = this.state.emplist.endtime / 3600;
          var splitendhr = eveendhr.toString().split(".");
          var endsec = splitendhr[1];
          if (endsec === undefined) {
            var endmins = "00";
          } else {
            endmins = (+"0" + "." + endsec) * 60;
          }

          if (num.indexOf(splitendhr[0]) === -1) {
            var endtimehr = splitendhr[0] + ":" + endmins;
          } else {
            endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
          }
          this.setState({
            edit: false,
            starttime: starttimehr,
            starttime_view: starttimehr,
            endtime: endtimehr,
            breaktime1: this.state.emplist.breaktime / 60,
            endtime_view: endtimehr,
            breaktime_view: this.state.emplist.breaktime / 60,
            starttime_edit: this.state.emplist.starttime,
            endtime_edit: this.state.emplist.endtime,
            breaktime_edit: this.state.emplist.breaktime,
            shift_type: this.state.emplist.shift_type,
            title: this.state.emplist.title,
            id: this.state.emplist._id,
            start_date_time: this.state.emplist.start_date,
            end_date_time: this.state.emplist.end_date,
            start_date1: moment(this.state.emplist.start_date).format("DD-MM-YYYY"),
            end_date1: moment(this.state.emplist.end_date).format("DD-MM-YYYY"),
            shift_option: this.state.emplist.shift_option,
            status: this.state.emplist.status,
            notes: this.state.emplist.notes,
            client1: res.response.result[0].client_data[0]._id || "",
            clientview: this.state.emplist.client,
            job_typeview: this.state.emplist.job_type,
            job_type1: res.response.result[0].jobtype_data[0]._id || "",
            locationview: this.state.emplist.locations,
            branch: this.state.emplist.branch,
            branchview: this.state.emplist.branch_data ? this.state.emplist.branch_data[0].branchname : "",
            location1: this.state.emplist.location_data ? this.state.emplist.location_data[0]._id : ""
          });
          this.EditnestedModal();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    } else {
      this.setState({
        eventlarge: !this.state.eventlarge,
        edit: false,
        id: "",
        title: "",
        location: "",
        starttime: "",
        endtime: "",
        breaktime: "",
        start_date: null,
        end_date: null,
        shift_option: "",
        job_type: "",
        shift_type: "",
        shift_template: "",
        client: "",
        notes: "",
        branch: ""
      });
    }
  };
  EditnestedModal = () => {
    if (this.state.shift_option === "weekly") {
      var sunday = this.state.sun;
      var monday = this.state.mon;
      var tueday = this.state.tue;
      var wedday = this.state.wed;
      var thuday = this.state.thu;
      var friday = this.state.fri;
      var satday = this.state.sat;
    } else {
      sunday = false;
      monday = false;
      tueday = false;
      wedday = false;
      thuday = false;
      friday = false;
      satday = false;
    }
    if (this.state.shift_option === "weekly") {
      this.setState({
        weekly: true,
        rotational: false,
        monthly: false
      });
    } else if (this.state.shift_option === "rotational") {
      this.setState({
        weekly: false,
        rotational: true,
        monthly: false
      });
    } else if (this.state.shift_option === "monthly") {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: true
      });
    } else {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: false
      });
    }
    this.setState({
      edit: true,
      starttime: this.state.starttime_edit || this.state.starttime.value,
      endtime: this.state.endtime_edit || this.state.endtime.value,
      breaktime1: this.state.breaktime_edit || 0,
      shift_type: this.state.shift_type,
      title: this.state.title,
      id: this.state.id,
      start_date1: moment(this.state.start_date_time),
      end_date1: moment(this.state.end_date_time),
      shift_option: this.state.shift_option,
      status: this.state.status,
      notes: this.state.notes,
      clientview: this.state.clientview,
      client1: this.state.client1,
      job_typeview: this.state.job_typeview,
      job_type1: this.state.job_type1,
      locationview: this.state.locationview,
      branch: this.state.branch,
      branchview: this.state.branchview,
      location1: this.state.location1,
      sun: sunday,
      mon: monday,
      tue: tueday,
      wed: wedday,
      thu: thuday,
      fri: friday,
      sat: satday
    });
  };
  EditModelClose = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      title: "",
      location1: "",
      starttime: "",
      endtime: "",
      breaktime1: "",
      start_date1: null,
      end_date1: null,
      shift_type: "",
      job_type1: "",
      branch: "",
      shift_option: "",
      shift_template: "",
      start_date_time: null,
      end_date_time: null,
      edit: false,
      notes: "",
      client: ""
    });
    this.componentDidMount();
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type: value.value });
    }
  };
  OnFormEdit = (e, values) => {
    request({
      url: "/client/shift/save",
      method: "POST",
      data: {
        id: this.state.id,
        // title: this.state.title,
        locations: this.state.location1,
        branch: this.state.branch,
        client: this.state.client1,
        job_type: this.state.job_type1,
        starttime: this.state.starttime.value || this.state.starttime,
        endtime: this.state.endtime.value || this.state.endtime,
        breaktime: this.state.breaktime1.value || this.state.breaktime1,
        start_date: this.state.start_date1,
        end_date: this.state.end_date1,
        shift_type: this.state.shift_type,
        shift_option: this.state.shift_option,
        notes: this.state.notes,
        status: this.state.status,
        monthly_select_day: this.state.monthlydate,
        option_data: this.state.option_data
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Shift Updated!");
          this.setState({
            eventlarge: !this.state.eventlarge,
            slotdata: [],
            start_date1: null,
            end_date1: null,
            title: "",
            location1: "",
            starttime: "",
            endtime: "",
            breaktime1: "",
            shift_type: "",
            job_type1: "",
            shift_option: "",
            shift_template: "",
            option_data: [],
            start_date_time: null,
            end_date_time: null,
            client: "",
            notes: "",
            branch: ""
          });
          this.componentDidMount();
          this.props.history.push({
            pathname: "/client/requestemployees",
            state: { rowid: { _id: res.shiftId, status: res.shift_status } }
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ starttime: value, selecterror: false });
    } else {
      this.setState({ starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ endtime: value, selectenderror: false });
    } else {
      this.setState({ endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktime1: value, selectbreakerror: false });
    } else {
      this.setState({ breaktime1: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.starttime === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.endtime === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.breaktime1 === "") {
      this.setState({
        selectbreakerror: true
      });
    }
  };
  selectdateChange = value => {
    if (value) {
      this.setState({ monthlydate: value });
    } else {
      this.setState({ monthlydate: "" });
    }
  };
  worklocationselect = value => {
    if (value) {
      this.setState({ location1: value.value });
    }
  };
  branchselect = value => {
    if (value) {
      this.setState({ branch: value.value, location1: value.locationID });
    }
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type1: value.value });
    }
  };
  ShiftChange = num => {
    if (num) {
      this.setState(state => {
        state.tableOptions.status = num;
        this.populateData();
      });
    } else {
      this.setState(state => {
        state.tableOptions.status = "";
        this.populateData();
      });
    }
  };
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row className="sub-ranges">
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(1)} header={this.state.added ? this.state.added : "0"} mainText="New" icon="fa fa-send" color="primary" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(3)} header={this.state.accepted ? this.state.accepted : "0"} mainText="Accepted" icon="fa fa-check-circle" color="warning" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(4)} header={this.state.assigned ? this.state.assigned : "0"} mainText="Assigned" icon="fa fa-list" color="info" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(5)} header={this.state.ongoing ? this.state.ongoing : "0"} mainText="Ongoing" icon="fa fa-spinner" color="success" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(6)} header={this.state.completed ? this.state.completed : "0"} mainText="Completed" icon="fa fa-check-square-o" color="success" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(7)} header={this.state.timehseet_approved ? this.state.timehseet_approved : "0"} mainText="Timesheets Approved" icon="fa fa-check" color="warning" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(8)} header={this.state.invoice_approved ? this.state.invoice_approved : "0"} mainText="Invoice Approved" icon="fa fa-check" color="info" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(9)} header={this.state.Payment_completed ? this.state.Payment_completed : "0"} mainText="Paid" icon="fa fa-money" color="danger" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={() => this.ShiftChange(10)} header={this.state.expired ? this.state.expired : "0"} mainText="Expired" icon="fa fa-close" color="secondary" />
          </Col>
        </Row>
        <Card>
          <CardHeader>
            <i className="icon-list" />
            <span className="cursor-pointer" onClick={() => this.ShiftChange("")}>
              Shift List
            </span>
            <div
              className="card-actions primary"
              style={tStyle}
              onClick={() => {
                this.props.history.push("/client/editshift");
              }}
            >
              <button style={tStyles}>
                <i className="fa fa-calendar" /> <strong> Calender View</strong>
                {/*<small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-between">
              {/* <div className="col-lg-1 pl-0">
                <Button
                  size="md"
                  color="primary pull-left"
                  onClick={this.export}
                >
                  Export
                </Button>
              </div> */}
              <div className="col-lg-7">
                <DateRangePicker
                  showClearDates={true}
                  startDate={this.state.start_date}
                  startDateId="start_date"
                  endDate={this.state.end_date}
                  endDateId="end_date"
                  onDatesChange={({ startDate, endDate }) => {
                    if (startDate === null && endDate === null) {
                      this.setState(state => {
                        state.tableOptions.from_date = "";
                        state.tableOptions.to_date = "";
                      });
                      this.populateData();
                    }
                    this.setState({
                      start_date: startDate,
                      end_date: endDate
                    });
                  }}
                  isOutsideRange={day => day.isBefore(this.state.start_date)}
                  focusedInput={this.state.focusedInput}
                  onFocusChange={focusedInput => this.setState({ focusedInput })}
                  displayFormat="DD-MM-YYYY"
                />
                <Button
                  className="rounded-0"
                  size="md"
                  color="primary"
                  onClick={() => {
                    this.fromTo();
                  }}
                >
                  <i className="fa fa-search" />
                </Button>
              </div>
              <div className="col-lg-5 pr-0">
                <InputGroup>
                  <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                    <option>All</option>
                  </Input>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0 col-lg-6" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("title");
                      }}
                    >
                      Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("location");
                      }}
                    >
                      Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="location" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("branch");
                      }}
                    >
                      Work Location {/* branch */} <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("job_type");
                      }}
                    >
                      Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_type" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("starttime");
                      }}
                    >
                      Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("start_date");
                      }}
                    >
                      Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" />
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("end_date");
                      }}
                    >
                      End Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th>*/}
                    <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.title}</td>
                        <td>{item.locations}</td>
                        <td>{item.branch1}</td>
                        <td>{item.job_type}</td>
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        {/*<td>{moment(item.end_date).format('DD-MM-YYYY')}</td>*/}
                        <td>
                          {item.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                          {item.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                          {item.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                          {item.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                          {item.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                          {item.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                          {item.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                          {item.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                          {item.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                          {item.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                        </td>
                        <td>
                          {/* {item.status === 1 ? (
                            <div>
                              <button
                                type="button"
                                title="Edit"
                                className="btn btn-info btn-sm"
                                id={`edit${i}`}
                                onClick={id => this.sendRequest(item)}
                              >
                                <i className="fa fa-envelope" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                Send Request
                              </UncontrolledTooltip>
                            </div>
                          ) : ( */}
                          <div className={"d-flex"}>
                            {item.status !== 10 ? (
                              <Fragment>
                                <button type="button" title="Edit" className="btn  view-table mr-1" id={`edit${i}`} onClick={id => this.editpage(item)}>
                                  <i className="fa fa-eye" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                  View
                                </UncontrolledTooltip>
                              </Fragment>
                            ) : null}
                            {item.status < 3 ? (
                              <Fragment>
                                {item.status !== 10 ? (
                                  <Fragment>
                                    <button type="button" title="Edit" className="btn  view-table" id={`edit21${i}`} onClick={(e, id) => this.EditModelOpen(e, item)}>
                                      <i className="fa fa-edit" />
                                    </button>
                                    <UncontrolledTooltip placement="top" target={`edit21${i}`}>
                                      Edit Shifts
                                    </UncontrolledTooltip>
                                  </Fragment>
                                ) : null}
                              </Fragment>
                            ) : null}
                          </div>
                          {/* )} */}
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={11}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
        <Modal isOpen={this.state.eventlarge} className={"modal-lg " + this.props.className}>
          {this.state.edit ? (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>EDIT SHIFT</ModalHeader>
              <AvForm onValidSubmit={this.OnFormEdit}>
                <ModalBody>
                  <Row>
                    <Col xs="12" md="4">
                      <Label>Work Location {/* branch */}</Label>
                      <Select name="branch" value={this.state.branch} options={this.state.branchlistselected} onChange={this.branchselect} />
                    </Col>
                    <Col xs="12" md="4">
                      <Label>Role</Label>
                      <Select name="form-field-name2" value={this.state.job_type1} options={this.state.jobtypelistselect} onChange={this.saveChanges} />
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="3">
                      <Label>Shift</Label>
                      <Select name="form-field-name2" value={this.state.shift_template} options={this.state.shifttemplatelistselect} onChange={this.shifttemplateChange} />
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                      <Select className={this.state.selecterror ? "custom_select_validation" : null} name="starttime" value={this.state.starttime} options={this.state.timelist} onChange={this.selectstarttimeChange} />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select className={this.state.selectenderror ? "custom_select_validation" : null} name="form-field-name2" value={this.state.endtime} options={this.state.timelist} onChange={this.selectendtimeChange} />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                        <Label>
                          Break <small>(In Minutes)</small>
                        </Label>
                      )}
                      <Select className={this.state.selectbreakerror ? "custom_select_validation" : null} name="form-field-name2" value={this.state.breaktime1} options={this.state.breaklist} onChange={this.selectbreaktimeChange} />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="6" className="p-0">
                      <Col xs="12" md="12">
                        <AvGroup>
                          <Label>Notes :</Label>
                          <AvInput type="textarea" name="notes" placeholder="Enter Notes" onChange={this.onChange} value={this.state.notes} autoComplete="notes" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                    </Col>
                    <Col xs="12" md="6" className="p-0">
                      <Col xs="12" md="12">
                        <AvField
                          type="select"
                          name="shift_type"
                          label="Day"
                          placeholder="Select shift_type"
                          onChange={this.onChange}
                          value={this.state.shift_type}
                          validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          disabled
                        >
                          <option>Please Select</option>
                          <option value="single">Single Day</option>
                          {/* <option value="multiple">Multiple Days</option> */}
                        </AvField>
                      </Col>
                      {this.state.shift_type === "single" ? (
                        <Row>
                          <Col sm="12">
                            <p>Select Date</p>
                            <SingleDatePicker
                              date={this.state.start_date1}
                              openDirection={this.state.openDirection}
                              onDateChange={date => this.setState({ start_date: date })}
                              focused={this.state.focused}
                              onFocusChange={({ focused }) => this.setState({ focused })}
                              id="start_date"
                              displayFormat="DD-MM-YYYY"
                            />
                          </Col>
                        </Row>
                      ) : null}
                      {this.state.shift_type === "multiple" ? (
                        <div>
                          <Row>
                            <Col md="12">
                              <p>Select Date</p>
                              <DateRangePicker
                                startDate={this.state.start_date}
                                startDateId="start_date"
                                endDate={this.state.end_date}
                                endDateId="end_date"
                                onDatesChange={({ startDate, endDate }) =>
                                  this.setState({
                                    start_date: startDate,
                                    end_date: endDate
                                  })
                                }
                                focusedInput={this.state.focusedInput}
                                onFocusChange={focusedInput => this.setState({ focusedInput })}
                                displayFormat="DD-MM-YYYY"
                              />
                            </Col>
                          </Row>
                          <Row>
                            <Col md="12">
                              <AvRadioGroup name="shift_option" onChange={this.onChange} value={this.state.shift_option} required>
                                <AvRadio label="Daily" value="daily" />
                                <AvRadio label="Every Weekday (Mon - Fri)" value="weekday" onClick={this.radioselected} />
                                <AvRadio label="Weekly (Selected Days)" value="weekly" onClick={() => this.radioselected("Weekly")} />
                                {this.state.weekly ? (
                                  <div>
                                    <Row>
                                      <Col md="1" />
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="sun" value={this.state.sun} onChange={this.onChange} />
                                          <span>Sun</span>
                                        </Label>
                                      </Col>
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="mon" onChange={this.onChange} value={this.state.mon} />
                                          Mon
                                        </Label>
                                      </Col>
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="tue" onChange={this.onChange} value={this.state.tue} />
                                          Tue
                                        </Label>
                                      </Col>
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="wed" onChange={this.onChange} value={this.state.wed} />
                                          Wed
                                        </Label>
                                      </Col>
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="thu" onChange={this.onChange} value={this.state.thu} />
                                          Thu
                                        </Label>
                                      </Col>
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="fri" onChange={this.onChange} value={this.state.fri} />
                                          Fri
                                        </Label>
                                      </Col>
                                      <Col md="1">
                                        <Label>
                                          <AvInput type="checkbox" name="sat" onChange={this.onChange} value={this.state.sat} />
                                          sat
                                        </Label>
                                      </Col>
                                    </Row>
                                  </div>
                                ) : null}
                                <AvRadio label="X Days On / Y Days Off" value="rotational" onClick={() => this.radioselected("rotational")} />
                                {this.state.rotational ? (
                                  <Row>
                                    <Col xs="12" md="2">
                                      <AvField type="text" name="xday" onChange={this.onChange} value={this.state.xday} />
                                    </Col>{" "}
                                    /
                                    <Col xs="12" md="2">
                                      <AvField type="text" name="yday" onChange={this.onChange} value={this.state.yday} />
                                    </Col>
                                  </Row>
                                ) : null}
                                <AvRadio label="Monthly (Selected Date)" value="monthly" onClick={() => this.radioselected("monthly-day")} />
                                {this.state.monthly ? (
                                  <div className="mt-2 ml-3">
                                    <Row>
                                      <Col xs="12" md="3">
                                        <Label for="from">Select Date</Label>
                                        <Select name="monthlydate" value={this.state.monthlydate} options={this.state.monthlydates} onChange={this.selectdateChange()} />
                                      </Col>
                                    </Row>
                                  </div>
                                ) : null}
                              </AvRadioGroup>
                            </Col>
                          </Row>
                        </div>
                      ) : null}
                    </Col>
                  </Row>
                </ModalBody>
                <ModalFooter className="justify-content-between modal-gal">
                  <div>
                    <Button color="secondary" onClick={this.EditModelClose} title="Close">
                      <i className="fa fa-close" />
                    </Button>
                    {/* <Button color="secondary" className="ml-3" onClick={this.EditnestedModalview} title="Back to View Details">
                      <i className="fa fa-arrow-left" />
                    </Button> */}
                  </div>
                  <Button color="success" type="submit" title="Save & Continue" id="savecontinue2" onClick={this.selectfield}>
                    {" "}
                    <i className="fa fa-arrow-right" />
                  </Button>{" "}
                  <UncontrolledTooltip placement="left" target="savecontinue2">
                    Save & Continue
                  </UncontrolledTooltip>
                </ModalFooter>
              </AvForm>
            </Fragment>
          ) : (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>Shift Details</ModalHeader>
              <ModalBody>
                <Row>
                  <Col xs="12" md="4">
                    <b>Shift:</b> {this.state.title}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Client:</b> {this.state.clientview}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Area:</b> {this.state.locationview}
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Work Location {/* branch */}:</b> {this.state.branchview}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Role:</b> {this.state.job_typeview}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Date:</b>
                    {this.state.start_date1}{" "}
                  </Col>
                </Row>{" "}
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Start Time:</b> {this.state.starttime} - {this.state.endtime}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Break:</b> {this.state.breaktime_view} Minutes
                  </Col>
                  <Col xs="12" md="4">
                    <b>Shift Status:</b>
                    {this.state.emplist.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                    {this.state.emplist.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                    {this.state.emplist.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                    {this.state.emplist.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                    {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                    {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                    {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                    {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                    {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                    {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                  </Col>
                  {/*<Col xs="12" md="4">*/}
                  {/*<b>End Date:</b>*/}
                  {/*{this.state.end_date1}{" "}*/}
                  {/*</Col>*/}
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="6">
                    <b>Notes :</b> {this.state.notes}
                  </Col>
                  {this.state.shift_type === "multiple" ? (
                    <Col xs="12" md="8">
                      <span>
                        <b>Multiple Day:</b>
                        {this.state.shift_option === "daily" ? <span>Daily </span> : null}
                        {this.state.shift_option === "weekday" ? <span>Every Weekday (Mon - Fri)</span> : null}
                        {this.state.shift_option === "weekly" ? (
                          <span>
                            Weekly {this.state.sun === true ? <strong>Sun, </strong> : null}
                            {this.state.mon === true ? <strong>Mon, </strong> : null}
                            {this.state.tue === true ? <strong>Tue, </strong> : null}
                            {this.state.wed === true ? <strong>Wed, </strong> : null}
                            {this.state.thu === true ? <strong>Thu, </strong> : null}
                            {this.state.fri === true ? <strong>Fri, </strong> : null}
                            {this.state.sat === true ? <strong>Sat </strong> : null}
                            Days
                          </span>
                        ) : null}
                        {this.state.shift_option === "rotational" ? (
                          <span>
                            X Days : {this.state.xday} / Y Days : {this.state.yday}
                          </span>
                        ) : null}
                        {this.state.shift_option === "monthly" ? <span>Monthly {this.state.monthlydate} Days</span> : null}{" "}
                      </span>
                    </Col>
                  ) : null}
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between modal-gal">
                <Button color="secondary" onClick={this.EditModelClose} title="Close">
                  <i className="fa fa-close" />
                </Button>
                <div>
                  {this.state.emplist.status !== 10 ? (
                    <Button
                      color="success"
                      className="ml-2"
                      onClick={() => {
                        this.props.history.push({
                          pathname: "/client/requestemployees",
                          state: { rowid: { _id: this.state.id, status: this.state.status } }
                        });
                      }}
                      title="View Assign Employee"
                    >
                      <i className="fa fa-eye" />
                    </Button>
                  ) : null}
                  {this.state.emplist.status < 3 ? (
                    <Fragment>
                      {this.state.emplist.status !== 10 ? (
                        <Button color="primary" className="ml-2" onClick={this.EditnestedModal} title="Edit">
                          <i className="fa fa-edit" />
                        </Button>
                      ) : null}
                    </Fragment>
                  ) : null}
                </div>
              </ModalFooter>
            </Fragment>
          )}
        </Modal>
      </div>
    );
  }
}

export default Shiftlist;
