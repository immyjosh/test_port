import moment from "moment";
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import "react-select/dist/react-select.css";
import { toast, ToastContainer } from "react-toastify";
import { Label, Badge, Button, Card, CardBody, CardFooter, CardHeader, Col, Input, InputGroup, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table, UncontrolledTooltip } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import RequestMapView from "./requestmap";
import Pagination from "react-js-pagination";
import { AvGroup, AvFeedback, AvInput, AvForm } from "availity-reactstrap-validation";
import { breaklist, timelist, monthlydates } from "../../common/utils";
import "react-dates/initialize";
import { SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import FileSaver from "file-saver";
import Rating from "react-rating";

const tStyle = {
  cursor: "pointer"
};

class RequestEmployees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      branch1: "",
      starttime1: "",
      endtime1: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      job_type: "",
      value: [],
      sortOrder: true,
      bulk: [],
      count: 0,
      pages: "",
      currPage: 25,
      tableOptions: {
        search: "",
        filter: "all",
        page: {
          history: "",
          current: 1
        },
        order: "",
        field: "",
        limit: 10,
        skip: 0,
        shiftId: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid._id
      },
      worklist: [],
      employee_requested: [],
      shiftId: "",
      employeeId: "",
      empcurrPage: "",
      mapviewShow: true,
      deletedisable: true,
      profilemodal: false,
      Profile_details: "",
      Profile_name: "",
      Profile_email: "",
      Profile_job_type: "",
      Profile_avatar: "",
      Profile_locations: "",
      Profile_holiday_allowance: "",
      Profile_joining_date: "",
      Profile_final_date: "",
      Profile_address: "",
      Profile_username: "",
      Profile_phone: "",
      Profile_status: "",
      emplist_status: "",
      RequestMap: false,
      RowID: "",
      Shift_ID: "",
      RowIDC: ""
    };
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ Shift_ID: this.props.location.state.rowid._id });
    this.setState({ timelist, breaklist, monthlydates });
    request({
      url: "/client/shift/view",
      method: "POST",
      data: { id: this.state.Shift_ID || this.props.location.state.rowid._id }
    })
      .then(res => {
        if (res.status === 1) {
          const brdata = res.response.result[0];
          const clientDetails =
            brdata.branch_data &&
            brdata.branch_data.length &&
            brdata.branch_data.map(client => {
              const { branches } = client;
              return {
                lat: branches.branchlat,
                lng: branches.branchlng,
                name: res.response.result ? res.response.result[0].client : "",
                email: res.response.result ? res.response.result[0].client_data[0].email : "",
                avatar: res.response.result ? res.response.result[0].client_data[0].avatar : "",
                locations: res.response.result ? res.response.result[0].locations : "",
                job_type: res.response.result ? res.response.result[0].job_type : "",
                label: "C"
              };
            });
          this.setState({
            emplist: res.response.result[0],
            employee_requested: res.response.result[0].employee_requested,
            // pages: res.response.fullcount,
            // currPage: res.response.length
            mapInfo: {
              ...brdata,
              client: clientDetails
            }
          });
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = this.state.emplist.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startsec = splithr[1];
          const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
          const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          const eveendhr = this.state.emplist.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          this.setState({
            RowID: this.state.Shift_ID || this.props.location.state.rowid._id,
            shiftId: this.state.emplist._id,
            title: this.state.emplist.title,
            breaktime: this.state.emplist.breaktime / 60 + " Minutes",
            end_date: moment(this.state.emplist.end_date).format("DD-MM-YYYY"),
            endtime: endtimehr,
            start_date: moment(this.state.emplist.start_date).format("DD-MM-YYYY"),
            starttime: starttimehr,
            location: this.state.emplist.locations,
            branch: this.state.emplist.branch_data[0].branches.branchname,
            job_type: this.state.emplist.job_type,
            notes: this.state.emplist.notes,
            shift_type: this.state.emplist.shift_type
          });
          this.populateData();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error(err);
      });
    request({
      url: "/client/job_types",
      method: "POST"
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            jobtypelist: res.response.result || {}
          });
          let job_role_selected = [];
          request({
            url: "/client/profile",
            method: "POST",
            data: { id: token.username }
          }).then(res => {
            if (res.status === 1) {
              job_role_selected = res.response.result && res.response.result[0] && res.response.result[0].settings.job_roles;
            }
            let jobtypelistselect = [];
            if (job_role_selected.length > 0) {
              this.setState({
                jobtypelistselect: job_role_selected
              });
            } else {
              jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
                value: emp._id,
                label: emp.name
              }));
              this.setState({
                jobtypelistselect
              });
            }
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
    request({
      url: "/client/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        const branchlist = res.response.result.length ? res.response.result.map(list => list.branches) : [];
        const branchlistselected =
          branchlist &&
          branchlist[0].map((branch, key) => ({
            value: branch._id,
            label: branch.branchname,
            locationID: branch.branchlocation
          }));
        this.setState({ branchlistselected });
      }
    });
    request({
      url: "/client/shifttemplate/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ shifttemplatelist: res.response.result });
        const shifttemplatelistselect = this.state.shifttemplatelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          shifttemplatelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  populateData() {
    request({
      url: "/client/shift/get_employees",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          employee_assign: res.response.result,
          employee_requested: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.result.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["locations", "email", "name", "distance", "employee_ratings"];
    for (const i in id) {
      if (i) {
        document.getElementById(id[i]).className = sorticondef;
      }
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Address") {
        state.tableOptions.filter = "address";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }

  RequestEmployeetoastId = "RequestEmployee";
  nodify() {
    if (!toast.isActive(this.RequestEmployeetoastId)) {
      this.RequestEmployeetoastId = toast.warn("Updated");
    }
  }
  RequestEmployeenodifytoastId = "RequestEmployee";
  adminsavenodify() {
    if (!toast.isActive(this.RequestEmployeenodifytoastId)) {
      this.RequestEmployeenodifytoastId = toast.success("Successfully Saved!");
    }
  }
  assignShift = item => {
    request({
      url: "/client/assign_shift",
      method: "POST",
      data: {
        shiftId: this.state.shiftId,
        employeeId: item.employee,
        status: this.state.emplist.status,
        employeeData: item
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.componentDidMount();
          setTimeout(() => {
            this.componentDidMount();
          }, 2500);
          this.closeprofileModal();
          this.setState({ profilemodal: false, RequestMap: true, RowIDC: this.state.RowID });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  };
  assignShiftOther = item => {
    request({
      url: "/client/assign_shift",
      method: "POST",
      data: {
        shiftId: this.state.shiftId,
        employeeId: item.employee,
        status: this.state.emplist.status,
        employeeData: item
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.componentDidMount();
          setTimeout(() => {
            this.componentDidMount();
          }, 2000);
          this.closeprofileModal();
          this.setState({ profilemodal: false, RequestMap: true, RowIDC: this.state.RowID });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  };

  checkbox(index, id) {
    const c = document.getElementById(index);
    this.setState({
      singlecheck: true
    });
    const ca = document.getElementById("checkall");
    let count = this.state.count;
    const len = this.state.currPage;
    const bulk = this.state.bulk;
    if (c.checked === true) {
      count++;
      bulk.push(id);
      this.setState({ count, bulk });
    } else {
      count--;
      const i = bulk.indexOf(id);
      bulk.splice(i, 1);
      this.setState({ count, bulk });
    }
    if (count === len) {
      ca.checked = true;
      this.checkall("checkall", len);
    } else {
      ca.checked = false;
    }
  }
  checkall(id, len) {
    const c = document.getElementById(id);
    const val = [];
    if (c.checked) {
      this.setState({ count: len });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = true;
        val.push(document.getElementById(i).value);
      }
      this.setState({ bulk: val });
    } else {
      this.setState({ count: 0 });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = false;
      }
      this.setState({ bulk: [] });
    }
  }
  sendtoselected(val) {
    request({
      url: "/client/send_shift_requests",
      method: "POST",
      data: {
        shiftId: this.state.Shift_ID || this.props.location.state.rowid._id,
        employees: val
      }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response.result);
        this.componentDidMount();
        this.setState({ RequestMap: true, RowIDC: this.state.RowID });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  viewprofileModal = item => {
    this.setState({
      profilemodal: !this.state.profilemodal,
      Profile_details: item,
      Profile_name: item.name,
      Profile_username: item.username,
      Profile_phone: item.phone,
      Profile_email: item.email,
      Profile_locations: item.locations,
      Profile_holiday_allowance: item.holiday_allowance,
      Profile_joining_date: moment(item.joining_date).format("DD-MM-YYYY"),
      Profile_final_date: moment(item.final_date).format("DD-MM-YYYY"),
      Profile_address: item.address,
      Profile_job_type: item.job_type,
      Profile_avatar: item.avatar,
      Profile_status: item.status,
      emplist_status: this.state.emplist.status
    });
  };
  closeprofileModal = item => {
    this.setState({
      profilemodal: !this.state.profilemodal,
      Profile_details: "",
      Profile_name: "",
      Profile_username: "",
      Profile_phone: "",
      Profile_email: "",
      Profile_locations: "",
      Profile_holiday_allowance: "",
      Profile_joining_date: "",
      Profile_final_date: "",
      Profile_address: "",
      Profile_job_type: "",
      Profile_avatar: "",
      Profile_status: "",
      emplist_status: ""
    });
  };
  EditModelOpen = event => {
    this.setState({
      eventlarge: !this.state.eventlarge
    });
    this.eventset(event);
    this.EditModelClose();
  };
  eventset = event => {
    request({
      url: "/client/shift/view",
      method: "POST",
      data: { id: event }
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          edit: true,
          breaktime1: res.response.result[0].breaktime,
          shift_type: res.response.result[0].shift_type,
          title: res.response.result[0].title,
          id: res.response.result[0]._id,
          start_date1: moment(res.response.result[0].start_date),
          shift_option: res.response.result[0].shift_option,
          status: res.response.result[0].status,
          notes: res.response.result[0].notes,
          client1: res.response.result[0].client_data[0]._id || "",
          job_type1: res.response.result[0].jobtype_data[0]._id || "",
          branch1: res.response.result[0].branch || "",
          location1: res.response.result[0].location_data ? res.response.result[0].location_data[0]._id : "",
          starttime1: res.response.result[0].starttime,
          endtime1: res.response.result[0].endtime
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  EditModelClose = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      title: "",
      location1: "",
      starttime1: "",
      endtime1: "",
      breaktime1: "",
      start_date1: null,
      end_date1: null,
      shift_type: "",
      job_type1: "",
      branch1: "",
      shift_option: "",
      shift_template: "",
      start_date_time: null,
      end_date_time: null,
      edit: false,
      notes: "",
      client: ""
    });
    this.componentDidMount();
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type: value.value });
    }
  };
  shifttemplateChange = value => {
    this.setState({ shift_template: value });
    this.state.shifttemplatelist.filter((shift, key) => {
      if (value && shift._id === value.value) {
        this.setState({
          starttime1: shift.from,
          endtime1: shift.to,
          breaktime1: shift.break
        });
      }
      return true;
    });
  };
  OnFormEdit = (e, values) => {
    request({
      url: "/client/shift/save",
      method: "POST",
      data: {
        id: this.state.id,
        // title: this.state.title,
        locations: this.state.location1,
        branch: this.state.branch1,
        client: this.state.client1,
        job_type: this.state.job_type1,
        starttime: this.state.starttime1.value || this.state.starttime1,
        endtime: this.state.endtime1.value || this.state.endtime1,
        breaktime: this.state.breaktime1.value || this.state.breaktime1,
        start_date: this.state.start_date1,
        end_date: this.state.end_date1,
        shift_type: this.state.shift_type,
        shift_option: this.state.shift_option,
        notes: this.state.notes,
        status: this.state.status,
        monthly_select_day: this.state.monthlydate,
        option_data: this.state.option_data
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Shift Updated!");
          this.setState({
            slotdata: [],
            start_date1: null,
            end_date1: null,
            title: "",
            location1: "",
            starttime1: "",
            endtime1: "",
            breaktime1: "",
            shift_type: "",
            job_type1: "",
            shift_option: "",
            shift_template: "",
            option_data: [],
            start_date_time: null,
            end_date_time: null,
            client: "",
            notes: "",
            branch1: ""
          });
          this.componentDidMount();
          this.EditModelClose();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ starttime1: value, selecterror: false });
    } else {
      this.setState({ starttime1: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ endtime1: value, selectenderror: false });
    } else {
      this.setState({ endtime1: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktime1: value, selectbreakerror: false });
    } else {
      this.setState({ breaktime1: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.starttime1 === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.endtime1 === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.breaktime1 === "") {
      this.setState({
        selectbreakerror: true
      });
    }
  };
  selectdateChange = value => {
    if (value) {
      this.setState({ monthlydate: value });
    } else {
      this.setState({ monthlydate: "" });
    }
  };
  worklocationselect = value => {
    if (value) {
      this.setState({ location1: value.value });
    }
  };
  branchselect = value => {
    if (value) {
      this.setState({ branch1: value.value, location1: value.locationID });
    }
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type1: value.value });
    }
  };
  openRequest = () => {
    let toastId = "Open_Request_02";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/client/open_request_to_all",
      method: "POST",
      data: {
        shiftId: this.props.location.state.rowid._id || this.state.Shift_ID
      }
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        setTimeout(() => {
          this.componentDidMount();
        }, 2500);
        toast.update(toastId, { render: res.response.result, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.setState({ RequestMap: true, RowIDC: this.state.RowID });
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  profileDownloadPDF = value => {
    let toastId = "Down_Profile_02";
    let name = this.state.Profile_details.name;
    if (value === "download") {
      client.defaults.responseType = "blob";
      toastId = toast.info("Downloading......", { autoClose: false });
    } else {
      client.defaults.responseType = "json";
      toastId = toast.info("Sending......", { autoClose: false });
    }
    request({
      url: "/client/employee/downloadpdf",
      method: "POST",
      data: {
        id: this.state.Profile_details.employee,
        for: value
      }
    })
      .then(res => {
        if (res.status === 1) {
          if ((res.msg = "Mail Sent")) {
            toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
          }
        } else if (res.status === 0) {
          toast.error(res.response);
        } else {
          if (res) {
            const file = new Blob([res], { type: "application/pdf" });
            const profile = name ? name.replace(" ", "_") : "Profile";
            FileSaver(file, `${profile}.pdf`);
            toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
          }
        }
          client.defaults.responseType = "json";
      })
      .catch(error => {
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        client.defaults.responseType = "json";
      });
  };
  unAssign(item) {
    request({
      url: "/client/unassign_shift",
      method: "POST",
      data: {
        id: this.state.Shift_ID,
        employeeid: item.employee
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          toast.success("Shift Cancelled");
        } else if (res.status === 0) {
          this.componentDidMount();
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    // const { tableData } = this.state;
    // let current = this.state.tableOptions.page.current;
    const limit = this.state.tableOptions.limit;
    const len = Math.ceil(this.state.pages / limit);
    const arr = [];
    for (let i = 1; i <= len; i++) {
      arr.push(i);
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Assign Employee
            {this.state.emplist && this.state.emplist.status < 3 ? (
              <div className="card-actions" style={tStyle} onClick={id => this.EditModelOpen(this.state.Shift_ID || this.props.location.state.rowid._id)}>
                <button>
                  <i className="fa fa-edit" /> Edit Shift
                  {/* <small className="text-muted" />*/}
                </button>
              </div>
            ) : null}
          </CardHeader>
          <Fragment>
            <CardBody>
              <Row>
                <Col xs="12" md="8">
                  <div>
                    <Row>
                      <div className="dash-points">
                        <Col xs="12" md="6">
                          <span className="po-left">
                            {" "}
                            <b>
                              {" "}
                              <i className="fa fa-bars" /> Shift :
                            </b>{" "}
                          </span>
                          <span className="po-right"> {this.state.title} </span>
                        </Col>

                        <Col xs="12" md="6">
                          <span className="po-left">
                            {" "}
                            <b>
                              {" "}
                              <i className="fa fa-map-marker" /> Area:
                            </b>{" "}
                          </span>
                          <span className="po-right"> {this.state.location} </span>
                        </Col>
                      </div>
                    </Row>

                    <Row>
                      <div className="dash-points">
                        <Col xs="12" md="6">
                          <span className="po-left">
                            {" "}
                            <b>
                              {" "}
                              <i className="fa fa-map-marker" /> Work Location {/* branch */}:
                            </b>
                          </span>
                          <span className="po-right"> {this.state.branch} </span>
                        </Col>
                        <Col xs="12" md="6">
                          <span className="po-left">
                            {" "}
                            <b>
                              {" "}
                              <i className="fa fa-arrow-circle-right" /> Job Role:
                            </b>
                          </span>
                          <span className="po-right"> {this.state.job_type} </span>
                        </Col>
                      </div>
                    </Row>

                    <Row>
                      <div className="dash-points">
                        <Col xs="12" md="6">
                          <span className="po-left">
                            {" "}
                            <b>
                              {" "}
                              <i className="fa fa-calendar"> </i> Date:
                            </b>{" "}
                          </span>
                          <span className="po-right"> {this.state.start_date} </span>
                        </Col>
                        <Col xs="12" md="6">
                          <span className="po-left">
                            {" "}
                            <b>
                              {" "}
                              <i className="fa fa-clock-o" /> Time:{" "}
                            </b>{" "}
                          </span>
                          <span className="po-right">
                            {" "}
                            {this.state.starttime} - {this.state.endtime}{" "}
                          </span>
                        </Col>
                      </div>
                    </Row>

                    {this.state.shift_type === "single" ? (
                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-clock-o" /> Break:
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.breaktime} </span>
                          </Col>
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-certificate" aria-hidden="true" /> Shift Status:
                              </b>{" "}
                            </span>
                            <span className="po-right">
                              {this.state.emplist.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                              {this.state.emplist.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                              {this.state.emplist.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                              {this.state.emplist.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                              {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                              {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                              {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                              {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                              {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                              {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                            </span>
                          </Col>
                        </div>
                      </Row>
                    ) : null}
                    {this.state.shift_type === "multiple" ? (
                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-comment-o" aria-hidden="true" /> Notes :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.notes} </span>
                          </Col>
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-clock-o" /> Date :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.start_date} </span>
                          </Col>
                          {/* <Col xs="12" md="4">*/}
                          {/* <b>End Date:</b> {this.state.end_date}*/}
                          {/* </Col>*/}
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                <i className="fa fa-filter" aria-hidden="true" /> Type :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.shift_type} </span>
                          </Col>
                        </div>
                      </Row>
                    ) : null}
                    <br />
                  </div>
                  {this.state && this.state.emplist ? (
                    <Fragment>
                      {this.state && this.state.emplist && this.state.emplist.status === 1 ? (
                        <Fragment>
                          <div className="row d-flex justify-content-between">
                            <div className="col-lg-7">
                              <Button className="rounded-0" color="success" id="open_shiftss" onClick={this.openRequest}>
                                Open Shift
                              </Button>
                              <UncontrolledTooltip placement="top" target="open_shiftss">
                                Open Shift (Send To All)
                              </UncontrolledTooltip>
                            </div>
                            <div className="col-lg-5 pr-0">
                              <InputGroup>
                                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                                  <option>All</option>
                                  <option>Name</option>
                                </Input>
                                <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0 col-lg-6" />
                                <Button
                                  className="rounded-0"
                                  color="primary"
                                  id="clear"
                                  onClick={() => {
                                    ReactDOM.findDOMNode(this.refs.search).value = "";
                                    this.search("");
                                  }}
                                >
                                  <i className="fa fa-remove" />
                                </Button>
                                <UncontrolledTooltip placement="top" target="clear">
                                  Clear
                                </UncontrolledTooltip>
                              </InputGroup>
                            </div>
                          </div>
                          <div className="table-responsive mt-3 mb-4">
                            <Table hover bordered responsive size="sm">
                              <thead>
                                <tr>
                                  <th>
                                    <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.currPage)} />
                                  </th>
                                  <th>S.No.</th>
                                  <th
                                    onClick={() => {
                                      this.sort("name");
                                    }}
                                  >
                                    Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("email");
                                    }}
                                  >
                                    Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("locations");
                                    }}
                                  >
                                    Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("distance");
                                    }}
                                  >
                                    Distance <i style={{ paddingLeft: "25px" }} className={sorticondef} id="distance" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("employee_ratings");
                                    }}
                                  >
                                    Rating <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee_ratings" />
                                  </th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.employee_assign ? (
                                  this.state.employee_assign.map((item, i) => (
                                    <tr key={i}>
                                      <td>
                                        <input type="checkbox" className="checkbox1" id={i} value={item.employee} onClick={() => this.checkbox(i, item.employee)} />
                                      </td>
                                      <td>{this.state.tableOptions.skip + i + 1}</td>
                                      <td>{item.name}</td>
                                      <td>{item.email}</td>
                                      <td>
                                        {" "}
                                        {item.locations.map((list, key) => (
                                          <span key={key}>
                                            {list} <Fragment>,</Fragment>
                                          </span>
                                        ))}
                                      </td>
                                      <td>{item.distance}</td>
                                      <td>
                                        <Rating initialRating={item.employee_ratings} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} value={item.employee_ratings} readonly />
                                      </td>
                                      <td>
                                        {this.state && this.state.emplist && this.state.emplist.status === 1 ? (
                                          <Fragment>
                                            {/* <Badge color="success">Available</Badge>*/}
                                            <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShift(item)}>
                                              Assign <i className="fa fa-check-circle" />
                                            </Button>{" "}
                                          </Fragment>
                                        ) : (
                                          <Fragment>
                                            {item.status === 1 || item.status === 2 ? (
                                              <Fragment>
                                                {this.state.emplist.status <= 3 ? (
                                                  <Fragment>
                                                    <Badge color="info">Shift Accepted</Badge>
                                                    <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShiftOther(item)}>
                                                      Assign <i className="fa fa-check-circle" />
                                                    </Button>{" "}
                                                  </Fragment>
                                                ) : null}
                                                {this.state.emplist.status > 3 ? <Badge color="info">Assigned</Badge> : null}
                                                {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                                                {this.state.emplist.status === 6 ? <Badge color="danger">Shift Completed</Badge> : null}
                                                {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                                                {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                                                {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                                                {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                                              </Fragment>
                                            ) : (
                                              <Fragment>{this.state.emplist && this.state.emplist.status < 4 ? <Badge color="warning">Requested</Badge> : null}</Fragment>
                                            )}
                                          </Fragment>
                                        )}
                                        <Button size="sm" color="info" className="ml-2" onClick={items => this.viewprofileModal(item)}>
                                          <i className="fa fa-user-circle" />
                                        </Button>
                                      </td>
                                    </tr>
                                  ))
                                ) : (
                                  <tr className="text-center">
                                    <td colSpan={9}>
                                      <h2>
                                        <strong>No record available</strong>
                                      </h2>
                                    </td>
                                  </tr>
                                )}
                              </tbody>
                            </Table>
                            {this.state.bulk.length !== 0 ? (
                              <div>
                                <Button id="delete" size="sm" color="success" title="Assign" onClick={() => this.sendtoselected(this.state.bulk)}>
                                  Send Request <i className="fa fa-check" />
                                </Button>
                              </div>
                            ) : (
                              <Button id="delete" size="sm" color="success" title="Assign" onClick={() => this.sendtoselected(this.state.bulk)} disabled>
                                Send Request <i className="fa fa-check" />
                              </Button>
                            )}
                          </div>
                          <nav className="float-left">
                            <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                              <option>10</option>
                              <option>25</option>
                              <option>50</option>
                              <option>100</option>
                              <option>200</option>
                            </Input>
                          </nav>
                          <nav className="float-right">
                            <div>
                              <Pagination
                                prevPageText="Prev"
                                nextPageText="Next"
                                firstPageText="First"
                                lastPageText="Last"
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.tableOptions.limit}
                                totalItemsCount={this.state.pages}
                                pageRangeDisplayed={this.state.pageRangeDisplayed}
                                onChange={this.paginate}
                              />
                            </div>
                          </nav>
                        </Fragment>
                      ) : (
                        <div className="table-responsive mt-2">
                          <Table hover bordered responsive size="sm">
                            <thead>
                              <tr>
                                {/* <th>
                            <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.empcurrPage)} />
                          </th> */}
                                <th>S.No.</th>
                                <th
                                  onClick={() => {
                                    this.sort("name");
                                  }}
                                >
                                  Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("email");
                                  }}
                                >
                                  Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("locations");
                                  }}
                                >
                                  Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("distance");
                                  }}
                                >
                                  Distance <i style={{ paddingLeft: "25px" }} className={sorticondef} id="distance" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("employee_ratings");
                                  }}
                                >
                                  Rating <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee_ratings" />
                                </th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state && this.state.employee_requested && this.state.employee_requested.length > 0 ? (
                                this.state.employee_requested.map((item, i) => (
                                  <tr key={i}>
                                    <td>{this.state.tableOptions.skip + i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td>
                                      {" "}
                                      {item.locations &&
                                        item.locations.map((list, key) => (
                                          <span key={key}>
                                            {list} <Fragment>,</Fragment>
                                          </span>
                                        ))}
                                    </td>
                                    <td>{item.distance}</td>
                                    <td>
                                      <Rating initialRating={item.employee_ratings} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} value={item.employee_ratings} readonly />
                                    </td>
                                    <td>
                                      {this.state.emplist.status === 1 ? (
                                        <div>
                                          {/* <Badge color="success">Available</Badge>*/}
                                          <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShift(item)}>
                                            Assign <i className="fa fa-check-circle" />
                                          </Button>{" "}
                                        </div>
                                      ) : (
                                        <Fragment>
                                          {item.status === 2 ? (
                                            <Fragment>
                                              {this.state.emplist.status <= 3 ? (
                                                <Fragment>
                                                  <Badge color="info">Shift Accepted</Badge>
                                                  <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShiftOther(item)}>
                                                    Assign <i className="fa fa-check-circle" />
                                                  </Button>{" "}
                                                </Fragment>
                                              ) : null}
                                              {this.state.emplist.status > 3 && this.state.emplist.status < 5 ? (
                                                <>
                                                  <Badge color="info">Assigned</Badge>
                                                  <Button type="submit" size="sm" color="success ml-1 pull-left" title="Assign" onClick={items => this.unAssign(item)}>
                                                    Un-assign <i className="fa fa-check-circle" />
                                                  </Button>{" "}
                                                </>
                                              ) : null}
                                              {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                                              {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                                              {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                                              {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                                              {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                                              {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                                            </Fragment>
                                          ) : (
                                            <Fragment>
                                              {this.state.emplist && this.state.emplist.status < 4 ? (
                                                <>
                                                  <Badge color="warning">Requested</Badge>
                                                  <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShift(item)}>
                                                    Assign <i className="fa fa-check-circle" />
                                                  </Button>{" "}
                                                </>
                                              ) : null}
                                            </Fragment>
                                          )}
                                        </Fragment>
                                      )}
                                      <Button size="sm" color="info" className="ml-2" onClick={items => this.viewprofileModal(item)}>
                                        <i className="fa fa-user-circle" />
                                      </Button>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                <tr className="text-center">
                                  <td colSpan={10}>
                                    <h2>
                                      <strong>No record available</strong>
                                    </h2>
                                  </td>
                                </tr>
                              )}
                            </tbody>
                          </Table>
                          {/* <Button
                      id="delete"
                      size="sm"
                      color="success"
                      title="Assign"
                      onClick={() => this.sendtoselected(this.state.bulk)}
                      disabled={this.state.deletedisable}
                    >
                      Send Request <i className="fa fa-check" />
                    </Button> */}
                        </div>
                      )}
                    </Fragment>
                  ) : null}
                </Col>
                <Col xs="12" md="4" className="p-0">
                  {this.state && this.state.mapInfo ? <RequestMapView locations={this.state.employee_assign} request={this.state.employee_requested} emplist={this.state.mapInfo} /> : null}
                </Col>
              </Row>
            </CardBody>
          </Fragment>
          <CardFooter>
            <Link to="/client/shiftlist">
              <Button type="submit" color="secondary" title="Back">
                <i className="fa fa-arrow-left" /> Back
              </Button>
            </Link>
          </CardFooter>
        </Card>
        <Modal isOpen={this.state.profilemodal} className={"modal-lg view_profile"}>
          <ModalHeader toggle={this.closeprofileModal}>Profile</ModalHeader>
          <ModalBody>
            <section className="empl_profle ">
              <div className="fl_flex">
                <div className="pro_lft">
                  <img alt="" src={NodeURL + "/" + this.state.Profile_avatar} />
                </div>
                <div className="pro_rght">
                  <p>{this.state.Profile_name}</p>
                  <p>{this.state.Profile_email}</p>
                  <p>
                    {" "}
                    {this.state.Profile_job_type &&
                      this.state.Profile_job_type.map((list, key) => (
                        <Fragment key={key}>
                          {list}
                          <Fragment>,</Fragment>
                        </Fragment>
                      ))}
                  </p>
                </div>
              </div>

              <div className="emp_list">
                <ul className="list_det">
                  <li>
                    <span>Name </span>
                    <span>{this.state.Profile_name} </span>
                  </li>
                  <li>
                    <span>Area</span>
                    <span>
                      {this.state.Profile_locations.length > 0 &&
                        this.state.Profile_locations.map((list, key) => (
                          <span key={key}>
                            {list} <Fragment>,</Fragment>
                          </span>
                        ))}
                    </span>
                  </li>{" "}
                  <li>
                    <span>Phone</span>
                    <span>
                      {" "}
                      {this.state.Profile_phone.code} - {this.state.Profile_phone.number}
                    </span>
                  </li>{" "}
                  <li>
                    <span>Job Role </span>
                    <span>
                      {" "}
                      {this.state.Profile_job_type &&
                        this.state.Profile_job_type.map((list, key) => (
                          <Fragment key={key}>
                            {list}
                            <Fragment>,</Fragment>
                          </Fragment>
                        ))}
                    </span>
                  </li>{" "}
                  <li>
                    <span>User Name </span>
                    <span>{this.state.Profile_username}</span>
                  </li>{" "}
                  <li>
                    <span>Holiday Allowance</span>
                    <span>£ {this.state.Profile_holiday_allowance}</span>
                  </li>{" "}
                  <li>
                    <span>Email </span>
                    <span>{this.state.Profile_email}</span>
                  </li>{" "}
                  <li>
                    <span>Joining Date </span>
                    <span>{this.state.Profile_joining_date}</span>
                  </li>
                  <li>
                    <span>Address </span>
                    <span>
                      {this.state.Profile_address.formatted_address} , {this.state.Profile_address.zipcode} .
                    </span>
                  </li>
                </ul>
              </div>
            </section>
          </ModalBody>

          <ModalFooter>
            <Button size="sm" color="info" className="pull-left" onClick={() => this.profileDownloadPDF("download")}>
              <i className="fa fa-download" />
            </Button>
            <Button size="sm" color="info" className="pull-left" onClick={() => this.profileDownloadPDF("mail")}>
              <i className="fa fa-envelope" />
            </Button>
            {this.state.emplist_status === 1 ? (
              <Fragment>
                {/* <Badge color="success">Available</Badge>*/}
                <Button
                  type="submit"
                  size="sm"
                  color="success ml-1"
                  title="Assign"
                  onClick={() => {
                    this.assignShift(this.state.Profile_details);
                  }}
                >
                  Assign <i className="fa fa-check-circle" />
                </Button>{" "}
              </Fragment>
            ) : (
              <Fragment>
                {this.state.Profile_status === 1 || this.state.Profile_status === 2 ? (
                  <Fragment>
                    {this.state.emplist_status <= 3 ? (
                      <Fragment>
                        <Badge color="info">Shift Accepted</Badge>
                        <Button
                          type="submit"
                          size="sm"
                          color="success ml-1"
                          title="Assign"
                          onClick={() => {
                            this.assignShiftOther(this.state.Profile_details);
                          }}
                        >
                          Assign <i className="fa fa-check-circle" />
                        </Button>{" "}
                      </Fragment>
                    ) : null}
                    {this.state.emplist_status > 3 && this.state.emplist_status < 5 ? <Badge color="info">Assigned</Badge> : null}
                    {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                    {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                    {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                    {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                    {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                    {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                  </Fragment>
                ) : (
                  <Fragment>{this.state.emplist && this.state.emplist.status < 4 ? <Badge color="warning">Requested</Badge> : null}</Fragment>
                )}
              </Fragment>
            )}
            <Button size="sm" color="secondary" onClick={this.closeprofileModal}>
              <i className="fa fa-close" />
            </Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.eventlarge} className={"modal-lg " + this.props.className}>
          {this.state.edit ? (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>EDIT SHIFT</ModalHeader>
              <AvForm onValidSubmit={this.OnFormEdit}>
                <ModalBody>
                  <Row>
                    <Col xs="12" md="4">
                      <Label>Work Location {/* branch */}</Label>
                      <Select name="branch1" value={this.state.branch1} options={this.state.branchlistselected} onChange={this.branchselect} />
                    </Col>
                    <Col xs="12" md="4">
                      <Label>Role</Label>
                      <Select name="form-field-name2" value={this.state.job_type1} options={this.state.jobtypelistselect} onChange={this.saveChanges} />
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="3">
                      <Label>Shift</Label>
                      <Select name="form-field-name2" value={this.state.shift_template} options={this.state.shifttemplatelistselect} onChange={this.shifttemplateChange} />
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Time</Label> : <Label for="from">Time</Label>}
                      <Select className={this.state.selecterror ? "custom_select_validation" : null} name="starttime" value={this.state.starttime1} options={this.state.timelist} onChange={this.selectstarttimeChange} />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select className={this.state.selectenderror ? "custom_select_validation" : null} name="form-field-name2" value={this.state.endtime1} options={this.state.timelist} onChange={this.selectendtimeChange} />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                        <Label>
                          Break <small>(In Minutes)</small>
                        </Label>
                      )}
                      <Select className={this.state.selectbreakerror ? "custom_select_validation" : null} name="form-field-name2" value={this.state.breaktime1} options={this.state.breaklist} onChange={this.selectbreaktimeChange} />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="6">
                      <Col xs="12" md="12">
                        <AvGroup>
                          <Label>Notes :</Label>
                          <AvInput type="textarea" name="notes" placeholder="Enter Notes" onChange={this.onChange} value={this.state.notes} autoComplete="notes" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                    </Col>
                    <Col xs="12" md="6">
                      <Col xs="12" md="12">
                        {this.state.shift_type === "single" ? (
                          <Row>
                            <Col sm="12">
                              <p>Select Date</p>
                              <SingleDatePicker
                                date={this.state.start_date1}
                                openDirection={this.state.openDirection}
                                onDateChange={date => this.setState({ start_date1: date })}
                                focused={this.state.focused}
                                onFocusChange={({ focused }) => this.setState({ focused })}
                                id="start_date"
                                displayFormat="DD-MM-YYYY"
                              />
                            </Col>
                          </Row>
                        ) : null}
                      </Col>
                    </Col>
                  </Row>
                </ModalBody>
                <ModalFooter className="justify-content-between">
                  <div>
                    <Button color="secondary" onClick={this.EditModelClose} title="Close">
                      <i className="fa fa-close" />
                    </Button>
                  </div>
                  <Button color="success" type="submit" title="Save & Continue" id="savecontinue2" onClick={this.selectfield}>
                    {" "}
                    <i className="fa fa-arrow-right" />
                  </Button>{" "}
                  <UncontrolledTooltip placement="left" target="savecontinue2">
                    Save & Continue
                  </UncontrolledTooltip>
                </ModalFooter>
              </AvForm>
            </Fragment>
          ) : null}
        </Modal>
      </div>
    );
  }
}

export default RequestEmployees;
