/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, {Component} from 'react';
import {Card, CardHeader, CardBody, NavLink} from 'reactstrap';
import {withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps';
import {Settings} from '../../../Settings/Settings';
import request, { NodeURL } from "../../../api/api";

const defaultZoom = 1;
const defaultCenter = {lat: 37.431489, lng: -122.163719};
const locations = [
  {
    lat: 37.431489,
    lng: -122.163719,
    label: 'S',
    draggable: false,
  },
];

class MarkerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      godlist:[]
    };
  }
  componentDidMount() {
    request({
      url: "/administrators/employee/show_employees",
      method: "POST",
    }).then(res => {
      this.setState({ godlist: res.response.locations });
    });
  }

  render() {
    return this.state.godlist.map((locations, index) => {
        return (
          <MarkerWithInfoWindow key={index.toString()} location={locations}/>
        )
      }
    );
  }
}

class MarkerWithInfoWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const {location} = this.props;

    return (
      <Marker onClick={this.toggle} position={location} title={location.title} label={location.label}>
        {this.state.isOpen &&
        <InfoWindow onCloseClick={this.toggle}>
          <NavLink href={location.www}>{location.title}</NavLink>
        </InfoWindow>}
      </Marker>
    )
  }
}

const GoogleMapsComponent = withScriptjs(withGoogleMap((props) => {
    return (
      <GoogleMap defaultZoom={defaultZoom} defaultCenter={defaultCenter}>
        {<MarkerList locations={locations}/>}
      </GoogleMap>
    );
  }
));

class Showemployees extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      // return this.props.history.replace("/agency");
    }
    request({
      url: "/administrators/employee/show_employees",
      method: "POST",
    }).then(res => {
      this.setState({ godlist: res.response.locations });
    });
  }
  // To use the Google Maps JavaScript API, you must register your app project on the Google API Console and get a Google API key which you can add to your app

  render() {
    return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <i className="icon-map"></i> Employee Locations
          <div className="card-actions">
          </div>
        </CardHeader>
        <CardBody>
          <GoogleMapsComponent
            key="map"
            googleMapURL={Settings.googleMapURL+Settings.googleMapKey}
            loadingElement={<div style={{height: `100%`}}/>}
            containerElement={<div style={{height: `420px`}}/>}
            mapElement={<div style={{height: `100%`}}/>}
          />
        </CardBody>
      </Card>
    </div>
    )
  }
}

export default Showemployees;