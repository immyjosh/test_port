import React, { Component, Fragment } from "react";
import { AvField, AvInput, AvForm, AvGroup, AvFeedback, AvRadio, AvRadioGroup } from "availity-reactstrap-validation";
import moment from "moment";
import BigCalendar from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { ToastContainer, toast } from "react-toastify";
import classnames from "classnames";
import {
  Label,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Input,
  Badge,
  UncontrolledTooltip,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import request from "../../../api/api";
import { breaklist, timelist, monthlydates } from "../../common/utils";
import "react-dates/initialize";
import { DateRangePicker, SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";

let tStyle = {
  cursor: "pointer"
};
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));
const currDate = new Date();
// const currDate1 = currDate.getDate();
const currYear = currDate.getFullYear();
const currMonth = currDate.getMonth();
// const currtime = currDate.getHours() + ":" + currDate.getMinutes();
const OpenDirection = {
  Down: "down",
  Up: "up"
};
class Myshift extends Component {
  state = {
    jobname: "",
    jobrate: "",
    jobmodel: false,
    jobtypelistselect: [],
    eventlarge: false,
    nestedeventlarge: false,
    closeAll: false,
    large: false,
    allday: true,
    title: "",
    locations: "",
    starttime: "",
    endtime: "",
    breaktime: "",
    slotdata: [],
    start_date: null,
    end_date: null,
    shift_type: "",
    multiselecteddays: "",
    job_type: "",
    job_type1: "",
    timelist: [],
    breaklist: [],
    monthlydates: [],
    eventlist: [],
    worklocationlist: [],
    eventdata: "",
    id: "",
    weekly: false,
    rotational: false,
    monthly: false,
    monthlydate: "",
    isGoing: true,
    sun: false,
    mon: false,
    tue: false,
    wed: false,
    thu: false,
    fri: false,
    sat: false,
    jobtypelist: [],
    option_data: {},
    shift_option: "",
    shifttemplatelist: [],
    shifttemplatelistselect: [],
    date_list: [],
    Weekly_select_date_list: [],
    montofri: 0,
    Weekly_select_date_count: 0,
    monthly_date_count: 0,
    count: 0,
    shift_template: "",
    template: "",
    values: "",
    status: "",
    start_date_time: null,
    end_date_time: null,
    edit: false,
    emplist: "",
    openDirection: OpenDirection.Down,
    clientlist: [],
    client: "",
    clientview: "",
    locationview: "",
    branchview: "",
    job_typeview: "",
    xday: "",
    yday: "",
    shifttemplatemodel: false,
    shifttemplatename: "",
    shifttemplatefrom: "",
    shifttemplateto: "",
    shifttemplatebreak: "",
    radiodata: "",
    selecterror: false,
    selectbreakerror: false,
    selectenderror: false,
    branch: "",
    branchlist: [],
    selectjobroleerror: false,
    selectlocationerror: false,
    selectbrancherror: false,
    shifttitle: "",
    notes: "",
    job_role_selected: [],
    activeTab: "single",
    HolidayModal: false,
    HolidayName: "",
    HolidayDate: "",
    HolidayDesc: "",
    totalhours: "0:00",
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all",
    next: <span>Next<i className="fa fa fa-angle-double-right ml-2" ></i></span>,
    previous: <span><i className="fa fa fa-angle-double-left mr-2" ></i>Previous</span>
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ timelist, breaklist, monthlydates });
    // timelist.map((item, i) => {
    // var startarr = item.split(":");
    // var hour = startarr[0] * 3600;
    // var mins = startarr[1] * 60;
    // var sec = hour + mins;
    //    })
    request({
      url: "/client/shift/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        const evelist = res.response.result;
        const eventlist = evelist.map((eve, key) => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const start_split = (eve.starttime / 3600).toString().split(".");
          const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
          const starttimehr = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
          const start_times = starttimehr.split(":");
          const end_split = (eve.endtime / 3600).toString().split(".");
          const endmins = end_split[1] === undefined ? "00" : (+"0" + "." + end_split[1]) * 60;
          const endtimehr = num.indexOf(end_split[0]) === -1 ? end_split[0] + ":" + endmins : +"0" + "" + end_split[0] + ":" + endmins;
          const end_times = endtimehr.split(":");
          const start_d = new Date(eve.start_date);
          start_d.setHours(Number(start_times && start_times[0]));
          start_d.setMinutes(Number(start_times && start_times[1]));
          const end_d = new Date(eve.start_date);
          end_d.setHours(Number(end_times && end_times[0]));
          end_d.setMinutes(Number(end_times && end_times[1]));
          let title = eve.jobtype_data && eve.jobtype_data.length > 0 ? eve.jobtype_data[0].name : "";
          if (eve.status > 3) {
            title = `${eve.employee_data && eve.employee_data.length > 0 ? eve.employee_data[0].name : ""} - ${eve.branch_data && eve.branch_data.length > 0 ? eve.branch_data[0].branchname : ""}`;
          } else {
            title = `${eve.jobtype_data && eve.jobtype_data.length > 0 ? eve.jobtype_data[0].name : ""} - ${eve.branch_data && eve.branch_data.length > 0 ? eve.branch_data[0].branchname : ""}`;
          }
          return {
            title: title,
            shifttitle: eve.title,
            start: start_d ? start_d : eve.start_date,
            end: end_d ? end_d : eve.start_date,
            starttime: eve.starttime,
            endtime: eve.endtime,
            breaktime: eve.breaktime,
            shift_type: eve.shift_type,
            id: eve._id,
            start_date: eve.start_date,
            end_date: eve.end_date,
            shift_option: eve.shift_option || {},
            status: eve.status,
            notes: eve.notes,
            option_data: eve.option_data,
            clientview: eve.client_datas ? eve.client_datas.companyname : "",
            client: eve.client_datas ? eve.client_datas._id : "",
            job_typeview: eve.jobtype_data ? eve.jobtype_data[0].name : "",
            job_type: eve.jobtype_data ? eve.jobtype_data[0]._id : "",
            locationview: eve.location_data ? eve.location_data[0].name : "",
            branchview: eve.branch_data ? eve.branch_data[0].branchname : "",
            branch: eve.branch,
            locations: eve.location_data ? eve.location_data[0]._id : ""
          };
        });
        request({
          url: "/client/daysoff/forshiftslist",
          method: "GET"
        }).then(res => {
          if (res.status === 1) {
            const daysofflist =
              res.response &&
              res.response.result.length > 0 &&
              res.response.result.map(list => ({
                allDay: true,
                title: list.name,
                start: list.dates,
                end: list.dates,
                desc: list.reason,
                status: 12
              }));
            this.setState({
              eventlist: eventlist.concat(daysofflist)
            });
          }
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/client/job_types",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
        console.log('res.response.result ', res.response.result);
       const jobtypelistselect = res.response.result.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          jobtypelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/client/location/list",
      method: "POST",
      data: { status: 1 }
    }).then(res => {
      if (res.status === 1) {
        const branchlist = res.response.result.length ? res.response.result[0].branches : [];
        const branchlistselected =
          branchlist &&
          branchlist.map((branch, key) => ({
            value: branch._id,
            label: branch.branchname,
            locationID: branch.branchlocation
          }));
        this.setState({ branchlistselected });
      }
    });
    request({
      url: "/client/shifttemplate/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ shifttemplatelist: res.response.result ? res.response.result : [] });
        const shifttemplatelistselect = this.state.shifttemplatelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          shifttemplatelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    const start = moment().startOf('month');
    const end = moment().endOf('month');
    request({
      url: "/clientdashboard",
      method: "post",
      data: { selectshift: this.state.selectshift, selecttimesheet: this.state.selecttimesheet, selectinvoice: this.state.selectinvoice, startdate: start, enddate: end }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          if (res.response.result[0].totalhours) {
            const pretotal = Math.abs(res.response.result[0].totalhours.hours);
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const start_split = (pretotal / 3600).toString().split(".");
            const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
            const totalhours = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
            this.setState({ totalhours });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  branchselect = value => {
    if (value) {
      this.setState({ branch: value.value, locations: value.locationID });
    }
  };
  onChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    e.persist();
    this.setState({ [e.target.name]: value });
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  shifttemplateChange = value => {
    this.setState({ shift_template: value });
    this.state.shifttemplatelist.filter((shift, key) => {
      if (value && shift._id === value.value) {
        this.setState({
          starttime: shift.from,
          endtime: shift.to,
          breaktime: shift.break
        });
      }
      return true;
    });
  };
  jobTypeModel = () => {
    this.setState({
      jobmodel: !this.state.jobmodel
    });
  };
  OnFormSubmitjobtype = (e, values) => {
    e.persist();
    request({
      url: "/client/jobtype/save",
      method: "POST",
      data: {
        name: this.state.jobname,
        jobrate: this.state.jobrate,
        status: 1
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("New Role Added");
          this.componentDidMount();
          this.jobTypeModel();
          this.setState({ jobname: "", jobrate: "" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };

  EditnestedModalview = () => {
    this.setState({
      edit: false,
      starttime: this.state.starttime.label || this.state.starttime_view,
      endtime: this.state.endtime.label || this.state.endtime_view,
      breaktime: this.state.breaktime.label || 0,
      shift_type: this.state.shift_type,
      shifttitle: this.state.shifttitle,
      id: this.state.id,
      start_date: moment(this.state.start_date_time).format("DD-MM-YYYY"),
      end_date: moment(this.state.end_date_time).format("DD-MM-YYYY"),
      shift_option: this.state.shift_option,
      status: this.state.status,
      clientview: this.state.clientview,
      client: this.state.client,
      notes: this.state.notes,
      job_typeview: this.state.job_typeview,
      job_type: this.state.job_type,
      locationview: this.state.locationview,
      branchview: this.state.branchview,
      branch: this.state.branch,
      locations: this.state.locations
    });
  };
  EditnestedModal = () => {
    if (this.state.shift_option === "weekly") {
      var sunday = this.state.sun;
      var monday = this.state.mon;
      var tueday = this.state.tue;
      var wedday = this.state.wed;
      var thuday = this.state.thu;
      var friday = this.state.fri;
      var satday = this.state.sat;
    } else {
      sunday = false;
      monday = false;
      tueday = false;
      wedday = false;
      thuday = false;
      friday = false;
      satday = false;
    }
    if (this.state.shift_option === "weekly") {
      this.setState({
        weekly: true,
        rotational: false,
        monthly: false
      });
    } else if (this.state.shift_option === "rotational") {
      this.setState({
        weekly: false,
        rotational: true,
        monthly: false
      });
    } else if (this.state.shift_option === "monthly") {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: true
      });
    } else {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: false
      });
    }
    this.setState({
      edit: true,
      starttime: this.state.starttime_edit || this.state.starttime.value,
      endtime: this.state.endtime_edit || this.state.endtime.value,
      breaktime: this.state.breaktime_edit || 0,
      shift_type: this.state.shift_type,
      shifttitle: this.state.shifttitle,
      id: this.state.id,
      start_date: moment(this.state.start_date_time),
      end_date: moment(this.state.end_date_time),
      shift_option: this.state.shift_option,
      status: this.state.status,
      clientview: this.state.clientview,
      client: this.state.client,
      notes: this.state.notes,
      job_typeview: this.state.job_typeview,
      job_type: this.state.job_type,
      locationview: this.state.locationview,
      branchview: this.state.branchview,
      branch: this.state.branch,
      locations: this.state.locations,
      sun: sunday,
      mon: monday,
      tue: tueday,
      wed: wedday,
      thu: thuday,
      fri: friday,
      sat: satday
    });
  };
  EditModelOpen = event => {
    if (event.status === 12) {
      this.setState({ HolidayModal: !this.state.HolidayModal, HolidayName: event.title, HolidayDate: event.dates, HolidayDesc: event.desc });
    } else {
      this.setState({
        eventlarge: !this.state.eventlarge
      });
      this.eventset(event);
    }
  };
  HolidayModalclose = () => {
    this.setState({ HolidayModal: false, HolidayName: "", HolidayDate: "", HolidayDesc: "" });
  };
  eventset = event => {
    let daycounts = 0;
    this.setState({
      daycount: daycounts,
      date_list: [],
      Weekly_select_date_list: [],
      montofri: 0,
      Weekly_select_date_count: 0,
      monthly_date_count: 0,
      count: 0
    });
    if (event.start_date) {
      request({
        url: "/client/shift/view",
        method: "POST",
        data: { id: event.id }
      }).then(res => {
        if (res.status === 1) {
          this.setState({
            emplist: res.response.result[0],
            job_type1: res.response.result[0].job_type
          });
          let num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          let evestarthr = event.starttime / 3600;
          let splithr = evestarthr.toString().split(".");
          let startsec = splithr[1];
          if (startsec === undefined) {
            var startmins = "00";
          } else {
            startmins = (+"0" + "." + startsec) * 60;
          }

          if (num.indexOf(splithr[0]) === -1) {
            var starttimehr = splithr[0] + ":" + startmins;
          } else {
            starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
          }

          let eveendhr = event.endtime / 3600;
          let splitendhr = eveendhr.toString().split(".");
          let endsec = splitendhr[1];
          if (endsec === undefined) {
            var endmins = "00";
          } else {
            endmins = (+"0" + "." + endsec) * 60;
          }

          if (num.indexOf(splitendhr[0]) === -1) {
            var endtimehr = splitendhr[0] + ":" + endmins;
          } else {
            endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
          }
          this.setState({
            edit: false,
            starttime: starttimehr,
            starttime_view: starttimehr,
            endtime: endtimehr,
            breaktime: event.breaktime / 60,
            endtime_view: endtimehr,
            breaktime_view: event.breaktime / 60,
            starttime_edit: event.starttime,
            endtime_edit: event.endtime,
            breaktime_edit: event.breaktime,
            shift_type: event.shift_type,
            shifttitle: event.shifttitle,
            id: event.id,
            start_date_time: event.start_date,
            end_date_time: event.end_date,
            start_date: moment(event.start_date).format("DD-MM-YYYY"),
            end_date: moment(event.end_date).format("DD-MM-YYYY"),
            shift_option: event.shift_option,
            status: event.status,
            notes: event.notes,
            client: event.client,
            clientview: event.clientview,
            job_typeview: event.job_typeview,
            job_type: res.response.result[0].jobtype_data[0]._id || "",
            locationview: event.locationview,
            branchview: event.branchview,
            branch: event.branch,
            locations: event.locations
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    } else {
      this.setState({
        eventlarge: !this.state.eventlarge,
        edit: false,
        id: "",
        shifttitle: "",
        location: "",
        starttime: "",
        endtime: "",
        breaktime: "",
        start_date: null,
        end_date: null,
        shift_option: "",
        job_type: "",
        shift_type: "",
        shift_template: "",
        notes: "",
        client: ""
      });
    }
  };
  EditModelClose = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      shifttitle: "",
      locations: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      start_date: null,
      end_date: null,
      shift_type: "",
      job_type: "",
      shift_option: "",
      shift_template: "",
      start_date_time: null,
      end_date_time: null,
      edit: false,
      notes: "",
      client: ""
    });
  };
  OpenModel = slot => {
    let now = new Date();
    now.setHours(0, 0, 0, 0);
    let then = new Date(slot.end);

    if (now.getTime() < then.getTime() || now.getTime() === then.getTime() || slot.action === "select") {
      this.setState({
        slotdata: slot,
        large: !this.state.large,
        shift_type: "single"
      });
      this.singledate();
    } else {
      // toast.error("Selected date is in the past!");
    }
  };
  singledate = () => {
    let setsingledate = this.state.slotdata.start || new Date();
    this.setState({
      start_date: moment(setsingledate),
      end_date: moment(setsingledate),
      id: "",
      shifttitle: "",
      locations: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      multiselecteddays: "",
      job_type: "",
      shift_template: "",
      client: "",
      notes: ""
    });
    if (this.state.branchlistselected.length === 1) {
      this.setState({ branch: this.state.branchlistselected[0].value, locations: this.state.branchlistselected[0].locationID });
    }
  };
  CloseModel = () => {
    this.setState({
      large: !this.state.large,
      id: "",
      shifttitle: "",
      locations: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      start_date: null,
      end_date: null,
      multiselecteddays: "",
      job_type: "",
      shift_template: "",
      client: "",
      notes: ""
    });
  };
  radioselected = radiodata => {
    this.setState({ radiodata });
    if (radiodata === "weekly") {
      this.setState({
        weekly: true,
        rotational: false,
        monthly: false
      });
    } else if (radiodata === "rotational") {
      this.setState({
        weekly: false,
        rotational: true,
        monthly: false
      });
    } else if (radiodata === "monthly") {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: true
      });
    } else {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: false
      });
    }
  };
  OnFormSubmit = (e, values) => {
    if (this.state.starttime === "") {
      this.setState({
        selecterror: true
      });
    } else if (this.state.endtime === "") {
      this.setState({
        selectenderror: true
      });
    } else if (this.state.breaktime === "") {
      this.setState({
        selectbreakerror: true
      });
    } else if (this.state.job_type === "") {
      this.setState({
        selectjobroleerror: true
      });
    } else if (this.state.branch === "") {
      this.setState({
        selectbrancherror: true
      });
    } else {
      if (this.state.radiodata === "daily") {
        this.setState({ option_data: { daily: "daily" } });
      } else if (this.state.radiodata === "weekday") {
        this.setState({ option_data: { weekday: "weekday" } });
      } else if (this.state.radiodata === "weekly") {
        this.setState({
          option_data: {
            weekly: {
              0: this.state.sun,
              1: this.state.mon,
              2: this.state.tue,
              3: this.state.wed,
              4: this.state.thu,
              5: this.state.fri,
              6: this.state.sat
            }
          }
        });
      } else if (this.state.radiodata === "rotational") {
        this.setState({ option_data: { rotational: { x: this.state.xday, y: this.state.yday } } });
      } else if (this.state.radiodata === "monthly") {
        this.setState({ option_data: { monthly: this.state.monthlydate } });
      }
      let data = {
        locations: this.state.locations,
        branch: this.state.branch,
        job_type: this.state.job_type,
        starttime: this.state.starttime.value || this.state.starttime,
        endtime: this.state.endtime.value || this.state.endtime,
        breaktime: this.state.breaktime || this.state.breaktime.value || 0,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
        notes: this.state.notes,
        shift_type: this.state.shift_type,
        shift_option: this.state.shift_option,
        option_data: this.state.option_data,
        status: 1
      };
      request({
        url: "/client/shift/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            toast.success("New Shift Added!");
            this.setState({
              large: !this.state.large,
              slotdata: [],
              start_date: null,
              end_date: null,
              shifttitle: "",
              locations: "",
              starttime: "",
              endtime: "",
              breaktime: "",
              shift_type: "",
              job_type: "",
              shift_option: "",
              shift_template: "",
              option_data: [],
              client: "",
              notes: ""
            });
            this.componentDidMount();
            this.props.history.push({
              pathname: "/client/requestemployees",
              state: { rowid: { _id: res.shiftId, status: res.shift_status } }
            });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type: value.value });
    }
  };
  OnFormEdit = (e, values) => {
    // if (this.state.radiodata === "daily") {
    //   this.setState({ option_data: { daily: "daily" } });
    // } else if (this.state.radiodata === "weekday") {
    //   this.setState({ option_data: { weekday: "weekday" } });
    // } else if (this.state.radiodata === "weekly") {
    //   this.setState({
    //     option_data: {
    //       weekly: {
    //         0: this.state.sun,
    //         1: this.state.mon,
    //         2: this.state.tue,
    //         3: this.state.wed,
    //         4: this.state.thu,
    //         5: this.state.fri,
    //         6: this.state.sat
    //       }
    //     }
    //   });
    // } else if (this.state.radiodata === "rotational") {
    //   this.setState({ option_data: { rotational: { x: this.state.xday, y: this.state.yday } } });
    // } else if (this.state.radiodata === "monthly") {
    //   this.setState({ option_data: { monthly: this.state.monthlydate } });
    // }
    request({
      url: "/client/shift/save",
      method: "POST",
      data: {
        id: this.state.id,
        // title: this.state.title,
        locations: this.state.locations,
        branch: this.state.branch,
        client: this.state.client,
        notes: this.state.notes,
        job_type: this.state.job_type,
        starttime: this.state.starttime.value || this.state.starttime,
        endtime: this.state.endtime.value || this.state.endtime,
        breaktime: this.state.breaktime || this.state.breaktime.value || 0,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
        shift_type: this.state.shift_type,
        // shift_option: this.state.shift_option,
        status: this.state.status,
        monthly_select_day: this.state.monthlydate,
        option_data: this.state.option_data
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Shift Updated!");
          this.setState({
            eventlarge: !this.state.eventlarge,
            slotdata: [],
            start_date: null,
            end_date: null,
            shifttitle: "",
            locations: "",
            starttime: "",
            endtime: "",
            breaktime: "",
            shift_type: "",
            job_type: "",
            shift_option: "",
            shift_template: "",
            option_data: [],
            start_date_time: null,
            end_date_time: null,
            client: "",
            notes: ""
          });
          this.componentDidMount();
          this.props.history.push({
            pathname: "/client/requestemployees",
            state: { rowid: { _id: res.shiftId, status: res.shift_status } }
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  shifttemplateModel = () => {
    this.setState({
      shifttemplatemodel: !this.state.shifttemplatemodel
    });
  };
  OnFormSubmitshifttemplate = (e, values) => {
    // e.preventDefault();
    e.persist();
    request({
      url: "/client/shifttemplate/save",
      method: "POST",
      data: {
        name: this.state.shifttemplatename,
        from: this.state.shifttemplatefrom,
        to: this.state.shifttemplateto,
        break: this.state.shifttemplatebreak,
        status: 1
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("New Shift Template Added");
          this.componentDidMount();
          this.shifttemplateModel();
          this.setState({ shifttemplatename: "", shifttemplatefrom: "", shifttemplateto: "", shifttemplatebreak: "" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ starttime: value, selecterror: false });
    } else {
      this.setState({ starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ endtime: value, selectenderror: false });
    } else {
      this.setState({ endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktime: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktime: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.starttime === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.endtime === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.breaktime === "") {
      this.setState({
        selectbreakerror: true
      });
    }
    if (this.state.locations === "") {
      this.setState({
        selectlocationerror: true
      });
    }
    if (this.state.job_type === "") {
      this.setState({
        selectjobroleerror: true
      });
    }
    if (this.state.branch === "") {
      this.setState({
        selectbrancherror: true
      });
    }
  };
  selectdateChange = value => {
    if (value) {
      const values = value.map(value => value.value);
      this.setState({ monthlydate: values });
    } else {
      this.setState({ monthlydate: "" });
    }
  };
  eventStyleGetter(event, start, end, isSelected) {
    if (event.status === 2) {
      const styless = {
        backgroundColor: "#ffc107",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 3 || event.status === 4) {
      const styless = {
        backgroundColor: "#17a2b8",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 5) {
      const styless = {
        backgroundColor: "#4dbd74",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 6 || event.status === 7 || event.status === 8 || event.status === 9) {
      const styless = {
        backgroundColor: "#f86c6b",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 10) {
      const styless = {
        backgroundColor: "#818a91",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 12) {
      const styless = {
        backgroundColor: "#e66929",
        color: "white"
      };
      return {
        style: styless
      };
    } else {
      const styless = {};
      return {
        style: styless
      };
    }
  }
  RangeHandler = (range) => {
    if (range) {
      if (range.length > 0) {
        if (range.length === 7) {
          const end = moment(range[6]).endOf('day');
          this.RangeRequest(range[0], end);
        } else {
          const end = moment(range[0]).endOf('day');
          this.RangeRequest(range[0], end);
        }
      }
    }
  }
  NavHandler = (range, view) => {
    if (range && view === "month") {
      const end = moment(range).endOf('month');
      this.RangeRequest(range, end);
    }
  }
  RangeRequest = (start, end) => {
    request({
      url: "/clientdashboard",
      method: "post",
      data: { selectshift: this.state.selectshift, selecttimesheet: this.state.selecttimesheet, selectinvoice: this.state.selectinvoice, startdate: start, enddate: end }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          if (res.response.result[0].totalhours) {
            const pretotal = Math.abs(res.response.result[0].totalhours.hours);
              const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
              const start_split = (pretotal / 3600).toString().split(".");
              const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
              const totalhours = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
            // const total = (pretotal / 3600).toFixed(2);
            // const split = total.split('.');
            // const totalhours = `${split[0]}:` + split[1] || `00`;
            this.setState({ totalhours });
          } else {
            this.setState({ totalhours: "0:00" });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  render() {
    const formats = {
      timeGutterFormat: "HH:mm",
      eventTimeRangeFormat: () => ""
    };
    const { emplist } = this.state;
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row className="flex justify-content-end mb-2 color-info">
          <div className="mr-3">
            <UncontrolledDropdown>
              <DropdownToggle className="btn-sm btn-info">
                <i className="fa fa-info-circle" />
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem header>Color Information</DropdownItem>
                <DropdownItem className="shift_added">New</DropdownItem>
                <DropdownItem className="shift_requested">Requested</DropdownItem>
                <DropdownItem className="shift_assigned">Accepted / Assigned</DropdownItem>
                <DropdownItem className="shift_ongoing">Ongoing</DropdownItem>
                <DropdownItem className="shift_completed">Completed / Paid</DropdownItem>
                <DropdownItem className="shift_expired">Expired</DropdownItem>
                <DropdownItem className="shift_holiday">Holiday</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </Row>
        <Card>
          <CardHeader>
            <i className="icon-calendar" />
            Shifts <strong />
            <div
              className="card-actions primary"
              style={tStyle}
              onClick={() => {
                this.props.history.push("/client/shiftlist");
              }}
            >
              <button>
                <i className="fa fa-calendar" /> <strong> List View</strong>
                {/* <small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody className={"pb-2"}>
            <Row className="d-flex justify-content-end">
              <Col md="3">
                <Button color="success" title="Total Hours" className={"pull-right"}>
                  <i className="fa fa-clock-o" /> {this.state.totalhours} hrs
                </Button>
              </Col>
            </Row>
          </CardBody>
          <CardBody style={{ height: "44em" }}>
            <BigCalendar
              selectable
              popup
              longPressThreshold={0}
              events={this.state.eventlist}
              onSelectEvent={event => this.EditModelOpen(event)}
              onSelectSlot={slot => this.OpenModel(slot)}
              views={["month", "week", "day"]}
              step={30}
              messages={{ previous: this.state.previous, today: "Today", next: this.state.next, month: "Month", week: "Week", day: "Day" }}
              // min={new Date()}
              // max={new Date("2018")}
              defaultDate={new Date(currYear, currMonth, 1)}
              defaultView="month"
              toolbar={true}
              eventPropGetter={this.eventStyleGetter}
              formats={formats}
              onRangeChange={range => this.RangeHandler(range)}
              onNavigate={(range, view) => this.NavHandler(range, view)}
            />
          </CardBody>
        </Card>
        <Modal isOpen={this.state.large} toggle={this.OpenModel} className={"modal-lg " + this.props.className}>
          <AvForm onValidSubmit={this.OnFormSubmit}>
            <ModalHeader toggle={this.CloseModel}>ADD SHIFTS</ModalHeader>
            <ModalBody>
              <Row className="mb-1">
                {/* <Col xs="12" md="4">
                  <Label className={this.state.selectlocationerror ? "error-color" : ""}>Location</Label>
                  <Select
                    name="locations"
                    className={this.state.selectlocationerror ? "custom_select_validation" : null}
                    value={this.state.locations}
                    options={this.state.worklocationlist}
                    onChange={this.worklocationselect}
                  />
                  {this.state.selectlocationerror ? <div className="error-color"> This is required!</div> : null}
                </Col>*/}
                <Col xs="12" md="4">
                  <Label className={this.state.selectbrancherror ? "error-color" : ""}>Work Location {/* branch */}</Label>
                  <Select
                    name="branch"
                    className={this.state.selectbrancherror ? "custom_select_validation" : null}
                    value={this.state.branch}
                    options={this.state.branchlistselected}
                    onChange={this.branchselect}
                  />
                  {this.state.selectbrancherror ? <div className="error-color"> This is required!</div> : null}
                </Col>
                <Col xs="12" md="4">
                  <Label className={this.state.selectjobroleerror ? "error-color" : ""}>Role</Label>
                  <Select
                    name="job_type"
                    className={this.state.selectjobroleerror ? "custom_select_validation" : null}
                    value={this.state.job_type}
                    options={this.state.jobtypelistselect}
                    onChange={this.saveChanges}
                  />
                  {this.state.selectjobroleerror ? <div className="error-color"> This is required!</div> : null}
                </Col>
              </Row>
              <Row className="mt-3">
                <Col xs="12" md="3">
                  <Label>Shift</Label>
                  <Select name="form-field-name2" value={this.state.shift_template} options={this.state.shifttemplatelistselect} onChange={this.shifttemplateChange} />
                </Col>
                <Col xs="12" md="3">
                  {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                  <Select
                    className={this.state.selecterror ? "custom_select_validation" : null}
                    name="starttime"
                    value={this.state.starttime}
                    options={this.state.timelist}
                    onChange={this.selectstarttimeChange}
                  />
                  {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                </Col>
                <Col xs="12" md="3">
                  {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                  <Select
                    className={this.state.selectenderror ? "custom_select_validation" : null}
                    name="form-field-name2"
                    value={this.state.endtime}
                    options={this.state.timelist}
                    onChange={this.selectendtimeChange}
                  />
                  {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                </Col>
                <Col xs="12" md="3">
                  {this.state.selectbreakerror ? (
                    <Label className="error-color">
                      {" "}
                      Break <small>(In Minutes)</small>
                    </Label>
                  ) : (
                      <Label>
                        Break <small>(In Minutes)</small>
                      </Label>
                    )}
                  <Select
                    className={this.state.selectbreakerror ? "custom_select_validation" : null}
                    name="form-field-name2"
                    value={this.state.breaktime}
                    options={this.state.breaklist}
                    onChange={this.selectbreaktimeChange}
                  />
                  {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                </Col>
              </Row>
              <Row className="mt-3">
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>Notes :</Label>
                    <AvInput type="textarea" name="notes" placeholder="Enter Notes" onChange={this.onChange} value={this.state.notes} autoComplete="notes" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                <Col xs="12" md="12">
                  {/* <Row>
                <Col xs="12" md="4">
                  <AvField
                    type="select"
                    name="shift_type"
                    label="Day"
                    placeholder="Select Shifttype"
                    onChange={this.onChange}
                    value={this.state.shift_type}
                    validate={{ required: { value: true, errorMessage: "This is required!" } }}
                  >
                    <option>Please Select</option>
                    <option value="single" selected>
                      Single Day
                    </option>
                    <option value="multiple">Multiple Days</option>
                  </AvField>
                </Col>
              </Row> */}
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                          className={classnames({ active: this.state.activeTab === "single" })}
                          onClick={() => {
                            this.toggle("single");
                            this.setState({ shift_type: "single" });
                          }}
                      >
                        Single Day
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                          className={classnames({ active: this.state.activeTab === "multiple" })}
                          onClick={() => {
                            this.toggle("multiple");
                            this.setState({ shift_type: "multiple" });
                          }}
                      >
                        Multiple Days
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="single">
                      {this.state.shift_type === "single" ? (
                          <Row>
                            <Col sm="12">
                              <p>Select Date</p>
                              <SingleDatePicker
                                  date={this.state.start_date}
                                  openDirection={this.state.openDirection}
                                  onDateChange={date => this.setState({ start_date: date })}
                                  focused={this.state.focused}
                                  onFocusChange={({ focused }) => this.setState({ focused })}
                                  id="start_date"
                                  displayFormat="DD-MM-YYYY"
                              />
                            </Col>
                          </Row>
                      ) : null}
                    </TabPane>
                    <TabPane tabId="multiple">
                      {this.state.shift_type === "multiple" ? (
                          <div>
                            <Row>
                              <Col md="12">
                                <p>Select Date</p>
                                <DateRangePicker
                                    startDate={this.state.start_date}
                                    startDateId="start_date"
                                    openDirection={this.state.openDirection}
                                    endDate={this.state.end_date}
                                    endDateId="end_date"
                                    onDatesChange={({ startDate, endDate }) =>
                                        this.setState({
                                          start_date: startDate,
                                          end_date: endDate
                                        })
                                    }
                                    isOutsideRange={day => day.isBefore(this.state.start_date)}
                                    focusedInput={this.state.focusedInput}
                                    onFocusChange={focusedInput => this.setState({ focusedInput })}
                                    displayFormat="DD-MM-YYYY"
                                />
                              </Col>
                            </Row>
                            <Row>
                              <Col md="12">
                                <AvRadioGroup name="shift_option" onChange={this.onChange} value={this.state.shift_option} required>
                                  <AvRadio label="Daily" value="daily" onClick={() => this.radioselected("daily")} />
                                  <AvRadio label="Every Weekday (Mon - Fri)" value="weekday" onClick={() => this.radioselected("weekday")} />
                                  <AvRadio label="Weekly (Selected Days)" value="weekly" onClick={() => this.radioselected("weekly")} />
                                  {this.state.weekly ? (
                                      <div>
                                        <Row>
                                          <Col md="1" />
                                          <Col md="1">
                                            <Label>
                                              <Input name="sun" type="checkbox" checked={this.state.sun} onChange={this.onChange} />
                                              Sun
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <Input name="mon" type="checkbox" checked={this.state.mon} onChange={this.onChange} />
                                              Mon
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <Input type="checkbox" name="tue" onChange={this.onChange} checked={this.state.tue} />
                                              Tue
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <Input type="checkbox" name="wed" onChange={this.onChange} checked={this.state.wed} />
                                              Wed
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <Input type="checkbox" name="thu" onChange={this.onChange} checked={this.state.thu} />
                                              Thu
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <Input type="checkbox" name="fri" onChange={this.onChange} checked={this.state.fri} />
                                              Fri
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <Input type="checkbox" name="sat" onChange={this.onChange} checked={this.state.sat} />
                                              sat
                                            </Label>
                                          </Col>
                                        </Row>
                                      </div>
                                  ) : null}

                                  <AvRadio label="X Days On / Y Days Off" value="rotational" onClick={() => this.radioselected("rotational")} />
                                  {this.state.rotational ? (
                                      <Row>
                                        <Col xs="12" md="2">
                                          <AvField type="number" name="xday" onChange={this.onChange} value={this.state.xday} />
                                        </Col>{" "}
                                        /
                                        <Col xs="12" md="2">
                                          <AvField type="number" name="yday" onChange={this.onChange} value={this.state.yday} />
                                        </Col>
                                      </Row>
                                  ) : null}
                                  <AvRadio label="Monthly (Selected Date)" value="monthly" onClick={() => this.radioselected("monthly")} />
                                  {this.state.monthly ? (
                                      <div className="mt-2 ml-3">
                                        <Row>
                                          <Col xs="12" md="3">
                                            <Label for="from">Select Date</Label>
                                            <Select name="monthlydate" value={this.state.monthlydate} options={this.state.monthlydates} onChange={this.selectdateChange} multi/>
                                          </Col>
                                        </Row>
                                      </div>
                                  ) : null}
                                </AvRadioGroup>
                              </Col>
                            </Row>
                          </div>
                      ) : null}
                    </TabPane>
                  </TabContent>
                </Col>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter className="justify-content-between modal-gal">
              <Button color="secondary" onClick={this.CloseModel} title="Close">
                <i className="fa fa-close" />
              </Button>
              <Button color="success" type="submit" title="Save & Continue" id="savecontinue1" onClick={this.selectfield}>
                <i className="fa fa-arrow-right" />
              </Button>
              <UncontrolledTooltip placement="left" target="savecontinue1">
                Save & Continue
              </UncontrolledTooltip>
            </ModalFooter>
          </AvForm>
        </Modal>
        <Modal isOpen={this.state.eventlarge} toggle={this.EditModelOpen} className={"modal-lg " + this.props.className}>
          {this.state.edit ? (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>EDIT SHIFT</ModalHeader>
              <AvForm onValidSubmit={this.OnFormEdit}>
                <ModalBody>
                  <Row>
                    {/* <Col xs="12" md="4">
                      <Label>Location</Label>
                      <Select name="form-field-name2" value={this.state.locations} options={this.state.worklocationlist} onChange={this.worklocationselect} />
                    </Col>*/}
                    <Col xs="12" md="4">
                      <Label>Work Location {/* branch */}</Label>
                      <Select name="branch" value={this.state.branch} options={this.state.branchlistselected} onChange={this.branchselect} />
                    </Col>
                    <Col xs="12" md="4">
                      <Label>Role</Label>
                      <Select name="form-field-name2" value={this.state.job_type} options={this.state.jobtypelistselect} onChange={this.saveChanges} />
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="3">
                      <Label>Shift</Label>
                      <Select name="form-field-name2" value={this.state.shift_template} options={this.state.shifttemplatelistselect} onChange={this.shifttemplateChange} />
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                      <Select
                        className={this.state.selecterror ? "custom_select_validation" : null}
                        name="starttime"
                        value={this.state.starttime}
                        options={this.state.timelist}
                        onChange={this.selectstarttimeChange}
                      />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select
                        className={this.state.selectenderror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.endtime}
                        options={this.state.timelist}
                        onChange={this.selectendtimeChange}
                      />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                          <Label>
                            Break <small>(In Minutes)</small>
                          </Label>
                        )}
                      <Select
                        className={this.state.selectbreakerror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.breaktime}
                        options={this.state.breaklist}
                        onChange={this.selectbreaktimeChange}
                      />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Notes :</Label>
                        <AvInput type="textarea" name="notes" placeholder="Enter Notes" onChange={this.onChange} value={this.state.notes} autoComplete="notes" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="6">
                      {this.state.shift_type === "single" ? (
                          <Row>
                            <Col sm="12">
                              <p>Select Date</p>
                              <SingleDatePicker
                                  date={this.state.start_date}
                                  openDirection={this.state.openDirection}
                                  onDateChange={date => this.setState({ start_date: date })}
                                  focused={this.state.focused}
                                  onFocusChange={({ focused }) => this.setState({ focused })}
                                  id="start_date"
                                  displayFormat="DD-MM-YYYY"
                              />
                            </Col>
                          </Row>
                      ) : null}
                      {this.state.shift_type === "multiple" ? (
                          <div>
                            <Row>
                              <Col md="12">
                                <p>Select Date</p>
                                <DateRangePicker
                                    startDate={this.state.start_date}
                                    startDateId="start_date"
                                    endDate={this.state.end_date}
                                    endDateId="end_date"
                                    onDatesChange={({ startDate, endDate }) =>
                                        this.setState({
                                          start_date: startDate,
                                          end_date: endDate
                                        })
                                    }
                                    focusedInput={this.state.focusedInput}
                                    onFocusChange={focusedInput => this.setState({ focusedInput })}
                                    displayFormat="DD-MM-YYYY"
                                />
                              </Col>
                            </Row>
                            <Row>
                              <Col md="12">
                                <AvRadioGroup name="shift_option" onChange={this.onChange} value={this.state.shift_option} required>
                                  <AvRadio label="Daily" value="daily" />
                                  <AvRadio label="Every Weekday (Mon - Fri)" value="weekday" onClick={this.radioselected} />
                                  <AvRadio label="Weekly (Selected Days)" value="weekly" onClick={() => this.radioselected("Weekly")} />
                                  {this.state.weekly ? (
                                      <div>
                                        <Row>
                                          <Col md="1" />
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="sun" value={this.state.sun} onChange={this.onChange} />
                                              <span>Sun</span>
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="mon" onChange={this.onChange} value={this.state.mon} />
                                              Mon
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="tue" onChange={this.onChange} value={this.state.tue} />
                                              Tue
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="wed" onChange={this.onChange} value={this.state.wed} />
                                              Wed
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="thu" onChange={this.onChange} value={this.state.thu} />
                                              Thu
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="fri" onChange={this.onChange} value={this.state.fri} />
                                              Fri
                                            </Label>
                                          </Col>
                                          <Col md="1">
                                            <Label>
                                              <AvInput type="checkbox" name="sat" onChange={this.onChange} value={this.state.sat} />
                                              sat
                                            </Label>
                                          </Col>
                                        </Row>
                                      </div>
                                  ) : null}
                                  <AvRadio label="X Days On / Y Days Off" value="rotational" onClick={() => this.radioselected("rotational")} />
                                  {this.state.rotational ? (
                                      <Row>
                                        <Col xs="12" md="2">
                                          <AvField type="text" name="xday" onChange={this.onChange} value={this.state.xday} />
                                        </Col>{" "}
                                        /
                                        <Col xs="12" md="2">
                                          <AvField type="text" name="yday" onChange={this.onChange} value={this.state.yday} />
                                        </Col>
                                      </Row>
                                  ) : null}
                                  <AvRadio label="Monthly (Selected Date)" value="monthly" onClick={() => this.radioselected("monthly-day")} />
                                  {this.state.monthly ? (
                                      <div className="mt-2 ml-3">
                                        <Row>
                                          <Col xs="12" md="3">
                                            <Label for="from">Select Date</Label>
                                            <Select name="monthlydate" value={this.state.monthlydate} options={this.state.monthlydates} onChange={this.selectdateChange()} />
                                          </Col>
                                        </Row>
                                      </div>
                                  ) : null}
                                </AvRadioGroup>
                              </Col>
                            </Row>
                          </div>
                      ) : null}
                  </Col>
                  </Row>
                  {/* <Row>
                    <Col xs="12" md="4">
                      <AvField
                        type="select"
                        name="shift_type"
                        label="Day"
                        placeholder="Select shift_type"
                        onChange={this.onChange}
                        value={this.state.shift_type}
                        validate={{ required: { value: true, errorMessage: "This is required!" } }}
                        disabled
                      >
                        <option>Please Select</option>
                        <option value="single">Single Day</option>
                        <option value="multiple">Multiple Days</option>
                      </AvField>
                    </Col>
                  </Row> */}
                </ModalBody>
                <ModalFooter className="justify-content-between modal-gal">
                  <div>
                    <Button color="secondary" onClick={this.EditModelClose} title="Close">
                      <i className="fa fa-close" />
                    </Button>
                    <Button color="secondary" className="ml-3" onClick={this.EditnestedModalview} title="Back to View Details">
                      <i className="fa fa-arrow-left" />
                    </Button>
                  </div>
                  <Button color="success" type="submit" title="Save & Continue" id="savecontinue2" onClick={this.selectfield}>
                    {" "}
                    <i className="fa fa-arrow-right" />
                  </Button>{" "}
                  <UncontrolledTooltip placement="left" target="savecontinue2">
                    Save & Continue
                  </UncontrolledTooltip>
                </ModalFooter>
              </AvForm>
            </Fragment>
          ) : (
              <Fragment>
                <ModalHeader toggle={this.EditModelClose}>Shift Details</ModalHeader>
                <ModalBody>
                  <Row>
                    <div className="dash-points">
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-bars" /> Shift:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.shifttitle} </span>
                      </Col>
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-user" aria-hidden="true" /> Client:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.clientview} </span>
                      </Col>
                    </div>
                  </Row>

                  <Row>
                    <div className="dash-points">
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-map-marker" /> Area:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.locationview} </span>
                      </Col>
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-map-marker" /> Work Location {/* branch */}:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.branchview} </span>
                      </Col>
                    </div>
                  </Row>

                  <Row>
                    <div className="dash-points">
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-arrow-circle-right" /> Role:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.job_typeview} </span>
                      </Col>
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-calendar"> </i> Date:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.start_date}</span>
                      </Col>
                    </div>
                  </Row>

                  <Row>
                    <div className="dash-points">
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-clock-o" /> Start Time:
                        </b>{" "}
                        </span>
                        <span className="po-right">
                          {" "}
                          {this.state.starttime} - {this.state.endtime}{" "}
                        </span>
                      </Col>
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-clock-o" /> Break:
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.breaktime} Minutes </span>
                      </Col>
                    </div>
                  </Row>

                  <Row>
                    <div className="dash-points">
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            {" "}
                            <i className="fa fa-certificate" aria-hidden="true" /> Shift Status:
                        </b>{" "}
                        </span>
                        <span className="po-right">
                          {this.state.emplist.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                          {this.state.emplist.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                          {this.state.emplist.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                          {this.state.emplist.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                          {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                          {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                          {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                          {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                          {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                          {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                        </span>
                      </Col>
                      <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>
                            <i className="fa fa-comment-o" aria-hidden="true" /> Notes :
                        </b>{" "}
                        </span>
                        <span className="po-right"> {this.state.notes}</span>
                      </Col>
                    </div>
                  </Row>

                  <Row>
                    <div className="dash-points">
                      {(emplist && emplist.employee && emplist.employee.length > 0) && <Col xs="12" md="6">
                        <span className="po-left">
                          {" "}
                          <b>{" "}<i className="fa icon-people" aria-hidden="true" /> Employee : </b>{" "}
                        </span>
                        {emplist ? (emplist.employee && emplist.employee.length > 0 && emplist.employee[0]) : ""}{" "}
                      </Col>}
                      <Col xs="12" md="8">
                        {this.state.shift_type === "multiple" ? (
                          <span>
                            <b>Multiple Day:</b>
                            {this.state.shift_option === "daily" ? <span>Daily </span> : null}
                            {this.state.shift_option === "weekday" ? <span>Every Weekday (Mon - Fri)</span> : null}
                            {this.state.shift_option === "weekly" ? (
                              <span>
                                Weekly {this.state.sun === true ? <strong>Sun, </strong> : null}
                                {this.state.mon === true ? <strong>Mon, </strong> : null}
                                {this.state.tue === true ? <strong>Tue, </strong> : null}
                                {this.state.wed === true ? <strong>Wed, </strong> : null}
                                {this.state.thu === true ? <strong>Thu, </strong> : null}
                                {this.state.fri === true ? <strong>Fri, </strong> : null}
                                {this.state.sat === true ? <strong>Sat </strong> : null}
                                Days
                          </span>
                            ) : null}
                            {this.state.shift_option === "rotational" ? (
                              <span>
                                X Days : {this.state.xday} / Y Days : {this.state.yday}
                              </span>
                            ) : null}
                            {this.state.shift_option === "monthly" ? <span>Monthly {this.state.monthlydate} Days</span> : null}{" "}
                          </span>
                        ) : null}
                      </Col>
                    </div>
                  </Row>
                </ModalBody>
                <ModalFooter className="justify-content-between modal-gal">
                  <Button color="secondary rounded-0" onClick={this.EditModelClose} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                  <div>
                    {this.state.emplist.status !== 10 ? (
                      <Button
                        color="success"
                        className="ml-2 rounded-0"
                        onClick={() => {
                          this.props.history.push({
                            pathname: "/client/requestemployees",
                            state: { rowid: { _id: this.state.id, status: this.state.status } }
                          });
                        }}
                        title="View Assign Employee"
                      >
                        <i className="fa fa-eye" />
                      </Button>
                    ) : null}
                    {this.state.emplist.status < 3 ? (
                      <Fragment>
                        {this.state.emplist.status !== 10 ? (
                          <Button color="primary" className="ml-2" onClick={this.EditnestedModal} title="Edit">
                            <i className="fa fa-edit" />
                          </Button>
                        ) : null}
                      </Fragment>
                    ) : null}
                  </div>
                </ModalFooter>
              </Fragment>
            )}
        </Modal>
        <Modal isOpen={this.state.shifttemplatemodel} toggle={this.shifttemplateModel} className={"modal-sm " + this.props.className}>
          <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmitshifttemplate}>
            <ModalHeader toggle={this.shifttemplateModel}>Add Shift Template</ModalHeader>
            <ModalBody>
              <Row>
                <Col xs="12" md="12">
                  <AvGroup>
                    <Label>Shift Title</Label>
                    <AvInput type="text" name="name" placeholder="Enter Shift Title" onChange={this.onChange} value={this.state.shifttemplatename} required autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="6">
                  <AvField type="select" name="from" label="Start Time" placeholder="Select from" onChange={this.onChange} value={this.state.shifttemplaterom}>
                    <option>Select</option>
                    {this.state.timelist.map((e, key) => {
                      return (
                        <option key={key} value={e}>
                          {e}
                        </option>
                      );
                    })}
                  </AvField>
                </Col>
                <Col xs="12" md="6">
                  <AvField type="select" name="to" label="End Time" placeholder="Select to" onChange={this.onChange} value={this.state.shifttemplateto}>
                    <option>Select</option>
                    {this.state.timelist.map((e, key) => {
                      return (
                        <option key={key} value={e}>
                          {e}
                        </option>
                      );
                    })}
                  </AvField>
                </Col>
                <Col xs="12" md="6">
                  <AvField type="select" name="break" label="Break" placeholder="Select break" onChange={this.onChange} value={this.state.shifttemplatebreak}>
                    <option>Select</option>
                    {this.state.breaklist
                      ? this.state.breaklist.map((e, key) => {
                        return (
                          <option key={key} value={e}>
                            {e}
                          </option>
                        );
                      })
                      : null}
                  </AvField>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter className="d-flex justify-content-between modal-gal">
              <Button color="secondary" onClick={this.shifttemplateModel}>
                <i className="fa fa-close" />
              </Button>
              <Button type="submit" color="success" title="Submit">
                <i className="fa fa-check" />
              </Button>{" "}
            </ModalFooter>
          </AvForm>
        </Modal>
        <Modal isOpen={this.state.HolidayModal} toggle={this.HolidayModalOpen} className={this.props.className}>
          <ModalHeader toggle={this.HolidayModalOpen}>Holiday Details</ModalHeader>
          <ModalBody className="pop-shifts">
            <Row>
			  <div className="dash-points">
              <Col xs="12" md="12">
                <span className="po-left"> <b><i class="fa fa-user-o" aria-hidden="true"></i> Name:</b> </span>
				<span className="po-right"> {this.state.HolidayName} </span>
              </Col>
			  </div>
            </Row>
            
            <Row>
			   <div className="dash-points">
              <Col xs="12" md="12">
                <span className="po-left"> <b><i class="fa fa-clock-o"></i> Date:</b> </span>
				<span className="po-right"> {moment(this.state.HolidayDate).format("DD-MM-YYYY")} </span>
              </Col>
			  </div>
            </Row>
           
            <Row>
			<div className="dash-points">
              <Col xs="12" md="12">
                <span className="po-left"> <b><i class="fa fa-commenting-o" aria-hidden="true"></i> Description:</b> </span>
				<span className="po-right"> {this.state.HolidayDesc} </span>
              </Col>
			 </div> 
            </Row>
          </ModalBody>
          <ModalFooter className="modal-gal">
            <Button color="secondary" onClick={this.HolidayModalclose}>
              <i class="fa fa-close"></i>
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Myshift;
