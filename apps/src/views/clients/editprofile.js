/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Popover, PopoverHeader, PopoverBody } from "reactstrap";
import request, { NodeURL } from "../../api/api";
import editnodify from "./clientdashboard";
import deactivatenodify from "./clientlogin/clientlogin";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
// import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { toast, ToastContainer } from "react-toastify";
import Avatar from "react-avatar-edit";
import { Link } from "react-router-dom";

class Editprofile extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    email: "",
    status: "",
    role: "",
    code: "",
    address: "",
    number: "",
    dailcountry: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    avatar: "",
    establishmenttype: "",
    numberofbeds: "",
    patienttype: "",
    specialrequirements: "",
    shiftpatens: "",
    agreedstaffrates: "",
    attain: "",
    invoiceaddress: "",
    invoicephone: "",
    invoicefax: "",
    invoiceemail: "",
    additionallocations: "",
    subscription: "",
    phoneerror: false,
    stausactive: false,
    phonestatus: "",
    avatar_return: "",
    companyname: "",
    company_email: "",
    fax: "",
    _id: "",
    avatarName: "",
    worklist: [{ name: "", address: "", formatted_address2: "", lat: "", lon: "" }],
    DeactivatepopoverOpen: false,
    propreview: null
  };
  flash = new editnodify();
  deactiveflash = new deactivatenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    request({
      url: "/client/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      this.setState({ adminlist: res.response.result[0] });
      this.setState({
        _id: this.state.adminlist._id,
        username: this.state.adminlist.username,
        name: this.state.adminlist.name,
        email: this.state.adminlist.email,
        status: this.state.adminlist.status,
        number: this.state.adminlist.phone.number,
        dailcountry: this.state.adminlist.phone.dailcountry || "gb",
        code: this.state.adminlist.phone.code,
        line1: this.state.adminlist.address.line1,
        line2: this.state.adminlist.address.line2,
        city: this.state.adminlist.address.city,
        state: this.state.adminlist.address.state,
        country: this.state.adminlist.address.country,
        zipcode: this.state.adminlist.address.zipcode,
        lat: this.state.adminlist.address.lat,
        lng: this.state.adminlist.address.lon,
        formatted_address: this.state.adminlist.address.formatted_address,
        avatar_return: this.state.adminlist.avatar,
        avatarName: this.state.adminlist.avatar.substr(24, 30),
        companyname: this.state.adminlist.companyname,
        company_email: this.state.adminlist.company_email,
        fax: this.state.adminlist.fax,
        establishmenttype: this.state.adminlist.establishmenttype,
        numberofbeds: this.state.adminlist.numberofbeds,
        patienttype: this.state.adminlist.patienttype,
        specialrequirements: this.state.adminlist.specialrequirements,
        shiftpatens: this.state.adminlist.shiftpatens,
        agreedstaffrates: this.state.adminlist.agreedstaffrates,
        attain: this.state.adminlist.attain,
        invoiceaddress: this.state.adminlist.invoiceaddress,
        invoicephone: this.state.adminlist.invoicephone,
        invoicefax: this.state.adminlist.invoicefax,
        invoiceemail: this.state.adminlist.invoiceemail,
        additionallocations: this.state.adminlist.additionallocations
        // worklist: this.state.adminlist.locations
      });
      if (this.state.adminlist.status === 1) {
        this.setState({
          stausactive: true
        });
      } else {
        this.setState({
          stausactive: false
        });
      }
    });
  }
  handleChange = address => {
    this.setState({ address });
    this.setState({ formatted_address: address });
  };

  handleAddedu = () => {
    this.setState({ worklist: this.state.worklist.concat([{}]) });
  };
  handleRemove = idx => () => {
    this.setState({
      worklist: this.state.worklist.filter((s, sidx) => idx !== sidx)
    });
  };

  handleeduChange = idx => e => {
    const newedu = this.state.worklist.map((worklist, sidx) => {
      if (idx !== sidx) {
        return worklist;
      } else {
        return { ...worklist, [e.target.name]: e.target.value };
      }
    });
    this.setState({ worklist: newedu });
  };

  /* handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        }else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        }else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name+" "+results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };*/
  OnFormSubmit = (e, values) => {
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else {
      const data = new FormData();
      data.append("_id", this.state._id);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      // data.append("locations", this.state.worklist);
      data.append("establishmenttype", this.state.establishmenttype);
      data.append("numberofbeds", this.state.numberofbeds);
      data.append("patienttype", this.state.patienttype);
      data.append("specialrequirements", this.state.specialrequirements);
      data.append("shiftpatens", this.state.shiftpatens);
      // data.append("agreedstaffrates", this.state.agreedstaffrates);
      // data.append("attain", this.state.attain);
      data.append("invoiceaddress", this.state.invoiceaddress);
      data.append("invoicephone", this.state.invoicephone);
      data.append("invoicefax", this.state.invoicefax);
      data.append("invoiceemail", this.state.invoiceemail);
      // data.append("additionallocations", this.state.additionallocations);
      data.append("companyname", this.state.companyname);
      data.append("company_email", this.state.company_email);
      data.append("fax", this.state.fax);
      data.append("status", this.state.status);
      request({
        url: "/client/profile/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            toast.success("Updated");
            this.componentDidMount();
            // this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  Deactivatetoggle = () => {
    this.setState({
      DeactivatepopoverOpen: !this.state.DeactivatepopoverOpen
    });
  };
  deactivateUser() {
    request({
      url: "/client/profile/deactivate",
      method: "POST",
      data: {
        _id: this.state._id,
        status: 0
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.userdeactive();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  saveuser = () => {
    return this.props.history.push("/client/dashboard"), this.flash.nodify();
  };
  userdeactive() {
    return localStorage.removeItem("APUSC"), this.props.history.replace("/client"), this.deactiveflash.deactivatenodify();
  }
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2,
      phonestatus: status
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          avatar: file_data,
          avatarName: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      var pos = propreview.indexOf(";base64,");
      var type = propreview.substring(5, pos);
      var b64 = propreview.substr(pos + 8);
      // decode base64
      var imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      var buffer = new ArrayBuffer(imageContent.length);
      var view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      var blob = new Blob([buffer], { type: type });
      return blob;
    }
    let dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              <AvForm onValidSubmit={this.OnFormSubmit}>
                <CardHeader>
                  <strong>Edit</strong> Client
                </CardHeader>
                <CardBody>
                  <p className="h5">Basic Details</p>
                  <Row>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Username</Label>
                        <AvInput type="text" name="username" placeholder="Enter Username.." onChange={this.onChange} value={this.state.username} required disabled />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Name</Label>
                        <AvInput type="text" name="name" placeholder="Enter Name.." onChange={this.onChange} value={this.state.name} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <p>Phone</p>
                        {this.state && this.state.dailcountry ? (
                          <IntlTelInput style={{ width: "100%" }} defaultCountry={this.state.dailcountry} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler} value={this.state.number} />
                        ) : null}
                        {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                      </AvGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="6" md="4">
                      <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                    </Col>
                    {this.state.propreview ? (
                      <Col xs="6" md="3">
                        <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                      </Col>
                    ) : null}
                    <Col xs="6" md="3">
                      <Label>Profile Image</Label>
                      <img
                        width="180px"
                        height="180px"
                        src={NodeURL + "/" + this.state.avatar_return}
                        alt="Profile"
                        onError={() => {
                          this.setState({ avatar_return: "../../img/user-profile.png" });
                        }}
                      />
                    </Col>
                  </Row>
                  <br />
                  <p className="h5">Login Details</p>
                  <Row>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Email</Label>
                        <AvInput name="email" type="email" placeholder="Enter Email.." onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <Label>Password</Label>
                      <AvInput type="password" name="password" placeholder="Enter Password.." onChange={this.onChange} value={this.state.password} autoComplete="off" />
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Confirm Password</Label>
                        <AvInput type="password" name="confirm_password" placeholder="Enter Password.." onChange={this.onChange} value={this.state.confirm_password} validate={{ match: { value: "password" } }} autoComplete="off" />
                        <AvFeedback>Match Password!</AvFeedback>
                      </AvGroup>
                    </Col>
                  </Row>
                  {/*  <p className="h5">Address</p>
                  <div>
                    <Label>Search address</Label>
                  </div>
                  <Row>
                    <Col xs="12" md="12">
                      <PlacesAutocomplete value={this.state.formatted_address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                          <div>
                            <input
                              {...getInputProps({
                                placeholder: "Search Places ...",
                                className: "form-control"
                              })}
                            />
                            <div className="autocomplete-dropdown-container absolute">
                              {suggestions.map(suggestion => {
                                const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                  ? {
                                      backgroundColor: "#fafafa",
                                      cursor: "pointer"
                                    }
                                  : {
                                      backgroundColor: "#ffffff",
                                      cursor: "pointer"
                                    };
                                return (
                                  <div
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                      style
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        )}
                      </PlacesAutocomplete>
                    </Col>
                  </Row>
                  <Row className="mt-2">
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Line1</Label>
                        <AvInput type="text" name="line1" placeholder="Enter Line1.." onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City/Town</Label>
                        <AvInput type="text" name="line2" placeholder="Enter City/Town" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                     <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City</Label>
                        <AvInput
                          type="text"
                          name="city"
                          placeholder="Enter City.."
                          onChange={this.onChange}
                          value={this.state.city}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>State/Region</Label>
                        <AvInput type="text" name="state" placeholder="Enter State .." onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Country</Label>
                        <AvInput type="text" name="country" placeholder="Enter Country.." onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Zipcode</Label>
                        <AvInput type="text" name="zipcode" placeholder="Enter Zipcode.." onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  </Row>*/}
                  {/* <p className="h5">Hospital Details</p>
                  <Row>
                    <Col xs="12" md="4">
                      <AvField type="text" label="Name" name="companyname" placeholder="Enter Name.." onChange={this.onChange} value={this.state.companyname} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="email" label="Email" name="company_email" placeholder="Enter Email.." onChange={this.onChange} value={this.state.company_email} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="number" label="Fax" name="fax" placeholder="Enter Fax.." onChange={this.onChange} value={this.state.fax} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="text" label="Type of Establishment" name="establishmenttype" placeholder="Enter Establishment Type.." onChange={this.onChange} value={this.state.establishmenttype} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="number" label="Number of beds" name="numberofbeds" placeholder="Enter Number of Beds.." onChange={this.onChange} value={this.state.numberofbeds} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="text" label="Type of patients" name="patienttype" placeholder="Enter Patient Type.." onChange={this.onChange} value={this.state.patienttype} required />
                    </Col>

                    <Col xs="12" md="4">
                      <AvField type="text" label="Special requirements" name="specialrequirements" placeholder="Enter Special Requirements.." onChange={this.onChange} value={this.state.specialrequirements} required />
                    </Col>
                  </Row>*/}
                  {/* <Row>
                    <Col xs="12" md="4">
                      <AvField
                        type="text"
                        label="Shift Patens"
                        name="shiftpatens"
                        placeholder="Enter Shift Patens.."
                        onChange={this.onChange}
                        value={this.state.shiftpatens}
                        required
                      />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField
                        type="text"
                        label="Agreed Staff Rates"
                        name="agreedstaffrates"
                        placeholder="Enter Agreed Staff Rates.."
                        onChange={this.onChange}
                        value={this.state.agreedstaffrates}
                        required
                      />
                    </Col>

                    <Col xs="12" md="4">
                      <AvField
                        type="text"
                        label="Attain"
                        name="attain"
                        placeholder="Enter Attain.."
                        onChange={this.onChange}
                        value={this.state.attain}
                        required
                      />
                    </Col>
                  </Row> */}
                  {/* <p className="h5">Billing Information</p>
                  <Row>
                    <Col xs="12" md="4">
                      <AvField type="text" label="Billing Fax" name="invoicefax" minLength="4" maxLength="30" placeholder="Enter Billing Fax.." onChange={this.onChange} value={this.state.invoicefax} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="text" label="Billing Address" name="invoiceaddress" minLength="4" maxLength="30" placeholder="Enter Billing Address.." onChange={this.onChange} value={this.state.invoiceaddress} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="number" min="0" label="Billing Phone Number" name="invoicephone" placeholder="Enter Billing Phone.." onChange={this.onChange} value={this.state.invoicephone} required />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="4">
                      <AvField type="email" label="Billing Email" name="invoiceemail" placeholder="Enter Billing Email.." onChange={this.onChange} value={this.state.invoiceemail} required />
                    </Col>
                  </Row>*/}
                  {/* <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>*/}
                </CardBody>
                <CardFooter>
                  <Button type="submit" size="md" color="primary pull-right" title="Update">
                    <i
                      className="fa fa-dot-circle-o"
                      onClick={() => {
                        this.phonefield(this.state.number);
                      }}
                    />
                    Update
                  </Button>
                  <Link to="/client/dashboard">
                    <Button type="submit" size="md" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>

                  <Button id="Popover1" className="ml-5" color="danger" onClick={this.Deactivatetoggle}>
                    <i className="fa fa-remove" /> Deactivate Account
                  </Button>
                  <Popover placement="bottom" isOpen={this.state.DeactivatepopoverOpen} target="Popover1" toggle={this.Deactivatetoggle}>
                    <PopoverHeader>
                      <h5>Are You Sure ?</h5>
                    </PopoverHeader>
                    <PopoverBody>
                      <Button
                        className="ml-1"
                        color="danger"
                        onClick={() => {
                          this.deactivateUser();
                        }}
                        title="Deactivate Account"
                      >
                        Yes
                      </Button>
                      <Button className="ml-3" onClick={this.Deactivatetoggle}>
                        No
                      </Button>
                    </PopoverBody>
                  </Popover>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Editprofile;
