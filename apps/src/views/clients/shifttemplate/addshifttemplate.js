/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";

import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import savenodify from "./listshifttemplate";
import { breaklist, timelist } from "../../common/utils";
import Select from "react-select";
import "react-select/dist/react-select.css";
class Addshifttemplate extends Component {
  state = {
    name: "",
    from: "",
    to: "",
    break: "",
    timelist: [],
    breaklist: [],
    selecterror: false,
    selectbreakerror: false,
    selectenderror: false
  };
  flash = new savenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ timelist, breaklist });
  }
  OnFormSubmit = (e, values) => {
      if (this.state.from === "") {
          this.setState({
              selecterror: true
          });
      } else if (this.state.to === "") {
          this.setState({
              selectenderror: true
          });
      } else if (this.state.break === "") {
          this.setState({
              selectbreakerror: true
          });
      } else {
    // e.preventDefault();
    e.persist();
    const adminform = {
      name: this.state.name,
      from: this.state.from.value,
      to: this.state.to.value,
      break: this.state.break.value,
      status: 1
    };
    request({
      url: "/client/shifttemplate/save",
      method: "POST",
      data: adminform
    })
      .then(res => {
        if (res.status === 1) {
          this.saveuser();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
    }
  };
  saveuser = () => {
    return this.form && this.form.reset(),this.props.history.push("/client/listshifttemplate"), this.flash.adminsavenodify();
  };
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ from: value, selecterror: false });
    } else {
      this.setState({ from: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ to: value, selectenderror: false });
    } else {
      this.setState({ to: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ break: value, selectbreakerror: false });
    } else {
      this.setState({ break: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.from === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.to === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.break === "") {
      this.setState({
        selectbreakerror: true
      });
    }
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />Add Shift Template
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>                
                  <Row>
				  <Col xs="12" md="12">
				    <div className="cus-design edited-section">
				    <Col xs="12" md="12">
				    <p className="h5">Shift Details</p>
					</Col>
                    <Col xs="12" md="3">
                      <AvGroup>
                        <Label>Title</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter Title"
                          onChange={this.onChange}
                          value={this.state.name}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                      <Select
                        className={this.state.selecterror ? "custom_select_validation" : null}
                        name="from"
                        value={this.state.from}
                        options={this.state.timelist}
                        onChange={this.selectstarttimeChange}
                      />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select
                        className={this.state.selectenderror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.to}
                        options={this.state.timelist}
                        onChange={this.selectendtimeChange}
                      />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                        <Label>
                          Break <small>(In Minutes)</small>
                        </Label>
                      )}
                      <Select
                        className={this.state.selectbreakerror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.break}
                        options={this.state.breaklist}
                        onChange={this.selectbreaktimeChange}
                      />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit"  color="success pull-right mb-3" title="Add Shift Template" onClick={this.selectfield}>
                    Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addshifttemplate;
