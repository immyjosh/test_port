import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
// import Loader from '../../common/loader';
import { Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import Pagination from "react-js-pagination";

class Listshifttemplate extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    isLoader: false,
    deletedisable: true,
    subscriptionlist: [],
    loginId: "",
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0
    }
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    // this.setState({isLoader:true});
    this.populateData();
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/client/editshifttemplate",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/client/shifttemplate/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        let getshiftdata = res.response.result.map((item, i) => {
          var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          var evestarthr = item.from / 3600;
          var splithr = evestarthr.toString().split(".");
          var startsec = splithr[1];
          if (startsec === undefined) {
            var startmins = "00";
          } else {
            startmins = (+"0" + "." + startsec) * 60;
          }

          if (num.indexOf(splithr[0]) === -1) {
            var starttimehr = splithr[0] + ":" + startmins;
          } else {
            starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
          }
          var eveendhr = item.to / 3600;
          var splitendhr = eveendhr.toString().split(".");
          var endsec = splitendhr[1];
          if (endsec === undefined) {
            var endmins = "00";
          } else {
            endmins = (+"0" + "." + endsec) * 60;
          }

          if (num.indexOf(splitendhr[0]) === -1) {
            var endtimehr = splitendhr[0] + ":" + endmins;
          } else {
            endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
          }
          return {
            _id: item._id,
            from: starttimehr,
            to: endtimehr,
            name: item.name,
            break: item.break / 60,
            status: item.status
          };
        });
        this.setState({
          isLoader: false,
          adminlist: getshiftdata,
          pages: res.response.fullcount,
          currPage: res.response.length,
          loginId: res.response.loginId
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["name", "from", "to", "break", "status"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "From") {
        state.tableOptions.filter = "from";
      } else if (value === "To") {
        state.tableOptions.filter = "to";
      } else if (value === "Break") {
        state.tableOptions.filter = "break";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  nodify() {
    return toast.warn("Updated");
  }
  toastId = "addshifttemplate";
  adminsavenodify() {
    if (!toast.isActive(this.toastId)) {
      this.toastId = toast.success("New Shift Template Added");
    }
  }
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Shift Template List
            <div className="card-actions">
              <button onClick={() => this.props.history.push("/client/addshifttemplate")}>
                <i className="fa fa-plus" /> Add New
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
              <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Name</option>
                </Input>
              </div>
              <div className="col-lg-3 pl-0">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Shift Title <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("from");
                      }}
                    >
                      Start Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="from" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("to");
                      }}
                    >
                      End Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="to" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("break");
                      }}
                    >
                      Break <i style={{ paddingLeft: "25px" }} className={sorticondef} id="break" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.from}</td>
                        <td>{item.to}</td>
                        <td>{item.break + " mins"}</td>
                        <td>
                          {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                        </td>
                        <td>
                          <div>
                            <button type="button" title="Edit" className="btn view-table" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                              <i className="fa fa-edit" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit${i}`}>
                              Edit
                            </UncontrolledTooltip>
                          </div>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan={"10"} className="text-center">
                        {" "}
                        {!this.state.isLoader && <h4> No Template Added Yet</h4>}
                      </td>
                    </tr>
                  )}
                  {/* { this.state.isLoader && <Loader/> } */}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Listshifttemplate;
