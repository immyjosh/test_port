/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, Input, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";

import { breaklist, timelist } from "../../common/utils";
import Select from "react-select";
import "react-select/dist/react-select.css";
class Editshifttemplate extends Component {
  state = {
    name: "",
    status: "",
    from: "",
    to: "",
    break: "",
    timelist: [],
    breaklist: [],
    stausactive: false,
    selecterror: false,
    selectbreakerror: false,
    selectenderror: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ timelist, breaklist });
    request({
      url: "/client/shifttemplate/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ adminlist: res.response.result[0] });
      this.setState({
        name: this.state.adminlist.name,
        from: this.state.adminlist.from,
        to: this.state.adminlist.to,
        break: this.state.adminlist.break,
        status: this.state.adminlist.status
      });
      if (this.state.adminlist.status === 1) {
        this.setState({
          stausactive: true
        });
      } else {
        this.setState({
          stausactive: false
        });
      }
    });
  }

  OnFormSubmit = (e, values) => {
    if (this.state.from === "") {
      this.setState({
        selecterror: true
      });
    } else if (this.state.to === "") {
      this.setState({
        selectenderror: true
      });
    } else if (this.state.break === "") {
      this.setState({
        selectbreakerror: true
      });
    } else {
      request({
        url: "/client/shifttemplate/save",
        method: "POST",
        data: {
          _id: this.props.location.state.rowid,
          name: this.state.name,
          from: this.state.from,
          to: this.state.to,
          break: this.state.break,
          status: this.state.status
        }
      }).then(res => {
        if (res.status === 1) {
          this.editnodify();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    }
  };
  toastId = "shifttemplateEdit";
  editnodify() {
    if (!toast.isActive(this.toastId)) {
      this.toastId = toast.success("Updated");
    }
    this.componentDidMount();
  }
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ from: value.value, selecterror: false });
    } else {
      this.setState({ from: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ to: value.value, selectenderror: false });
    } else {
      this.setState({ to: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ break: value.value, selectbreakerror: false });
    } else {
      this.setState({ break: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.from === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.to === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.break === "") {
      this.setState({
        selectbreakerror: true
      });
    }
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Edit Shift Template
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
                 
                 
                  <Row>
				  <Col xs="12" md="12">
				    <div className="cus-design edited-section">
					<Col xs="12" md="12">
					   <p className="h5">Shift Details</p>
					</Col>
                    <Col xs="12" md="3">
                      <AvGroup>
                        <Label>Name</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter Name"
                          onChange={this.onChange}
                          value={this.state.name}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                      <Select
                        className={this.state.selecterror ? "custom_select_validation" : null}
                        name="from"
                        value={this.state.from}
                        options={this.state.timelist}
                        onChange={this.selectstarttimeChange}
                      />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select
                        className={this.state.selectenderror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.to}
                        options={this.state.timelist}
                        onChange={this.selectendtimeChange}
                      />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                        <Label>
                          Break <small>(In Minutes)</small>
                        </Label>
                      )}
                      <Select
                        className={this.state.selectbreakerror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.break}
                        options={this.state.breaklist}
                        onChange={this.selectbreaktimeChange}
                      />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
					
					<Col xs="12" md="3">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
					
					</div>
					</Col>
                  </Row>
                 
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update" onClick={this.selectfield}>
                    <i className="fa fa-dot-circle-o" />
                    Update
                  </Button>
                  <Link to="/client/listshifttemplate">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editshifttemplate;
