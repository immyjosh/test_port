/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, CustomInput, FormGroup } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import Select from "react-select";
import "react-select/dist/react-select.css";
import request, { NodeURL } from "../../../api/api";

class General extends Component {
  state = {
    Job_List: [],
    job_roles: []
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    request({
      url: "/client/settings/job_types",
      method: "POST"
    })
      .then(res => {
        if (res.status === 1) {
          let job_roles_list = res.response.result.map(list => ({
            value: list._id,
            label: list.name
          }));
          this.setState({ job_roles_list });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
    request({
      url: "/client/profile",
      method: "POST",
      data: { id: token.username }
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({ job_roles: res.response.result && res.response.result[0].settings && res.response.result[0].settings.job_roles });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }
  Job_select = value => {
    if (value) {
      this.setState({ job_roles: value });
    }
  };
  onEditAdmin = (e, values) => {
    request({
      url: "/client/settings/general/job_roles/save",
      method: "POST",
      data: { job_roles: this.state.job_roles }
    })
      .then(response => {
        if (response.status === 1) {
          toast.success("Updated");
          this.componentDidMount();
        } else if (response.status === 0) {
          toast.error(response);
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>General</strong> Settings
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col>
                      <Label>Select Job Roles</Label>
                      <Select name="branch" value={this.state.job_roles} options={this.state.job_roles_list} onChange={this.Job_select} multi />
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" size="md" color="success pull-right mb-3" title="Save">
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default General;
