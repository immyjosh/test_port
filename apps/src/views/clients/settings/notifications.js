import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Input } from "reactstrap";
import { AvForm } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import request from "../../../api/api";

class Notifications extends Component {
  state = {
    mailstatus: 0,
    smsstatus: 0,
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    request({
      url: "/client/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      this.setState({
        mailstatus: res.response.result ? res.response.result[0].settings.notifications.email: "",
        smsstatus: res.response.result ? res.response.result[0].settings.notifications.sms:""
      });
    });
  }
  statusChangeemail = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      mailstatus: e.target.checked || value
    });
  };
  statusChangesms = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      smsstatus: e.target.checked || value
    });
  };
  onEditAdmin = (e, values) => {
    request({
      url: "/client/settings/notifications/save",
      method: "POST",
      data: {
        mailstatus: this.state.mailstatus,
        smsstatus: this.state.smsstatus
      }
    })
      .then(response => {
        if (response.status === 1) {
          toast.success("Updated");
          this.componentDidMount();
        }
      })
      .catch(error => {
        return toast.error(error);
      });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <ToastContainer position="top-right" autoClose={2500} />
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-settings" />
                  <strong>Notifications</strong> Settings
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>Mail</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.mailstatus} onChange={this.statusChangeemail} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="4">
                      <div>
                        <label>SMS</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.smsstatus} onChange={this.statusChangesms} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" size="md" color="success pull-right mb-3" title="Save">
                    <i className="fa fa-dot-circle-o" /> Save
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Notifications;
