/* eslint no-sequences: 0*/
import { AvForm } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Row, Table, TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import request from "../../../api/api";
import { breaklist, timelist } from "../../common/utils";
import classnames from "classnames";
// import { toast } from "react-toastify";

class Editjobtype extends Component {
  state = {
    name: "",
    status: "",
    client: "",
    clientlist: [],
    mon_day_from: "",
    mon_day_to: "",
    mon_day_client_rate: "",
    mon_day_employee_rate: "",
    tue_day_from: "",
    tue_day_to: "",
    tue_day_client_rate: "",
    tue_day_employee_rate: "",
    wed_day_from: "",
    wed_day_to: "",
    wed_day_client_rate: "",
    wed_day_employee_rate: "",
    thu_day_from: "",
    thu_day_to: "",
    thu_day_client_rate: "",
    thu_day_employee_rate: "",
    fri_day_from: "",
    fri_day_to: "",
    fri_day_client_rate: "",
    fri_day_employee_rate: "",
    sat_day_from: "",
    sat_day_to: "",
    sat_day_client_rate: "",
    sat_day_employee_rate: "",
    sun_day_from: "",
    sun_day_to: "",
    sun_day_client_rate: "",
    sun_day_employee_rate: "",
    public_day_from: "",
    public_day_to: "",
    public_day_client_rate: "",
    public_day_employee_rate: "",
    mon_night_from: "",
    mon_night_to: "",
    mon_night_client_rate: "",
    mon_night_employee_rate: "",
    tue_night_from: "",
    tue_night_to: "",
    tue_night_client_rate: "",
    tue_night_employee_rate: "",
    wed_night_from: "",
    wed_night_to: "",
    wed_night_client_rate: "",
    wed_night_employee_rate: "",
    thu_night_from: "",
    thu_night_to: "",
    thu_night_client_rate: "",
    thu_night_employee_rate: "",
    fri_night_from: "",
    fri_night_to: "",
    fri_night_client_rate: "",
    fri_night_employee_rate: "",
    sat_night_from: "",
    sat_night_to: "",
    sat_night_client_rate: "",
    sat_night_employee_rate: "",
    sun_night_from: "",
    sun_night_to: "",
    sun_night_client_rate: "",
    sun_night_employee_rate: "",
    public_night_from: "",
    public_night_to: "",
    public_night_client_rate: "",
    public_night_employee_rate: "",
    late_fee_amount: "",
    late_fee_duration: "",
    client_id: "",
    activeTab: "1",
    clientname: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "client"))) {
      return this.props.history.replace("/client");
    }
    this.setState({ timelist, breaklist });
    request({
      url: "/client/jobtype/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.status === 1) {
        this.setState({ adminlist: res.response.result[0] });
        const Rate_details = [];
        res.response.result &&
          res.response.result[0].rate_details.map(list => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = list.from / 3600;
            const splithr = evestarthr.toString().split(".");
            const startmins = splithr[1] === undefined ? "00" : (+"0" + "." + splithr[1]) * 60;
            const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            const eveendhr = list.to / 3600;
            const splitendhr = eveendhr.toString().split(".");
            const endmins = splitendhr[1] === undefined ? "00" : (+"0" + "." + splitendhr[1]) * 60;
            const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            Rate_details.push({
              from: starttimehr,
              to: endtimehr,
              client_rate: list.client_rate,
              employee_rate: list.employee_rate
            });
            return true;
          });
        this.setState({
          name: this.state.adminlist ? this.state.adminlist.name : "",
          client_id: this.state.adminlist ? this.state.adminlist.client : "",
          mon_day_from: this.state.adminlist ? Rate_details[0].from : "",
          mon_day_to: this.state.adminlist ? Rate_details[0].to : "",
          mon_day_client_rate: this.state.adminlist ? Rate_details[0].client_rate : "",
          mon_day_employee_rate: this.state.adminlist ? Rate_details[0].employee_rate : "",
          tue_day_from: this.state.adminlist ? Rate_details[1].from : "",
          tue_day_to: this.state.adminlist ? Rate_details[1].to : "",
          tue_day_client_rate: this.state.adminlist ? Rate_details[1].client_rate : "",
          tue_day_employee_rate: this.state.adminlist ? Rate_details[1].employee_rate : "",
          wed_day_from: this.state.adminlist ? Rate_details[2].from : "",
          wed_day_to: this.state.adminlist ? Rate_details[2].to : "",
          wed_day_client_rate: this.state.adminlist ? Rate_details[2].client_rate : "",
          wed_day_employee_rate: this.state.adminlist ? Rate_details[2].employee_rate : "",
          thu_day_from: this.state.adminlist ? Rate_details[3].from : "",
          thu_day_to: this.state.adminlist ? Rate_details[3].to : "",
          thu_day_client_rate: this.state.adminlist ? Rate_details[3].client_rate : "",
          thu_day_employee_rate: this.state.adminlist ? Rate_details[3].employee_rate : "",
          fri_day_from: this.state.adminlist ? Rate_details[4].from : "",
          fri_day_to: this.state.adminlist ? Rate_details[4].to : "",
          fri_day_client_rate: this.state.adminlist ? Rate_details[4].client_rate : "",
          fri_day_employee_rate: this.state.adminlist ? Rate_details[4].employee_rate : "",
          sat_day_from: this.state.adminlist ? Rate_details[5].from : "",
          sat_day_to: this.state.adminlist ? Rate_details[5].to : "",
          sat_day_client_rate: this.state.adminlist ? Rate_details[5].client_rate : "",
          sat_day_employee_rate: this.state.adminlist ? Rate_details[5].employee_rate : "",
          sun_day_from: this.state.adminlist ? Rate_details[6].from : "",
          sun_day_to: this.state.adminlist ? Rate_details[6].to : "",
          sun_day_client_rate: this.state.adminlist ? Rate_details[6].client_rate : "",
          sun_day_employee_rate: this.state.adminlist ? Rate_details[6].employee_rate : "",
          public_day_from: this.state.adminlist ? Rate_details[7].from : "",
          public_day_to: this.state.adminlist ? Rate_details[7].to : "",
          public_day_client_rate: this.state.adminlist ? Rate_details[7].client_rate : "",
          public_day_employee_rate: this.state.adminlist ? Rate_details[7].employee_rate : "",
          mon_night_from: this.state.adminlist ? Rate_details[8].from : "",
          mon_night_to: this.state.adminlist ? Rate_details[8].to : "",
          mon_night_client_rate: this.state.adminlist ? Rate_details[8].client_rate : "",
          mon_night_employee_rate: this.state.adminlist ? Rate_details[8].employee_rate : "",
          tue_night_from: this.state.adminlist ? Rate_details[9].from : "",
          tue_night_to: this.state.adminlist ? Rate_details[9].to : "",
          tue_night_client_rate: this.state.adminlist ? Rate_details[9].client_rate : "",
          tue_night_employee_rate: this.state.adminlist ? Rate_details[9].employee_rate : "",
          wed_night_from: this.state.adminlist ? Rate_details[10].from : "",
          wed_night_to: this.state.adminlist ? Rate_details[10].to : "",
          wed_night_client_rate: this.state.adminlist ? Rate_details[10].client_rate : "",
          wed_night_employee_rate: this.state.adminlist ? Rate_details[10].employee_rate : "",
          thu_night_from: this.state.adminlist ? Rate_details[11].from : "",
          thu_night_to: this.state.adminlist ? Rate_details[11].to : "",
          thu_night_client_rate: this.state.adminlist ? Rate_details[11].client_rate : "",
          thu_night_employee_rate: this.state.adminlist ? Rate_details[11].employee_rate : "",
          fri_night_from: this.state.adminlist ? Rate_details[12].from : "",
          fri_night_to: this.state.adminlist ? Rate_details[12].to : "",
          fri_night_client_rate: this.state.adminlist ? Rate_details[12].client_rate : "",
          fri_night_employee_rate: this.state.adminlist ? Rate_details[12].employee_rate : "",
          sat_night_from: this.state.adminlist ? Rate_details[13].from : "",
          sat_night_to: this.state.adminlist ? Rate_details[13].to : "",
          sat_night_client_rate: this.state.adminlist ? Rate_details[13].client_rate : "",
          sat_night_employee_rate: this.state.adminlist ? Rate_details[13].employee_rate : "",
          sun_night_from: this.state.adminlist ? Rate_details[14].from : "",
          sun_night_to: this.state.adminlist ? Rate_details[14].to : "",
          sun_night_client_rate: this.state.adminlist ? Rate_details[14].client_rate : "",
          sun_night_employee_rate: this.state.adminlist ? Rate_details[14].employee_rate : "",
          public_night_from: this.state.adminlist ? Rate_details[15].from : "",
          public_night_to: this.state.adminlist ? Rate_details[15].to : "",
          public_night_client_rate: this.state.adminlist ? Rate_details[15].client_rate : "",
          public_night_employee_rate: this.state.adminlist ? Rate_details[15].employee_rate : "",
          late_fee_amount: this.state.adminlist ? this.state.adminlist.late_fee.amount : "",
          late_fee_duration: this.state.adminlist ? this.state.adminlist.late_fee.duration : "",
          status: this.state.adminlist.status
        });
      }
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        {/* <ToastContainer position="top-right" autoClose={2500} />*/}
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                View Job Rates
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                <CardBody className="jobs-clients">
                  
                  <Row>
				  <Col xs="12" md="12">
				   <div className="cus-design edited-section">
				    <Col xs="12" md="12">
				    <p className="h5">
                    Job Details <small>(Shift/Rate Per Hour)</small>
                    </p>
				   </Col>
                   <Col xs="12" md="6">
                      <b>Job Role / Grade</b> : {this.state.name}
                    </Col>
                  
				  <Col xs="12" md="6">
                    <strong className="late_fee">Late Booking Fee</strong>
                   <Row>
                    <Col xs="12" md="6">
                      <b>Amount </b> : £ {this.state.late_fee_amount}
                    </Col>
                    <Col xs="12" md="6">
                      <b>Duration </b> : {this.state.late_fee_duration / 60} Minutes
                    </Col>
					</Row>
				  </Col>
				  </div>
				  </Col>
                  </Row>
				  
                  <br />
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "1" })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Day Time
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "2" })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Night Time
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <Table className="table-bordered">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Rate</th>
                            {/*<th>Employee Rate</th>*/}
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">Monday</th>
                            <td>{this.state.mon_day_from}</td>
                            <td>{this.state.mon_day_to}</td>
                            <td>£ {this.state.mon_day_client_rate} </td>
                            {/*<td>£ {this.state.mon_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Tuesday</th>
                            <td>{this.state.tue_day_from}</td>
                            <td>{this.state.tue_day_to}</td>
                            <td>£ {this.state.tue_day_client_rate} </td>
                            {/*<td>£ {this.state.tue_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Wednesday</th>
                            <td>{this.state.wed_day_from}</td>
                            <td>{this.state.wed_day_to}</td>
                            <td>£ {this.state.wed_day_client_rate} </td>
                            {/*<td>£ {this.state.wed_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Thursday</th>
                            <td>{this.state.thu_day_from}</td>
                            <td>{this.state.thu_day_to}</td>
                            <td>£ {this.state.thu_day_client_rate} </td>
                            {/*<td>£ {this.state.thu_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Friday</th>
                            <td>{this.state.fri_day_from}</td>
                            <td>{this.state.fri_day_to}</td>
                            <td>£ {this.state.fri_day_client_rate} </td>
                            {/*<td>£ {this.state.fri_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Saturday</th>
                            <td>{this.state.sat_day_from}</td>
                            <td>{this.state.sat_day_to}</td>
                            <td>£ {this.state.sat_day_client_rate} </td>
                            {/*<td>£ {this.state.sat_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Sunday</th>
                            <td>{this.state.sun_day_from}</td>
                            <td>{this.state.sun_day_to}</td>
                            <td>£ {this.state.sun_day_client_rate} </td>
                            {/*<td>£ {this.state.sun_day_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Public Holidays</th>
                            <td>{this.state.public_day_from}</td>
                            <td>{this.state.public_day_to}</td>
                            <td>£ {this.state.public_day_client_rate} </td>
                            {/*<td>£ {this.state.public_day_employee_rate} </td>*/}
                          </tr>
                        </tbody>
                      </Table>
                    </TabPane>
                    <TabPane tabId="2">
                      <Table className="table-bordered">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Rate</th>
                            {/*<th>Employee Rate</th>*/}
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">Monday</th>
                            <td>{this.state.mon_night_from}</td>
                            <td>{this.state.mon_night_to}</td>
                            <td>£ {this.state.mon_night_client_rate} </td>
                            {/*<td>£ {this.state.mon_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Tuesday</th>
                            <td>{this.state.tue_night_from}</td>
                            <td>{this.state.tue_night_to}</td>
                            <td>£ {this.state.tue_night_client_rate} </td>
                            {/*<td>£ {this.state.tue_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Wednesday</th>
                            <td>{this.state.wed_night_from}</td>
                            <td>{this.state.wed_night_to}</td>
                            <td>£ {this.state.wed_night_client_rate} </td>
                            {/*<td>£ {this.state.wed_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Thursday</th>
                            <td>{this.state.thu_night_from}</td>
                            <td>{this.state.thu_night_to}</td>
                            <td>£ {this.state.thu_night_client_rate} </td>
                            {/*<td>£ {this.state.thu_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Friday</th>
                            <td>{this.state.fri_night_from}</td>
                            <td>{this.state.fri_night_to}</td>
                            <td>£ {this.state.fri_night_client_rate} </td>
                            {/*<td>£ {this.state.fri_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Saturday</th>
                            <td>{this.state.sat_night_from}</td>
                            <td>{this.state.sat_night_to}</td>
                            <td>£ {this.state.sat_night_client_rate} </td>
                            {/*<td>£ {this.state.sat_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Sunday</th>
                            <td>{this.state.sun_night_from}</td>
                            <td>{this.state.sun_night_to}</td>
                            <td>£ {this.state.sun_night_client_rate} </td>
                            {/*<td>£ {this.state.sun_night_employee_rate} </td>*/}
                          </tr>
                          <tr>
                            <th scope="row">Public Holidays</th>
                            <td>{this.state.public_night_from}</td>
                            <td>{this.state.public_night_to}</td>
                            <td>£ {this.state.public_night_client_rate} </td>
                            {/*<td>£ {this.state.public_night_employee_rate} </td>*/}
                          </tr>
                        </tbody>
                      </Table>
                    </TabPane>
                  </TabContent>
                </CardBody>
                <CardFooter>
                  <Button
                    type="button"
                    color="secondary mb-3 mr-5"
                    title="Back To List"
                    onClick={() => {
                      return this.props.history.push({
                        pathname: "/client/jobrates",
                        state: { rowid: this.props.location.state.clientID, activetabs: "5" }
                      });
                    }}
                  >
                    <i className="fa fa-arrow-left"></i>  Back To List
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editjobtype;
