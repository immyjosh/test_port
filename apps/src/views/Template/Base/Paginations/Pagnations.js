/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, {Component} from 'react';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Pagination, PaginationItem, PaginationLink
} from 'reactstrap';

class Paginations extends Component {

  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i><strong>Pagination</strong>
            <div className="card-actions">
              <a href="https://reactstrap.github.io/components/pagination/">
                <small className="text-muted">docs</small>
              </a>
            </div>
          </CardHeader>
          <CardBody>
            <Pagination>
              <PaginationItem>
                <PaginationLink previous  />
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  1
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  2
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  4
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  5
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink next  />
              </PaginationItem>
            </Pagination>
          </CardBody>
        </Card>
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i><strong>Pagination</strong><small> disabled and active states</small>
          </CardHeader>
          <CardBody>
            <Pagination>
              <PaginationItem disabled>
                <PaginationLink previous  />
              </PaginationItem>
              <PaginationItem active>
                <PaginationLink >
                  1
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  2
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  4
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  5
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink next  />
              </PaginationItem>
            </Pagination>
          </CardBody>
        </Card>
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i><strong>Pagination</strong><small> sizing</small>
          </CardHeader>
          <CardBody>
            <Pagination size="lg">
              <PaginationItem>
                <PaginationLink previous  />
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  1
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  2
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem className="d-none d-sm-block">
                <PaginationLink next  />
              </PaginationItem>
            </Pagination>
            <Pagination>
              <PaginationItem>
                <PaginationLink previous  />
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  1
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  2
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink next  />
              </PaginationItem>
            </Pagination>
            <Pagination size="sm">
              <PaginationItem>
                <PaginationLink previous  />
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  1
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  2
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink >
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink next  />
              </PaginationItem>
            </Pagination>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Paginations;