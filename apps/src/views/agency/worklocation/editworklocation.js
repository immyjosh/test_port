/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Input } from "reactstrap";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import Worklocationmap from "./worklocationmap";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";

class Editworklocation extends Component {
  state = {
    name: "",
    zipcode: "",
    formatted_address: "",
    lat: "",
    lng: "",
    status: "",
    code: "",
    address: "",
    phoneerror: false,
    phonestatus: "",
    colorerror: false,
    stausactive: false,
    worklist: "",
    radiusinmiles: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/agency/location/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ worklist: res.response.result[0] });
      this.setState({
        name: this.state.worklist.name,
        status: this.state.worklist.status,
        // number: this.state.worklist.phone.number,
        formatted_address: this.state.worklist.address,
        // color: this.state.worklist.color,
        radius: this.state.worklist.radius,
        radiusinmiles: this.state.worklist.radius * 0.000621371192 * 999999,
        lat: this.state.worklist.geolocation.lat,
        lng: this.state.worklist.geolocation.lng
      });
      if (this.state.worklist.status === 1) {
        this.setState({
          stausactive: true
        });
      } else {
        this.setState({
          stausactive: false
        });
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  onChangeRadius = e => {
    this.setState({ [e.target.name]: e.target.value });
    let miles = e.target.value * 0.000621371192 * 999999;
    this.setState({ radiusinmiles: miles });
  };
  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            zipcode: results[0].address_components[6].lngg_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            zipcode: results[0].address_components[5].lngg_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            zipcode: results[0].address_components[6].lngg_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            zipcode: results[0].address_components[7].lngg_name
          });
        }
        getLatLng(results[0]).then(latlngg => {
          this.setState({ lat: latlngg.lat });
          this.setState({ lng: latlngg.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };

  OnFormSubmit = (e, values) => {
    request({
      url: "/agency/location/save",
      method: "POST",
      data: {
        _id: this.props.location.state.rowid,
        name: this.state.name,
        geolocation: {
          lat: this.state.lat,
          lng: this.state.lng
        },
        address: this.state.formatted_address,
        radius: this.state.radius,
        status: this.state.status
      }
    }).then(res => {
      if (res.status === 1) {
        this.editnodify();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };

  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
  };

  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      phonestatus: status
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  EditworkloactiontoastId = "Editworkloaction";
  editnodify() {
    if (!toast.isActive(this.EditworkloactiontoastId)) {
      this.EditworkloactiontoastId = toast.success("Updated");
    }
    this.componentDidMount();
  }
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Edit Area
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
                  <Row>
				    <Col xs="12" md="12">
				     <div className="cus-design edited-section">
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Name</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter Name"
                          value={this.state.name}
                          onChange={this.onChange}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Radius</Label>
                        <AvInput
                          type="number"
                          name="radius"
                          placeholder="Enter radius"
                          onChange={this.onChangeRadius}
                          value={this.state.radius}
                          required
                          autoComplete="radius"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>

                    <Col xs="12" md="4">
					 <Label>Search address</Label>
                      <PlacesAutocomplete
                        value={this.state.formatted_address}
                        onChange={this.handleChange}
                        onSelect={this.handleSelect}
                        onFocus={this.geolocate}
                      >
                        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                          <div>
                            <input
                              {...getInputProps({
                                placeholder: "Search Places",
                                className: "form-control"
                              })}
                            />
                            <div className="autocomplete-dropdown-container absolute">
                              {suggestions.map(suggestion => {
                                const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                  ? {
                                      backgroundColor: "#fafafa",
                                      cursor: "pointer"
                                    }
                                  : {
                                      backgroundColor: "#ffffff",
                                      cursor: "pointer"
                                    };
                                return (
                                  <div
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                      style
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        )}
                      </PlacesAutocomplete>
                    </Col>
					</div>
					</Col>
                  </Row>
                  <Worklocationmap radiusData={{ lat: this.state.lat, lng: this.state.lng, radius: this.state.radiusinmiles, address: this.state.address }} />
                  <Row className="mt-3">
                    <Col xs="12" md="4">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                        <span className="switch-label" data-on="active" data-off="inactive" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i className="fa fa-dot-circle-o" />
                    Update
                  </Button>
                  <Link to="/agency/listworklocation">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editworklocation;
