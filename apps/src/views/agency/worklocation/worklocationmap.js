/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
/*Google Google*/
import React, { Component } from "react";
import { Card, CardHeader, CardBody, NavLink, Col, button } from "reactstrap";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Circle } from "react-google-maps";
import { Settings } from "../../../api/key";
import request, { NodeURL } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
const { DrawingManager } = require("react-google-maps/lib/components/drawing/DrawingManager");

const defaultZoom = 6;
const defaultOptions = { gestureHandling: "greedy" };
const defaultCenter = { lat: 53.50853, lng: -0.12574 };

let locations;
class MarkerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      godlist: []
    };
  }

  componentDidMount() {}

  render() {
    let locations = [
      {
        lat: this.props.radiusData ? this.props.radiusData.lat : 51.50853,
        lng: this.props.radiusData ? this.props.radiusData.lng : -0.12574,
        label: "S",
        draggable: true,
        center: { lat: 41.878, lng: -87.629 },
        population: 2714856,
        radius: this.props.radiusData ? this.props.radiusData.radius : 80000,
        title: this.props.radiusData ? this.props.radiusData.address : "Current"
      }
    ];
    return locations.map((locations, index) => {
      return <MarkerWithInfoWindow key={index.toString()} location={locations} />;
    });
  }
}

class MarkerWithInfoWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggle = this.toggle.bind(this);
  }
  componentDidMount() {}
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  onCenterChanged = value => {
    // console.log("onCenterChanged", value);
  };
  onRadiusChanged = value => {
    // console.log("onradiuschange", value);
  };
  render() {
    const { location } = this.props;
    return (
      <Marker onClick={this.toggle} title={location.title} label={location.label}>
        <Circle
          center={location}
          radius={parseInt(location.radius, 0)}
          ref={circle => {
            this.circle = circle;
          }}
          onCenterChanged={e => this.onCenterChanged(e)}
          onRadiusChanged={e => this.onRadiusChanged(e)}
          options={{
            fillColor: "#f00",
            strokeColor: "#f00"
          }}
        />
        {this.state.isOpen && (
          <InfoWindow onCloseClick={this.toggle}>
            <NavLink href={location.www}>{location.title}</NavLink>
          </InfoWindow>
        )}
      </Marker>
    );
  }
}

const GoogleMapsComponent = withScriptjs(
  withGoogleMap(props => {
    let Dcenter;
    if (props.radiusData && props.radiusData.lat && props.radiusData.lng) {
      Dcenter = { lat: props.radiusData.lat, lng: props.radiusData.lng };
    } else {
      Dcenter = defaultCenter;
    }
    return (
      <GoogleMap defaultZoom={defaultZoom} center={Dcenter} defaultOptions={defaultOptions}>
        {<MarkerList locations={locations} radiusData={props.radiusData} />}
      </GoogleMap>
    );
  })
);

class Worklocation extends Component {
  componentDidMount() {}
  // To use the Google Maps JavaScript API, you must register your app project on the
  // Google API Console and get a Google API key which you can add to your app
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <GoogleMapsComponent
          radiusData={this.props.radiusData}
          key="map"
          googleMapURL={Settings.googleMapURL + Settings.googleMapKey}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `620px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
    );
  }
}

export default Worklocation;
