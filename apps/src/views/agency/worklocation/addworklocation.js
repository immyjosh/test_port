/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import savenodify from "./listworklocation";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import Worklocationmap from "./worklocationmap";

class Addworklocation extends Component {
  state = {
    name: "",
    zipcode: "",
    formatted_address: "",
    color: "",
    number: "",
    status: "",
    code: "",
    address: "",
    phoneerror: false,
    phonestatus: "",
    colorerror: false,
    lat: 0,
    lng: 0,
    radius: 0,
    radiusinmiles: ""
  };
  flash = new savenodify();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangeRadius = e => {
    this.setState({ [e.target.name]: e.target.value });
    let miles = e.target.value * 0.000621371192 * 999999;
    this.setState({ radiusinmiles: miles });
  };
  onChangecolor = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    if (e.target.name === "radius") {
      this.setState({ [e.target.name]: e.target.value * 1609.344 });
    }
  };
  handleChange = address => {
    this.setState({ address });
    this.setState({ formatted_address: address });
    if (address === "") {
      this.setState({
        formatted_address: "",
        zipcode: ""
      });
    }
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    const adminform = {
      name: this.state.name,
      // phone: {
      //   code: this.state.code,
      //   number: this.state.number
      // },
      geolocation: {
        lat: this.state.lat,
        lng: this.state.lng
      },
      radius: this.state.radius,
      address: this.state.formatted_address,
      //  color: this.state.color,
      zipcode: this.state.zipcode,
      status: 1
    };
    request({
      url: "/agency/location/save",
      method: "POST",
      data: adminform
    })
      .then(res => {
        if (res.status === 1) {
          this.saveuser();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };

  saveuser = () => {
    return (
      this.form && this.form.reset(),
      this.setState({ number: "", address: "" }),
      this.props.history.push("/agency/listworklocation"),
      this.flash.adminsavenodify()
    );
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            zipcode: results[0].address_components[6].lngg_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            zipcode: results[0].address_components[5].lngg_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            zipcode: results[0].address_components[6].lngg_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            zipcode: results[0].address_components[7].lngg_name
          });
        }
        getLatLng(results[0]).then(latlngg => {
          this.setState({ lat: latlngg.lat });
          this.setState({ lng: latlngg.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      phonestatus: status
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Add Area
                {/* <div className="card-actions">
                  <a>
                    <i className="fa fa-user-plus" />
                    <small className="text-muted" />
                  </a>
                </div> */}
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
				 
                  <Row>
				  <Col xs="12" md="12">
				   <div className="cus-design edited-section">
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Name</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter Name"
                          value={this.state.name}
                          onChange={this.onChange}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Radius</Label>
                        <AvInput
                          type="number"
                          name="radius"
                          placeholder="Enter radius"
                          onChange={this.onChangeRadius}
                          value={this.state.radius}
                          required
                          autoComplete="radius"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="4">
					<Label>Search address</Label>
                      <PlacesAutocomplete value={this.state.address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                          <div>
                            <input
                              {...getInputProps({
                                placeholder: "Search Places",
                                className: "form-control"
                              })}
                            />
                            <div className="autocomplete-dropdown-container absolute">
                              {suggestions.map(suggestion => {
                                const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                  ? {
                                      backgroundColor: "#fafafa",
                                      cursor: "pointer"
                                    }
                                  : {
                                      backgroundColor: "#ffffff",
                                      cursor: "pointer"
                                    };
                                return (
                                  <div
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                      style
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        )}
                      </PlacesAutocomplete>
                    </Col>
					</div>
					</Col>
                  </Row>
				  
                  <Worklocationmap radiusData={{ lat: this.state.lat, lng: this.state.lng, radius: this.state.radiusinmiles, address: this.state.address }} />
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    title="Add Area"
                    onClick={() => {
                      this.phonefield(this.state.number);
                    }}
                  >
                    Add 
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addworklocation;
