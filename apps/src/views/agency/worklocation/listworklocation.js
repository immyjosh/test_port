import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import { Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip, Row, Col, Badge,NavLink } from "reactstrap";
import Pagination from "react-js-pagination";
import fileDownload from "js-file-download";
import { DateRangePicker } from "react-dates";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Circle } from "react-google-maps";
import { Settings } from "../../../api/key";
import request from "../../../api/api";
import MapList from "../../common/maplist";
// const { DrawingManager } = require("react-google-maps/lib/components/drawing/DrawingManager");

const defaultZoom = 5;
const defaultOptions = { gestureHandling: "greedy" };
const defaultCenter = { lat: 53.50853, lng: -0.12574 };

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "140px"
};
let locations;
class MarkerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      godlist: []
    };
  }

  componentDidMount() {}

  render() {
    let locations = [
      {
        lat: this.props.radiusData ? this.props.radiusData.lat : 51.50853,
        lng: this.props.radiusData ? this.props.radiusData.lng : -0.12574,
        label: "S",
        draggable: true,
        center: { lat: 41.878, lng: -87.629 },
        population: 2714856,
        radius: this.props.radiusData ? this.props.radiusData.radius : 80000,
        title: this.props.radiusData ? this.props.radiusData.address : "Current"
      }
    ];
    if(this.props && this.props.mapListDetails && this.props.mapListDetails.length>0){
       locations = this.props.mapListDetails.map(list=>({
        lat: list.lat ? list.lat : 51.50853,
        lng: list.lng ? list.lng : -0.12574,
        label: "S",
        draggable: false,
        center: { lat: 41.878, lng: -87.629 },
        population: 2714856,
        radius: list.radius ? list.radius * 0.000621371192 * 999999 : 80000,
       }))
    }

    return locations.map((locations, index) => {
      return <MarkerWithInfoWindow key={index.toString()} location={locations} />;
    });
  }
}

class MarkerWithInfoWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggle = this.toggle.bind(this);
  }
  componentDidMount() {}
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  onCenterChanged = value => {
    // console.log("onCenterChanged", value);
  };
  onRadiusChanged = value => {
    // console.log("onradiuschange", value);
  };
  render() {
    const { location } = this.props;
    return (
      <Marker onClick={this.toggle} title={location.title} label={location.label}>
        <Circle
          center={location}
          radius={parseInt(location.radius, 0)}
          ref={circle => {
            this.circle = circle;
          }}
          onCenterChanged={e => this.onCenterChanged(e)}
          onRadiusChanged={e => this.onRadiusChanged(e)}
          options={{
            fillColor: "#f00",
            strokeColor: "#f00"
          }}
        />
        {this.state.isOpen && (
          <InfoWindow onCloseClick={this.toggle}>
            <NavLink href={location.www}>{location.title}</NavLink>
          </InfoWindow>
        )}
      </Marker>
    );
  }
}

const GoogleMapsComponent = withScriptjs(
  withGoogleMap(props => {
    let Dcenter;
    if (props.radiusData && props.radiusData.lat && props.radiusData.lng) {
      Dcenter = { lat: props.radiusData.lat, lng: props.radiusData.lng };
    } else {
      Dcenter = defaultCenter;
    }
    return (
      <GoogleMap defaultZoom={defaultZoom} center={Dcenter} defaultOptions={defaultOptions}>
        {<MarkerList locations={locations} mapListDetails={props.mapListDetails} />}
      </GoogleMap>
    );
  })
);

class Listworklocation extends Component {
  state = {
    adminredirect: false,
    sortOrder: true,
    isLoader: false,
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0
    },
    worklist: []
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    request({
      url: "/agency/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res) {
        this.setState({
          isLoader: false,
          worklist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/editworklocation",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/agency/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (!res.response.result && !res.response.fullcount) {
        this.setState({ worklist: [], pages: "" });
      } else {
        this.setState({
          worklist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["name", "address", "radius", "status"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Address") {
        state.tableOptions.filter = "address";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  ListworkloactiontoastId = "Listworkloaction";
  nodify() {
    if (!toast.isActive(this.ListworkloactiontoastId)) {
      this.ListworkloactiontoastId = toast.warn("Updated");
    }
  }
  ListworknodifytoastId = "Listworkloaction";
  adminsavenodify() {
    if (!toast.isActive(this.ListworknodifytoastId)) {
      this.ListworknodifytoastId = toast.success("New Work Location Added");
    }
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d || "";
        state.tableOptions.to_date = this.state.end_date._d || "";
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  export() {
    request({
      url: "/agency/location/locationexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }

    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            All Areas
            <div className="card-actions" style={tStyle} >
              <button style={tStyles} onClick={()=>this.props.history.push("/agency/addworklocation")}>
                <i className="fa fa-plus" /> Add New
              </button>
              <button style={tStyles} onClick={this.export}>
                <i className="fa fa-upload" /> Export
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <Row>
              <Col xs="12" md="12">
                <div className="row">
                  <div className="col-lg-7">
                    <DateRangePicker
                      showClearDates={true}
                      startDate={this.state.start_date}
                      startDateId="start_date"
                      endDate={this.state.end_date}
                      endDateId="end_date"
                      onDatesChange={({ startDate, endDate }) => {
                        if (startDate ===null && endDate=== null ) {
                          this.setState(state => {
                            state.tableOptions.from_date = "";
                            state.tableOptions.to_date = "";
                          });
                          this.populateData();
                        }
                        this.setState({
                          start_date: startDate,
                          end_date: endDate
                        });
                      }}
                      isOutsideRange={() => false}
                      focusedInput={this.state.focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      displayFormat="DD-MM-YYYY"
                    />
                    <Button
                      size="md"
                      color="primary"
                      onClick={() => {
                        this.fromTo();
                      }}
                    >
                      <i className="fa fa-search" />
                    </Button>
                  </div>
                  <div className="col-lg-5 pr-0">
                    <InputGroup>
                      <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-3">
                        <option>All</option>
                        <option>Name</option>
                        <option>Address</option>
                      </Input>
                      <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0 col-lg-7" />
                      <Button
                        className="rounded-0"
                        color="primary"
                        id="clear"
                        onClick={() => {
                          ReactDOM.findDOMNode(this.refs.search).value = "";
                          this.search("");
                        }}
                      >
                        <i className="fa fa-remove" />
                      </Button>
                      <UncontrolledTooltip placement="top" target="clear">
                        Clear
                      </UncontrolledTooltip>
                    </InputGroup>
                  </div>
                </div>

                <div className="table-responsive mt-2">
                  <Table hover bordered responsive size="sm">
                    <thead>
                      <tr>
                        <th>S.No.</th>
                        <th
                          onClick={() => {
                            this.sort("name");
                          }}
                        >
                          Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("address");
                          }}
                        >
                          Address <i style={{ paddingLeft: "25px" }} className={sorticondef} id="address" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("radius");
                          }}
                        >
                          Radius
                          <i style={{ paddingLeft: "25px" }} className={sorticondef} id="radius" />
                        </th>
                        <th>
                          Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                        </th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.worklist.length > 0 ? (
                        this.state.worklist.map((item, i) => (
                          <tr key={item._id} onClick={id => this.editpage(item._id)}>
                            <td>{this.state.tableOptions.skip + i + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.address}</td>
                            <td>{item.radius}</td>
                            <td>
                              {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                              {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                            </td>
                            <td>
                              <button type="button" title="Edit" className="btn table-edit" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                                <i className="fa fa-edit" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                Edit
                              </UncontrolledTooltip>
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr className="text-center">
                          <td colSpan={13}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                        </tr>
                      )}
                      {this.state.isLoader && <Loader />}
                    </tbody>
                  </Table>
                </div>
                <nav className="float-left">
                  <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                  </Input>
                </nav>
                <nav className="float-right">
                  <div>
                    <Pagination
                      prevPageText="Prev"
                      nextPageText="Next"
                      firstPageText="First"
                      lastPageText="Last"
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.tableOptions.limit}
                      totalItemsCount={this.state.pages}
                      pageRangeDisplayed={this.state.pageRangeDisplayed}
                      onChange={this.paginate}
                    />
                  </div>
                </nav>
              </Col>
              <Col xs="12" md="12" className="map_min">
                {this.state.worklist && this.state.worklist.length > 0 ? (
                  <Fragment>
                    {/* <GoogleMapsComponent
                    mapListDetails={this.state.worklist}
                    key="map"
                    googleMapURL={Settings.googleMapURL + Settings.googleMapKey}
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `100%` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                  /> */}
                  <GoogleMapsComponent
          // radiusData={this.props.radiusData}
          mapListDetails={this.state.worklist}
          key="map"
          googleMapURL={Settings.googleMapURL + Settings.googleMapKey}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `620px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
                  </Fragment>
                ) : (
                  <MapList mapListDetails={{ locationDetails: this.state.worklist, Page: "worklist" }} />
                )}
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Listworklocation;
