/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { Container, Row, Col, CardGroup, Card, CardBody, Button, Alert, InputGroupAddon, InputGroupText } from "reactstrap";
import { AvForm, AvGroup, AvInput, AvFeedback } from "availity-reactstrap-validation";
import loginnodify from "../agencydashboard";
import loginnodifyNoSub from "../subscribe/subscribe";
import { ToastContainer, toast } from "react-toastify";
import jwt_decode from "jwt-decode";
import request, { client } from "../../../api/api";
import Gettoken from "../../../api/gettoken";
import Context from "../../../api/context";
import { Link } from "react-router-dom";

import SocialLogin from "./sociallogin"; //google

class Agencylogin extends Component {
  state = {
    email: "",
    password: "",
    emaillists: [],
    invalidemail: false,
    error: false
  };
  flash = new loginnodify();
  flash1 = new loginnodifyNoSub();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidMount() {
    if (this.props.location.state === true) {
      return toast.success("Registration Completed Successfully!  Please Check Your Mail"), this.props.history.push({ state: false });
    }
    const token = this.props.theme.AuthtokenAgency;
    if (token.role) {
      switch (token.role) {
        case "agency":
          if (token.subscription === 1) {
            this.props.history.replace("/agency/dashboard");
          } else if (token.subscription === 0) {
            this.props.history.replace("/agency/subscribe");
          }
          break;
        default:
          this.props.history.replace("/");
      }
    }
    if (this.props.location.state === "notify") {
      toast.info("Loged Out!");
      this.props.history.push({ state: "" });
    }
  }

  agencynodifytoastId = "AgencyLogin";

  agencynodify() {
    if (!toast.isActive(this.agencynodifytoastId)) {
      this.agencynodifytoastId = toast.success("Registration Completed Successfully!  Please Check Your Mail");
    }
  }

  OnFormSubmit = e => {
    const loginform = {
      email: this.state.email,
      password: this.state.password
    };
    request({
      url: "/agencylogin",
      method: "POST",
      data: loginform
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUSA", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          let tokenAgency;
          if (res.response.auth_token) {
            tokenAgency = jwt_decode(res.response.auth_token);
          }
          if (tokenAgency.subscription === 1) {
            this.loginnodify();
          } else if (tokenAgency.subscription === 0) {
            this.loginnodifyNoSubscription();
          }
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };

  loginnodify() {
    return this.props.history.push({ pathname: "/agency/dashboard", state: true });
    // return this.props.history.push("/agency/dashboard"), this.flash.nodify();
  }

  loginnodifyNoSubscription() {
    return this.props.history.replace("/agency/subscribe"), this.flash1.nodifynosub();
  }

  deactivatenodifytoastId = "AgencyLogin";

  deactivatenodify() {
    if (!toast.isActive(this.deactivatenodifytoastId)) {
      this.deactivatenodifytoastId = toast.error("Deactivated!");
    }
  }

  passwordnodifytoastId = "AgencyLogin";

  passwordnodify() {
    if (!toast.isActive(this.passwordnodifytoastId)) {
      this.passwordnodifytoastId = toast.info("Password Changed!");
    }
  }

  logoutmsgtoastId = "AgencyLogin";
  logoutmsg = () => {
    if (!toast.isActive(this.logoutmsgtoastId)) {
      this.logoutmsgtoastId = toast.info("Loged Out!");
    }
  };

  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <CardBody>
                      <h1 className="login-infos">Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput name="email" label="Email" placeholder="Enter email or username" onChange={this.onChange} value={this.state.email} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="password" name="password" placeholder="Enter password" onChange={this.onChange} value={this.state.password} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button type={"submit"} color="primary" className="px-4">
                            Login
                          </Button>
                        </Col>
                        {this.state.error ? <Alert color="danger">Password Incorrect.Enter Correct Password!</Alert> : null}
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">
                            <Link className="for-pass" to="/agency/forgotpassword">Forgot password?</Link>
                          </Button>
                        </Col>
                      </Row>
                    </CardBody>
                  </AvForm>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none new-social-details" style={{ width: 44 + "%" }}>
                  <CardBody className="text-center">
                    <div>
                      {/* <h2>Sign up</h2> */}
                      {/* <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                        aliqua.
                      </p> */}
                    </div>
                    <SocialLogin />
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default props => (
  <Gettoken>
    <Context.Consumer>{theme => <Agencylogin {...props} theme={theme} />}</Context.Consumer>
  </Gettoken>
);
