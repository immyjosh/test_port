/*eslint no-sequences: 0*/
import React, { Component } from "react";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import savenodify from "./agencylogin";
import { toast, ToastContainer } from "react-toastify";
import { Container, Row, Col, Card, CardBody, CardFooter, Button, InputGroup, Label } from "reactstrap";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import request from "../../../api/api";
import IntlTelInput from "react-intl-tel-input";
import { withRouter } from "react-router-dom";
import ReCAPTCHA from "react-google-recaptcha";
import { Settings } from "../../../api/key";
import Avatar from "react-avatar-edit";

class AgencyRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.history.location !== undefined || this.props.history.location.state !== undefined ? this.props.history.location.state.name : "",
      email: this.props.history.location !== undefined || this.props.history.location.state !== undefined ? this.props.history.location.state.email : "",
      password: "",
      confirm_password: "",
      number: "",
      username: "",
      code: "",
      address: "",
      line1: "",
      line2: "",
      city: "",
      state: "",
      dailcountry: "",
      country: "",
      zipcode: "",
      formatted_address: "",
      phoneerror: false,
      avatar: "",
      status: 1,
      isverified: 0,
      avatarfile: "",
      recapcha: ""
    };
  }
  flash1 = new savenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2
    });
  };

  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: ""
      });
    }
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  onChangeCapcha = e => {
    this.setState({ recapcha: e && e });
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarfile: URL.createObjectURL(evt.target.files[0]),
      avatarName: evt.target.files[0].name
    });
  };
  onClose = () => {
    this.setState({ propreview: null });
  };

  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  OnFormSubmit = (e, values) => {
    if (this.state.number === "" || this.state.avatar === "") {
      toast.error("Please Enter Required Fields");
    } else {
      if (this.state.recapcha === "") {
        toast.error("Capcha Is Required");
      } else {
        const data = new FormData();
        data.append("username", this.state.username);
        data.append("password", this.state.password);
        data.append("confirm_password", this.state.confirm_password);
        data.append("name", this.state.name);
        data.append("surname", this.state.surname);
        data.append("email", this.state.email);
        data.append("avatar", this.state.avatar);
        data.append("phone[code]", this.state.code);
        data.append("phone[number]", this.state.number);
        data.append("phone[dailcountry]", this.state.dailcountry);
        data.append("address[line1]", this.state.line1);
        data.append("address[line2]", this.state.line2);
        data.append("address[city]", this.state.city);
        data.append("address[state]", this.state.state);
        data.append("address[country]", this.state.country);
        data.append("address[zipcode]", this.state.zipcode);
        data.append("address[formatted_address]", this.state.formatted_address);
        data.append("address[lat]", this.state.lat);
        data.append("address[lon]", this.state.lon);
        data.append("status", this.state.status);
        data.append("isverified", this.state.isverified);
        request({
          url: "/register/agency",
          method: "POST",
          data: data
        })
          .then(res => {
            if (res.status === 1) {
              this.saveuser();
            } else if (res.status === 0) {
              toast.error(res.response);
            }
          })
          .catch(err => {
            toast.error("error");
          });
      }
    }
  };
  saveuser() {
    return this.form && this.form.reset(), this.setState({ number: "", address: "" }), this.props.history.push({ pathname: "/agency", state: true });
  }
  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <AvForm onValidSubmit={this.OnFormSubmit}>
            <Row className="justify-content-center">
              <Col md="5">
                <Card className="register-info">
                  <CardBody className="register-sections">
                    <div className="login-register">
                      <h1>Register</h1>
                    </div>
                    <div className="reg-app">
                      <Col xs="12" md="12">
                        <h4>Personel Details</h4>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-user" />
                            </span>
                          </div>
                          <AvInput type="text" name="name" placeholder="Name" onChange={this.onChange} value={this.state.name} required />
                        </InputGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-user" />
                            </span>
                          </div>
                          <AvInput type="text" name="surname" placeholder="Surname" onChange={this.onChange} value={this.state.surname} required />
                        </InputGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-phone" />
                            </span>
                          </div>
                          <IntlTelInput defaultCountry={"gb"} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler} value={this.state.number} />
                          {this.state.phoneerror ? <div style={{ color: "red" }}>Enter Valid Phone Number!</div> : null}
                        </InputGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <Row>
                          <Col md="6">
                            <Label for="exampleCustomFileBrowser">Profile Image</Label>
                            {/* <CustomInput type="file" id="exampleCustomFileBrowser" name="avatar" onChange={this.fileChangedHandler} label={this.state.avatarName ? this.state.avatarName : "Upload  Image"} encType="multipart/form-data" />*/}
                            <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                          </Col>
                          {this.state.propreview ? (
                            <Col md="6">
                              <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                            </Col>
                          ) : null}
                          {this.state.avatarfile ? <img className="prof-image img-fluid" width="120px" height="100px" src={this.state.avatarfile} alt="Preview" /> : null}
                        </Row>
                      </Col>
                      <Col xs="12" md="12">
                        <h4 for="exampleCustomFileBrowser">Search address</h4>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-map" />
                            </span>
                          </div>
                          <PlacesAutocomplete value={this.state.address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                            {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                              <div className="ser-page">
                                <input
                                  {...getInputProps({
                                    placeholder: "Search Your Location",
                                    className: "form-control"
                                  })}
                                />
                                <div className="autocomplete-dropdown-container absolute my_styles">
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                          backgroundColor: "#fafafa",
                                          cursor: "pointer"
                                        }
                                      : {
                                          backgroundColor: "#ffffff",
                                          cursor: "pointer"
                                        };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                          </PlacesAutocomplete>
                        </InputGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
						
                          <div className="input-group-prepend ">
                            <span className="input-group-text">
                              <i className="icon-location-pin" />
                            </span>
                          </div>
                          <AvInput type="text" name="line1" placeholder="Line 1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                           
                          
                        </InputGroup>
                      </Col>
					  
					  <Col xs="12" md="12">
                        <InputGroup className="mb-3">
						
                         
                          <div className="input-group-prepend ">
                            <span className="input-group-text">
                              <i className="icon-location-pin" />
                            </span>
                          </div>

                          <AvInput type="text" name="line2" placeholder="line2" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
						 
                        </InputGroup>
                      </Col>
					  
					  <Col xs="12" md="12">
                        <InputGroup className="mb-3">
						
                          <div className="input-group-prepend ">
                            <span className="input-group-text">
                              <i className="icon-location-pin" />
                            </span>
                          </div>

                          <AvInput type="text" name="line2" placeholder="City/Town" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
						 
                        </InputGroup>
                      </Col>
					  
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          {/* <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-location-pin" />
                        </span>
                      </div>
                      <AvInput
                        type="text"
                        name="city"
                        placeholder="City"
                        onChange={this.onChange}
                        value={this.state.city}
                        required
                        autoComplete="name"
                      /> */}
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-location-pin" />
                            </span>
                          </div>
                          <AvInput type="text" name="state" placeholder="State/Region" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                        </InputGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-location-pin" />
                            </span>
                          </div>
                          <AvInput  type="text" name="country" placeholder="Country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                         
                        </InputGroup>
                      </Col>
					  
					   <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend ">
                            <span className="input-group-text">
                              <i className="icon-location-pin" />
                            </span>
                          </div>
                          <AvInput type="text" name="zipcode" placeholder="Postal Code" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                        </InputGroup>
                      </Col>
					  
                      <Col xs="12" md="12">
                        <h4 for="exampleCustomFileBrowser">Login Details</h4>
                      </Col>
                      <Col xs="12" md="12">
                        <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-user-following" />
                            </span>
                          </div>
                          <AvInput type="text" name="username" placeholder="Username" onChange={this.onChange} value={this.state.username} required />
                        </InputGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <AvGroup className="input-group no-mar">
                          <InputGroup className="mb-3">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <i className="icon-envelope" />{" "}
                              </span>
                            </div>
                            <AvInput type="email" name="email" placeholder="Email Address" onChange={this.onChange} value={this.state.email} required /> <AvFeedback>Enter Valid Email!</AvFeedback>
                          </InputGroup>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <AvGroup className="input-group no-mar">
                          <InputGroup className="mb-3">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <i className="icon-lock" />
                              </span>
                            </div>
                            <AvInput type="password" name="password" placeholder="Password" onChange={this.onChange} value={this.state.password} required autoComplete="off" minLength="6" pattern="(?=.*[A-Z])(?=.*\d)" />
                            <AvFeedback>
                              <small>Password is Required. Password Must Contain At least One uppercase,One lower case,One Numeric digit. Password must be minimum of 6 characters.</small>
                            </AvFeedback>
                          </InputGroup>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <AvGroup className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-lock" />
                            </span>
                          </div>
                          <AvInput
                            type="password"
                            name="confirm_password"
                            placeholder="Re-Enter Password"
                            onChange={this.onChange}
                            value={this.state.confirm_password}
                            required
                            validate={{ match: { value: "password" } }}
                            autoComplete="off"
                          />
                          <AvFeedback>Password Doesn't match!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <div className="d-flex justify-content-center">
                        <ReCAPTCHA sitekey={Settings.googleCapcha} onChange={this.onChangeCapcha} />
                      </div>
                    </div>
                  </CardBody>
                  <CardFooter className="p-4">
                    <Button
                      type="submit"
                      block
                      color="success pull-right mb-3"
                      title="Ok"
                      className="btn-add"
                      onClick={() => {
                        this.phonefield(this.state.number);
                      }}
                    >
                      Register
                    </Button>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </AvForm>
        </Container>
      </div>
    );
  }
}

export default withRouter(AgencyRegister);
