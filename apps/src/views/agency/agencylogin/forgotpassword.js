import React, { Component } from "react";
import { Button, Card, CardBody, CardGroup, Col, Container, InputGroupAddon, InputGroupText, Row, Alert } from "reactstrap";
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import { toast, ToastContainer } from "react-toastify";
import request from "../../../api/api";

class agencyforgotpassword extends Component {
  state = {
    email: "",
    mailalert: false,
    mailerror: false,
    nomail: false
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  OnFormSubmit = e => {
    request({
      url: "/agency/forgotpassword",
      method: "POST",
      data: { email: this.state.email }
    })
      .then(res => {
        console.log(res);
        if (res.status === "1") {
          this.setState({
            mailalert: true
          });
        } else if (res.status === "0") {
          this.setState({
            mailerror: true
          });
        }
        setTimeout(() => {
          this.setState({
            mailalert: false
          });
        }, 3500);
        setTimeout(() => {
          this.setState({
            mailerror: false
          });
        }, 3500);
      })
      .catch(err => {
        this.setState({
          nomail: true
        });
        setTimeout(() => {
          this.setState({
            nomail: false
          });
        }, 3500);
      });
  };

  componentDidMount() {
    if (localStorage.getItem("notify")) {
      this.logoutmsg();
      localStorage.clear();
    }
  }
  logerrorstoastId = "Forgotpasswords";
  logerror = () => {
    if (!toast.isActive(this.logerrorstoastId)) {
      this.logerrorstoastId = toast.error("No User Found!");
    }
  };

  render() {
    return (
      <div className="app flex-row align-items-center fullimage login-valid">
        <ToastContainer position="top-right" autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="ag-lo">
                <Card className="p-4">
                  <AvForm onValidSubmit={this.OnFormSubmit}>
                    <CardBody>
                      <h1 className="login-infos">Forgot Password</h1>
                      <p className="text-muted">Enter your Email</p>
                      <AvGroup className="input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <AvInput type="text" name="email" placeholder="Enter email" onChange={this.onChange} value={this.state.email} required />
                        <AvFeedback>This is invalid!</AvFeedback>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">
                            Send
                          </Button>
                        </Col>
                      </Row>
                      <br />
                      <Row>
                        {this.state.mailalert ? <Alert color="info">Check your email , Follow the instructions and Reset Password!</Alert> : null}
                        {this.state.mailerror ? <Alert color="danger">Please Enter Valid Email Address!</Alert> : null}
                        {this.state.nomail ? <Alert color="danger">Please Enter Valid Email Address!</Alert> : null}
                      </Row>
                    </CardBody>
                  </AvForm>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + "%" }}>
                  <CardBody className="text-center">
                    <div className="for-text">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      <Button
                        type="button"
                        color="primary"
                        className="mt-3"
                        active
                        onClick={() => {
                          this.props.history.push("/agency");
                        }}
                      >
                        Back to Login
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default agencyforgotpassword;
