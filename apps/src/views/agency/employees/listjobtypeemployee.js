/*eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { withRouter } from "react-router-dom";
import { Table, UncontrolledTooltip, Badge } from "reactstrap";

class Listjobtype extends Component {
  state = {
    subscribedlist: []
  };
  componentDidMount() {
    this.setState({ subscribedlist: this.props && this.props.joblistdata ? this.props.joblistdata : [] });
  }
    static getDerivedStateFromProps(nextProps, prevState) {
       return ({ subscribedlist: nextProps && nextProps.joblistdata ? nextProps.joblistdata : [] });
    }
  AddJobtype = value => {
    this.props.AddJobtype(value);
  };
  RemoveJobtype = value => {
    this.props.RemoveJobtype(value);
  };
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/editjobtypeemployee",
      state: { rowid: id, employeeid: this.props.employeeid }
    });
  };
  render() {
    return (
      <Fragment>
        <Fragment>
          {this.state.subscribedlist && this.state.subscribedlist.length > 0 ? (
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>Job Role</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.subscribedlist && this.state.subscribedlist.length > 0 ? (
                    this.state.subscribedlist.map((item, i) => (
                      <tr key={item._id}>
                        <td>{item.name}</td>
                        <td>{item.job_id ? <Badge color="success">Active</Badge> : <Badge color="danger">In-active</Badge>}</td>
                        <td>
                          <Fragment>
                            {item.job_id ? (
                              <>
                                <button type="button" title="Edit" className="btn btn-danger btn-sm" id={`edit52s99`} onClick={id => this.RemoveJobtype(item._id)}>
                                  <i className="fa fa-remove" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`edit52s99`}>
                                  Remove from Employee
                                </UncontrolledTooltip>
                                <button type="button" title="Edit" className="btn btn-primary btn-sm ml-2" id={`edit875`} onClick={id => this.editpage(item._id)}>
                                  <i className="fa fa-edit" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`edit875`}>
                                  Edit
                                </UncontrolledTooltip>
                              </>
                            ) : (
                              <Fragment>
                                <button type="button" title="Edit" className="btn btn-primary btn-sm" id={`edit52s1`} onClick={id => this.AddJobtype(item._id)}>
                                  <i className="fa fa-check" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`edit52s1`}>
                                  Add to Employee
                                </UncontrolledTooltip>
                              </Fragment>
                            )}
                          </Fragment>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>
                        <h5>No record available</h5>
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
          ) : null}
        </Fragment>
      </Fragment>
    );
  }
}

export default withRouter(Listjobtype);
