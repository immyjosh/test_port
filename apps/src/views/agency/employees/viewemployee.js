/* eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import Rating from "react-rating";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,
  Input,
  Popover,
  PopoverHeader,
  PopoverBody,
  InputGroup,
  InputGroupAddon,
  ModalHeader,
  ModalBody,
  ModalFooter,
  UncontrolledTooltip,
  Modal
  // UncontrolledTooltip
} from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
// import { Link } from "react-router-dom";
import { timelist } from "../../common/utils";
import { Bar } from "react-chartjs-2";
import Widget02 from "../../Template/Widgets/Widget02";
import FileSaver from "file-saver";
import moment from "moment";
import Employeemap from "./employeemapview";
import MapList from "../../common/maplist";
import Shiftcalender from "./shiftcalender";
import { AvField, AvForm } from "availity-reactstrap-validation";
import { Editor } from "@tinymce/tinymce-react/lib/es2015";
import { Settings } from "../../../api/key";

const getTime = time => {
  return timelist.filter(val => Number(val.value) === Number(time)).map(list => list.label);
};

class Editemployee extends Component {
  state = {
    employee: false,
    employeee: this.props.location.state.rowid,
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all",
    employeeedashboard: false,
    added: 0,
    accepted: 0,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    timehseet_approved: 0,
    invoice_approved: 0,
    Payment_completed: 0,
    expired: 0,
    totalshiftCount: 0,
    draft: 0,
    approved: 0,
    part_paid: 0,
    paid: 0,
    draft_rate: 0,
    approved_rate: 0,
    part_paid_rate: 0,
    paid_rate: 0,
    mailpopover: false,
    recepient: "",
    name: ""
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/administrators/employee/get",
      method: "POST",
      data: { id: this.props.location.state.rowid, pagefor: "view" }
    }).then(res => {
      if (res.status === 1 && res.response && res.response.result && res.response.result[0]) {
        const datas = res.response.result[0] || {};
        const lat = datas.address.lat;
        const lng = datas.address.lon;
        const available = datas.available.map(aval => {
          return {
            from: getTime(aval.from),
            to: getTime(aval.to),
            day: aval.day,
            status: aval.status === 1 ? "Active" : "In-Active"
          };
        });
        const unAvailable = datas.timeoff.map(val => {
          return {
            from: getTime(val.from),
            to: getTime(val.to),
            date: moment(val.date).format("DD-MM-YYYY")
          };
        });
        this.setState({
          avatar_return: datas.avatar,
          staffprofile: res.response.result && datas.staffprofile ? datas.staffprofile : "",
          employee: datas,
          employeemap: { lat, lng, ...datas },
          name: datas.name,
          available,
          timeoff: unAvailable
        });
      } else if (res.status === 1) {
        toast.error(res.response);
      }
    });
    this.dashboard();
    request({
      url: "/agency/emailtemplate/selecttypedata",
      method: "POST",
      data: { type: "profile" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({ emailtemplatelist: res.response.result });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/employeerating",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.status === 1) {
        this.setState({ employee_ratings: res.response.result });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  editpage = (id, job_t) => {
    return this.props.history.push({
      pathname: "/agency/editemployee",
      state: { rowid: this.props.location.state.rowid, job_type: "" }
    });
  };

  newPage = page => {
    if (page) {
      let URL = "";
      switch (page) {
        case "staffprofile":
          URL = "/agency/employee/staffprofile/view";
          break;
        case "Invoice":
          URL = "/agency/employee/invoice";
          break;
        case "Time":
          URL = "/agency/employee/timesheet";
          break;
        default:
          break;
      }
      this.props.history.push({
        pathname: URL,
        state: {
          rowid: this.props.location.state.rowid
        }
      });
    }
  };

  dashboard() {
    const { employeee } = this.state;
    if (employeee) {
      request({
        url: "/agency/employee/dashboard",
        method: "POST",
        data: {
          employee: employeee,
          selectshift: this.state.selectshift,
          selecttimesheet: this.state.selecttimesheet,
          selectinvoice: this.state.selectinvoice
        }
      }).then(res => {
        if (res.status === 1) {
          if (res.response.result && res.response.result.length > 0) {
            const employeeedashboard = res.response.result[0] || false;
            this.setState({ employeeedashboard });
            if (employeeedashboard && employeeedashboard.shifts) {
              let data = employeeedashboard.shifts.length > 0 ? employeeedashboard.shifts[0] : {};
              this.setState({
                added: data.added || 0,
                accepted: data.accepted || 0,
                assigned: data.assigned || 0,
                completed: data.completed || 0,
                ongoing: data.ongoing || 0,
                timehseet_approved: data.timehseet_approved || 0,
                invoice_approved: data.invoice_approved || 0,
                Payment_completed: data.Payment_completed || 0,
                expired: data.expired || 0,
                totalshiftCount:
                  (data.added ? data.added : 0) +
                  (data.accepted ? data.accepted : 0) +
                  (data.assigned ? data.assigned : 0) +
                  (data.completed ? data.completed : 0) +
                  (data.ongoing ? data.ongoing : 0) +
                  (data.timehseet_approved ? data.timehseet_approved : 0) +
                  (data.invoice_approved ? data.invoice_approved : 0) +
                  (data.Payment_completed ? data.Payment_completed : 0) +
                  (data.expired ? data.expired : 0)
              });
            }
            if (employeeedashboard && employeeedashboard.invoices) {
              let data = employeeedashboard.invoices.length > 0 ? employeeedashboard.invoices[0] : {};
              this.setState({
                draft: data.draft || 0,
                approved: data.approved || 0,
                part_paid: data.part_paid || 0,
                paid: data.paid || 0,
                draft_rate: data.draft_rate || 0,
                approved_rate: data.approved_rate || 0,
                part_paid_rate: data.part_paid_rate || 0,
                paid_rate: data.paid_rate || 0
              });
            }
          }
        }
      });
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.dashboard());
  };
  handleInvalidSubmit(event, errors, values) {
    toast.error("Please Fill Required Fields");
  }
  downloadPDF = purpose => {
    client.defaults.responseType = "blob";
    let toastId = "downmail0";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/agency/employee/profile",
      method: "POST",
      data: { id: this.props.location.state.rowid, purpose }
    })
      .then(res => {
        if (res && res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        } else if (res) {
          const file = new Blob([res], { type: "application/pdf" });
          const name = this.state.name ? `${this.state.name} Profile.pdf` : "Profile.pdf";
          FileSaver(file, name);
          toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        }
        client.defaults.responseType = "json";
      })
      .catch(error => {
        client.defaults.responseType = "json";
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
  };

  mailPDF = purpose => {
    if (this.state.recepient) {
      let toastId = "downmail";
      toastId = toast.info("Sending......", { autoClose: false });
      request({
        url: "/agency/employee/profile",
        method: "POST",
        data: { id: this.props.location.state.rowid, purpose, email: this.state.recepient }
      })
        .then(res => {
          if (res && res.status === 1) {
            toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.setState({
              mailpopover: false
            });
            this.componentDidMount();
          } else {
            toast.update(toastId, { render: res.response || "Unable To Send Mail, Try Again Later", type: toast.TYPE.ERROR, autoClose: 2500 });
          }
        })
        .catch(error => {
          toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        });
    } else {
      toast.error("Recepient Mail ID Required");
    }
  };

  MailPopOver = () => {
    this.setState({
      mailpopover: !this.state.mailpopover
    });
  };
  actAsEmployee = (e, id) => {
    e.stopPropagation();
    const tabOpen2 = window.open("about:blank", "newtab2");
    request({
      url: "/agency/employee/actasemployee",
      method: "POST",
      data: { id: id }
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUSE", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          return (
            (tabOpen2.location = "/user/dashboard"),
            setTimeout(() => {
              window.location.reload();
            }, 800)
          );
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };
  MailModalOpen = () => {
    if (!this.state.mailmodal) {
      const typeid = this.state.emailtemplatelist.filter(list => list.default_mail).map(list => list._id)[0];
      this.Templatelist(typeid);
      this.setState({ typeid });
    }
    this.setState({
      mailmodal: !this.state.mailmodal
    });
  };
  Templatelist(value) {
    request({
      url: "/agency/emailtemplate/getemailcontent",
      method: "POST",
      data: { id: value, client: this.state.client_data && this.state.client_data.companyname, employee: this.state.employee_name }
    }).then(res => {
      if (res.status === 1) {
        const data = res.response.result;
        this.setState({
          email_content: data.html,
          email_subject: data.subject
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  ProfileMailPDF = () => {
    client.defaults.responseType = "json";
    if (this.state.recepient) {
      let toastId = "mail_025452_mail";
      toastId = toast.info("Sending......", { autoClose: false });
      request({
        url: "/agency/employee/profile",
        method: "POST",
        data: {
          id: this.props.location.state.rowid,
          purpose: "mail",
          email: this.state.recepient,
          recepient: this.state.recepient,
          email_content: this.state.email_content,
          email_subject: this.state.email_subject
        }
      }).then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.MailModalOpen();
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    } else {
      toast.error("Recepient Mail ID Required");
    }
  };
  render() {
    const { employee, recepient, typeid, emailtemplatelist, email_content, email_subject, employeeedashboard, employee_ratings } = this.state || {};
    const bar = {
      labels: ["Shifts"],
      datasets: [
        {
          label: "Total",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.totalshiftCount]
        },
        {
          label: "Assigned",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.assigned]
        },
        {
          label: "Ongoing",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.ongoing]
        },
        {
          label: "Completed",
          backgroundColor: "#4dbd74",
          borderColor: "#4dbd74",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#4dbd74",
          data: [this.state && this.state.completed]
        }
      ]
    };
    const bar1 = {
      labels: ["Invoices"],
      datasets: [
        {
          label: "Pending",
          backgroundColor: "#20a8d8b8",
          borderColor: "#1382a9",
          borderWidth: 1,
          hoverBackgroundColor: "#20a8d8",
          hoverBorderColor: "#1382a9",
          data: [this.state && this.state.draft_rate]
        },
        {
          label: "Earnings",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.approved_rate]
        }
      ]
    };
    const {
      staff_grade,
      staff_dateofbirth,
      staff_gender,
      staff_pinno,
      staff_nmc_expiry,
      staff_dbs_certificate,
      staff_dbs_issue_date,
      staff_disclosure_name,
      staff_online_status,
      staff_workinuk,
      staff_skill_check,
      staff_reference,
      staff_qualification_verify,
      staff_face_interview,
      education_qualification,
      healthsafety,
      health_safety_date,
      coshh,
      coshh_end_date,
      firesafety,
      fire_safety_date,
      infectioncontrol,
      infection_control_date,
      foodsafety,
      food_safety_date,
      movinghandling,
      moving_handling_date,
      basiclifesupport,
      basiclife_support_date,
      dataprotection,
      data_protection_date,
      equalityinclusion,
      equalityinclusion_date,
      safeguardingvulnerable,
      safeguardingvulnerable_date,
      conflictmanagement,
      conflict_mgn_date,
      loneworkertraining,
      loneworkertraining_date,
      challengingbehaviour,
      challenging_behaviour_date,
      restrainttraining,
      restraint_trg_date,
      mentalcapacity,
      mental_capacity_date,
      staff_language,
      staff_religion,
      staff_work_experience
    } = this.state && this.state.staffprofile ? this.state.staffprofile : {};
    return (
      <div className="animated fadeIn">
        <Row>
          {employee ? (
            <Col xs="12" md="12">
              <Card>
                <ToastContainer position="top-right" autoClose={2500} />
                <CardHeader>
                  <strong>View</strong> Details
                  <div className="card-actions">
                    {/* <button onClick={() => this.newPage("staffprofile")}>
                      <i className="icon-user" /> Staff Profile
                      <small className="text-muted" />
                    </button>*/}
                    <button onClick={() => this.newPage("Time")}>
                      <i className="icon-clock" /> Timesheets
                      <small className="text-muted" />
                    </button>
                    <button onClick={() => this.newPage("Invoice")}>
                      <i className="fa fa-money" /> Earnings
                      <small className="text-muted" />
                    </button>
                    <button onClick={this.editpage}>
                      <i className="fa fa-edit" /> Edit
                      <small className="text-muted" />
                    </button>
                  </div>
                </CardHeader>
                <CardBody>
                  <div className="row">
                    <div className="emp-full">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>Personal Details</h4>

                          <div className="info-widths">
                            <div className="cate-ranges">
                              <img
                                width="180px"
                                height="180px"
                                src={NodeURL + "/" + this.state.avatar_return}
                                alt="Profile"
                                onError={() => {
                                  this.setState({ avatar_return: "../../img/user-profile.png" });
                                }}
                              />
                            </div>
                          </div>
                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Username <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.username}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Name <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.name}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Phone <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.phone && employee.phone.code && employee.phone.number ? `${employee.phone.code} - ${employee.phone.number}` : ""}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Email <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.email}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Status <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.status === 1 ? "Active" : "Inactive"}</span>
                              </p>

                              <p>
                                <span className="left-views">
                                  Rating <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">
                                  <Rating initialRating={employee_ratings} emptySymbol="fa fa-star-o fa-1x" fullSymbol="fa fa-star fa-2x" fractions={2} value={employee_ratings} readonly />
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4 className="dash-foheading">
                            <span className="pull-left left-dash"> Dashboard Details </span>
                            <span className="pull-right right-dash">
                              <Button title="Assign Shifts" className="pull-right ml-3 btn-assign" id="assignshifytt" onClick={() => this.props.history.push("/agency/editshift")}>
                                <i className="fa fa-mail-forward" /> Assign Shifts
                              </Button>
                              <Button title="Act As Employee" className="pull-right ml-3 btn-ask" id="actasemployee" onClick={(e, id) => this.actAsEmployee(e, this.props.location.state.rowid)}>
                                <i className="fa fa-sign-in" /> Act As Employee
                              </Button>
                              <Button title="Download" className="pull-right ml-3 btn-download" onClick={() => this.downloadPDF("download")}>
                                <i className="fa fa-download" /> Download
                              </Button>
                              {/*<Button  title="Mail" className="pull-right ml-3 btn-mail" id="mailPopover" onClick={this.MailPopOver}>*/}
                              <Button title="Mail" className="pull-right btn-mail" id="mailPopover" onClick={this.MailModalOpen}>
                                <i className="fa fa-envelope-o" /> Mail
                              </Button>
                            </span>
                          </h4>
                          <Popover placement="left" isOpen={this.state.mailpopover} target="mailPopover" toggle={this.MailPopOver}>
                            <PopoverHeader>Send Mail</PopoverHeader>
                            <PopoverBody>
                              <Row className="m-2">
                                <InputGroup>
                                  <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                                  <Input placeholder="E-mail" name="recepient" value={this.state.recepient} onChange={this.onChange} required />
                                </InputGroup>
                                <button className="btn btn-success mt-2 pull-right" onClick={() => this.mailPDF("mail")}>
                                  Send
                                </button>
                              </Row>
                            </PopoverBody>
                          </Popover>
                        </div>
                        {this.state && <Shiftcalender employeeid={this.props.location.state.rowid} />}
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges">
                            <h5>Shifts</h5>
                            <Row className="d-flex justify-content-end">
                              <Col md="4" xs="12">
                                <Input type="select" name="selectshift" value={this.state.selectshift} onChange={this.onChange}>
                                  <option value="all">All</option>
                                  <option value="1">Today</option>
                                  <option value="8">Last 7 Days</option>
                                  <option value="31">Last 30 Days</option>
                                  <option value="366">Last 12 Months</option>
                                </Input>
                              </Col>
                            </Row>
                            <div className="chart-wrapper">
                              <Bar
                                data={bar}
                                options={{
                                  maintainAspectRatio: false
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges">
                            <h5>Invoice</h5>
                            <Row className="d-flex justify-content-end">
                              <Col md="4" xs="12">
                                <Input type="select" name="selectinvoice" value={this.state.selectinvoice} onChange={this.onChange}>
                                  <option value="all">All</option>
                                  <option value="1">Today</option>
                                  <option value="8">Last 7 Days</option>
                                  <option value="31">Last 30 Days</option>
                                  <option value="366">Last 12 Months</option>
                                </Input>
                              </Col>
                            </Row>
                            <div className="chart-wrapper">
                              <Bar
                                data={bar1}
                                options={{
                                  maintainAspectRatio: false
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <div className="sub-ranges wg-class">
                            <h5>Timesheets</h5>
                            <Row>
                              <Col xs="12" md="4">
                                <Widget02 header={employeeedashboard ? employeeedashboard.timesheetapproved + employeeedashboard.timesheetpending : "0"} mainText="Total" icon="fa fa-users" color="warning" variant="1" />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02 header={employeeedashboard.timesheetapproved ? employeeedashboard.timesheetapproved : "0"} mainText="Approved" icon="fa fa-users" color="success" variant="1" />
                              </Col>
                              <Col xs="12" md="4">
                                <Widget02 header={employeeedashboard.timesheetpending ? employeeedashboard.timesheetpending : "0"} mainText="Pending" icon="fa fa-user-plus" color="warning" variant="1" />
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="emp-full">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>Address</h4>

                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Address <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.address && employee.address.formatted_address ? employee.address.formatted_address : ""}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  State/Region <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.address && employee.address.state ? employee.address.state : ""}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Country <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.address && employee.address.country ? employee.address.country : ""}</span>
                              </p>
                            </div>
                          </div>
                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Line1 <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.address && employee.address.line1 ? employee.address.line1 : ""}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  City/Town <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.address && employee.address.line2 ? employee.address.line2 : ""}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Postal Code <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{employee.address && employee.address.zipcode ? employee.address.zipcode : ""}</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>Work Details</h4>
                          {employee.locations &&
                            employee.locations.length > 0 && (
                              <p>
                                <span className="left-views">
                                  Area <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">
                                  {employee.locations.map((item, i) => (
                                    <span key={i}>{item.name},</span>
                                  ))}
                                </span>
                              </p>
                            )}
                          <p>
                            <span className="left-views">
                              Joining Date <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">{moment(employee.joining_date).format("DD-MM-YYYY")}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Job Role <span className="col-qus"> : </span>{" "}
                            </span>
                            <span className="right-views">
                              {employee.job_type.map((item, i) => (
                                <span key={i}>{item.name},</span>
                              ))}
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>Basic</h4>
                          <p>
                            <span className="left-views">
                              Grade <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_grade}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              DOB <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_dateofbirth && moment(staff_dateofbirth).format("DD-MM-YYYY")}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Gender <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_gender === "female" ? "Female" : staff_gender === "male" ? "Male" : ""}</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/*<div className="col-md-6">
                      <div className="cate-ranges">
                        {employee.job_type &&
                          employee.job_type.length > 0 && (
                            <p>
                              <span className="left-views">
                                Job Role <span className="col-qus"> : </span>{" "}
                              </span>
                              {employee.job_type.map(item => (
                                <span>{item.name},</span>
                              ))}
                            </p>
                          )}
                        
                      </div>
                    </div>*/}

                  <Row />

                  <Row>
                    <div className="col-md-6">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>DBS Details</h4>
                          <p>
                            <span className="left-views">
                              Certificate No
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_dbs_certificate}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Issue Date <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_dbs_issue_date && moment(staff_dbs_issue_date).format("DD-MM-YYYY")}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Disclosure Employer Name <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_disclosure_name}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Online Check Status <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_online_status}</span>
                          </p>
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>NMC Details</h4>
                          <p>
                            <span className="left-views">
                              PIN No <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_pinno}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Expiry Date <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{staff_nmc_expiry && moment(staff_nmc_expiry).format("DD-MM-YYYY")}</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </Row>

                  <Row>
                    <div className="emp-full">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>Checks & Verification</h4>

                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Right to Work in UK <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_workinuk}</span>
                              </p>

                              <p>
                                <span className="left-views">
                                  2 Reference on file <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_reference}</span>
                              </p>

                              <p>
                                <span className="left-views">
                                  Face to Face Intervie <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_face_interview}</span>
                              </p>
                            </div>
                          </div>

                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Skills Check <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_skill_check}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Qualification Verified <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_qualification_verify}</span>
                              </p>

                              <p>
                                <span className="left-views">
                                  Education/Qualifications <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{education_qualification}</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Row>

                  <Row>
                    <div className="emp-full">
                      <div className="cus-design">
                        <div className="cate-ranges">
                          <h4>Mandatory training</h4>

                          <div className="info-widths">
                            <div className="cate-ranges">
                              {healthsafety ? (
                                <p>
                                  <span className="left-views">
                                    Health and Safety <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{health_safety_date && moment(health_safety_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {firesafety ? (
                                <p>
                                  <span className="left-views">
                                    Fire Safety <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{fire_safety_date && moment(fire_safety_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {foodsafety ? (
                                <p>
                                  <span className="left-views">
                                    Food Safety & Nutrition <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{food_safety_date && moment(food_safety_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {basiclifesupport ? (
                                <p>
                                  <span className="left-views">
                                    Basic life support <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{basiclife_support_date && moment(basiclife_support_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {equalityinclusion ? (
                                <p>
                                  <span className="left-views">
                                    Equality & Inclusion <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{equalityinclusion_date && moment(equalityinclusion_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {conflictmanagement ? (
                                <p>
                                  <span className="left-views">
                                    Conflict Management <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{conflict_mgn_date && moment(conflict_mgn_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {challengingbehaviour ? (
                                <p>
                                  <span className="left-views">
                                    Managing Challenging Behaviour <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{challenging_behaviour_date && moment(challenging_behaviour_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {mentalcapacity ? (
                                <p>
                                  <span className="left-views">
                                    Mental Capacity Act 2005 <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{mental_capacity_date && moment(mental_capacity_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              <p>
                                <span className="left-views">
                                  Languages Spoken <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_language}</span>
                              </p>
                            </div>
                          </div>

                          <div className="info-widths">
                            <div className="cate-ranges">
                              {coshh ? (
                                <p>
                                  <span className="left-views">
                                    COSHH <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{coshh_end_date && moment(coshh_end_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {infectioncontrol ? (
                                <p>
                                  <span className="left-views">
                                    Infection Control <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{infection_control_date && moment(infection_control_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {movinghandling ? (
                                <p>
                                  <span className="left-views">
                                    Moving & Handling <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{moving_handling_date && moment(moving_handling_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {dataprotection ? (
                                <p>
                                  <span className="left-views">
                                    Data Protection <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{data_protection_date && moment(data_protection_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {safeguardingvulnerable ? (
                                <p>
                                  <span className="left-views">
                                    Safeguarding Vulnerable Adults & Children <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{safeguardingvulnerable_date && moment(safeguardingvulnerable_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {loneworkertraining ? (
                                <p>
                                  <span className="left-views">
                                    Lone Worker Training <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{loneworkertraining_date && moment(loneworkertraining_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              {restrainttraining ? (
                                <p>
                                  <span className="left-views">
                                    Restraint Training <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{restraint_trg_date && moment(restraint_trg_date).format("DD-MM-YYYY")}</span>
                                </p>
                              ) : null}
                              <p>
                                <span className="left-views">
                                  Religion <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_religion}</span>
                              </p>

                              <p>
                                <span className="left-views">
                                  Work Experience <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{staff_work_experience}</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Row>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="cate-ranges">
                        <h4>Availability</h4>

                        <table className="table table-bordered view-table ">
                          <thead>
                            <tr>
                              <th>Days</th>
                              <th> Status </th>
                              <th> Start Time </th>
                              <th> End Time </th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.available.map((day, i) => (
                              <tr className="common-tab" key={i}>
                                <td className="bold-days">
                                  {day.day === "sun"
                                    ? "Sunday"
                                    : day.day === "mon"
                                      ? "Monday"
                                      : day.day === "tue"
                                        ? "Tuesday"
                                        : day.day === "wed"
                                          ? "Wednesday"
                                          : day.day === "thu"
                                            ? "Thursday"
                                            : day.day === "fri"
                                              ? "Friday"
                                              : day.day === "sat"
                                                ? "Saturday"
                                                : ""}
                                </td>
                                <td className={day.status || ""}>{day.status}</td>
                                <td>{day.from}</td>
                                <td>{day.to}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="cate-ranges">
                        <h4>Un Availability</h4>
                        <table className="table table-bordered view-table over-fun">
                          <thead>
                            <tr>
                              <th> Status </th>
                              <th> Start Time </th>
                              <th> End Time </th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.timeoff && this.state.timeoff.length > 0 ? (
                              this.state.timeoff.map((day, i) => (
                                <tr className="common-tab" key={i}>
                                  <td>{day.date}</td>
                                  <td>{day.from}</td>
                                  <td>{day.to}</td>
                                </tr>
                              ))
                            ) : (
                              <tr>
                                <td colSpan="9">
                                  <p className="no_record d-flex justify-content-center">No record available</p>
                                </td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                   <CardHeader className="located-map"><span className="left-loca">Employee Location </span> </CardHeader>
                  {this.state && this.state.employeemap ? <Employeemap markers={[this.state.employeemap]} /> : <MapList mapListDetails={{ locationDetails: [this.state.employeemap], Page: "joblist" }} />}
                </CardBody>
                <CardFooter>
                  {/* <Link to="/agency/employeelist">*/}
                  <Button
                    type="button"
                    color="secondary"
                    title="Back"
                    onClick={() => {
                      window.history.go(-1);
                    }}
                  >
                    <i className="fa fa-arrow-left" /> Back
                  </Button>
                  {/*</Link>*/}
                </CardFooter>
              </Card>
            </Col>
          ) : null}
        </Row>
        <Modal isOpen={this.state.mailmodal} className="modal-lg">
          <Fragment>
            <ModalHeader toggle={this.MailModalOpen}>Profile</ModalHeader>
            <AvForm onValidSubmit={this.ProfileMailPDF} onInvalidSubmit={this.handleInvalidSubmit}>
              <ModalBody>
                <Row>
                  <Col xs="12" md="6">
                    <AvField name="recepient" onChange={this.onChange} value={recepient} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  <Col xs="12" md="6">
                    <AvField type="select" name="typeid" label="Email Template" onChange={this.onChangeSelect} value={typeid} required>
                      <option>Select</option>
                      {emailtemplatelist && emailtemplatelist.length > 0 ? (
                        <>
                          {emailtemplatelist.map((list, i) => (
                            <option key={i} value={list._id}>
                              {list.heading}
                            </option>
                          ))}
                        </>
                      ) : null}
                    </AvField>
                  </Col>
                  <Col xs="12" md="12">
                    <AvField name="email_subject" onChange={this.onChange} value={email_subject} placeholder="Subject" label="Subject" type="text" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="12">
                    <p>Message</p>
                    <Editor
                      apiKey={Settings.tinymceKey}
                      initialValue={email_content}
                      init={{
                        plugins: "link image code",
                        toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                        height: 480
                      }}
                      onChange={this.handleEditorChange}
                      value={email_content}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between">
                <div>
                  <Button color="secondary" type="button" onClick={this.MailModalOpen} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                </div>
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  <i className="fa fa-envelope-open-o" />
                </Button>{" "}
                <UncontrolledTooltip placement="left" target="savecontinue2">
                  Send Mail
                </UncontrolledTooltip>
              </ModalFooter>
            </AvForm>
          </Fragment>
        </Modal>
      </div>
    );
  }
}
export default Editemployee;
