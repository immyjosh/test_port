import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  Input,
  InputGroup,
  Table,
  UncontrolledTooltip,
  Row,
  Col
  /* Modal, ModalBody, ModalFooter, ModalHeader */
} from "reactstrap";
import fileDownload from "js-file-download";
import Loader from "../../common/loader";
import Pagination from "react-js-pagination";
import moment from "moment/moment";
import MapList from "../../common/maplist";
import request, { /* NodeURL, */ client } from "../../../api/api";
import Employeemap from "./employeemaplist";
// import { DateRangePicker } from "react-dates";

const tStyle = {
  cursor: "pointer"
};

class Employeelist extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    isLoader: false,
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    profilemodal: false
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    this.populateData();
  }
  editpage = (e, id, job_t) => {
    e.stopPropagation();
    return this.props.history.push({
      pathname: "/agency/editemployee",
      state: { rowid: id, job_type: job_t[0]._id }
    });
  };
  viewpage = (id, job_t) => {
    return this.props.history.push({
      pathname: "/agency/viewemployee",
      state: { rowid: id, job_type: job_t[0]._id }
    });
  };
  populateData() {
    request({
      url: "/administrators/employee/list",
      method: "POST",
      data: this.state.tableOptions
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            isLoader: false,
            adminlist: res.response.result,
            pages: res.response.fullcount,
            currPage: res.response.length
          });
          console.log("res.response.result ", res.response.result);
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["name", "job_type", "tel", "locations", "status", "hours", "lastshift"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Email") {
        state.tableOptions.filter = "email";
      } else if (value === "Area") {
        state.tableOptions.filter = "locations";
      } else if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Phone") {
        state.tableOptions.filter = "phone.number";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Job Role") {
        state.tableOptions.filter = "job_type";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/employee/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("New Employee Added");
  }
  actAsEmployee = (e, id) => {
    e.stopPropagation();
    const tabOpen2 = window.open("about:blank", "newtab2");
    request({
      url: "/agency/employee/actasemployee",
      method: "POST",
      data: { id: id }
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUSE", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          return (
            (tabOpen2.location = "/user/dashboard"),
            setTimeout(() => {
              window.location.reload();
            }, 800)
          );
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };
  viewprofileModal = (e, item) => {
    e.stopPropagation();
    this.setState({
      profilemodal: !this.state.profilemodal,
      Profile_details: item,
      Profile_name: item.name,
      Profile_username: item.username,
      Profile_phone: item.phone,
      Profile_email: item.email,
      Profile_locations: item.locations,
      Profile_holiday_allowance: item.holiday_allowance,
      Profile_joining_date: moment(item.joining_date).format("DD-MM-YYYY"),
      Profile_final_date: moment(item.final_date).format("DD-MM-YYYY"),
      Profile_address: item.address,
      Profile_job_type: item.job_type,
      Profile_avatar: item.avatar,
      Profile_status: item.status
    });
  };
  closeprofileModal = item => {
    this.setState({
      profilemodal: !this.state.profilemodal,
      Profile_details: "",
      Profile_name: "",
      Profile_username: "",
      Profile_phone: "",
      Profile_email: "",
      Profile_locations: "",
      Profile_holiday_allowance: "",
      Profile_joining_date: "",
      Profile_final_date: "",
      Profile_address: "",
      Profile_job_type: "",
      Profile_avatar: "",
      Profile_status: "",
      emplist_status: ""
    });
  };
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    const hours = hour => {
      if (hour && hour[0]) {
        const total = hour[0].toFixed(2);
        const split = total.split(".");
        const totalhours = `${split[0]}:` + split[1] || `00`;
        return totalhours;
      } else {
        return "0:00";
      }
    };

    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          {this.props.location.pathname === "/agency/dashboard" ? (
            <CardHeader>
              <i className="icon-list" />
              Recent Employees List
            </CardHeader>
          ) : (
            <CardHeader>
              <i className="icon-list" />
              Employees List
              <div className="card-actions" style={tStyle}>
                <button onClick={() => this.props.history.push("/agency/addemployee")}>
                  <i className="fa fa-plus" /> Add New
                  {/* <small className="text-muted" />*/}
                </button>
                <button onClick={this.export}>
                  <i className="fa fa-upload" /> Export
                  {/* <small className="text-muted" />*/}
                </button>
              </div>
            </CardHeader>
          )}
          <CardBody>
            <Row>
              <Col xs="12" md="12">
                <div className="row justify-content-end">
                  {/* <div className="col-lg-7">
                    <DateRangePicker
                      showClearDates={true}
                      startDate={this.state.start_date}
                      startDateId="start_date"
                      endDate={this.state.end_date}
                      endDateId="end_date"
                      onDatesChange={({ startDate, endDate }) => {
                        this.setState({
                          start_date: startDate,
                          end_date: endDate
                        });
                      }}
                      isOutsideRange={day => day.isBefore(this.state.start_date)}
                      focusedInput={this.state.focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      displayFormat="DD-MM-YYYY"
                    />
                    <Button
                      size="md"
                      color="primary rounded-0"
                      onClick={() => {
                        this.fromTo();
                      }}
                    >
                      <i className="fa fa-search" />
                    </Button>
                  </div> */}
                  <div className="col-lg-5">
                    <InputGroup>
                      <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-2">
                        <option>All</option>
                        <option>Name</option>
                        <option>Job Role</option>
                        <option>Area</option>
                        <option>Tel</option>
                      </Input>
                      <Input type="text" ref="search" placeholder="Search..." name="search" onChange={e => this.search(e.target.value)} className="rounded-0  col-lg-10" />
                      <Button
                        className="rounded-0 "
                        color="primary"
                        id="clear"
                        onClick={() => {
                          ReactDOM.findDOMNode(this.refs.search).value = "";
                          this.search("");
                        }}
                      >
                        <i className="fa fa-remove" />
                      </Button>
                      <UncontrolledTooltip placement="top" target="clear">
                        Clear
                      </UncontrolledTooltip>
                    </InputGroup>
                  </div>
                </div>
                <div className="table-responsive">
                  <Table hover bordered responsive size="sm">
                    <thead>
                      <tr>
                        <th>S.No.</th>
                        <th
                          onClick={() => {
                            this.sort("name");
                          }}
                        >
                          Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                        </th>
                        {/*  <th
                          onClick={() => {
                            this.sort("email");
                          }}
                        >
                          Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                        </th> */}

                        <th
                          onClick={() => {
                            this.sort("job_type");
                          }}
                        >
                          Job Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_type" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("phone");
                          }}
                        >
                          Tel <i style={{ paddingLeft: "25px" }} className={sorticondef} id="tel" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("locations");
                          }}
                        >
                          Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                        </th>
                        {/* <th
                          onClick={() => {
                            this.sort("createdAt");
                          }}
                        >
                          Created Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                        </th> */}
                        <th
                          onClick={() => {
                            this.sort("status");
                          }}
                        >
                          Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("hours");
                          }}
                        >
                          Hours <i style={{ paddingLeft: "25px" }} className={sorticondef} id="hours" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("lastshift");
                          }}
                        >
                          Last Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="lastshift" />
                        </th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.adminlist.length > 0 ? (
                        this.state.adminlist.map((item, i) => (
                          <tr key={item._id} onClick={(id, job) => this.viewpage(item._id, item.job_data)}>
                            <td>{this.state.tableOptions.skip + i + 1}</td>
                            <td>{item.name}</td>
                            {/* <td className="client-email">{item.email}</td> */}
                            <td className="client-role">
                              {" "}
                              {item.job_type.map((list, key) => (
                                <span key={key}>
                                  {list} <Fragment>,</Fragment>
                                </span>
                              ))}
                            </td>
                            <td className="client-email">
                              {item.phone.code} {item.phone.number}
                            </td>
                            <td>
                              {" "}
                              {item.locations.map((list, key) => (
                                <span key={key}>
                                  {list} <Fragment>,</Fragment>
                                </span>
                              ))}
                            </td>
                            {/* <td>{moment(item.createdAt).format("DD-MM-YYYY")}</td> */}
                            <td>
                              {item.status === 1 && item.isverified === 1 ? <Badge color="success">Active</Badge> : null}
                              {item.status === 0 && item.isverified === 1 ? <Badge color="danger">In-active</Badge> : null}
                              {item.status === 1 && item.isverified === 0 ? <Badge color="danger">Not Verified</Badge> : null}
                            </td>
                            <td className="client-email">{hours(item.hours)}</td>
                            <td className="">{item.lastshift && item.lastshift.length > 0 ? moment(item.lastshift[0]).format("DD-MM-YYYY") : "-"}</td>
                            <td>
                              {/* <Button size="sm" color="info" className="mr-2 table-edit" onClick={(e, items) => this.viewprofileModal(e, item)}>
                                <i className="fa fa-user-circle" />
                              </Button> */}
                              <button type="button" title="Edit" className="btn table-edit ml-3" color="info" id={`edit${i}`} onClick={(e, id, job) => this.editpage(e, item._id, item.job_data)}>
                                <i className="fa fa-edit" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                Edit
                              </UncontrolledTooltip>
                              {item.status === 1 && item.isverified === 1 ? (
                                <Fragment>
                                  <button type="button" title="Act As Employee" className="btn table-edit ml-3 mr-3" id={`act${i}`} onClick={(e, id) => this.actAsEmployee(e, item._id)}>
                                    <i className="fa fa-sign-in" />
                                  </button>
                                  <UncontrolledTooltip placement="top" target={`act${i}`}>
                                    Act As Employee
                                  </UncontrolledTooltip>
                                </Fragment>
                              ) : null}
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr className="text-center">
                          <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                        </tr>
                      )}
                      {this.state.isLoader && <Loader />}
                    </tbody>
                  </Table>
                </div>
                <nav className="float-left">
                  <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                  </Input>
                </nav>
                <nav className="float-right">
                  <div>
                    <Pagination
                      prevPageText="Prev"
                      nextPageText="Next"
                      firstPageText="First"
                      lastPageText="Last"
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.tableOptions.limit}
                      totalItemsCount={this.state.pages}
                      pageRangeDisplayed={this.state.pageRangeDisplayed}
                      onChange={this.paginate}
                    />
                  </div>
                </nav>
              </Col>
              <Col xs="12" md="12" className="map_min">
                <CardHeader className="located-map"><span className="left-loca">Employees Locations </span> {/* <span className="right-loca"> <ul> <li className="agency-pro"><span></span> Agency</li> <li className="client-pro"><span></span> Client</li> <li className="emply-pro"><span></span> Employee</li> <li className="building-pro"><span></span> Building</li> </ul> </span>*/}</CardHeader>
                {this.state && this.state.adminlist && this.state.adminlist.length && this.state.adminlist.length > 0 ? (
                  <Employeemap markers={this.state.adminlist} />
                ) : (
                  <MapList mapListDetails={{ locationDetails: this.state.adminlist, Page: "joblist" }} />
                )}
              </Col>
            </Row>
          </CardBody>
        </Card>
        {/*   <Modal isOpen={this.state.profilemodal} className={"modal-lg view_profile"}>
          <ModalHeader toggle={this.closeprofileModal}>Profile</ModalHeader>
          <ModalBody>
            <section className="empl_profle ">
              <div className="fl_flex">
                <div className="pro_lft">
                  <img alt="" src={NodeURL + "/" + this.state.Profile_avatar} />
                </div>
                <div className="pro_rght">
                  <p>{this.state.Profile_name}</p>
                  <p>{this.state.Profile_email}</p>
                  <p>
                    {this.state.Profile_job_type &&
                      this.state.Profile_job_type.map((list, key) => (
                        <Fragment key={key}>
                          {list}
                          <Fragment>,</Fragment>
                        </Fragment>
                      ))}
                  </p>
                </div>
              </div>

              <div className="emp_list">
                <ul className="list_det">
                  <li>
                    <span>Name </span>
                    <span>{this.state.Profile_name} </span>
                  </li>
                  <li>
                    <span>Area</span>
                    <span>
                      {this.state.Profile_locations &&
                        this.state.Profile_locations.length > 0 &&
                        this.state.Profile_locations.map((list, key) => (
                          <span key={key}>
                            {list} <Fragment>,</Fragment>
                          </span>
                        ))}
                    </span>
                  </li>{" "}
                  <li>
                    <span>Phone</span>
                    <span>
                      {" "}
                      {this.state.Profile_phone ? (
                        <Fragment>
                          {this.state.Profile_phone.code} - {this.state.Profile_phone.number}
                        </Fragment>
                      ) : null}
                    </span>
                  </li>{" "}
                  <li>
                    <span>Job Role </span>
                    <span>
                      {" "}
                      {this.state.Profile_job_type &&
                        this.state.Profile_job_type.map((list, key) => (
                          <Fragment key={key}>
                            {list}
                            <Fragment>,</Fragment>
                          </Fragment>
                        ))}
                    </span>
                  </li>{" "}
                  <li>
                    <span>User Name </span>
                    <span>{this.state.Profile_username}</span>
                  </li>{" "}
                  <li>
                    <span>Email </span>
                    <span>{this.state.Profile_email}</span>
                  </li>{" "}
                  <li>
                    <span>Joining Date </span>
                    <span>{this.state.Profile_joining_date}</span>
                  </li>
                  <li>
                    <span>Last Working Date </span>
                    <span>{this.state.Profile_final_date}</span>
                  </li>
                  <li>
                    <span>Address </span>
                    <span>
                      {this.state.Profile_address ? (
                        <Fragment>
                          {this.state.Profile_address.formatted_address} , {this.state.Profile_address.zipcode} .
                        </Fragment>
                      ) : null}
                    </span>
                  </li>
                </ul>
              </div>
            </section>
          </ModalBody>
          <ModalFooter className="modal-gal">
            <Button color="secondary" onClick={this.closeprofileModal}>
              <i className="fa fa-close" />
            </Button>
          </ModalFooter>
        </Modal>
       */}
      </div>
    );
  }
}

export default Employeelist;
