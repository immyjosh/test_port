/* eslint no-unused-vars: 0*/
import { AvField, AvFeedback, AvForm, AvGroup, AvInput, AvRadioGroup, AvRadio } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, TabContent, TabPane, Nav, NavItem, NavLink, Input } from "reactstrap";
import moment from "moment";
import request, { client } from "../../../api/api";
import addnodify from "./employeelist";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import classnames from "classnames";
import { timelist } from "../../common/utils";
import { SingleDatePicker } from "react-dates";
import Select from "react-select";
import "react-select/dist/react-select.css";
import Avatar from "react-avatar-edit";
import Datetime from "react-datetime";

const subh4 = {
  color: "rgb(177, 50, 50)"
};

class Addemployee extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    surname: "",
    jobname: "",
    workname: "",
    email: "",
    role: "",
    code: "",
    job_type: [],
    employee_rate: "",
    agency_rate: "",
    number: "",
    number1: "",
    address: "",
    address1: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    zipcode1: "",
    formatted_address: "",
    lat: "",
    lon: "",
    avatar: "",
    phoneerror: false,
    phoneerror1: false,
    jobtypelist: [],
    worklist: [],
    phonestatus: "",
    phonestatus1: "",
    activeTab: "1",
    availableactiveTab: "1",
    unavailableactiveTab: "1",
    timelist: [],
    available_list: [],
    holiday_allowance: "",
    joining_date: null,
    final_date: null,
    sunstarttime: "",
    sunendtime: "",
    sunavailablestatus: "",
    monstarttime: "",
    monendtime: "",
    monavailablestatus: "",
    tuestarttime: "",
    tueendtime: "",
    tueavailablestatus: "",
    wedstarttime: "",
    wedendtime: "",
    wedavailablestatus: "",
    thustarttime: "",
    thuendtime: "",
    thuavailablestatus: "",
    fristarttime: "",
    friendtime: "",
    friavailablestatus: "",
    satstarttime: "",
    satendtime: "",
    satavailablestatus: "",
    unavaiablestarttime: "",
    unavaiableendtime: "",
    unavailablestatus: null,
    status: 1,
    isverified: 1,
    educationlist: [{ from: "", to: "", date: "" }],
    locations: "",
    avatarfile: "",
    jobrate: "",
    jobmodel: false,
    workmodel: false,
    worklistselect: [],
    jobtypelistselect: [],
    employee_count: "",
    agenydashboard: {},
    job_role_error: false,
    location_error: false,
    propreview: null,
    staff_dateofbirth: moment(new Date()).subtract(18, "years")
  };
  flash = new addnodify();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ timelist, employee_count: token.employee_count });
    request({
      url: "/agencydashboard",
      method: "post"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ agenydashboard: res.response.result[0] || {} });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/jobtype/common/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
        const jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          jobtypelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/location/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          worklist: res.response.result || {}
        });
        const worklistselect = this.state.worklist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          worklistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: "",
        lat: "",
        lon: ""
      });
    }
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type: value });
    } else {
      this.setState({ job_type: "" });
    }
  };
  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name + " " + results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2,
      phonestatus: status
    });
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2,
      phonestatus1: status
    });
  };
  joinDate(date) {
    if (date._isAMomentObject === true) {
      this.setState({
        joindateError: false
      });
    } else {
      this.setState({
        joindateError: true
      });
    }
    this.setState({
      joining_date: date
    });
  }
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  joinDatefield = value => {
    if (value === "" || value === null) {
      this.setState({
        joindateError: true
      });
    }
  };
  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          avatar: file_data,
          avatarfile: URL.createObjectURL(file_data),
          avatarName: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  OnFormSubmit = (e, valuess) => {
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else if (this.state.joining_date === null || this.state.joining_date === "") {
      this.setState({
        joindateError: true
      });
    } else if (this.state.locations === "") {
      this.setState({
        location_error: true
      });
    } else if (this.state.job_type === "") {
      this.setState({
        job_role_error: true
      });
    } else {
      client.defaults.responseType = "json";
      this.setState({
        joindateError: false
      });
      const values = { ...valuess, ...this.state };
      const data = new FormData();
      values.locations.map((item, i) => {
        data.append("locations", item.value);
        return true;
      });
      values.job_type.map((item, i) => {
        data.append("job_type", item.value);
        return true;
      });

      const {
        // staff_name,
        // staff_preffered_name,
        // staff_position,
        // staff_photo,
        staff_dateofbirth,
        staff_nmc_expiry,
        staff_dbs_issue_date,
        health_safety_date,
        coshh_end_date,
        fire_safety_date,
        infection_control_date,
        food_safety_date,
        moving_handling_date,
        basiclife_support_date,
        data_protection_date,
        equalityinclusion_date,
        safeguardingvulnerable_date,
        conflict_mgn_date,
        loneworkertraining_date,
        challenging_behaviour_date,
        restraint_trg_date,
        mental_capacity_date,
      } = this.state && this.state.staffprofile ? { ...this.state.staffprofile, ...this.state } : {};

      data.append("username", values.username);
      data.append("password", values.password);
      data.append("confirm_password", values.confirm_password);
      data.append("name", values.name);
      data.append("surname", values.surname);
      data.append("email", values.email);
      data.append("avatar", values.avatar);
      data.append("phone[code]", values.code);
      data.append("phone[number]", values.number);
      data.append("phone[dailcountry]", values.dailcountry);
      data.append("address[line1]", values.line1);
      data.append("address[line2]", values.line2);
      data.append("address[city]", values.city);
      data.append("address[state]", values.state);
      data.append("address[country]", values.country);
      data.append("address[zipcode]", values.zipcode);
      data.append("address[formatted_address]", values.formatted_address);
      data.append("address[lat]", values.lat);
      data.append("address[lon]", values.lon);
      data.append("status", values.status);
      data.append("isverified", values.isverified);
      // data.append("holiday_allowance", values.holiday_allowance);
      data.append("employee_rate", values.employee_rate);
      data.append("agency_rate", values.agency_rate);
      data.append("joining_date", values.joining_date);
      // data.append("final_date", values.final_date);
      data.append("available[0][day]", "sun");
      data.append("available[0][from]", values.sunstarttime.value || "");
      data.append("available[0][to]", values.sunendtime.value || "");
      data.append("available[0][status]", values.sunavailablestatus || "");
      data.append("available[1][day]", "mon");
      data.append("available[1][from]", values.monstarttime.value || "");
      data.append("available[1][to]", values.monendtime.value || "");
      data.append("available[1][status]", values.monavailablestatus || 0);
      data.append("available[2][day]", "tue");
      data.append("available[2][from]", values.tuestarttime.value || "");
      data.append("available[2][to]", values.tueendtime.value || "");
      data.append("available[2][status]", values.tueavailablestatus || 0);
      data.append("available[3][day]", "wed");
      data.append("available[3][from]", values.wedstarttime.value || "");
      data.append("available[3][to]", values.wedendtime.value || "");
      data.append("available[3][status]", values.wedavailablestatus || 0);
      data.append("available[4][day]", "thu");
      data.append("available[4][from]", values.thustarttime.value || "");
      data.append("available[4][to]", values.thuendtime.value || "");
      data.append("available[4][status]", values.thuavailablestatus || 0);
      data.append("available[5][day]", "fri");
      data.append("available[5][from]", values.fristarttime.value || "");
      data.append("available[5][to]", values.friendtime.value || "");
      data.append("available[5][status]", values.friavailablestatus || 0);
      data.append("available[6][day]", "sat");
      data.append("available[6][from]", values.satstarttime.value || "");
      data.append("available[6][to]", values.satendtime.value || "");
      data.append("available[6][status]", values.satavailablestatus || 0);
      data.append("staffprofile[staff_grade]", values.staff_grade);
      data.append("staffprofile[staff_dateofbirth]", values.staff_dateofbirth || staff_dateofbirth);
      data.append("staffprofile[staff_gender]", values.staff_gender);
      data.append("staffprofile[staff_pinno]", values.staff_pinno);
      data.append("staffprofile[staff_nmc_expiry]", values.staff_nmc_expiry || staff_nmc_expiry || "");
      data.append("staffprofile[staff_dbs_certificate]", values.staff_dbs_certificate);
      data.append("staffprofile[staff_dbs_issue_date]", values.staff_dbs_issue_date || staff_dbs_issue_date || "");
      data.append("staffprofile[staff_disclosure_name]", values.staff_disclosure_name);
      data.append("staffprofile[staff_online_status]", values.staff_online_status);
      data.append("staffprofile[staff_workinuk]", values.staff_workinuk);
      data.append("staffprofile[staff_skill_check]", values.staff_skill_check);
      data.append("staffprofile[staff_reference]", values.staff_reference);
      data.append("staffprofile[staff_qualification_verify]", values.staff_qualification_verify);
      data.append("staffprofile[staff_face_interview]", values.staff_face_interview);
      data.append("staffprofile[education_qualification]", values.education_qualification);
      data.append("staffprofile[healthsafety]", values.healthsafety);
      data.append("staffprofile[health_safety_date]", values.health_safety_date || health_safety_date || "");
      data.append("staffprofile[coshh]", values.coshh);
      data.append("staffprofile[coshh_end_date]", values.coshh_end_date || coshh_end_date || "");
      data.append("staffprofile[firesafety]", values.firesafety);
      data.append("staffprofile[fire_safety_date]", values.fire_safety_date || fire_safety_date || "");
      data.append("staffprofile[infectioncontrol]", values.infectioncontrol);
      data.append("staffprofile[infection_control_date]", values.infection_control_date || infection_control_date || "");
      data.append("staffprofile[foodsafety]", values.foodsafety);
      data.append("staffprofile[food_safety_date]", values.food_safety_date || food_safety_date || "");
      data.append("staffprofile[movinghandling]", values.movinghandling);
      data.append("staffprofile[moving_handling_date]", values.moving_handling_date || moving_handling_date || "");
      data.append("staffprofile[basiclifesupport]", values.basiclifesupport);
      data.append("staffprofile[basiclife_support_date]", values.basiclife_support_date || basiclife_support_date || "");
      data.append("staffprofile[dataprotection]", values.dataprotection);
      data.append("staffprofile[data_protection_date]", values.data_protection_date || data_protection_date || "");
      data.append("staffprofile[equalityinclusion]", values.equalityinclusion);
      data.append("staffprofile[equalityinclusion_date]", values.equalityinclusion_date || equalityinclusion_date || "");
      data.append("staffprofile[safeguardingvulnerable]", values.safeguardingvulnerable);
      data.append("staffprofile[safeguardingvulnerable_date]", values.safeguardingvulnerable_date || safeguardingvulnerable_date || "");
      data.append("staffprofile[dataprotection]", values.dataprotection);
      data.append("staffprofile[conflictmanagement]", values.conflictmanagement);
      data.append("staffprofile[conflict_mgn_date]", values.conflict_mgn_date || conflict_mgn_date || "");
      data.append("staffprofile[loneworkertraining]", values.loneworkertraining);
      data.append("staffprofile[loneworkertraining_date]", values.loneworkertraining_date || loneworkertraining_date || "");
      data.append("staffprofile[challengingbehaviour]", values.challengingbehaviour);
      data.append("staffprofile[challenging_behaviour_date]", values.challenging_behaviour_date || challenging_behaviour_date || "");
      data.append("staffprofile[restrainttraining]", values.restrainttraining);
      data.append("staffprofile[restraint_trg_date]", values.restraint_trg_date || restraint_trg_date || "");
      data.append("staffprofile[mentalcapacity]", values.mentalcapacity);
      data.append("staffprofile[mental_capacity_date]", values.mental_capacity_date || mental_capacity_date || "");
      data.append("staffprofile[staff_language]", values.staff_language);
      data.append("staffprofile[staff_religion]", values.staff_religion);
      data.append("staffprofile[staff_work_experience]", values.staff_work_experience);
      request({
        url: "/administrators/employee/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  saveuser = () => {
    return (
      this.form && this.form.reset(),
      this.setState({
        number: "",
        address: ""
      }),
      this.props.history.push("/agency/employeelist"),
      this.flash.adminsavenodify()
    );
  };
  selectChange = value => {
    this.setState({ locations: value });
  };
  availablestatusChangesessun = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivesun: e.target.checked,
      sunavailablestatus: value
    });
  };
  selectstarttimeChange = selecttime => value => {
    if (selecttime === "sunstarttime") {
      if (value) {
        this.setState({ sunstarttime: value.value });
      }
    }
    if (selecttime === "sunendtime") {
      if (value) {
        this.setState({ sunendtime: value.value });
      }
    }
    if (selecttime === "monstarttime") {
      if (value) {
        this.setState({ monstarttime: value.value });
      }
    }
    if (selecttime === "monendtime") {
      if (value) {
        this.setState({ monendtime: value.value });
      }
    }
    if (selecttime === "tuestarttime") {
      if (value) {
        this.setState({ tuestarttime: value.value });
      }
    }
    if (selecttime === "tueendtime") {
      if (value) {
        this.setState({ tueendtime: value.value });
      }
    }
    if (selecttime === "wedstarttime") {
      if (value) {
        this.setState({ wedstarttime: value.value });
      }
    }
    if (selecttime === "wedendtime") {
      if (value) {
        this.setState({ wedendtime: value.value });
      }
    }
    if (selecttime === "thustarttime") {
      if (value) {
        this.setState({ thustarttime: value.value });
      }
    }
    if (selecttime === "thuendtime") {
      if (value) {
        this.setState({ thuendtime: value.value });
      }
    }
    if (selecttime === "fristarttime") {
      if (value) {
        this.setState({ fristarttime: value.value });
      }
    }
    if (selecttime === "friendtime") {
      if (value) {
        this.setState({ friendtime: value.value });
      }
    }
    if (selecttime === "satstarttime") {
      if (value) {
        this.setState({ satstarttime: value.value });
      }
    }
    if (selecttime === "satendtime") {
      if (value) {
        this.setState({ satendtime: value.value });
      }
    }
    if (selecttime === "unavailablefrom") {
      if (value) {
        this.setState({ unavailablefrom: value.value });
      }
    }
    if (selecttime === "unavailableto") {
      if (value) {
        this.setState({ unavailableto: value.value });
      }
    }
  };
  ApplytoallAvailability = () => {
    if (this.state.stausactivesun === "" && this.state.sunstarttime === "" && this.state.sunendtime === "") {
      toast.error("Please fill all fields");
    } else {
      this.setState({
        monstarttime: this.state.sunstarttime,
        tuestarttime: this.state.sunstarttime,
        wedstarttime: this.state.sunstarttime,
        thustarttime: this.state.sunstarttime,
        fristarttime: this.state.sunstarttime,
        satstarttime: this.state.sunstarttime,
        monendtime: this.state.sunendtime,
        tueendtime: this.state.sunendtime,
        wedendtime: this.state.sunendtime,
        thuendtime: this.state.sunendtime,
        friendtime: this.state.sunendtime,
        satendtime: this.state.sunendtime,
        stausactivemon: this.state.stausactivesun,
        monavailablestatus: this.state.sunavailablestatus,
        stausactivetue: this.state.stausactivesun,
        tueavailablestatus: this.state.sunavailablestatus,
        stausactivewed: this.state.stausactivesun,
        wedavailablestatus: this.state.sunavailablestatus,
        stausactivethu: this.state.stausactivesun,
        thuavailablestatus: this.state.sunavailablestatus,
        stausactivefri: this.state.stausactivesun,
        friavailablestatus: this.state.sunavailablestatus,
        stausactivesat: this.state.stausactivesun,
        satavailablestatus: this.state.sunavailablestatus
      });
      toast.success("Applied");
    }
  };
  jobTypeModel = () => {
    this.setState({
      jobmodel: !this.state.jobmodel
    });
  };
  selectboxValidation = () => {
    if (this.state.locations === "") {
      this.setState({
        location_error: true
      });
    }
    if (this.state.job_type === "") {
      this.setState({
        job_role_error: true
      });
    }
  };
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  handleInvalidSubmit(event, errors, values) {
    toast.error("Please Fill Required Fields");
  }
  availabletoggle = tab => {
    if (this.state.availableactiveTab !== tab) {
      this.setState({
        availableactiveTab: tab
      });
    }
  };
  render() {
    const {
      // staff_name,
      // staff_preffered_name,
      // staff_position,
      // staff_photo,
      staff_grade,
      staff_dateofbirth,
      staff_gender,
      staff_pinno,
      staff_nmc_expiry,
      staff_dbs_certificate,
      staff_dbs_issue_date,
      staff_disclosure_name,
      staff_online_status,
      staff_workinuk,
      staff_skill_check,
      staff_reference,
      staff_qualification_verify,
      staff_face_interview,
      education_qualification,
      healthsafety,
      health_safety_date,
      coshh,
      coshh_end_date,
      firesafety,
      fire_safety_date,
      infectioncontrol,
      infection_control_date,
      foodsafety,
      food_safety_date,
      movinghandling,
      moving_handling_date,
      basiclifesupport,
      basiclife_support_date,
      dataprotection,
      data_protection_date,
      equalityinclusion,
      equalityinclusion_date,
      safeguardingvulnerable,
      safeguardingvulnerable_date,
      conflictmanagement,
      conflict_mgn_date,
      loneworkertraining,
      loneworkertraining_date,
      challengingbehaviour,
      challenging_behaviour_date,
      restrainttraining,
      restraint_trg_date,
      mentalcapacity,
      mental_capacity_date,
      staff_language,
      staff_religion,
      staff_work_experience
    } = this.state && this.state.staffprofile ? { ...this.state.staffprofile, ...this.state } : {};
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              {(this.state.agenydashboard.employees || 0) < this.state.employee_count ? (
                <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                  <CardHeader>
                    <strong>Add</strong> Employee
                  </CardHeader>
                  <CardBody className="p-0">
                    <Nav tabs>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === "1" })}
                          onClick={() => {
                            this.toggle("1");
                          }}
                        >
                          Staff Profile
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === "2" })}
                          onClick={() => {
                            this.toggle("2");
                          }}
                        >
                          Availability
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === "3" })}
                          onClick={() => {
                            this.toggle("3");
                          }}
                        >
                          Login Details
                        </NavLink>
                      </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="1">
                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">Basic Details</p>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>First Name</Label>
                                <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Surname</Label>
                                <AvInput type="text" name="surname" placeholder="Enter Surname" onChange={this.onChange} value={this.state.surname} required autoComplete="surname" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Email</Label>
                                <AvInput name="email" type="email" placeholder="Enter email" onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4" className={"for-phone-right"}>
                              <Label className={this.state.phoneerror ? "error-color" : null}>Phone</Label>
                              <IntlTelInput
                                style={{ width: "100%" }}
                                defaultCountry={"gb"}
                                utilsScript={libphonenumber}
                                css={this.state.phoneerror ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                onPhoneNumberChange={this.handler}
                                value={this.state.number}
                              />
                              {this.state.phoneerror ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                            </Col>

                            {/* <Col xs="6" md="4">
                        <Label for="exampleCustomFileBrowser">Profile Image</Label>
                        <CustomInput
                          type="file"
                          id="exampleCustomFileBrowser"
                          name="avatar"
                          onChange={this.fileChangedHandler}
                          label={this.state.avatarName ? this.state.avatarName : "Upload  Image"}
                          encType="multipart/form-data"
                        />
                      </Col>
                      {this.state.avatarfile ? (
                        <Col xs="6" md="4">
                          <img className="prof-image img-fluid" width="180px" height="140px" src={this.state.avatarfile} alt="Preview" />
                        </Col>
                      ) : null}*/}

                            <Col xs="6" md="4">
                              <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                            </Col>
                            {this.state.propreview ? (
                              <Col xs="6" md="3">
                                <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                              </Col>
                            ) : null}
                          </div>
                        </Row>

                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">Address</p>
                            </Col>
                            <Col xs="12" md="4">
                              <label>Search address</label>
                              <PlacesAutocomplete value={this.state.address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                                {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                                  <div>
                                    <input
                                      {...getInputProps({
                                        placeholder: "Search Places ...",
                                        className: "form-control"
                                      })}
                                    />
                                    <div className="autocomplete-dropdown-container absolute">
                                      {suggestions.map(suggestion => {
                                        const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                        // inline style for demonstration purpose
                                        const style = suggestion.active
                                          ? {
                                              backgroundColor: "#fafafa",
                                              cursor: "pointer"
                                            }
                                          : {
                                              backgroundColor: "#ffffff",
                                              cursor: "pointer"
                                            };
                                        return (
                                          <div
                                            {...getSuggestionItemProps(suggestion, {
                                              className,
                                              style
                                            })}
                                          >
                                            <span>{suggestion.description}</span>
                                          </div>
                                        );
                                      })}
                                    </div>
                                  </div>
                                )}
                              </PlacesAutocomplete>
                            </Col>

                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Line1</Label>
                                <AvInput type="text" name="line1" placeholder="Enter line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>City/Town</Label>
                                <AvInput type="text" name="line2" placeholder="Enter City/Town" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            {/* <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City</Label>
                        <AvInput
                          type="text"
                          name="city"
                          placeholder="Enter city.."
                          onChange={this.onChange}
                          value={this.state.city}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col> */}

                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>State/Region</Label>
                                <AvInput type="text" name="state" placeholder="Enter state" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Country</Label>
                                <AvInput type="text" name="country" placeholder="Enter country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Post Code</Label>
                                <AvInput type="text" name="zipcode" placeholder="Enter zipcode" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                          </div>
                        </Row>

                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">Work Details</p>
                            </Col>
                            <Col xs="12" md="4">
                              <Label className={this.state.location_error ? "error-color" : ""}>Area</Label>
                              <Select name="locations" className={this.state.location_error ? "custom_select_validation" : null} value={this.state.locations} options={this.state.worklistselect} onChange={this.selectChange} multi />
                              {/* <small className="ml-1 cursor-pointer" onClick={this.worklocationModel}>
                          {" "}
                          <i className="fa fa-info-circle" />
                          <span className="ml-1 cursor-pointer cm-strong">Add New Work Location</span>
                        </small> */}
                              {this.state.location_error ? <div className="error-color"> This is required!</div> : null}
                            </Col>
                            <Col xs="12" md="4">
                              <Label className={this.state.job_role_error ? "error-color" : ""}>Job Role</Label>
                              <Select name="job_type" className={this.state.job_role_error ? "custom_select_validation" : null} value={this.state.job_type} options={this.state.jobtypelistselect} onChange={this.saveChanges} multi />
                              <small className="ml-1 cursor-pointer">
                                {" "}
                                <i className="fa fa-info-circle" />
                                <span className="ml-1 cursor-pointer cm-strong">If No Job Roles Available Please Add From Settings - Job Roles </span>
                              </small>
                              {this.state.job_role_error ? <div className="error-color"> This is required!</div> : null}
                            </Col>
                            {/* <Col xs="12" md="4">
                        <AvGroup>
                          <Label>Holiday Allowance (£)</Label>
                          <AvInput
                            type="text"
                            name="holiday_allowance"
                            placeholder="Enter holiday allowance"
                            onChange={this.onChange}
                            value={this.state.holiday_allowance}
                            required
                            autoComplete="holiday_allowance"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col> */}
                            <Col xs="12" md="4">
                              <label className={this.state.joindateError ? "error-color" : null}>Joining Date</label>
                              <SingleDatePicker
                                required={true}
                                id="joining_date"
                                date={this.state.joining_date}
                                onDateChange={joining_date => this.joinDate(joining_date)}
                                focused={this.state.focused2}
                                onFocusChange={({ focused: focused2 }) => this.setState({ focused2 })}
                                displayFormat="DD-MM-YYYY"
                                isOutsideRange={() => false}
                              />{" "}
                              {this.state.joindateError ? <div className="error-color"> Enter Valid Date!</div> : null}
                            </Col>
                          </div>
                        </Row>

                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">Personal Details</p>
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Grade</Label>
                                <AvInput type="text" name="staff_grade" onChange={this.onChange} value={staff_grade} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <label>DOB</label>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={moment(staff_dateofbirth)}
                                inputProps={{ placeholder: "Pick Date" }}
                                isValidDate={(current) => current.isBefore(moment(new Date()).subtract(18, "years"))}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ staff_dateofbirth: e });
                                  } else if (!e) {
                                    this.setState({ staff_dateofbirth: null });
                                  } else {
                                    this.setState({ staff_dateofbirth: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="4">
                              <AvField className="form-control fname" type="select" name="staff_gender" label="Gender" placeholder="Please Select" onChange={this.onChange} value={staff_gender} required>
                                <option>Please Select</option>
                                <option value={"male"}>Male</option>
                                <option value={"female"}>Female</option>
                              </AvField>
                            </Col>
                          </div>
                        </Row>

                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">NMC Details</p>
                            </Col>
                            <Col xs="12" md="4">
                              <AvField name="staff_pinno" label="PIN No:" onChange={this.onChange} value={staff_pinno} validate={{ required: { value: true, errorMessage: "This is required!" } }} autoComplete="email" />
                            </Col>
                            <Col xs="12" md="4">
                              <label>Expiry Date</label>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={staff_nmc_expiry}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ staff_nmc_expiry: e });
                                  } else if (!e) {
                                    this.setState({ staff_nmc_expiry: null });
                                  } else {
                                    this.setState({ staff_nmc_expiry: null });
                                  }
                                }}
                              />
                            </Col>
                          </div>
                        </Row>

                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">DBS Details</p>
                            </Col>
                            <Col xs="12" md="4">
                              <AvField
                                name="staff_dbs_certificate"
                                label="Certificate No:"
                                onChange={this.onChange}
                                value={staff_dbs_certificate}
                                validate={{ required: { value: true, errorMessage: "This is required!" } }}
                                autoComplete="email"
                              />
                            </Col>
                            <Col xs="12" md="4">
                              <label>Issue Date</label>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={staff_dbs_issue_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ staff_dbs_issue_date: e });
                                  } else if (!e) {
                                    this.setState({ staff_dbs_issue_date: null });
                                  } else {
                                    this.setState({ staff_dbs_issue_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Disclosure Employer Name: </Label>
                                <AvInput type="text" name="staff_disclosure_name" onChange={this.onChange} value={staff_disclosure_name} required autoComplete="off" />
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvRadioGroup inline label={"Online Check Status"} name="staff_online_status" value={staff_online_status} required>
                                <AvRadio label="Yes" value="yes" />
                                <AvRadio label="No" value="no" />
                              </AvRadioGroup>
                            </Col>
                          </div>
                        </Row>

                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">Checks & Verification</p>
                            </Col>
                            <Col xs="12" md="4">
                              <AvRadioGroup inline label={"Right to Work in UK"} name="staff_workinuk" value={staff_workinuk} required>
                                <AvRadio label="Yes" value="yes" />
                                <AvRadio label="No" value="no" />
                              </AvRadioGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvRadioGroup inline label={"Skills Check"} name="staff_skill_check" value={staff_skill_check} required>
                                <AvRadio label="Yes" value="yes" />
                                <AvRadio label="No" value="no" />
                              </AvRadioGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvRadioGroup inline label={"2 Reference on file"} name="staff_reference" value={staff_reference} required>
                                <AvRadio label="Yes" value="yes" />
                                <AvRadio label="No" value="no" />
                              </AvRadioGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvRadioGroup inline label={"Qualification Verified"} name="staff_qualification_verify" value={staff_qualification_verify} required>
                                <AvRadio label="Yes" value="yes" />
                                <AvRadio label="No" value="no" />
                              </AvRadioGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <AvRadioGroup inline label={"Face to Face Interview"} name="staff_face_interview" value={staff_face_interview} required>
                                <AvRadio label="Yes" value="yes" />
                                <AvRadio label="No" value="no" />
                              </AvRadioGroup>
                            </Col>

                            <Col xs="12" md="12">
                              <AvField
                                name="education_qualification"
                                type="textarea"
                                label="Education/Qualifications"
                                onChange={this.onChange}
                                value={education_qualification}
                                validate={{ required: { value: true, errorMessage: "This is required!" } }}
                                autoComplete="email"
                              />
                            </Col>
                          </div>
                        </Row>

                        <Row className=" custom_single_date">
                          <div className="cus-design edited-section">
                            <Col xs="12" md="12">
                              <p className="h5">Mandatory training</p>
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="healthsafety" onChange={this.onChange} checked={healthsafety === ("true" || true) ? "true" : false} /> <span className="int-pad"> Health and Safety </span>{" "}
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={health_safety_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ health_safety_date: e });
                                  } else if (!e) {
                                    this.setState({ health_safety_date: null });
                                  } else {
                                    this.setState({ health_safety_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="coshh" onChange={this.onChange} checked={coshh} /> <span className="int-pad"> COSHH </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={coshh_end_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ coshh_end_date: e });
                                  } else if (!e) {
                                    this.setState({ coshh_end_date: null });
                                  } else {
                                    this.setState({ coshh_end_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="firesafety" onChange={this.onChange} checked={firesafety} />
                              <span className="int-pad"> Fire Safety </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={fire_safety_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ fire_safety_date: e });
                                  } else if (!e) {
                                    this.setState({ fire_safety_date: null });
                                  } else {
                                    this.setState({ fire_safety_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="infectioncontrol" onChange={this.onChange} checked={infectioncontrol} />
                              <span className="int-pad"> Infection Control </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={infection_control_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ infection_control_date: e });
                                  } else if (!e) {
                                    this.setState({ infection_control_date: null });
                                  } else {
                                    this.setState({ infection_control_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="foodsafety" onChange={this.onChange} checked={foodsafety} />
                              <span className="int-pad"> Food Safety & Nutrition </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={food_safety_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ food_safety_date: e });
                                  } else if (!e) {
                                    this.setState({ food_safety_date: null });
                                  } else {
                                    this.setState({ food_safety_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="movinghandling" onChange={this.onChange} checked={movinghandling} />
                              <span className="int-pad"> Moving & Handling </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={moving_handling_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ moving_handling_date: e });
                                  } else if (!e) {
                                    this.setState({ moving_handling_date: null });
                                  } else {
                                    this.setState({ moving_handling_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="basiclifesupport" onChange={this.onChange} checked={basiclifesupport} />
                              <span className="int-pad"> Basic life support </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={basiclife_support_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ basiclife_support_date: e });
                                  } else if (!e) {
                                    this.setState({ basiclife_support_date: null });
                                  } else {
                                    this.setState({ basiclife_support_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="dataprotection" onChange={this.onChange} checked={dataprotection} />
                              <span className="int-pad"> Data Protection </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={data_protection_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ data_protection_date: e });
                                  } else if (!e) {
                                    this.setState({ data_protection_date: null });
                                  } else {
                                    this.setState({ data_protection_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="equalityinclusion" onChange={this.onChange} checked={equalityinclusion} />
                              <span className="int-pad"> Equality & Inclusion </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={equalityinclusion_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ equalityinclusion_date: e });
                                  } else if (!e) {
                                    this.setState({ equalityinclusion_date: null });
                                  } else {
                                    this.setState({ equalityinclusion_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="safeguardingvulnerable" onChange={this.onChange} checked={safeguardingvulnerable} />
                              <span className="int-pad"> Safeguarding Vulnerable Adults & Children </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={safeguardingvulnerable_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ safeguardingvulnerable_date: e });
                                  } else if (!e) {
                                    this.setState({ safeguardingvulnerable_date: null });
                                  } else {
                                    this.setState({ safeguardingvulnerable_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="conflictmanagement" onChange={this.onChange} checked={conflictmanagement} />
                              <span className="int-pad"> Conflict Management </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={conflict_mgn_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ conflict_mgn_date: e });
                                  } else if (!e) {
                                    this.setState({ conflict_mgn_date: null });
                                  } else {
                                    this.setState({ conflict_mgn_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="loneworkertraining" onChange={this.onChange} checked={loneworkertraining} /> <span className="int-pad"> Lone Worker Training</span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={loneworkertraining_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ loneworkertraining_date: e });
                                  } else if (!e) {
                                    this.setState({ loneworkertraining_date: null });
                                  } else {
                                    this.setState({ loneworkertraining_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="challengingbehaviour" onChange={this.onChange} checked={challengingbehaviour} />
                              <span className="int-pad"> Managing Challenging Behaviour </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={challenging_behaviour_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ challenging_behaviour_date: e });
                                  } else if (!e) {
                                    this.setState({ challenging_behaviour_date: null });
                                  } else {
                                    this.setState({ challenging_behaviour_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="restrainttraining" onChange={this.onChange} checked={restrainttraining} />
                              <span className="int-pad"> Restraint Training </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={restraint_trg_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ restraint_trg_date: e });
                                  } else if (!e) {
                                    this.setState({ restraint_trg_date: null });
                                  } else {
                                    this.setState({ restraint_trg_date: null });
                                  }
                                }}
                              />
                            </Col>
                            <Col xs="12" md="6">
                              <AvInput type="checkbox" name="mentalcapacity" onChange={this.onChange} checked={mentalcapacity} />
                              <span className="int-pad"> Mental Capacity Act 2005 </span>
                              <Datetime
                                dateFormat="DD-MM-YYYY"
                                timeFormat={false}
                                closeOnSelect={true}
                                value={mental_capacity_date}
                                inputProps={{ placeholder: "Pick Date" }}
                                onChange={e => {
                                  if (e._isValid) {
                                    this.setState({ mental_capacity_date: e });
                                  } else if (!e) {
                                    this.setState({ mental_capacity_date: null });
                                  } else {
                                    this.setState({ mental_capacity_date: null });
                                  }
                                }}
                              />
                            </Col>
                          </div>
                        </Row>
                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="4">
                              <AvField type="text" label="Languages Spoken" name="staff_language" onChange={this.onChange} value={staff_language} />
                            </Col>
                            <Col xs="12" md="4">
                              <AvField type="text" label="Religion" name="staff_religion" onChange={this.onChange} value={staff_religion} />
                            </Col>
                            <Col xs="12" md="4">
                              <AvField type="text" label="Work Experience" name="staff_work_experience" onChange={this.onChange} value={staff_work_experience} />
                            </Col>
                          </div>
                        </Row>
                        {/* <Row>
                      <Col xs="12" md="4">
                        <AvGroup>
                          <Label>Employee Rate (£)</Label>
                          <AvInput
                            type="number"
                            name="employee_rate"
                            placeholder="Enter Employee Rate"
                            onChange={this.onChange}
                            value={this.state.employee_rate}
                            required
                            autoComplete="employee_rate"
                            min="0"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="4">
                        <AvGroup>
                          <Label>Agency Rate (£)</Label>
                          <AvInput
                            type="number"
                            name="agency_rate"
                            placeholder="Enter Agency Rate"
                            onChange={this.onChange}
                            value={this.state.agency_rate}
                            required
                            autoComplete="agency_rate"
                            min="0"
                          />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Client Rate (£)</Label>
                        {this.state.agency_rate & this.state.employee_rate ? (
                          <p>
                            <strong>{parseInt(this.state.employee_rate, Number) + parseInt(this.state.agency_rate, Number)}</strong>
                          </p>
                        ) : null}
                      </Col>
                    </Row> */}
                        <Row>
                          {/* <Col xs="12" md="4">
                        <label className={this.state.finaldateError ? "error-color" : null}>Final Working Date</label>
                        <SingleDatePicker
                          className={this.state.finaldateError ? "error-color" : null}
                          date={this.state.final_date}
                          isOutsideRange={day => day.isBefore(this.state.joining_date)}
                          onDateChange={final_date => this.finalDate(final_date)}
                          focused={this.state.focused1}
                          onFocusChange={({ focused: focused1 }) => this.setState({ focused1 })}
                          id="final_date"
                          displayFormat="DD-MM-YYYY"
                        />{" "}
                        {this.state.finaldateError ? <div className="error-color"> Enter Valid Date!</div> : null}
                      </Col>*/}
                        </Row>
                        <br />
                      </TabPane>
                      <TabPane tabId="2">
                        <Row>
                          <Nav tabs className="days-calender">
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "1"
                                })}
                                onClick={() => {
                                  this.availabletoggle("1");
                                }}
                              >
                                {" "}
                                Sun
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "2"
                                })}
                                onClick={() => {
                                  this.availabletoggle("2");
                                }}
                              >
                                Mon
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "3"
                                })}
                                onClick={() => {
                                  this.availabletoggle("3");
                                }}
                              >
                                Tue
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "4"
                                })}
                                onClick={() => {
                                  this.availabletoggle("4");
                                }}
                              >
                                Wed
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "5"
                                })}
                                onClick={() => {
                                  this.availabletoggle("5");
                                }}
                              >
                                Thu
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "6"
                                })}
                                onClick={() => {
                                  this.availabletoggle("6");
                                }}
                              >
                                Fri
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.availableactiveTab === "7"
                                })}
                                onClick={() => {
                                  this.availabletoggle("7");
                                }}
                              >
                                Sat
                              </NavLink>
                            </NavItem>
                          </Nav>
                          <TabContent activeTab={this.state.availableactiveTab}>
                            <TabPane tabId="1">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="sunstarttime" value={this.state.sunstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("sunstarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="sunendtime" value={this.state.sunendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("sunendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivesun} onChange={this.availablestatusChangesessun} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                  <Col xs="12" md="2" className="var-values">
                                    <button type="button" className="btn btn-add" onClick={this.ApplytoallAvailability}>
                                      Apply to all
                                    </button>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                            <TabPane tabId="2">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="monstarttime" value={this.state.monstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("monstarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="monendtime" value={this.state.monendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("monendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivemon} onChange={this.availablestatusChangesesmon} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                            <TabPane tabId="3">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="tuestarttime" value={this.state.tuestarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("tuestarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="tueendtime" value={this.state.tueendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("tueendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivetue} onChange={this.availablestatusChangesestue} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                            <TabPane tabId="4">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="wedstarttime" value={this.state.wedstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("wedstarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="wedendtime" value={this.state.wedendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("wedendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivewed} onChange={this.availablestatusChangeseswed} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                            <TabPane tabId="5">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="thustarttime" value={this.state.thustarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("thustarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="thuendtime" value={this.state.thuendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("thuendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivethu} onChange={this.availablestatusChangesesthu} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                            <TabPane tabId="6">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="fristarttime" value={this.state.fristarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("fristarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="friendtime" value={this.state.friendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("friendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivefri} onChange={this.availablestatusChangesesfri} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                            <TabPane tabId="7">
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <Label for="from">Start Time</Label>
                                    <Select name="satstarttime" value={this.state.satstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("satstarttime")} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="from">End Time</Label>
                                    <Select name="satendtime" value={this.state.satendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("satendtime")} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <div>
                                      <label>Status</label>
                                    </div>
                                    <Label className="switch switch-text switch-success new-switch">
                                      <Input type="checkbox" className="switch-input" checked={this.state.stausactivesat} onChange={this.availablestatusChangesessat} />
                                      <span className="switch-label" data-on="active" data-off="inactive" />
                                      <span className="switch-handle new-handle" />
                                    </Label>
                                  </Col>
                                </div>
                              </Row>
                            </TabPane>
                          </TabContent>
                        </Row>
                      </TabPane>
                      <TabPane tabId="3">
                        <Row>
                          <div className="cus-design edited-section">
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Username</Label>
                                <AvInput type="text" name="username" placeholder="Enter username" onChange={this.onChange} value={this.state.username} required autoComplete="name" />
                                <AvFeedback>This is required!</AvFeedback>
                              </AvGroup>
                            </Col>
                            <Col xs="12" md="4">
                              <Label>Password</Label>
                              <AvInput type="password" name="password" placeholder="Enter password" onChange={this.onChange} value={this.state.password} required autoComplete="off" />
                            </Col>
                            <Col xs="12" md="4">
                              <AvGroup>
                                <Label>Confirm Password</Label>
                                <AvInput
                                  type="password"
                                  name="confirm_password"
                                  placeholder="Enter password"
                                  onChange={this.onChange}
                                  value={this.state.confirm_password}
                                  required
                                  validate={{ match: { value: "password" } }}
                                  autoComplete="off"
                                />
                                <AvFeedback>Match Password!</AvFeedback>
                              </AvGroup>
                            </Col>
                          </div>
                        </Row>
                      </TabPane>
                    </TabContent>
                  </CardBody>
                  <CardFooter>
                    {this.state.activeTab === "3" ? (
                      <button
                        type="submit"
                        className="btn btn-success pull-right mb-3"
                        title="Add Employee"
                        onClick={() => {
                          this.phonefield(this.state.number);
                          this.joinDatefield(this.state.joining_date);
                          this.selectboxValidation();
                        }}
                      >
                        Add
                      </button>
                    ) : null}
                    {this.state.activeTab < "3" ? (
                      <button
                        className="btn btn-info pull-right mb-3"
                        type="button"
                        title="Next"
                        onClick={() => {
                          this.setState({ activeTab: (parseInt(this.state.activeTab) + 1).toString() });
                        }}
                      >
                        Next
                      </button>
                    ) : null}
                    {this.state.activeTab > "1" ? (
                      <button
                        className="btn btn-info pull-left mb-3"
                        type="button"
                        title="Previous"
                        onClick={() => {
                          this.setState({ activeTab: (parseInt(this.state.activeTab) - 1).toString() });
                        }}
                      >
                        Previous
                      </button>
                    ) : null}
                  </CardFooter>
                </AvForm>
              ) : (
                <div className="text-center mt-2 mb-2">
                  <div>
                    <h4 style={subh4}>
                      <p>
                        <strong>Current Subscription Exceeds Employees Limit</strong>
                      </p>
                      <small
                        onClick={() => {
                          return this.props.history.push("/agency/subscribe");
                        }}
                        className={"cursor-pointer"}
                      >
                        Click here to subscribe
                      </small>
                    </h4>
                  </div>
                </div>
              )}
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addemployee;
