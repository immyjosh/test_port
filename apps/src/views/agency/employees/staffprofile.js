/* eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput, AvRadio, AvRadioGroup } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Button, Col, Label, Row, CustomInput } from "reactstrap";
import { NodeURL } from "../../../api/api";
import Datetime from 'react-datetime';
import moment from "moment";

class Staffprofile extends Component {
  state = {

  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  fileChangedHandler = evt => {
    this.setState({
      staff_photo: evt.target.files[0],
      staff_photo_file: URL.createObjectURL(evt.target.files[0]),
      staff_photo_Name: evt.target.files[0].name
    });
  };
  componentDidMount() {
    if (this.props && this.props.staffprofile) {
      const {
        staff_name,
        staff_preffered_name,
        staff_position,
        staff_photo,
        staff_grade,
        staff_dateofbirth,
        staff_gender,
        staff_pinno,
        staff_nmc_expiry,
        staff_dbs_certificate,
        staff_dbs_issue_date,
        staff_disclosure_name,
        staff_online_status,
        staff_workinuk,
        staff_skill_check,
        staff_reference,
        staff_qualification_verify,
        staff_face_interview,
        education_qualification,
        healthsafety,
        health_safety_date,
        coshh,
        coshh_end_date,
        firesafety,
        fire_safety_date,
        infectioncontrol,
        infection_control_date,
        foodsafety,
        food_safety_date,
        movinghandling,
        moving_handling_date,
        basiclifesupport,
        basiclife_support_date,
        dataprotection,
        data_protection_date,
        equalityinclusion,
        equalityinclusion_date,
        safeguardingvulnerable,
        safeguardingvulnerable_date,
        conflictmanagement,
        conflict_mgn_date,
        loneworkertraining,
        loneworkertraining_date,
        challengingbehaviour,
        challenging_behaviour_date,
        restrainttraining,
        restraint_trg_date,
        mentalcapacity,
        mental_capacity_date,
        staff_language,
        staff_religion,
        staff_work_experience
      } = this.props && this.props.staffprofile ? this.props.staffprofile : {};
      this.setState({
        staff_name,
        staff_preffered_name,
        staff_position,
        staff_photo,
        staff_grade,
        staff_dateofbirth,
        staff_gender,
        staff_pinno,
        staff_nmc_expiry,
        staff_dbs_certificate,
        staff_dbs_issue_date,
        staff_disclosure_name,
        staff_online_status,
        staff_workinuk,
        staff_skill_check,
        staff_reference,
        staff_qualification_verify,
        staff_face_interview,
        education_qualification,
        healthsafety,
        health_safety_date,
        coshh,
        coshh_end_date,
        firesafety,
        fire_safety_date,
        infectioncontrol,
        infection_control_date,
        foodsafety,
        food_safety_date,
        movinghandling,
        moving_handling_date,
        basiclifesupport,
        basiclife_support_date,
        dataprotection,
        data_protection_date,
        equalityinclusion,
        equalityinclusion_date,
        safeguardingvulnerable,
        safeguardingvulnerable_date,
        conflictmanagement,
        conflict_mgn_date,
        loneworkertraining,
        loneworkertraining_date,
        challengingbehaviour,
        challenging_behaviour_date,
        restrainttraining,
        restraint_trg_date,
        mentalcapacity,
        mental_capacity_date,
        staff_language,
        staff_religion,
        staff_work_experience
      });
    }
  }
  onEditStaff = (e, values) => {
    this.props.onEditStaff({ ...values, ...this.state });
  };
  render() {
    const {
      staff_name,
      staff_preffered_name,
      staff_position,
      staff_photo,
      staff_grade,
      staff_dateofbirth,
      staff_gender,
      staff_pinno,
      staff_nmc_expiry,
      staff_dbs_certificate,
      staff_dbs_issue_date,
      staff_disclosure_name,
      staff_online_status,
      staff_workinuk,
      staff_skill_check,
      staff_reference,
      staff_qualification_verify,
      staff_face_interview,
      education_qualification,
      healthsafety,
      health_safety_date,
      coshh,
      coshh_end_date,
      firesafety,
      fire_safety_date,
      infectioncontrol,
      infection_control_date,
      foodsafety,
      food_safety_date,
      movinghandling,
      moving_handling_date,
      basiclifesupport,
      basiclife_support_date,
      dataprotection,
      data_protection_date,
      equalityinclusion,
      equalityinclusion_date,
      safeguardingvulnerable,
      safeguardingvulnerable_date,
      conflictmanagement,
      conflict_mgn_date,
      loneworkertraining,
      loneworkertraining_date,
      challengingbehaviour,
      challenging_behaviour_date,
      restrainttraining,
      restraint_trg_date,
      mentalcapacity,
      mental_capacity_date,
      staff_language,
      staff_religion,
      staff_work_experience
    } = this.state ? this.state : {};
    return (
      <div className="animated fadeIn">
        <AvForm onValidSubmit={this.onEditStaff}>
          <Row>
            <Col xs="12" md="4">
              <AvGroup>
                <Label>Name</Label>
                <AvInput type="text" name="staff_name" onChange={this.onChange} value={staff_name} required autoComplete="name" />
                <AvFeedback>This is required!</AvFeedback>
              </AvGroup>
            </Col>
            <Col xs="12" md="4">
              <AvGroup>
                <Label>Preferred Name</Label>
                <AvInput type="text" name="staff_preffered_name" onChange={this.onChange} value={staff_preffered_name} required autoComplete="name" />
                <AvFeedback>This is required!</AvFeedback>
              </AvGroup>
            </Col>
            <Col xs="12" md="4">
              <Label for="exampleCustomFileBrowser">Upload ID Photo</Label>
              <CustomInput type="file" id="exampleCustomFileBrowser" name="staff_photo" onChange={this.fileChangedHandler} label={this.state.staff_photo_Name ? this.state.staff_photo_Name : "Upload  Image"} encType="multipart/form-data" />
            </Col>
            <Col xs="12" md="4">
              <AvGroup>
                <Label>Position/Role</Label>
                <AvInput type="text" name="staff_position" onChange={this.onChange} value={staff_position} required autoComplete="name" />
                <AvFeedback>This is required!</AvFeedback>
              </AvGroup>
            </Col>
            {staff_photo ? (
              <Col xs="6" md="4">
                <img className="img-fluid" src={NodeURL + "/" + staff_photo} alt="Profile" />
              </Col>
            ) : null}
          </Row>
          <Row>
            <Col xs="12" md="4">
              <AvGroup>
                <Label>Grade</Label>
                <AvInput type="text" name="staff_grade" onChange={this.onChange} value={staff_grade} required autoComplete="name" />
                <AvFeedback>This is required!</AvFeedback>
              </AvGroup>
            </Col>
            <Col xs="12" md="4">
              <label>DOB</label>
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {moment(staff_dateofbirth)}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ staff_dateofbirth: e });
                    } else if (!e) {
                      this.setState({ staff_dateofbirth: null });
                    } else {
                      this.setState({ staff_dateofbirth: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="4">
              <AvGroup>
                <Label>Gender</Label>
                <AvInput type="text" name="staff_gender" onChange={this.onChange} value={staff_gender} required autoComplete="name" />
                <AvFeedback>This is required!</AvFeedback>
              </AvGroup>
            </Col>
          </Row>
          <p className="h5">NMC Details</p>
          <Row>
            <Col xs="12" md="4">
              <AvField name="staff_pinno" label="PIN No:" onChange={this.onChange} value={staff_pinno} validate={{ required: { value: true, errorMessage: "This is required!" } }} autoComplete="email" />
            </Col>
            <Col xs="12" md="4">
              <label>Expiry Date</label>
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {moment(staff_nmc_expiry)}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ staff_nmc_expiry: e });
                    } else if (!e) {
                      this.setState({ staff_nmc_expiry: null });
                    } else {
                      this.setState({ staff_nmc_expiry: null });
                    }
                  }}
              />
            </Col>
          </Row>
          <p className="h5">DBS Details</p>
          <Row>
            <Col xs="12" md="4">
              <AvField name="staff_dbs_certificate" label="Certificate No:" onChange={this.onChange} value={staff_dbs_certificate} validate={{ required: { value: true, errorMessage: "This is required!" } }} autoComplete="email" />
            </Col>
            <Col xs="12" md="4">
              <label>Issue Date</label>
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {moment(staff_dbs_issue_date)}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ staff_dbs_issue_date: e });
                    } else if (!e) {
                      this.setState({ staff_dbs_issue_date: null });
                    } else {
                      this.setState({ staff_dbs_issue_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="4">
              <AvGroup>
                <Label>Disclosure Employer Name: </Label>
                <AvInput type="text" name="staff_disclosure_name" onChange={this.onChange} value={staff_disclosure_name} required autoComplete="off" />
              </AvGroup>
            </Col>
            <Col xs="12" md="4">
              <AvRadioGroup inline label={"Online Check Status"} name="staff_online_status" value={staff_online_status} required>
                <AvRadio label="Yes" value="yes" />
                <AvRadio label="No" value="no" />
              </AvRadioGroup>
            </Col>
          </Row>
          <p className="h5">Checks & Verification</p>
          <Row>
            <Col xs="12" md="4">
              <AvRadioGroup inline label={"Right to Work in UK"} name="staff_workinuk" value={staff_workinuk} required>
                <AvRadio label="Yes" value="yes" />
                <AvRadio label="No" value="no" />
              </AvRadioGroup>
            </Col>
            <Col xs="12" md="4">
              <AvRadioGroup inline label={"Skills Check"} name="staff_skill_check" value={staff_skill_check} required>
                <AvRadio label="Yes" value="yes" />
                <AvRadio label="No" value="no" />
              </AvRadioGroup>
            </Col>
            <Col xs="12" md="4">
              <AvRadioGroup inline label={"2 Reference on file"} name="staff_reference" value={staff_reference} required>
                <AvRadio label="Yes" value="yes" />
                <AvRadio label="No" value="no" />
              </AvRadioGroup>
            </Col>
            <Col xs="12" md="4">
              <AvRadioGroup inline label={"Qualification Verified"} name="staff_qualification_verify" value={staff_qualification_verify} required>
                <AvRadio label="Yes" value="yes" />
                <AvRadio label="No" value="no" />
              </AvRadioGroup>
            </Col>
            <Col xs="12" md="4">
              <AvRadioGroup inline label={"Face to Face Interview"} name="staff_face_interview" value={staff_face_interview} required>
                <AvRadio label="Yes" value="yes" />
                <AvRadio label="No" value="no" />
              </AvRadioGroup>
            </Col>
          </Row>
          <Row>
            <Col xs="12" md="12">
              <AvField
                name="education_qualification"
                type="textarea"
                label="Education/Qualifications"
                onChange={this.onChange}
                value={education_qualification}
                validate={{ required: { value: true, errorMessage: "This is required!" } }}
                autoComplete="email"
              />
            </Col>
          </Row>
          <p className="h5">Mandatory training</p>
          <Row className="ml-3 custom_single_date">
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="healthsafety" onChange={this.onChange} checked={healthsafety} /> Health and Safety{" "}
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {moment(health_safety_date)}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ health_safety_date: e });
                    } else if (!e) {
                      this.setState({ health_safety_date: null });
                    } else {
                      this.setState({ health_safety_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="coshh" onChange={this.onChange} checked={coshh} /> COSHH
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {coshh_end_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ coshh_end_date: e });
                    } else if (!e) {
                      this.setState({ coshh_end_date: null });
                    } else {
                      this.setState({ coshh_end_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="firesafety" onChange={this.onChange} checked={firesafety} /> Fire Safety
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {fire_safety_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ fire_safety_date: e });
                    } else if (!e) {
                      this.setState({ fire_safety_date: null });
                    } else {
                      this.setState({ fire_safety_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="infectioncontrol" onChange={this.onChange} checked={infectioncontrol} /> Infection Control
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {infection_control_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ infection_control_date: e });
                    } else if (!e) {
                      this.setState({ infection_control_date: null });
                    } else {
                      this.setState({ infection_control_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="foodsafety" onChange={this.onChange} checked={foodsafety} /> Food Safety & Nutrition
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {food_safety_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ food_safety_date: e });
                    } else if (!e) {
                      this.setState({ food_safety_date: null });
                    } else {
                      this.setState({ food_safety_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="movinghandling" onChange={this.onChange} checked={movinghandling} /> Moving & Handling
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {moving_handling_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ moving_handling_date: e });
                    } else if (!e) {
                      this.setState({ moving_handling_date: null });
                    } else {
                      this.setState({ moving_handling_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="basiclifesupport" onChange={this.onChange} checked={basiclifesupport} /> Basic life support
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {basiclife_support_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ basiclife_support_date: e });
                    } else if (!e) {
                      this.setState({ basiclife_support_date: null });
                    } else {
                      this.setState({ basiclife_support_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="dataprotection" onChange={this.onChange} checked={dataprotection} /> Data Protection
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {data_protection_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ data_protection_date: e });
                    } else if (!e) {
                      this.setState({ data_protection_date: null });
                    } else {
                      this.setState({ data_protection_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="equalityinclusion" onChange={this.onChange} checked={equalityinclusion} /> Equality & Inclusion
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {equalityinclusion_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ equalityinclusion_date: e });
                    } else if (!e) {
                      this.setState({ equalityinclusion_date: null });
                    } else {
                      this.setState({ equalityinclusion_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="safeguardingvulnerable" onChange={this.onChange} checked={safeguardingvulnerable} /> Safeguarding Vulnerable Adults & Children
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {safeguardingvulnerable_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ safeguardingvulnerable_date: e });
                    } else if (!e) {
                      this.setState({ safeguardingvulnerable_date: null });
                    } else {
                      this.setState({ safeguardingvulnerable_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="conflictmanagement" onChange={this.onChange} checked={conflictmanagement} /> Conflict Management
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {conflict_mgn_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ conflict_mgn_date: e });
                    } else if (!e) {
                      this.setState({ conflict_mgn_date: null });
                    } else {
                      this.setState({ conflict_mgn_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="loneworkertraining" onChange={this.onChange} checked={loneworkertraining} /> Lone Worker Training
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {loneworkertraining_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ loneworkertraining_date: e });
                    } else if (!e) {
                      this.setState({ loneworkertraining_date: null });
                    } else {
                      this.setState({ loneworkertraining_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="challengingbehaviour" onChange={this.onChange} checked={challengingbehaviour} /> Managing Challenging Behaviour
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {challenging_behaviour_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ challenging_behaviour_date: e });
                    } else if (!e) {
                      this.setState({ challenging_behaviour_date: null });
                    } else {
                      this.setState({ challenging_behaviour_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="restrainttraining" onChange={this.onChange} checked={restrainttraining} /> Restraint Training
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {restraint_trg_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ restraint_trg_date: e });
                    } else if (!e) {
                      this.setState({ restraint_trg_date: null });
                    } else {
                      this.setState({ restraint_trg_date: null });
                    }
                  }}
              />
            </Col>
            <Col xs="12" md="6">
              <AvInput type="checkbox" name="mentalcapacity" onChange={this.onChange} checked={mentalcapacity} /> Mental Capacity Act 2005
              <Datetime
                  dateFormat="DD-MM-YYYY"
                  timeFormat={false}
                  closeOnSelect={true}
                  value = {mental_capacity_date}
                  inputProps={{ placeholder: 'Pick Date'}}
                  onChange={(e) => {
                    if (e._isValid) {
                      this.setState({ mental_capacity_date: e });
                    } else if (!e) {
                      this.setState({ mental_capacity_date: null });
                    } else {
                      this.setState({ mental_capacity_date: null });
                    }
                  }}
              />
            </Col>
          </Row>
          <Row>
            <Col xs="12" md="4">
              <AvField type="text" label="Languages Spoken" name="staff_language" onChange={this.onChange} value={staff_language} />
            </Col>
            <Col xs="12" md="4">
              <AvField type="text" label="Religion" name="staff_religion" onChange={this.onChange} value={staff_religion} />
            </Col>
            <Col xs="12" md="4">
              <AvField type="text" label="Work Experience" name="staff_work_experience" onChange={this.onChange} value={staff_work_experience} />
            </Col>
          </Row>
          <Button type="submit" size="md" color="primary pull-right" title="Update">
            <i className="fa fa-dot-circle-o" /> Update
          </Button>
        </AvForm>
      </div>
    );
  }
}

export default Staffprofile;
