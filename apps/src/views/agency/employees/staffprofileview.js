/* eslint no-sequences: 0*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Row } from "reactstrap";
import request, { NodeURL } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
// import { Link } from "react-router-dom";
// import Widget02 from "../../Template/Widgets/Widget02";
const moment = require("moment");

class Staffprofileview extends Component {
  state = {
    staffprofile: ""
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/administrators/employee/get",
      method: "POST",
      data: { id: this.props.location.state.rowid, pagefor: "view" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({ staffprofile: res.response.result && res.response.result[0].staffprofile ? res.response.result[0].staffprofile : "" });
      } else if (res.status === 1) {
        toast.error(res.response);
      }
    });
  }

  render() {
    const {
      staff_name,
      staff_preffered_name,
      staff_position,
      staff_photo,
      staff_grade,
      staff_dateofbirth,
      staff_gender,
      staff_pinno,
      staff_nmc_expiry,
      staff_dbs_certificate,
      staff_dbs_issue_date,
      staff_disclosure_name,
      staff_online_status,
      staff_workinuk,
      staff_skill_check,
      staff_reference,
      staff_qualification_verify,
      staff_face_interview,
      education_qualification,
      healthsafety,
      health_safety_date,
      coshh,
      coshh_end_date,
      firesafety,
      fire_safety_date,
      infectioncontrol,
      infection_control_date,
      foodsafety,
      food_safety_date,
      movinghandling,
      moving_handling_date,
      basiclifesupport,
      basiclife_support_date,
      dataprotection,
      data_protection_date,
      equalityinclusion,
      equalityinclusion_date,
      safeguardingvulnerable,
      safeguardingvulnerable_date,
      conflictmanagement,
      conflict_mgn_date,
      loneworkertraining,
      loneworkertraining_date,
      challengingbehaviour,
      challenging_behaviour_date,
      restrainttraining,
      restraint_trg_date,
      mentalcapacity,
      mental_capacity_date,
      staff_language,
      staff_religion,
      staff_work_experience
    } = this.state && this.state.staffprofile ? this.state.staffprofile : {};
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              <CardHeader>
                <strong>View</strong> Details
                <div className="card-actions" />
              </CardHeader>
              <CardBody>
                <h4>Staff Profile</h4>
                <Row>
                  <div className="col-md-12">
                    <div className="cate-ranges">
                      <h4>Basic</h4>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="cate-ranges">{staff_photo ? <img className="img-fluid" src={NodeURL + "/" + staff_photo} alt="Profile" /> : null}</div>
                  </div>

                  <div className="col-md-6">
                    <div className="cate-ranges">
                      <p>
                        <span className="left-views">
                          Name <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_name}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Preferred Name <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_preffered_name}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Position/Role <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_position}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Grade <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_grade}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          DOB <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(staff_dateofbirth).format("DD-MM-YYYY")}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Gender <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_gender}</span>
                      </p>
                    </div>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-6">
                    <div className="cate-ranges">
                      <h4>NMC Details</h4>
                      <p>
                        <span className="left-views">
                          PIN No <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_pinno}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Expiry Date <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(staff_nmc_expiry).format("DD-MM-YYYY")}</span>
                      </p>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="cate-ranges">
                      <h4>DBS Details</h4>
                      <p>
                        <span className="left-views">
                          Certificate No
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_dbs_certificate}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Issue Date <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(staff_dbs_issue_date).format("DD-MM-YYYY")}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Disclosure Employer Name <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_disclosure_name}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Online Check Status <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_online_status}</span>
                      </p>
                    </div>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-12">
                    <div className="cate-ranges">
                      <h4>Checks & Verification</h4>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="cate-ranges">
                      <p>
                        <span className="left-views">
                          Right to Work in UK <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_workinuk}</span>
                      </p>

                      <p>
                        <span className="left-views">
                          2 Reference on file <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_reference}</span>
                      </p>

                      <p>
                        <span className="left-views">
                          Face to Face Intervie <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_face_interview}</span>
                      </p>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="cate-ranges">
                      <p>
                        <span className="left-views">
                          Skills Check <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_skill_check}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Qualification Verified <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_qualification_verify}</span>
                      </p>

                      <p>
                        <span className="left-views">
                          Education/Qualifications <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{education_qualification}</span>
                      </p>
                    </div>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-12">
                    <div className="cate-ranges">
                      <h4>Mandatory training</h4>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="cate-ranges">
                      {healthsafety ? (
                        <p>
                          <span className="left-views">
                            Health and Safety <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{health_safety_date && moment(health_safety_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {firesafety ? (
                        <p>
                          <span className="left-views">
                            Fire Safety <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{fire_safety_date && moment(fire_safety_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {foodsafety ? (
                        <p>
                          <span className="left-views">
                            Food Safety & Nutrition <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{food_safety_date && moment(food_safety_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {basiclifesupport ? (
                        <p>
                          <span className="left-views">
                            Basic life support <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{basiclife_support_date && moment(basiclife_support_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {equalityinclusion ? (
                        <p>
                          <span className="left-views">
                            Equality & Inclusion <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{equalityinclusion_date && moment(equalityinclusion_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {conflictmanagement ? (
                        <p>
                          <span className="left-views">
                            Conflict Management <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{conflict_mgn_date && moment(conflict_mgn_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {challengingbehaviour ? (
                        <p>
                          <span className="left-views">
                            Managing Challenging Behaviour <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{challenging_behaviour_date && moment(challenging_behaviour_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {mentalcapacity ? (
                        <p>
                          <span className="left-views">
                            Mental Capacity Act 2005 <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{mental_capacity_date && moment(mental_capacity_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      <p>
                        <span className="left-views">
                          Languages Spoken <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_language}</span>
                      </p>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="cate-ranges">
                      {coshh ? (
                        <p>
                          <span className="left-views">
                            COSHH <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{coshh_end_date && moment(coshh_end_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {infectioncontrol ? (
                        <p>
                          <span className="left-views">
                            Infection Control <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{infection_control_date && moment(infection_control_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {movinghandling ? (
                        <p>
                          <span className="left-views">
                            Moving & Handling <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{moving_handling_date && moment(moving_handling_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {dataprotection ? (
                        <p>
                          <span className="left-views">
                            Data Protection <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{data_protection_date && moment(data_protection_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {safeguardingvulnerable ? (
                        <p>
                          <span className="left-views">
                            Safeguarding Vulnerable Adults & Children <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{safeguardingvulnerable_date && moment(safeguardingvulnerable_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {loneworkertraining ? (
                        <p>
                          <span className="left-views">
                            Lone Worker Training <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{loneworkertraining_date && moment(loneworkertraining_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      {restrainttraining ? (
                        <p>
                          <span className="left-views">
                            Restraint Training <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{restraint_trg_date && moment(restraint_trg_date).format("DD-MM-YYYY")}</span>
                        </p>
                      ) : null}
                      <p>
                        <span className="left-views">
                          Religion <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_religion}</span>
                      </p>

                      <p>
                        <span className="left-views">
                          Work Experience <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{staff_work_experience}</span>
                      </p>
                    </div>
                  </div>
                </Row>
              </CardBody>
              <CardFooter>
                {/*<Link to="/agency/employeelist">*/}
                <Button
                  type="button"
                  color="secondary"
                  title="Back"
                  onClick={() => {
                    window.history.go(-1);
                  }}
                >
                  <i className="fa fa-arrow-left" /> Back
                </Button>
                {/*</Link>*/}
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Staffprofileview;
