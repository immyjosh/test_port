/* eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput, AvRadio, AvRadioGroup } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, TabContent, TabPane, Nav, NavItem, NavLink, Input, Popover, PopoverBody } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { toast, ToastContainer } from "react-toastify";
import classnames from "classnames";
import { timelist } from "../../common/utils";
import { SingleDatePicker } from "react-dates";
import moment from "moment";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import JobtypeListEmployee from "./listjobtypeemployee";
import Avatar from "react-avatar-edit";
import Datetime from "react-datetime";

class Editemployee extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    surname: "",
    jobname: "",
    workname: "",
    email: "",
    status: "",
    isverified: "",
    role: "",
    code: "",
    code1: "",
    job_type: [],
    employee_rate: "",
    staffprofile:"",
    agency_rate: "",
    number: "",
    dailcountry: "",
    number1: "",
    address: "",
    address1: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    zipcode1: "",
    formatted_address: "",
    avatar: "",
    formatted_address1: "",
    phoneerror: false,
    phoneerror1: false,
    stausactive: false,
    isverifiedstaus: false,
    phonestatus: "",
    phonestatus1: "",
    avatar_return: "",
    jobtypelist: [],
    timeoffdata: [],
    activeTab: "1",
    availableactiveTab: "1",
    unavailableactiveTab: "1",
    holiday_allowance: "",
    joining_date: null,
    // final_date: null,
    timelist: [],
    sunstarttime: "",
    sunendtime: "",
    sunavailablestatus: 0,
    monstarttime: "",
    monendtime: "",
    monavailablestatus: 0,
    tuestarttime: "",
    tueendtime: "",
    tueavailablestatus: 0,
    wedstarttime: "",
    wedendtime: "",
    wedavailablestatus: 0,
    thustarttime: "",
    thuendtime: "",
    thuavailablestatus: 0,
    fristarttime: "",
    friendtime: "",
    friavailablestatus: 0,
    satstarttime: "",
    satendtime: "",
    satavailablestatus: 0,
    unavaiablestarttime: "",
    unavaiableendtime: "",
    unavailablestatus: 0,
    adminlist: "",
    avatarName: "",
    worklist: [],
    locations: "",
    dates: [],
    unavailablefrom: "",
    unavailableto: "",
    jobrate: "",
    jobmodel: false,
    workmodel: false,
    worklistselect: [],
    jobtypelistselect: [],
    stausactivesun: false,
    stausactivemon: false,
    stausactivetue: false,
    stausactivewed: false,
    stausactivethu: false,
    stausactivefri: false,
    stausactivesat: false,
    locationsValue: false,
    job_role_error: false,
    location_error: false,
    jobIDC: "",
    RowIDC: "",
    const_job_type: "",
    subscribedids: "",
    popoverDateOpen: false,
    propreview: null,
    staff_dateofbirth: moment(new Date()).subtract(18, "years")
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      timelist,
      RowIDC: this.props.location.state.rowid,
      activeTab: this.props.location && this.props.location.state && this.props.location.state.activetabs ? this.props.location.state.activetabs : "1"
    });
    request({
      url: "/administrators/employee/get",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          const datas = res.response.result[0];
          this.setState({ adminlist: datas });
          const available_data = datas.available && datas.available.length ? datas.available : "";
          const joiningdate = datas.joining_date ? moment(datas.joining_date) : null;
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          let timeoffdata;
          if (datas.timeoff) {
            timeoffdata = datas.timeoff.map((time, key) => {
              const timefromhr = time.from / 3600;
              const timefromsplithr = timefromhr.toString().split(".");
              const timefromsec = timefromsplithr[1];
              const timefrommins = timefromsec === undefined ? "00" : (+"0" + "." + timefromsec) * 60;
              const timehr = num.indexOf(timefromsplithr[0]) === -1 ? timefromsplithr[0] + ":" + timefrommins : +"0" + "" + timefromsplithr[0] + ":" + timefrommins;
              const timetohr = time.to / 3600;
              const timetosplithr = timetohr.toString().split(".");
              const timetosec = timetosplithr[1];
              const timetomins = timetosec === undefined ? "00" : (+"0" + "." + timetosec) * 60;
              const timeofftohr = num.indexOf(timetosplithr[0]) === -1 ? timetosplithr[0] + ":" + timetomins : +"0" + "" + timetosplithr[0] + ":" + timetomins;
              return {
                date: moment(time.date).format("DD-MM-YYYY"),
                dates: time.date,
                day: time.day,
                from: timehr,
                to: timeofftohr,
                from_sec: time.from,
                to_sec: time.to
              };
            });
          }
          this.setState({
            _id: datas._id,
            username: datas.username,
            name: datas.name,
            surname: datas.surname,
            email: datas.email,
            locations: datas.locations,
            job_type: datas.job_type,
            const_job_type: datas.job_type,
            isverifiedCheck: datas.isverifiedCheck,
            joining_date: joiningdate,
            status: datas.status,
            number: datas.phone && datas.phone.number,
            dailcountry: datas.phone ? datas.phone.dailcountry : "gb",
            code: datas.phone && datas.phone.code,
            line1: datas.address && datas.address.line1,
            line2: datas.address && datas.address.line2,
            city: datas.address && datas.address.city,
            state: datas.address && datas.address.state,
            country: datas.address && datas.address.country,
            zipcode: datas.address && datas.address.zipcode,
            formatted_address: datas.address && datas.address.formatted_address,
            lat: datas.address && datas.address.lat,
            lon: datas.address && datas.address.lon,
            avatar_return: datas.avatar,
            // avatarName: datas.avatar ? datas.avatar.substring(24, 60) : "",
            isverified: datas.isverified,
            sunstarttime: available_data[0] !== undefined ? available_data[0].from : "",
            sunendtime: available_data[0] !== undefined ? available_data[0].to : "",
            sunavailablestatus: available_data[0] ? available_data[0].status : "",
            monstarttime: available_data[1] !== undefined ? available_data[1].from : "",
            monendtime: available_data[1] !== undefined ? available_data[1].to : "",
            monavailablestatus: available_data[1] ? available_data[1].status : "",
            tuestarttime: available_data[2] !== undefined ? available_data[2].from : "",
            tueendtime: available_data[2] !== undefined ? available_data[2].to : "",
            tueavailablestatus: available_data[2] ? available_data[2].status : "",
            wedstarttime: available_data[3] !== undefined ? available_data[3].from : "",
            wedendtime: available_data[3] !== undefined ? available_data[3].to : "",
            wedavailablestatus: available_data[3] ? available_data[3].status : "",
            thustarttime: available_data[4] !== undefined ? available_data[4].from : "",
            thuendtime: available_data[4] !== undefined ? available_data[4].to : "",
            thuavailablestatus: available_data[4] ? available_data[4].status : "",
            fristarttime: available_data[5] !== undefined ? available_data[5].from : "",
            friendtime: available_data[5] !== undefined ? available_data[5].to : "",
            friavailablestatus: available_data[5] ? available_data[5].status : "",
            satstarttime: available_data[6] !== undefined ? available_data[6].from : "",
            satendtime: available_data[6] !== undefined ? available_data[6].to : "",
            satavailablestatus: available_data[6] ? available_data[6].status : "",
            staffprofile: res.response.result && datas.staffprofile ? datas.staffprofile : "",
            timeoffdata: timeoffdata,
            stausactive: datas.status === 1 ? true : false,
            isverifiedstaus: datas.isverified === 1 ? true : false,
            stausactivesun: available_data[0] && available_data[0].status === 1 ? true : false,
            stausactivemon: available_data[1] && available_data[1].status === 1 ? true : false,
            stausactivetue: available_data[2] && available_data[2].status === 1 ? true : false,
            stausactivewed: available_data[3] && available_data[3].status === 1 ? true : false,
            stausactivethu: available_data[4] && available_data[4].status === 1 ? true : false,
            stausactivefri: available_data[5] && available_data[5].status === 1 ? true : false,
            stausactivesat: available_data[6] && available_data[6].status === 1 ? true : false
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/jobtype/list",
      method: "POST",
      data: { status: 1, for: "editemployee" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
        const jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          jobtypelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/location/list",
      method: "POST",
      data: { status: 1 }
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          worklist: res.response.result || {}
        });
        const worklistselect = this.state.worklist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          worklistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/employee/jobtype/list",
      method: "POST",
      data: {
        employee_id: this.props.location.state.rowid,
        search: "",
        filter: "all",
        page: {
          history: "",
          current: 1
        },
        order: "",
        field: "",
        limit: 5,
        status: 1
      }
    }).then(res => {
      if (res.status === 1) {
        const subscribedids = res.response.result.map(list => list._id);
        this.setState({
          subscribedlist: res.response.result,
          subscribedids: subscribedids ? subscribedids[0] : ""
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  saveChanges = value => {
    if (value) {
      const val = value.map(list => list.value);
      this.setState({ job_type: val, jobIDC: "" });
    }
  };
  selectChange = value => {
    this.setState({ locations: value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  isverifiedChanges = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      isverifiedstaus: e.target.checked,
      isverified: value
    });
  };
  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
  };
  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name + " " + results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2,
      phonestatus: status
    });
  };
  joinDate(date) {
    if (date._isAMomentObject === true) {
      this.setState({
        joindateError: false
      });
    } else {
      this.setState({
        joindateError: true
      });
    }
    this.setState({
      joining_date: date
    });
  }
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  joinDatefield = value => {
    if (value === "" || value === null) {
      this.setState({
        joindateError: true
      });
    }
  };
  finalDatefield = value => {
    if (value === "" || value === null) {
      this.setState({
        finaldateError: true
      });
    }
  };
  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          avatar: evt.target.files[0],
          avatarName: evt.target.files[0].name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  availabletoggle(tab) {
    if (this.state.availableactiveTab !== tab) {
      this.setState({
        availableactiveTab: tab
      });
    }
  }
  unavailabletoggle(tab) {
    if (this.state.unavailableactiveTab !== tab) {
      this.setState({
        unavailableactiveTab: tab
      });
    }
  }
  OnFormSubmit = (e, valuess) => {
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
      toast.error("Enter Phone Number");
    } else if (this.state.joining_date === null || this.state.joining_date === "") {
      this.setState({
        joindateError: true
      });
      toast.error("Enter Joining Date");
    } else if (this.state.locations === undefined || this.state.locations.length === 0) {
      this.setState({
        location_error: true
      });
      toast.error("Select Location");
    } else if (this.state.job_type === "") {
      this.setState({
        job_role_error: true
      });
      toast.error("Select Job Role");
    } else {
      this.setState({
        joindateError: false,
        finaldateError: false
      });
      const values = { ...valuess, ...this.state };
      const data = new FormData();
      values.locations.map((item, i) => {
        if (item.value) {
          this.setState({ locationsValue: true });
        }
        return true;
      });
      if (values.locationsValue) {
        values.locations.map((item, i) => {
          if (item.value) {
            data.append("locations", item.value);
          }
          return true;
        });
      } else {
        values.locations.map((item, i) => {
          if (item) {
            data.append("locations", item);
          }
          return true;
        });
      }
      values.job_type.map((item, i) => {
        data.append("job_type", item);
        return true;
      });
      
      data.append("_id", this.props.location.state.rowid);
      data.append("username", values.username);
      data.append("password", values.password);
      data.append("confirm_password", values.confirm_password);
      data.append("name", values.name);
      data.append("surname", values.surname);
      data.append("email", values.email);
      data.append("avatar", values.avatar);
      data.append("phone[code]", values.code);
      data.append("phone[number]", values.number);
      data.append("phone[dailcountry]", values.dailcountry);
      data.append("address[line1]", values.line1);
      data.append("address[line2]", values.line2);
      data.append("address[city]", values.city);
      data.append("address[state]", values.state);
      data.append("address[country]", values.country);
      data.append("address[zipcode]", values.zipcode);
      data.append("address[formatted_address]", values.formatted_address);
      data.append("address[lat]", values.lat);
      data.append("address[lon]", values.lon);
      data.append("joining_date", values.joining_date);
      data.append("status", values.status);
      data.append("isverified", values.isverified);
      // data.append("job_type", values.job_type);
      // data.append("locations", values.locations);
      // data.append("holiday_allowance", values.holiday_allowance);
      // data.append("final_date", values.final_date);
      data.append("available[0][day]", "sun");
      data.append("available[0][from]", values.sunstarttime || "");
      data.append("available[0][to]", values.sunendtime || "");
      data.append("available[0][status]", values.sunavailablestatus || 0);
      data.append("available[1][day]", "mon");
      data.append("available[1][from]", values.monstarttime || "");
      data.append("available[1][to]", values.monendtime || "");
      data.append("available[1][status]", values.monavailablestatus || 0);
      data.append("available[2][day]", "tue");
      data.append("available[2][from]", values.tuestarttime || "");
      data.append("available[2][to]", values.tueendtime || "");
      data.append("available[2][status]", values.tueavailablestatus || 0);
      data.append("available[3][day]", "wed");
      data.append("available[3][from]", values.wedstarttime || "");
      data.append("available[3][to]", values.wedendtime || "");
      data.append("available[3][status]", values.wedavailablestatus || 0);
      data.append("available[4][day]", "thu");
      data.append("available[4][from]", values.thustarttime || "");
      data.append("available[4][to]", values.thuendtime || "");
      data.append("available[4][status]", values.thuavailablestatus || 0);
      data.append("available[5][day]", "fri");
      data.append("available[5][from]", values.fristarttime || "");
      data.append("available[5][to]", values.friendtime || "");
      data.append("available[5][status]", values.friavailablestatus || 0);
      data.append("available[6][day]", "sat");
      data.append("available[6][from]", values.satstarttime || "");
      data.append("available[6][to]", values.satendtime || "");
      data.append("available[6][status]", values.satavailablestatus || 0);
      request({
        url: "/administrators/employee/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.componentDidMount();
            const filtered = values.const_job_type.filter(item => values.job_type.indexOf(item) === -1);
            if (filtered && filtered.length > 0) {
              request({
                url: "/agency/employee/jobtype/remove",
                method: "POST",
                data: {
                  ids: filtered,
                  employee_id: this.props.location.state.rowid
                }
              })
                .then(res => {
                  if (res.status === 1) {
                    this.componentDidMount();
                  } else if (res.status === 0) {
                    // toast.error(res.response);
                    this.componentDidMount();
                  }
                })
                .catch(err => {
                  console.log(err);
                });
              setTimeout(() => {
                window.location.reload();
              }, 2000);
            }
            toast.success("Updated");
            this.componentDidMount();
            this.setState({ jobIDC: values.job_type });
            setTimeout(() => {
              this.componentDidMount();
            }, 2000);
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  unavailablesave = () => {
    if (this.state.dates.length < 0 || this.state.unavailablefrom === "" || this.state.unavailableto === "") {
      let toastId = "unavail5645546556";
      if (!toast.isActive(toastId)) {
        toastId = toast.error("Please Fill All Fields");
      }
    } else {
      const datesCond = this.state.dates.map(list => moment(list).format("DD-MM-YYYY"));
      const Filter_Cond = this.state.timeoffdata && this.state.timeoffdata.filter(list => datesCond.indexOf(list.date) !== -1).filter(list => this.state.unavailablefrom === list.from_sec && this.state.unavailableto === list.to_sec);
      if (Filter_Cond && Filter_Cond.length > 0) {
        toast.error("Already Added On Same Date & Time");
      } else {
        let toastId_01 = "unavailable_Save_001";
        toastId_01 = toast.info("Please Wait......", { autoClose: false });
        const UnavailableData =
          this.state.dates &&
          this.state.dates.map(list => ({
            date: list,
            from: this.state.unavailablefrom,
            to: this.state.unavailableto
          }));
        request({
          url: "/agency/employee/update_availabilty",
          method: "POST",
          data: {
            employee: this.props.location.state.rowid,
            UnavailableData
          }
        })
          .then(res => {
            if (res.status === 1) {
              toast.update(toastId_01, { render: "Added!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
              this.componentDidMount();
              this.setState({
                dates: [],
                unavailablefrom: "",
                unavailableto: "",
                activeTab: "3"
              });
            } else if (res.status === 0) {
              toast.update(toastId_01, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
            }
          })
          .catch(error => {
            toast.success(error);
          });
      }
    }
  };
  deletetimeoff = index => {
    request({
      url: "/agency/employee/delete_availabilty",
      method: "POST",
      data: {
        employee: this.props.location.state.rowid,
        index: index
      }
    })
      .then(res => {
        if (res.status === 1) {
          let toastId = "delete_Time_OFF";
          if (!toast.isActive(toastId)) {
            toastId = toast.error("Deleted");
          }
          this.componentDidMount();
          this.setState({ activeTab: "3" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  selectstarttimeChange = selecttime => value => {
    if (selecttime === "sunstarttime") {
      if (value) {
        this.setState({ sunstarttime: value.value });
      } else {
        this.setState({ sunstarttime: "" });
      }
    }
    if (selecttime === "sunendtime") {
      if (value) {
        this.setState({ sunendtime: value.value });
      } else {
        this.setState({ sunendtime: "" });
      }
    }
    if (selecttime === "monstarttime") {
      if (value) {
        this.setState({ monstarttime: value.value });
      } else {
        this.setState({ monstarttime: "" });
      }
    }
    if (selecttime === "monendtime") {
      if (value) {
        this.setState({ monendtime: value.value });
      } else {
        this.setState({ monendtime: "" });
      }
    }
    if (selecttime === "tuestarttime") {
      if (value) {
        this.setState({ tuestarttime: value.value });
      } else {
        this.setState({ tuestarttime: "" });
      }
    }
    if (selecttime === "tueendtime") {
      if (value) {
        this.setState({ tueendtime: value.value });
      } else {
        this.setState({ tueendtime: "" });
      }
    }
    if (selecttime === "wedstarttime") {
      if (value) {
        this.setState({ wedstarttime: value.value });
      } else {
        this.setState({ wedstarttime: "" });
      }
    }
    if (selecttime === "wedendtime") {
      if (value) {
        this.setState({ wedendtime: value.value });
      } else {
        this.setState({ wedendtime: "" });
      }
    }
    if (selecttime === "thustarttime") {
      if (value) {
        this.setState({ thustarttime: value.value });
      } else {
        this.setState({ thustarttime: "" });
      }
    }
    if (selecttime === "thuendtime") {
      if (value) {
        this.setState({ thuendtime: value.value });
      } else {
        this.setState({ thuendtime: "" });
      }
    }
    if (selecttime === "fristarttime") {
      if (value) {
        this.setState({ fristarttime: value.value });
      } else {
        this.setState({ fristarttime: "" });
      }
    }
    if (selecttime === "friendtime") {
      if (value) {
        this.setState({ friendtime: value.value });
      } else {
        this.setState({ friendtime: "" });
      }
    }
    if (selecttime === "satstarttime") {
      if (value) {
        this.setState({ satstarttime: value.value });
      } else {
        this.setState({ satstarttime: "" });
      }
    }
    if (selecttime === "satendtime") {
      if (value) {
        this.setState({ satendtime: value.value });
      } else {
        this.setState({ satendtime: "" });
      }
    }
    if (selecttime === "unavailablefrom") {
      if (value) {
        this.setState({ unavailablefrom: value.value });
      } else {
        this.setState({ unavailablefrom: "" });
      }
    }
    if (selecttime === "unavailableto") {
      if (value) {
        this.setState({ unavailableto: value.value });
      } else {
        this.setState({ unavailableto: "" });
      }
    }
  };
  availablestatusChangesessun = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivesun: e.target.checked,
      sunavailablestatus: value
    });
  };
  availablestatusChangesesmon = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivemon: e.target.checked,
      monavailablestatus: value
    });
  };
  availablestatusChangesestue = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivetue: e.target.checked,
      tueavailablestatus: value
    });
  };
  availablestatusChangeseswed = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivewed: e.target.checked,
      wedavailablestatus: value
    });
  };
  availablestatusChangesesthu = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivethu: e.target.checked,
      thuavailablestatus: value
    });
  };
  availablestatusChangesesfri = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivefri: e.target.checked,
      friavailablestatus: value
    });
  };
  availablestatusChangesessat = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactivesat: e.target.checked,
      satavailablestatus: value
    });
  };
  selectboxValidation = () => {
    if (this.state.locations === "") {
      this.setState({
        location_error: true
      });
    }
    if (this.state.job_type === "") {
      this.setState({
        job_role_error: true
      });
    }
  };
  handleInvalidSubmit(event, errors, values) {
    toast.error("Please Fill Required Fields");
  }
  ApplytoallAvailability = () => {
    if (this.state.stausactivesun === "" && this.state.sunstarttime === "" && this.state.sunendtime === "") {
      toast.error("Please fill all fields");
    } else {
      this.setState({
        monstarttime: this.state.sunstarttime,
        tuestarttime: this.state.sunstarttime,
        wedstarttime: this.state.sunstarttime,
        thustarttime: this.state.sunstarttime,
        fristarttime: this.state.sunstarttime,
        satstarttime: this.state.sunstarttime,
        monendtime: this.state.sunendtime,
        tueendtime: this.state.sunendtime,
        wedendtime: this.state.sunendtime,
        thuendtime: this.state.sunendtime,
        friendtime: this.state.sunendtime,
        satendtime: this.state.sunendtime,
        stausactivemon: this.state.stausactivesun,
        monavailablestatus: this.state.sunavailablestatus,
        stausactivetue: this.state.stausactivesun,
        tueavailablestatus: this.state.sunavailablestatus,
        stausactivewed: this.state.stausactivesun,
        wedavailablestatus: this.state.sunavailablestatus,
        stausactivethu: this.state.stausactivesun,
        thuavailablestatus: this.state.sunavailablestatus,
        stausactivefri: this.state.stausactivesun,
        friavailablestatus: this.state.sunavailablestatus,
        stausactivesat: this.state.stausactivesun,
        satavailablestatus: this.state.sunavailablestatus
      });
      toast.success("Applied");
    }
  };
  toggleDate = () => {
    this.setState({
      popoverDateOpen: !this.state.popoverDateOpen
    });
  };
  handleDayClick = (day, { selected, highlighted }) => {
    if (selected) {
      const selectedIndex = this.state.dates.findIndex(selectedDay => DateUtils.isSameDay(selectedDay, day));
      this.state.dates.splice(selectedIndex, 1);
    } else {
      this.state.dates.push(day);
    }
    this.setState({ dates: this.state.dates });
  };
  onEditStaff = (e, valuess) => {
    e.persist();
    if (this.state.number === "" || this.state.joining_date === "" || this.state.locations === "" || this.state.job_type === "") {
      toast.error("Please Fill Required Fields");
    } else {
      client.defaults.responseType = "json";
      const data = new FormData();
      const values = { ...valuess, ...this.state }; 
      values.locations.map((item, i) => {
        if (item.value) {
          this.setState({ locationsValue: true });
        }
        return true;
      });
      if (values.locationsValue) {
        values.locations.map((item, i) => {
          if (item.value) {
            data.append("locations", item.value);
          }
          return true;
        });
      } else {
        values.locations.map((item, i) => {
          if (item) {
            data.append("locations", item);
          }
          return true;
        });
      }
      values.job_type.map((item, i) => {
        data.append("job_type", item);
        return true;
      });
      // console.log(values.staff_workinuk)

      const {
        // staff_name,
        // staff_preffered_name,
        // staff_position,
        // staff_photo,
        staff_dateofbirth,
        staff_nmc_expiry,
        staff_dbs_issue_date,
        health_safety_date,
        coshh_end_date,
        fire_safety_date,
        infection_control_date,
        food_safety_date,
        moving_handling_date,
        basiclife_support_date,
        data_protection_date,
        equalityinclusion_date,
        safeguardingvulnerable_date,
        conflict_mgn_date,
        loneworkertraining_date,
        challenging_behaviour_date,
        restraint_trg_date,
        mental_capacity_date,
      } = this.state && this.state.staffprofile ? { ...this.state.staffprofile, ...this.state } : {};

      data.append("_id", values._id);
      data.append("username", values.username);
      data.append("password", values.password);
      data.append("confirm_password", values.confirm_password);
      data.append("name", values.name);
      data.append("surname", values.surname);
      data.append("email", values.email);
      data.append("avatar", values.avatar);
      data.append("phone[code]", values.code);
      data.append("phone[number]", values.number);
      data.append("phone[dailcountry]", values.dailcountry);
      data.append("address[line1]", values.line1);
      data.append("address[line2]", values.line2);
      data.append("address[city]", values.city);
      data.append("address[state]", values.state);
      data.append("address[country]", values.country);
      data.append("address[zipcode]", values.zipcode);
      data.append("address[formatted_address]", values.formatted_address);
      data.append("address[lat]", values.lat);
      data.append("address[lon]", values.lon);
      data.append("joining_date", values.joining_date);
      data.append("status", values.status);
      data.append("isverified", values.isverified);
      // data.append("staffprofile[staff_name]", values.staff_name);
      // data.append("staffprofile[staff_preffered_name]", values.staff_preffered_name);
      // data.append("staffprofile[staff_position]", values.staff_position);
      // data.append("staff_photo", values.staff_photo);
      data.append("staffprofile[staff_grade]", values.staff_grade);
      data.append("staffprofile[staff_dateofbirth]", values.staff_dateofbirth || staff_dateofbirth);
      data.append("staffprofile[staff_gender]", values.staff_gender);
      data.append("staffprofile[staff_pinno]", values.staff_pinno);
      data.append("staffprofile[staff_nmc_expiry]", values.staff_nmc_expiry || staff_nmc_expiry || "");
      data.append("staffprofile[staff_dbs_certificate]", values.staff_dbs_certificate);
      data.append("staffprofile[staff_dbs_issue_date]", values.staff_dbs_issue_date || staff_dbs_issue_date || "");
      data.append("staffprofile[staff_disclosure_name]", values.staff_disclosure_name);
      data.append("staffprofile[staff_online_status]", values.staff_online_status);
      data.append("staffprofile[staff_workinuk]", values.staff_workinuk);
      data.append("staffprofile[staff_skill_check]", values.staff_skill_check);
      data.append("staffprofile[staff_reference]", values.staff_reference);
      data.append("staffprofile[staff_qualification_verify]", values.staff_qualification_verify);
      data.append("staffprofile[staff_face_interview]", values.staff_face_interview);
      data.append("staffprofile[education_qualification]", values.education_qualification);
      data.append("staffprofile[healthsafety]", values.healthsafety);
      data.append("staffprofile[health_safety_date]", values.health_safety_date || health_safety_date || "");
      data.append("staffprofile[coshh]", values.coshh);
      data.append("staffprofile[coshh_end_date]", values.coshh_end_date || coshh_end_date || "");
      data.append("staffprofile[firesafety]", values.firesafety);
      data.append("staffprofile[fire_safety_date]", values.fire_safety_date || fire_safety_date || "");
      data.append("staffprofile[infectioncontrol]", values.infectioncontrol);
      data.append("staffprofile[infection_control_date]", values.infection_control_date || infection_control_date || "");
      data.append("staffprofile[foodsafety]", values.foodsafety);
      data.append("staffprofile[food_safety_date]", values.food_safety_date || food_safety_date || "");
      data.append("staffprofile[movinghandling]", values.movinghandling);
      data.append("staffprofile[moving_handling_date]", values.moving_handling_date || moving_handling_date || "");
      data.append("staffprofile[basiclifesupport]", values.basiclifesupport);
      data.append("staffprofile[basiclife_support_date]", values.basiclife_support_date || basiclife_support_date || "");
      data.append("staffprofile[dataprotection]", values.dataprotection);
      data.append("staffprofile[data_protection_date]", values.data_protection_date || data_protection_date || "");
      data.append("staffprofile[equalityinclusion]", values.equalityinclusion);
      data.append("staffprofile[equalityinclusion_date]", values.equalityinclusion_date || equalityinclusion_date || "");
      data.append("staffprofile[safeguardingvulnerable]", values.safeguardingvulnerable);
      data.append("staffprofile[safeguardingvulnerable_date]", values.safeguardingvulnerable_date || safeguardingvulnerable_date || "");
      data.append("staffprofile[conflictmanagement]", values.conflictmanagement);
      data.append("staffprofile[conflict_mgn_date]", values.conflict_mgn_date || conflict_mgn_date || "");
      data.append("staffprofile[loneworkertraining]", values.loneworkertraining);
      data.append("staffprofile[loneworkertraining_date]", values.loneworkertraining_date || loneworkertraining_date || "");
      data.append("staffprofile[challengingbehaviour]", values.challengingbehaviour);
      data.append("staffprofile[challenging_behaviour_date]", values.challenging_behaviour_date || challenging_behaviour_date || "");
      data.append("staffprofile[restrainttraining]", values.restrainttraining);
      data.append("staffprofile[restraint_trg_date]", values.restraint_trg_date || restraint_trg_date || "");
      data.append("staffprofile[mentalcapacity]", values.mentalcapacity);
      data.append("staffprofile[mental_capacity_date]", values.mental_capacity_date || mental_capacity_date || "");
      data.append("staffprofile[staff_language]", values.staff_language);
      data.append("staffprofile[staff_religion]", values.staff_religion);
      data.append("staffprofile[staff_work_experience]", values.staff_work_experience);
      request({
        url: "/administrators/employee/save",
        method: "POST",
        data
      }).then(res => {
        if (res.status === 1) {
          const filtered = values.const_job_type.filter(item => values.job_type.indexOf(item) === -1);
          if (filtered && filtered.length > 0) {
            request({
              url: "/agency/employee/jobtype/remove",
              method: "POST",
              data: {
                ids: filtered,
                employee_id: this.props.location.state.rowid
              }
            })
              .then(res => {
                if (res.status === 1) {
                  this.componentDidMount();
                } else if (res.status === 0) {
                  // toast.error(res.response);
                  this.componentDidMount();
                }
              })
              .catch(err => {
                console.log(err);
              });
            setTimeout(() => {
              window.location.reload();
            }, 2000);
          }
          toast.success(res.response);
          this.componentDidMount();
          this.setState({ jobIDC: values.job_type });
          setTimeout(() => {
            this.componentDidMount();
          }, 2000);
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    }
  };
  AddJobtype = value => {
    request({
      url: "/agency/employee/jobtype/add",
      method: "POST",
      data: {
        id: value,
        employee_id: this.state._id
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Added");
          this.componentDidMount();
          this.setState({ activeTab: "4" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  RemoveJobtype = value => {
    request({
      url: "/agency/employee/jobtype/remove",
      method: "POST",
      data: {
        id: value,
        employee_id: this.state._id
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Removed");
          this.componentDidMount();
          this.setState({ activeTab: "4" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  viewpage = (id, job_t) => {
    return this.props.history.push({
      pathname: "/agency/viewemployee",
      state: { rowid: id, job_type: (job_t && job_t.length > 0 && job_t[0]._id) || "" }
    });
  };
  render() {
    const highl = this.state.timeoffdata && this.state.timeoffdata.map(list => new Date(list.dates));
    const modifiers = {
      highlighted: highl
    };
    const {
      // staff_name,
      // staff_preffered_name,
      // staff_position,
      // staff_photo,
      staff_grade,
      staff_dateofbirth,
      staff_gender,
      staff_pinno,
      staff_nmc_expiry,
      staff_dbs_certificate,
      staff_dbs_issue_date,
      staff_disclosure_name,
      staff_online_status,
      staff_workinuk,
      staff_skill_check,
      staff_reference,
      staff_qualification_verify,
      staff_face_interview,
      education_qualification,
      healthsafety,
      health_safety_date,
      coshh,
      coshh_end_date,
      firesafety,
      fire_safety_date,
      infectioncontrol,
      infection_control_date,
      foodsafety,
      food_safety_date,
      movinghandling,
      moving_handling_date,
      basiclifesupport,
      basiclife_support_date,
      dataprotection,
      data_protection_date,
      equalityinclusion,
      equalityinclusion_date,
      safeguardingvulnerable,
      safeguardingvulnerable_date,
      conflictmanagement,
      conflict_mgn_date,
      loneworkertraining,
      loneworkertraining_date,
      challengingbehaviour,
      challenging_behaviour_date,
      restrainttraining,
      restraint_trg_date,
      mentalcapacity,
      mental_capacity_date,
      staff_language,
      staff_religion,
      staff_work_experience
    } = this.state && this.state.staffprofile ? { ...this.state.staffprofile, ...this.state } : {};
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              <CardHeader>
                <strong>Edit</strong> Employee
                <div className="card-actions">
                  <button onClick={(id, job) => this.viewpage(this.state._id, this.state.job_type)}>
                    <i className="fa fa-eye" /> View Details
                    {/* <small className="text-muted" />*/}
                  </button>
                </div>
              </CardHeader>
              <CardBody>
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "1"
                      })}
                      onClick={() => {
                        this.toggle("1");
                      }}
                    >
                      Staff Profile
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "2"
                      })}
                      onClick={() => {
                        this.toggle("2");
                      }}
                    >
                      Availability
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "3"
                      })}
                      onClick={() => {
                        this.toggle("3");
                      }}
                    >
                      Unavailability
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "4"
                      })}
                      onClick={() => {
                        this.toggle("4");
                      }}
                    >
                      Job Roles & Rates
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "5"
                      })}
                      onClick={() => {
                        this.toggle("5");
                      }}
                    >
                      Login Details
                    </NavLink>
                  </NavItem>
                </Nav>

                <TabContent activeTab={this.state.activeTab}>
                  <TabPane tabId="1">
                    <AvForm onValidSubmit={this.onEditStaff} onInvalidSubmit={this.handleInvalidSubmit}>
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>First Name</Label>
                              <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Surname</Label>
                              <AvInput type="text" name="surname" placeholder="Enter Surname" onChange={this.onChange} value={this.state.surname} required autoComplete="surname" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Email</Label>
                              <AvInput name="email" type="email" placeholder="Enter email" onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4" className={"for-phone-right"}>
                            <Label className={this.state.phoneerror ? "error-color" : null}>Tel</Label>
                            {this.state && this.state.dailcountry ? (
                              <IntlTelInput
                                style={{ width: "100%" }}
                                defaultCountry={this.state.dailcountry}
                                utilsScript={libphonenumber}
                                css={this.state.phoneerror ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                onPhoneNumberChange={this.handler}
                                value={this.state.number}
                              />
                            ) : null}
                            {this.state.phoneerror ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                          </Col>
                          <Col xs="6" md="4">
                            <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                          </Col>
                          {this.state.propreview ? (
                            <Col xs="6" md="3">
                              <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                            </Col>
                          ) : null}
                          <Col xs="6" md="3">
                            <Label>Profile Image</Label>
                            <img
                              width="180px"
                              height="180px"
                              src={NodeURL + "/" + this.state.avatar_return}
                              alt="Profile"
                              onError={() => {
                                this.setState({ avatar_return: "../../img/user-profile.png" });
                              }}
                            />
                          </Col>
                        </div>
                      </Row>
                      <br />
                      <Row />

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Address</p>
                          </Col>
                          <Col xs="12" md="4">
                            <Label>Search address</Label>
                            <PlacesAutocomplete value={this.state.formatted_address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                              {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                                <div>
                                  <input
                                    {...getInputProps({
                                      placeholder: "Search Places ...",
                                      className: "form-control"
                                    })}
                                  />
                                  <div className="autocomplete-dropdown-container absolute">
                                    {suggestions.map(suggestion => {
                                      const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                      // inline style for demonstration purpose
                                      const style = suggestion.active
                                        ? {
                                            backgroundColor: "#fafafa",
                                            cursor: "pointer"
                                          }
                                        : {
                                            backgroundColor: "#ffffff",
                                            cursor: "pointer"
                                          };
                                      return (
                                        <div
                                          {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                          })}
                                        >
                                          <span>{suggestion.description}</span>
                                        </div>
                                      );
                                    })}
                                  </div>
                                </div>
                              )}
                            </PlacesAutocomplete>
                          </Col>

                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Line1</Label>
                              <AvInput type="text" name="line1" placeholder="Enter line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>City/Town</Label>
                              <AvInput type="text" name="line2" placeholder="Enter City/Town" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          {/* <Col xs="12" md="4">
                            <AvGroup>
                              <Label>City</Label>
                              <AvInput
                                type="text"
                                name="city"
                                placeholder="Enter city.."
                                onChange={this.onChange}
                                value={this.state.city}
                                required
                                autoComplete="name"
                              />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col> */}

                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>State/Region</Label>
                              <AvInput type="text" name="state" placeholder="Enter state" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Country</Label>
                              <AvInput type="text" name="country" placeholder="Enter country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Post Code</Label>
                              <AvInput type="text" name="zipcode" placeholder="Enter Post Code" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Work Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <Label className={this.state.location_error ? "error-color" : ""}>Area</Label>
                            <Select name="locations" className={this.state.location_error ? "custom_select_validation" : null} value={this.state.locations} options={this.state.worklistselect} onChange={this.selectChange} multi />
                            {/* <small className="ml-1 cursor-pointer" onClick={this.worklocationModel}>
                          {" "}
                          <i className="fa fa-info-circle" />
                          <span className="ml-1 cursor-pointer cm-strong">Add New Work Location</span>
                        </small> */}
                            {this.state.location_error ? <div className="error-color"> This is required!</div> : null}
                          </Col>

                          <Col xs="12" md="4">
                            <label className={this.state.joindateError ? "error-color" : null}>Joining Date</label>
                            <SingleDatePicker
                              // className={this.state.joindateError ? "error-color" : null}
                              required={true}
                              id="joining_date"
                              date={this.state.joining_date}
                              onDateChange={joining_date => this.joinDate(joining_date)}
                              focused={this.state.focused2}
                              onFocusChange={({ focused: focused2 }) => this.setState({ focused2 })}
                              displayFormat="DD-MM-YYYY"
                            />{" "}
                            {this.state.joindateError ? <div className="error-color"> Enter Valid Date!</div> : null}
                          </Col>

                          <Col xs="12" md="4">
                            <Label className={this.state.job_role_error ? "error-color" : ""}>Job Role</Label>
                            <Select name="job_type" className={this.state.job_role_error ? "custom_select_validation" : null} value={this.state.job_type} options={this.state.jobtypelistselect} onChange={this.saveChanges} multi />
                            {/* <small className="ml-1 cursor-pointer" onClick={this.jobTypeModel}>
                          {" "}
                          <i className="fa fa-info-circle" />
                          <span className="ml-1 cursor-pointer cm-strong">Add New Job Role</span>
                        </small> */}
                            {this.state.job_role_error ? <div className="error-color"> This is required!</div> : null}
                          </Col>

                          {/* <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Holiday Allowance (£)</Label>
                            <AvInput
                              type="text"
                              name="holiday_allowance"
                              placeholder="Enter holiday allowance"
                              onChange={this.onChange}
                              value={this.state.holiday_allowance}
                              required
                              autoComplete="holiday_allowance"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col> */}
                        </div>
                      </Row>
                      <br />
                      {/* <Row>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Name</Label>
                            <AvInput type="text" name="staff_name" onChange={this.onChange} value={staff_name} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Preferred Name</Label>
                            <AvInput type="text" name="staff_preffered_name" onChange={this.onChange} value={staff_preffered_name} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <Label for="exampleCustomFileBrowser">Upload ID Photo</Label>
                          <CustomInput type="file" id="exampleCustomFileBrowser" name="staff_photo" onChange={this.fileChangedHandler} label={this.state.staff_photo_Name ? this.state.staff_photo_Name : "Upload  Image"} encType="multipart/form-data" />
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Position/Role</Label>
                            <AvInput type="text" name="staff_position" onChange={this.onChange} value={staff_position} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        {staff_photo ? (
                            <Col xs="6" md="4">
                              <img className="img-fluid" src={NodeURL + "/" + staff_photo} alt="Profile" />
                            </Col>
                        ) : null}
                      </Row>*/}

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Personal Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Grade</Label>
                              <AvInput type="text" name="staff_grade" onChange={this.onChange} value={staff_grade} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <label>DOB</label>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(staff_dateofbirth)}
                              inputProps={{ placeholder: "Pick Date" }}
                              isValidDate={(current) => current.isBefore(moment(new Date()).subtract(18, "years"))}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ staff_dateofbirth: e });
                                } else if (!e) {
                                  this.setState({ staff_dateofbirth: null });
                                } else {
                                  this.setState({ staff_dateofbirth: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField className="form-control fname" type="select" name="staff_gender" label="Gender" placeholder="Please Select" onChange={this.onChange} value={staff_gender} required>
                              <option>Please Select</option>
                              <option value={"male"}>Male</option>
                              <option value={"female"}>Female</option>
                            </AvField>
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">NMC Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvField name="staff_pinno" label="PIN No:" onChange={this.onChange} value={staff_pinno} validate={{ required: { value: true, errorMessage: "This is required!" } }} autoComplete="email" />
                          </Col>
                          <Col xs="12" md="4">
                            <label>Expiry Date</label>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(staff_nmc_expiry) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ staff_nmc_expiry: e });
                                } else if (!e) {
                                  this.setState({ staff_nmc_expiry: null });
                                } else {
                                  this.setState({ staff_nmc_expiry: null });
                                }
                              }}
                            />
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">DBS Details</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvField
                              name="staff_dbs_certificate"
                              label="Certificate No:"
                              onChange={this.onChange}
                              value={staff_dbs_certificate}
                              validate={{ required: { value: true, errorMessage: "This is required!" } }}
                              autoComplete="email"
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <label>Issue Date</label>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(staff_dbs_issue_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ staff_dbs_issue_date: e });
                                } else if (!e) {
                                  this.setState({ staff_dbs_issue_date: null });
                                } else {
                                  this.setState({ staff_dbs_issue_date: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Disclosure Employer Name: </Label>
                              <AvInput type="text" name="staff_disclosure_name" onChange={this.onChange} value={staff_disclosure_name} required autoComplete="off" />
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline label={"Online Check Status"} name="staff_online_status" value={staff_online_status} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Checks & Verification</p>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline label={"Right to Work in UK"} name="staff_workinuk" value={staff_workinuk} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline label={"Skills Check"} name="staff_skill_check" value={staff_skill_check} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline label={"2 Reference on file"} name="staff_reference" value={staff_reference} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline label={"Qualification Verified"} name="staff_qualification_verify" value={staff_qualification_verify} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvRadioGroup inline label={"Face to Face Interview"} name="staff_face_interview" value={staff_face_interview} required>
                              <AvRadio label="Yes" value="yes" />
                              <AvRadio label="No" value="no" />
                            </AvRadioGroup>
                          </Col>
                          <Col xs="12" md="12">
                            <AvField
                              name="education_qualification"
                              type="textarea"
                              label="Education/Qualifications"
                              onChange={this.onChange}
                              value={education_qualification}
                              validate={{ required: { value: true, errorMessage: "This is required!" } }}
                              autoComplete="email"
                            />
                          </Col>
                        </div>
                      </Row>

                      <Row className="custom_single_date">
                        <div className="cus-design edited-section">
                          <Col xs="12" md="12">
                            <p className="h5">Mandatory training</p>
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="healthsafety" checked={healthsafety === ("true" || true) ? "true" : false} /> <span className="Man-heading">Health and Safety </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(health_safety_date) || "" }
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ health_safety_date: e });
                                } else if (!e) {
                                  this.setState({ health_safety_date: null });
                                } else {
                                  this.setState({ health_safety_date: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="coshh" checked={coshh === ("true" || true) ? "true" : false} /> <span className="Man-heading"> COSHH </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(coshh_end_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ coshh_end_date: e });
                                } else if (!e) {
                                  this.setState({ coshh_end_date: null });
                                } else {
                                  this.setState({ coshh_end_date: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="firesafety" checked={firesafety === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Fire Safety </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(fire_safety_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ fire_safety_date: e });
                                } else if (!e) {
                                  this.setState({ fire_safety_date: null });
                                } else {
                                  this.setState({ fire_safety_date: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="infectioncontrol" checked={infectioncontrol === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Infection Control </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(infection_control_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ infection_control_date: e });
                                } else if (!e) {
                                  this.setState({ infection_control_date: null });
                                } else {
                                  this.setState({ infection_control_date: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="foodsafety" checked={foodsafety === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Food Safety & Nutrition </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(food_safety_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ food_safety_date: e });
                                } else if (!e) {
                                  this.setState({ food_safety_date: "" });
                                } else {
                                  this.setState({ food_safety_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="movinghandling" checked={movinghandling === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Moving & Handling </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(moving_handling_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ moving_handling_date: e });
                                } else if (!e) {
                                  this.setState({ moving_handling_date: "" });
                                } else {
                                  this.setState({ moving_handling_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="basiclifesupport" checked={basiclifesupport === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Basic life support </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(basiclife_support_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ basiclife_support_date: e });
                                } else if (!e) {
                                  this.setState({ basiclife_support_date: "" });
                                } else {
                                  this.setState({ basiclife_support_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="dataprotection" checked={dataprotection === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Data Protection </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(data_protection_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ data_protection_date: e });
                                } else if (!e) {
                                  this.setState({ data_protection_date: null });
                                } else {
                                  this.setState({ data_protection_date: null });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="equalityinclusion" checked={equalityinclusion === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Equality & Inclusion </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(equalityinclusion_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ equalityinclusion_date: e });
                                } else if (!e) {
                                  this.setState({ equalityinclusion_date: "" });
                                } else {
                                  this.setState({ equalityinclusion_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="safeguardingvulnerable" checked={safeguardingvulnerable === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Safeguarding Vulnerable Adults & Children </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(safeguardingvulnerable_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ safeguardingvulnerable_date: e });
                                } else if (!e) {
                                  this.setState({ safeguardingvulnerable_date: "" });
                                } else {
                                  this.setState({ safeguardingvulnerable_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="conflictmanagement" checked={conflictmanagement === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Conflict Management </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(conflict_mgn_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ conflict_mgn_date: e });
                                } else if (!e) {
                                  this.setState({ conflict_mgn_date: "" });
                                } else {
                                  this.setState({ conflict_mgn_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="loneworkertraining" checked={loneworkertraining === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Lone Worker Training </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(loneworkertraining_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ loneworkertraining_date: e });
                                } else if (!e) {
                                  this.setState({ loneworkertraining_date: "" });
                                } else {
                                  this.setState({ loneworkertraining_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="challengingbehaviour" checked={challengingbehaviour === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Managing Challenging Behaviour </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(challenging_behaviour_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ challenging_behaviour_date: e });
                                } else if (!e) {
                                  this.setState({ challenging_behaviour_date: "" });
                                } else {
                                  this.setState({ challenging_behaviour_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="restrainttraining" checked={restrainttraining === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Restraint Training </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(restraint_trg_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ restraint_trg_date: e });
                                } else if (!e) {
                                  this.setState({ restraint_trg_date: "" });
                                } else {
                                  this.setState({ restraint_trg_date: "" });
                                }
                              }}
                            />
                          </Col>
                          <Col xs="12" md="6">
                            <AvInput type="checkbox" name="mentalcapacity" checked={mentalcapacity === ("true" || true) ? "true" : false} />
                            <span className="Man-heading"> Mental Capacity Act 2005 </span>
                            <Datetime
                              dateFormat="DD-MM-YYYY"
                              timeFormat={false}
                              closeOnSelect={true}
                              value={moment(mental_capacity_date) || ""}
                              inputProps={{ placeholder: "Pick Date" }}
                              onChange={e => {
                                if (e._isValid) {
                                  this.setState({ mental_capacity_date: e });
                                } else if (!e) {
                                  this.setState({ mental_capacity_date: "" });
                                } else {
                                  this.setState({ mental_capacity_date: "" });
                                }
                              }}
                            />
                          </Col>
                        </div>
                      </Row>
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="4">
                            <AvField type="text" label="Languages Spoken" name="staff_language" onChange={this.onChange} value={staff_language} />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField type="text" label="Religion" name="staff_religion" onChange={this.onChange} value={staff_religion} />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField type="text" label="Work Experience" name="staff_work_experience" onChange={this.onChange} value={staff_work_experience} />
                          </Col>
                        </div>
                      </Row>
                      <Button type="submit" color="primary pull-right" title="Update">
                        <i
                          className="fa fa-dot-circle-o"
                          onClick={() => {
                            this.phonefield(this.state.number);
                            this.joinDatefield(this.state.joining_date);
                            this.selectboxValidation();
                          }}
                        />{" "}
                        Update
                      </Button>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="2">
                    <AvForm onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                      <Row>
                        <Nav tabs className="days-calender">
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "1"
                              })}
                              onClick={() => {
                                this.availabletoggle("1");
                              }}
                            >
                              {" "}
                              Sun
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "2"
                              })}
                              onClick={() => {
                                this.availabletoggle("2");
                              }}
                            >
                              Mon
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "3"
                              })}
                              onClick={() => {
                                this.availabletoggle("3");
                              }}
                            >
                              Tue
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "4"
                              })}
                              onClick={() => {
                                this.availabletoggle("4");
                              }}
                            >
                              Wed
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "5"
                              })}
                              onClick={() => {
                                this.availabletoggle("5");
                              }}
                            >
                              Thu
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "6"
                              })}
                              onClick={() => {
                                this.availabletoggle("6");
                              }}
                            >
                              Fri
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.availableactiveTab === "7"
                              })}
                              onClick={() => {
                                this.availabletoggle("7");
                              }}
                            >
                              Sat
                            </NavLink>
                          </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.availableactiveTab}>
                          <TabPane tabId="1">
                            <Row>
							<div className="cus-design edited-section">
                              
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="sunstarttime" value={this.state.sunstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("sunstarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="sunendtime" value={this.state.sunendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("sunendtime")} />
                              </Col>
							  <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivesun} onChange={this.availablestatusChangesessun} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
                              <Col xs="12" md="2" className="var-values">
                                <button type="button" className="btn btn-add" onClick={this.ApplytoallAvailability}>
                                  Apply to all
                                </button>
                              </Col>
							  </div>
                            </Row>
                          </TabPane>
                          <TabPane tabId="2">
                            <Row>
							<div className="cus-design edited-section">
                             
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="monstarttime" value={this.state.monstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("monstarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="monendtime" value={this.state.monendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("monendtime")} />
                              </Col>
							   <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivemon} onChange={this.availablestatusChangesesmon} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
							  </div>
                            </Row>
                          </TabPane>
                          <TabPane tabId="3">
                            <Row>
							<div className="cus-design edited-section">
                             
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="tuestarttime" value={this.state.tuestarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("tuestarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="tueendtime" value={this.state.tueendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("tueendtime")} />
                              </Col>
							   <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivetue} onChange={this.availablestatusChangesestue} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
							  </div>
                            </Row>
                          </TabPane>
                          <TabPane tabId="4">
                            <Row>
							<div className="cus-design edited-section">
                              
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="wedstarttime" value={this.state.wedstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("wedstarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="wedendtime" value={this.state.wedendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("wedendtime")} />
                              </Col>
							  <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivewed} onChange={this.availablestatusChangeseswed} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
							  </div>
                            </Row>
                          </TabPane>
                          <TabPane tabId="5">
                            <Row>
							<div className="cus-design edited-section">
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="thustarttime" value={this.state.thustarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("thustarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="thuendtime" value={this.state.thuendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("thuendtime")} />
                              </Col>
							   <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivethu} onChange={this.availablestatusChangesesthu} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
							  </div>
                            </Row>
							
                          </TabPane>
                          <TabPane tabId="6">
						  
                            <Row>
                              <div className="cus-design edited-section">
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="fristarttime" value={this.state.fristarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("fristarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="friendtime" value={this.state.friendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("friendtime")} />
                              </Col>
							  <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivefri} onChange={this.availablestatusChangesesfri} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
							  </div>
                            </Row>
							
                          </TabPane>
                          <TabPane tabId="7">
                            <Row>
							 <div className="cus-design edited-section">
                              
                              <Col xs="12" md="4">
                                <Label for="from">Start Time</Label>
                                <Select name="satstarttime" value={this.state.satstarttime} options={this.state.timelist} onChange={this.selectstarttimeChange("satstarttime")} />
                              </Col>
                              <Col xs="12" md="4">
                                <Label for="from">End Time</Label>
                                <Select name="satendtime" value={this.state.satendtime} options={this.state.timelist} onChange={this.selectstarttimeChange("satendtime")} />
                              </Col>
							  <Col xs="12" md="2">
                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input type="checkbox" className="switch-input" checked={this.state.stausactivesat} onChange={this.availablestatusChangesessat} />
                                  <span className="switch-label" data-on="active" data-off="inactive" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
							  </div>
                            </Row>
                          </TabPane>
                        </TabContent>
                      </Row>
                      <Row className="d-flex justify-content-end mt-3">
                        <Button type="submit" color="primary" title="Update">
                          <i
                            className="fa fa-dot-circle-o"
                            onClick={() => {
                              this.phonefield(this.state.number);
                              this.joinDatefield(this.state.joining_date);
                              this.selectboxValidation();
                            }}
                          />{" "}
                          Update
                        </Button>
                      </Row>
                    </AvForm>
                  </TabPane>
                  <TabPane tabId="3">
                    <Fragment>
                      <Row className="emy-edits">
                        <div className="cus-design edited-section">
                          <Col xs="12" md="3">
                            <Label>Select Date</Label>
                            <Input type="text" name="date" id="PopoverDate" placeholder="Date" onChange={this.onChange} onClick={this.toggleDate} value={this.state.dates.map(list => moment(list).format("DD-MM-YYYY"))} />
                            <Popover placement="bottom" isOpen={this.state.popoverDateOpen} target="PopoverDate" toggle={this.toggleDate}>
                              <PopoverBody className="emp-date">
                                <DayPicker selectedDays={this.state.dates} disabledDays={{ before: new Date() }} modifiers={modifiers} onDayClick={this.handleDayClick} />
                              </PopoverBody>
                            </Popover>
                          </Col>
                          <Col xs="12" md="3">
                            <Label for="from">Start Time</Label>
                            <Select name="unavailablefrom" value={this.state.unavailablefrom} options={this.state.timelist} onChange={this.selectstarttimeChange("unavailablefrom")} />
                          </Col>
                          <Col xs="12" md="3">
                            <Label for="from">End Time</Label>
                            <Select name="unavailableto" value={this.state.unavailableto} options={this.state.timelist} onChange={this.selectstarttimeChange("unavailableto")} />
                          </Col>

                          <Col xs="12" md="3">
                            <Button color="success" className="mt-4 avail-addings" onClick={this.unavailablesave}>
                              <i className="fa fa-plus" />
                            </Button>
                          </Col>
                        </div>
                      </Row>
                      {this.state.timeoffdata.map((timeoff, key) => {
                        return (
                          <Row className="mt-1  val-show" key={key}>
                            <Col xs="12" md="3">
                              <b>{timeoff.date}</b>
                            </Col>
                            <Col xs="12" md="3">
                              {timeoff.from}
                            </Col>
                            <Col xs="12" md="3">
                              {timeoff.to}
                            </Col>
                            <Col xs="12" md="3">
                              <Button
                                size="sm"
                                onClick={() => {
                                  this.deletetimeoff(key);
                                }}
                                color="danger"
                                className=""
                              >
                                <i className="fa fa-minus" />
                              </Button>
                            </Col>
                          </Row>
                        );
                      })}
                    </Fragment>
                  </TabPane>
                  <TabPane tabId="4">
                    {this.state && this.state.subscribedlist && this.state.subscribedlist.length > 0 ? (
                      <JobtypeListEmployee joblistdata={this.state.subscribedlist} employeeid={this.state.RowIDC || this.state._id} AddJobtype={this.AddJobtype} RemoveJobtype={this.RemoveJobtype} />
                    ) : null}
                  </TabPane>
                  <TabPane tabId="5">
                    <AvForm onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                      <Row>
                        <div className="cus-design edited-section">
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Username</Label>
                              <AvInput type="text" name="username" placeholder="Enter username.." onChange={this.onChange} value={this.state.username} required autoComplete="name" />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <Label>Password</Label>
                            <AvInput type="password" name="password" placeholder="Enter password.." onChange={this.onChange} value={this.state.password} autoComplete="off" />
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Confirm Password</Label>
                              <AvInput type="password" name="confirm_password" placeholder="Enter password.." onChange={this.onChange} value={this.state.confirm_password} validate={{ match: { value: "password" } }} autoComplete="off" />
                              <AvFeedback>Match Password!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <div>
                              <label>Status</label>
                            </div>
                            <Label className="switch switch-text switch-success new-switch">
                              <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                              <span className="switch-label" data-on="active" data-off="inactive" />
                              <span className="switch-handle new-handle" />
                            </Label>
                          </Col>
                          {this.state.adminlist.isverified === 0 ? (
                            <Col xs="12" md="4">
                              <div>
                                <label>Verified</label>
                              </div>
                              <Label className="switch switch-text switch-success">
                                <Input type="checkbox" className="switch-input" checked={this.state.isverifiedstaus} onChange={this.isverifiedChanges} />
                                <span className="switch-label" data-on="yes" data-off="no" />
                                <span className="switch-handle" />
                              </Label>
                            </Col>
                          ) : null}
                        </div>
                      </Row>
                      {/* <Row>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Employee Rate</Label>
                            <AvInput
                              type="text"
                              name="employee_rate"
                              placeholder="Enter Employee Rrate.."
                              onChange={this.onChange}
                              value={this.state.employee_rate}
                              required
                              autoComplete="employee_rate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Agency Rate</Label>
                            <AvInput
                              type="text"
                              name="agency_rate"
                              placeholder="Enter Agency Rate.."
                              onChange={this.onChange}
                              value={this.state.agency_rate}
                              required
                              autoComplete="agency_rate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Client Rate</Label>
                          {this.state.agency_rate & this.state.employee_rate ? (
                            <p>
                              <strong>{parseInt(this.state.employee_rate, Number) + parseInt(this.state.agency_rate, Number)}</strong>
                            </p>
                          ) : null}
                        </Col>
                      </Row> */}
                      {/* <Row>
                         <Col xs="12" md="4">
                          <label className={this.state.finaldateError ? "error-color" : null}>Final Working Date</label>
                          <SingleDatePicker
                            // className={this.state.finaldateError ? "error-color" : null}
                            date={this.state.final_date}
                            isOutsideRange={day => day.isBefore(this.state.joining_date)}
                            onDateChange={final_date => this.finalDate(final_date)}
                            focused={this.state.focused1}
                            onFocusChange={({ focused: focused1 }) => this.setState({ focused1 })}
                            id="final_date"
                            displayFormat="DD-MM-YYYY"
                          />{" "}
                          {this.state.finaldateError ? <div className="error-color"> Enter Valid Date!</div> : null}
                        </Col>
                      </Row>*/}
                      <Row />
                      <Row className="d-flex justify-content-end mt-5">
                        <Button type="submit" color="primary" title="Update">
                          <i className="fa fa-dot-circle-o" /> Update
                        </Button>
                      </Row>
                    </AvForm>
                  </TabPane>
                </TabContent>
              </CardBody>
              <CardFooter>
                <Button
                  color="secondary"
                  title="Back"
                  onClick={() => {
                    window.history.go(-1);
                  }}
                >
                  <i className="fa fa-arrow-left" /> Back
                </Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Editemployee;
