import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../../common/loader";
import moment from "moment";
import { Badge, Button, Card, CardBody, CardHeader, CardFooter, Table } from "reactstrap";
import request, { NodeURL, client } from "../../../../api/api";
import { breaklist, timelist } from "../../../common/utils";
import FileSaver from "file-saver";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "125px"
};

class timesheet extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    singlecheck: false,
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    timesheet_endtimeview: "",
    timesheet_starttimeview: "",
    status_timesheet: "",
    tableOptions: {
      id: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid,
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 100,
      skip: 0,
      to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : "",
      employeeID: this.props && this.props.location.state ? this.props.location.state.id._id.employee[0]._id : "",
      clientID: this.props && this.props.location.state ? this.props.location.state.id._id.client[0]._id : "",
      // locationID: this.props && this.props.location.state ? this.props.location.state.id._id.locations[0]._id : "",
      // branchID: this.props && this.props.location.state ? this.props.location.state.id._id.branch[0].branches._id : "",
      jobtypeID: this.props && this.props.location.state ? this.props.location.state.id._id.job_type[0]._id : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    viewtimesheetid: "",
    timesheet: [],
    ForediState: false,
    ShowApprovebtn: true,
    agency_data: "",
    agency_avatar: "",
    TimesheetPrefix: "",
    currentusername: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      timelist,
      breaklist,
      currentusername: token && token.username,
      isLoader: true,
      timesheet_to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      timesheet_from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : ""
    });
    this.populateData();
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  populateData() {
    request({
      url: "/agency/employeetimesheet/employee/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            if (earn._id === 7) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 6) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            }
            return true;
          });
          let getshiftdata = [];
          this.setState({
            timesheetlists: res.response.result
          });
          res.response.result.map((item, i) => {
            var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            var evestarthr = item.starttime / 3600;
            var splithr = evestarthr.toString().split(".");
            var startsec = splithr[1];
            if (startsec === undefined) {
              var startmins = "00";
            } else {
              startmins = (+"0" + "." + startsec) * 60;
            }

            if (num.indexOf(splithr[0]) === -1) {
              var starttimehr = splithr[0] + ":" + startmins;
            } else {
              starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
            }
            var eveendhr = item.endtime / 3600;
            var splitendhr = eveendhr.toString().split(".");
            var endsec = splitendhr[1];
            if (endsec === undefined) {
              var endmins = "00";
            } else {
              endmins = (+"0" + "." + endsec) * 60;
            }

            if (num.indexOf(splitendhr[0]) === -1) {
              var endtimehr = splitendhr[0] + ":" + endmins;
            } else {
              endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
            }
            if (item.timesheet_status) {
              var timesheet_status = item.timesheet_status;
            } else {
              timesheet_status = "";
            }
            getshiftdata.push({
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.title,
              shiftId: item.shiftId,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            });
            this.setState({
              adminlist: getshiftdata,
              pages: res.response.fullcount,
              currPage: res.response.length,
              agency_data: res.response.result.length > 0 ? res.response.result[0].agency_data[0] : {},
              agency_avatar: res.response.result.length > 0 ? res.response.result[0].agency_data[0].company_logo : {}
            });
            return true;
          });
        }
        let TimesheetPrefix = "";
        const TimeID = res.response.result.filter(list => list.timesheetID);
        if (TimeID.length > 0) {
          TimesheetPrefix = TimeID[0].timesheetID;
        } else {
          request({
            url: "/site/timesheetprefixnumber",
            method: "POST",
            data: { for: "employee", username: this.state.currentusername }
          }).then(res => {
            if (res.status === 1) {
              TimesheetPrefix = res.response;
            } else if (res.status === 0) {
              if (res.response === "Timesheet Settings Not Available") {
                this.props.history.push("/agency/employee/timesheet");
                toast.error("Please Contact Agency");
              } else {
                toast.error(res.response);
              }
            }
          });
        }
        setTimeout(() => {
          this.setState({ TimesheetPrefix });
        }, 1000);
        const time_Sta = res.response.result.filter(list => list.timesheet_status === 1);
        if (time_Sta.length === 0) {
          this.setState({ ShowApprovebtn: false });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["starttime", "start_date", "branch", "title", "status", "breaktime"];
    for (let i in id) {
      if(document.getElementById(id[i])){
        document.getElementById(id[i]).className = sorticondef;
      }
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };

  TimesheetDownloadPDF = value => {
    let toastId = "Down_Time_01";
    if (value === "download") {
      client.defaults.responseType = "blob";
      toastId = toast.info("Downloading......", { autoClose: false });
    } else {
        client.defaults.responseType = "json";
      toastId = toast.info("Sending......", { autoClose: false });
    }
    if(this.state.TimesheetPrefix){
      request({
        url: "/agency/employeetimesheet/downloadpdf",
        method: "POST",
        data: {
          TabOpt: this.state.tableOptions,
          for: value,
          timesheet_timesheetID: this.state.TimesheetPrefix,
          timesheet_to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
          timesheet_from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
        }
      })
        .then(res => {
          if (res.status === 1) {
            if ((res.msg = "Mail Sent")) {
              toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
              this.componentDidMount();
            }
          } else if (res.status === 0) {
            toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
          } else {
            if (res) {
              const file = new Blob([res], { type: "application/pdf" });
              const name = `Timesheet_${this.state.TimesheetPrefix}.pdf`;
              FileSaver(file, name);
              toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
              this.componentDidMount();
            }
              client.defaults.responseType = "json";
          }
        })
        .catch(error => {
          client.defaults.responseType = "json";
          toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
          toast.error(error);
        });
    }else {
      client.defaults.responseType = "json";
      toast.update(toastId, { render: "Unable to Download", type: toast.TYPE.ERROR, autoClose: 2500 });
    }
  };
  goBack = () => {
    window.history.go(-1);
  }
  
  render() {

    return (
      <div className="right-edit-side">
        <div className="form__card card form__card--background float mobile-earnings">
          <ToastContainer position="top-right" autoClose={2500} />

          <Card className="p-0 for-radius card">  
            <CardHeader>
              Time Sheet List
              <div className="card-actions" style={tStyle}>
                <button className="exp-options" style={tStyles} onClick={() => this.TimesheetDownloadPDF("download")}>
                  <i className="fa fa-download" /> Download
                  <small className="text-muted" />
                </button>
               {/* <button className="exp-options" style={tStyles} onClick={() => this.TimesheetDownloadPDF("mail")}>
                  <i className="fa fa-envelope" /> Mail
                  <small className="text-muted" />
                </button>*/}
              </div>
            </CardHeader>
            <CardBody>
              <div className="row">
                <div className="col-md-12 fle-model">
                <div className="div-side">
                  <h5>Timesheet Date: <span>
                    {moment(this.state.timesheet_from_date).format("DD-MM-YYYY")} - {moment(this.state.timesheet_to_date).format("DD-MM-YYYY")}{" "}
                  </span> </h5>
                </div> 
                <div className="div-page"> 
                  <h5>Timesheet Number: <span>{this.state.TimesheetPrefix}</span></h5>
                  
                  </div>
                </div>
                {this.state.adminlist.length &&
                  this.state.adminlist.slice(0, 1).map((list, i) => (
                    <div className="agency-arra pt-0 col-md-6" key={i}>
                      <div className="main-profiles">
                        <div className="left-agency-profile">
                          <div className="time-sheet-src">
                            <img
                              width="180px"
                              height="180px"
                              src={`${NodeURL}/${list.employee_avatar}`}
                              alt="Profile"
                              onError={() => {
                                this.setState({ employee_avatar: "../../img/user-profile.png" });
                              }}
                            />
                          </div>
                        </div>
                        <div className="right-agency-profile">
                          <div className="upper-range">
                            <h5>Employee Name</h5>
                            <p>
                              <span>{list.employee}</span>
                            </p>
                          </div>
                          <div className="lower-range">
                            <h5>Job Role</h5>
                            <p>{list.job_type}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                <div className="col-md-6">
                  <div className="main-profiles">
                    <div className="left-agency-profile">
                      <div className="time-sheet-src">
                        <img
                          width="180px"
                          height="180px"
                          src={`${NodeURL}/${this.state.agency_avatar}`}
                          alt="Profile"
                          onError={() => {
                            this.setState({ agency_avatar: "../../img/user-profile.png" });
                          }}
                        />
                      </div>
                    </div>
                    <div className="right-agency-profile">
                      <div className="lower-range">
                        <h5>{this.state.agency_data && this.state.agency_data.company_name}</h5>
                        <p>
                          {" "}
                          {this.state.agency_data && this.state.agency_data.address ? (
                            <Fragment>
                              {this.state.agency_data.address.formatted_address},{this.state.agency_data.address.zipcode} .
                            </Fragment>
                          ) : null}{" "}
                          <br />
                          {this.state.agency_data && this.state.agency_data.address ? (
                            <Fragment>
                              Tel. : {this.state.agency_data.phone.code} - {this.state.agency_data.phone.number}
                            </Fragment>
                          ) : null}{" "}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="table-responsive mt-2">
                <Table hover responsive>
                  <thead>
                    <tr>
                      {/*<th>
                         <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.currPage)} />
                      </th>*/}
                      <th>
                        Shift No.
                      </th>

                      {/* <th
                      onClick={() => {
                        this.sort("client");
                      }}
                    >
                      Client <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client" />
                    </th>*/}
                      <th>
                        Work Location
                      </th>
                      <th>
                        Date
                      </th>
                      <th>
                        Time
                      </th>
                      <th>
                        Break
                      </th>
                      <th>
                        Time Worked
                      </th>
                      <th>
                        Status
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.adminlist.length > 0 ? (
                      this.state.adminlist.map((item, i) => (
                        <tr key={item.shiftId}>
                          {/*<td>
                                                <input type="checkbox" className="checkbox1" id={i} value={item._id} onClick={() => this.checkbox(i, item._id)} />
                                            </td>*/}
                          <td>{item.title}</td>
                          {/*<td>{item.client}</td>*/}
                          <td>{item.branch}</td>
                          <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                          <td>
                            {item.starttime} - {item.endtime}
                          </td>
                          <td>{item.breaktime / 60} Minutes</td>

                          <td>{Math.round(item.timesheet[0].workmins / 60)} Minutes</td>
                          <td>{item.timesheet_status === 2 ? <Badge color="success">Approved</Badge> : <Badge color="danger">Pending</Badge>}</td>
                        </tr>
                      ))
                    ) : (
                      <tr className="text-center">
                        <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                      </tr>
                    )}
                    {this.state.isLoader && <Loader />}
                  </tbody>
                </Table>
              </div>
            </CardBody>
            <CardFooter>
            <Button type="button" color="secondary" title="Back" onClick={this.goBack}>
                <i className="fa fa-arrow-left" /> Back
              </Button>

              {this.state.ShowApprovebtn ? (
                null
              ) : (
                <Badge color="success" className="pull-right">
                  Approved
                </Badge>
              )}
            </CardFooter>
          </Card>
        </div>
      </div>
    );
  }
}

export default timesheet;
