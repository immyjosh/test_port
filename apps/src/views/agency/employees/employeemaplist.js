import React, { Component, Fragment } from "react";
import { compose, withProps, lifecycle } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, InfoWindow, Marker } from "react-google-maps";
import { Col } from "reactstrap";
import { NodeURL } from "../../../api/api";
import { Settings } from "../../../api/key";
// import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";

const goldStar = {
  shape: { coords: [17, 17, 18], type: "circle" },
  fillColor: "yellow",
  scale: 1,
  strokeColor: "gold",
  strokeWeight: 1,
  borderColor: "green solid  5px",
  background: "#f95602"
};

const MapComponent = compose(
  withProps({
    googleMapURL: Settings.googleMapURL + Settings.googleMapKey,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div className="client-list-img" style={{ height: `100%` }} />
  }),
  lifecycle({
    componentDidMount() {
      console.log("this.props.markers.length", this.props.markers.length);
      if (this.props.markers.length) {
        this.setState({
          zoomToMarkers: map => {
            const bounds = new window.google.maps.LatLngBounds();
            this.props.markers.forEach((child, index) => {
              bounds.extend(new window.google.maps.LatLng(child.lat, child.lng));
            });
            if (map) map.fitBounds(bounds);
          }
        });
      }
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap ref={props.zoomToMarkers} defaultZoom={5} defaultCenter={{ lat: 25.0391667, lng: 121.525 }} defaultOptions={{ gestureHandling: "greedy" }}>
    {/*<MarkerClusterer onClick={props.onMarkerClustererClick} averageCenter enableRetinaIcons gridSize={60} key={props.selectClient._id}>*/}
    {/* {https://png.pngtree.com/element_origin_min_pic/00/00/06/12575cb97a22f0f.jpg} */}
    {props.markers.map(marker => {
      return (
        <div className="marger1" key={marker._id}>
          <Marker
            className="marger"
            key={marker._id}
            position={{ lat: marker.lat, lng: marker.lng }}
            icon={{
              ...goldStar,
              url: `${marker.avatar ? `${NodeURL}/${marker.avatar}` : "https://png.pngtree.com/element_origin_min_pic/00/00/06/12575cb97a22f0f.jpg"}`,
              scaledSize: new window.google.maps.Size(40, 40),
              anchor: new window.google.maps.Point(5, 5)
            }}
            onClick={() => props.client(marker)}
          >
            {props.isOpen &&
              props.selectClient._id === marker._id && (
                <InfoWindow onCloseClick={() => props.closeModel()}>
                  <Col className="full-mapdetails">
                    <Col className="left-mapdetails">
                      <img width="80px" height="80px" alt="" src={NodeURL + "/" + props.selectClient.avatar} />
                    </Col>
                    <Col className="right-mapdetails">
                      <p className="view-det map-names">
                        <span>{props.selectClient.name}</span>
                      </p>
                      <p className="view-det job_type">
                        {props.selectClient.job_type &&
                          props.selectClient.job_type.map((list, key) => (
                            <span key={key}>
                              {list} {key < props.selectClient.job_type.length - 1 ? <Fragment>,</Fragment> : <Fragment>.</Fragment>}
                            </span>
                          ))}
                      </p>
                      <p className="view-det phone">
                        <span>
                          {props.selectClient.phone.code} - {props.selectClient.phone.number}
                        </span>
                      </p>
                      <p className="view-det location">
                        {props.selectClient.locations &&
                          props.selectClient.locations.map((list, key) => (
                            <span key={key}>
                              {list} {key < props.selectClient.locations.length - 1 ? <Fragment>|</Fragment> : null}
                            </span>
                          ))}
                      </p>
                      <p className="view-det email">
                        <span>{props.selectClient.email}</span>
                      </p>
                    </Col>
                  </Col>
                </InfoWindow>
              )}
          </Marker>
        </div>
      );
    })}
    {/*</MarkerClusterer>*/}
  </GoogleMap>
));

class Employeemap extends Component {
  state = {
    selectClient: "",
    isOpen: false
  };

  clientDetail = client => {
    this.setState({
      selectClient: client,
      isOpen: true
    });
  };

  closeModel = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    return <div>{this.props.markers.length > 0 ? <MapComponent markers={this.props.markers} client={this.clientDetail} selectClient={this.state.selectClient || false} isOpen={this.state.isOpen} closeModel={this.closeModel} /> : null}</div>;
  }
}

export default Employeemap;
