/* eslint no-sequences: 0*/
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Row, Input, Popover, PopoverHeader, PopoverBody, InputGroup, InputGroupAddon } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import { Bar } from "react-chartjs-2";
import Widget02 from "../../Template/Widgets/Widget02";
import FileSaver from "file-saver";

class Editclient extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    newbranchname: "",
    workname: "",
    email: "",
    status: "",
    isverified: "",
    role: "",
    code: "",
    newbranchcode: "",
    newbranchdailcountry: "",
    code1: "",
    address: "",
    newbranchaddress: "",
    address1: "",
    newbranchnumber: "",
    number: "",
    dailcountry: "",
    number1: "",
    dailcountry1: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    newbranchzipcode: "",
    zipcode1: "",
    formatted_address: "",
    newbranchformatted_address: "",
    avatar: "",
    establishmenttype: "",
    numberofbeds: "",
    patienttype: "",
    specialrequirements: "",
    shiftpatens: "",
    agreedstaffrates: "",
    attain: "",
    invoiceaddress: "",
    invoicephone: "",
    invoicefax: "",
    invoiceemail: "",
    additionallocations: "",
    subscription: "",
    phoneerror: false,
    newbranchphoneerror: false,
    phoneerror1: false,
    phonestatus: "",
    newbranchphonestatus: "",
    phonestatus1: "",
    avatar_return: "",
    companyname: "",
    stausactive: false,
    isverifiedstaus: false,
    fax: "",
    isverifiedCheck: "",
    worklist: [{ name: "", address: "", formatted_address2: "", lat: "", lng: "" }],
    emplist: [],
    lat: "",
    lng: "",
    newbranchlat: "",
    newbranchlng: "",
    additional_locations: [],
    locationdata: [],
    formatted_address1: "",
    workmodel: false,
    activeTab: "1",
    mainbranchlocation: "",
    branchlist: [
      {
        branchname: "",
        branchformatted_address: "",
        branchzipcode: "",
        branchlocation: "",
        branchaddress: "",
        branchlat: "",
        branchlng: "",
        phone: {},
        phoneno: "",
        dailcountry: "",
        branchArea: ""
      }
    ],
    location_error: false,
    locations_error: false,
    forClient: true,
    otheractiveTab: "A1",
    activeTabE: "1E",
    filteredemplist: [],
    RowIDC: "",
    company_logo: "",
    client: this.props.location.state.rowid,
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all",
    clientdashboard: false,
    added: 0,
    accepted: 0,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    timehseet_approved: 0,
    invoice_approved: 0,
    Payment_completed: 0,
    expired: 0,
    totalshiftCount: 0,
    draft: 0,
    approved: 0,
    part_paid: 0,
    paid: 0,
    draft_rate: 0,
    approved_rate: 0,
    part_paid_rate: 0,
    paid_rate: 0,
    mailpopover: false,
    recepient: "",
    locations: []
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }

    request({
      url: "/administrators/client/get",
      method: "POST",
      data: { id: this.props.location.state.rowid, pagefor: "view" }
    }).then(res => {
      if (res.status === 1 && res.response.result[0]) {
        const clientlist = res.response.result[0];
        request({
          url: "/administrators/client/viewclientlocations",
          method: "POST",
          data: { locations: res.response.result[0].locations }
        }).then(resp => {
          if (resp.status === 1 && resp.response.result.length > 0) {
            const brlist = res.response.result[0].branches.map(list => ({
              _id: list._id,
              branchname: list.branchname,
              branchaddress: list.branchaddress,
              branchzipcode: list.branchzipcode,
              branchformatted_address: list.branchformatted_address,
              branchlat: list.branchlat,
              branchlng: list.branchlng,
              branchlocation: list.branchlocation,
              phone: list.phone,
              phoneno: list.phone.number || "",
              dailcountry: list.phone.dailcountry || "gb",
              branchArea: resp.response.result.map(area => {
                if (area._id === list.branchlocation) return area.name;
                return true;
              })
            }));
            this.setState({
              locationList: resp.response.result,
              branchlist: brlist
            });
            this.setState({
              username: clientlist.username,
              name: clientlist.name,
              email: clientlist.email,
              status: clientlist.status,
              // isverifiedCheck: clientlist.isverified,
              // isverified: clientlist.isverified,
              number: clientlist.phone.number,
              // dailcountry: clientlist.phone ? clientlist.phone.dailcountry : "gb",
              code: clientlist.phone.code,
              line1: clientlist.address.line1,
              line2: clientlist.address.line2,
              // city: clientlist.address.city,
              state: clientlist.address.state,
              country: clientlist.address.country,
              zipcode: clientlist.address.zipcode,
              formatted_address: clientlist.address.formatted_address,
              avatar_return: clientlist.avatar,
              companyname: clientlist.companyname,
              fax: clientlist.fax,
              establishmenttype: clientlist.establishmenttype,
              numberofbeds: clientlist.numberofbeds,
              patienttype: clientlist.patienttype,
              specialrequirements: clientlist.specialrequirements,
              invoiceaddress: clientlist.invoiceaddress,
              invoicephone: clientlist.invoicephone,
              invoicefax: clientlist.invoicefax,
              invoiceemail: clientlist.invoiceemail,
              locations: clientlist.locations
            });
            // const iddata = res.response.result[0].locations.map(list => list);
            // const filteredemplist = this.state.emplist.filter(item => iddata.indexOf(item.value) !== -1);
            // this.setState({ filteredemplist });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });

    this.dashboard();
  }

  newPage = page => {
    if (page === "Invoice") {
      this.props.history.push({
        pathname: "/agency/client/invoice",
        state: {
          rowid: this.props.location.state.rowid
        }
      });
    }
    if (page === "Time") {
      this.props.history.push({
        pathname: "/agency/client/timesheet",
        state: {
          rowid: this.props.location.state.rowid
        }
      });
    }
  };

  dashboard() {
    const { client } = this.state;
    if (client) {
      request({
        url: "/agency/client/dashboard",
        method: "POST",
        data: {
          client,
          selectshift: this.state.selectshift,
          selecttimesheet: this.state.selecttimesheet,
          selectinvoice: this.state.selectinvoice
        }
      }).then(res => {
        if (res.status === 1) {
          if (res.response.result && res.response.result.length > 0) {
            const clientdashboard = res.response.result[0] || false;
            this.setState({ clientdashboard });
            if (clientdashboard && clientdashboard.shifts) {
              let data = clientdashboard.shifts.length > 0 ? clientdashboard.shifts[0] : {};
              this.setState({
                added: data.added || 0,
                accepted: data.accepted || 0,
                assigned: data.assigned || 0,
                completed: data.completed || 0,
                ongoing: data.ongoing || 0,
                timehseet_approved: data.timehseet_approved || 0,
                invoice_approved: data.invoice_approved || 0,
                Payment_completed: data.Payment_completed || 0,
                expired: data.expired || 0,
                totalshiftCount:
                  (data.added ? data.added : 0) +
                  (data.accepted ? data.accepted : 0) +
                  (data.assigned ? data.assigned : 0) +
                  (data.completed ? data.completed : 0) +
                  (data.ongoing ? data.ongoing : 0) +
                  (data.timehseet_approved ? data.timehseet_approved : 0) +
                  (data.invoice_approved ? data.invoice_approved : 0) +
                  (data.Payment_completed ? data.Payment_completed : 0) +
                  (data.expired ? data.expired : 0)
              });
            }
            if (clientdashboard && clientdashboard.invoices) {
              let data = clientdashboard.invoices.length > 0 ? clientdashboard.invoices[0] : {};
              this.setState({
                draft: data.draft || 0,
                approved: data.approved || 0,
                part_paid: data.part_paid || 0,
                paid: data.paid || 0,
                draft_rate: data.draft_rate || 0,
                approved_rate: data.approved_rate || 0,
                part_paid_rate: data.part_paid_rate || 0,
                paid_rate: data.paid_rate || 0
              });
            }
          }
        }
      });
    }
  }

  editpage = e => {
    e.stopPropagation();
    return this.props.history.push({
      pathname: "/agency/editclient",
      state: { rowid: this.props.location.state.rowid }
    });
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.dashboard());
  };

  downloadPDF = purpose => {
    client.defaults.responseType = "blob";
    let toastId = "downmail0";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/agency/client/profile",
      method: "POST",
      data: { id: this.props.location.state.rowid, purpose }
    })
      .then(res => {
        if (res && res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        } else if (res) {
          const file = new Blob([res], { type: "application/pdf" });
          const name = this.state.name ? `${this.state.name} Profile.pdf` : "Profile.pdf";
          FileSaver(file, name);
          toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        }
        client.defaults.responseType = "json";
      })
      .catch(error => {
        client.defaults.responseType = "json";
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
  };

  mailPDF = purpose => {
    if (this.state.recepient) {
      let toastId = "downmail";
      toastId = toast.info("Sending......", { autoClose: false });
      request({
        url: "/agency/client/profile",
        method: "POST",
        data: { id: this.props.location.state.rowid, purpose, email: this.state.recepient }
      })
        .then(res => {
          if (res && res.status === 1) {
            toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.setState({
              mailpopover: false
            });
            this.componentDidMount();
          } else {
            toast.update(toastId, { render: res.response || "Unable To Send Mail, Try Again Later", type: toast.TYPE.ERROR, autoClose: 2500 });
          }
        })
        .catch(error => {
          toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        });
    } else {
      toast.error("Recepient Mail ID Required");
    }
  };

  MailPopOver = () => {
    this.setState({
      mailpopover: !this.state.mailpopover
    });
  };
  actAsClient = (e, id) => {
    e.stopPropagation();
    let tabOpen1 = window.open("about:blank", "newtab1");
    request({
      url: "/agency/client/actasclient",
      method: "POST",
      data: { id: id }
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUSC", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          return (
            (tabOpen1.location = "/portal/client/dashboard"),
            setTimeout(() => {
              window.location.reload();
            }, 800)
          );
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };
  render() {
    const { state } = this;
    const { clientdashboard } = this.state;
    const bar = {
      labels: ["Shifts"],
      datasets: [
        {
          label: "Total",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.totalshiftCount]
        },
        {
          label: "Assigned",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.assigned]
        },
        {
          label: "Ongoing",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.ongoing]
        },
        {
          label: "Completed",
          backgroundColor: "#4dbd74",
          borderColor: "#4dbd74",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#4dbd74",
          data: [this.state && this.state.completed]
        }
      ]
    };
    const bar1 = {
      labels: ["Invoices"],
      datasets: [
        {
          label: "Draft",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.draft_rate]
        },
        {
          label: "Approved Rate",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.approved_rate]
        },
        {
          label: "Part Paid",
          backgroundColor: "#20a8d8b8",
          borderColor: "#1382a9",
          borderWidth: 1,
          hoverBackgroundColor: "#20a8d8",
          hoverBorderColor: "#1382a9",
          data: [this.state && this.state.part_paid_rate]
        },
        {
          label: "Paid",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.paid_rate]
        }
      ]
    };
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="fa fa-desktop" />
                Client Details
                <div className="card-actions">
                  <button onClick={() => this.newPage("Time")}>
                    <i className="icon-clock" /> Timesheets
                    <small className="text-muted" />
                  </button>
                  <button onClick={() => this.newPage("Invoice")}>
                    <i className="fa fa-money" /> Invoice
                    <small className="text-muted" />
                  </button>
                  <button onClick={this.editpage}>
                    <i className="fa fa-edit" /> Edit
                    <small className="text-muted" />
                  </button>
                </div>
              </CardHeader>
              <CardBody>
                <div className="views-information">
                  <div className="info-categories">
                    <div className="row">
                      <div className="emp-full">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Personal Details </h4>

                            <div className="info-widths">
                              <div className="cate-ranges">
                                <img
                                  width="180px"
                                  height="180px"
                                  src={NodeURL + "/" + this.state.avatar_return}
                                  alt="Profile"
                                  onError={() => {
                                    this.setState({ avatar_return: "../../img/user-profile.png" });
                                  }}
                                />
                              </div>
                            </div>
                            <div className="info-widths">
                              <div className="cate-ranges">
                                <p>
                                  <span className="left-views">
                                    Username <span className="col-qus"> : </span>{" "}
                                  </span>
                                  <span className="right-views">{state.username}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Name <span className="col-qus"> : </span>{" "}
                                  </span>
                                  <span className="right-views">{state.name}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Phone <span className="col-qus"> : </span>{" "}
                                  </span>
                                  <span className="right-views">{`${state.code} ${state.number}`}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Email <span className="col-qus"> : </span>{" "}
                                  </span>
                                  <span className="right-views">{state.email}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Status <span className="col-qus"> : </span>{" "}
                                  </span>
                                  <span className="right-views">{state.status === 1 ? "Active" : "In-active"}</span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4 className="dash-foheading no-bar">
                              <span className="pull-left left-dash"> Dashboard Details </span>
                              <span className="pull-right right-dash">
                                <Button title="Act As Client" className="pull-right btn-ask ml-3" onClick={(e, id) => this.actAsClient(e, this.props.location.state.rowid)}>
                                  <i className="fa fa-sign-in" /> Act As Client
                                </Button>
                                <Button title="Download" className="pull-right btn-download ml-3" onClick={() => this.downloadPDF("download")}>
                                  <i className="fa fa-download" /> Download
                                </Button>
                                <Button title="Mail" className="pull-right btn-mail ml-3" id="mailPopover" onClick={this.MailPopOver}>
                                  <i className="fa fa-envelope-o" /> Mail
                                </Button>
                              </span>
                            </h4>
                            <Popover placement="left" isOpen={this.state.mailpopover} target="mailPopover" toggle={this.MailPopOver}>
                              <PopoverHeader>Send Mail</PopoverHeader>
                              <PopoverBody>
                                <Row className="m-2">
                                  <InputGroup>
                                    <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                                    <Input placeholder="E-mail" name="recepient" value={this.state.recepient} onChange={this.onChange} required />
                                  </InputGroup>
                                  <button className="btn btn-success mt-2 pull-right" onClick={() => this.mailPDF("mail")}>
                                    Send
                                  </button>
                                </Row>
                              </PopoverBody>
                            </Popover>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <div className="sub-ranges">
                              <h5>Shifts</h5>
                              <Row className="d-flex justify-content-end">
                                <Col md="5" xs="12">
                                  <Input type="select" name="selectshift" value={this.state.selectshift} onChange={this.onChange}>
                                    <option value="all">All</option>
                                    <option value="1">Today</option>
                                    <option value="8">Last 7 Days</option>
                                    <option value="31">Last 30 Days</option>
                                    <option value="366">Last 12 Months</option>
                                  </Input>
                                </Col>
                              </Row>
                              <div className="chart-wrapper">
                                <Bar
                                  data={bar}
                                  options={{
                                    maintainAspectRatio: false
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <div className="sub-ranges">
                              <h5>Invoice</h5>
                              <Row className="d-flex justify-content-end">
                                <Col md="5" xs="12">
                                  <Input type="select" name="selectinvoice" value={this.state.selectinvoice} onChange={this.onChange}>
                                    <option value="all">All</option>
                                    <option value="1">Today</option>
                                    <option value="8">Last 7 Days</option>
                                    <option value="31">Last 30 Days</option>
                                    <option value="366">Last 12 Months</option>
                                  </Input>
                                </Col>
                              </Row>
                              <div className="chart-wrapper">
                                <Bar
                                  data={bar1}
                                  options={{
                                    maintainAspectRatio: false
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <div className="sub-ranges wg-class">
                              <h5>Timesheets</h5>
                              <Row>
                                <Col xs="12" md="4">
                                  <Widget02 header={clientdashboard ? clientdashboard.timesheetapproved + clientdashboard.timesheetpending : "0"} mainText="Total" icon="fa fa-users" color="warning" variant="1" />
                                </Col>
                                <Col xs="12" md="4">
                                  <Widget02 header={clientdashboard.timesheetapproved ? clientdashboard.timesheetapproved : "0"} mainText="Approved" icon="fa fa-users" color="success" variant="1" />
                                </Col>
                                <Col xs="12" md="4">
                                  <Widget02 header={clientdashboard.timesheetpending ? clientdashboard.timesheetpending : "0"} mainText="Pending" icon="fa fa-user-plus" color="warning" variant="1" />
                                </Col>
                              </Row>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Organisation Details</h4>
                            <p>
                              <span className="left-views">
                                Name <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.companyname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Establishment Type <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.establishmenttype}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Type of Patients <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.patienttype}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Fax Number <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.fax}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Number of Beds <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.numberofbeds}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Special requirements <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.specialrequirements}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Logo <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">
                                {" "}
                                <img
                                  width="100px"
                                  src={NodeURL + "/" + this.state.avatar_return}
                                  alt="Profile"
                                  onError={() => {
                                    this.setState({ avatar_return: "../../img/user-profile.png" });
                                  }}
                                />
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Address</h4>
                            <p>
                              <span className="left-views">
                                Address <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.formatted_address}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Line1 <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.line1}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                City/Town <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.line2}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                State/Region <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.state}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Country <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.country}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Post Code <span className="col-qus"> : </span>{" "}
                              </span>
                              <span className="right-views">{state.zipcode}</span>
                            </p>
                            <h4 className="mt-3">Location Details</h4>
                            <p>
                              <span className="left-views">
                                Area <span className="col-qus"> : </span>{" "}
                              </span>
                              {state.locations && state.locations.length > 0 && state.locations.map((item, i) => <span key={i}>{item.name},</span>)}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="cate-ranges">
                          {state.branchlist.map(
                            (branch, index) =>
                              index ? (
							  
							    <div className="row">
                                <div className="emp-full">
								 <div className="cus-design">
                                  <h5> Other Branch </h5>
                                  <div className="row">
                                    <div className="col-md-6">
                                      <p>
                                        <span className="left-views">
                                          Branch Name <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.branchname}</span>
                                      </p>
                                      <p>
                                        <span className="left-views">
                                          Post Code <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.branchzipcode}</span>
                                      </p>
                                      <p>
                                        <span className="left-views">
                                          phone <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.phone.code && branch.phone.number ? `${branch.phone.code} ${branch.phone.number}` : null}</span>
                                      </p>
                                    </div>
                                    <div className="col-md-6">
                                      <p>
                                        <span className="left-views">
                                          Address <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.branchformatted_address}</span>
                                      </p>
                                      <p>
                                        <span className="left-views">
                                          Area <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.branchArea}</span>
                                      </p>
                                    </div>
                                  </div>
								  </div>
                                </div>
								</div>
								
                              ) : (
                                <div className="row">
                                <div className="emp-full">
								 <div className="cus-design">
                                  <h5>Main Branch</h5>
                                  <div className="row">
                                    <div className="col-md-6">
                                      <p>
                                        <span className="left-views">
                                          Branch Name <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.branchname}</span>
                                      </p>
                                      <p>
                                        <span className="left-views">
                                          Post Code <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{state.branchlist[0].branchzipcode}</span>
                                      </p>
                                      <p>
                                        <span className="left-views">
                                          phone <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{state.branchlist[0].phone.code && state.branchlist[0].phone.number ? `${state.branchlist[0].phone.code} ${state.branchlist[0].phone.number}` : null}</span>
                                      </p>
                                    </div>

                                    <div className="col-md-6">
                                      <p>
                                        <span className="left-views">
                                          Address <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{state.branchlist[0].branchformatted_address}</span>
                                      </p>
                                      <p>
                                        <span className="left-views">
                                          Area <span className="col-qus"> : </span>{" "}
                                        </span>
                                        <span className="right-views">{branch.branchArea}</span>
                                      </p>
                                    </div>
                                  </div>
                                </div>
								</div>
								</div>
                              )
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="emp-full">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Billing Details</h4>
                          </div>
                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Billing Fax <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{state.invoicefax}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Billing Phone Number <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{state.invoicephone}</span>
                              </p>
                            </div>
                          </div>
                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Billing Address <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{state.invoiceaddress}</span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Billing Email <span className="col-qus"> : </span>{" "}
                                </span>
                                <span className="right-views">{state.invoiceemail}</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </CardBody>
              <CardFooter>
                <Link to="/agency/clientlist">
                  <Button type="submit" color="secondary" title="Back">
                    <i className="fa fa-arrow-left" /> Back
                  </Button>
                </Link>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Editclient;
