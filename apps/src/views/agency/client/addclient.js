/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, CustomInput, TabContent, TabPane, Nav, NavItem, NavLink, Input } from "reactstrap";
import request, { client } from "../../../api/api";
import addnodify from "./clientlist";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import Select from "react-select";
import "react-select/dist/react-select.css";
import classnames from "classnames";
import { establishment } from "../../common/utils";
import Avatar from "react-avatar-edit";
// import ReactCrop from "react-image-crop";
// import "react-image-crop/dist/ReactCrop.css";

class Addclient extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    workname: "",
    email: "",
    role: "",
    code: "",
    number: "",
    number1: "",
    branchaddress: "",
    address: "",
    address1: "",
    line1: "",
    lng: "",
    lat: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    zipcode1: "",
    formatted_address: "",
    avatar: "",
    establishmenttype: "",
    numberofbeds: "",
    patienttype: "",
    specialrequirements: "",
    shiftpatens: "",
    agreedstaffrates: "",
    attain: "",
    invoiceaddress: "",
    invoicephone: "",
    invoicefax: "",
    invoiceemail: "",
    additionallocations: "",
    subscription: "",
    phoneerror: false,
    phoneerror1: false,
    addressactive: false,
    phonestatus: "",
    phonestatus1: "",
    companyname: "",
    company_email: "",
    dailcountry: "",
    dailcountry1: "",
    fax: "",
    status: 1,
    isverified: 1,
    emplist: [],
    additional_locations: [],
    locations: "",
    locationdata: [],
    avatarfile: "",
    formatted_address1: "",
    workmodel: false,
    activeTab: "1",
    establishment: [],
    mainbranchlocation: "",
    branchlist: [
      {
        branchname: "",
        branchformatted_address: "",
        branchline1: "",
        branchline2: "",
        branchcity: "",
        branchstate: "",
        branchcountry: "",
        branchzipcode: "",
        branchlocation: "",
        branchaddress: "",
        branchlat: "",
        branchlng: "",
        phone: {},
        phoneno: ""
      }
    ],
    location_error: false,
    locations_error: false,
    filteredemplist: [],
    company_logo: "",
    propreview: null,
    src: null,
    crop: {
      x: 120,
      y: 120,
      aspect: 16 / 9,
      maxWidth: 180,
      maxHeight: 180
    }
  };
  flash = new addnodify();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ establishment });
    request({
      url: "/agency/location/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          worklist: res.response.result
        });
        const emplist = this.state.worklist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          emplist: emplist
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  selectChange = value => {
    this.setState({ locations: value });
    if (value) {
      let iddata = value.map(list => list.value);
      let filteredemplist = this.state.emplist.filter(item => iddata.indexOf(item.value) !== -1);
      this.setState({ filteredemplist });
    }
  };
  // Changemainbranchlocation = value => {
  //   if (value !== null && value !== undefined) {
  //     this.setState({ mainbranchlocation: value.value });
  //   } else {
  //     this.setState({ mainbranchlocation: "" });
  //   }
  // };
  selectestablishmenttype = value => {
    if (value !== null && value !== undefined) {
      this.setState({ establishmenttype: value.value });
    } else {
      this.setState({ establishmenttype: "" });
    }
  };
  saveChanges = value => {
    if (value !== null && value !== undefined) {
      this.setState({ additional_locations: value.value });
    } else {
      this.setState({ additional_locations: "" });
    }
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({
      line1: "",
      line2: "",
      city: "",
      state: "",
      country: "",
      zipcode: ""
    });
    this.setState({ address });
    if (address === "") {
      this.setState({
        line1: "",
        line2: "",
        city: "",
        state: "",
        country: "",
        zipcode: "",
        formatted_address: ""
      });
    }
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name+" "+results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        // this.setState({ formatted_address: results[0].formatted_address });
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lng: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2,
      phonestatus: status
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2,
      phonestatus1: status
    });
  };
  handlerbranch = (status, value, countryData, number, id, key) => {
    if (status === true) {
      this.setState({
        phoneerrorbranch: false
      });
    } else {
      this.setState({
        phoneerrorbranch: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2,
      phonestatusbranch: status
    });
    const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, phone: { code: countryData.dialCode, number: value, dailcountry: countryData.iso2 }, phoneno: value };
        // };
      }
    });
    this.setState({ branchlist: newedu });
  };
  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          avatar: evt.target.files[0],
          avatarfile: URL.createObjectURL(evt.target.files[0]),
          avatarName: evt.target.files[0].name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  OnFormSubmit = (e, values) => {
    client.defaults.responseType = "json";
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else if (this.state.branchlist[0].branchformatted_address === "") {
      this.setState({
        branchformatted_address_error: true
      });
      toast.error("Fill Branch Address");
    } else if (this.state.branchlist[0].branchlocation === "" || this.state.branchlist.length === 0) {
      this.setState({
        branchlocation_error: true
      });
      toast.error("Select Branch Location");
    } else {
      const data = new FormData();
      this.state.locations.map((item, i) => {
        data.append("locations", item.value);
        return true;
      });
      this.state.branchlist.map((item, i) => {
        if (item.branchlocation !== "") {
          data.append(`branches[${i}][branchname]`, item.branchname);
          data.append(`branches[${i}][branchaddress]`, item.branchaddress);
          data.append(`branches[${i}][branchline1]`, item.branchline1);
          data.append(`branches[${i}][branchline2]`, item.branchline2);
          data.append(`branches[${i}][branchcity]`, item.branchcity);
          data.append(`branches[${i}][branchstate]`, item.branchstate);
          data.append(`branches[${i}][branchcountry]`, item.branchcountry);
          data.append(`branches[${i}][branchzipcode]`, item.branchzipcode);
          data.append(`branches[${i}][branchformatted_address]`, item.branchformatted_address);
          data.append(`branches[${i}][branchlat]`, item.branchlat);
          data.append(`branches[${i}][branchlng]`, item.branchlng);
          data.append(`branches[${i}][branchlocation]`, item.branchlocation);
          data.append(`branches[${i}][phone.code]`, item.phone.code || "");
          data.append(`branches[${i}][phone.number]`, item.phone.number || "");
          data.append(`branches[${i}][phone.dailcountry]`, item.phone.dailcountry || "gb");
        }
        return true;
      });
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lng]", this.state.lng);
      data.append("establishmenttype", this.state.establishmenttype);
      data.append("numberofbeds", this.state.numberofbeds);
      data.append("patienttype", this.state.patienttype);
      data.append("specialrequirements", this.state.specialrequirements);
      data.append("shiftpatens", this.state.shiftpatens);
      data.append("agreedstaffrates", this.state.agreedstaffrates);
      data.append("attain", this.state.attain);
      data.append("invoiceaddress", this.state.invoiceaddress);
      data.append("invoicephone", this.state.invoicephone);
      data.append("invoicefax", this.state.invoicefax);
      data.append("invoiceemail", this.state.invoiceemail);
      data.append("companyname", this.state.companyname);
      data.append("company_email", this.state.company_email);
      data.append("fax", this.state.fax);
      data.append("company_logo", this.state.company_logo);
      data.append("status", 1);
      data.append("isverified", this.state.isverified);
      request({
        url: "/administrators/client/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  saveuser = () => {
    return this.setState({ number: "", address: "" }), this.form && this.form.reset(), this.props.history.push("/agency/clientlist"), this.flash.addclientnodify();
  };
  worklocationModel = () => {
    this.setState({
      workmodel: !this.state.workmodel
    });
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  addBranches = () => {
    this.setState({ branchlist: this.state.branchlist.concat([{}]) });
  };
  removeBranches = idx => {
    this.setState({
      branchlist: this.state.branchlist.filter((s, sidx) => idx !== sidx)
    });
  };
  onChangeBranches = idx => e => {
    const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (idx !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, [e.target.name]: e.target.value };
      }
    });
    this.setState({ branchlist: newedu });
  };

  onChangeBranchesSelect = (key, value) => {
    if (value) {
      const newedu = this.state.branchlist.map((educationlist, sidx) => {
        if (key !== sidx) {
          return educationlist;
        } else {
          return { ...educationlist, branchlocation: value.value };
          // };
        }
      });
      this.setState({ branchlist: newedu });
      if (value.value === undefined || value.value === 0) {
        this.setState({
          branchlocation_error: true
        });
      }
      if (this.state.branchlocation) {
        this.setState({
          branchlocation_error: false
        });
      }
    }
  };
  branchhandleChange = (address, key) => {
    const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchformatted_address: address, branchaddress: address };
        // };
      }
    });
    this.setState({ branchlist: newedu });
  };
  branchhandleSelect = (address, key) => {
    const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchformatted_address: address, branchaddress: address };
        // };
      }
    });
    this.setState({ branchlist: newedu });
    geocodeByAddress(address).then(results => {
      if (results[0].address_components.length === 1) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchcountry: results[0].address_components[0].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 2) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchstate: results[0].address_components[0].long_name,
              branchcountry: results[0].address_components[1].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 3) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchcity: results[0].address_components[0].long_name,
              branchstate: results[0].address_components[1].long_name,
              branchcountry: results[0].address_components[2].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 4) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchcity: results[0].address_components[0].long_name,
              branchstate: results[0].address_components[2].long_name,
              branchcountry: results[0].address_components[3].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 5) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchline1: results[0].address_components[0].long_name,
              branchline2: results[0].address_components[1].long_name,
              branchcity: results[0].address_components[2].long_name,
              branchstate: results[0].address_components[3].long_name,
              branchcountry: results[0].address_components[4].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 6) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchline1: results[0].address_components[0].long_name,
              branchline2: results[0].address_components[1].long_name,
              branchcity: results[0].address_components[2].long_name,
              branchstate: results[0].address_components[3].long_name,
              branchcountry: results[0].address_components[4].long_name,
              branchzipcode: results[0].address_components[5].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 7) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchline1: results[0].address_components[0].long_name,
              branchline2: results[0].address_components[1].long_name,
              branchcity: results[0].address_components[3].long_name,
              branchstate: results[0].address_components[4].long_name,
              branchcountry: results[0].address_components[5].long_name,
              branchzipcode: results[0].address_components[6].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else if (results[0].address_components.length === 8) {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchline1: results[0].address_components[1].long_name,
              branchline2: results[0].address_components[2].long_name,
              branchcity: results[0].address_components[3].long_name,
              branchstate: results[0].address_components[5].long_name,
              branchcountry: results[0].address_components[6].long_name,
              branchzipcode: results[0].address_components[7].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      } else {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return {
              ...educationlist,
              branchline1: results[0].address_components[0].long_name,
              branchline2: results[0].address_components[1].long_name,
              branchcity: results[0].address_components[results[0].address_components.length - 3].long_name,
              branchstate: results[0].address_components[results[0].address_components.length - 2].long_name,
              branchcountry: results[0].address_components[results[0].address_components.length - 1].long_name
            };
          }
        });
        this.setState({ branchlist: newedu });
      }
      getLatLng(results[0]).then(latlong => {
        const newedu = this.state.branchlist.map((educationlist, sidx) => {
          if (key !== sidx) {
            return educationlist;
          } else {
            return { ...educationlist, branchlat: latlong.lat, branchlng: latlong.lng };
          }
        });
        this.setState({ branchlist: newedu });
      });
    }).catch(error => console.error("Error", error));
  };
  selectboxValidation = () => {
    if (this.state.branchlist[0].branchformatted_address === "") {
      this.setState({
        branchformatted_address_error: true
      });
      // toast.error("Select Branch Name");
    }
    if (this.state.locations === undefined || this.state.locations.length === 0) {
      this.setState({
        locations_error: true
      });
    }
    if (this.state.branchlocation === undefined || this.state.branchlocation.length === 0) {
      this.setState({
        branchlocation_error: true
      });
    }
  };
  handleInvalidSubmit(event, errors, values) {
    toast.error("Please Fill Required Fields");
  }
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      var pos = propreview.indexOf(";base64,");
      var type = propreview.substring(5, pos);
      var b64 = propreview.substr(pos + 8);
      // decode base64
      var imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      var buffer = new ArrayBuffer(imageContent.length);
      var view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      var blob = new Blob([buffer], { type: type });
      return blob;
    }
    let dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
 /* onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => this.setState({ src: reader.result }));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
  };

  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, "newFile.jpeg");
    this.setState({ croppedImageUrl: croppedImageUrl.url, company_logo: croppedImageUrl.file });
  };

  onCropChange = crop => {
    this.setState({ crop });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);
    return new Promise((resolve, reject) => {
      canvas.toBlob(file => {
        if (file) {
          file.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(file);
          resolve({ url: this.fileUrl, file: file });
        }
      }, "image/jpeg");
    });
  }*/
  fileChangedHandler1 = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          company_logo: file_data,
          logo_file: URL.createObjectURL(file_data),
          company_logo_name: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  addressChanges = e => {
    if (e.target.checked) {
      this.setState({
        addressactive: e.target.checked,
        invoiceaddress: this.state.line1+" "+
                        this.state.line2+" "+
                        this.state.city+" "+
                        this.state.state+" "+
                        this.state.country+" "+
                        this.state.zipcode
      });
    } else {
      this.setState({
        addressactive: e.target.checked
      });
    }
  };
  render() {
    // const { croppedImageUrl } = this.state;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                <CardHeader>
                  <strong>Add</strong> Client
                </CardHeader>
                <CardBody className={"p-0"}>
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "1" })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Basic Details
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "2" })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Organisation Details
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "3" })}
                        onClick={() => {
                          this.toggle("3");
                        }}
                      >
                        Work Locations
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <Row>
					    <div className="cus-design edited-section">
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Username</Label>
                            <AvInput type="text" name="username" placeholder="Enter Username" onChange={this.onChange} value={this.state.username} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Name</Label>
                            <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4" className={"for-phone-right"}>
                          <Label>Phone</Label>
                          <IntlTelInput style={{ width: "100%" }} defaultCountry={"gb"} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler} value={this.state.number} />
                          {this.state.phoneerror ? <div style={{ color: "red" }}>Enter Valid Phone Number!</div> : null}
                        </Col>
                        <Col xs="6" md="4">
                          <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                        </Col>
                        {this.state.propreview ? (
                          <Col xs="6" md="3">
                            <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                          </Col>
                        ) : null}
						</div>
                      </Row>
                     
                     
                      
                      <Row>
					   <div className="cus-design edited-section">
					   <Col xs="12" md="12">
					   <p className="h5">Login Details</p>
					   </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Email</Label>
                            <AvInput name="email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Password</Label>
                          <AvInput type="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password} autoComplete="off" />
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Confirm Password</Label>
                            <AvInput type="password" name="confirm_password" placeholder="Enter Password" onChange={this.onChange} value={this.state.confirm_password} validate={{ match: { value: "password" } }} autoComplete="off" />
                            <AvFeedback>Match Password!</AvFeedback>
                          </AvGroup>
                        </Col>
						</div>
                      </Row>
                    </TabPane>
                    <TabPane tabId="2">
                      <Row>
					   <div className="cus-design edited-section">
                        <Col xs="12" md="4">
                          <AvField type="text" label="Company/Organisation Name" name="companyname" placeholder="Enter Name.." onChange={this.onChange} value={this.state.companyname} validate={{ required: { value: true, errorMessage: "This is required!" } }} />
                        </Col><Col xs="12" md="4">
                          <AvField type="email" label="Email" name="company_email" placeholder="Enter Email.." onChange={this.onChange} value={this.state.company_email} validate={{ required: { value: true, errorMessage: "This is required!" } }} />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="number" label="Fax" name="fax" placeholder="Enter Fax.." onChange={this.onChange} value={this.state.fax} validate={{ required: { value: true, errorMessage: "This is required!" } }} />
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Establishment Tpe</Label>
                          <Select name="form-field-name2" value={this.state.establishmenttype} options={this.state.establishment} onChange={this.selectestablishmenttype} />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="number"
                            label="Number of beds"
                            name="numberofbeds"
                            placeholder="Enter Number of Beds.."
                            onChange={this.onChange}
                            value={this.state.numberofbeds}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Type of patients"
                            name="patienttype"
                            placeholder="Enter Patient Type.."
                            onChange={this.onChange}
                            value={this.state.patienttype}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="textarea"
                            label="Special requirements"
                            name="specialrequirements"
                            placeholder="Enter Special Requirements.."
                            onChange={this.onChange}
                            value={this.state.specialrequirements}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        {/* <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Shift Patens"
                            name="shiftpatens"
                            placeholder="Enter Shift Patens.."
                            onChange={this.onChange}
                            value={this.state.shiftpatens}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>*/}
                        <Col xs="6" md="4">
                          <Label for="exampleCustomFileBrowser1">Logo</Label>
                         {/* <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser1"
                            name="avatar"
                            onChange={this.onSelectFile}
                            label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                            encType="multipart/form-data"
                          />*/}
                          <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser1"
                            name="avatar"
                            onChange={this.fileChangedHandler1}
                            label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                            encType="multipart/form-data"
                          />
                        </Col>
                        {this.state.logo_file ? (
                            <Col xs="6" md="3">
                              <Label>Preview</Label> <img className="img-fluid" src={this.state.logo_file} alt="Preview" />{" "}
                            </Col>
                        ) : null}
                        {/* <Col xs="6" md="4">
                          {this.state.src && <ReactCrop src={this.state.src} crop={this.state.crop} onImageLoaded={this.onImageLoaded} onComplete={this.onCropComplete} onChange={this.onCropChange} />}
                        </Col>
                        <Col xs="6" md="4">
                          {croppedImageUrl && <img width="100%" height="180px" alt="Crop" src={croppedImageUrl} />}{" "}
                        </Col>*/}
						</div>
                      </Row>
                      
                     
                      <Row>
					  <div className="cus-design edited-section">
					   <Col xs="12" md="12">
					    <p className="h5">Address</p>
						</Col>
                        <Col xs="12" md="4">
                          <Label>Search Location</Label>
                          <PlacesAutocomplete value={this.state.formatted_address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                            {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                              <div>
                                <input
                                  {...getInputProps({
                                    placeholder: "Search Places",
                                    className: "form-control"
                                  })}
                                />
                                <div className="autocomplete-dropdown-container absolute1">
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                        backgroundColor: "#fafafa",
                                        cursor: "pointer"
                                      }
                                      : {
                                        backgroundColor: "#ffffff",
                                        cursor: "pointer"
                                      };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                          </PlacesAutocomplete>
                        </Col>
                        {/* <Col xs="12" md="4">
                          <Label className={this.state.location_error ? "error-color" : ""}>Location</Label>
                          <Select
                            className={this.state.location_error ? "custom_select_validation" : null}
                            name="mainbranchlocation"
                            value={this.state.mainbranchlocation}
                            options={this.state.filteredemplist}
                            onChange={this.Changemainbranchlocation}
                          />
                          {this.state.location_error ? <div className="error-color"> This is required!</div> : null}
                        </Col> */}
						
                      
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Line 1</Label>
                            <AvInput type="text" name="line1" placeholder="Enter line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>City/Town</Label>
                            <AvInput type="text" name="line2" placeholder="Enter line2" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        {/* <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City</Label>
                        <AvInput
                          type="text"
                          name="city"
                          placeholder="Enter city.."
                          onChange={this.onChange}
                          value={this.state.city}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col> */}
                      
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>State/Region</Label>
                            <AvInput type="text" name="state" placeholder="Enter state" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Country</Label>
                            <AvInput type="text" name="country" placeholder="Enter country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Post Code</Label>
                            <AvInput type="text" name="zipcode" placeholder="Enter zipcode" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
						</div>
                      </Row>
                      <div>
                       
                      </div>
                      <Row>
					   <div className="cus-design edited-section">
					   <Col xs="12" md="12">
					    <p className="h5"> Billing Details </p>
					   </Col>	
                        <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Billing Fax"
                            name="invoicefax"
                            minLength="4"
                            maxLength="30"
                            placeholder="Enter Billing Fax.."
                            onChange={this.onChange}
                            value={this.state.invoicefax}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        
                      
                        <Col xs="12" md="4">
                          <AvField
                            type="number"
                            min="0"
                            label="Billing Phone Number"
                            name="invoicephone"
                            placeholder="Enter Billing Phone.."
                            onChange={this.onChange}
                            value={this.state.invoicephone}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Billing Email"
                            name="invoiceemail"
                            placeholder="Enter Billing Email.."
                            onChange={this.onChange}
                            value={this.state.invoiceemail}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
						<Col xs="12" md="12">
                          <AvField
                            type="textarea"
                            label="Billing Address"
                            name="invoiceaddress"
                            minLength="4"
                            placeholder="Enter Billing Address.."
                            onChange={this.onChange}
                            value={this.state.invoiceaddress}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                          <div>
                            <label>Same as Address</label>
                          </div>
                          <Label className="switch switch-text switch-success new-switch same_address">
                            <Input type="checkbox" className="switch-input" checked={this.state.addressactive} onChange={this.addressChanges} />
                            <span className="switch-label" data-on="" data-off="" />
                            <span className="switch-handle new-handle" />
                          </Label>
                        </Col>
						</div>
                      </Row>
                    </TabPane>
                    <TabPane tabId="3">
                      <Row>
					   <div className="cus-design edited-section">
                        <Col xs="12" md="12">
                          <Label className={this.state.locations_error ? "error-color h5" : "h5"}>Area</Label>
                          <Select className={this.state.locations_error ? "error-color" : ""} name="locations" value={this.state.locations} options={this.state.emplist} onChange={this.selectChange} multi />
                          {this.state.locations_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
						</div>
                      </Row>
                      

                      {this.state.branchlist.map((edulist, key) => (
                        <Row key={key}>
						<div className="cus-design edited-section">
                          {key === 0 ? (
                            <div className="col-md-12">
                              <h5>Main Branch</h5>
                              <hr />
                            </div>
                          ) : null}
                          {key > 0 ? (
                            <div className="col-md-12">
                              <Label className="h5">Other Branches</Label>
                            </div>
                          ) : null}
                          <br />
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Branch Name</Label>
                              <AvInput type="text" name="branchname" placeholder="Enter Branch Name" onChange={this.onChangeBranches(key)} value={edulist.branchname} autoComplete="name" required />
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <Label>Search address</Label>
                            <PlacesAutocomplete value={edulist.branchformatted_address} onChange={address => this.branchhandleChange(address, key)} onSelect={address => this.branchhandleSelect(address, key)} onFocus={this.geolocate}>
                              {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                                <div>
                                  <input
                                    {...getInputProps({
                                      placeholder: "Search Places",
                                      className: "form-control"
                                    })}
                                  />
                                  <div className="autocomplete-dropdown-container absolute1">
                                    {suggestions.map(suggestion => {
                                      const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                      // inline style for demonstration purpose
                                      const style = suggestion.active
                                        ? {
                                          backgroundColor: "#fafafa",
                                          cursor: "pointer"
                                        }
                                        : {
                                          backgroundColor: "#ffffff",
                                          cursor: "pointer"
                                        };
                                      return (
                                        <div
                                          {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style
                                          })}
                                        >
                                          <span>{suggestion.description}</span>
                                        </div>
                                      );
                                    })}
                                  </div>
                                </div>
                              )}
                            </PlacesAutocomplete>
                            {this.state.branchformatted_address_error ? <div className="error-color"> This is required!</div> : null}
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Line 1</Label>
                              <AvInput type="text" name="branchline1" placeholder="Enter Line 1" onChange={this.onChangeBranches(key)} value={edulist.branchline1} autoComplete="name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Line 2</Label>
                              <AvInput type="text" name="branchline2" placeholder="Enter Line 2" onChange={this.onChangeBranches(key)} value={edulist.branchline2} autoComplete="name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>City</Label>
                              <AvInput type="text" name="branchcity" placeholder="Enter City" onChange={this.onChangeBranches(key)} value={edulist.branchcity} autoComplete="name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>State</Label>
                              <AvInput type="text" name="branchstate" placeholder="Enter State" onChange={this.onChangeBranches(key)} value={edulist.branchstate} autoComplete="name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Country</Label>
                              <AvInput type="text" name="branchcountry" placeholder="Enter Country" onChange={this.onChangeBranches(key)} value={edulist.branchcountry} autoComplete="name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <AvGroup>
                              <Label>Post Code</Label>
                              <AvInput type="text" name="branchzipcode" placeholder="Enter Post Code" onChange={this.onChangeBranches(key)} value={edulist.branchzipcode} autoComplete="name" required />
                              <AvFeedback>This is required!</AvFeedback>
                            </AvGroup>
                          </Col>
                          <Col xs="12" md="4">
                            <Label className={this.state.branchlocation_error ? "error-color" : ""}>Area</Label>
                            <Select name="branchlocation" value={edulist.branchlocation} options={this.state.filteredemplist} onChange={value => this.onChangeBranchesSelect(key, value)} />
                            {this.state.branchlocation_error ? <div className="error-color"> This is required!</div> : null}
                          </Col>
                          <Col xs="12" md="4" className={"for-phone-right"}>
                            <Label>Phone</Label>
                            <IntlTelInput
                              style={{ width: "100%" }}
                              defaultCountry={"gb"}
                              utilsScript={libphonenumber}
                              css={["intl-tel-input", "form-control"]}
                              onPhoneNumberChange={(status, value, countryData, number, id) => this.handlerbranch(status, value, countryData, number, id, key)}
                              value={edulist.phoneno}
                            />
                            {this.state.phoneerrorbranch ? <div style={{ color: "red" }}>Enter Valid Phone Number!</div> : null}
                          </Col>
                          <Col xs="12" md="2" className={"mt-4 text-right"}>
                            {/* {key === 0 ? (
                                <Button color="success" onClick={this.addBranches}>
                                  <i className="fa fa-plus" />
                                </Button>
                              ) : null} */}
                            {key > 0 ? (
                              <Button
                                onClick={() => {
                                  this.removeBranches(key);
                                }}
                                color="danger"
                                className=""
                              >
                                <i className="fa fa-minus" />
                              </Button>
                            ) : null}
                          </Col>
						  </div>
                        </Row>
                      ))}
                    </TabPane>
                  </TabContent>
                </CardBody>
                <CardFooter>
                  {this.state.activeTab === "2" || this.state.activeTab === "3" ? (
                    <Button
                      color="secondary pull-left mb-3"
                      onClick={() => {
                        this.toggle((parseInt(this.state.activeTab, Number) - 1).toString());
                      }}
                    >
                      Previous
                    </Button>
                  ) : null}
                  {this.state.activeTab === "1" || this.state.activeTab === "2" ? (
                    <Button
                      color="info pull-right mb-3"
                      onClick={() => {
                        this.toggle((parseInt(this.state.activeTab, Number) + 1).toString());
                      }}
                    >
                      Next
                    </Button>
                  ) : null}
                  {this.state.activeTab === "3" ? (
                    <Button
                      type="submit"
                      color="success pull-right mb-3"
                      title="Add Client"
                      onClick={() => {
                        this.phonefield(this.state.number);
                        this.selectboxValidation();
                      }}
                    >
                      Add
                    </Button>
                  ) : null}
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addclient;
