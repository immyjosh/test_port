/*eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import { Card, CardBody, CardHeader, Table } from "reactstrap";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import Loader from "../../common/loader";
import request from "../../../api/api";

class Recentclientlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adminlist: [],
      url: "",
      adminredirect: false,
      deletedisable: true,
      sortOrder: true,
      isLoader: false,
      bulk: [],
      count: 0,
      pages: "",
      currPage: 25,
      tableOptions: {
        search: "",
        filter: "createdAt",
        page: {
          history: "",
          current: 1
        },
        order: "",
        field: "",
        limit: 10,
        skip: 0,
        to_date: "",
        from_date: ""
      },
      start_date: "",
      end_date: ""
    };
  }
  clientlistdeletetoastId = "clientlist";
  delete() {
    if (!toast.isActive(this.clientlistdeletetoastId)) {
      this.clientlistdeletetoastId = toast.success("Deleted!");
    }
  }
  componentDidMount() {
    const token = this.props.token_role;
    // if (!token || (token && (!token.username || token.role !== "agency"))) {
    //   return this.props.history.replace("/agency");
    // }
    this.setState({ isLoader: true });
    request({
      url: "/administrators/client/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        if (res) {
          this.setState({
            isLoader: false,
            adminlist: res.response.result || {},
            pages: res.response.fullcount,
            currPage: res.response.length
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  render() {
    return (
      <div className="animated">
        {/*<ToastContainer position="top-right" autoClose={2500} />*/}
        <Card>
          <CardHeader
            onClick={() => {
              this.props.history.push("/agency/clientlist");
            }}
            className="cursor-pointer"
          >
            <i className="icon-menu" />
            Recent Client List
          </CardHeader>
          <CardBody className="p-1">
            <div className="row d-flex justify-content-end" />
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.email}</td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default withRouter(Recentclientlist);
