import fileDownload from "js-file-download";
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../../common/loader";
import moment from "moment";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Table,
} from "reactstrap";
import request, { NodeURL, client } from "../../../../api/api";
import { breaklist, timelist } from "../../../common/utils";
import FileSaver from "file-saver";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "130px"
};

class Invoicegroupdetails extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    singlecheck: false,
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    timesheet_endtimeview: "",
    timesheet_starttimeview: "",
    status_timesheet: "",
    tableOptions: {
      id: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid,
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 100,
      skip: 0,
      to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : "",
      // employeeID: this.props && this.props.location.state ? this.props.location.state.id._id.employee[0]._id : "",
      clientID: this.props && this.props.location.state ? this.props.location.state.id._id.client[0]._id : ""
      // locationID: this.props && this.props.location.state ? this.props.location.state.id._id.locations[0]._id : "",
      // branchID: this.props && this.props.location.state ? this.props.location.state.id._id.branch[0].branches._id : "",
      // jobtypeID: this.props && this.props.location.state ? this.props.location.state.id._id.job_type[0]._id : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    viewtimesheetid: "",
    timesheet: [],
    client_avatar: "",
    agency_avatar: "",
    client_data: {},
    ForediState: false,
    ShowApprovebtn: true,
    INVCurrentTotal: "",
    InvoiceCycleDetails: "",
    invoicePrefix: "",
    invoice_to_date: "",
    invoice_from_date: "",
    currentusername: "",
    invoice_due_date: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      timelist,
      breaklist,
      currentusername: token && token.username,
      isLoader: true,
      invoice_to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      invoice_from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : ""
    });
    request({
      url: "/agency/clientinvoice/employee/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            if (earn._id === 7) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 6) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            }
            return true;
          });

          this.setState({
            timesheetlists: res.response.result
          });
          let totalrate,
            clienttotalrate,
            latetotalrate = 0;
          let clientAr = res.response.result.map(list => list.client_rate);
          clienttotalrate = clientAr.reduce((a, b) => a + b);
          let lateAr = res.response.result.map(list => list.late_amount);
          latetotalrate = lateAr.reduce((a, b) => a + b);
          totalrate = clienttotalrate + latetotalrate;
          let getshiftdata = res.response.result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            let evestarthr = item.starttime / 3600;
            let splithr = evestarthr.toString().split(".");
            let startsec = splithr[1];
            let startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
            let starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            let eveendhr = item.endtime / 3600;
            let splitendhr = eveendhr.toString().split(".");
            let endsec = splitendhr[1];
            let endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
            let endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            let timesheet_status = item.timesheet_status ? item.timesheet_status : "";
            return {
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              job_rate: item.rate_details.length > 0 ? item.rate_details[0].client_rate : "",
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.shift_id,
              shiftId: item.shift_id,
              shift_data: item.shift_data,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            };
          });
          this.setState({
            adminlist: getshiftdata,
            pages: res.response.fullcount,
            currPage: res.response.length,
            total_rate: totalrate,
            clienttotal_rate: clienttotalrate,
            latetotal_rate: latetotalrate,
            client_avatar: res.response.result.length > 0 ? res.response.result[0].client_data[0].company_logo : "",
            agency_avatar: res.response.result.length > 0 ? res.response.result[0].agency_data[0].company_logo : "",
            client_data: res.response.result.length > 0 ? res.response.result[0].client_data[0] : {},
            agency_data: res.response.result.length > 0 ? res.response.result[0].agency_data[0] : {}
          });
          let invoicePrefix = "";
          const TimeID = res.response.result.filter(list => list.invoiceID);
          if (TimeID.length > 0) {
            invoicePrefix = TimeID[0].invoiceID;
            this.GetInvoice(invoicePrefix);
          } else {
            request({
              url: "/site/invoiceprefixnumber",
              method: "POST",
              data: { for: "client", username: this.state.currentusername }
            }).then(res => {
              if (res.status === 1) {
                invoicePrefix = res.response;
              } else if (res.status === 0) {
                if (res.response === "invoice Settings Not Available") {
                  this.props.history.push("/agency/client/invoice");
                  toast.error("Please Contact Agency");
                } else {
                  toast.error(res.response);
                }
              }
            });
          }
          setTimeout(() => {
            this.setState({ invoicePrefix });
          }, 1000);
          const time_Sta = res.response.result.filter(list => list.status === 1);
          if (time_Sta.length === 0) {
            this.setState({ ShowApprovebtn: false });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  populateData() {
    request({
      url: "/agency/clientinvoice/employee/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          let getshiftdata = [];
          this.setState({
            timesheetlists: res.response.result
          });
          res.response.result.map((item, i) => {
            var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            var evestarthr = item.starttime / 3600;
            var splithr = evestarthr.toString().split(".");
            var startsec = splithr[1];
            if (startsec === undefined) {
              var startmins = "00";
            } else {
              startmins = (+"0" + "." + startsec) * 60;
            }

            if (num.indexOf(splithr[0]) === -1) {
              var starttimehr = splithr[0] + ":" + startmins;
            } else {
              starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
            }
            var eveendhr = item.endtime / 3600;
            var splitendhr = eveendhr.toString().split(".");
            var endsec = splitendhr[1];
            if (endsec === undefined) {
              var endmins = "00";
            } else {
              endmins = (+"0" + "." + endsec) * 60;
            }

            if (num.indexOf(splitendhr[0]) === -1) {
              var endtimehr = splitendhr[0] + ":" + endmins;
            } else {
              endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
            }
            if (item.timesheet_status) {
              var timesheet_status = item.timesheet_status;
            } else {
              timesheet_status = "";
            }
            getshiftdata.push({
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.shift_id,
              shiftId: item.shift_id,
              shift_data: item.shift_data,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            });
            this.setState({
              adminlist: getshiftdata,
              pages: res.response.fullcount,
              currPage: res.response.length
            });
            return true;
          });
          const time_Sta = res.response.result.filter(list => list.status === 1);
          if (time_Sta.length === 0) {
            this.setState({ ShowApprovebtn: false });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  GetInvoice(value) {
    request({
      url: "/agency/clientinvoice/getinvoicecycle",
      method: "POST",
      data: { invoiceID: value }
    }).then(res => {
      if (res.status === 1) {
        let CurrentRate = 0;
        if (res.response && res.response.payment_details && res.response.payment_details.length > 0) {
          let CurrAr = res.response.payment_details.map(list => list.amount);
          CurrentRate = CurrAr && CurrAr.length > 0 && CurrAr.reduce((a, b) => a + b);
        }
        this.setState({
          InvoiceCycleDetails: res.response,
          INVCurrentTotal: CurrentRate,
          invoice_due_date: res.response.due_date ? res.response.due_date : ""
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["starttime", "start_date", "employee", "title", "status"];
    for (let i in id) {
      if(document.getElementById(id[i])){
        document.getElementById(id[i]).className = sorticondef;
      }
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/administrators/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  TimesheettoastId = "Timesheet";
  nodify() {
    if (!toast.isActive(this.TimesheettoastId)) {
      this.TimesheettoastId = toast.warn("Updated");
    }
  }
  adminsavenodifytoastId = "Timesheet";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifytoastId)) {
      this.adminsavenodifytoastId = toast.success("Successfully Saved!");
    }
  }
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ timesheet_starttime: value.value, selecterror: false });
    } else {
      this.setState({ timesheet_starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ timesheet_endtime: value.value, selectenderror: false });
    } else {
      this.setState({ timesheet_endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktimesheet: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktimesheet: "", selectbreakerror: true });
    }
  };
  InvoiceDownloadPDF = value => {
    let toastId = "Down_Invoice_02";
    if (value === "download") {
      client.defaults.responseType = "blob";
      toastId = toast.info("Downloading......", { autoClose: false });
    } else {
      client.defaults.responseType = "json";
      toastId = toast.info("Sending......", { autoClose: false });
    }
    if (this.state.invoicePrefix) {
      request({
        url: "/agency/clientinvoice/downloadpdf",
        method: "POST",
        data: {
          TabOpt: this.state.tableOptions,
          for: value,
          invoice_invoicePrefix: this.state.invoicePrefix,
          invoice_to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
          invoice_from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
        }
      })
        .then(res => {
          if (res.status === 1) {
            if ((res.msg = "Mail Sent")) {
              toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
              this.componentDidMount();
            }
          } else if (res.status === 0) {
            toast.error(res.response);
          } else {
            if (res) {
              const file = new Blob([res], { type: "application/pdf" });
              const name = `Invoice_${this.state.invoicePrefix}.pdf`;
              FileSaver(file, name);
              toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
              this.componentDidMount();
            }
          }
            client.defaults.responseType = "json";
        })
        .catch(error => {
          client.defaults.responseType = "json";
          toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        });
    } else {
      client.defaults.responseType = "json";
      toast.update(toastId, { render: "Unable to Download", type: toast.TYPE.ERROR, autoClose: 2500 });
    }
  };
  goBack = () => {
    window.history.go(-1);
  }
  render() {
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Invoice Details
            <div className="card-actions" style={tStyle}>
              <button style={tStyles} onClick={() => this.InvoiceDownloadPDF("download")}>
                <i className="fa fa-download" /> Download
                <small className="text-muted" />
              </button>
             {/* <button style={tStyles} onClick={() => this.InvoiceDownloadPDF("mail")}>
                <i className="fa fa-envelope" /> Mail
                <small className="text-muted" />
              </button>*/}
            </div>
          </CardHeader>
          <CardBody>
            <div className="new_invoice_pages">
              <div className="invoice_logos" />
              <div className="top_address_wraps">
                <div className="col-md-6">
                  <div className="left_invoive_add">
                    <h1>INVOICE</h1>
                    <p>
                      {this.state.client_data.companyname}
                      <br />
                      {this.state.client_data.address ? (
                        <Fragment>
                          {this.state.client_data.address.formatted_address},{this.state.client_data.address.zipcode} .
                        </Fragment>
                      ) : null}{" "}
                      <br />
                      {this.state.client_data.address ? (
                        <Fragment>
                          Tel. : {this.state.client_data.phone.code} - {this.state.client_data.phone.number}
                        </Fragment>
                      ) : null}{" "}
                    </p>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="col-md-12 crop-img">
                    <img
                      width="250px"
                      src={`${NodeURL}/${this.state.agency_avatar}`}
                      alt="Profile"
                      onError={() => {
                        this.setState({ agency_avatar: "../../img/user-profile.png" });
                      }}
                    />
                  </div>
                  <div className="right_invoive_add">
                    <div className="col-md-6">
                      <h1>Invoice Date </h1>
                      <p>
                        {moment(this.state.invoice_from_date).format("DD-MM-YYYY")} - {moment(this.state.invoice_to_date).format("DD-MM-YYYY")}{" "}
                      </p>
                      <h1>Invoice Number</h1>
                      <p>{this.state.invoicePrefix}</p>
                    </div>
                    <div className="col-md-6">
                      <p>
                        {" "}
                        {this.state.agency_data ? <Fragment>{this.state.agency_data.company_name},</Fragment> : null} <br />
                        {this.state.agency_data && this.state.agency_data.address ? (
                          <Fragment>
                            {this.state.agency_data.address.formatted_address},{this.state.agency_data.address.zipcode} .
                          </Fragment>
                        ) : null}{" "}
                        <br />
                        {this.state.agency_data && this.state.agency_data.address ? (
                          <Fragment>
                            Tel. : {this.state.agency_data.phone.code} - {this.state.agency_data.phone.number}
                          </Fragment>
                        ) : null}{" "}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="table-responsive mt-2">
              <Table hover responsive>
                <thead>
                  <tr>
                    {/*<th>
                                        <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.currPage)} />
                                    </th>*/}
                    <th
                    // onClick={() => {
                    //   this.sort("title");
                    // }}
                    >
                      Shift No.
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" /> */}
                    </th>

                    <th
                    // onClick={() => {
                    //   this.sort("branch");
                    // }}
                    >
                      Work Location
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("employee");
                    // }}
                    >
                      Employee
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("start_date");
                    // }}
                    >
                      Date
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("starttime");
                    // }}
                    >
                      Time
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("job_rate");
                    // }}
                    >
                      Rate
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_rate" /> */}
                    </th>
                    <th>Time Worked</th>
                    <th
                    // onClick={() => {
                    //   this.sort("client_rate");
                    // }}
                    >
                      Total Rate
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client_rate" /> */}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item.shiftId}>
                        {/*<td>
                                                <input type="checkbox" className="checkbox1" id={i} value={item._id} onClick={() => this.checkbox(i, item._id)} />
                                            </td>*/}
                        <td>{item.title}</td>
                        <td>{item.branch}</td>
                        <td>{item.employee}</td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        <td>£ {item.job_rate}</td>
                        <td>{Math.round(item.shift_data[0].timesheet[0].workmins / 60)} Minutes</td>
                        <td>£ {item.client_rate}</td>
                      </tr>
                    ))
                  ) : (
                      <tr className="text-center">
                        <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                      </tr>
                    )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
              <div className="overall-total ">
                <div className="total-price">
                  <ul>
                    <li>
                      <span className="pull-left">Late Booking Fee</span> <span className="pull-right">£ {this.state.latetotal_rate}</span>
                    </li>
                    <li>
                      <span className="pull-left align-width-left">
                        <b>Total</b>
                      </span>{" "}
                      <span className="align-width-right">£ {this.state.total_rate}</span>
                    </li>
                    <li>
                      <span className="pull-left align-width-left">Due Amount</span>{" "}
                      <span className="align-width-right">£ {this.state.total_rate - this.state.INVCurrentTotal}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

          </CardBody>
          <CardFooter>
            <Button type="button" color="secondary" title="Back" onClick={this.goBack}>
              <i className="fa fa-arrow-left" /> Back
              </Button>
          </CardFooter>
          <div className="due_payment">
            <div className="inner_dues">
              <h1>Due Date: {moment(this.state.invoice_due_date).format("DD-MM-YYYY")} </h1>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

export default Invoicegroupdetails;
