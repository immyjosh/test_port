import fileDownload from "js-file-download";
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../../common/loader";
import moment from "moment";
import { Button, Card, CardBody, CardHeader, CardFooter, Row, Col, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../../api/api";
import Pagination from "react-js-pagination";
import { breaklist, timelist } from "../../../common/utils";
import "react-select/dist/react-select.css";
import Widget02 from "../../../Template/Widgets/Widget02";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "110px"
};

class Invoice extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    timesheet_endtimeview: "",
    timesheet_starttimeview: "",
    status_timesheet: "",
    tableOptions: {
      id: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid,
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      status : "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    viewtimesheetid: "",
    timesheet: [],
    datesList: [],
    datesvalue: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ timelist, breaklist, isLoader: true });
    request({
      url: "/agency/clientinvociedates",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        const datesList = res.response.datelist.map((list, key) => ({
          //value: key,
          label: list.billingcycyle,
          value: `${list.start_date} / ${list.end_date}`
        }));
        datesList.unshift({
          label: `${moment(res.response.lastinvoice).format("DD/MM/YYYY")} - All`,
          value: `${moment(res.response.lastinvoice)} / ${new Date()}`
        })
        this.setState({ datesList, datesvalue: datesList[0] });
        const parts = datesList[0].value.split("/");
        this.setState(state => {
          state.tableOptions.from_date = parts[0];
          state.tableOptions.to_date = parts[1];
        });
        this.populateData();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  populateData() {
    request({
      url: "/agency/clientinvoice/list/groupbyemployee",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            /* if (earn._id === 7) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 6) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            } */
            if (earn._id === 4) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 3) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            }
            if (earn._id === 2) {
              this.setState({
                approved: earn.client_rate,
                approvedcount: earn.count
              });
            }
            if (earn._id === 1) {
              this.setState({
                draft: earn.client_rate,
                draftcount: earn.count
              });
            }
            return true;
          });
          this.setState({
            timesheetlists: res.response.result,
            adminlist: res.response.result,
            pages: res.response.fullcount,
            currPage: res.response.length
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["client", "shifts", "date", "duedate", "overdueby", "paid", "due"];
    for (let i in id) {
      if (document.getElementById(id[i])) {
        document.getElementById(id[i]).className = sorticondef;
      }
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/clientinvoice/list/export",
      method: "POST",
      data: {
        id: this.state.tableOptions.id
      }
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }

  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/client/invoicegroup",
      state: { rowid: this.props.location.state.rowid, id, Dates: { from: this.state.tableOptions.from_date, to: this.state.tableOptions.to_date } }
    });
  };

  changeDates = value => {
    if (value) {
      this.setState({ datesvalue: value.value });
      let parts = value.value.split("/");
      this.setState(state => {
        state.tableOptions.from_date = parts[0];
        state.tableOptions.to_date = parts[1];
      });
      this.populateData();
    }
  };

  goBack = () => {
    window.history.go(-1);
  }

  invoiceStatus = num => {
    if(num){
      this.setState(state => {
        state.tableOptions.status = num;
        this.populateData();
      });
    }else{
        this.setState(state => {
          state.tableOptions.status = "";
          this.populateData();
        });
    }
  }

  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.invoiceStatus(2)} header={this.state.approved ? <Fragment>£ {this.state.approved}</Fragment> : "0"} mainText="Due" icon="fa fa-check" color="warning" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.invoiceStatus(3)} header={this.state.pending ? <Fragment>£ {this.state.pending}</Fragment> : "0"} mainText="Part Paid" icon="fa fa-clock-o" color="primary" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.invoiceStatus(4)} header={this.state.earning ? <Fragment>£ {this.state.earning}</Fragment> : "0"} mainText="Paid" icon="fa fa-money" color="success" />
          </Col>
        </Row>
        <Card>
          <CardHeader>
            <i className="icon-list" />
            <span className="cursor-pointer" onClick={()=>this.invoiceStatus("")}>Invoice List</span>
            <div className="card-actions" style={tStyle} onClick={() => this.export()}>
              <button style={tStyles}>
                <i className="fa fa-upload" /> Export
                <small className="text-muted" />
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row">
              <div className="col-lg-7 new_deisn_warp">
                <div className="col-lg-7">
                  <Select
                    name="timezone"
                    className="time-zone-style"
                    value={this.state.datesvalue}
                    options={this.state.datesList}
                    onChange={this.changeDates}
                  />
                </div>
              </div>
              <div className="col-lg-5 pr-0">
                <InputGroup>
                  <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                    <option>All</option>
                  </Input>
                  <Input
                    type="text"
                    ref="search"
                    placeholder="Search"
                    name="search"
                    onChange={e => this.search(e.target.value)}
                    className="rounded-0 col-lg-6"
                  />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>Number</th>
                    <th
                      onClick={() => {
                        this.sort("client");
                      }}
                    >
                      To
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("shifts");
                      }}
                    >
                      Shifts <i style={{ paddingLeft: "25px" }} className={sorticondef} id="shifts" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("date");
                      }}
                    >
                      Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="date" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("duedate");
                      }}
                    >
                      Due Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="duedate" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("overdueby");
                      }}
                    >
                      Overdue by <i style={{ paddingLeft: "25px" }} className={sorticondef} id="overdueby" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("paid");
                      }}
                    >
                      Paid <i style={{ paddingLeft: "25px" }} className={sorticondef} id="paid" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("due");
                      }}
                    >
                      Due <i style={{ paddingLeft: "25px" }} className={sorticondef} id="due" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={i} onClick={e => this.editpage(item)}>
                        <td>{item.inv_data && item.inv_data[0].invoiceID}</td>
                        <td>{item.client[0]}</td>
                        <td>{item.count}</td>
                        <td>{moment(item.inv_data && item.inv_data[0].createdAt).format('DD-MM-YYYY')}</td>
                        <td>{moment(item.inv_data && item.inv_data[0].due_date).format('DD-MM-YYYY')}</td>
                        <td> {moment(item.inv_data && item.inv_data[0].due_date).diff(moment(item.inv_data && item.inv_data[0].createdAt), 'days')} Days</td>
                        <td>£ {item.payment_amount[0]}</td>
                        <td>£ {(item.client_rate + item.late_amount) - Number(item.payment_amount[0])}</td>
                        <td>
                          <div>
                            <button type="button" title="Edit" className="btn btn-primary btn-sm rounded-0" id={`edit${i}`} onClick={e => this.editpage(item)}>
                              <i className="fa fa-eye" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit${i}`}>
                              View
                            </UncontrolledTooltip>
                          </div>
                        </td>
                      </tr>
                    ))
                  ) : (
                      <tr className="text-center">
                        <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                      </tr>
                    )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>

            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
          <CardFooter>
            <Button type="button" color="secondary" title="Back" onClick={this.goBack}>
              <i className="fa fa-arrow-left" /> Back
              </Button>
          </CardFooter>
        </Card>
      </div>
    );
  }
}

export default Invoice;
