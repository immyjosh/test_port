/* eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Input, CustomInput, TabContent, TabPane, Nav, NavItem, NavLink, Table, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import request, { client, NodeURL } from "../../../api/api";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { toast, ToastContainer } from "react-toastify";
import classnames from "classnames";
import Select from "react-select";
import "react-select/dist/react-select.css";
import { establishment } from "../../common/utils";
import JobtypeListClient from "../jobtype/listjobtypeclient";
import Avatar from "react-avatar-edit";
// import ReactCrop from "react-image-crop";
// import "react-image-crop/dist/ReactCrop.css";

class Editclient extends Component {
  state = {
    username: "",
    password: "",
    confirm_password: "",
    name: "",
    newbranchname: "",
    workname: "",
    email: "",
    status: "",
    isverified: "",
    role: "",
    code: "",
    newbranchcode: "",
    newbranchdailcountry: "",
    code1: "",
    address: "",
    newbranchaddress: "",
    newbranchline1: "",
    newbranchline2: "",
    newbranchcity: "",
    newbranchstate: "",
    newbranchcountry: "",
    address1: "",
    newbranchnumber: "",
    number: "",
    dailcountry: "",
    number1: "",
    dailcountry1: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    newbranchzipcode: "",
    zipcode1: "",
    formatted_address: "",
    newbranchformatted_address: "",
    avatar: "",
    establishmenttype: "",
    numberofbeds: "",
    patienttype: "",
    specialrequirements: "",
    shiftpatens: "",
    agreedstaffrates: "",
    attain: "",
    invoiceaddress: "",
    invoicephone: "",
    invoicefax: "",
    invoiceemail: "",
    additionallocations: "",
    subscription: "",
    phoneerror: false,
    newbranchphoneerror: false,
    phoneerror1: false,
    phonestatus: "",
    newbranchphonestatus: "",
    phonestatus1: "",
    avatar_return: "",
    companyname: "",
    company_email: "",
    stausactive: false,
    addressactive: false,
    isverifiedstaus: false,
    fax: "",
    isverifiedCheck: "",
    worklist: [{ name: "", address: "", formatted_address2: "", lat: "", lng: "" }],
    emplist: [],
    lat: "",
    lng: "",
    newbranchlat: "",
    newbranchlng: "",
    additional_locations: [],
    locations: "",
    locationdata: [],
    formatted_address1: "",
    workmodel: false,
    activeTab: "1",
    mainbranchlocation: "",
    branchlist: [
      {
        branchname: "",
        branchformatted_address: "",
        branchline1: "",
        branchline2: "",
        branchcity: "",
        branchstate: "",
        branchcountry: "",
        branchzipcode: "",
        branchlocation: "",
        branchaddress: "",
        branchlat: "",
        branchlng: "",
        phone: {},
        phoneno: "",
        dailcountry: "",
        status: ""
      }
    ],
    location_error: false,
    locations_error: false,
    forClient: true,
    otheractiveTab: "A1",
    activeTabE: "1E",
    filteredemplist: [],
    RowIDC: "",
    company_logo: "",
    propreview: null,
    src: null,
    crop: {
      x: 120,
      y: 120,
      aspect: 16 / 9,
      maxWidth: 180,
      maxHeight: 180
    },
    addbranchmodal: false,
    editbranchmodal: false,
    Editbranchlist: []
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  addressChanges = e => {
    if (e.target.checked) {
      this.setState({
        addressactive: e.target.checked,
        invoiceaddress: this.state.line1+" "+
                        this.state.line2+" "+
                        this.state.city+" "+
                        this.state.state+" "+
                        this.state.country+" "+
                        this.state.zipcode
      });
    } else {
      this.setState({
        addressactive: e.target.checked
      });
    }
  };
  isverifiedChanges = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      isverifiedstaus: e.target.checked,
      isverified: value
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      RowIDC: this.props && this.props.location.state.rowid,
      activeTab: this.props.location && this.props.location.state && this.props.location.state.activetabs ? this.props.location.state.activetabs : "1"
    });
    this.setState({ establishment });
    request({
      url: "/agency/location/list",
      method: "POST"
      // data: this.state.tableOptions
    }).then(res => {
      if (res) {
        this.setState({
          worklist: res.response.result
          // pages: res.response.fullcount,
          // currPage: res.response.length
        });
        const emplist = this.state.worklist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          emplist: emplist,
          locations: emplist
        });
      }
    });
    request({
      url: "/administrators/client/get",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.status === 1) {
        const adminlist = res.response.result && res.response.result.length > 0 ? res.response.result[0] : {};
        const brlist =
          adminlist &&
          adminlist.branches.map(list => ({
            _id: list._id,
            branchname: list.branchname,
            branchaddress: list.branchaddress,
            branchzipcode: list.branchzipcode,
            branchline1: list.branchline1,
            branchline2: list.branchline2,
            branchstate: list.branchstate,
            branchcity: list.branchcity,
            branchcountry: list.branchcountry,
            branchformatted_address: list.branchformatted_address,
            branchlat: list.branchlat,
            branchlng: list.branchlng,
            branchlocation: list.branchlocation,
            phone: list.phone,
            status: list.status,
            phoneno: list.phone.number || "",
            dailcountry: list.phone.dailcountry || "gb"
          }));
        this.setState({
          username: adminlist.username,
          name: adminlist.name,
          email: adminlist.email,
          status: adminlist.status,
          isverifiedCheck: adminlist.isverified,
          isverified: adminlist.isverified,
          number: adminlist.phone.number,
          dailcountry: adminlist.phone ? adminlist.phone.dailcountry : "gb",
          code: adminlist.phone.code,
          line1: adminlist.address.line1,
          line2: adminlist.address.line2,
          city: adminlist.address.city,
          state: adminlist.address.state,
          country: adminlist.address.country,
          zipcode: adminlist.address.zipcode,
          formatted_address: adminlist.address.formatted_address,
          mainbranchlocation: adminlist.address.location,
          avatar_return: adminlist.avatar,
          // avatarName: adminlist.avatar.substring(24, 60),
          companyname: adminlist.companyname,
          company_email: adminlist.company_email,
          fax: adminlist.fax,
          establishmenttype: adminlist.establishmenttype,
          numberofbeds: adminlist.numberofbeds,
          patienttype: adminlist.patienttype,
          specialrequirements: adminlist.specialrequirements,
          shiftpatens: adminlist.shiftpatens,
          agreedstaffrates: adminlist.agreedstaffrates,
          attain: adminlist.attain,
          invoiceaddress: adminlist.invoiceaddress,
          invoicephone: adminlist.invoicephone,
          invoicefax: adminlist.invoicefax,
          invoiceemail: adminlist.invoiceemail,
          locations: adminlist.locations,
          // additional_locations: adminlist.additional_locations,
          logo_return: adminlist.company_logo,
          company_logo_name: adminlist.company_logo && adminlist.company_logo.substring(24, 60),
          branchlist: brlist,
          stausactive: adminlist.status === 1 ? true : false,
          isverifiedstaus: adminlist.isverified === 1 ? true : false
        });
        const iddata = res.response.result[0].locations.map(list => list);
        const filteredemplist = this.state.emplist.filter(item => iddata.indexOf(item.value) !== -1);
        this.setState({ filteredemplist });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  selectChange = value => {
    this.setState({ locations: value });
    if (value) {
      const iddata = value.map(list => list.value);
      const filteredemplist = this.state.emplist.filter(item => iddata.indexOf(item.value) !== -1);
      this.setState({ filteredemplist });
    }
  };
  // Changemainbranchlocation = value => {
  //   this.setState({ mainbranchlocation: value.value });
  // };
  selectestablishmenttype = value => {
    this.setState({ establishmenttype: value.value });
  };
  saveChanges = value => {
    this.setState({ additional_locations: value });
  };
  handleChange = address => {
    this.setState({ formatted_address: address });
    this.setState({ address });
  };
  handleChangenewbranch = address => {
    this.setState({ newbranchformatted_address: address });
    this.setState({ newbranchaddress: address });
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2,
      phonestatus1: status
    });
  };
  phonefield1 = value => {
    if (value === "") {
      this.setState({
        phoneerror1: true
      });
    }
  };
  handleAddedu = () => {
    this.setState({ worklist: this.state.worklist.concat([{}]) });
  };
  handleRemove = idx => () => {
    this.setState({
      worklist: this.state.worklist.filter((s, sidx) => idx !== sidx)
    });
  };

  handleeduChange = idx => e => {
    const newedu = this.state.worklist.map((worklist, sidx) => {
      if (idx !== sidx) {
        return worklist;
      } else {
        return { ...worklist, [e.target.name]: e.target.value };
      }
    });
    this.setState({ worklist: newedu });
  };

  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name+" "+results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        // this.setState({ formatted_address: results[0].formatted_address });
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lng: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handleSelectnewbranch = address => {
    this.setState({ newbranchaddress: address });
    this.setState({ newbranchformatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            newbranchcountry: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            newbranchstate: results[0].address_components[0].long_name,
            newbranchcountry: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            newbranchcity: results[0].address_components[0].long_name,
            newbranchstate: results[0].address_components[1].long_name,
            newbranchcountry: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            newbranchcity: results[0].address_components[0].long_name,
            newbranchstate: results[0].address_components[2].long_name,
            newbranchcountry: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            newbranchline1: results[0].address_components[0].long_name,
            newbranchline2: results[0].address_components[1].long_name,
            newbranchcity: results[0].address_components[2].long_name,
            newbranchstate: results[0].address_components[3].long_name,
            newbranchcountry: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            newbranchline1: results[0].address_components[0].long_name,
            newbranchline2: results[0].address_components[1].long_name,
            newbranchcity: results[0].address_components[2].long_name,
            newbranchstate: results[0].address_components[3].long_name,
            newbranchcountry: results[0].address_components[4].long_name,
            newbranchzipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            newbranchline1: results[0].address_components[0].long_name,
            newbranchline2: results[0].address_components[1].long_name,
            newbranchcity: results[0].address_components[3].long_name,
            newbranchstate: results[0].address_components[4].long_name,
            newbranchcountry: results[0].address_components[5].long_name,
            newbranchzipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            newbranchline1: results[0].address_components[1].long_name,
            newbranchline2: results[0].address_components[2].long_name,
            newbranchcity: results[0].address_components[3].long_name,
            newbranchstate: results[0].address_components[5].long_name,
            newbranchcountry: results[0].address_components[6].long_name,
            newbranchzipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            newbranchline1: results[0].address_components[0].long_name,
            newbranchline2: results[0].address_components[1].long_name,
            newbranchcity: results[0].address_components[results[0].address_components.length - 3].long_name,
            newbranchstate: results[0].address_components[results[0].address_components.length - 2].long_name,
            newbranchcountry: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ newbranchlat: latlong.lat });
          this.setState({ newbranchlng: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  fileChangedHandler1 = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          company_logo: file_data,
          company_logo_flie: URL.createObjectURL(file_data),
          company_logo_name: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  OnFormSubmit = (e, values) => {
    client.defaults.responseType = "json";
    e.persist();
    if (this.state.phonestatus === false || this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    }
    // else if (this.state.mainbranchlocation === "") {
    //   this.setState({
    //     location_error: true
    //   });
    //   toast.error("Select Main Branch Location");
    // }
    else if (this.state.locations === undefined || this.state.locations.length === 0) {
      this.setState({
        locations_error: true
      });
      toast.error("Select Locations");
    } else {
      const data = new FormData();
      this.state.locations.map((item, i) => {
        data.append("locations", item.value || item);
        return true;
      });
      if (this.state.branchlist) {
        this.state.branchlist.map((item, i) => {
          if (item.branchlocation !== "") {
            data.append(`branches[${i}][branchname]`, item.branchname);
            data.append(`branches[${i}][branchaddress]`, item.branchaddress);
            data.append(`branches[${i}][branchzipcode]`, item.branchzipcode);
            data.append(`branches[${i}][branchformatted_address]`, item.branchformatted_address);
            data.append(`branches[${i}][branchlat]`, item.branchlat);
            data.append(`branches[${i}][branchlng]`, item.branchlng);
            data.append(`branches[${i}][branchlocation]`, item.branchlocation);
            data.append(`branches[${i}][phone.code]`, item.phone.code);
            data.append(`branches[${i}][phone.number]`, item.phone.number);
            data.append(`branches[${i}][phone.dailcountry]`, item.phone.dailcountry);
          }
          return true;
        });
      }
      data.append("_id", this.props.location.state.rowid);
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      // data.append("address[location]", this.state.mainbranchlocation);
      data.append("address[lat]", this.state.lat);
      data.append("address[lng]", this.state.lng);
      data.append("establishmenttype", this.state.establishmenttype);
      data.append("numberofbeds", this.state.numberofbeds);
      data.append("patienttype", this.state.patienttype);
      data.append("specialrequirements", this.state.specialrequirements);
      data.append("shiftpatens", this.state.shiftpatens);
      data.append("agreedstaffrates", this.state.agreedstaffrates);
      data.append("attain", this.state.attain);
      data.append("invoiceaddress", this.state.invoiceaddress);
      data.append("invoicephone", this.state.invoicephone);
      data.append("invoicefax", this.state.invoicefax);
      data.append("invoiceemail", this.state.invoiceemail);
      data.append("companyname", this.state.companyname);
      data.append("company_email", this.state.company_email);
      data.append("fax", this.state.fax);
      data.append("company_logo", this.state.company_logo);
      data.append("status", this.state.status);
      data.append("isverified", this.state.isverified);
      request({
        url: "/administrators/client/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
            this.setState({ src: null, croppedImageUrl: null });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  toastId = "clientEdit";
  saveuser = () => {
    if (!toast.isActive(this.toastId)) {
      this.toastId = toast.success("Updated");
    }
    this.componentDidMount();
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2,
      phonestatus: status
    });
  };
  handlernewbranch = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        newbranchphoneerror: false
      });
    } else {
      this.setState({
        newbranchphoneerror: true
      });
    }
    this.setState({
      newbranchnumber: value,
      newbranchcode: countryData.dialCode,
      newbranchdailcountry: countryData.iso2,
      newbranchphonestatus: status
    });
  };
  handlerbranch = (status, value, countryData, number, id, key) => {
    if (status === true) {
      this.setState({
        phoneerrorbranch: false
      });
    } else {
      this.setState({
        phoneerrorbranch: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      phonestatusbranch: status
    });
    /* const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, phone: { code: countryData.dialCode, number: value, dailcountry: countryData.iso2 }, phoneno: value };
        // };
      }
    });
    this.setState({ branchlist: newedu }); */
    const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, phone: { code: countryData.dialCode, number: value, dailcountry: countryData.iso2 }, phoneno: value };
        // };
      }
    });
    this.setState({ Editbranchlist: newedu });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarName: evt.target.files[0].name
    });
  };
  worklocationModel = () => {
    this.setState({
      workmodel: !this.state.workmodel
    });
  };
  handleChange1 = address => {
    this.setState({ formatted_address1: address });
    this.setState({ address1: address });
  };
  handleSelect1 = address => {
    this.setState({ address1: address });
    this.setState({ formatted_address1: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 6) {
          this.setState({
            zipcode1: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            zipcode1: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            zipcode1: results[0].address_components[7].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lng: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  addBranches = () => {
    this.setState({ branchlist: this.state.branchlist.concat([{}]) });
  };
  removeBranches = idx => {
    this.setState({
      branchlist: this.state.branchlist.filter((s, sidx) => idx !== sidx)
    });
  };
  onChangeBranches = idx => e => {
    const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
      if (idx !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, [e.target.name]: e.target.value };
      }
    });
    this.setState({ Editbranchlist: newedu });
    /* const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (idx !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, [e.target.name]: e.target.value };
        // };
      }
    }); */
    // this.setState({ branchlist: newedu });
  };
  onChangeBranchesSelect = (key, value) => {
    const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchlocation: value.value };
        // };
      }
    });
    this.setState({ Editbranchlist: newedu });
    /* const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchlocation: value.value };
        // };
      }
    });
    this.setState({ branchlist: newedu }); */
  };
  onChangenmewBranchesSelect = value => {
    if (value) {
      this.setState({ newbranchlocation: value.value });
    }
  };
  branchhandleChange = (address, key) => {
    /* const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchformatted_address: address, branchaddress: address };
      }
    });
    this.setState({ branchlist: newedu }); */
    const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchformatted_address: address, branchaddress: address };
      }
    });
    this.setState({ Editbranchlist: newedu });
  };
  branchhandleSelect = (address, key) => {
    /* const newedu = this.state.branchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return { ...educationlist, branchformatted_address: address, branchaddress: address };
        // };
      }
    });
    this.setState({ branchlist: newedu });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchcountry: results[0].address_components[0].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 2) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchstate: results[0].address_components[0].long_name,
                branchcountry: results[0].address_components[1].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 3) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchcity: results[0].address_components[0].long_name,
                branchstate: results[0].address_components[1].long_name,
                branchcountry: results[0].address_components[2].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 4) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchcity: results[0].address_components[0].long_name,
                branchstate: results[0].address_components[2].long_name,
                branchcountry: results[0].address_components[3].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 5) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[2].long_name,
                branchstate: results[0].address_components[3].long_name,
                branchcountry: results[0].address_components[4].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 6) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[2].long_name,
                branchstate: results[0].address_components[3].long_name,
                branchcountry: results[0].address_components[4].long_name,
                branchzipcode: results[0].address_components[5].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 7) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[3].long_name,
                branchstate: results[0].address_components[4].long_name,
                branchcountry: results[0].address_components[5].long_name,
                branchzipcode: results[0].address_components[6].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else if (results[0].address_components.length === 8) {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[1].long_name,
                branchline2: results[0].address_components[2].long_name,
                branchcity: results[0].address_components[3].long_name,
                branchstate: results[0].address_components[5].long_name,
                branchcountry: results[0].address_components[6].long_name,
                branchzipcode: results[0].address_components[7].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        } else {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[results[0].address_components.length - 3].long_name,
                branchstate: results[0].address_components[results[0].address_components.length - 2].long_name,
                branchcountry: results[0].address_components[results[0].address_components.length - 1].long_name
              };
            }
          });
          this.setState({ branchlist: newedu });
        }
        getLatLng(results[0]).then(latlong => {
          const newedu = this.state.branchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return { ...educationlist, branchlat: latlong.lat, branchlng: latlong.lng };
            }
          });
          this.setState({ branchlist: newedu });
        });
      }).catch(error => console.error("Error", error));
    */
    const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
      if (key !== sidx) {
        return educationlist;
      } else {
        return {
          ...educationlist,
          branchformatted_address: address,
          branchaddress: address
        };
      }
    });
    this.setState({ Editbranchlist: newedu });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchcountry: results[0].address_components[0].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 2) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchstate: results[0].address_components[0].long_name,
                branchcountry: results[0].address_components[1].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 3) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchcity: results[0].address_components[0].long_name,
                branchstate: results[0].address_components[1].long_name,
                branchcountry: results[0].address_components[2].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 4) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchcity: results[0].address_components[0].long_name,
                branchstate: results[0].address_components[2].long_name,
                branchcountry: results[0].address_components[3].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 5) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[2].long_name,
                branchstate: results[0].address_components[3].long_name,
                branchcountry: results[0].address_components[4].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 6) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[2].long_name,
                branchstate: results[0].address_components[3].long_name,
                branchcountry: results[0].address_components[4].long_name,
                branchzipcode: results[0].address_components[5].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 7) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[3].long_name,
                branchstate: results[0].address_components[4].long_name,
                branchcountry: results[0].address_components[5].long_name,
                branchzipcode: results[0].address_components[6].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else if (results[0].address_components.length === 8) {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[1].long_name,
                branchline2: results[0].address_components[2].long_name,
                branchcity: results[0].address_components[3].long_name,
                branchstate: results[0].address_components[5].long_name,
                branchcountry: results[0].address_components[6].long_name,
                branchzipcode: results[0].address_components[7].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        } else {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return {
                ...educationlist,
                branchline1: results[0].address_components[0].long_name,
                branchline2: results[0].address_components[1].long_name,
                branchcity: results[0].address_components[results[0].address_components.length - 3].long_name,
                branchstate: results[0].address_components[results[0].address_components.length - 2].long_name,
                branchcountry: results[0].address_components[results[0].address_components.length - 1].long_name
              };
            }
          });
          this.setState({ Editbranchlist: newedu });
        }
        getLatLng(results[0]).then(latlong => {
          const newedu = this.state.Editbranchlist.map((educationlist, sidx) => {
            if (key !== sidx) {
              return educationlist;
            } else {
              return { ...educationlist, branchlat: latlong.lat, branchlng: latlong.lng };
            }
          });
          this.setState({ Editbranchlist: newedu });
        });
      })
      .catch(error => console.error("Error", error));
  };
  selectboxValidation = () => {
    // if (this.state.mainbranchlocation === "") {
    //   this.setState({
    //     location_error: true
    //   });
    // }
    if (this.state.locations === undefined || this.state.locations.length === 0) {
      this.setState({
        locations_error: true
      });
    }
  };
  otherToggle(tab) {
    if (this.state.otheractiveTab !== tab) {
      this.setState({
        otheractiveTab: tab
      });
    }
  }
  handleInvalidSubmit(event, errors, values) {
    toast.error("Please Fill Required Fields");
  }
  addnewBranches = () => {
    if (
      (this.state.newbranchname === undefined || "") &&
      (this.state.newbranchline1 === undefined || "") &&
      (this.state.newbranchline2 === undefined || "") &&
      (this.state.newbranchcity === undefined || "") &&
      (this.state.newbranchstate === undefined || "") &&
      (this.state.newbranchcountry === undefined || "") &&
      (this.state.newbranchformatted_address === undefined || "") &&
      (this.state.newbranchcode === undefined || "") &&
      (this.state.newbranchdailcountry === undefined || "") &&
      (this.state.newbranchlocation === undefined || "") &&
      (this.state.newbranchnumber === undefined || "")
    ) {
      toast.error("Please Fill All Reqiured Fields");
    } else {
      request({
        url: "/agency/client/addbranch",
        method: "POST",
        data: {
          clientID: this.props.location.state.rowid || this.state.RowIDC,
          branchname: this.state.newbranchname,
          branchaddress: this.state.newbranchaddress,
          branchline1: this.state.newbranchline1,
          branchline2: this.state.newbranchline2,
          branchcity: this.state.newbranchcity,
          branchstate: this.state.newbranchstate,
          branchcountry: this.state.newbranchcountry,
          branchzipcode: this.state.newbranchzipcode,
          branchformatted_address: this.state.newbranchformatted_address,
          branchlat: this.state.newbranchlat,
          branchlng: this.state.newbranchlng,
          branchlocation: this.state.newbranchlocation,
          code: this.state.newbranchcode,
          number: this.state.newbranchnumber,
          dailcountry: this.state.newbranchdailcountry,
          status: 1
        }
      })
        .then(res => {
          if (res.status === 1) {
            toast.success("New Branch Added");
            this.componentDidMount();
            this.setState({
              newbranchname: "",
              newbranchaddress: "",
              newbranchline1: "",
              newbranchline2: "",
              newbranchcity: "",
              newbranchstate: "",
              newbranchcountry: "",
              newbranchzipcode: "",
              newbranchformatted_address: "",
              newbranchlat: "",
              newbranchlng: "",
              newbranchlocation: "",
              newbranchcode: "",
              newbranchnumber: "",
              newbranchdailcountry: ""
            });
            this.setState({ activeTab: "3", addbranchmodal: false });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  deletenewBranches = value => {
    request({
      url: "/agency/client/deletebranch",
      method: "POST",
      data: {
        clientID: this.props.location.state.rowid,
        branchID: value,
        status: 0
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Branch Deleted");
          this.componentDidMount();
          this.setState({ activeTab: "3" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  updatenewBranches = value => {
    if (
      value._id === "" &&
      value.branchname === "" &&
      value.branchlocation === "" &&
      value.branchzipcode === "" &&
      value.branchformatted_address === "" &&
      value.branchline1 === "" &&
      value.branchline2 === "" &&
      value.branchcity === "" &&
      value.branchstate === "" &&
      value.branchcountry === "" &&
      value.branchlat === "" &&
      value.branchlng === "" &&
      value.phone.number === "" &&
      value.phone.dailcountry === "" &&
      value.phone.code === ""
    ) {
      toast.error("Please Fill All Reqiured Fields");
    } else {
      request({
        url: "/agency/client/updatebranch",
        method: "POST",
        data: {
          clientID: this.props.location.state.rowid,
          branchID: value._id,
          branchname: value.branchname,
          branchaddress: value.branchaddress,
          branchzipcode: value.branchzipcode,
          branchformatted_address: value.branchformatted_address,
          branchline1: value.branchline1,
          branchline2: value.branchline2,
          branchcity: value.branchcity,
          branchstate: value.branchstate,
          branchcountry: value.branchcountry,
          branchlat: value.branchlat,
          branchlng: value.branchlng,
          branchlocation: value.branchlocation,
          code: value.phone.code,
          number: value.phone.number,
          dailcountry: value.phone.dailcountry,
          status: 1
        }
      })
        .then(res => {
          if (res.status === 1) {
            toast.success("Branch Updated");
            this.componentDidMount();
            this.setState({ activeTab: "3", editbranchmodal: false });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  toggleE(tab) {
    if (this.state.activeTabE !== tab) {
      this.setState({
        activeTabE: tab
      });
    }
  }
  onClose = () => {
    this.setState({ propreview: null });
  };
  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      const pos = propreview.indexOf(";base64,");
      const type = propreview.substring(5, pos);
      const b64 = propreview.substr(pos + 8);
      // decode base64
      const imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      const buffer = new ArrayBuffer(imageContent.length);
      const view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      const blob = new Blob([buffer], { type: type });
      return blob;
    }
    const dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => this.setState({ src: reader.result }));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
  };

  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, "newFile.jpeg");
    this.setState({ croppedImageUrl: croppedImageUrl.url, company_logo: croppedImageUrl.file });
  };

  onCropChange = crop => {
    this.setState({ crop });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);
    return new Promise((resolve, reject) => {
      canvas.toBlob(file => {
        if (file) {
          file.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(file);
          resolve({ url: this.fileUrl, file: file });
        }
      }, "image/jpeg");
    });
  }
  toggleAddbranch = () => {
    this.setState({
      addbranchmodal: !this.state.addbranchmodal
    });
  };
  toggleEditbranch = () => {
    this.setState({
      editbranchmodal: !this.state.editbranchmodal
    });
  };
  EditnewBranches = value => {
    if (value) {
      const Editbranchlist = this.state.branchlist.filter(list => list._id === value);
      this.setState({ Editbranchlist, editbranchmodal: true });
    }
  };
  viewpage = id => {
    return this.props.history.push({
      pathname: "/agency/viewclient",
      state: { rowid: id }
    });
  };
  render() {
    // const { croppedImageUrl } = this.state;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              <AvForm onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                <CardHeader>
                  <strong>Edit</strong> Client
                  <div className="card-actions" >
                    <button onClick={id => this.viewpage(this.props.location.state.rowid)}>
                      <i className="fa fa-eye" /> View Details
                      {/* <small className="text-muted" />*/}
                    </button>
                  </div>
                </CardHeader>
                <CardBody className={"p-0"}>
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "1" })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Basic Details
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "2" })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Organisation Details
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "3" })}
                        onClick={() => {
                          this.toggle("3");
                        }}
                      >
                        Work Locations
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "5" })}
                        onClick={() => {
                          this.toggle("5");
                        }}
                      >
                        Job Roles & Rates
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <Row>
					    <div className="cus-design edited-section">
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Username</Label>
                            <AvInput type="text" name="username" placeholder="Enter Username" onChange={this.onChange} value={this.state.username} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Name</Label>
                            <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4" className={"for-phone-right"}>
                          <Label>Phone</Label>
                          {this.state && this.state.dailcountry ? (
                            <IntlTelInput
                              style={{ width: "100%" }}
                              defaultCountry={this.state.dailcountry}
                              utilsScript={libphonenumber}
                              css={["intl-tel-input", "form-control"]}
                              onPhoneNumberChange={this.handler}
                              value={this.state.number}
                            />
                          ) : null}
                          {this.state.phoneerror ? <div style={{ color: "red" }}>Enter Valid Phone Number!</div> : null}
                        </Col>
						
                      
                        <Col xs="6" md="4">
                          <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                        </Col>
                        {this.state.propreview ? (
                          <Col xs="6" md="3">
                            <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                          </Col>
                        ) : null}
                        <Col xs="6" md="3">
                          <Label>Profile Image</Label>
                          <img
                            width="180px"
                            height="180px"
                            src={NodeURL + "/" + this.state.avatar_return}
                            alt="Profile"
                            onError={() => {
                              this.setState({ avatar_return: "../../img/user-profile.png" });
                            }}
                          />
                        </Col>
						</div>
                      </Row>
                      
                     
                      <Row>
					   <div className="cus-design edited-section">
					    <Col xs="12" md="12">
						 <p className="h5">Login Details</p>
						</Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Email</Label>
                            <AvInput name="email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Password</Label>
                          <AvInput type="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password} autoComplete="off" />
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Confirm Password</Label>
                            <AvInput type="password" name="confirm_password" placeholder="Enter Password" onChange={this.onChange} value={this.state.confirm_password} validate={{ match: { value: "password" } }} autoComplete="off" />
                            <AvFeedback>Match Password!</AvFeedback>
                          </AvGroup>
                        </Col>
						
                        <Col xs="12" md="4">
                          <div>
                            <label>Status</label>
                          </div>
                          <Label className="switch switch-text switch-success new-switch">
                            <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                            <span className="switch-label" data-on="active" data-off="inactive" />
                            <span className="switch-handle new-handle" />
                          </Label>
                        </Col>
                        {this.state.isverifiedCheck === 0 ? (
                          <Col xs="12" md="4">
                            <div>
                              <label>Verified</label>
                            </div>
                            <Label className="switch switch-text switch-success">
                              <Input type="checkbox" className="switch-input" checked={this.state.isverifiedstaus} onChange={this.isverifiedChanges} />
                              <span className="switch-label" data-on="yes" data-off="no" />
                              <span className="switch-handle" />
                            </Label>
                          </Col>
                        ) : null}
						</div>
                      
                      </Row>
                    </TabPane>
                    <TabPane tabId="2">
                      <Row>
					    <div className="cus-design edited-section">
                        <Col xs="12" md="4">
                          <AvField type="text" label="Company/Organisation Name" name="companyname" placeholder="Enter Name" onChange={this.onChange} value={this.state.companyname} validate={{ required: { value: true, errorMessage: "This is required!" } }} />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Email"
                            name="company_email"
                            placeholder="Enter Email"
                            onChange={this.onChange}
                            value={this.state.company_email}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="number" label="Fax Number" name="fax" placeholder="Enter Fax Number" onChange={this.onChange} value={this.state.fax} validate={{ required: { value: true, errorMessage: "This is required!" } }} />
                        </Col>
                        <Col xs="12" md="4">
                          <Label>Establishment Type</Label>
                          <Select name="form-field-name2" value={this.state.establishmenttype} options={this.state.establishment} onChange={this.selectestablishmenttype} />
                        </Col>
						
                        <Col xs="12" md="4">
                          <AvField
                            type="number"
                            label="Number of Beds"
                            name="numberofbeds"
                            placeholder="Enter Number of Beds"
                            onChange={this.onChange}
                            value={this.state.numberofbeds}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Type of Patients"
                            name="patienttype"
                            placeholder="Enter Patient Type"
                            onChange={this.onChange}
                            value={this.state.patienttype}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="textarea"
                            label="Special requirements"
                            name="specialrequirements"
                            placeholder="Enter Special Requirements"
                            onChange={this.onChange}
                            value={this.state.specialrequirements}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
						
                      
                        {/* <Col xs="6" md="4">
                          <Label for="exampleCustomFileBrowser1">Logo</Label>
                          <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser1"
                            name="company_logo_name"
                            onChange={this.fileChangedHandler1}
                            label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                            encType="multipart/form-data"
                          />
                        </Col> */}
                        <Col xs="6" md="4">
                          <Label for="exampleCustomFileBrowser1">Logo</Label>
                          <CustomInput
                            type="file"
                            id="exampleCustomFileBrowser1"
                            name="company_logo"
                            onChange={this.fileChangedHandler1}
                            label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                            encType="multipart/form-data"
                          />
                        </Col>
                        {/* <Col xs="6" md="4">
                          {this.state.src && <ReactCrop src={this.state.src} crop={this.state.crop} onImageLoaded={this.onImageLoaded} onComplete={this.onCropComplete} onChange={this.onCropChange} />}
                        </Col>
                        <Col xs="6" md="4">
                          {croppedImageUrl && <img width="100%" height="180px" alt="Crop" src={croppedImageUrl} />}{" "}
                        </Col>*/}
                        {this.state.company_logo_flie ? (
                          <Col xs="6" md="3">
                            <Label>Preview</Label> <img className="img-fluid" src={this.state.company_logo_flie} alt="Preview" />{" "}
                          </Col>
                        ) : null}
                        {this.state.logo_return ? (
                          <Col xs="6" md="3">
                            <img width="180px" height="140px" className="img-fluid" src={NodeURL + "/" + this.state.logo_return} alt="Profile" />
                          </Col>
                        ) : null}
						</div>
                      </Row>
                     
                      <Row>
					    <div className="cus-design edited-section">
						  <Col xs="12" md="12">
							<p className="h5">Address</p>
						  </Col>
                        <Col xs="12" md="4">
                          <Label>Search Location</Label>
                          <PlacesAutocomplete value={this.state.formatted_address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                            {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                              <div>
                                <input
                                  {...getInputProps({
                                    placeholder: "Search Places",
                                    className: "form-control"
                                  })}
                                />
                                <div className="autocomplete-dropdown-container absolute1">
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                          backgroundColor: "#fafafa",
                                          cursor: "pointer"
                                        }
                                      : {
                                          backgroundColor: "#ffffff",
                                          cursor: "pointer"
                                        };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                          </PlacesAutocomplete>
                        </Col>
                        {/* <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Post Code</Label>
                            <AvInput type="text" name="zipcode" placeholder="Enter Post Code" onChange={this.onChange} value={this.state.zipcode} autoComplete="name" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>*/}
						
                     
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Line1</Label>
                            <AvInput type="text" name="line1" placeholder="Enter line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>City/Town</Label>
                            <AvInput type="text" name="line2" placeholder="Enter line2" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        {/* <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City</Label>
                        <AvInput
                          type="text"
                          name="city"
                          placeholder="Enter city"
                          onChange={this.onChange}
                          value={this.state.city}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                      </Col> */}
					 
                     
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>State/Region</Label>
                            <AvInput type="text" name="state" placeholder="Enter state" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Country</Label>
                            <AvInput type="text" name="country" placeholder="Enter country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="4">
                          <AvGroup>
                            <Label>Post Code</Label>
                            <AvInput type="text" name="zipcode" placeholder="Enter zipcode" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
						 </div>
                      </Row>
                      
                      <Row>
					   <div className="cus-design edited-section">
					   <Col xs="12" md="12">
					     <p className="h5">Billing Details</p>
						</Col> 
                        <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Billing Fax"
                            name="invoicefax"
                            minLength="4"
                            maxLength="30"
                            placeholder="Enter Billing Fax"
                            onChange={this.onChange}
                            value={this.state.invoicefax}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
						
						<Col xs="12" md="4">
                          <AvField
                            type="number"
                            min="0"
                            label="Billing Phone Number"
                            name="invoicephone"
                            placeholder="Enter Billing Phone"
                            onChange={this.onChange}
                            value={this.state.invoicephone}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
						
                       
                      
                        
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Billing Email"
                            name="invoiceemail"
                            placeholder="Enter Billing Email"
                            onChange={this.onChange}
                            value={this.state.invoiceemail}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                        </Col>
						
						 <Col xs="12" md="12">
                          <AvField
                            type="textarea"
                            label="Billing Address"
                            name="invoiceaddress"
                            minLength="4"
                            placeholder="Enter Billing Address"
                            onChange={this.onChange}
                            value={this.state.invoiceaddress}
                            validate={{ required: { value: true, errorMessage: "This is required!" } }}
                          />
                          <div>
                            <label>Same as Address</label>
                          </div>
                          <Label className="switch switch-text switch-success new-switch">
                            <Input type="checkbox" className="switch-input" checked={this.state.addressactive} onChange={this.addressChanges} />
                            <span className="switch-label" data-on="yes" data-off="no" />
                            <span className="switch-handle new-handle" />
                          </Label>
                        </Col>
						
						
						</div>
                      </Row>
                    </TabPane>
                    <TabPane tabId="3">
                      <Row>
                        <Col xs="12" md="12">
                          <Label className={this.state.locations_error ? "error-color h5" : "h5"}>Area</Label>
                          <Select className={this.state.locations_error ? "error-color" : ""} name="locations" value={this.state.locations} options={this.state.emplist} onChange={this.selectChange} multi />
                          {this.state.locations_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                      </Row>
                      <br />
                      <div className="d-flex justify-content-end">
                        <Button type="button" className="btn-add" onClick={this.toggleAddbranch}>
                          Add New Branch
                        </Button>
                      </div>
                      <div className="table-responsive mt-2 view-table">
                        <Table hover bordered responsive size="sm">
                          <thead>
                            <tr>
                              <th>S.No.</th>
                              <th>Name</th>
                              <th>Address</th>
                              <th>Area</th>
                              <th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.branchlist.length > 0 ? (
                              this.state.branchlist.map((item, i) => (
                                <tr key={i}>
                                  <td>{i + 1}</td>
                                  <td>{item.branchname}</td>
                                  <td>
                                    {item.branchaddress}- {item.branchzipcode}
                                  </td>
                                  <td>
                                    {this.state.emplist.map(list => {
                                      if (list.value === item.branchlocation) {
                                        return list.label;
                                      }
                                      return true;
                                    })}
                                  </td>
                                  <td>
                                    <Button
                                      type="button"
                                      onClick={() => {
                                        this.EditnewBranches(item._id);
                                      }}
                                     
                                      title="Edit"
                                      className="mr-2 table-edit"
                                    >
                                      <i className="fa fa-edit" />
                                    </Button>
                                    {i > 0 ? (
                                      <Button
                                        type="button"
                                        onClick={() => {
                                          this.deletenewBranches(item._id);
                                        }}                                   
                                        className="pull-right table-edit"
                                        title="Delete"
                                      >
                                        <i className="fa fa-minus" />
                                      </Button>
                                    ) : null}
                                  </td>
                                </tr>
                              ))
                            ) : (
                              <tr className="text-center">
                                <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                              </tr>
                            )}
                          </tbody>
                        </Table>
                      </div>
                    </TabPane>
                    <TabPane tabId="5">
                      <JobtypeListClient forClient={{ clientactive: this.state.forClient, clientid: this.props.location.state.rowid }} />
                    </TabPane>
                  </TabContent>
                </CardBody>
                <CardFooter>
                  {this.state.activeTab !== "5" ? (
                    <Button type="submit" color="primary pull-right" title="Update">
                      <i
                        className="fa fa-dot-circle-o"
                        onClick={() => {
                          this.phonefield(this.state.number);
                          this.selectboxValidation();
                        }}
                      />{" "}
                      Update
                    </Button>
                  ) : null}
                  <Button
                    color="secondary"
                    title="Back"
                    onClick={() => {
                      window.history.go(-1);
                    }}
                  >
                    <i className="fa fa-arrow-left" /> Back
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
        <Modal isOpen={this.state.addbranchmodal} toggle={this.toggleAddbranch} className={this.props.className}>
          <ModalHeader>Add New Branch</ModalHeader>
          <ModalBody>
            <AvForm>
              <Row>
                <Col xs="12" md="12">
                  <AvGroup>
                    <Label>Branch Name</Label>
                    <AvInput type="text" name="newbranchname" placeholder="Enter Branch Name" onChange={this.onChange} value={this.state.newbranchname} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="12">
                  <Label>Search address</Label>
                  <PlacesAutocomplete value={this.state.newbranchformatted_address} onChange={this.handleChangenewbranch} onSelect={this.handleSelectnewbranch} onFocus={this.geolocate}>
                    {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                      <div>
                        <input
                          {...getInputProps({
                            placeholder: "Search Places",
                            className: "form-control"
                          })}
                        />
                        <div className="autocomplete-dropdown-container absolute1">
                          {suggestions.map(suggestion => {
                            const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                            // inline style for demonstration purpose
                            const style = suggestion.active
                              ? {
                                  backgroundColor: "#fafafa",
                                  cursor: "pointer"
                                }
                              : {
                                  backgroundColor: "#ffffff",
                                  cursor: "pointer"
                                };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style
                                })}
                              >
                                <span>{suggestion.description}</span>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
                  </PlacesAutocomplete>
                </Col>
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>Line 1</Label>
                    <AvInput type="text" name="newbranchline1" placeholder="Enter Line 1" onChange={this.onChange} value={this.state.newbranchline1} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>Line 2</Label>
                    <AvInput type="text" name="newbranchline2" placeholder="Enter Line 2" onChange={this.onChange} value={this.state.newbranchline2} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>City</Label>
                    <AvInput type="text" name="newbranchcity" placeholder="Enter City" onChange={this.onChange} value={this.state.newbranchcity} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>State</Label>
                    <AvInput type="text" name="newbranchstate" placeholder="Enter State" onChange={this.onChange} value={this.state.newbranchstate} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>Country</Label>
                    <AvInput type="text" name="newbranchcountry" placeholder="Enter Country" onChange={this.onChange} value={this.state.newbranchcountry} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                  <AvGroup>
                    <Label>Post Code</Label>
                    <AvInput type="text" name="newbranchzipcode" placeholder="Enter Post Code" onChange={this.onChange} value={this.state.newbranchzipcode} autoComplete="name" />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
                <Col xs="12" md="6">
                  <Label className={this.state.newbranchlocation_error ? "error-color" : ""}>Area</Label>
                  <Select name="newbranchlocation" value={this.state.newbranchlocation} options={this.state.filteredemplist} onChange={this.onChangenmewBranchesSelect} />
                  {this.state.newbranchlocation_error ? <div className="error-color"> This is required!</div> : null}
                </Col>
                <Col xs="12" md="6" className={"for-phone-right"}>
                  <Label>Phone</Label>
                  <IntlTelInput style={{ width: "100%" }} defaultCountry={"gb"} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handlernewbranch} value={this.state.newbranchnumber} />
                  {this.state.newbranchphoneerror ? <div style={{ color: "red" }}>Enter Valid Phone Number!</div> : null}
                </Col>
                {/* <Col xs="12" md="2" className={"mt-4"}>
                <Button type="button" onClick={this.addnewBranches} color="success" className="pull-left" title="Add New Branch">
                  Add
                </Button>
              </Col>*/}
              </Row>
            </AvForm>
          </ModalBody>
          <ModalFooter>
            <div className="d-flex justify-content-between">
              <Button color="secondary" type="button" className="pull-left" onClick={this.toggleAddbranch}>
                Close
              </Button>
              <Button type="button" onClick={this.addnewBranches} color="success" className="pull-right" title="Add New Branch">
                Add
              </Button>
            </div>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.editbranchmodal} toggle={this.toggleEditbranch} className={this.props.className}>
          <ModalHeader>Edit Branch</ModalHeader>
          <ModalBody>
            <AvForm>
              {this.state.Editbranchlist &&
                this.state.Editbranchlist.length > 0 &&
                this.state.Editbranchlist.map((edulist, key) => (
                  <>
                    <Row key={key}>
                      <br />
                      <Col xs="12" md="12">
                        <AvGroup>
                          <Label>Branch Name</Label>
                          <AvInput type="text" name="branchname" placeholder="Enter Branch Name" onChange={this.onChangeBranches(key)} value={edulist.branchname} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="12">
                        <Label>Search address</Label>
                        <PlacesAutocomplete value={edulist.branchformatted_address} onChange={address => this.branchhandleChange(address, key)} onSelect={address => this.branchhandleSelect(address, key)} onFocus={this.geolocate}>
                          {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                            <div>
                              <input
                                {...getInputProps({
                                  placeholder: "Search Places",
                                  className: "form-control"
                                })}
                              />
                              <div className="autocomplete-dropdown-container absolute1">
                                {suggestions.map(suggestion => {
                                  const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                  // inline style for demonstration purpose
                                  const style = suggestion.active
                                    ? {
                                        backgroundColor: "#fafafa",
                                        cursor: "pointer"
                                      }
                                    : {
                                        backgroundColor: "#ffffff",
                                        cursor: "pointer"
                                      };
                                  return (
                                    <div
                                      {...getSuggestionItemProps(suggestion, {
                                        className,
                                        style
                                      })}
                                    >
                                      <span>{suggestion.description}</span>
                                    </div>
                                  );
                                })}
                              </div>
                            </div>
                          )}
                        </PlacesAutocomplete>
                      </Col>
                      <Col xs="12" md="6">
                        <AvGroup>
                          <Label>Line 1</Label>
                          <AvInput type="text" name="branchline1" placeholder="Enter Line 1" onChange={this.onChangeBranches(key)} value={edulist.branchline1} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="6">
                        <AvGroup>
                          <Label>Line 2</Label>
                          <AvInput type="text" name="branchline2" placeholder="Enter Line 2" onChange={this.onChangeBranches(key)} value={edulist.branchline2} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="6">
                        <AvGroup>
                          <Label>City</Label>
                          <AvInput type="text" name="branchcity" placeholder="Enter City" onChange={this.onChangeBranches(key)} value={edulist.branchcity} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="6">
                        <AvGroup>
                          <Label>State</Label>
                          <AvInput type="text" name="branchstate" placeholder="Enter State" onChange={this.onChangeBranches(key)} value={edulist.branchstate} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="6">
                        <AvGroup>
                          <Label>Country</Label>
                          <AvInput type="text" name="branchcountry" placeholder="Enter Country" onChange={this.onChangeBranches(key)} value={edulist.branchcountry} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="6">
                        <AvGroup>
                          <Label>Post Code</Label>
                          <AvInput type="text" name="branchzipcode" placeholder="Enter Post Code" onChange={this.onChangeBranches(key)} value={edulist.branchzipcode} autoComplete="name" />
                          <AvFeedback>This is required!</AvFeedback>
                        </AvGroup>
                      </Col>
                      <Col xs="12" md="6">
                        <Label className={this.state.branchlocation_error ? "error-color" : ""}>Area</Label>
                        <Select name="branchlocation" value={edulist.branchlocation} options={this.state.filteredemplist} onChange={value => this.onChangeBranchesSelect(key, value)} />
                        {this.state.branchlocation_error ? <div className="error-color"> This is required!</div> : null}
                      </Col>
                      <Col xs="12" md="6" className={"for-phone-right"}>
                        <Label>Phone</Label>
                        {edulist.dailcountry ? (
                          <IntlTelInput
                            style={{ width: "100%" }}
                            defaultCountry={edulist.dailcountry}
                            utilsScript={libphonenumber}
                            css={["intl-tel-input", "form-control"]}
                            onPhoneNumberChange={(status, value, countryData, number, id) => this.handlerbranch(status, value, countryData, number, id, key)}
                            value={edulist.phoneno}
                          />
                        ) : null}
                        {this.state.phoneerrorbranch ? <div style={{ color: "red" }}>Enter Valid Phone Number!</div> : null}
                      </Col>
                      <Col xs="12" md="2" className={"mt-4 text-right"}>
                        {/* {key === 0 ? (
                              <Button color="success" onClick={this.addBranches}>
                                <i className="fa fa-plus" />
                              </Button>
                            ) : null} */}
                      </Col>
                    </Row>
                    <hr />
                    <Row className="d-flex justify-content-between mt-3">
                      <Button color="secondary" type="button" onClick={this.toggleEditbranch}>
                        Close
                      </Button>
                      <Button
                        type="button"
                        onClick={() => {
                          this.updatenewBranches(edulist);
                        }}
                        color="success"
                        title="Update"
                      >
                        Update
                      </Button>
                    </Row>
                  </>
                ))}
            </AvForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
export default Editclient;
