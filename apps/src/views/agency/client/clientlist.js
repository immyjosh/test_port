import React, { Component, Fragment } from "react";
import { Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip, Row, Col } from "reactstrap";
import ReactDOM from "react-dom";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { ToastContainer, toast } from "react-toastify";
import Loader from "../../common/loader";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
import moment from "moment";
import MapList from "../../common/maplist";
import Clientmap from "./clientmapview";
import request, { client } from "../../../api/api";

const tStyle = {
  cursor: "pointer"
};

class Clientlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adminlist: [],
      url: "",
      isLoader: false,
      adminredirect: false,
      deletedisable: true,
      sortOrder: true,
      activePage: 1,
      pageRangeDisplayed: 4,
      bulk: [],
      count: 0,
      pages: "",
      currPage: 25,
      tableOptions: {
        search: "",
        filter: "all",
        page: {
          history: "",
          current: 1
        },
        order: "-1",
        field: "createdAt",
        limit: 10,
        skip: 0,
        to_date: "",
        from_date: "",
        notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
      },
      start_date: null,
      end_date: null
    };
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    this.populateData();
  }

  editpage = (e, id) => {
    e.stopPropagation();
    return this.props.history.push({
      pathname: "/agency/editclient",
      state: { rowid: id }
    });
  };

  viewpage = id => {
    return this.props.history.push({
      pathname: "/agency/viewclient",
      state: { rowid: id }
    });
  };

  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };

  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };

  toastId = "addclient";

  delete() {
    return toast.success("Deleted!");
  }

  populateData() {
    request({
      url: "/administrators/client/list",
      method: "POST",
      data: this.state.tableOptions
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            isLoader: false,
            adminlist: res.response.result,
            pages: res.response.fullcount,
            currPage: res.response.length
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }

  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["companyname", "name", "phone", "locations", "status", "hours", "lastshift"];
    for (let i in id) {
      if (document.getElementById(id[i])) {
        document.getElementById(id[i]).className = sorticondef;
      }
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }

  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "companyname";
      } else if (value === "Email") {
        state.tableOptions.filter = "email";
      } else if (value === "Area") {
        state.tableOptions.filter = "locations";
      } else if (value === "Contact") {
        state.tableOptions.filter = "name";
      } else if (value === "Phone") {
        state.tableOptions.filter = "phone.number";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Establishment Type") {
        state.tableOptions.filter = "establishmenttype";
      }
    });
    this.populateData();
  }

  export() {
    request({
      url: "/administrators/client/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }

  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }

  nodify() {
    return toast.warn("Updated");
  }

  addclientnodify() {
    if (!toast.isActive(this.toastId)) {
      this.toastId = toast.success("New Client Added!");
    }
  }

  actAsClient = (e, id) => {
    e.stopPropagation();
    let tabOpen1 = window.open("about:blank", "newtab1");
    request({
      url: "/agency/client/actasclient",
      method: "POST",
      data: { id: id }
    })
      .then(res => {
        if (res.status === 1) {
          localStorage.setItem("APUSC", res.response.auth_token);
          client.defaults.headers.common["Authorization"] = res.response.auth_token;
          return (
            (tabOpen1.location = "/portal/client/dashboard"),
            setTimeout(() => {
              window.location.reload();
            }, 800)
          );
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error("No User Found!");
      });
  };

  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    const hours = hour => {
      if (hour && hour[0]) {
        let total = hour[0].toFixed(2);
        let split = total.split(".");
        let totalhours = `${split[0]}:` + split[1] || `00`;
        return totalhours;
      } else {
        return "0:00";
      }
    };
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          {this.props.location.pathname === "/agency/dashboard" ? (
            <CardHeader>
              <i className="icon-list" />
              Recent Client List
            </CardHeader>
          ) : (
            <CardHeader>
              <i className="icon-list" />
              Client List
              <div className="card-actions" style={tStyle}>
                <button onClick={() => this.props.history.push("/agency/addclient")}>
                  <i className="fa fa-plus" /> Add New
                  {/*<small className="text-muted" />*/}
                </button>
                <button onClick={this.export}>
                  <i className="fa fa-upload" /> Export
                  {/*<small className="text-muted" />*/}
                </button>
              </div>
            </CardHeader>
          )}
          <CardBody>
            <Row>
              <Col xs="12" md="12">
                <div className="row justify-content-end">
                  {/* <div className="col-lg-7">
                    <DateRangePicker
                      showClearDates={true}
                      startDate={this.state.start_date}
                      startDateId="start_date"
                      endDate={this.state.end_date}
                      endDateId="end_date"
                      onDatesChange={({ startDate, endDate }) => {
                        this.setState({
                          start_date: startDate,
                          end_date: endDate
                        });
                      }}
                      isOutsideRange={day => day.isBefore(this.state.start_date)}
                      focusedInput={this.state.focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      displayFormat="DD-MM-YYYY"
                    />
                    <Button
                      color="primary rounded-0"
                      onClick={() => {
                        this.fromTo();
                      }}
                    >
                      <i className="fa fa-search" />
                    </Button>
                  </div> */}
                  <div className="col-lg-5">
                    <InputGroup>
                      <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-2">
                        <option>All</option>
                        {/* <option>Username</option> */}
                        <option>Contact</option>
                        <option>Name</option>
                        <option>Phone</option>
                        <option>Area</option>
                        {/* <option>Establishment Type</option> */}
                      </Input>
                      <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0 col-lg-10" />
                      <Button
                        className="rounded-0"
                        color="primary"
                        id="clear"
                        onClick={() => {
                          ReactDOM.findDOMNode(this.refs.search).value = "";
                          this.search("");
                        }}
                      >
                        <i className="fa fa-remove" />
                      </Button>
                      <UncontrolledTooltip placement="top" target="clear">
                        Clear
                      </UncontrolledTooltip>
                    </InputGroup>
                  </div>
                </div>
                <div className="table-responsive">
                  <Table hover bordered responsive size="sm">
                    <thead>
                      <tr>
                        <th>S.No.</th>
                        <th
                          onClick={() => {
                            this.sort("companyname");
                          }}
                        >
                          Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="companyname" />
                        </th>
                        {/* <th
                          onClick={() => {
                            this.sort("email");
                          }}
                        >
                          Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                        </th> */}
                        <th
                          onClick={() => {
                            this.sort("name");
                          }}
                        >
                          Contact <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("phone");
                          }}
                        >
                          Tel <i style={{ paddingLeft: "25px" }} className={sorticondef} id="phone" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("locations");
                          }}
                        >
                          Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                        </th>
                        {/* <th
                          onClick={() => {
                            this.sort("createdAt");
                          }}
                        >
                          Created Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                        </th> */}

                        <th
                          onClick={() => {
                            this.sort("status");
                          }}
                        >
                          Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("hours");
                          }}
                        >
                          Hours <i style={{ paddingLeft: "25px" }} className={sorticondef} id="hours" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("lastshift");
                          }}
                        >
                          Last Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="lastshift" />
                        </th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.adminlist.length > 0 ? (
                        this.state.adminlist.map((item, i) => (
                          <tr key={item._id} onClick={id => this.viewpage(item._id)}>
                            <td>{this.state.tableOptions.skip + i + 1}</td>
                            <td>{item.companyname}</td>
                            <td className="client-role">{item.name}</td>
                            {/* <td className="client-email">{item.email}</td> */}
                            <td className="client-email">
                              {item.phone.code} {item.phone.number}
                            </td>
                            <td>
                              {item.locations &&
                                item.locations.map((list, key) => (
                                  <span key={key}>
                                    {list} {key < item.locations.length - 1 ? <Fragment>,</Fragment> : <Fragment>.</Fragment>}
                                  </span>
                                ))}
                            </td>
                            {/* <td>{moment(item.createdAt).format("DD-MM-YYYY")}</td> */}
                            <td>
                              {item.status === 1 && item.isverified === 1 ? <Badge color="success">Active</Badge> : null}
                              {item.status === 0 && item.isverified === 1 ? <Badge color="danger">In-active</Badge> : null}
                              {item.status === 1 && item.isverified === 0 ? <Badge color="danger">Not Verified</Badge> : null}
                            </td>
                            <td className="client-email">{hours(item.hours)}</td>
                            <td className="">{item.lastshift && item.lastshift.length > 0 ? moment(item.lastshift[0]).format("DD-MM-YYYY") : "-"}</td>
                            <td>
                              <button type="button" title="Edit" className="btn table-edit" color="info" id={`edit${i}`} onClick={(e, id) => this.editpage(e, item._id)}>
                                <i className="fa fa-edit" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                Edit
                              </UncontrolledTooltip>
                              {item.status === 1 && item.isverified === 1 ? (
                                <Fragment>
                                  <button type="button" title="Edit" className="btn table-edit ml-1 mr-1" id={`act${i}`} onClick={(e, id) => this.actAsClient(e, item._id)}>
                                    <i className="fa fa-sign-in" />
                                  </button>
                                  <UncontrolledTooltip placement="top" target={`act${i}`}>
                                    Act As Client
                                  </UncontrolledTooltip>
                                </Fragment>
                              ) : null}
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr className="text-center">
                          <td colSpan={13}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                        </tr>
                      )}
                      {this.state.isLoader && <Loader />}
                    </tbody>
                  </Table>
                </div>
                <nav className="float-left">
                  {/*<Label>Show no.of items : </Label>*/}
                  <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                  </Input>
                </nav>
                <nav className="float-right">
                  <div>
                    <Pagination
                      prevPageText="Prev"
                      nextPageText="Next"
                      firstPageText="First"
                      lastPageText="Last"
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.tableOptions.limit}
                      totalItemsCount={this.state.pages}
                      pageRangeDisplayed={this.state.pageRangeDisplayed}
                      onChange={this.paginate}
                    />
                  </div>
                </nav>
              </Col>
              <Col xs="12" md="12" className="map_min">
				 <CardHeader className="located-map"><span className="left-loca">Client Locations </span> {/*<span className="right-loca"> <ul> <li className="agency-pro"><span></span> Agency</li> <li className="client-pro"><span></span> Client</li> <li className="emply-pro"><span></span> Employee</li><li className="building-pro"><span></span> Building</li>  </ul> </span>*/}</CardHeader>
                {this.state && this.state.adminlist && this.state.adminlist.length && this.state.adminlist.length > 0 ? (
                  <Clientmap markers={this.state.adminlist} />
                ) : (
                  <MapList mapListDetails={{ locationDetails: this.state.adminlist, Page: "joblist" }} />
                )}
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Clientlist;
