import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast } from "react-toastify";
import { Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip, Row, Col } from "reactstrap";
import fileDownload from "js-file-download";
import { DateRangePicker } from "react-dates";
import Loader from "../../common/loader";
import Pagination from "react-js-pagination";
import moment from "moment/moment";
import request from "../../../api/api";

const tStyle = {
  cursor: "pointer"
};

class Candidatelist extends Component {
  state = {
    adminlist: [],
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    pages: "",
    isLoader: false,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      form_status: 1,
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    this.populateData();
  }
  editpage = (e, id) => {
    e.stopPropagation();
    let URL = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      const val = this.props && this.props.location && this.props.location.pathname;
      switch (val) {
        case "/agency/recruitment/candidates":
          URL = "/agency/recruitmentform/candidates";
          break;
        case "/agency/recruitment/application":
          URL = "/agency/recruitmentform/application";
          break;
        case "/agency/recruitment/interview":
          URL = "/agency/recruitmentform/interview";
          break;
        case "/agency/recruitment/files":
          URL = "/agency/recruitmentform/files";
          break;
        case "/agency/recruitment/references":
          URL = "/agency/recruitmentform/references";
          break;
        case "/agency/recruitment/summary":
          URL = "/agency/recruitmentform/summary";
          break;
        default:
      }
      return this.props.history.push({
        pathname: URL,
        state: { rowid: id }
      });
    }
  };
  viewpage = id => {
    let URL = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      const val = this.props && this.props.location && this.props.location.pathname;
      switch (val) {
        case "/agency/recruitment/candidates":
          URL = "/agency/recruitment/form/candidates/view";
          break;
        case "/agency/recruitment/application":
          URL = "/agency/recruitment/form/application/view";
          break;
        case "/agency/recruitment/interview":
          URL = "/agency/recruitment/form/interview/view";
          break;
        case "/agency/recruitment/files":
          URL = "/agency/recruitment/form/files/view";
          break;
        case "/agency/recruitment/references":
          URL = "/agency/recruitment/form/references/view";
          break;
        case "/agency/recruitment/summary":
          URL = "/agency/recruitment/form/summary/view";
          break;
        default:
      }
      return this.props.history.push({
        pathname: URL,
        state: { rowid: id }
      });
    }
  };
  populateData() {
    request({
      url: "/agency/employees/apply-online-form/list",
      method: "POST",
      data: this.state.tableOptions
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            isLoader: false,
            adminlist: res.response.result,
            pages: res.response.fullcount,
            currPage: res.response.length
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => console.log(err));
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["name", "job_type", "email", "onlineform.submitted_date", "locations", "createdAt", "status"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Email") {
        state.tableOptions.filter = "email";
      } else if (value === "Area") {
        state.tableOptions.filter = "locations";
      } else if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Phone") {
        state.tableOptions.filter = "phone.number";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Job Role") {
        state.tableOptions.filter = "job_type";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/employees/recruitment/export",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    let Heading = "";
    let path_value = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      path_value = this.props && this.props.location && this.props.location.pathname;
      switch (path_value) {
        case "/agency/recruitment/candidates":
          Heading = "Candidates";
          break;
        case "/agency/recruitment/application":
          Heading = "Application Forms";
          break;
        case "/agency/recruitment/interview":
          Heading = "Interviews";
          break;
        case "/agency/recruitment/files":
          Heading = "Files";
          break;
        case "/agency/recruitment/references":
          Heading = "References";
          break;
        case "/agency/recruitment/summary":
          Heading = "Summary";
          break;
        default:
      }
    }
    return (
      <div className="animated">
        <Card>
          <CardHeader>
            <i className="icon-list" />
            {Heading}
            <div className="card-actions" style={tStyle} onClick={this.export}>
              <button>
                <i className="fa fa-upload" /> Export
                {/* <small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <Row>
              <Col xs="12" md="12">
                <div className="row">
                  <div className="col-lg-7">
                    <DateRangePicker
                      showClearDates={true}
                      startDate={this.state.start_date}
                      startDateId="start_date"
                      endDate={this.state.end_date}
                      endDateId="end_date"
                      onDatesChange={({ startDate, endDate }) => {
                        this.setState({
                          start_date: startDate,
                          end_date: endDate
                        });
                      }}
                      isOutsideRange={day => day.isBefore(this.state.start_date)}
                      focusedInput={this.state.focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      displayFormat="DD-MM-YYYY"
                    />
                    <Button
                      size="md"
                      color="primary rounded-0"
                      onClick={() => {
                        this.fromTo();
                      }}
                    >
                      <i className="fa fa-search" />
                    </Button>
                  </div>
                  <div className="col-lg-5">
                    <InputGroup>
                      <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-3">
                        <option>All</option>
                        <option>Name</option>
                        <option>Area</option>
                        <option>Email</option>
                        <option>Job Role</option>
                      </Input>
                      <Input type="text" ref="search" placeholder="Search..." name="search" onChange={e => this.search(e.target.value)} className="rounded-0  col-lg-7" />
                      <Button
                        className="rounded-0 "
                        color="primary"
                        id="clear"
                        onClick={() => {
                          ReactDOM.findDOMNode(this.refs.search).value = "";
                          this.search("");
                        }}
                      >
                        <i className="fa fa-remove" />
                      </Button>
                      <UncontrolledTooltip placement="top" target="clear">
                        Clear
                      </UncontrolledTooltip>
                    </InputGroup>
                  </div>
                </div>
                <div className="table-responsive mt-2">
                  <Table hover bordered responsive size="sm">
                    <thead>
                      <tr>
                        <th>S.No.</th>
                        <th
                          onClick={() => {
                            this.sort("name");
                          }}
                        >
                          Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                        </th>
                        <th
                          onClick={() => {
                            this.sort("email");
                          }}
                        >
                          Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                        </th>
                        {path_value === "/agency/recruitment/files" ? null : (
                          <>
                            <th
                              onClick={() => {
                                this.sort("job_type");
                              }}
                            >
                              Job Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_type" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("locations");
                              }}
                            >
                              Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("onlineform.submitted_date");
                              }}
                            >
                              Submitted Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="onlineform.submitted_date" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("status");
                              }}
                            >
                              Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                            </th>
                          </>
                        )}
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.adminlist.length > 0 ? (
                        this.state.adminlist.map((item, i) => (
                          <tr key={item._id} onClick={(id, job) => this.viewpage(item._id, item.job_data)}>
                            <td>{this.state.tableOptions.skip + i + 1}</td>
                            <td>{item.name}</td>
                            <td className="client-email">{item.email}</td>
                            {/* <td>{item.phone.number}</td> */}

                            {path_value === "/agency/recruitment/files" ? null : (
                              <>
                                <td className="client-role">
                                  {" "}
                                  {item.job_type.map((list, key) => (
                                    <span key={key}>
                                      {list} <Fragment>,</Fragment>
                                    </span>
                                  ))}
                                </td>
                                <td>
                                  {" "}
                                  {item.locations.map((list, key) => (
                                    <span key={key}>
                                      {list} <Fragment>,</Fragment>
                                    </span>
                                  ))}
                                </td>
                                <td>{moment(item.onlineform && item.onlineform.submitted_date).format("DD-MM-YYYY")}</td>
                                <td>
                                  {item.onlineform && item.onlineform.form_status === 1 ? <Badge color="success">New</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 2 ? <Badge color="success">Application Form Approved</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 3 ? <Badge color="success">Interview Scheduled</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 4 ? <Badge color="success">Interview Completed</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 5 ? <Badge color="success">Verification Process Completed</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 6 ? <Badge color="success">Verification Completed</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 7 ? <Badge color="success">Reference Form Sent</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 8 ? <Badge color="success">Reference Verification Completed</Badge> : null}
                                  {item.onlineform && item.onlineform.form_status === 9 ? <>{item.onlineform.final_verify_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</> : null}
                                </td>
                              </>
                            )}
                            <td>
                              <button type="button" title="Edit" className="btn table-edit btn-sm" color="info" id={`edit${i}`} onClick={id => this.viewpage(item._id)}>
                                <i className="fa fa-eye" />
                              </button>
                              <button type="button" title="Edit" className="btn table-edit btn-sm" color="info" id={`edit${i}`} onClick={(e, id) => this.editpage(e, item._id)}>
                                <i className="fa fa-edit" />
                              </button>
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr className="text-center">
                          <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                        </tr>
                      )}
                      {this.state.isLoader && <Loader />}
                    </tbody>
                  </Table>
                </div>
                <nav className="float-left">
                  <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                  </Input>
                </nav>
                <nav className="float-right">
                  <div>
                    <Pagination
                      prevPageText="Prev"
                      nextPageText="Next"
                      firstPageText="First"
                      lastPageText="Last"
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.tableOptions.limit}
                      totalItemsCount={this.state.pages}
                      pageRangeDisplayed={this.state.pageRangeDisplayed}
                      onChange={this.paginate}
                    />
                  </div>
                </nav>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Candidatelist;
