import React, { Component } from "react";
import {
  Badge,
  Card,
  CardBody,
  CardHeader,
  Col,
  Label,
  Modal,
  Row,
  Table,
  // TabPane,
  UncontrolledTooltip
} from "reactstrap";
import request, { NodeURL } from "../../../api/api";
import moment from "moment";
import { toast } from "react-toastify";
import FileSaver from "file-saver";
import FileViewer from "react-file-viewer";
const Canditates_URL = "/agency/recruitment/form/candidates/view";
const Application_URL = "/agency/recruitment/form/application/view";
const Interview_URL = "/agency/recruitment/form/interview/view";
const Files_URL = "/agency/recruitment/form/files/view";
const Referee_URL = "/agency/recruitment/form/references/view";
const Summary_URL = "/agency/recruitment/form/summary/view";

const tStyle = {
  cursor: "pointer"
};

class Candidatelist extends Component {
  state = {};
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    const path_name = this.props && this.props.location && this.props.location.pathname ? this.props.location.pathname : "";
    this.setState({
      Emp_ID: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid,
      path_name
    });
    request({
      url: "/administrators/employee/get",
      method: "POST",
      data: { id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID }
    }).then(res => {
      if (res.status === 1) {
        const res_result = res.response.result && res.response.result.length > 0 ? res.response.result[0] : "";
        const form_data = res_result.onlineform ? res_result.onlineform : {};
        const form_status = res_result.onlineform && res_result.onlineform.form_status ? res_result.onlineform.form_status : 0;
        if (form_data) {
          this.setState({
            form_status: form_status,
            form_data: form_data,
            employee_data: res_result,
            employee_email: res_result.email
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
      if (path_name === Canditates_URL || path_name === Application_URL) {
        this.setState({ activeTab: "1" });
      } else if (path_name === Interview_URL) {
        this.setState({ activeTab: "6" });
      } else if (path_name === Files_URL) {
        this.setState({ activeTab: "4" });
      } else if (path_name === Referee_URL) {
        this.setState({ activeTab: "7" });
      } else if (path_name === Summary_URL) {
        this.setState({ activeTab: "8" });
      }
    });
    request({
      url: "/agency/jobtype/list",
      method: "POST",
      data: { status: 1, for: "editemployee" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  editpage = (e, id) => {
    e.stopPropagation();
    let URL = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      const val = this.props && this.props.location && this.props.location.pathname;
      switch (val) {
        case "/agency/recruitment/form/candidates/view":
          URL = "/agency/recruitmentform/candidates";
          break;
        case "/agency/recruitment/form/application/view":
          URL = "/agency/recruitmentform/application";
          break;
        case "/agency/recruitment/form/interview/view":
          URL = "/agency/recruitmentform/interview";
          break;
        case "/agency/recruitment/form/files/view":
          URL = "/agency/recruitmentform/files";
          break;
        case "/agency/recruitment/form/references/view":
          URL = "/agency/recruitmentform/references";
          break;
        case "/agency/recruitment/form/summary/view":
          URL = "/agency/recruitmentform/summary";
          break;
        default:
          break;
      }
      return this.props.history.push({
        pathname: URL,
        state: { rowid: this.props.location.state.rowid }
      });
    }
  };
  FileDownloader = (filepath, name) => {
    let toastId = "downloadfiles0101";
    toastId = toast.info("Please Wait......", { autoClose: false });
    if (filepath) {
      const file = NodeURL + "/" + filepath;
      FileSaver.saveAs(file, name);
      toast.update(toastId, { render: "File Downloaded!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
    } else {
      toast.update(toastId, { render: "Unable to Download", type: toast.TYPE.ERROR, autoClose: 2500 });
    }
  };
  FileView = path => {
    if (path) {
      const filepath = `${NodeURL}/${path}`;
      let fileext = path && path.split(".");
      fileext = fileext[fileext.length - 1];
      this.setState({ filepath, fileext, fileviewmodal: true });
    } else {
      this.setState({ fileviewmodal: false });
    }
  };
  render() {
    const { jobtypelist, path_name, form_data, filepath, fileext } = this.state;
    let Heading = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      const val = this.props && this.props.location && this.props.location.pathname;
      switch (val) {
        case "/agency/recruitment/form/candidates/view":
          Heading = "Candidates - View";
          break;
        case "/agency/recruitment/form/application/view":
          Heading = "Applicant - View";
          break;
        case "/agency/recruitment/form/interview/view":
          Heading = "Interviews - View";
          break;
        case "/agency/recruitment/form/files/view":
          Heading = "File Manager - View";
          break;
        case "/agency/recruitment/form/references/view":
          Heading = "References - View";
          break;
        case "/agency/recruitment/form/summary/view":
          Heading = "Summary - View";
          break;
        default:
          break;
      }
    }
    const {
      // form_status,
      skillcheck: form_skillcheck,
      healthcheck: form_healthcheck,
      dbscheck: form_dbscheck,
      interview: form_interview,
      personaldetails: form_personaldetails,
      drivingdetails: form_driving,
      nextofkindetails: form_kin,
      gpdetails: form_gp,
      postapplydetails,
      availablestartdate,
      UploadIDPhoto,
      employmenteligibility: { visapermit, visapermittype } = {},
      // UploadCV,
      langauages: { englishspoken, englishwritten, otherlanguage, IELTStest } = {},
      employmenthistory: employmenthistorylist,
      education: educationlist,
      reference1,
      reference1: { referee: form_reference1 } = {},
      reference2,
      reference2: { referee: form_reference2 } = {},
      mandatory_training,
      movinghandling,
      basiclifesupport,
      healthsafety,
      firesafety,
      firstaid,
      infectioncontrol,
      foodsafety,
      medicationadmin,
      safeguardingvulnerable,
      trainingdates,
      healthcare_training,
      diplomal3,
      diplomal2,
      personalsafetymental,
      intermediatelife,
      advancedlife,
      complaintshandling,
      handlingviolence,
      doolsmental,
      coshh,
      dataprotection,
      equalityinclusion,
      loneworkertraining,
      resuscitation,
      interpretation,
      trainindates2,
      healthdeclaration,
      medicalhistory: { medicalillness, medicalillnesscaused, treatment, needanyadjustments, needanyadjustmentsnotes } = {},
      tuberculosis: { livedUK, livedUKnotes, BCGvaccination, BCGvaccinationdate, cough, weightloss, fever, TB } = {},
      chickenpoxorshingles: { chickenpox, chickenpoxdate } = {},
      immunisationhistory: { triplevaccination, polio, tetanus, hepatitisB, proneProcedures } = {},
      declaration: {
        // occupationalHealthServices,
        criminaloffence,
        warningcriminaloffence
        // DBSdetails
      } = {},
      righttowork: { passport } = {},
      worktimedirectives: { workhours } = {},
      uploadcertificate: certificatedocs,
      uploadpassport: passportdocs,
      uploadaddress: addressdocs,
      uploaddbs: dbsdocs,
      uploadothers: otherdocs,
      final_verify_status,
      final_verify_date,
      final_verify_notes
    } = form_data ? form_data : {};
    const { abletowork: form_aboutwork, firstchoice, secondchoice, thirdchoice, shortnotice, reduceworkflexibility, workstate } = form_data && form_data.aboutyourwork ? form_data.aboutyourwork : {};
    const { date: skillcheckdate, notes: skillchecknotes, status: skillcheckstatus } = form_skillcheck ? form_skillcheck : {};
    const { date: dbscheckdate, notes: dbschecknotes, status: dbscheckstatus } = form_dbscheck ? form_dbscheck : {};
    const { date: healthcheckdate, notes: healthchecknotes, status: healthcheckstatus } = form_healthcheck ? form_healthcheck : {};
    const { interviewdate, interviewnotes, intervieweddate, interviewednotes, status: interviewedstatus, videocallurl } = form_interview ? form_interview : {};
    const {
      title,
      firstname,
      lastname,
      homephone,
      email,
      dateofbirth,
      insuranceno,
      nationality,
      gender,
      religion,
      race,
      sexualorientation,
      address: { streetAddress, addressline2, city, state, zip, country } = {},
      mobileno
    } = form_personaldetails ? form_personaldetails : {};
    const { vehicledocumentsvalid, drivinglicence, traveldetails, drivinglicenceno, caraccess, bannedfromdriving, carinsurance } = form_driving ? form_driving : {};
    const { work_mornings, work_evenings, work_afternoons, work_occasional, work_fulltime, work_parttime, work_nights, work_weekends, work_anytime } = form_aboutwork ? form_aboutwork : {};
    const { kinprefix, kinfirstname, kinlastname, kinrelationship, kinhomephone, kinmobileno, kinemail, Address: { kinstreetAddress, kinaddressline2, kincity, kinstate, kinzip, kincountry } = {} } = form_kin ? form_kin : {};
    const { firstrefrelationship, firstreffirstname, firstreflastname, firstrefphone, firstrefemail, Address: { firstrefstreetAddress, firstrefcity, firstrefstate, firstrefzip } = {} } = reference1 ? reference1 : {};
    const { secondrefrelationship, secondreffirstname, secondreflastname, secondrefphone, secondrefemail, Address: { secondrefstreetAddress, secondrefcity, secondrefstate, secondrefzip } = {} } = reference2 ? reference2 : {};
    const { sufferedlongtermillness, leaveforneckinjury, neckinjuries, sixweeksillness, communicabledisease, medicalattention, healthdeclarationdetails, registereddisabled, absentfromwork, statereason } = healthdeclaration
      ? healthdeclaration
      : {};
    const { GPname, gpaddress: { GPstreetAddress, GPcity, GPstate, GPzip, contactDoctor, GPhealthdetails } = {} } = form_gp ? form_gp : {};
    const {
      refereefirstname,
      refereelastname,
      refereeemail,
      refereephone,
      refereejobtitle,
      refereecompany,
      refereecapacity,
      applicantfirstname,
      applicantlastname,
      applicantemail,
      applicantphone,
      applicantjobtitle,
      applicantcompany,
      employedfrom,
      employedto,
      remployee,
      remployeenotes,
      followcareplans,
      reliability,
      character,
      dignity,
      attitude,
      communication,
      relationships,
      workunderinitiative,
      disciplinaryaction,
      disciplinaryactiondetails,
      investigations,
      investigationsdetails,
      vulnerablepeople,
      vulnerablepeopledetails,
      criminaloffences,
      criminaloffencedetails,
      additionalcomments,
      // campanystamp,
      referenceverifydate,
      // referenceverifynotes
    } = form_reference1 ? form_reference1 : {};
    const reference_details1 = form_reference1;
    const {
      refereefirstname: refereefirstname2,
      refereelastname: refereelastname2,
      refereeemail: refereeemail2,
      refereephone: refereephone2,
      refereejobtitle: refereejobtitle2,
      refereecompany: refereecompany2,
      refereecapacity: refereecapacity2,
      applicantfirstname: applicantfirstname2,
      applicantlastname: applicantlastname2,
      applicantemail: applicantemail2,
      applicantphone: applicantphone2,
      applicantjobtitle: applicantjobtitle2,
      applicantcompany: applicantcompany2,
      employedfrom: employedfrom2,
      employedto: employedto2,
      remployee: remployee2,
      remployeenotes: remployeenotes2,
      followcareplans: followcareplans2,
      reliability: reliability2,
      character: character2,
      dignity: dignity2,
      attitude: attitude2,
      communication: communication2,
      relationships: relationships2,
      workunderinitiative: workunderinitiative2,
      disciplinaryaction: disciplinaryaction2,
      disciplinaryactiondetails: disciplinaryactiondetails2,
      investigations: investigations2,
      investigationsdetails: investigationsdetails2,
      vulnerablepeople: vulnerablepeople2,
      vulnerablepeopledetails: vulnerablepeopledetails2,
      criminaloffences: criminaloffence2,
      criminaloffencedetails: criminaloffencedetails2,
      additionalcomments: additionalcomments2,
      // campanystamp: campanystamp2,
      referenceverifydate: referenceverifydate2,
      // referenceverifynotes: referenceverifynotes2
    } = form_reference2 ? form_reference2 : {};
    
    const reference_details2 = form_reference2;
    /* if (path_name === Canditates_URL || path_name === Referee_URL || path_name === Summary_URL) {
      this.setState({
        refemailsend1: form_data.reference1 && form_data.reference1.firstrefemail,
        refemailsend2: form_data.reference2 && form_data.reference2.secondrefemail,
        refereename1: `${form_data.reference1.firstreffirstname}${form_data.reference1.firstreflastname}`,
        refereename2: `${form_data.reference2.secondreffirstname}${form_data.reference2.secondreflastname}`
      });
    }*/
    const externalCloseBtn = (
      <button className="close fileview_close" onClick={() => this.FileView("")}>
        &times;
      </button>
    );
    return (
      <div className="animated">
        <Card>
          <CardHeader>
            <i className="icon-list" />
            {Heading}
            <div className="card-actions" style={tStyle} onClick={this.editpage}>
              <button>
                <i className="fa fa-edit" /> Edit
                {/* <small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            {path_name === Canditates_URL || path_name === Application_URL || path_name === Files_URL ? (
              <>
                {path_name !== Files_URL ? (
                  <>
                    <Row>
                      <div className="emp-full">
                        <div className="cus-design">
                          <div className="info-widths">
                            <div className="cate-ranges">
                              <img width="180px" height="180px" src={NodeURL + "/" + UploadIDPhoto} alt="Profile" />
                            </div>
                          </div>
                          <div className="info-widths">
                            <div className="cate-ranges">
                              <p>
                                <span className="left-views">
                                  Post Applying for <span className="col-qus">:</span>
                                </span>
                                {jobtypelist &&
                                        jobtypelist.length > 0 &&
                                        jobtypelist.map((list, i) => (
                                <span className="right-views" key={i} value={list._id}>
                               
                                          
                                            {list.name}
                                       
                                 
                                </span> ))}
                              </p>
                              <p>
                                <span className="left-views">
                                  How did you hear about this us? <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">
                                  {postapplydetails === "internetsearch"
                                    ? "Internet Search"
                                    : postapplydetails === "jobcentre"
                                      ? "Job Centre"
                                      : postapplydetails === "friend"
                                        ? "Friend"
                                        : postapplydetails === "newspaper"
                                          ? "Newspaper"
                                          : postapplydetails === "other"
                                            ? "Other"
                                            : ""}
                                </span>
                              </p>
                              <p>
                                <span className="left-views">
                                  Date available to start <span className="col-qus">:</span>
                                </span>
                                <span className="right-views">{moment(availablestartdate).format("DD-MM-YYYY")}</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Personal Details 1</h4>
                            <p>
                              <span className="left-views">
                                Title <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {title === "mr" ? "Mr." : title === "mrs" ? "Mrs." : title === "miss" ? "Miss." : title === "ms" ? "Ms." : title === "dr" ? "Dr." : title === "prof" ? "Prof." : title === "rev" ? "Rev." : ""}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                First Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Last Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{lastname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Home Phone <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{homephone}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Mobile No <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {mobileno && mobileno.code}-{mobileno && mobileno.number}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                Email <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{email}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Date Of Birth <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{moment(dateofbirth).format("DD-MM-YYYY")}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                National Insurance No <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{insuranceno}</span>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Address</h4>
                            <p>
                              <span className="left-views">
                                Street Address <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{streetAddress}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Address Line 2 <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{addressline2}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                City <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{city}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                County / State / Region <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{state}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                ZIP / Postal Code <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{zip}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Country <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{country}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Personal Details 2</h4>
                            <p>
                              <span className="left-views">
                                Nationality <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{nationality === "british" ? "British" : nationality === "eucitizen" ? "EU Citizen" : nationality === "other" ? "Other" : ""}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Religion <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {religion === "bhudist"
                                  ? "Bhudist"
                                  : religion === "christian"
                                    ? "Christian"
                                    : religion === "jewish"
                                      ? "Jewish"
                                      : religion === "hindu"
                                        ? "Hindu"
                                        : religion === "muslim"
                                          ? "Muslim"
                                          : religion === "sikh"
                                            ? "Sikh"
                                            : religion === "other"
                                              ? "Other"
                                              : ""}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                Sexual Orientation <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {sexualorientation === "bisexual"
                                  ? "Bisexual"
                                  : sexualorientation === "gayman"
                                    ? "Gay Man"
                                    : sexualorientation === "gaywoman"
                                      ? "Gay Woman/Lesbian"
                                      : sexualorientation === "straight"
                                        ? "Straight/Heterosexual"
                                        : sexualorientation === "prefer"
                                          ? "Prefer not to answer"
                                          : ""}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                Gender <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{gender === "female" ? "Female" : gender === "male" ? "Male" : ""}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Race/Ethnicity <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {race === "whitebritish"
                                  ? "White British"
                                  : race === "whiteother"
                                    ? "White(Other)"
                                    : race === "whiteirish"
                                      ? "White Irish"
                                      : race === "mixesrace"
                                        ? "Mixes Race"
                                        : race === "indian"
                                          ? "Indian"
                                          : race === "pakistani"
                                            ? "Pakistani"
                                            : race === "bangladeshi"
                                              ? "Bangladeshi"
                                              : race === "otherasian"
                                                ? "Other Asian(non-Chinese)"
                                                : race === "blackcaribbean"
                                                  ? "Black Caribbean"
                                                  : race === "blackafrican"
                                                    ? "Black African"
                                                    : race === "blackothers"
                                                      ? "Black(others)"
                                                      : race === "chinese"
                                                        ? "Chinese"
                                                        : race === "other"
                                                          ? "Other"
                                                          : ""}
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Languages</h4>
                            <p>
                              <span className="left-views">
                                English - Spoken <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{englishspoken === "fluent" ? "Fluent" : englishspoken === "good" ? "Good" : englishspoken === "fair" ? "Fair" : ""}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Other Languages Spoken <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{otherlanguage}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                English - Written <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{englishwritten === "fluent" ? "Fluent" : englishwritten === "good" ? "Good" : englishwritten === "fair" ? "Fair" : ""}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Have you passed each of the academic modules of the IELTS test? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{IELTStest === "yes" ? "Yes" : "No"}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Driving Details</h4>
                            <p>
                              <span className="left-views">
                                Do you have full Driving Licence that allows you to drive in the UK?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{drivinglicence === "yes" ? "YES" : "NO"}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Do you have access to a car that you can use for work?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{caraccess === "yes" ? "YES" : "NO"}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Have you been banned from driving. or do you have any current endorsements on you licence?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{bannedfromdriving === "yes" ? "YES" : "NO"}</span>
                            </p>{" "}
                            <p>
                              <span className="left-views">
                                Driving Licence No:
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{drivinglicenceno}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Are all your vehicle documents up to date and valid?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{vehicledocumentsvalid === "yes" ? "YES" : "NO"}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Does you car insurance include Class 1 business insurance? (in order to use you vehicle for work you must have class 1 business insurance)
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{carinsurance === "yes" ? "YES" : "NO"}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                How would you travel to work if assigned? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {traveldetails === "drive"
                                  ? "Drive"
                                  : traveldetails === "public"
                                    ? "Public Transport"
                                    : traveldetails === "willgetalist"
                                      ? "Will get a lift"
                                      : traveldetails === "bicycle"
                                        ? "Bicycle"
                                        : traveldetails === "other"
                                          ? "Other"
                                          : ""}
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-6">
                        <div className="cate-ranges" />
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Next of kin details</h4>
                            <p>
                              <span className="left-views">
                                Prefix <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {kinprefix === "mr"
                                  ? "Mr."
                                  : kinprefix === "mrs"
                                    ? "Mrs."
                                    : kinprefix === "miss"
                                      ? "Miss."
                                      : kinprefix === "ms"
                                        ? "Ms."
                                        : kinprefix === "dr"
                                          ? "Dr."
                                          : kinprefix === "prof"
                                            ? "Prof."
                                            : kinprefix === "rev"
                                              ? "Rev."
                                              : ""}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                First Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinfirstname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Last Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinlastname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Relationship to you <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinrelationship}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Home/Work Phone <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinhomephone}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Mobile No <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {kinmobileno && kinmobileno.code} - {kinmobileno && kinmobileno.number}
                              </span>
                            </p>

                            <p>
                              <span className="left-views">
                                Email <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinemail}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Street Address <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinstreetAddress}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Address Line 2 <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinaddressline2}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                City <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kincity}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                County / State / Region <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinstate}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                ZIP / Postal Code <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kinzip}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Country <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{kincountry}</span>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>About Your Work</h4>
                            <p>
                              <span className="left-views">
                                When are you able to work? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {work_mornings ? "Mornings ," : ""}
                                {work_evenings ? "Evenings ," : ""}
                                {work_afternoons ? "Afternoons ," : ""}
                                {work_occasional ? "Occasional Weeks ," : ""}
                                {work_fulltime ? "Full Time ," : ""}
                                {work_parttime ? "Part Time ," : ""}
                                {work_nights ? "Nights ," : ""}
                                {work_weekends ? "Weekends ," : ""}
                                {work_anytime ? "Anytime ," : ""}
                              </span>
                            </p>

                            <p>
                              <span className="left-views">
                                1st Choice <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstchoice}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                2nd Choice <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondchoice}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                3rd Choice <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{thirdchoice}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Would you be willing to work at short notice? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{shortnotice === "yes" ? "Yes" : shortnotice === "no" ? "No" : ""}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Do you have any commitments that reduce your flexibility to work <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{reduceworkflexibility === "yes" ? "Yes" : reduceworkflexibility === "no" ? "No" : ""}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                If yes, please state
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{workstate}</span>
                            </p>
                            <h4 className="mt-2">Employment Eligibility</h4>
                            <p>
                              <span className="left-views">
                                What visa/permit/status do you currently hold? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {visapermit === "workingholiday" ? "Working Holiday" : visapermit === "workpermit" ? "Work Permit" : visapermit === "leavetoremain" ? "Leave to Remain" : visapermit === "other" ? "Other" : ""}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                Visa Type <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{visapermittype}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-12">
                        <div className="cate-ranges">
                          <h2>Employment and Education</h2>
                        </div>
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Your History</h4>
                            <p>
                              Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps unaccounted
                              for and it covers full work history including your education. Please use extra paper if required. Full work history including your education Dates to and from are shown in a mm/yy format Dates are continual
                              with NO gaps Where there have been gaps in work history please state the reason for the gaps Lists all relevant training undertaken
                            </p>
                            <p>
                              Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps unaccounted
                              for and it covers full work history including your education. Please use extra paper if required.
                            </p>
                            <br />
                            <p> Full work history including your education </p>
                            <p> Dates to and from are shown in a mm/yy format </p>
                            <p> Dates are continual with NO gaps </p>
                            <p> Where there have been gaps in work history please state the reason for the gaps </p>
                            <p> Lists all relevant training undertaken </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <div className="row">
                      {employmenthistorylist &&
                        employmenthistorylist.map((list, i) => (
                          <div className="col-md-6" key={i}>
                            <div className="cus-design">
                              <div className="cate-ranges">
                                <h4>Employment History</h4>
                                <p>
                                  <span className="left-views">
                                    Employer <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.employer}</span>
                                </p>

                                <p>
                                  <span className="left-views">
                                    Duties <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.duties}</span>
                                </p>

                                <p>
                                  <span className="left-views">
                                    Salary on Leaving <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.salaryonleaving}</span>
                                </p>

                                <p>
                                  <span className="left-views">
                                    position <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.position}</span>
                                </p>

                                <p>
                                  <span className="left-views">
                                    Date <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{moment(list.start_date).format("DD-MM-YYYY") + " - " + moment(list.end_date).format("DD-MM-YYYY")}</span>
                                </p>
                              </div>
                            </div>
                          </div>
                        ))}
                      {educationlist &&
                        educationlist.map((list, i) => (
                          <div className="col-md-6" key={i}>
                            <div className="cus-design">
                              <div className="cate-ranges">
                                <h4>Education</h4>
                                <p>
                                  <span className="left-views">
                                    Institution <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.institution}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Course <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.course}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Year <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.year}</span>
                                </p>
                                <p>
                                  <span className="left-views">
                                    Grade <span className="col-qus">:</span>
                                  </span>
                                  <span className="right-views">{list.grade}</span>
                                </p>
                              </div>
                            </div>
                          </div>
                        ))}
                    </div>

                    <Row />
                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Reference</h4>
                            <p>What visa/permit/status do you currently hold?:</p>
                            <p>
                              Please supply us with two professional referees. One must be from your present or most recent employer and must be a senior grade to yourself and you must have worked for that person for a period of not less
                              than three months duration.
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Reference 1</h4>
                            <p>
                              <span className="left-views">
                                Relationship <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstrefrelationship}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                First Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstreffirstname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Last Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstreflastname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Mobile No <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {firstrefphone && firstrefphone.code} - {firstrefphone && firstrefphone.number}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                Email <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstrefemail}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Street Address <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstrefstreetAddress}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                City <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstrefcity}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                County / State / Region <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstrefstate}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                ZIP / Postal Code <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{firstrefzip}</span>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Reference 2</h4>
                            <p>
                              <span className="left-views">
                                Relationship <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondrefrelationship}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                First Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondreffirstname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Last Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondreflastname}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Mobile No <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {secondrefphone && secondrefphone.code} - {secondrefphone && secondrefphone.number}
                              </span>
                            </p>
                            <p>
                              <span className="left-views">
                                Email <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondrefemail}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Street Address <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondrefstreetAddress}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                City <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondrefcity}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                County / State / Region <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondrefstate}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                ZIP / Postal Code <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{secondrefzip}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-12">
                        <div className="cate-ranges">
                          <h2>Skill and Education</h2>
                        </div>
                      </div>
                      <div className="cus-design">
                        <div className="col-md-12">
                          <div className="cate-ranges">
                            <h4>Skill, Experience and Training</h4>
                            <p>
                              <span className="left-views">
                                Have you completed mandatory training within the last year <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{mandatory_training && mandatory_training.toUpperCase()}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="cus-design">
                        <div className="col-md-12">
                          <div className="cate-ranges">
                            <h4>Other Training/Courses & Qualifications</h4>
                            <p>
                              <span className="left-views">
                                Please check which training you have completed and the date on the notes (certificates must be provided). <span className="col-qus">:</span>
                              </span>
                              {movinghandling ? <span className="right-views">Moving & Handling</span> : null}
                              {basiclifesupport ? <span className="right-views">Basic life support</span> : null}
                              {healthsafety ? <span className="right-views">Health and Safety</span> : null}
                              {firesafety ? <span className="right-views">Fire Safety</span> : null}
                              {firstaid ? <span className="right-views">First Aid</span> : null}
                              {infectioncontrol ? <span className="right-views">Infection Control</span> : null}
                              {foodsafety ? <span className="right-views">Food Safety & Nutrition</span> : null}
                              {medicationadmin ? <span className="right-views">Medication Administration</span> : null}
                              {safeguardingvulnerable ? <span className="right-views">Safeguarding Vulnerable Adults & Children</span> : null}
                            </p>
                            <p>
                              <span className="left-views">
                                Training Dates <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{trainingdates}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="cus-design">
                        <div className="col-md-12">
                          <div className="cate-ranges">
                            <h4>Skill, Experience and Training</h4>
                            <p>
                              <span className="left-views">
                                Have you completed other health care training? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{healthcare_training && healthcare_training.toUpperCase()}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="cus-design">
                        <div className="col-md-12">
                          <div className="cate-ranges">
                            <h4>Other Training/Courses & Qualifications</h4>
                            <p>
                              <span className="left-views">
                                Please check which training you have completed and the date on the notes (certificates must be provided). <span className="col-qus">:</span>
                              </span>
                              {diplomal3 ? <span className="right-views">Diploma in Health & Social Care L3</span> : null}
                              {diplomal2 ? <span className="right-views">Diploma in Health & Social Care L2</span> : null}
                              {personalsafetymental ? <span className="right-views">Personal Safety (Mental Health & Learning Disability)</span> : null}
                              {intermediatelife ? <span className="right-views">Intermediate Life Support</span> : null}
                              {advancedlife ? <span className="right-views">Advanced Life Support</span> : null}
                              {complaintshandling ? <span className="right-views">Complaints Handling</span> : null}
                              {handlingviolence ? <span className="right-views">Handling Violence and Aggression</span> : null}
                              {doolsmental ? <span className="right-views">DOLLS & Mental Capacity</span> : null}
                              {coshh ? <span className="right-views">COSHH</span> : null}
                              {dataprotection ? <span className="right-views">Data Protection</span> : null}
                              {equalityinclusion ? <span className="right-views">Equality & Inclusion</span> : null}
                              {loneworkertraining ? <span className="right-views">Lone Worker Training</span> : null}
                              {resuscitation ? <span className="right-views">Resuscitation of the Newborn (Midwifery)</span> : null}
                              {interpretation ? <span className="right-views">Interpretation of Cardiotocograph Traces (Midwifery)</span> : null}
                            </p>

                            <p>
                              <span className="left-views">
                                Training Dates <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{trainindates2}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                  </>
                ) : null}
                <Row>
                  <div className="col-md-12">
                    <div className="cate-ranges">
                      <h2>Documents</h2>
                    </div>
                  </div>
                </Row>
                <Row className="onl-mar">
                  <Col xs="12" md="6" className="upl-files">
                    <div className="upload-forms">
                      <Label for="exampleCustomFileBrowser3">Certificate</Label>
                      <Col className="doc-upload">
                        {certificatedocs &&
                          certificatedocs.length > 0 &&
                          certificatedocs.map((list, i) => (
                            <p key={i}>
                              <img src={`${NodeURL}/uploads/profiles.png`} alt="files" className="fileviewimg" id={`fileview1${i}`} onClick={() => this.FileView(list.path)} />
                              <UncontrolledTooltip placement="bottom" target={`fileview1${i}`}>
                                View Document
                              </UncontrolledTooltip>
                              <span className="documents-name"> {list && list.name} </span>
                              <span className="documents-del">
                                <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                  {" "}
                                  <i className="fa fa-download" aria-hidden="true" />{" "}
                                </button>{" "}
                              </span>
                            </p>
                          ))}
                      </Col>
                    </div>
                  </Col>

                  <Col xs="12" md="6" className="upl-files">
                    <div className="upload-forms">
                      <Label for="exampleCustomFileBrowser4">Passport</Label>

                      <Col className="doc-upload">
                        {passportdocs &&
                          passportdocs.length > 0 &&
                          passportdocs.map((list, i) => (
                            <p key={i}>
                              <img src={`${NodeURL}/uploads/profiles.png`} alt="files" className="fileviewimg" id={`fileview2${i}`} onClick={() => this.FileView(list.path)} />
                              <UncontrolledTooltip placement="bottom" target={`fileview2${i}`}>
                                View Document
                              </UncontrolledTooltip>
                              <span className="documents-name">{list && list.name} </span>
                              <span className="documents-del">
                                <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                  {" "}
                                  <i className="fa fa-download" aria-hidden="true" />{" "}
                                </button>{" "}
                              </span>
                            </p>
                          ))}
                      </Col>
                    </div>
                  </Col>

                  <Col xs="12" md="6" className="upl-files">
                    <div className="upload-forms">
                      <Label for="exampleCustomFileBrowser5">Proof Of Address</Label>
                      <Col className="doc-upload">
                        {addressdocs &&
                          addressdocs.length > 0 &&
                          addressdocs.map((list, i) => (
                            <p key={i}>
                              <img src={`${NodeURL}/uploads/profiles.png`} alt="files" className="fileviewimg" id={`fileview3${i}`} onClick={() => this.FileView(list.path)} />
                              <UncontrolledTooltip placement="bottom" target={`fileview3${i}`}>
                                View Document
                              </UncontrolledTooltip>
                              <span className="documents-name"> {list && list.name} </span>
                              <span className="documents-del">
                                <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                  {" "}
                                  <i className="fa fa-download" aria-hidden="true" />{" "}
                                </button>{" "}
                              </span>
                            </p>
                          ))}
                      </Col>
                    </div>
                  </Col>

                  <Col xs="12" md="6" className="upl-files">
                    <div className="upload-forms">
                      <Label for="exampleCustomFileBrowser6">DBS Certificate</Label>
                      <Col className="doc-upload">
                        {dbsdocs &&
                          dbsdocs.length > 0 &&
                          dbsdocs.map((list, i) => (
                            <p key={i}>
                              <img src={`${NodeURL}/uploads/profiles.png`} alt="files" className="fileviewimg" id={`fileview4${i}`} onClick={() => this.FileView(list.path)} />
                              <UncontrolledTooltip placement="bottom" target={`fileview4${i}`}>
                                View Document
                              </UncontrolledTooltip>
                              <span className="documents-name"> {list && list.name} </span>
                              <span className="documents-del">
                                <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                  {" "}
                                  <i className="fa fa-download" aria-hidden="true" />{" "}
                                </button>{" "}
                              </span>
                            </p>
                          ))}
                      </Col>
                    </div>
                  </Col>

                  <Col xs="12" md="6" className="upl-files">
                    <div className="upload-forms">
                      <Label for="exampleCustomFileBrowser6">Other Documents</Label>
                      <Col className="doc-upload">
                        {otherdocs &&
                          otherdocs.length > 0 &&
                          otherdocs.map((list, i) => (
                            <p key={i}>
                              <img src={`${NodeURL}/uploads/profiles.png`} alt="files" className="fileviewimg" id={`fileview5${i}`} onClick={() => this.FileView(list.path)} />
                              <UncontrolledTooltip placement="bottom" target={`fileview5${i}`}>
                                View Document
                              </UncontrolledTooltip>
                              <span className="documents-name">{list && list.name} </span>
                              <span className="documents-del">
                                <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                  {" "}
                                  <i className="fa fa-download" aria-hidden="true" />{" "}
                                </button>{" "}
                              </span>
                            </p>
                          ))}
                      </Col>
                    </div>
                  </Col>
                </Row>
                {path_name !== Files_URL ? (
                  <>
                    <Row>
                      <div className="col-md-12">
                        <div className="cate-ranges">
                          <h2>Declaration and Terms</h2>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Health Declaration</h4>
                            <p>
                              <span className="left-views">
                                Do you or have ever suffered from long term illness? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{sufferedlongtermillness && sufferedlongtermillness.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Have you ever required sick leave for a back or neck injury?: <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{leaveforneckinjury && leaveforneckinjury.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Have you ever required sick leave for a back or neck injury? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{neckinjuries && neckinjuries.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Have you been in contact with anyone who is suffering from a contagious illness within the last six weeks? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{sixweeksillness && sixweeksillness.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Do you suffer with a communicable disease? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{communicabledisease && communicabledisease.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Are you currently receiving active medical attention? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{medicalattention && medicalattention.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                If you have answered ‘yes’ to any of the above, please give details <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{healthdeclarationdetails}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                Are you registered disabled? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{registereddisabled && registereddisabled.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                How many days have you been absent from work due to illness in the last 12 months?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{absentfromwork && absentfromwork.toUpperCase()}</span>
                            </p>

                            <p>
                              <span className="left-views">
                                State reason(s) for absence <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{statereason}</span>
                            </p>

                            <h5 className="mt-2">GP Details</h5>
                            <p>
                              <span className="left-views">
                                GP Name <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{GPname}</span>
                            </p>

                            <h5 className="mt-2">GP Address</h5>

                            <p>
                              <span className="left-views">
                                Street Address <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{GPstreetAddress}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                County / State / Region <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{GPstate}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Health <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{GPhealthdetails}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                May we contact your Doctor for health check? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{contactDoctor && contactDoctor.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                City <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{GPcity}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                ZIP / Postal Code <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{GPzip}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Medical History</h4>
                            <p>
                              <span className="left-views">
                                Do you have any illness/impairment/disability (physical or psychological) which may affect your work? <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{medicalillness && medicalillness.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Have you ever had any illness/impairment/disability which may have been caused or made worse by your work?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{medicalillnesscaused && medicalillnesscaused.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Are you having, or waiting for treatment (including medication) or investigations at present? If your answer is yes, please provide further details of the condition, treatment and dates?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{treatment && treatment.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Do you think you may need any adjustments or assistance to help you to do the job?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{needanyadjustments && needanyadjustments.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Notes
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{needanyadjustmentsnotes}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Tuberculosis</h5>
                            <p> Clinical diagnosis and management of tuberculosis, and measures for its prevention and control (NICE 2006) </p>
                            <p>
                              <span className="left-views">
                                Have you lived continuously in the UK for the last 5 years
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{livedUK && livedUK.toUpperCase()}</span>
                            </p>{" "}
                            <p>
                              <span className="left-views">
                                Notes
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{livedUKnotes}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Have you had a BCG vaccination in relation to Tuberculosis?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{BCGvaccination && BCGvaccination.toUpperCase()}</span>
                            </p>{" "}
                            <p>
                              <span className="left-views">
                                Date
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{moment(BCGvaccinationdate).format("DD-MM-YYYY")}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                A cough which has lasted for more than 3 weeks
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{cough && cough.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Unexplained weight loss
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{weightloss && weightloss.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Unexplained fever
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{fever && fever.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Have you had tuberculosis (TB) or been in recent contact with open TB
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{TB && TB.toUpperCase()}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>

                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Chicken Pox or Shingles</h4>
                            <p>
                              <span className="left-views">
                                Have you ever had chicken pox or shingles?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{chickenpox && chickenpox.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Date
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{moment(chickenpoxdate).format("DD-MM-YYYY")}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5> Immunisation History </h5>
                            <p>
                              {" "}
                              The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body for
                              receipt of DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all staff. It
                              is a condition of proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could invalidate your
                              application or lead to the cancellation of your registration with us.{" "}
                            </p>
                            <p>
                              <span className="left-views">
                                Triple vaccination as a child (Diptheria / Tetanus / Whooping cough)?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{triplevaccination && triplevaccination.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Polio
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{polio && polio.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Tetanus
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{tetanus && tetanus.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Hepatitis B<span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{hepatitisB && hepatitisB.toUpperCase()}</span>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Varicella</h5>
                            <p>You must provide a written statement to confirm that you have had chicken pox or shingles however we strongly advise that you provide serology test result showing varicella immunity</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Tuberculosis</h5>
                            <p>We require an occupational health/GP certificate of a positive scar or a record of a positive skin test result (Do not Self Declare)</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Rubella, Measles & Mumps</h5>
                            <p>Certificate of “two” MMR vaccinations or proof of a positive antibody for Rubella Measles & Mumps</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Hepatitis B</h5>
                            <p>You must provide a copy of the most recent pathology report showing titre levels of 100lu/l or above</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Hepatitis B Surface Antigen</h5>
                            <p>Evidence of a negative Surface Antigen Test Report must be an identified validated sample. (IVS)</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Hepatitis C</h5>
                            <p>Evidence of a negative antibody test Report must be an identified validated sample. (IVS)</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>HIV</h5>
                            <p>Evidence of a negative antibody test Report must be an identified validated s ample. (IVS)</p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h5>Exposure Prone Procedures</h5>
                            <p>
                              <span className="left-views">
                                Will your role involve Exposure Prone Procedures
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{proneProcedures && proneProcedures.toUpperCase()}</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Declaration</h4>
                            <p>
                              {" "}
                              I declare that the answers to the above questions are true and complete to the best of my knowledge and belief. I also give consent for our appointed Occupational Health Services provider to make
                              recommendations to my employer{" "}
                            </p>
                            <h5 className="mt-2"> Disclosure Barring Service (DBS) </h5>
                            <p>
                              The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body for
                              receipt of DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all staff. It
                              is a condition of proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could invalidate your
                              application or lead to the cancellation of your registration with us
                            </p>
                            <p>
                              <span className="left-views">
                                Have you been convicted of a criminal offence?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{criminaloffence && criminaloffence.toUpperCase()}</span>
                            </p>
                            <p>
                              <span className="left-views">
                                Have you ever been cautioned or issued with a formal warning for a criminal offence?
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">{warningcriminaloffence && warningcriminaloffence.toUpperCase()}</span>
                            </p>
                            {/*<p>I confirm the above is true and I understand that a DBS check will be sort in the event of a successful application.</p>*/}
                            <h5 className="mt-2"> Rehabilitation of Offenders Act 1974 and Criminal Records </h5>
                            <p>
                              By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is
                              concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties. You should there
                              force list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Right To Work</h4>
                            <p>
                              It is a legal requirement that before any offer of work can be made all candidates provide the company with confirmation of their eligibility to work in the UK by providing one of the original documents
                              detailed below.
                            </p>
                            <p>
                              <span className="left-views">
                                A passport which describes the holder as a British Citizen or as having a right of abode in the United Kingdom or a passport or other travel
                                <span className="col-qus">:</span>
                              </span>
                              <span className="right-views">
                                {passport === "britishcitizen"
                                  ? "A passport which describes the holder as a British Citizen or as having a right of abode in the United Kingdom or a passport or other travel document to show that the holder has IDENFINITE LEAVE TO REMAIN in the United Kingdom and is not precluded from taking the work in question."
                                  : passport === "state"
                                    ? "A passport or identity card issued by a State which is party to the European Union and EEA Agreement and which describes the holder as a national or a state which is a arty to that agreement."
                                    : passport === "homeoffice"
                                      ? "A letter issued by the Home Office or the Department of Education and Employment indicating that the person named in the letter has permission to take agency work in question or a biometric residence permit."
                                      : ""}
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </Row>
                    <Row>
                      <div className="col-md-12">
                        <div className="cus-design">
                          <div className="cate-ranges">
                            <h4>Work Time Directives</h4>
                            <p>
                              By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment which is
                              concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties. You should there
                              force list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                            </p>
                            <p>
                              <span className="left-views">{workhours === "yes" ? "I DO wish to work more than 48 hours per week" : workhours === "no" ? "I DO NOT wish to work more than 48 hours per week" : ""}</span>
                            </p>
                            <h5 className="mt-20"> Registration form Declaration </h5>
                            <p>I declare that all information given in this registration form is to the best of my knowledge complete and accurate in all respects and that I am eligible to work in the UK.</p>
                          </div>
                        </div>
                      </div>
                    </Row>
                  </>
                ) : null}
              </>
            ) : null}
            {path_name === Canditates_URL || path_name === Interview_URL ? (
              <Row>
                <div className="col-md-12">
                  <div className="cate-ranges">
                    <h2>Verification and Interview</h2>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="cus-design">
                    <div className="cate-ranges">
                      <h4>Skills Check</h4>
                      <p>
                        <span className="left-views">
                          Date
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(skillcheckdate).format("DD-MM-YYYY")}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Notes
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{skillchecknotes}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Status
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{skillcheckstatus === 1 ? "Qualified" : "Disqualified"}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="cus-design">
                    <div className="cate-ranges">
                      <h4>DBS Check</h4>
                      <p>
                        <span className="left-views">
                          Date
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(dbscheckdate).format("DD-MM-YYYY")}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Notes
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{dbschecknotes}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Status
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{dbscheckstatus === 1 ? "Qualified" : "Disqualified"}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="cus-design">
                    <div className="cate-ranges">
                      <h4>Health Check</h4>
                      <p>
                        <span className="left-views">
                          Date
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(healthcheckdate).format("DD-MM-YYYY")}</span>
                      </p>

                      <p>
                        <span className="left-views">
                          Notes
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{healthchecknotes}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Status
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{healthcheckstatus === 1 ? "Qualified" : "Disqualified"}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="cus-design">
                    <div className="cate-ranges">
                      <h4>Interview Call</h4>
                      <p>
                        <span className="left-views">
                          Interview Date
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(interviewdate).format("DD-MM-YYYY")}</span>
                      </p>

                      <p>
                        <span className="left-views">
                          Video Call URL
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{videocallurl}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Notes
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{interviewnotes}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="cus-design">
                    <div className="cate-ranges">
                      <h4>Post Interview Result</h4>
                      <p>
                        <span className="left-views">
                          Interviewed Date
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{moment(intervieweddate).format("DD-MM-YYYY")}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Notes
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{interviewednotes}</span>
                      </p>
                      <p>
                        <span className="left-views">
                          Status
                          <span className="col-qus">:</span>
                        </span>
                        <span className="right-views">{interviewedstatus === 1 ? "Qualified" : "Disqualified"}</span>
                      </p>
                    </div>
                  </div>
                </div>
              </Row>
            ) : null}
            {path_name === Canditates_URL || path_name === Referee_URL ? (
              <>
                <Row>
                  <div className="col-md-12">
                    <div className="cate-ranges">
                      <h4>Referee Form - I</h4>
                    </div>
                  </div>
                  <div className="emp-full">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Referee Details</h5>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              First name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereefirstname}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Email
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereeemail}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Job Title
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereejobtitle}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              In what capacity did you know the applicant?
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereecapacity}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              last name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereelastname}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Phone
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {refereephone && refereephone.code} - {refereephone && refereephone.number}
                            </span>
                          </p>
                          <p>
                            <span className="left-views">
                              Company
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereecompany}</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="emp-full">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Applicant Details</h5>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              First name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantfirstname}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Email
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantemail}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Job Title
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantjobtitle}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Date Employed from
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{moment(employedfrom).format("DD-MM-YYYY")}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Would you re-employ this person?
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{remployee === "yes" ? "Yes" : remployee === "no" ? "No" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Re-employ Notes
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{remployeenotes}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Last name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantlastname}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Phone
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {applicantphone && applicantphone.code} - {applicantphone && applicantphone.number}
                            </span>
                          </p>
                          <p>
                            <span className="left-views">
                              Company
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantcompany}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Date Employed to
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{moment(employedto).format("DD-MM-YYYY")}</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="emp-full">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Section1: How Would you Access the Following</h5>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Ability to follow care plans
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{followcareplans === "excellent" ? "Excellent" : followcareplans === "good" ? "Good" : followcareplans === "average" ? "Average" : followcareplans === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Character
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{character === "excellent" ? "Excellent" : character === "good" ? "Good" : character === "average" ? "Average" : character === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Ability to ensure dignity is upheld
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{dignity === "excellent" ? "Excellent" : dignity === "good" ? "Good" : dignity === "average" ? "Average" : dignity === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Relationships with colleagues
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{relationships === "excellent" ? "Excellent" : relationships === "good" ? "Good" : relationships === "average" ? "Average" : relationships === "poor" ? "Poor" : ""}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Reliability, timekeeping,attendance
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{reliability === "excellent" ? "Excellent" : reliability === "good" ? "Good" : reliability === "average" ? "Average" : reliability === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Attitude
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{attitude === "excellent" ? "Excellent" : attitude === "good" ? "Good" : attitude === "average" ? "Average" : attitude === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Communication
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{communication === "excellent" ? "Excellent" : communication === "good" ? "Good" : communication === "average" ? "Average" : communication === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Ability to work under own initiative
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {workunderinitiative === "excellent" ? "Excellent" : workunderinitiative === "good" ? "Good" : workunderinitiative === "average" ? "Average" : workunderinitiative === "poor" ? "Poor" : ""}
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-12">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Section2: Please answer the following questions</h5>
                        <p>
                          <span className="left-views">
                            Has the applicant been subject to any disciplinary action?
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{disciplinaryaction && disciplinaryaction.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{disciplinaryactiondetails && disciplinaryactiondetails}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Are you aware of the applicants involvement in any safeguarding investigations previous or current .<span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{investigations && investigations.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{investigationsdetails && investigationsdetails}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{vulnerablepeople && vulnerablepeople.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{vulnerablepeopledetails && vulnerablepeopledetails}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{criminaloffences === "yes" ? "YES" : "NO"}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{criminaloffencedetails && criminaloffencedetails}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Additional Comments
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{additionalcomments}</span>
                        </p>

                        <p>
                          <span className="left-views">
                            Official company stamp
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views" />
                        </p>
                      </div>
                    </div>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-12">
                    <div className="cate-ranges">
                      <h4>Referee Form - II</h4>
                    </div>
                  </div>
                  <div className="emp-full">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Referee Details</h5>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              First name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereefirstname2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Email
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereeemail2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Job Title
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereejobtitle2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              In what capacity did you know the applicant?
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereecapacity2}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              last name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereelastname2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Phone
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {refereephone2 && refereephone2.code} - {refereephone2 && refereephone2.number}
                            </span>
                          </p>
                          <p>
                            <span className="left-views">
                              Company
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{refereecompany2}</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="emp-full">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Applicant Details</h5>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              First name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantfirstname2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Email
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantemail2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Job Title
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantjobtitle2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Date Employed from
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{moment(employedfrom2).format("DD-MM-YYYY")}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Would you re-employ this person?
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{remployee2 === "yes" ? "Yes" : remployee2 === "no" ? "No" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Re-employ Notes
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{remployeenotes2}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Last name
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantlastname2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Phone
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {applicantphone2 && applicantphone2.code} - {applicantphone2 && applicantphone2.number}
                            </span>
                          </p>
                          <p>
                            <span className="left-views">
                              Company
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{applicantcompany2}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Date Employed to
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{moment(employedto2).format("DD-MM-YYYY")}</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="emp-full">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Section1: How Would you Access the Following</h5>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Ability to follow care plans
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {followcareplans2 === "excellent" ? "Excellent" : followcareplans2 === "good" ? "Good" : followcareplans2 === "average" ? "Average" : followcareplans2 === "poor" ? "Poor" : ""}
                            </span>
                          </p>
                          <p>
                            <span className="left-views">
                              Character
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{character2 === "excellent" ? "Excellent" : character2 === "good" ? "Good" : character2 === "average" ? "Average" : character2 === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Ability to ensure dignity is upheld
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{dignity2 === "excellent" ? "Excellent" : dignity2 === "good" ? "Good" : dignity2 === "average" ? "Average" : dignity2 === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Relationships with colleagues
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{relationships2 === "excellent" ? "Excellent" : relationships2 === "good" ? "Good" : relationships2 === "average" ? "Average" : relationships2 === "poor" ? "Poor" : ""}</span>
                          </p>
                        </div>
                      </div>
                      <div className="info-widths">
                        <div className="cate-ranges">
                          <p>
                            <span className="left-views">
                              Reliability, timekeeping,attendance
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{reliability2 === "excellent" ? "Excellent" : reliability2 === "good" ? "Good" : reliability2 === "average" ? "Average" : reliability2 === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Attitude
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{attitude2 === "excellent" ? "Excellent" : attitude2 === "good" ? "Good" : attitude2 === "average" ? "Average" : attitude2 === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Communication
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">{communication2 === "excellent" ? "Excellent" : communication2 === "good" ? "Good" : communication2 === "average" ? "Average" : communication2 === "poor" ? "Poor" : ""}</span>
                          </p>
                          <p>
                            <span className="left-views">
                              Ability to work under own initiative
                              <span className="col-qus">:</span>
                            </span>
                            <span className="right-views">
                              {workunderinitiative2 === "excellent" ? "Excellent" : workunderinitiative2 === "good" ? "Good" : workunderinitiative2 === "average" ? "Average" : workunderinitiative2 === "poor" ? "Poor" : ""}
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-12">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h5>Section2: Please answer the following questions</h5>
                        <p>
                          <span className="left-views">
                            Has the applicant been subject to any disciplinary action?
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{disciplinaryaction2 && disciplinaryaction2.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{disciplinaryactiondetails2 && disciplinaryactiondetails2}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Are you aware of the applicants involvement in any safeguarding investigations previous or current .<span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{investigations2 && investigations2.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{investigationsdetails2 && investigationsdetails2}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{vulnerablepeople2 && vulnerablepeople2.toUpperCase()}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{vulnerablepeopledetails2 && vulnerablepeopledetails2}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{criminaloffence2 === "yes" ? "YES" : "NO"}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Please provide details:
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{criminaloffencedetails2 && criminaloffencedetails2}</span>
                        </p>
                        <p>
                          <span className="left-views">
                            Additional Comments
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{additionalcomments2}</span>
                        </p>

                        <p>
                          <span className="left-views">
                            Official company stamp
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views" />
                        </p>
                      </div>
                    </div>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-6">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h4>Reference 1 Verification</h4>

                        <p>
                          <span className="left-views">
                            Date
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{moment(reference_details1 && reference_details1.verified_date).format("DD-MM-YYYY")}</span>
                        </p>

                        <p>
                          <span className="left-views">
                            Notes
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{reference_details1 && reference_details1.verified_notes}</span>
                        </p>

                        <p>
                          <span className="left-views">
                            Status
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{reference_details1 && reference_details1.verified_status === 1 ? "Qualified" : "Disqualified"}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="cus-design">
                      <div className="cate-ranges">
                        <h4>Reference 2 Verification</h4>

                        <p>
                          <span className="left-views">
                            Date
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{moment(reference_details2 && reference_details2.verified_date).format("DD-MM-YYYY")}</span>
                        </p>

                        <p>
                          <span className="left-views">
                            Notes
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{reference_details2 && reference_details2.verified_notes}</span>
                        </p>

                        <p>
                          <span className="left-views">
                            Status
                            <span className="col-qus">:</span>
                          </span>
                          <span className="right-views">{reference_details2 && reference_details2.verified_status === 1 ? "Qualified" : "Disqualified"}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </Row>
              </>
            ) : null}
            {path_name === Canditates_URL || path_name === Summary_URL ? (
              <>
                <Row>
                  <div className="col-md-12">
                      <div className="table-responsive mt-2">
                          <Table hover bordered responsive>
                              <thead>
                              <tr>
                                  <th>Context</th>
                                  <th>Date</th>
                                  <th>Notes</th>
                                  <th>Status</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>Interview</td>
                                  <td>{moment(intervieweddate).format("DD-MM-YYYY")}</td>
                                  <td>{interviewednotes}</td>
                                  <td>{interviewedstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                  <td>Skill Check</td>
                                  <td>{moment(skillcheckdate).format("DD-MM-YYYY")}</td>
                                  <td>{skillchecknotes}</td>
                                  <td>{skillcheckstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                  <td>DBS Check</td>
                                  <td>{moment(dbscheckdate).format("DD-MM-YYYY")}</td>
                                  <td>{dbschecknotes}</td>
                                  <td>{dbscheckstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                  <td>Health Check</td>
                                  <td>{moment(healthcheckdate).format("DD-MM-YYYY")}</td>
                                  <td>{healthchecknotes}</td>
                                  <td>{healthcheckstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>

                              <tr>
                                  <td>Reference 1</td>
                                  <td>{moment(referenceverifydate).format("DD-MM-YYYY")}</td>
                                  <td>{reference_details1 && reference_details1.verified_notes}</td>
                                  <td>{reference_details1 && reference_details1.verified_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                  <td>Reference 2</td>
                                  <td>{moment(referenceverifydate2).format("DD-MM-YYYY")}</td>
                                  <td>{reference_details2 && reference_details2.verified_notes}</td>
                                  <td>{reference_details2 && reference_details2.verified_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                  <td>
                                      <h5>Final Verification</h5>
                                  </td>
                                  <td>{moment(final_verify_date).format("DD-MM-YYYY")}</td>
                                  <td>{final_verify_notes}</td>
                                  <td>{final_verify_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              </tbody>
                          </Table>
                      </div>
                  </div>
                </Row>
              </>
            ) : null}
          </CardBody>
        </Card>
        <Modal isOpen={this.state.fileviewmodal} toggle={() => this.FileView("")} className="file_view_modal modal-lg" backdrop={"static"} external={externalCloseBtn}>
         {filepath && fileext && <FileViewer fileType={fileext} filePath={filepath} onError={() => toast.error("File Not Found/Supported")} />}
        </Modal>
      </div>
    );
  }
}

export default Candidatelist;
