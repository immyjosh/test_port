import React, { Component, Fragment } from "react";
import {
  TabContent,
  TabPane,
  Table,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  Button,
  Card,
  Collapse,
  CustomInput,
  CardBody,
  CardFooter,
  CardHeader,
  Label,
  Input,
  Badge,
  ModalHeader,
  ModalBody,
  ModalFooter,
  UncontrolledTooltip,
  Modal
} from "reactstrap";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { AvForm, AvField, AvGroup, AvInput, AvRadioGroup, AvRadio } from "availity-reactstrap-validation";
import request, { client, NodeURL } from "../../../api/api";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import classnames from "classnames";
import { DateRangePicker, SingleDatePicker } from "react-dates";
import { Link } from "react-router-dom";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import FileSaver from "file-saver";
import { Editor } from "@tinymce/tinymce-react/lib/es2015";
import { Settings } from "../../../api/key";
const Canditates_URL = "/agency/recruitmentform/candidates";
const Application_URL = "/agency/recruitmentform/application";
const Interview_URL = "/agency/recruitmentform/interview";
const Files_URL = "/agency/recruitmentform/files";
const Referee_URL = "/agency/recruitmentform/references";
const Summary_URL = "/agency/recruitmentform/summary";

class onlineform extends Component {
  state = {
    otheremp: false,
    drivelicenceyes: false,
    caraccessyes: false,
    IELTStestyes: false,
    activeTab: "1",
    availablestartdate: "",
    mobileno: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    dateofbirth: "",
    UploadIDPhoto: "",
    kinmobileno: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    employer: "",
    position: "",
    duties: "",
    salaryonleaving: "",
    institution: "",
    course: "",
    year: "",
    grade: "",
    UploadCV: "",
    firstrefphone: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    secondrefphone: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    BCGvaccinationyes: false,
    employmenthistorylist: [],
    educationlist: [],
    skillcheckdate: moment(new Date()),
    skillcheckstatus: "",
    dbscheckdate: moment(new Date()),
    dbscheckstatus: "",
    healthcheckdate: moment(new Date()),
    healthcheckstatus: "",
    referenceverifydate: moment(new Date()),
    referenceverifystatus: "",
    referenceverifynotes: "",
    referenceverifydate2: moment(new Date()),
    referenceverifystatus2: "",
    referenceverifynotes2: "",
    refereephone: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    refereephone2: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    applicantphone: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    applicantphone2: {
      number: "",
      code: "",
      dialcountry: "gb"
    },
    phoneerror: false,
    phoneerror1: false,
    phoneerror2: false,
    phoneerror3: false,
    phoneerror4: false,
    phoneerror5: false,
    phoneerror6: false,
    phoneerror7: false,
    refereecollapse1: true,
    refereecollapse2: false,
    mandatorytrainingyes: false,
    healthcaretrainingyes: false,
    livedUKyes: false
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    const path_name = this.props && this.props.location && this.props.location.pathname ? this.props.location.pathname : "";
    this.setState({
      Emp_ID: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid,
      path_name
    });
    request({
      url: "/agency/jobtype/list",
      method: "POST",
      data: { status: 1, for: "editemployee" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/administrators/employee/get",
      method: "POST",
      data: { id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID }
    }).then(res => {
      if (res.status === 1) {
        const res_result = res.response.result && res.response.result.length > 0 ? res.response.result[0] : "";
        const form_data = res_result.onlineform ? res_result.onlineform : {};
        const form_status = res_result.onlineform && res_result.onlineform.form_status ? res_result.onlineform.form_status : 0;
        if (form_data) {
          this.setState({
            form_status: form_status,
            form_data: form_data,
            employee_data: res_result,
            employee_email: res_result.email,
            // uploadcertificate: form_data.uploadcertificate,
            // uploadpassport: form_data.uploadpassport,
            // uploadaddress: form_data.uploadaddress,
            // uploaddbs: form_data.uploaddbs,
            // uploadcertificateName: form_data.uploadcertificate && form_data.uploadcertificate.substr(24, 30),
            // uploadpassportName: form_data.uploadpassport && form_data.uploadpassport.substr(24, 30),
            // uploadaddressName: form_data.uploadaddress && form_data.uploadaddress.substr(24, 30),
            // uploadDBSName: form_data.uploaddbs && form_data.uploaddbs.substr(24, 30),
            // certificatedocs: form_data.uploadcertificate && form_data.uploadcertificate.length > 0 ? form_data.uploadcertificate : [],
            // addressdocs: form_data.uploadaddress && form_data.uploadaddress.length > 0 ? form_data.uploadaddress : [],
            // passportdocs: form_data.uploadpassport && form_data.uploadpassport.length > 0 ? form_data.uploadpassport : [],
            // dbsdocs: form_data.uploaddbs && form_data.uploaddbs.length > 0 ? form_data.uploaddbs : [],
            // otherdocs: form_data.uploadothers && form_data.uploadothers.length > 0 ? form_data.uploadothers : [],
            final_verify_status: form_data.final_verify_status,
            final_verify_date: moment(form_data.final_verify_date),
            final_verify_notes: form_data.final_verify_notes,
            mandatorytrainingyes: form_data.mandatory_training === "yes",
            healthcaretrainingyes: form_data.healthcare_training === "yes",
            IELTStestyes: form_data.langauages && form_data.langauages.IELTStest === "yes",
            needanyadjustmentsyes: form_data.medicalhistory && form_data.medicalhistory.needanyadjustments === "yes",
            livedUKyes: form_data.tuberculosis && form_data.tuberculosis.livedUK === "no"
          });
          if (path_name === Canditates_URL || path_name === Application_URL) {
            const form_personaldetails = res_result.onlineform && res_result.onlineform.personaldetails ? res_result.onlineform.personaldetails : {};
            const form_driving = res_result.onlineform && res_result.onlineform.drivingdetails ? res_result.onlineform.drivingdetails : {};
            const form_kin = res_result.onlineform && res_result.onlineform.nextofkindetails ? res_result.onlineform.nextofkindetails : {};
            this.setState({
              availablestartdate: moment(form_data.availablestartdate),
              mobileno: form_personaldetails.mobileno || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              dateofbirth: moment(form_personaldetails.dateofbirth),
              UploadIDPhoto: form_data.UploadIDPhoto,
              UploadIDName: form_data.UploadIDPhoto && form_data.UploadIDPhoto.substr(24, 30),
              drivelicenceyes: form_driving.drivinglicence === "yes",
              caraccessyes: form_driving.caraccess === "yes",
              UploadCV: form_data.UploadCV && form_data.UploadCV,
              UploadCVName: form_data.UploadCV && form_data.UploadCV.substr(24, 30),
              kinmobileno: form_kin.kinmobileno || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              employmenthistorylist: form_data.employmenthistory,
              educationlist: form_data.education,
              firstrefphone: (form_data.reference1 && form_data.reference1.firstrefphone) || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              secondrefphone: (form_data.reference2 && form_data.reference2.secondrefphone) || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              BCGvaccinationyes: form_data.tuberculosis && form_data.tuberculosis.BCGvaccination === "yes",
              chickenpoxyes: form_data.chickenpoxorshingles && form_data.chickenpoxorshingles.chickenpox === "yes"
            });
          }
          if (path_name === Canditates_URL || path_name === Interview_URL || path_name === Summary_URL) {
            const form_skillcheck = res_result.onlineform && res_result.onlineform.skillcheck ? res_result.onlineform.skillcheck : 0;
            const form_healthcheck = res_result.onlineform && res_result.onlineform.healthcheck ? res_result.onlineform.healthcheck : 0;
            const form_dbscheck = res_result.onlineform && res_result.onlineform.dbscheck ? res_result.onlineform.dbscheck : 0;
            const form_interview = res_result.onlineform && res_result.onlineform.interview ? res_result.onlineform.interview : 0;
            this.setState({
              skillcheckdate: form_skillcheck.date ? moment(form_skillcheck.date) : moment(new Date()),
              skillcheckstatus: form_skillcheck.status,
              dbscheckdate: form_dbscheck.date ? moment(form_dbscheck.date) : moment(new Date()),
              dbscheckstatus: form_dbscheck.status,
              healthcheckdate: form_healthcheck.date ? moment(form_healthcheck.date) : moment(new Date()),
              healthcheckstatus: form_healthcheck.status,
              interviewdate: form_interview.interviewdate ? moment(form_interview.interviewdate) : moment(new Date()),
              interviewtime: form_interview.interviewtime ? form_interview.interviewtime : moment(new Date()),
              interviewnotes: form_interview.interviewnotes,
              intervieweddate: form_interview.intervieweddate ? moment(form_interview.intervieweddate) : moment(new Date()),
              interviewedstatus: form_interview.status,
              videocallurl: form_interview.videocallurl
            });
          }
          if (path_name === Canditates_URL || path_name === Referee_URL || path_name === Summary_URL) {
            const form_reference1 = res_result.onlineform && res_result.onlineform.reference1 && res_result.onlineform.reference1.referee ? res_result.onlineform.reference1.referee : {};
            const form_reference2 = res_result.onlineform && res_result.onlineform.reference2 && res_result.onlineform.reference2.referee ? res_result.onlineform.reference2.referee : {};
            this.setState({
              refemailsend1: form_data.reference1 && form_data.reference1.firstrefemail,
              refemailsend2: form_data.reference2 && form_data.reference2.secondrefemail,
              refereename1: `${form_data.reference1.firstreffirstname}${form_data.reference1.firstreflastname}`,
              refereename2: `${form_data.reference2.secondreffirstname}${form_data.reference2.secondreflastname}`,
              refereephone: form_reference1.refereephone || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              applicantphone: form_reference1.applicantphone || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              employedfrom: moment(form_reference1.employedfrom),
              employedto: moment(form_reference1.employedto),
              campanystamp: form_reference1.campanystamp,
              campanystampName: form_reference1.campanystamp && form_reference1.campanystamp.substr(24, 30),
              referenceverifydate: moment(form_reference1.verified_date),
              referenceverifynotes: form_reference1.verified_notes,
              referenceverifystatus: form_reference1.verified_status,
              refereestatus1: form_reference1.status,
              remployeeyes: form_reference1.remployee === "no",
              disciplinaryactionyes: form_reference1.disciplinaryaction === "yes",
              investigationsyes: form_reference1.investigations === "yes",
              vulnerablepeopleyes: form_reference1.vulnerablepeople === "yes",
              criminaloffenceyes: form_reference1.criminaloffence === "yes",
              refereephone2: form_reference2.refereephone || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              applicantphone2: form_reference2.applicantphone || {
                number: "",
                code: "",
                dialcountry: "gb"
              },
              employedfrom2: moment(form_reference2.employedfrom),
              employedto2: moment(form_reference2.employedto),
              campanystamp2: form_reference2.campanystamp,
              campanystampName2: form_reference2.campanystamp && form_reference2.campanystamp.substr(24, 30),
              referenceverifydate2: moment(form_reference2.verified_date),
              referenceverifynotes2: form_reference2.verified_notes,
              referenceverifystatus2: form_reference2.verified_status,
              refereestatus2: form_reference2.status,
              remployeeyes2: form_reference2.remployee === "no",
              disciplinaryactionyes2: form_reference2.disciplinaryaction === "yes",
              investigationsyes2: form_reference2.investigations === "yes",
              vulnerablepeopleyes2: form_reference2.vulnerablepeople === "yes",
              criminaloffenceyes2: form_reference2.criminaloffence === "yes"
            });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    if (path_name === Canditates_URL || path_name === Application_URL) {
      this.setState({ activeTab: "1" });
    } else if (path_name === Interview_URL) {
      this.setState({ activeTab: "6" });
    } else if (path_name === Files_URL) {
      this.setState({ activeTab: "4" });
    } else if (path_name === Referee_URL) {
      this.setState({ activeTab: "7" });
    } else if (path_name === Summary_URL) {
      this.setState({ activeTab: "8" });
    }
  }
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  addmploymenthistory = () => {
    if (this.state.employer === "" || this.state.position === "" || this.state.duties === "" || this.state.start_date === "" || this.state.end_date === "" || this.state.salaryonleaving === "") {
      toast.error("Please fill all fields");
    } else {
      const value = {
        employer: this.state.employer,
        position: this.state.position,
        duties: this.state.duties,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
        salaryonleaving: this.state.salaryonleaving
      };
      this.setState({ employmenthistorylist: this.state.employmenthistorylist.concat([value]) });
    }
  };
  removeemploymenthistory = value => {
    const arr = this.state.employmenthistorylist.filter((list, i) => i !== value);
    this.setState({ employmenthistorylist: arr });
  };
  addeducationlist = () => {
    if (this.state.institution === "" || this.state.year === "" || this.state.course === "" || this.state.grade === "") {
      toast.error("Please fill all fields");
    } else {
      const value = { year: this.state.year, institution: this.state.institution, course: this.state.course, grade: this.state.grade };
      this.setState({ educationlist: this.state.educationlist.concat([value]) });
    }
  };
  removeeducationlist = value => {
    const arr = this.state.educationlist.filter((list, i) => i !== value);
    this.setState({ educationlist: arr });
  };
  otheremp = value => {
    if (value === "other") {
      this.setState({
        otheremp: true
      });
    } else {
      this.setState({
        otheremp: false
      });
    }
  };
  drivelicenceyes = yes => {
    if (yes === "yes") {
      this.setState({
        drivelicenceyes: true
      });
    } else {
      this.setState({
        drivelicenceyes: false
      });
    }
  };
  caraccessyes = yes => {
    if (yes === "yes") {
      this.setState({
        caraccessyes: true
      });
    } else {
      this.setState({
        caraccessyes: false
      });
    }
  };
  DBSyes = yes => {
    if (yes === "yes") {
      this.setState({
        DBSyes: true
      });
    } else {
      this.setState({
        DBSyes: false
      });
    }
  };
  IELTStestyes = yes => {
    if (yes === "yes") {
      this.setState({
        IELTStestyes: true
      });
    } else {
      this.setState({
        IELTStestyes: false
      });
    }
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleChange = date => {
    this.setState({
      availablestartdate: date
    });
  };
  DBSChange = date => {
    this.setState({
      DBSdate: date
    });
  };
  dateChange = date => {
    this.setState({
      dateofbirth: date
    });
  };
  fileChangedHandler = evt => {
    this.setState({
      UploadIDPhoto: evt.target.files[0],
      UploadIDfile: URL.createObjectURL(evt.target.files[0]),
      UploadIDName: evt.target.files[0].name
    });
  };
  fileChangedHandlerCN1 = evt => {
    this.setState({
      campanystamp: evt.target.files[0],
      campanystampfile: URL.createObjectURL(evt.target.files[0]),
      campanystampName: evt.target.files[0].name
    });
  };
  fileChangedHandlerCN2 = evt => {
    this.setState({
      campanystamp2Photo: evt.target.files[0],
      campanystamp2file: URL.createObjectURL(evt.target.files[0]),
      campanystamp2Name: evt.target.files[0].name
    });
  };
  uploadCV = evt => {
    this.setState({
      UploadCV: evt.target.files[0],
      UploadCVfile: URL.createObjectURL(evt.target.files[0]),
      UploadCVName: evt.target.files[0].name
    });
  };
  uploadCertificate = evt => {
    this.setState({
      uploadcertificate: evt.target.files[0],
      uploadcertificatefile: URL.createObjectURL(evt.target.files[0]),
      uploadcertificateName: evt.target.files[0].name
    });
  };
  uploadPassport = evt => {
    this.setState({
      uploadpassport: evt.target.files[0],
      uploadpassportfile: URL.createObjectURL(evt.target.files[0]),
      uploadpassportName: evt.target.files[0].name
    });
  };
  uploadAddress = evt => {
    this.setState({
      uploadaddress: evt.target.files[0],
      uploadaddressfile: URL.createObjectURL(evt.target.files[0]),
      uploadaddressName: evt.target.files[0].name
    });
  };
  uploadDBS = evt => {
    this.setState({
      uploaddbs: evt.target.files[0],
      uploaddbsfile: URL.createObjectURL(evt.target.files[0]),
      uploadDBSName: evt.target.files[0].name
    });
  };
  mandatorytraining = yes => {
    if (yes === "yes") {
      this.setState({
        mandatorytrainingyes: true
      });
    } else {
      this.setState({
        mandatorytrainingyes: false
      });
    }
  };
  healthcaretraining = yes => {
    if (yes === "yes") {
      this.setState({
        healthcaretrainingyes: true
      });
    } else {
      this.setState({
        healthcaretrainingyes: false
      });
    }
  };
  needanyadjustments = yes => {
    if (yes === "yes") {
      this.setState({
        needanyadjustmentsyes: true
      });
    } else {
      this.setState({
        needanyadjustmentsyes: false
      });
    }
  };
  livedUK = no => {
    if (no === "no") {
      this.setState({
        livedUKyes: true
      });
    } else {
      this.setState({
        livedUKyes: false
      });
    }
  };
  BCGvaccination = yes => {
    if (yes === "yes") {
      this.setState({
        BCGvaccinationyes: true
      });
    } else {
      this.setState({
        BCGvaccinationyes: false
      });
    }
  };
  chickenpox = yes => {
    if (yes === "yes") {
      this.setState({
        chickenpoxyes: true
      });
    } else {
      this.setState({
        chickenpoxyes: false
      });
    }
  };
  OnFormSubmit = (event, errors, values) => {
    if (errors && errors.length === 0) {
      if (this.state.activeTab === "1") {
        if (this.state.availablestartdate === "" || this.state.dateofbirth === "") {
          toast.error("Select D.O.B / Available to work start date");
        } else {
          const data = new FormData();
          data.append("forpage", 1);
          data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
          data.append("postapplyfor", values.postapplyfor);
          data.append("postapplydetails", values.postapplydetails);
          data.append("availablestartdate", this.state.availablestartdate);
          data.append("personaldetails[title]", values.title);
          data.append("personaldetails[firstname]", values.firstname);
          data.append("personaldetails[middlename]", values.middlename);
          data.append("personaldetails[lastname]", values.lastname);
          data.append("personaldetails[homephone]", values.homephone);
          data.append("personaldetails[mobileno][number]", this.state.mobileno.number);
          data.append("personaldetails[mobileno][code]", this.state.mobileno.code);
          data.append("personaldetails[mobileno][dialcountry]", this.state.mobileno.dialcountry);
          data.append("personaldetails[email]", values.email);
          data.append("personaldetails[dateofbirth]", this.state.dateofbirth);
          data.append("personaldetails[insuranceno]", values.insuranceno);
          data.append("personaldetails[address][streetAddress]", values.streetAddress);
          data.append("personaldetails[address][addressline2]", values.addressline2);
          data.append("personaldetails[address][city]", values.city);
          data.append("personaldetails[address][state]", values.state);
          data.append("personaldetails[address][zip]", values.zip);
          data.append("personaldetails[address][country]", values.country);
          data.append("personaldetails[address][label]", values.label);
          data.append("personaldetails[address][title]", values.title);
          data.append("personaldetails[nationality]", values.nationality);
          data.append("personaldetails[gender]", values.gender);
          data.append("personaldetails[religion]", values.religion);
          data.append("personaldetails[race]", values.race);
          data.append("personaldetails[sexualorientation]", values.sexualorientation);
          data.append("UploadIDPhoto", this.state.UploadIDPhoto);
          data.append("employmenteligibility[visapermit]", values.visapermit);
          data.append("drivingdetails[drivinglicence]", values.drivinglicence);
          data.append("drivingdetails[traveldetails]", values.traveldetails);
          data.append("drivingdetails[drivinglicenceno]", values.drivinglicenceno || "");
          data.append("drivingdetails[caraccess]", values.caraccess);
          data.append("drivingdetails[bannedfromdriving]", values.bannedfromdriving);
          data.append("drivingdetails[carinsurance]", values.carinsurance);
          data.append("drivingdetails[vehicledocumentsvalid]", values.vehicledocumentsvalid);
          data.append("langauages[englishspoken]", values.englishspoken);
          data.append("langauages[englishwritten]", values.englishwritten);
          data.append("langauages[otherlanguage]", values.otherlanguage);
          data.append("langauages[IELTStest]", values.IELTStest);
          data.append("nextofkindetails[kinprefix]", values.kinprefix);
          data.append("nextofkindetails[kinfirstname]", values.kinfirstname);
          data.append("nextofkindetails[kinlastname]", values.kinlastname);
          data.append("nextofkindetails[kinrelationship]", values.kinrelationship);
          data.append("nextofkindetails[kinhomephone]", values.kinhomephone);
          data.append("nextofkindetails[kinmobileno][number]", this.state.kinmobileno.number);
          data.append("nextofkindetails[kinmobileno][code]", this.state.kinmobileno.code);
          data.append("nextofkindetails[kinmobileno][dialcountry]", this.state.kinmobileno.dialcountry);
          data.append("nextofkindetails[kinemail]", values.kinemail);
          data.append("nextofkindetails[Address][kinstreetAddress]", values.kinstreetAddress);
          data.append("nextofkindetails[Address][kinaddressline2]", values.kinaddressline2);
          data.append("nextofkindetails[Address][kincity]", values.kincity);
          data.append("nextofkindetails[Address][kinstate]", values.kinstate);
          data.append("nextofkindetails[Address][kinzip]", values.kinzip);
          data.append("nextofkindetails[Address][kincountry]", values.kincountry);
          data.append("aboutyourwork[abletowork][work_mornings]", values.work_mornings);
          data.append("aboutyourwork[abletowork][work_evenings]", values.work_evenings);
          data.append("aboutyourwork[abletowork][work_afternoons]", values.work_afternoons);
          data.append("aboutyourwork[abletowork][work_occasional]", values.work_occasional);
          data.append("aboutyourwork[abletowork][work_fulltime]", values.work_fulltime);
          data.append("aboutyourwork[abletowork][work_parttime]", values.work_parttime);
          data.append("aboutyourwork[abletowork][work_nights]", values.work_nights);
          data.append("aboutyourwork[abletowork][work_weekends]", values.work_weekends);
          data.append("aboutyourwork[abletowork][work_anytime]", values.work_anytime);
          data.append("aboutyourwork[firstchoice]", values.firstchoice);
          data.append("aboutyourwork[secondchoice]", values.secondchoice);
          data.append("aboutyourwork[thirdchoice]", values.thirdchoice);
          data.append("aboutyourwork[shortnotice]", values.shortnotice);
          data.append("aboutyourwork[reduceworkflexibility]", values.reduceworkflexibility);
          data.append("aboutyourwork[workstate]", values.workstate);
          this.save_online_form(data);
        }
      } else if (this.state.activeTab === "2" || this.state.activeTab === "7") {
        if (this.state.educationlist.length === 0 || this.state.employmenthistorylist === 0) {
          toast.error("Please fill all required fields");
        } else {
          const data = new FormData();
          data.append("forpage", 2);
          data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
          data.append("UploadCV", this.state.UploadCV);
          data.append("reference1[firstrefrelationship]", values.firstrefrelationship);
          data.append("reference1[firstreffirstname]", values.firstreffirstname);
          data.append("reference1[firstreflastname]", values.firstreflastname);
          data.append("reference1[firstrefphone][number]", this.state.firstrefphone.number);
          data.append("reference1[firstrefphone][code]", this.state.firstrefphone.code);
          data.append("reference1[firstrefphone][dialcountry]", this.state.firstrefphone.dialcountry);
          data.append("reference1[firstrefemail]", values.firstrefemail);
          data.append("reference1[firstrefconfirmemail]", values.firstrefconfirmemail);
          data.append("reference1[Address][firstrefstreetAddress]", values.firstrefstreetAddress);
          data.append("reference1[Address][firstrefcity]", values.firstrefcity);
          data.append("reference1[Address][firstrefstate]", values.firstrefstate);
          data.append("reference1[Address][firstrefzip]", values.firstrefzip);
          data.append("reference2[secondrefrelationship]", values.secondrefrelationship);
          data.append("reference2[secondreffirstname]", values.secondreffirstname);
          data.append("reference2[secondreflastname]", values.secondreflastname);
          data.append("reference2[secondrefphone][number]", this.state.secondrefphone.number);
          data.append("reference2[secondrefphone][code]", this.state.secondrefphone.code);
          data.append("reference2[secondrefphone][dialcountry]", this.state.secondrefphone.dialcountry);
          data.append("reference2[secondrefemail]", values.secondrefemail);
          data.append("reference2[secondrefconfirmemail]", values.secondrefconfirmemail);
          data.append("reference2[Address][secondrefstreetAddress]", values.secondrefstreetAddress);
          data.append("reference2[Address][secondrefcity]", values.secondrefcity);
          data.append("reference2[Address][secondrefstate]", values.secondrefstate);
          data.append("reference2[Address][secondrefzip]", values.secondrefzip);
          this.state.employmenthistorylist.map((item, i) => {
            if (item) {
              data.append(`employmenthistory[${i}][employer]`, item.employer);
              data.append(`employmenthistory[${i}][position]`, item.position);
              data.append(`employmenthistory[${i}][duties]`, item.duties);
              data.append(`employmenthistory[${i}][start_date]`, item.start_date);
              data.append(`employmenthistory[${i}][end_date]`, item.end_date);
              data.append(`employmenthistory[${i}][salaryonleaving]`, item.salaryonleaving);
            }
            return true;
          });
          this.state.educationlist.map((item, i) => {
            if (item) {
              data.append(`education[${i}][institution]`, item.institution);
              data.append(`education[${i}][year]`, item.year);
              data.append(`education[${i}][course]`, item.course);
              data.append(`education[${i}][grade]`, item.grade);
            }
            return true;
          });
          this.save_online_form(data);
        }
      } else if (this.state.activeTab === "3") {
        const data = new FormData();
        data.append("forpage", 3);
        data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
        data.append("movinghandling", values.movinghandling);
        data.append("basiclifesupport", values.basiclifesupport);
        data.append("healthsafety", values.healthsafety);
        data.append("firesafety", values.firesafety);
        data.append("firstaid", values.firstaid);
        data.append("infectioncontrol", values.infectioncontrol);
        data.append("foodsafety", values.foodsafety);
        data.append("medicationadmin", values.medicationadmin);
        data.append("safeguardingvulnerable", values.safeguardingvulnerable);
        data.append("trainingdates", values.trainingdates);
        data.append("healthcare_training", values.healthcare_training);
        data.append("diplomal3", values.diplomal3);
        data.append("diplomal2", values.diplomal2);
        data.append("personalsafetymental", values.personalsafetymental);
        data.append("intermediatelife", values.intermediatelife);
        data.append("advancedlife", values.advancedlife);
        data.append("complaintshandling", values.complaintshandling);
        data.append("handlingviolence", values.handlingviolence);
        data.append("doolsmental", values.doolsmental);
        data.append("coshh", values.coshh);
        data.append("dataprotection", values.dataprotection);
        data.append("equalityinclusion", values.equalityinclusion);
        data.append("loneworkertraining", values.loneworkertraining);
        data.append("resuscitation", values.resuscitation);
        data.append("interpretation", values.interpretation);
        data.append("trainindates2", values.trainindates2);
        this.save_online_form(data);
      } else if (this.state.activeTab === "4") {
        if (!this.state.uploadcertificate || !this.state.uploadpassport || !this.state.uploadaddress || !this.state.uploaddbs) {
          toast.error("Please fill all required fields");
        } else {
          const data = new FormData();
          data.append("forpage", 4);
          data.append("uploadcertificate", this.state.uploadcertificate);
          data.append("uploadpassport", this.state.uploadpassport);
          data.append("uploadaddress", this.state.uploadaddress);
          data.append("uploaddbs", this.state.uploaddbs);
          this.save_online_form(data);
        }
      } else if (this.state.activeTab === "5") {
        const data = new FormData();
        data.append("forpage", 5);
        data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
        data.append("healthdeclaration[sufferedlongtermillness]", values.sufferedlongtermillness);
        data.append("healthdeclaration[leaveforneckinjury]", values.leaveforneckinjury);
        data.append("healthdeclaration[neckinjuries]", values.neckinjuries);
        data.append("healthdeclaration[sixweeksillness]", values.sixweeksillness);
        data.append("healthdeclaration[communicabledisease]", values.communicabledisease);
        data.append("healthdeclaration[medicalattention]", values.medicalattention);
        data.append("healthdeclaration[healthdeclarationdetails]", values.healthdeclarationdetails);
        data.append("healthdeclaration[registereddisabled]", values.registereddisabled);
        data.append("healthdeclaration[absentfromwork]", values.absentfromwork);
        data.append("healthdeclaration[statereason]", values.statereason);
        data.append("gpdetails[GPname]", values.GPname);
        data.append("gpdetails[gpaddress][GPstreetAddress]", values.GPstreetAddress);
        data.append("gpdetails[gpaddress][GPcity]", values.GPcity);
        data.append("gpdetails[gpaddress][GPstate]", values.GPstate);
        data.append("gpdetails[gpaddress][GPzip]", values.GPzip);
        data.append("gpdetails[gpaddress][contactDoctor]", values.contactDoctor);
        data.append("gpdetails[gpaddress][GPhealthdetails]", values.GPhealthdetails);
        data.append("medicalhistory[medicalillness]", values.medicalillness);
        data.append("medicalhistory[medicalillnesscaused]", values.medicalillnesscaused);
        data.append("medicalhistory[treatment]", values.treatment);
        data.append("medicalhistory[needanyadjustments]", values.needanyadjustments);
        data.append("medicalhistory[needanyadjustmentsnotes]", values.needanyadjustmentsnotes || "");
        data.append("tuberculosis[livedUK]", values.livedUK);
        data.append("tuberculosis[livedUKnotes]", values.livedUKnotes || "");
        data.append("tuberculosis[BCGvaccination]", values.BCGvaccination);
        data.append("tuberculosis[BCGvaccinationdate]", moment(values.BCGvaccinationdate).format("DD/MM/YYYY") || "");
        data.append("tuberculosis[cough]", values.cough);
        data.append("tuberculosis[weightloss]", values.weightloss);
        data.append("tuberculosis[fever]", values.fever);
        data.append("tuberculosis[TB]", values.TB);
        data.append("chickenpoxorshingles[chickenpox]", values.chickenpox);
        data.append("chickenpoxorshingles[chickenpoxdate]", moment(values.chickenpoxdate).format("DD/MM/YYYY") || "");
        data.append("immunisationhistory[triplevaccination]", values.triplevaccination);
        data.append("immunisationhistory[polio]", values.polio);
        data.append("immunisationhistory[tetanus]", values.tetanus);
        data.append("immunisationhistory[hepatitisB]", values.hepatitisB);
        data.append("immunisationhistory[proneProcedures]", values.proneProcedures);
        data.append("declaration[occupationalHealthServices]", values.occupationalHealthServices);
        data.append("declaration[criminaloffence]", values.criminaloffence);
        data.append("declaration[warningcriminaloffence]", values.warningcriminaloffence);
        data.append("declaration[DBSdetails]", values.DBSdetails);
        data.append("righttowork[passport]", values.passport);
        data.append("worktimedirectives[workhours]", values.workhours);
        this.save_online_form(data);
      }
    } else {
      toast.error("Please fill all required fields");
    }
  };
  save_online_form(data) {
    let toastId = "save_online_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/agency/employees/apply-online-form/update",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: "Updated!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  VerificationSubmit = (event, errors, values) => {
    if (errors && errors.length === 0) {
      let toastId = "verification_online_form_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        skillcheck: {
          date: this.state.skillcheckdate,
          notes: values.skillchecknotes,
          status: this.state.skillcheckstatus
        },
        healthcheck: {
          date: this.state.healthcheckdate,
          notes: values.healthchecknotes,
          status: this.state.healthcheckstatus
        },
        dbscheck: {
          date: this.state.dbscheckdate,
          notes: values.dbschecknotes,
          status: this.state.dbscheckstatus
        }
      };
      request({
        url: "/agency/employees/apply-online-form/verify",
        method: "POST",
        data: data
      }).then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          toast.update(toastId, { render: "Updated!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.CheckFormModal();
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      });
    }
  };

  ReferenceVerification1 = () => {
    if (this.state.referenceverifydate && this.state.referenceverifynotes) {
      let toastId = "referee_verification_form_02";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        referencefor: "reference1",
        referenceverifystatus: this.state.referenceverifystatus,
        referenceverifydate: this.state.referenceverifydate,
        referenceverifynotes: this.state.referenceverifynotes
      };
      this.referenceverifyapi(data, toastId);
    } else {
      toast.error("Please fill all fields");
    }
  };
  ReferenceVerification2 = () => {
    if (this.state.referenceverifydate2 && this.state.referenceverifynotes2) {
      let toastId = "referee_verification_form_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        referencefor: "reference2",
        referenceverifystatus: this.state.referenceverifystatus2,
        referenceverifydate: this.state.referenceverifydate2,
        referenceverifynotes: this.state.referenceverifynotes2
      };
      this.referenceverifyapi(data, toastId);
    } else {
      toast.error("Please fill all fields");
    }
  };
  referenceverifyapi(data, toastId) {
    request({
      url: "/agency/employees/referee-form/verify",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        this.MailModalOpen(this.state.referencefor);
        toast.update(toastId, { render: "Updated!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        setTimeout(() => {
          this.DetailsFormModal(this.state.referencefor);
        }, 1000);
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  FinalVerification = (event, errors, values) => {
    if (errors && errors.length === 0) {
      if (this.state.interviewdate) {
        let toastId = "final_verification_form_01";
        toastId = toast.info("Please Wait......", { autoClose: false });
        const data = {
          id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
          final_verify_notes: this.state.final_verify_notes,
          final_verify_date: this.state.final_verify_date,
          final_verify_status: this.state.final_verify_status
        };
        request({
          url: "/agency/employees/recruitment/final-verify",
          method: "POST",
          data: data
        }).then(res => {
          if (res.status === 1) {
            this.componentDidMount();
            toast.update(toastId, { render: "Updated!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.MailModalOpen("finalverify");
          } else if (res.status === 0) {
            toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
          }
        });
      } else {
        toast.error("Please fill all fields");
      }
    }
  };
  remployeeyes = yes => {
    if (yes === "no") {
      this.setState({
        remployeeyes: true
      });
    } else {
      this.setState({
        remployeeyes: false
      });
    }
  };
  disciplinaryactionyes = yes => {
    if (yes === "yes") {
      this.setState({
        disciplinaryactionyes: true
      });
    } else {
      this.setState({
        disciplinaryactionyes: false
      });
    }
  };
  investigationsyes = yes => {
    if (yes === "yes") {
      this.setState({
        investigationsyes: true
      });
    } else {
      this.setState({
        investigationsyes: false
      });
    }
  };
  vulnerablepeopleyes = yes => {
    if (yes === "yes") {
      this.setState({
        vulnerablepeopleyes: true
      });
    } else {
      this.setState({
        vulnerablepeopleyes: false
      });
    }
  };
  criminaloffenceyes = yes => {
    if (yes === "yes") {
      this.setState({
        criminaloffenceyes: true
      });
    } else {
      this.setState({
        criminaloffenceyes: false
      });
    }
  };
  remployeeyes2 = yes => {
    if (yes === "no") {
      this.setState({
        remployeeyes2: true
      });
    } else {
      this.setState({
        remployeeyes2: false
      });
    }
  };
  disciplinaryactionyes2 = yes => {
    if (yes === "yes") {
      this.setState({
        disciplinaryactionyes2: true
      });
    } else {
      this.setState({
        disciplinaryactionyes2: false
      });
    }
  };
  investigationsyes2 = yes => {
    if (yes === "yes") {
      this.setState({
        investigationsyes2: true
      });
    } else {
      this.setState({
        investigationsyes2: false
      });
    }
  };
  vulnerablepeopleyes2 = yes => {
    if (yes === "yes") {
      this.setState({
        vulnerablepeopleyes2: true
      });
    } else {
      this.setState({
        vulnerablepeopleyes2: false
      });
    }
  };
  criminaloffenceyes2 = yes => {
    if (yes === "yes") {
      this.setState({
        criminaloffenceyes2: true
      });
    } else {
      this.setState({
        criminaloffenceyes2: false
      });
    }
  };
  RefereeSubmit = (event, errors, values) => {
    if (errors && errors.length === 0) {
      let toastId = "referee_form_back_site_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = new FormData();
      data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
      data.append("refereefirstname", values.refereefirstname);
      data.append("refereelastname", values.refereelastname);
      data.append("refereeemail", values.refereeemail);
      data.append("refereephone[number]", this.state.refereephone.number);
      data.append("refereephone[code]", this.state.refereephone.code);
      data.append("refereephone[dialcountry]", this.state.refereephone.dialcountry);
      data.append("refereejobtitle", values.refereejobtitle);
      data.append("refereecompany", values.refereecompany);
      data.append("refereecapacity", values.refereecapacity);
      data.append("applicantfirstname", values.applicantfirstname);
      data.append("applicantlastname", values.applicantlastname);
      data.append("applicantemail", values.applicantemail);
      data.append("applicantphone[number]", this.state.applicantphone.number);
      data.append("applicantphone[code]", this.state.applicantphone.code);
      data.append("applicantphone[dialcountry]", this.state.applicantphone.dialcountry);
      data.append("applicantjobtitle", values.applicantjobtitle);
      data.append("applicantcompany", values.applicantcompany);
      data.append("employedfrom", this.state.employedfrom);
      data.append("employedto", this.state.employedto);
      data.append("remployee", values.remployee);
      data.append("remployeenotes", values.remployeenotes || "");
      data.append("followcareplans", values.followcareplans);
      data.append("reliability", values.reliability);
      data.append("character", values.character);
      data.append("dignity", values.dignity);
      data.append("attitude", values.attitude);
      data.append("communication", values.communication);
      data.append("relationships", values.relationships);
      data.append("workunderinitiative", values.workunderinitiative);
      data.append("disciplinaryaction", values.disciplinaryaction);
      data.append("disciplinaryactiondetails", values.disciplinaryactiondetails || "");
      data.append("investigations", values.investigations);
      data.append("investigationsdetails", values.investigationsdetails || "");
      data.append("vulnerablepeople", values.vulnerablepeople);
      data.append("vulnerablepeopledetails", values.vulnerablepeopledetails || "");
      data.append("criminaloffence", values.criminaloffences);
      data.append("criminaloffencedetails", values.criminaloffencedetails || "");
      data.append("additionalcomments", values.additionalcomments);
      data.append("confirm", values.confirm);
      data.append("agree", values.agree);
      data.append("campanystamp", this.state.campanystamp);
      data.append("status", this.state.refereestatus1);
      data.append("referencefor", "reference1");
      this.refereeformupdateapi(data, toastId);
    } else {
      toast.error("Please fill all required fields");
    }
  };
  RefereeSubmit2 = (event, errors, values) => {
    if (errors && errors.length === 0) {
      let toastId = "referee_form_back_site_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = new FormData();
      data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
      data.append("refereefirstname", values.refereefirstname2);
      data.append("refereelastname", values.refereelastname2);
      data.append("refereeemail", values.refereeemail2);
      data.append("refereephone[number]", this.state.refereephone2.number);
      data.append("refereephone[code]", this.state.refereephone2.code);
      data.append("refereephone[dialcountry]", this.state.refereephone2.dialcountry);
      data.append("refereejobtitle", values.refereejobtitle2);
      data.append("refereecompany", values.refereecompany2);
      data.append("refereecapacity", values.refereecapacity2);
      data.append("applicantfirstname", values.applicantfirstname2);
      data.append("applicantlastname", values.applicantlastname2);
      data.append("applicantemail", values.applicantemail2);
      data.append("applicantphone[number]", this.state.applicantphone2.number);
      data.append("applicantphone[code]", this.state.applicantphone2.code);
      data.append("applicantphone[dialcountry]", this.state.applicantphone2.dialcountry);
      data.append("applicantjobtitle", values.applicantjobtitle2);
      data.append("applicantcompany", values.applicantcompany2);
      data.append("employedfrom", this.state.employedfrom2);
      data.append("employedto", this.state.employedto2);
      data.append("remployee", values.remployee2);
      data.append("remployeenotes", values.remployeenotes2 || "");
      data.append("followcareplans", values.followcareplans2);
      data.append("reliability", values.reliability2);
      data.append("character", values.character2);
      data.append("dignity", values.dignity2);
      data.append("attitude", values.attitude2);
      data.append("communication", values.communication2);
      data.append("relationships", values.relationships2);
      data.append("workunderinitiative", values.workunderinitiative2);
      data.append("disciplinaryaction", values.disciplinaryaction2);
      data.append("disciplinaryactiondetails", values.disciplinaryactiondetails2 || "");
      data.append("investigations", values.investigations2);
      data.append("investigationsdetails", values.investigationsdetails2 || "");
      data.append("vulnerablepeople", values.vulnerablepeople2);
      data.append("vulnerablepeopledetails", values.vulnerablepeopledetails2 || "");
      data.append("criminaloffence", values.criminaloffence2);
      data.append("criminaloffencedetails", values.criminaloffencedetails2 || "");
      data.append("additionalcomments", values.additionalcomments2);
      data.append("confirm", values.confirm2);
      data.append("agree", values.agree2);
      data.append("campanystamp", this.state.campanystamp2);
      data.append("status", this.state.refereestatus2);
      data.append("referencefor", "reference2");
      this.refereeformupdateapi(data, toastId);
    } else {
      toast.error("Please fill all required fields");
    }
  };
  refereeformupdateapi(data, toastId) {
    request({
      url: "/agency/employees/referee-form/update",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        toast.update(toastId, { render: "Updated!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      mobileno: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      kinmobileno: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler2 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror2: false
      });
    } else {
      this.setState({
        phoneerror2: true
      });
    }
    this.setState({
      firstrefphone: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler3 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror3: false
      });
    } else {
      this.setState({
        phoneerror3: true
      });
    }
    this.setState({
      secondrefphone: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler4 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror4: false
      });
    } else {
      this.setState({
        phoneerror4: true
      });
    }
    this.setState({
      refereephone: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler5 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror5: false
      });
    } else {
      this.setState({
        phoneerror5: true
      });
    }
    this.setState({
      refereephone2: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler6 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror6: false
      });
    } else {
      this.setState({
        phoneerror6: true
      });
    }
    this.setState({
      applicantphone: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  handler7 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror7: false
      });
    } else {
      this.setState({
        phoneerror7: true
      });
    }
    this.setState({
      applicantphone2: {
        number: value,
        code: countryData.dialCode,
        dialcountry: countryData.iso2
      }
    });
  };
  uploadDocs = (evt, place) => {
    evt.persist();
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.files_api(file_data || null, place);
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  deleteDocs = (filepath, filefor) => {
    let toastId = "employee_files_delete_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/agency/employees/apply-online-form/delete-files",
      method: "POST",
      data: { filefor, filepath, id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID }
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  files_api(uploadfiles, filefor) {
    let toastId = "employee_files_save_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = new FormData();
    data.append("id", this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID);
    data.append("filefor", filefor);
    data.append("uploadfiles", uploadfiles);
    request({
      url: "/agency/employees/apply-online-form/save-files",
      method: "POST",
      data
    }).then(res => {
      console.log("res ", res);
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  }
  FileDownloader = filepath => {
    let toastId = "downloadfiles156";
    toastId = toast.info("Please Wait......", { autoClose: false });
    if (filepath) {
      const file = NodeURL + "/" + filepath;
      FileSaver.saveAs(file);
      toast.update(toastId, { render: "File Downloaded!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
    } else {
      toast.update(toastId, { render: "Unable to Download", type: toast.TYPE.ERROR, autoClose: 2500 });
    }
  };
  DownloadRefereeForm = referencefor => {
    let toastId = "referee_form_download_022";
    client.defaults.responseType = "blob";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/agency/employees/referee-form/download",
      method: "POST",
      data: {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        referencefor
      }
    })
      .then(res => {
        if (res) {
          const file = new Blob([res], { type: "application/pdf" });
          FileSaver(file, "refereeform.pdf");
          toast.update(toastId, { render: "Downloaded!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
        client.defaults.responseType = "json";
      })
      .catch(error => {
        client.defaults.responseType = "json";
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
  };
  refereeCollapse = value => {
    if (value === "referee1") this.setState({ refereecollapse1: !this.state.refereecollapse1 });
    else if (value === "referee2") this.setState({ refereecollapse2: !this.state.refereecollapse2 });
  };
  MailModalOpen = value => {
    if (!this.state.mailmodal) {
      request({
        url: "/agency/emailtemplate/selecttypedata",
        method: "POST",
        data: {
          type:
            value === "refrence1"
              ? "sendrefereeform"
              : value === "refrence2"
                ? "sendrefereeform"
                : value === "interviewcall"
                  ? "interviewcall"
                  : value === "postinterviewcall"
                    ? "interviewstatus"
                    : value === "verificationstatus"
                      ? "verificationstatus"
                      : value === "refverify1"
                        ? "refereeverification"
                        : value === "refverify2"
                          ? "refereeverification"
                          : value === "finalverify"
                            ? "candidatewelcome"
                            : ""
        }
      }).then(res => {
        if (res.status === 1) {
          const data = res.response.result;
          this.setState({ emailtemplatelist: data });
          const typeid = data.filter(list => list.default_mail).map(list => list._id)[0];
          this.Templatelist(typeid, value);
          this.setState({ typeid });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    }
    this.setState({
      mailmodal: !this.state.mailmodal,
      referencefor: value,
      refe_email: value === "refrence1" ? this.state.refemailsend1 : value === "refrence2" ? this.state.refemailsend2 : value === "refverify1" ? this.state.refemailsend1 : value === "refverify2" ? this.state.refemailsend2 : ""
    });
  };
  ComplianceModalOpen = value => {
    if (!this.state.compliancemodal) {
      request({
        url: "/agency/emailtemplate/selecttypedata",
        method: "POST",
        data: { type: "compliancemail" }
      }).then(res => {
        if (res.status === 1) {
          const data = res.response.result;
          this.setState({ emailtemplatelist: data });
          const typeid = data.filter(list => list.default_mail).map(list => list._id)[0];
          this.Templatelist(typeid, (value = "compliancemail"));
          this.setState({ typeid, compliancemodal: !this.state.compliancemodal });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    } else {
      this.setState({ compliancemodal: !this.state.compliancemodal });
    }
  };
  ApplicationFormApproval = value => {
    if (!this.state.applicationmodal) {
      request({
        url: "/agency/emailtemplate/selecttypedata",
        method: "POST",
        data: { type: "applicationapproval" }
      }).then(res => {
        if (res.status === 1) {
          const data = res.response.result;
          this.setState({ emailtemplatelist: data });
          const typeid = data.filter(list => list.default_mail).map(list => list._id)[0];
          this.Templatelist(typeid, (value = "applicationapproval"));
          this.setState({ typeid, applicationmodal: !this.state.applicationmodal });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    } else {
      this.setState({ applicationmodal: !this.state.applicationmodal });
    }
  };
  onChangeSelect = e => {
    if (e) {
      this.Templatelist(e.target.value, this.state.referencefor);
      this.setState({ [e.target.name]: e.target.value });
    }
  };
  Templatelist(value, valuefor) {
    const { refereename1, refereename2, employee_data, referencefor, referenceverifystatus, referenceverifystatus2, referenceverifynotes, referenceverifynotes2 } = this.state;
    let data;
    if (referencefor === "refrence1" || referencefor === "refrence2") {
      data = {
        page: "reference",
        id: value,
        referencefor: valuefor === "refrence1" ? "reference1" : valuefor === "refrence2" ? "reference2" : "",
        refereename: valuefor === "refrence1" ? refereename1 : valuefor === "refrence2" ? refereename2 : "",
        employee: employee_data.name,
        employee_username: employee_data.username
      };
    } else if (referencefor === "interviewcall") {
      data = {
        page: "interviewcall",
        id: value,
        employee_data: employee_data.name,
        employee: employee_data.name,
        interviewdate: this.state.interviewdate,
        interviewtime: this.state.interviewtime,
        videocallurl: this.state.videocallurl,
        interviewnotes: this.state.interviewnotes
      };
    } else if (referencefor === "postinterviewcall") {
      data = {
        page: "postinterviewcall",
        id: value,
        employee_data: employee_data.name,
        employee: employee_data.name,
        intervieweddate: this.state.intervieweddate,
        interviewedstatus: this.state.interviewedstatus,
        interviewednotes: this.state.interviewednotes
      };
    } else if (referencefor === "verificationstatus") {
      data = {
        page: "verificationstatus",
        id: value,
        employee_data: employee_data.name,
        employee: employee_data.name,
        skillcheckstatus: this.state.skillcheckstatus,
        dbscheckstatus: this.state.dbscheckstatus,
        healthcheckstatus: this.state.healthcheckstatus
      };
    } else if (referencefor === "refverify1" || referencefor === "refverify2") {
      data = {
        page: "refereeverification",
        id: value,
        employee_data: employee_data.name,
        employee: employee_data.name,
        refereenotes: valuefor === "refverify1" ? referenceverifynotes : valuefor === "refverify2" ? referenceverifynotes2 : "",
        refereestatus: valuefor === "refverify1" ? referenceverifystatus : valuefor === "refverify2" ? referenceverifystatus2 : "",
        refereenamesfor: valuefor === "refverify1" ? refereename1 : valuefor === "refverify2" ? refereename2 : ""
      };
    } else if (referencefor === "finalverify") {
      data = {
        page: "candidatewelcome",
        id: value,
        employee_data: employee_data.name,
        employee: employee_data.name,
        empstatus: this.state.final_verify_status,
        empnote: this.state.final_verify_notes
      };
    } else {
      data = {
        page: "compliancemail",
        id: value,
        employee_data: employee_data.name,
        employee: employee_data.name
      };
    }
    request({
      url: "/agency/emailtemplate/getemailcontent",
      method: "POST",
      data
    }).then(res => {
      if (res.status === 1) {
        const data = res.response.result;
        this.setState({
          email_content: data.html,
          email_subject: data.subject
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  SendRefereeForm = () => {
    const { referencefor, refereename1, refereename2, employee_data, refe_email, email_content, email_subject } = this.state;
    let toastId = "referee_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = {
      id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
      refe_email: refe_email,
      employee_data: employee_data,
      email_content: email_content,
      email_subject: email_subject,
      refereename: referencefor === "refrence1" ? refereename1 : referencefor === "refrence2" ? refereename2 : "",
      referencefor
    };
    request({
      url: "/agency/employees/send-referee-form",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: "Form Sent!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
        this.MailModalOpen();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  MailInterviewCall = () => {
    const { employee_email, employee_data, email_content, email_subject } = this.state;
    let toastId = "referee_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = {
      id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
      employee_email: employee_email,
      employee_data: employee_data,
      email_content: email_content,
      email_subject: email_subject
    };
    request({
      url: "/agency/employees/apply-online-form/interviewcall",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
        this.MailModalOpen();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  PostInterview = () => {
    const { employee_email, employee_data, email_content, email_subject, interviewedstatus } = this.state;
    if (this.state.intervieweddate) {
      let toastId = "interview_status_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        interviewedstatus: interviewedstatus,
        employee_email: employee_email,
        employee_data: employee_data,
        email_content: email_content,
        email_subject: email_subject
      };
      request({
        url: "/agency/employees/apply-online-form/postinterviewcall",
        method: "POST",
        data: data
      }).then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          this.MailModalOpen();
          toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      });
    } else {
      toast.error("Please select interviewed date");
    }
  };
  VerificationStatus = () => {
    const { employee_email, employee_data, email_content, email_subject, referencefor } = this.state;
    if (this.state.intervieweddate) {
      let toastId = "interview_status_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      const data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        employee_email: employee_email,
        employee_data: employee_data,
        email_content: email_content,
        email_subject: email_subject,
        referencefor
      };
      request({
        url: "/agency/employees/apply-online-form/verificationstatus",
        method: "POST",
        data: data
      }).then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          this.MailModalOpen();
          toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      });
    } else {
      toast.error("Please select interviewed date");
    }
  };
  ReferenceStatus = () => {
    const { employee_email, employee_data, email_content, email_subject, referencefor } = this.state;
    let toastId = "reference_interview_status_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = {
      id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
      employee_email: employee_email,
      employee_data: employee_data,
      email_content: email_content,
      email_subject: email_subject,
      referencefor: referencefor === "refverify1" ? "reference1" : referencefor === "refverify2" ? "reference2" : ""
    };
    request({
      url: "/agency/employees/apply-online-form/referenceverificationstatus",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        this.MailModalOpen();
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  FinalStatus = () => {
    const { employee_email, employee_data, email_content, email_subject, final_verify_status } = this.state;
    let toastId = "reference_interview_status_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = {
      id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
      employee_email: employee_email,
      employee_data: employee_data,
      email_content: email_content,
      email_subject: email_subject,
      final_verify_status: final_verify_status
    };
    request({
      url: "/agency/employees/apply-online-form/finalstatus",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        this.MailModalOpen();
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  ComplianceForm = () => {
    const { edit_form, employee_email, employee_data, email_content, email_subject } = this.state;
    let toastId = "compliance_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = {
      employee_email: employee_email,
      employee_data: employee_data,
      email_content: email_content,
      email_subject: email_subject,
      edit_form: edit_form
    };
    request({
      url: "/agency/employees/compliance-mail",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
        this.ComplianceModalOpen();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  ApplicationForm = () => {
    const { applicationcheckstatus, employee_email, employee_data, email_content, email_subject } = this.state;
    let toastId = "applicart124_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const data = {
      employee_email: employee_email,
      employee_data: employee_data,
      email_content: email_content,
      email_subject: email_subject,
      application_status: applicationcheckstatus
    };
    request({
      url: "/agency/employees/application-approval-mail",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
        this.ApplicationFormApproval();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  DetailsFormModal = referencefor => {
    this.setState({ detailsmodal: !this.state.detailsmodal, referencefor });
  };
  CheckFormModal = () => {
    this.setState({ checkmodal: !this.state.checkmodal });
  };
  InterviewDetailsForm = () => {
    const { referencefor, interviewdate, interviewtime, videocallurl, interviewnotes, intervieweddate, interviewedstatus, interviewednotes } = this.state;
    let data;
    if (referencefor === "interviewcall") {
      data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        interviewdate: interviewdate,
        interviewtime: interviewtime,
        videocallurl: videocallurl,
        interviewnotes: interviewnotes,
        pagefor: referencefor
      };
    } else if (referencefor === "postinterviewcall") {
      data = {
        id: this.props.location.state.rowid ? this.props.location.state.rowid : this.state.Emp_ID,
        intervieweddate: intervieweddate,
        interviewednotes: interviewednotes,
        interviewedstatus: interviewedstatus,
        pagefor: referencefor
      };
    }
    let toastId = "save_applicart124_form_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/agency/employees/interviewdetails-save",
      method: "POST",
      data: data
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
        this.MailModalOpen(referencefor);
        setTimeout(() => {
          this.DetailsFormModal(referencefor);
        }, 1000);
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  viewpage = () => {
    let URL = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      const val = this.props && this.props.location && this.props.location.pathname;
      switch (val) {
        case "/agency/recruitmentform/candidates":
          URL = "/agency/recruitment/form/candidates/view";
          break;
        case "/agency/recruitmentform/application":
          URL = "/agency/recruitment/form/application/view";
          break;
        case "/agency/recruitmentform/interview":
          URL = "/agency/recruitment/form/interview/view";
          break;
        case "/agency/recruitmentform/files":
          URL = "/agency/recruitment/form/files/view";
          break;
        case "/agency/recruitmentform/references":
          URL = "/agency/recruitment/form/references/view";
          break;
        case "/agency/recruitmentform/summary":
          URL = "/agency/recruitment/form/summary/view";
          break;
        default:
          break;
      }
      return this.props.history.push({
        pathname: URL,
        state: { rowid: this.props.location.state.rowid }
      });
    }
  };
  render() {
    let Heading = "";
    if (this.props && this.props.location && this.props.location.pathname) {
      const val = this.props && this.props.location && this.props.location.pathname;
      switch (val) {
        case "/agency/recruitmentform/candidates":
          Heading = "Candidates - View";
          break;
        case "/agency/recruitmentform/application":
          Heading = "Applicant - View";
          break;
        case "/agency/recruitmentform/interview":
          Heading = "Interviews - View";
          break;
        case "/agency/recruitmentform/files":
          Heading = "File Manager - View";
          break;
        case "/agency/recruitmentform/references":
          Heading = "References - View";
          break;
        case "/agency/recruitmentform/summary":
          Heading = "Summary - View";
          break;
        default:
          break;
      }
    }
    const { jobtypelist, path_name, form_data, typeid, emailtemplatelist, email_subject, email_content, refe_email, employee_email, referencefor, form_status, edit_form } = this.state;
    const {
      skillcheck: form_skillcheck,
      healthcheck: form_healthcheck,
      dbscheck: form_dbscheck,
      interview: form_interview,
      personaldetails: form_personaldetails,
      drivingdetails: form_driving,
      nextofkindetails: form_kin,
      gpdetails: form_gp,
      postapplyfor,
      postapplydetails,
      // availablestartdate,
      UploadIDPhoto,
      employmenteligibility: { visapermit, visapermittype } = {},
      UploadCV,
      langauages: { englishspoken, englishwritten, otherlanguage, IELTStest } = {},
      employmenthistory: employmenthistorylist,
      education: educationlist,
      reference1,
      reference1: { referee: form_reference1 } = {},
      reference2,
      reference2: { referee: form_reference2 } = {},
      mandatory_training,
      movinghandling,
      basiclifesupport,
      healthsafety,
      firesafety,
      firstaid,
      infectioncontrol,
      foodsafety,
      medicationadmin,
      safeguardingvulnerable,
      trainingdates,
      healthcare_training,
      diplomal3,
      diplomal2,
      personalsafetymental,
      intermediatelife,
      advancedlife,
      complaintshandling,
      handlingviolence,
      doolsmental,
      coshh,
      dataprotection,
      equalityinclusion,
      loneworkertraining,
      resuscitation,
      interpretation,
      trainindates2,
      healthdeclaration,
      medicalhistory: { medicalillness, medicalillnesscaused, treatment, needanyadjustments, needanyadjustmentsnotes } = {},
      tuberculosis: {
        livedUK,
        livedUKnotes,
        BCGvaccination,
        //  BCGvaccinationdate,
        cough,
        weightloss,
        fever,
        TB
      } = {},
      chickenpoxorshingles: {
        chickenpox
        // chickenpoxdate
      } = {},
      immunisationhistory: { triplevaccination, polio, tetanus, hepatitisB, proneProcedures } = {},
      declaration: { occupationalHealthServices, criminaloffence, warningcriminaloffence, DBSdetails } = {},
      righttowork: { passport } = {},
      worktimedirectives: { workhours } = {},
      uploadcertificate: certificatedocs,
      uploadpassport: passportdocs,
      uploadaddress: addressdocs,
      uploaddbs: dbsdocs,
      uploadothers: otherdocs,
      final_verify_status,
      final_verify_date,
      final_verify_notes
    } = form_data ? form_data : {};
    const { abletowork: form_aboutwork, firstchoice, secondchoice, thirdchoice, shortnotice, reduceworkflexibility, workstate } = form_data && form_data.aboutyourwork ? form_data.aboutyourwork : {};
    const { date: skillcheckdate, notes: skillchecknotes, status: skillcheckstatus } = form_skillcheck ? form_skillcheck : {};
    const { date: dbscheckdate, notes: dbschecknotes, status: dbscheckstatus } = form_dbscheck ? form_dbscheck : {};
    const { date: healthcheckdate, notes: healthchecknotes, status: healthcheckstatus } = form_healthcheck ? form_healthcheck : {};
    const { interviewdate, interviewtime, interviewnotes, intervieweddate, interviewednotes, status: interviewedstatus, videocallurl } = form_interview ? form_interview : {};
    const {
      title,
      firstname,
      lastname,
      homephone,
      email,
      // dateofbirth,
      insuranceno,
      nationality,
      gender,
      religion,
      race,
      sexualorientation,
      address: { streetAddress, addressline2, city, state, zip, country } = {},
      mobileno
    } = form_personaldetails ? form_personaldetails : {};
    const { vehicledocumentsvalid, drivinglicence, traveldetails, drivinglicenceno, caraccess, bannedfromdriving, carinsurance } = form_driving ? form_driving : {};
    const { work_mornings, work_evenings, work_afternoons, work_occasional, work_fulltime, work_parttime, work_nights, work_weekends, work_anytime } = form_aboutwork ? form_aboutwork : {};
    const { kinprefix, kinfirstname, kinlastname, kinrelationship, kinhomephone, kinmobileno, kinemail, Address: { kinstreetAddress, kinaddressline2, kincity, kinstate, kinzip, kincountry } = {} } = form_kin ? form_kin : {};
    const { firstrefrelationship, firstreffirstname, firstreflastname, firstrefphone, firstrefemail, firstrefconfirmemail, Address: { firstrefstreetAddress, firstrefcity, firstrefstate, firstrefzip } = {} } = reference1 ? reference1 : {};
    const { secondrefrelationship, secondreffirstname, secondreflastname, secondrefphone, secondrefemail, secondrefconfirmemail, Address: { secondrefstreetAddress, secondrefcity, secondrefstate, secondrefzip } = {} } = reference2
      ? reference2
      : {};
    const { sufferedlongtermillness, leaveforneckinjury, neckinjuries, sixweeksillness, communicabledisease, medicalattention, healthdeclarationdetails, registereddisabled, absentfromwork, statereason } = healthdeclaration
      ? healthdeclaration
      : {};
    const { GPname, gpaddress: { GPstreetAddress, GPcity, GPstate, GPzip, contactDoctor, GPhealthdetails } = {} } = form_gp ? form_gp : {};
    const {
      refereefirstname,
      refereelastname,
      refereeemail,
      refereephone,
      refereejobtitle,
      refereecompany,
      refereecapacity,
      applicantfirstname,
      applicantlastname,
      applicantemail,
      applicantphone,
      applicantjobtitle,
      applicantcompany,
      // employedfrom,
      // employedto,
      remployee,
      remployeenotes,
      followcareplans,
      reliability,
      character,
      dignity,
      attitude,
      communication,
      relationships,
      workunderinitiative,
      disciplinaryaction,
      disciplinaryactiondetails,
      investigations,
      investigationsdetails,
      vulnerablepeople,
      vulnerablepeopledetails,
      criminaloffences,
      criminaloffencedetails,
      additionalcomments,
      campanystamp,
      referenceverifydate,
      referenceverifynotes
    } = form_reference1 ? form_reference1 : {};
    const {
      refereefirstname: refereefirstname2,
      refereelastname: refereelastname2,
      refereeemail: refereeemail2,
      refereephone: refereephone2,
      refereejobtitle: refereejobtitle2,
      refereecompany: refereecompany2,
      refereecapacity: refereecapacity2,
      applicantfirstname: applicantfirstname2,
      applicantlastname: applicantlastname2,
      applicantemail: applicantemail2,
      applicantphone: applicantphone2,
      applicantjobtitle: applicantjobtitle2,
      applicantcompany: applicantcompany2,
      // employedfrom: employedfrom2,
      // employedto:  employedto2,
      remployee: remployee2,
      remployeenotes: remployeenotes2,
      followcareplans: followcareplans2,
      reliability: reliability2,
      character: character2,
      dignity: dignity2,
      attitude: attitude2,
      communication: communication2,
      relationships: relationships2,
      workunderinitiative: workunderinitiative2,
      disciplinaryaction: disciplinaryaction2,
      disciplinaryactiondetails: disciplinaryactiondetails2,
      investigations: investigations2,
      investigationsdetails: investigationsdetails2,
      vulnerablepeople: vulnerablepeople2,
      vulnerablepeopledetails: vulnerablepeopledetails2,
      criminaloffences: criminaloffence2,
      criminaloffencedetails: criminaloffencedetails2,
      additionalcomments: additionalcomments2,
      campanystamp: campanystamp2,
      referenceverifydate: referenceverifydate2,
      referenceverifynotes: referenceverifynotes2
    } = form_reference2 ? form_reference2 : {};
    const reference_details1 = form_reference1 ? form_reference1 : {};
    const reference_details2 = form_reference2 ? form_reference2 : {};

    return (
      <div className="animated fadeIn online-forms">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <ToastContainer position="top-right" autoClose={2500} />
              <CardHeader>
                <strong>{Heading || "Recruitment - Online Application Form"}</strong>
                <div className="card-actions" onClick={this.viewpage}>
                  <button>
                    <i className="fa fa-eye" /> View
                    {/* <small className="text-muted" />*/}
                  </button>
                </div>
              </CardHeader>
              <CardBody className="p-0">
                <Row className="d-flex justify-content-end mt-1">
                  <Button className="btn-complance" onClick={this.ComplianceModalOpen}>
                    Compliance
                  </Button>
                  {this.state.activeTab === "5" && form_status < 2 ? (
                    <Button color="success" className="mr-3 approve-app" onClick={this.ApplicationFormApproval}>
                      Approve Application Form
                    </Button>
                  ) : null}
                </Row>
                <Nav tabs>
                  {path_name === Canditates_URL || path_name === Application_URL || path_name === Files_URL ? (
                    <>
                      {path_name !== Files_URL ? (
                        <>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.activeTab === "1"
                              })}
                              onClick={() => {
                                this.toggle("1");
                              }}
                            >
                              Your Personal Details
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.activeTab === "2"
                              })}
                              onClick={() => {
                                this.toggle("2");
                              }}
                            >
                              Employment & Education
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.activeTab === "3"
                              })}
                              onClick={() => {
                                this.toggle("3");
                              }}
                            >
                              Skills & Training
                            </NavLink>
                          </NavItem>
                        </>
                      ) : null}
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "4"
                          })}
                          onClick={() => {
                            this.toggle("4");
                          }}
                        >
                          Documents
                        </NavLink>
                      </NavItem>
                      {path_name !== Files_URL ? (
                        <>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.activeTab === "5"
                              })}
                              onClick={() => {
                                this.toggle("5");
                              }}
                            >
                              Declaration & Terms
                            </NavLink>
                          </NavItem>
                        </>
                      ) : null}
                    </>
                  ) : null}
                  {path_name === Canditates_URL || path_name === Interview_URL ? (
                    <>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "6"
                          })}
                          onClick={() => {
                            this.toggle("6");
                          }}
                        >
                          Interview & Verification
                        </NavLink>
                      </NavItem>
                    </>
                  ) : null}
                  {path_name === Canditates_URL || path_name === Referee_URL ? (
                    <>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "7"
                          })}
                          onClick={() => {
                            this.toggle("7");
                          }}
                        >
                          References
                        </NavLink>
                      </NavItem>
                    </>
                  ) : null}
                  {path_name === Canditates_URL || path_name === Summary_URL ? (
                    <>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "8"
                          })}
                          onClick={() => {
                            this.toggle("8");
                          }}
                        >
                          Summary
                        </NavLink>
                      </NavItem>
                    </>
                  ) : null}
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                  {path_name === Canditates_URL || path_name === Application_URL || path_name === Files_URL ? (
                    <>
                      {path_name !== Files_URL ? (
                        <>
                          <TabPane tabId="1">
                            <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="4">
                                    <AvField className="form-control fname" type="select" name="postapplyfor" label="Post Applying for:" placeholder="Select Post Apply For" onChange={this.onChange} value={postapplyfor} required>
                                      <option>Please Select</option>
                                      {jobtypelist &&
                                        jobtypelist.length > 0 &&
                                        jobtypelist.map((list, i) => (
                                          <option key={i} value={list._id}>
                                            {list.name}
                                          </option>
                                        ))}
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="select" name="postapplydetails" label="How did you hear about this us?" onChange={this.onChange} value={postapplydetails} required>
                                      <option>Please Select</option>
                                      <option value="internetsearch">Internet Search</option>
                                      <option value="jobcentre">Job Centre</option>
                                      <option value="friend">Friend</option>
                                      <option value="newspaper">Newspaper</option>
                                      <option value="other">Other</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <label>Date available to start</label>
                                    <SingleDatePicker
                                      // className={this.state.finaldateError ? "error-color" : null}
                                      date={moment(this.state.availablestartdate)}
                                      onDateChange={availablestartdate => this.setState({ availablestartdate })}
                                      focused={this.state.focused1}
                                      onFocusChange={({ focused: focused1 }) => this.setState({ focused1 })}
                                      id="availablestartdate"
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={() => false}
                                    />
                                  </Col>
                                </div>
                              </Row>

                              {/* <strong>Name</strong> */}
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Personal Details 1</p>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="select" name="title" label="Title" placeholder="Select Title" onChange={this.onChange} value={title} required>
                                      <option>Please Select</option>
                                      <option value="mr">Mr.</option>
                                      <option value="mrs">Mrs.</option>
                                      <option value="miss">Miss.</option>
                                      <option value="ms">Ms.</option>
                                      <option value="dr">Dr.</option>
                                      <option value="prof">Prof.</option>
                                      <option value="rev">Rev.</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="First Name" name="firstname" placeholder="Enter First Name" onChange={this.onChange} value={firstname} required />
                                  </Col>
                                  {/* <Col xs="12" md="4">
                          <AvField
                            type="text"
                            label="Middle Name"
                            name="middlename"
                            placeholder="Enter Middle Name"
                            onChange={this.onChange}
                            value={middlename}
                            required
                          />
                        </Col>*/}
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Last Name" name="lastname" placeholder="Enter Last Name" onChange={this.onChange} value={lastname} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="number" label="Home Phone" name="homephone" placeholder="Enter Phone number" onChange={this.onChange} value={homephone} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label className={this.state.phoneerror ? "error-color" : null}>Mobile No</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={mobileno && mobileno.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler}
                                      value={mobileno && mobileno.number}
                                      required
                                    />
                                    {this.state.phoneerror ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" label="Mobile No" name="mobileno" placeholder="Enter Mobile Number" onChange={this.onChange} value={this.state.mobileno} required /> */}
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="email" label="Email" name="email" placeholder="Enter Email" onChange={this.onChange} value={email} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <label>Date Of Birth</label>
                                    <SingleDatePicker
                                      required={true}
                                      id="dateofbirth"
                                      date={moment(this.state.dateofbirth)}
                                      onDateChange={dateofbirth => this.setState({ dateofbirth })}
                                      focused={this.state.focused209}
                                      onFocusChange={({ focused: focused209 }) => this.setState({ focused209 })}
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={day => day.isAfter(moment(new Date()))}
                                    />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="number" label="National Insurance No" name="insuranceno" placeholder="Enter Insurance No" onChange={this.onChange} value={insuranceno} required />
                                  </Col>
                                </div>
                              </Row>

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Address</p>
                                    <strong className="an-heading">Your Current Address</strong>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Street Address" name="streetAddress" placeholder="Enter Street Address" onChange={this.onChange} value={streetAddress} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Address Line 2" name="addressline2" placeholder="Enter Address Line 2" onChange={this.onChange} value={addressline2} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="City" name="city" placeholder="Enter City" onChange={this.onChange} value={city} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="text" label="County / State / Region" name="state" placeholder="Enter State" onChange={this.onChange} value={state} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="ZIP / Postal Code" name="zip" placeholder="Enter ZIP / Postal Code" onChange={this.onChange} value={zip} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Country" name="country" placeholder="Enter Country" onChange={this.onChange} value={country} required />
                                  </Col>
                                </div>
                              </Row>
                              <br />

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Personal Details 2</p>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="select" name="nationality" label="Nationality" placeholder="Select Nationality" onChange={this.onChange} value={nationality} required>
                                      <option>Please Select</option>
                                      <option value="british">British</option>
                                      <option value="eucitizen">EU Citizen</option>
                                      <option value="other">Other</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="select" name="gender" label="Gender" placeholder="Select Gender" onChange={this.onChange} value={gender} required>
                                      <option>Please Select</option>
                                      <option value="female">Female</option>
                                      <option value="male">Male</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="select" name="religion" label="Religion" placeholder="Select Religion.." onChange={this.onChange} value={religion} required>
                                      <option>Please Select</option>
                                      <option value="bhudist">Bhudist</option>
                                      <option value="christian">Christian</option>
                                      <option value="jewish">Jewish</option>
                                      <option value="hindu">Hindu</option>
                                      <option value="muslim">Muslim</option>
                                      <option value="sikh">Sikh</option>
                                      <option value="other">Other</option>
                                    </AvField>
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="select" name="race" label="Race/Ethnicity" placeholder="Select Race/Ethnicity.." onChange={this.onChange} value={race} required>
                                      <option>Please Select</option>
                                      <option value="whitebritish">White British</option>
                                      <option value="whiteother">White(Other)</option>
                                      <option value="whiteirish">White Irish</option>
                                      <option value="mixesrace">Mixes Race</option>
                                      <option value="indian">Indian</option>
                                      <option value="pakistani">Pakistani</option>
                                      <option value="bangladeshi">Bangladeshi</option>
                                      <option value="otherasian">Other Asian(non-Chinese)</option>
                                      <option value="blackcaribbean">Black Caribbean</option>
                                      <option value="blackafrican">Black African</option>
                                      <option value="blackothers">Black(others)</option>
                                      <option value="chinese">Chinese</option>
                                      <option value="other">Other</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="select" name="sexualorientation" label="Sexual Orientation" placeholder="Select Sexual Orientation.." onChange={this.onChange} value={sexualorientation} required>
                                      <option>Please Select</option>
                                      <option value="bisexual">Bisexual</option>
                                      <option value="gayman">Gay Man</option>
                                      <option value="gaywoman">Gay Woman/Lesbian</option>
                                      <option value="straight">Straight/Heterosexual</option>
                                      <option value="prefer">Prefer not to answer</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <Label for="exampleCustomFileBrowser">Upload ID Photo</Label>
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser"
                                      name="UploadIDPhoto"
                                      onChange={this.fileChangedHandler}
                                      label={this.state.UploadIDName ? this.state.UploadIDName : "Upload  Image"}
                                      encType="multipart/form-data"
                                    />
                                    <p className="help-block">
                                      <small>
                                        <i>This Photo will be used on your ID badge, make sure your face is clear</i>
                                      </small>
                                    </p>
                                  </Col>
                                  <Col xs="6" md="3">
                                    <img width="180px" height="180px" src={NodeURL + "/" + UploadIDPhoto} alt="Profile" />
                                  </Col>
                                  {UploadIDPhoto && (
                                    <Col xs="12" md="12">
                                      <Button color="success" className="mt-2" onClick={() => this.FileDownloader(UploadIDPhoto, "IDphoto.jpg")}>
                                        Download
                                      </Button>
                                    </Col>
                                  )}
                                </div>
                              </Row>

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Employment Eligibility</p>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <AvRadioGroup inline name="visapermit" label="What visa/permit/status do you currently hold?:" value={visapermit} required>
                                      <AvRadio label="Working Holiday" value="workingholiday" onClick={this.otheremp} />
                                      <AvRadio label="Work Permit" value="workpermit" onClick={this.otheremp} />
                                      <AvRadio label="Leave to Remain" value="leavetoremain" onClick={this.otheremp} />
                                      <AvRadio label="Other" value="other" onClick={() => this.otheremp("other")} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.otheremp ? (
                                    <Col xs="12" md="4">
                                      <AvField type="text" label="Please state what visa/permit you hold:" name="visapermittype" placeholder="Enter visa/permit.." onChange={this.onChange} value={visapermittype} required />
                                    </Col>
                                  ) : null}
                                </div>
                              </Row>

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Driving Details</p>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="drivinglicence" label="Do you have full Driving Licence that allows you to drive in the UK?" value={drivinglicence} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.drivelicenceyes("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.drivelicenceyes} />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="select" name="traveldetails" label="How would you travel to work if assigned?" value={traveldetails} required>
                                      <option>Please Select</option>
                                      <option value="drive">Drive</option>
                                      <option value="public">Public Transport </option>
                                      <option value="willgetalist">Will get a lift</option>
                                      <option value="bicycle">Bicycle</option>
                                      <option value="other">Other</option>
                                    </AvField>
                                  </Col>
                                </div>
                              </Row>
                              {this.state.drivelicenceyes ? (
                                <Row>
                                  <div className="cus-design edited-section">
                                    <Col xs="12" md="4">
                                      <AvField type="text" label="Driving Licence No:" name="drivinglicenceno" placeholder="Enter Driving Licence No" onChange={this.onChange} value={drivinglicenceno || ""} required />
                                    </Col>
                                    <Col xs="12" md="4">
                                      <AvRadioGroup inline name="caraccess" label="Do you have access to a car that you can use for work?" value={caraccess} required>
                                        <AvRadio label="Yes" value="yes" onClick={() => this.caraccessyes("yes")} />
                                        <AvRadio label="No" value="no" onClick={this.caraccessyes} />
                                      </AvRadioGroup>
                                    </Col>
                                    <Col xs="12" md="4">
                                      <AvRadioGroup inline name="bannedfromdriving" label="Have you been banned from driving. or do you have any current endorsements on you licence?" value={bannedfromdriving} required>
                                        <AvRadio label="Yes" value="yes" />
                                        <AvRadio label="No" value="no" />
                                      </AvRadioGroup>
                                    </Col>
                                  </div>
                                </Row>
                              ) : null}
                              {this.state.caraccessyes ? (
                                <Row>
                                  <div className="cus-design edited-section">
                                    <Col xs="12" md="4">
                                      <AvRadioGroup
                                        inline
                                        name="carinsurance"
                                        label="Does you car insurance include Class 1 business insurance? (in order to use you vehicle for work you must have class 1 business insurance)"
                                        value={carinsurance}
                                        required
                                      >
                                        <AvRadio label="Yes" value="yes" />
                                        <AvRadio label="No" value="no" />
                                      </AvRadioGroup>
                                    </Col>
                                    <Col xs="12" md="4">
                                      <AvRadioGroup inline name="vehicledocumentsvalid" label={"Are all your vehicle documents up to date and valid?"} value={vehicledocumentsvalid} required>
                                        <AvRadio label="Yes" value="yes" />
                                        <AvRadio label="No" value="no" />
                                      </AvRadioGroup>
                                    </Col>
                                  </div>
                                </Row>
                              ) : null}

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Languages</p>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="englishspoken" label="English - Spoken" value={englishspoken} required>
                                      <AvRadio label="Fluent" value="fluent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Fair" value="fair" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="englishwritten" label={"English - Written"} value={englishwritten} required>
                                      <AvRadio label="Fluent" value="fluent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Fair" value="fair" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="Other Languages Spoken" name="otherlanguage" placeholder="Enter Other Languages Spoken" onChange={this.onChange} value={otherlanguage} />
                                  </Col>

                                  <Col xs="12" md="12">
                                    <AvRadioGroup inline label={"Have you passed each of the academic modules of the IELTS test?"} name="IELTStest" value={IELTStest} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.IELTStestyes("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.IELTStestyes} />
                                    </AvRadioGroup>
                                  </Col>
                                </div>
                              </Row>
                              {this.state.IELTStestyes ? (
                                <Row>
                                  <Col xs="12" md="12">
                                    <b>Please note: you have to provide copies of all IELTS certificates held by you</b>
                                  </Col>
                                </Row>
                              ) : null}

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Next of kin details</p>
                                    <strong className="an-heading">Name</strong>
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="select" name="kinprefix" label="Prefix" placeholder="Select Prefix" value={kinprefix} required>
                                      <option>Please Select</option>
                                      <option value="mr">Mr.</option>
                                      <option value="mrs">Mrs.</option>
                                      <option value="miss">Miss.</option>
                                      <option value="ms">Ms.</option>
                                      <option value="dr">Dr.</option>
                                      <option value="prof">Prof.</option>
                                      <option value="rev">Rev.</option>
                                    </AvField>
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="First Name" name="kinfirstname" placeholder="Enter First Name" onChange={this.onChange} value={kinfirstname} required />
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="Last Name" name="kinlastname" placeholder="Enter Last Name" onChange={this.onChange} value={kinlastname} required />
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="Relationship to you:" name="kinrelationship" placeholder="Enter Relationship to you:" onChange={this.onChange} value={kinrelationship} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="number" label="Home/Work Phone" name="kinhomephone" placeholder="Enter Home/Work Phone" onChange={this.onChange} value={kinhomephone} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    {/* <AvField type="number" label="Mobile No" name="kinmobileno" placeholder="Enter Mobile Number" onChange={this.onChange} value={kinmobileno} required /> */}
                                    <Label className={this.state.phoneerror1 ? "error-color" : null}>Mobile No</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={kinmobileno && kinmobileno.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror1 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler1}
                                      value={kinmobileno && kinmobileno.number}
                                      required
                                    />
                                    {this.state.phoneerror1 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="email" label="Email" name="kinemail" placeholder="Enter Email" onChange={this.onChange} value={kinemail} required />
                                  </Col>
                                  <Col xs="12" md="12">
                                    <strong className="an-heading">Address</strong>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Street Address" name="kinstreetAddress" placeholder="Enter Street Address" onChange={this.onChange} value={kinstreetAddress} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Address Line 2" name="kinaddressline2" placeholder="Enter Address Line 2" onChange={this.onChange} value={kinaddressline2} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="City" name="kincity" placeholder="Enter City" onChange={this.onChange} value={kincity} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="text" label="County / State / Region" name="kinstate" placeholder="Enter State" onChange={this.onChange} value={kinstate} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="ZIP / Postal Code" name="kinzip" placeholder="Enter ZIP / Postal Code" onChange={this.onChange} value={kinzip} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Country" name="kincountry" placeholder="Enter Country" onChange={this.onChange} value={kincountry} required />
                                  </Col>
                                </div>
                              </Row>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">About Your Work</p>
                                    <label>When are you able to work?</label>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_mornings" onChange={this.onChange} checked={work_mornings} /> Mornings
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_evenings" onChange={this.onChange} checked={work_evenings} /> Evenings
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_afternoons" onChange={this.onChange} checked={work_afternoons} /> Afternoons
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_occasional" onChange={this.onChange} checked={work_occasional} /> Occasional Weeks
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_fulltime" onChange={this.onChange} checked={work_fulltime} /> Full Time
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_parttime" onChange={this.onChange} checked={work_parttime} /> Part Time
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_nights" onChange={this.onChange} checked={work_nights} /> Nights
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_weekends" onChange={this.onChange} checked={work_weekends} /> Weekends
                                    </Col>
                                    <Col className="che-box">
                                      <AvInput type="checkbox" name="work_anytime" onChange={this.onChange} checked={work_anytime} /> Anytime
                                    </Col>
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="text" label="1st Choice" name="firstchoice" placeholder="Enter 1st Choice" onChange={this.onChange} value={firstchoice} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="2nd Choice" name="secondchoice" placeholder="Enter 2nd Choice" onChange={this.onChange} value={secondchoice} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="3rd Choice" name="thirdchoice" placeholder="Enter 3rd Choice" onChange={this.onChange} value={thirdchoice} />
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="shortnotice" label={"Would you be willing to work at short notice?"} value={shortnotice} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="reduceworkflexibility" label={"Do you have any commitments that reduce your flexibility to work"} value={reduceworkflexibility} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  <Col xs="12" md="12">
                                    <AvField type="textarea" label="If yes, please state:" name="workstate" placeholder="Enter State" onChange={this.onChange} value={workstate} />
                                  </Col>
                                </div>
                              </Row>
                              <Row className="d-flex justify-content-end">
                                <Button className="mr-3" color="success" type="submit">
                                  Update
                                </Button>
                              </Row>
                            </AvForm>
                          </TabPane>
                          <TabPane tabId="2">
                            <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Your History</p>
                                    <p>
                                      Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps
                                      unaccounted for and it covers full work history including your education. Please use extra paper if required. Full work history including your education Dates to and from are shown in a mm/yy format
                                      Dates are continual with NO gaps Where there have been gaps in work history please state the reason for the gaps Lists all relevant training undertaken
                                    </p>
                                    <p>
                                      Please ensure you complete this section even if you have a CV. The NHS states that “Employment history should be recorded on an Application Form which is signed” Please ensure that you leave no gaps
                                      unaccounted for and it covers full work history including your education. Please use extra paper if required.
                                    </p>
                                    <br />
                                    <p>Full work history including your education</p>
                                    <p>Dates to and from are shown in a mm/yy format</p>
                                    <p>Dates are continual with NO gaps</p>
                                    <p>Where there have been gaps in work history please state the reason for the gaps</p>
                                    <p>Lists all relevant training undertaken</p>
                                  </Col>
                                </div>
                              </Row>

                              <Row className="some-select">
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Employment History</p>
                                  </Col>
                                  <Col xs="12" md="2">
                                    <AvField type="text" label="Employer" name="employer" placeholder="Enter EMPLOYER" value={this.state.employer} onChange={this.onChange} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <AvField type="text" label="position" name="position" placeholder="Enter position" value={this.state.position} onChange={this.onChange} />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <AvField type="text" label="Duties" name="duties" placeholder="Enter DUTIES" value={this.state.duties} onChange={this.onChange} />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <label> Date </label>
                                    <DateRangePicker
                                      showClearDates={true}
                                      startDate={this.state.start_date}
                                      startDateId="start_date"
                                      endDate={this.state.end_date}
                                      endDateId="end_date"
                                      onDatesChange={({ startDate, endDate }) => {
                                        this.setState({
                                          start_date: startDate,
                                          end_date: endDate
                                        });
                                      }}
                                      focusedInput={this.state.focusedInput}
                                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={() => false}
                                    />
                                  </Col>
                                  <Col xs="12" md="2">
                                    <AvField type="text" label="Salary on Leaving" name="salaryonleaving" placeholder="Enter Salary on Leaving" value={this.state.salaryonleaving} onChange={this.onChange} />
                                  </Col>
                                </div>
                              </Row>

                              {employmenthistorylist &&
                                employmenthistorylist.length > 0 &&
                                employmenthistorylist.map((list, key) => (
                                  <>
                                    <Row key={key}>
                                      <div className="cus-design edited-section">
                                        <Col xs="12" md="2">
                                          <span>{list.employer}</span>
                                        </Col>
                                        <Col xs="12" md="2">
                                          <span>{list.position}</span>
                                        </Col>
                                        <Col xs="12" md="2">
                                          <span>{list.duties}</span>
                                        </Col>
                                        <Col xs="12" md="3">
                                          <span>{moment(list.start_date).format("DD-MM-YYYY") + " - " + moment(list.end_date).format("DD-MM-YYYY")}</span>
                                        </Col>
                                        <Col xs="12" md="2">
                                          <span>{list.salaryonleaving}</span>
                                        </Col>
                                        <Col xs="12" md="1">
                                          <Button className="npm-pad pl-sus" onClick={this.addmploymenthistory}>
                                            {" "}
                                            <i className="fa fa-plus" />
                                          </Button>
                                          <Button className="npm-pad pl-dan" onClick={() => this.removeemploymenthistory(key)}>
                                            {" "}
                                            <i className="fa fa-minus" />
                                          </Button>
                                        </Col>
                                      </div>
                                    </Row>
                                  </>
                                ))}
                              <br />

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Education</p>
                                    <p className="help-block">Please supply documentary evidence.</p>
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="INSTITUTION" name="institution" placeholder="Enter INSTITUTION" value={this.state.institution} onChange={this.onChange} />
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="COURSE" name="course" placeholder="Enter COURSE" value={this.state.course} onChange={this.onChange} />
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="YEAR" name="year" placeholder="Enter YEAR" value={this.state.year} onChange={this.onChange} />
                                  </Col>
                                  <Col xs="12" md="3">
                                    <AvField type="text" label="GRADE" name="grade" placeholder="Enter GRADE" value={this.state.grade} onChange={this.onChange} />
                                  </Col>
                                </div>
                              </Row>

                              {educationlist &&
                                educationlist.length > 0 &&
                                educationlist.map((item, key) => (
                                  <>
                                    <Row key={key}>
                                      <div className="cus-design edited-section">
                                        <Col xs="12" md="3">
                                          {item.institution}
                                        </Col>
                                        <Col xs="12" md="3">
                                          {item.course}
                                        </Col>
                                        <Col xs="12" md="2">
                                          {item.year}
                                        </Col>
                                        <Col xs="12" md="2">
                                          {item.grade}
                                        </Col>
                                        <Col xs="12" md="2">
                                          <Button className="npm-pad pl-sus" onClick={this.addeducationlist}>
                                            {" "}
                                            <i className="fa fa-plus" />
                                          </Button>
                                          <Button className="npm-pad pl-dan" onClick={() => this.removeeducationlist(key)}>
                                            {" "}
                                            <i className="fa fa-minus" />
                                          </Button>
                                        </Col>
                                      </div>
                                    </Row>
                                  </>
                                ))}

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="6" md="3">
                                    <Label for="exampleCustomFileBrowser1">Upload CV if you have one</Label>
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser1"
                                      name="UploadCV"
                                      onChange={this.uploadCV}
                                      label={this.state.UploadCVName ? this.state.UploadCVName : "Upload  Image"}
                                      encType="multipart/form-data"
                                    />
                                  </Col>
                                  {UploadCV && (
                                    <Col xs="6" md="3">
                                      <Label for="exampleCustomFileBrowser1" />
                                      <Button color="success" onClick={() => this.FileDownloader(UploadCV, `CV${this.state.UploadCVName.slice(-4)}`)}>
                                        Download
                                      </Button>
                                    </Col>
                                  )}
                                </div>
                              </Row>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5"> REFERENCES</p>
                                    <label>What visa/permit/status do you currently hold?:</label>
                                    <p>
                                      Please supply us with two professional referees. One must be from your present or most recent employer and must be a senior grade to yourself and you must have worked for that person for a period of not
                                      less than three months duration.
                                    </p>
                                  </Col>
                                </div>
                              </Row>

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Reference 1</p>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Relationship" name="firstrefrelationship" placeholder="Enter Relationship " onChange={this.onChange} value={firstrefrelationship} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="First Name" name="firstreffirstname" placeholder="Enter First Name" onChange={this.onChange} value={firstreffirstname} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Last Name" name="firstreflastname" placeholder="Enter Last Name" onChange={this.onChange} value={firstreflastname} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <Label className={this.state.phoneerror2 ? "error-color" : null}>Mobile No</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={firstrefphone && firstrefphone.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror2 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler2}
                                      value={firstrefphone && firstrefphone.number}
                                      required
                                    />
                                    {this.state.phoneerror2 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" label="Phone" name="firstrefphone" placeholder="Enter  Phone" onChange={this.onChange} value={this.state.firstrefphone} required /> */}
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="email" label="Email" name="firstrefemail" placeholder="Enter Email" onChange={this.onChange} value={firstrefemail} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField
                                      type="email"
                                      label="Confirm Email"
                                      name="firstrefconfirmemail"
                                      placeholder="Enter Email"
                                      onChange={this.onChange}
                                      value={firstrefconfirmemail}
                                      required
                                      validate={{ match: { value: "firstrefemail" } }}
                                    />
                                  </Col>
                                  <Col xs="12" md="12">
                                    <strong className="an-heading">Address</strong>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Street Address" name="firstrefstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={firstrefstreetAddress} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="City" name="firstrefcity" placeholder="Enter City.." onChange={this.onChange} value={firstrefcity} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="text" label="County / State / Region" name="firstrefstate" placeholder="Enter State.." onChange={this.onChange} value={firstrefstate} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="ZIP / Postal Code" name="firstrefzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={firstrefzip} required />
                                  </Col>
                                </div>
                              </Row>

                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <h4>Reference 2</h4>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Relationship" name="secondrefrelationship" placeholder="Enter Relationship .." onChange={this.onChange} value={secondrefrelationship} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="First Name" name="secondreffirstname" placeholder="Enter First Name.." onChange={this.onChange} value={secondreffirstname} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Last Name" name="secondreflastname" placeholder="Enter Last Name.." onChange={this.onChange} value={secondreflastname} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <Label className={this.state.phoneerror3 ? "error-color" : null}>Mobile No</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={secondrefphone && secondrefphone.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror3 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler3}
                                      value={secondrefphone && secondrefphone.number}
                                      required
                                    />
                                    {this.state.phoneerror3 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" label="Phone" name="secondrefphone" placeholder="Enter  Phone.." onChange={this.onChange} value={this.state.secondrefphone} required /> */}
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="email" label="Email" name="secondrefemail" placeholder="Enter Email.." onChange={this.onChange} value={secondrefemail} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField
                                      type="email"
                                      label="Confirm Email"
                                      name="secondrefconfirmemail"
                                      placeholder="Enter Email.."
                                      onChange={this.onChange}
                                      value={secondrefconfirmemail}
                                      required
                                      validate={{ match: { value: "secondrefemail" } }}
                                    />
                                  </Col>
                                  <Col xs="12" md="12">
                                    <strong className="an-heading">Address</strong>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Street Address" name="secondrefstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={secondrefstreetAddress} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="City" name="secondrefcity" placeholder="Enter City.." onChange={this.onChange} value={secondrefcity} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="text" label="County / State / Region" name="secondrefstate" placeholder="Enter State.." onChange={this.onChange} value={secondrefstate} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="ZIP / Postal Code" name="secondrefzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={secondrefzip} required />
                                  </Col>
                                </div>
                              </Row>
                              <Row className="d-flex justify-content-end">
                                <Button color="success" className="mr-3" type="submit">
                                  Update
                                </Button>
                              </Row>
                            </AvForm>
                          </TabPane>
                          <TabPane tabId="3">
                            <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <h4>SKILLS, EXPERIENCE & TRAINING</h4>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you completed mandatory training within the last year"} name="mandatory_training" value={mandatory_training} required>
                                      <AvRadio label="Yes" onClick={() => this.mandatorytraining("yes")} value="yes" />
                                      <AvRadio label="No" onClick={this.mandatorytraining} value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                </div>
                              </Row>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    {this.state.mandatorytrainingyes ? (
                                      <>
                                        <h4>Mandatory Training</h4>
                                        <h6>Please tick if you have completed the following training within the last 12 months</h6>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="movinghandling" onChange={this.onChange} checked={movinghandling} value={movinghandling} /> Moving & Handling
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="basiclifesupport" onChange={this.onChange} checked={basiclifesupport} value={basiclifesupport} /> Basic life support
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="healthsafety" onChange={this.onChange} checked={healthsafety} value={healthsafety} /> Health and Safety
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="firesafety" onChange={this.onChange} checked={firesafety} value={firesafety} /> Fire Safety
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="firstaid" onChange={this.onChange} checked={firstaid} value={firstaid} /> First Aid
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="infectioncontrol" onChange={this.onChange} checked={infectioncontrol} value={infectioncontrol} /> Infection Control
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="foodsafety" onChange={this.onChange} checked={foodsafety} value={foodsafety} /> Food Safety & Nutrition
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="medicationadmin" onChange={this.onChange} checked={medicationadmin} value={medicationadmin} /> Medication Administration
                                        </Col>
                                        <Col className="che-box">
                                          <AvInput type="checkbox" name="safeguardingvulnerable" onChange={this.onChange} checked={safeguardingvulnerable} value={safeguardingvulnerable} /> Safeguarding Vulnerable Adults & Children
                                        </Col>
                                        <Col xs="12" md="12">
                                          <AvField type="textarea" label="Training Dates:" name="trainingdates" placeholder="" onChange={this.onChange} value={trainingdates} required />
                                        </Col>
                                      </>
                                    ) : null}
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you completed other health care training?"} name="healthcare_training" value={healthcare_training} required>
                                      <AvRadio label="Yes" onClick={() => this.healthcaretraining("yes")} value="yes" />
                                      <AvRadio label="No" onClick={this.healthcaretraining} value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                </div>
                              </Row>

                              {this.state.healthcaretrainingyes ? (
                                <>
                                  <Row>
                                    <div className="cus-design edited-section">
                                      <Col xs="12" md="12">
                                        <p className="h5">Other Training/Courses & Qualifications</p>
                                        <h6>Please check which training you have completed and the date on the notes (certificates must be provided).</h6>
                                        <Col>
                                          <AvInput type="checkbox" name="diplomal3" onChange={this.onChange} checked={diplomal3} value={diplomal3} /> Diploma in Health & Social Care L3
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="diplomal2" onChange={this.onChange} checked={diplomal2} value={diplomal2} /> Diploma in Health & Social Care L2
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="personalsafetymental" onChange={this.onChange} checked={personalsafetymental} value={personalsafetymental} /> Personal Safety (Mental Health & Learning Disability)
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="intermediatelife" onChange={this.onChange} checked={intermediatelife} value={intermediatelife} /> Intermediate Life Support
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="advancedlife" onChange={this.onChange} checked={advancedlife} value={advancedlife} /> Advanced Life Support
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="complaintshandling" onChange={this.onChange} checked={complaintshandling} value={complaintshandling} /> Complaints Handling
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="handlingviolence" onChange={this.onChange} checked={handlingviolence} value={handlingviolence} /> Handling Violence and Aggression
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="doolsmental" onChange={this.onChange} checked={doolsmental} value={doolsmental} /> DOLLS & Mental Capacity
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="coshh" onChange={this.onChange} checked={coshh} value={coshh} /> COSHH
                                        </Col>{" "}
                                        <Col>
                                          <AvInput type="checkbox" name="dataprotection" onChange={this.onChange} checked={dataprotection} value={dataprotection} /> Data Protection
                                        </Col>{" "}
                                        <Col>
                                          <AvInput type="checkbox" name="equalityinclusion" onChange={this.onChange} checked={equalityinclusion} value={equalityinclusion} /> Equality & Inclusion
                                        </Col>{" "}
                                        <Col>
                                          <AvInput type="checkbox" name="loneworkertraining" onChange={this.onChange} checked={loneworkertraining} value={loneworkertraining} /> Lone Worker Training
                                        </Col>{" "}
                                        <Col>
                                          <AvInput type="checkbox" name="resuscitation" onChange={this.onChange} checked={resuscitation} value={resuscitation} /> Resuscitation of the Newborn (Midwifery)
                                        </Col>
                                        <Col>
                                          <AvInput type="checkbox" name="interpretation" onChange={this.onChange} checked={interpretation} value={interpretation} /> Interpretation of Cardiotocograph Traces (Midwifery)
                                        </Col>
                                        <Col xs="12" md="12">
                                          <AvField type="textarea" label="Training Dates:" name="trainindates2" placeholder="" onChange={this.onChange} value={trainindates2} required />
                                        </Col>
                                      </Col>
                                    </div>
                                  </Row>
                                </>
                              ) : null}
                              {/* <br />
                      <h4>APRAISALS</h4>
                      <p>
                        In order to work in the NHS you will need to be appraised annually by a Senior Practitioner of the same discipline, this person will become your “appraiser” Please give details below of the Senior Practitioner who
                        you have made arrangements with to act as your appraiser.
                      </p>
                      <strong>Name of Appraiser</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="First Name" name="appraiserfirstname" placeholder="Enter First Name.." onChange={this.onChange} value={this.state.appraiserfirstname} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Last Name" name="appraiserlastname" placeholder="Enter Last Name.." onChange={this.onChange} value={this.state.appraiserlastname} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Position and Grade of Appraiser:" name="gradeofappraiser" placeholder="Enter Position and Grade of Appraiser: .." onChange={this.onChange} value={this.state.gradeofappraiser} required />
                        </Col>
                      </Row>
                      <strong>Branch Address:</strong>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Street Address" name="appraiserstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={this.state.appraiserstreetAddress} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="City" name="appraisercity" placeholder="Enter City.." onChange={this.onChange} value={this.state.appraisercity} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="County / State / Region" name="appraiserstate" placeholder="Enter State.." onChange={this.onChange} value={this.state.appraiserstate} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="text" label="ZIP / Postal Code" name="appraiserzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={this.state.appraiserzip} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="number" label="Phone" name="appraiserphone" placeholder="Enter  Phone.." onChange={this.onChange} value={this.state.appraiserphone} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField type="text" label="Fax" name="appraiserfax" placeholder="Enter  Fax.." onChange={this.onChange} value={this.state.appraiserfax} required />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="email" label="Email" name="appraiseremail" placeholder="Enter Email.." onChange={this.onChange} value={this.state.appraiseremail} required />
                        </Col>
                        <Col xs="12" md="4">
                          <AvField
                            type="email"
                            label="Confirm Email"
                            name="appraiserconfirmemail"
                            placeholder="Enter Email.."
                            onChange={this.onChange}
                            value={this.state.appraiserconfirmemail}
                            required
                            validate={{ match: { value: "appraiseremail" } }}
                          />
                        </Col>
                      </Row>
                      <h3>YOUR DBS STATUS & UNIFORM</h3>
                      <h4>DBS Status</h4>
                      <p className="help-block">Please send a copy of your most recent DBS Disclosure (formally known as CRB)</p>
                      <Row>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline name="DBS" label={"Do you have a current DBS (Disclosure Barring Service) (formally known as CRB)?"} onChange={this.onChange} value={this.state.DBS} required>
                            <AvRadio label="Yes" value="Yes" onClick={() => this.DBSyes("yes")} />
                            <AvRadio label="No" value="No" onClick={this.DBSyes} />
                          </AvRadioGroup>
                          <p className="help-block">Current DBS Disclosure (formally known as CRB)</p>
                        </Col>
                        <Col xs="12" md="4">
                          <AvRadioGroup inline label={"Clear"} name="clear" onChange={this.onChange} value={this.state.clear} required>
                            <AvRadio label="Yes" value="Yes" />
                            <AvRadio label="No" value="No" />
                          </AvRadioGroup>
                        </Col>
                      </Row>
                      {this.state.DBSyes ? (
                        <Row>
                          <Col xs="12" md="4">
                            <p>Date of issue:</p>
                            <DatePicker value={this.state.DBSdate} onChange={this.DBSChange} placeholder="Enter Date of issue.." />
                          </Col>
                          <Col xs="12" md="4">
                            <AvField type="text" label="Disclosure number" name="disclosurenumber" placeholder="Enter  Disclosure number.." onChange={this.onChange} value={this.state.disclosurenumber} required />
                          </Col>
                        </Row>
                      ) : null}
                      <p>
                        All applicants who cannot provide a registered DBS or full immunisation record will be required to complete at their own cost. The company will cover the cost of any Mandatory Training updates however cancellations
                        outside of 48 hours and late attendances will be charged to the candidate.
                      </p>
                      <br />
                      <h4>Uniform</h4>
                      <p>
                        Candidates will be required to purchase uniform if required at the cost of £20 this will be deducted from your timesheet once you have started working through us. Please fill in the box below stating your uniform
                        size and quantity.
                      </p>
                      <br />
                      <Row>
                        <Col xs="12" md="4">
                          <AvField type="select" name="tunicsize" label="Your Tunic Size" placeholder="Select Your Tunic Size.." onChange={this.onChange} value={this.state.tunicsize}>
                            <option>Please Select</option>
                            <option>XS</option>
                            <option>S</option>
                            <option>M</option>
                            <option>L</option>
                            <option>XL</option>
                            <option>XXL</option>
                          </AvField>
                        </Col>
                      </Row>*/}
                              <Row className="d-flex justify-content-end">
                                <Button color="success" className="mr-3" type="submit">
                                  Update
                                </Button>
                              </Row>
                            </AvForm>
                          </TabPane>
                        </>
                      ) : null}
                      <TabPane tabId="4">
                        <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                          <Row className="onl-mar">
                            <div className="cus-design edited-section">
                              <Col xs="12" md="12">
                                <p className="h5">Upload & Submit Required Documents</p>
                              </Col>
                              <Col xs="12" md="6" className="upl-files">
                                <div className="upload-forms-edit">
                                  <Label className="frm-edit" for="exampleCustomFileBrowser3">
                                    Certificate
                                  </Label>
                                  {form_status > 9 ? null : (
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser3"
                                      name="uploadcertificate"
                                      onChange={(e, id) => this.uploadDocs(e, "certificate")}
                                      label={"Upload  Documents"}
                                      encType="multipart/form-data"
                                      accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                                    />
                                  )}

                                  <Col className="doc-upload">
                                    {certificatedocs &&
                                      certificatedocs.length > 0 &&
                                      certificatedocs.map((list, i) => (
                                        <p key={i}>
                                          <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                          <span className="documents-name"> {list && list.name} </span>
                                          {form_status > 9 ? null : (
                                            <span className="documents-del">
                                              <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "certificate")}>
                                                {" "}
                                                <i className="fa fa-trash-o" aria-hidden="true" />
                                              </button>
                                            </span>
                                          )}
                                          <span className="documents-download">
                                            <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                              {" "}
                                              <i className="fa fa-download" aria-hidden="true" />{" "}
                                            </button>{" "}
                                          </span>
                                        </p>
                                      ))}
                                  </Col>
                                </div>
                              </Col>

                              <Col xs="12" md="6" className="upl-files">
                                <div className="upload-forms-edit">
                                  <Label className="frm-edit" for="exampleCustomFileBrowser4">
                                    Passport
                                  </Label>
                                  {form_status > 9 ? null : (
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser4"
                                      name="uploadpassport"
                                      onChange={(e, id) => this.uploadDocs(e, "passport")}
                                      label={"Upload  Documents"}
                                      encType="multipart/form-data"
                                      accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                                    />
                                  )}

                                  <Col className="doc-upload">
                                    {passportdocs &&
                                      passportdocs.length > 0 &&
                                      passportdocs.map((list, i) => (
                                        <p key={i}>
                                          <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                          <span className="documents-name">{list && list.name} </span>
                                          {form_status > 9 ? null : (
                                            <span className="documents-del">
                                              <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "passport")}>
                                                <i className="fa fa-trash-o" aria-hidden="true" />
                                              </button>
                                            </span>
                                          )}
                                          <span className="documents-download">
                                            <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                              {" "}
                                              <i className="fa fa-download" aria-hidden="true" />{" "}
                                            </button>{" "}
                                          </span>
                                        </p>
                                      ))}
                                  </Col>
                                </div>
                              </Col>

                              <Col xs="12" md="6" className="upl-files">
                                <div className="upload-forms-edit">
                                  <Label className="frm-edit" for="exampleCustomFileBrowser5">
                                    Proof Of Address
                                  </Label>
                                  {form_status > 9 ? null : (
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser5"
                                      name="uploadaddress"
                                      onChange={(e, id) => this.uploadDocs(e, "address")}
                                      label={"Upload  Documents"}
                                      encType="multipart/form-data"
                                      accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                                    />
                                  )}
                                  <Col className="doc-upload">
                                    {addressdocs &&
                                      addressdocs.length > 0 &&
                                      addressdocs.map((list, i) => (
                                        <p key={i}>
                                          <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                          <span className="documents-name"> {list && list.name} </span>
                                          {form_status > 9 ? null : (
                                            <span className="documents-del">
                                              <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "address")}>
                                                {" "}
                                                <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                                              </button>{" "}
                                            </span>
                                          )}
                                          <span className="documents-download">
                                            <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                              {" "}
                                              <i className="fa fa-download" aria-hidden="true" />{" "}
                                            </button>{" "}
                                          </span>
                                        </p>
                                      ))}
                                  </Col>
                                </div>
                              </Col>

                              <Col xs="12" md="6" className="upl-files">
                                <div className="upload-forms-edit">
                                  <Label className="frm-edit" for="exampleCustomFileBrowser6">
                                    DBS Certificate
                                  </Label>
                                  {form_status > 9 ? null : (
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser6"
                                      name="uploaddbs"
                                      onChange={(e, id) => this.uploadDocs(e, "dbs")}
                                      label={"Upload  Documents"}
                                      encType="multipart/form-data"
                                      accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                                    />
                                  )}
                                  <Col className="doc-upload">
                                    {dbsdocs &&
                                      dbsdocs.length > 0 &&
                                      dbsdocs.map((list, i) => (
                                        <p key={i}>
                                          <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                          <span className="documents-name"> {list && list.name} </span>
                                          {form_status > 9 ? null : (
                                            <span className="documents-del">
                                              <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "dbs")}>
                                                {" "}
                                                <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                                              </button>{" "}
                                            </span>
                                          )}
                                          <span className="documents-download">
                                            <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                              {" "}
                                              <i className="fa fa-download" aria-hidden="true" />{" "}
                                            </button>{" "}
                                          </span>
                                        </p>
                                      ))}
                                  </Col>
                                </div>
                              </Col>

                              <Col xs="12" md="6" className="upl-files">
                                <div className="upload-forms-edit">
                                  <Label className="frm-edit" for="exampleCustomFileBrowser6">
                                    Other Documents
                                  </Label>
                                  {form_status > 9 ? null : (
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser9"
                                      name="uploadotherdocs"
                                      onChange={(e, id) => this.uploadDocs(e, "other")}
                                      label={"Upload  Documents"}
                                      encType="multipart/form-data"
                                      accept=".png, .jpg, .jpeg, .pdf, .docx, .xslx"
                                    />
                                  )}
                                  <Col className="doc-upload">
                                    {otherdocs &&
                                      otherdocs.length > 0 &&
                                      otherdocs.map((list, i) => (
                                        <p key={i}>
                                          <img src={`${NodeURL}/uploads/profiles.png`} alt="files" />
                                          <span className="documents-name">{list && list.name} </span>
                                          {form_status > 9 ? null : (
                                            <span className="documents-del">
                                              <button type="button" className="btn" onClick={() => this.deleteDocs(list.path, "other")}>
                                                {" "}
                                                <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                                              </button>
                                            </span>
                                          )}
                                          <span className="documents-download">
                                            <button type="button" className="btn" onClick={() => this.FileDownloader(list.path, list.name)}>
                                              {" "}
                                              <i className="fa fa-download" aria-hidden="true" />{" "}
                                            </button>{" "}
                                          </span>
                                        </p>
                                      ))}
                                  </Col>
                                </div>
                              </Col>
                            </div>
                          </Row>
                        </AvForm>
                      </TabPane>
                      {path_name !== Files_URL ? (
                        <>
                          <TabPane tabId="5">
                            <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Health Declaration</p>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Do you or have ever suffered from long term illness?:"} name="sufferedlongtermillness" value={sufferedlongtermillness} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you ever required sick leave for a back or neck injury?:"} name="leaveforneckinjury" value={leaveforneckinjury} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Do you suffer with any back or neck injuries?:"} name="neckinjuries" value={neckinjuries} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you been in contact with anyone who is suffering from a contagious illness within the last six weeks?:"} name="sixweeksillness" value={sixweeksillness} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Do you suffer with a communicable disease?:"} name="communicabledisease" value={communicabledisease} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Are you currently receiving active medical attention?:"} name="medicalattention" value={medicalattention} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  {/* <Col xs="12" md="4">
                                    <p>Do you suffer with a communicable disease?:</p>
                                   <AvRadioGroup
                                      name="clear"
                                      onChange={this.onChange}
                                      value={clear}
                                      required
                                    >
                                      <AvRadio label="Yes" value="Yes" />
                                      <AvRadio label="No" value="No"/>
                                    </AvRadioGroup>
                                    </Col> */}
                                  <Col xs="12" md="12">
                                    <AvField
                                      type="textarea"
                                      label="If you have answered ‘yes’ to any of the above, please give details:"
                                      name="healthdeclarationdetails"
                                      placeholder="Enter  Details"
                                      onChange={this.onChange}
                                      value={healthdeclarationdetails}
                                    />
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Are you registered disabled?:"} name="registereddisabled" value={registereddisabled} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField
                                      type="number"
                                      label="How many days have you been absent from work due to illness in the last 12 months?:"
                                      name="absentfromwork"
                                      placeholder="Enter Field"
                                      onChange={this.onChange}
                                      value={absentfromwork}
                                      required
                                    />
                                  </Col>

                                  <Col xs="12" md="12">
                                    <AvField type="textarea" label="State reason(s) for absence:" name="statereason" placeholder="Enter  State reason(s) for absence:" onChange={this.onChange} value={statereason} required />
                                  </Col>
                                  <Col xs="12" md="12">
                                    <strong className="an-heading">GP Details</strong>
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvField type="text" label="GP Name" name="GPname" placeholder="Enter GP Name" onChange={this.onChange} value={GPname} required />
                                  </Col>
                                  <Col xs="12" md="12">
                                    <strong className="an-heading">GP Address:</strong>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="Street Address" name="GPstreetAddress" placeholder="Enter Street Address" onChange={this.onChange} value={GPstreetAddress} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="City" name="GPcity" placeholder="Enter City" onChange={this.onChange} value={GPcity} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvField type="text" label="County / State / Region" name="GPstate" placeholder="Enter State" onChange={this.onChange} value={GPstate} required />
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvField type="text" label="ZIP / Postal Code" name="GPzip" placeholder="Enter ZIP / Postal Code" onChange={this.onChange} value={GPzip} required />
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"May we contact your Doctor for health check?:"} name="contactDoctor" value={contactDoctor} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <p>
                                      The above information will be held in strict confidence. If you aware of any health issue that you feel may affect your ability to undertake responsibilities of the post it is your responsibility to
                                      inform the Care Manager immediately. Again any details discussed in the meeting will be held in strict confidence.
                                    </p>
                                  </Col>
                                  <Col xs="12" md="1" />
                                  <Col xs="12" md="12">
                                    <AvGroup className="check-moves">
                                      <AvInput type="checkbox" name="GPhealthdetails" onChange={this.onChange} checked={GPhealthdetails} /> I understand that my GP may be contacted for details about my health.
                                    </AvGroup>
                                  </Col>
                                </div>
                              </Row>{" "}
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">MEDICAL HISTORY</p>
                                    <p className="help-block">All staff groups complete this section</p>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Do you have any illness/impairment/disability (physical or psychological) which may affect your work?"} name="medicalillness" value={medicalillness} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Have you ever had any illness/impairment/disability which may have been caused or made worse by your work?"} name="medicalillnesscaused" value={medicalillnesscaused} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvRadioGroup
                                      inline
                                      label={
                                        "Are you having, or waiting for treatment (including medication) or investigations at present? If your answer is yes, please provide further details of\n" +
                                        "                              the condition, treatment and dates?"
                                      }
                                      name="treatment"
                                      value={treatment}
                                      required
                                    >
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Do you think you may need any adjustments or assistance to help you to do the job?"} name="needanyadjustments" value={needanyadjustments} required>
                                      <AvRadio label="Yes" onClick={() => this.needanyadjustments("yes")} value="yes" />
                                      <AvRadio label="No" onClick={this.needanyadjustments} value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.needanyadjustmentsyes ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="If yes, please Provide:" name="needanyadjustmentsnotes" placeholder="Enter Information" onChange={this.onChange} value={needanyadjustmentsnotes || ""} />
                                    </Col>
                                  ) : null}
                                </div>
                              </Row>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Tuberculosis</p>
                                    <p className="help-block">Clinical diagnosis and management of tuberculosis, and measures for its prevention and control (NICE 2006)</p>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you lived continuously in the UK for the last 5 years"} name="livedUK" value={livedUK} required>
                                      <AvRadio label="Yes" onClick={this.livedUK} value="yes" />
                                      <AvRadio label="No" onClick={() => this.livedUK("no")} value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.livedUKyes ? (
                                    <Col xs="12" md="4">
                                      <AvField
                                        type="textarea"
                                        label="Please list all the countries that you have lived in over the last 5 years"
                                        name="livedUKnotes"
                                        placeholder="Enter Information"
                                        onChange={this.onChange}
                                        value={livedUKnotes || ""}
                                      />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you had a BCG vaccination in relation to Tuberculosis?"} name="BCGvaccination" value={BCGvaccination} required>
                                      <AvRadio label="Yes" onClick={() => this.BCGvaccination("yes")} value="yes" />
                                      <AvRadio label="No" onClick={this.BCGvaccination} value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.BCGvaccinationyes ? (
                                    <Col xs="12" md="4">
                                      <label>Date Of Birth</label>
                                      <SingleDatePicker
                                        id="BCGvaccinationdate"
                                        date={moment(this.state.BCGvaccinationdate)}
                                        onDateChange={BCGvaccinationdate => this.setState({ BCGvaccinationdate })}
                                        focused={this.state.focused85}
                                        onFocusChange={({ focused: focused85 }) => this.setState({ focused85 })}
                                        displayFormat="DD-MM-YYYY"
                                        isOutsideRange={() => false}
                                      />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"A cough which has lasted for more than 3 weeks"} name="cough" value={cough} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Unexplained weight loss"} name="weightloss" value={weightloss} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    {" "}
                                    <AvRadioGroup inline label={"Unexplained fever"} name="fever" value={fever} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline label={"Have you had tuberculosis (TB) or been in recent contact with open TB"} name="TB" value={TB} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                </div>
                              </Row>
                              <br />
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Chicken Pox or Shingles</p>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <AvRadioGroup inline label={"Have you ever had chicken pox or shingles?"} name="chickenpox" value={chickenpox} required>
                                      <AvRadio label="Yes" onClick={() => this.chickenpox("yes")} value="yes" />
                                      <AvRadio label="No" onClick={this.chickenpox} value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.chickenpoxyes ? (
                                    <Col xs="12" md="4">
                                      <label>Please state date</label>
                                      <SingleDatePicker
                                        id="chickenpoxdate"
                                        date={moment(this.state.chickenpoxdate)}
                                        onDateChange={chickenpoxdate => this.setState({ chickenpoxdate })}
                                        focused={this.state.focused95}
                                        onFocusChange={({ focused: focused95 }) => this.setState({ focused95 })}
                                        displayFormat="DD-MM-YYYY"
                                        isOutsideRange={() => false}
                                      />
                                    </Col>
                                  ) : null}

                                  <Col xs="12" md="12">
                                    <strong className="an-heading">Immunisation History</strong>
                                    <p>
                                      The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body
                                      for receipt of DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all
                                      staff. It is a condition of proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could
                                      invalidate your application or lead to the cancellation of your registration with us.
                                    </p>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Triple vaccination as a child (Diptheria / Tetanus / Whooping cough)?"} name="triplevaccination" value={triplevaccination} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Polio"} name="polio" value={polio} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Tetanus"} name="tetanus" value={tetanus} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Hepatitis B"} name="hepatitisB" value={hepatitisB} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>

                                  <Col xs="12" md="12">
                                    <strong className="an-heading">Varicella</strong>
                                    <p>You must provide a written statement to confirm that you have had chicken pox or shingles however we strongly advise that you provide serology test result showing varicella immunity</p>
                                    <strong className="an-heading">Tuberculosis</strong>
                                    <p>We require an occupational health/GP certificate of a positive scar or a record of a positive skin test result (Do not Self Declare)</p>
                                    <strong className="an-heading">Rubella, Measles & Mumps</strong>
                                    <p>Certificate of “two” MMR vaccinations or proof of a positive antibody for Rubella Measles & Mumps</p>
                                    <strong className="an-heading">Hepatitis B</strong>
                                    <p>You must provide a copy of the most recent pathology report showing titre levels of 100lu/l or above</p>
                                    <strong className="an-heading">Hepatitis B Surface Antigen</strong>
                                    <p>Evidence of a negative Surface Antigen Test Report must be an identified validated sample. (IVS)</p>
                                    <strong className="an-heading">Hepatitis C</strong>
                                    <p>Evidence of a negative antibody test Report must be an identified validated sample. (IVS)</p>
                                    <strong className="an-heading">HIV</strong>
                                    <p>Evidence of a negative antibody test Report must be an identified validated s ample. (IVS)</p>
                                    <strong className="an-heading">Exposure Prone Procedures</strong>
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Will your role involve Exposure Prone Procedures"} name="proneProcedures" value={proneProcedures} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                </div>
                              </Row>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Declaration</p>
                                    <AvGroup className="check-moves">
                                      <AvInput type="checkbox" name="occupationalHealthServices" onChange={this.onChange} checked={occupationalHealthServices} /> I declare that the answers to the above questions are true and complete to the
                                      best of my knowledge and belief. I also give consent for our appointed Occupational Health Services provider to make recommendations to my employer
                                    </AvGroup>
                                    <strong className="an-heading">Disclosure Barring Service (DBS)</strong>
                                    <p>
                                      The Disclosure and Barring Service (DBS – formerly Criminal Records Bureau CRB) is the executive agency of the Home Office responsible for conducting checks on criminal records. We are registered body
                                      for receipt of DBS disclosure information. NHS Trust and Private Sector hospitals and nursing homes insist on agencies making information recruitment decisions which require DBS checks to be made on all
                                      staff. It is a condition of proceeding with your application that you apply for a DBS disclosure check. The disclosure will be compared with the information given below and any inconsistencies could
                                      invalidate your application or lead to the cancellation of your registration with us
                                    </p>
                                  </Col>

                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Have you been convicted of a criminal offence?"} name="criminaloffence" value={criminaloffence} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline label={"Have you ever been cautioned or issued with a formal warning for a criminal offence?"} name="warningcriminaloffence" value={warningcriminaloffence} required>
                                      <AvRadio label="Yes" value="yes" />
                                      <AvRadio label="No" value="no" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <AvGroup className="check-moves">
                                      <AvInput type="checkbox" name="DBSdetails" onChange={this.onChange} checked={DBSdetails} /> I confirm the above is true and I understand that a DBS check will be sort in the event of a successful
                                      application.
                                    </AvGroup>
                                    <strong className="an-heading">Rehabilitation of Offenders Act 1974 and Criminal Records</strong>
                                    <p>
                                      By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment
                                      which is concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties.
                                      You should there force list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                                    </p>
                                    <strong className="an-heading"> Right To Work </strong>
                                    <p>
                                      It is a legal requirement that before any offer of work can be made all candidates provide the company with confirmation of their eligibility to work in the UK by providing one of the original documents
                                      detailed below.
                                    </p>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <AvRadioGroup inline name="passport" value={passport} required>
                                      <AvRadio
                                        label="A passport which describes the holder as a British Citizen or as having a right of abode in the United Kingdom or a passport or other travel document to show that the holder has IDENFINITE LEAVE TO REMAIN in the United Kingdom and is not precluded from taking the work in question."
                                        value="britishcitizen"
                                      />
                                      <AvRadio
                                        label="A passport or identity card issued by a State which is party to the European Union and EEA Agreement and which describes the holder as a national or a state which is a arty to that agreement."
                                        value="state"
                                      />
                                      <AvRadio
                                        label="A letter issued by the Home Office or the Department of Education and Employment indicating that the person named in the letter has permission to take agency work in question or a biometric residence permit."
                                        value="homeoffice"
                                      />
                                    </AvRadioGroup>
                                  </Col>
                                </div>
                              </Row>
                              <Row>
                                <div className="cus-design edited-section">
                                  <Col xs="12" md="12">
                                    <p className="h5">Work Time Directives</p>
                                    <p>
                                      By the virtue of the Rehabilitation of Offenders Act 1974 (Exemptions) (Amendments) Order 1986 the provision of section 4.2 of the Rehabilitation of Offenders Act 1974 do not apply to any employment
                                      which is concerned with the provision of health services and which is of such a kind to enable the holder to have access to persons in receipt of such services in the course of his/her normal duties.
                                      You should there force list all offences below even if you believe them to be ‘spent’ or ‘out of date’ for some other reason.
                                    </p>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <AvRadioGroup inline name="workhours" value={workhours} required>
                                      <AvRadio label="I DO NOT wish to work more than 48 hours per week" value="no" />
                                      <AvRadio label="I DO wish to work more than 48 hours per week" value="yes" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="12">
                                    <strong className="an-heading">Registration form Declaration</strong>
                                    <p>I declare that all information given in this registration form is to the best of my knowledge complete and accurate in all respects and that I am eligible to work in the UK.</p>
                                  </Col>
                                </div>
                              </Row>
                              <Row className="d-flex justify-content-end">
                                <Button color="success" className="mr-3" type="submit">
                                  Update
                                </Button>
                              </Row>
                            </AvForm>
                          </TabPane>
                        </>
                      ) : null}
                    </>
                  ) : null}
                  {path_name === Canditates_URL || path_name === Interview_URL ? (
                    <>
                      <TabPane tabId="6">
                        <Row>
                          <Col xs="12" md="4">
                            <div className="pro-interview gad_cl1">
                              <h4>
                                <span className="lf-path">
                                  <i className="fa fa-file-text-o" aria-hidden="true" />{" "}
                                </span>{" "}
                                <span className="rg-path"> Interview Details </span>
                              </h4>
                              {form_status === 2 ? (
                                <div className="interview-buttons">
                                  <Button className="int-views" onClick={() => this.DetailsFormModal("interviewcall")}>
                                    Send Interview Call
                                  </Button>
                                </div>
                              ) : null}
                              {form_status > 2 ? (
                                <div className="interview-rules">
                                  {" "}
                                  
                                  <p>
                                    <span> Interview Date : </span> {moment(interviewdate).format("DD-MM-YYYY")}
                                  </p>
                                  <p>
                                    <span> Interview Time : </span> {interviewtime}
                                  </p>
                                  <p>
                                    <span> Interview Call URL : </span> {videocallurl}
                                  </p>
                                  <p>
                                    <span> Interview Notes : </span> {interviewnotes}
                                  </p>
                                  <p>
                                    <span> Interview Status : </span> <Badge className="int-send"> Sent </Badge>
                                  </p>
                                </div>
                              ) : null}
                            </div>
                          </Col>
                          <Col xs="12" md="4">
                            <div className="pro-interview gad_cl2">
                              <h4>
                                <span className="lf-path">
                                  <i className="fa fa-file-text-o" aria-hidden="true" />{" "}
                                </span>{" "}
                                <span className="rg-path"> Post Interview Details </span>
                              </h4>

                              {form_status === 3 ? (
                                <div className="interview-buttons">
                                  <Button className="int-views" onClick={() => this.DetailsFormModal("postinterviewcall")}>
                                    Send Interview Result/Status
                                  </Button>
                                </div>
                              ) : null}
                              <div className="interview-buttons">{form_status > 3 ? <Badge className="int-views">Interview Completed</Badge> : null}</div>
                              {form_status > 3 ? (
                                <div className="interview-rules">
                                  <>
                                    <p>
                                      <span> Interviewed Date : </span> {moment(intervieweddate).format("DD-MM-YYYY")}
                                    </p>
                                    <p>
                                      <span> Notes : </span> {interviewednotes}
                                    </p>
                                  </>
                                </div>
                              ) : null}
                              {form_status > 3 ? (
                                <div className="interview-buttons">
                                  <>
                                    {this.state.interviewedstatus === 1 ? <Badge className="int-qua">Qualified</Badge> : null}
                                    {this.state.interviewedstatus === 0 ? <Badge color="int-dan">Disqualified</Badge> : null}
                                  </>
                                </div>
                              ) : null}
                            </div>
                          </Col>
                          <Col xs="12" md="4" className="qual-cals refer-colors">
                            <div className="pro-interview gad_cl3">
                              <h4>
                                <span className="lf-path">
                                  <i className="fa fa-file-text-o" aria-hidden="true" />{" "}
                                </span>{" "}
                                <span className="rg-path"> Verification Details </span>
                              </h4>

                              {form_status === 4 ? (
                                <div className="interview-buttons">
                                  <Button className="int-views" onClick={this.CheckFormModal}>
                                    Verification Process
                                  </Button>
                                </div>
                              ) : null}
                              {form_status === 5 ? (
                                <div className="interview-buttons">
                                  <Button className="int-views" onClick={() => this.MailModalOpen("verificationstatus")}>
                                    Send Verification Status Mail
                                  </Button>
                                </div>
                              ) : null}
                              <div className="interview-buttons">{this.state.form_status > 4 ? <Badge className="int-views">Verification Process Completed</Badge> : null}</div>
                            </div>
                          </Col>
                        </Row>
                      </TabPane>
                    </>
                  ) : null}
                  {path_name === Canditates_URL || path_name === Referee_URL ? (
                    <>
                      <TabPane tabId="7">
                        <AvForm ref={clear => (this.form = clear)} onSubmit={this.OnFormSubmit}>
                          <Row>
                            <div className="cus-design edited-section">
                              <Col xs="12" md="12">
                                <p className="h5">Reference 1</p>
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="Relationship" name="firstrefrelationship" placeholder="Enter Relationship " onChange={this.onChange} value={firstrefrelationship} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="First Name" name="firstreffirstname" placeholder="Enter First Name" onChange={this.onChange} value={firstreffirstname} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="Last Name" name="firstreflastname" placeholder="Enter Last Name" onChange={this.onChange} value={firstreflastname} required />
                              </Col>

                              <Col xs="12" md="4">
                                <Label className={this.state.phoneerror2 ? "error-color" : null}>Mobile No</Label>
                                <IntlTelInput
                                  style={{ width: "100%" }}
                                  defaultCountry={firstrefphone && firstrefphone.dialcountry}
                                  utilsScript={libphonenumber}
                                  css={this.state.phoneerror2 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                  onPhoneNumberChange={this.handler2}
                                  value={firstrefphone && firstrefphone.number}
                                  required
                                />
                                {this.state.phoneerror2 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                {/* <AvField type="number" label="Phone" name="firstrefphone" placeholder="Enter  Phone" onChange={this.onChange} value={this.state.firstrefphone} required /> */}
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="email" label="Email" name="firstrefemail" placeholder="Enter Email" onChange={this.onChange} value={firstrefemail} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField
                                  type="email"
                                  label="Confirm Email"
                                  name="firstrefconfirmemail"
                                  placeholder="Enter Email"
                                  onChange={this.onChange}
                                  value={firstrefconfirmemail}
                                  required
                                  validate={{ match: { value: "firstrefemail" } }}
                                />
                              </Col>
                              <Col xs="12" md="12">
                                <strong className="an-heading">Address</strong>
                              </Col>
                              <Col xs="12" md="12">
                                <AvField type="text" label="Street Address" name="firstrefstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={firstrefstreetAddress} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="City" name="firstrefcity" placeholder="Enter City.." onChange={this.onChange} value={firstrefcity} required />
                              </Col>

                              <Col xs="12" md="4">
                                <AvField type="text" label="County / State / Region" name="firstrefstate" placeholder="Enter State.." onChange={this.onChange} value={firstrefstate} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="ZIP / Postal Code" name="firstrefzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={firstrefzip} required />
                              </Col>
                            </div>
                          </Row>

                          <Row>
                            <div className="cus-design edited-section">
                              <Col xs="12" md="12">
                                <p className="h5">Reference 2</p>
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="Relationship" name="secondrefrelationship" placeholder="Enter Relationship .." onChange={this.onChange} value={secondrefrelationship} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="First Name" name="secondreffirstname" placeholder="Enter First Name.." onChange={this.onChange} value={secondreffirstname} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="Last Name" name="secondreflastname" placeholder="Enter Last Name.." onChange={this.onChange} value={secondreflastname} required />
                              </Col>

                              <Col xs="12" md="4">
                                <Label className={this.state.phoneerror3 ? "error-color" : null}>Mobile No</Label>
                                <IntlTelInput
                                  style={{ width: "100%" }}
                                  defaultCountry={secondrefphone && secondrefphone.dialcountry}
                                  utilsScript={libphonenumber}
                                  css={this.state.phoneerror3 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                  onPhoneNumberChange={this.handler3}
                                  value={secondrefphone && secondrefphone.number}
                                  required
                                />
                                {this.state.phoneerror3 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                {/* <AvField type="number" label="Phone" name="secondrefphone" placeholder="Enter  Phone.." onChange={this.onChange} value={this.state.secondrefphone} required /> */}
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="email" label="Email" name="secondrefemail" placeholder="Enter Email.." onChange={this.onChange} value={secondrefemail} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField
                                  type="email"
                                  label="Confirm Email"
                                  name="secondrefconfirmemail"
                                  placeholder="Enter Email.."
                                  onChange={this.onChange}
                                  value={secondrefconfirmemail}
                                  required
                                  validate={{ match: { value: "secondrefemail" } }}
                                />
                              </Col>
                              <Col xs="12" md="12">
                                <strong className="an-heading"> Address </strong>
                              </Col>
                              <Col xs="12" md="12">
                                <AvField type="text" label="Street Address" name="secondrefstreetAddress" placeholder="Enter Street Address.." onChange={this.onChange} value={secondrefstreetAddress} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="City" name="secondrefcity" placeholder="Enter City.." onChange={this.onChange} value={secondrefcity} required />
                              </Col>

                              <Col xs="12" md="4">
                                <AvField type="text" label="County / State / Region" name="secondrefstate" placeholder="Enter State.." onChange={this.onChange} value={secondrefstate} required />
                              </Col>
                              <Col xs="12" md="4">
                                <AvField type="text" label="ZIP / Postal Code" name="secondrefzip" placeholder="Enter ZIP / Postal Code.." onChange={this.onChange} value={secondrefzip} required />
                              </Col>
                            </div>
                          </Row>
                          <Row className="d-flex justify-content-end">
                            <Button color="success" className="mr-3" type="submit">
                              Update
                            </Button>
                          </Row>
                        </AvForm>
                        <hr />
                        <Row>
                          <Col xs="12" md="12">
                            <h4>Reference 1</h4>
                          </Col>
                        </Row>
                        {form_status >= 6 && form_status < 8 ? (
                          <Row className="d-flex justify-content-end">
                            {reference_details1 && reference_details1.status === 0 ? (
                              <Col xs="12" md="2">
                                <Button color="success" type="submit" onClick={() => this.MailModalOpen("refrence1")}>
                                  Send Form
                                </Button>
                              </Col>
                            ) : null}
                            {reference_details1 && reference_details1.status >= 1 ? (
                              <Col xs="12" md="2">
                                <Button color="success" type="submit" onClick={() => this.MailModalOpen("refrence1")}>
                                  Resend Form
                                </Button>
                              </Col>
                            ) : null}
                            {reference_details1 && reference_details1.status >= 2 ? (
                              <Col xs="12" md="2">
                                <Button color="info" type="button" onClick={() => this.DownloadRefereeForm("reference1")}>
                                  Download Form
                                </Button>
                              </Col>
                            ) : null}
                          </Row>
                        ) : null}
                        {reference_details1.status > 1 ? (
                          <div className="tog-refern">
                            <Button color="primary" block onClick={() => this.refereeCollapse("referee1")}>
                              Referee Form 1{" "}
                              <span className="pull-right">
                                {" "}
                                <i className="fa fa-caret-down" />{" "}
                              </span>
                            </Button>
                            <Collapse isOpen={this.state.refereecollapse1}>
                              <AvForm className="differ-colors" ref={clear => (this.form = clear)} onSubmit={this.RefereeSubmit}>
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>Referee Details </h4>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereefirstname" label="First name" value={refereefirstname} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereelastname" label="Last name" value={refereelastname} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="email" name="refereeemail" label="Email" value={refereeemail} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <Label className={this.state.phoneerror4 ? "error-color" : null}>Phone</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={refereephone && refereephone.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror4 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler4}
                                      value={refereephone && refereephone.number}
                                      required
                                    />
                                    {this.state.phoneerror4 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" name="refereephone" label="Phone" value={this.state.refereephone} required /> */}
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereejobtitle" label="Job Title" value={refereejobtitle} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereecompany" label="Company" value={refereecompany} required />
                                  </Col>
                                </Row>{" "}
                                <Row>
                                  <Col xs="12" md="12">
                                    <AvField type="textarea" name="refereecapacity" label="In what capacity did you know the applicant?:" value={refereecapacity} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>Applicant Details</h4>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantfirstname" label="First name" value={applicantfirstname} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantlastname" label="Last name" value={applicantlastname} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="email" name="applicantemail" label="Email" value={applicantemail} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <Label className={this.state.phoneerror6 ? "error-color" : null}>Phone</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={applicantphone && applicantphone.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror6 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler6}
                                      value={applicantphone && applicantphone.number}
                                      required
                                    />
                                    {this.state.phoneerror6 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" name="applicantphone" label="Phone" value={this.state.applicantphone} required /> */}
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantjobtitle" label="Job Title" value={applicantjobtitle} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantcompany" label="Company" value={applicantcompany} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <label>Date Employed from:</label>
                                    <SingleDatePicker
                                      // className={this.state.finaldateError ? "error-color" : null}
                                      date={moment(this.state.employedfrom)}
                                      onDateChange={employedfrom => this.setState({ employedfrom })}
                                      focused={this.state.focused1}
                                      onFocusChange={({ focused: focused1 }) => this.setState({ focused1 })}
                                      id="employedfrom"
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={() => false}
                                    />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <label>Date Employed to:</label>
                                    <SingleDatePicker
                                      // className={this.state.finaldateError ? "error-color" : null}
                                      date={moment(this.state.employedto)}
                                      onDateChange={employedto => this.setState({ employedto })}
                                      focused={this.state.focused2}
                                      onFocusChange={({ focused: focused2 }) => this.setState({ focused2 })}
                                      id="employedto"
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={() => false}
                                    />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="remployee" label="Would you re-employ this person?" value={remployee} required>
                                      <AvRadio label="Yes" value="yes" onClick={this.remployeeyes} />
                                      <AvRadio label="No" value="no" onClick={() => this.remployeeyes("no")} />
                                    </AvRadioGroup>
                                  </Col>
                                </Row>
                                {this.state.remployeeyes ? (
                                  <Row>
                                    <Col xs="12" md="12">
                                      <AvField type="textarea" label="Please explain why you would not re-employ them." name="remployeenotes" value={remployeenotes || ""} placeholder="" onChange={this.onChange} />
                                    </Col>
                                  </Row>
                                ) : null}
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>SECTION 1 - HOW WOULD YOU ASSESS THE FOLLOWING?</h4>
                                    <h6>Please tick the relevant boxes Excellent, Good, Average, Poor</h6>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="followcareplans" label="Ability to follow care plans" value={followcareplans} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="reliability" label="Reliability, timekeeping,attendance" value={reliability} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="character" label="Character" value={character} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="attitude" label="Attitude" value={attitude} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="dignity" label="Ability to ensure dignity is upheld" value={dignity} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="communication" label="Communication" value={communication} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="relationships" label="Relationships with colleagues" value={relationships} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="workunderinitiative" label="Ability to work under own initiative" value={workunderinitiative} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>SECTION - 2 Please answer the following questions</h4>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="disciplinaryaction" label="Has the applicant been subject to any disciplinary action?" value={disciplinaryaction} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.disciplinaryactionyes("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.disciplinaryactionyes} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.disciplinaryactionyes ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="disciplinaryactiondetails" value={disciplinaryactiondetails || ""} placeholder="" />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="investigations" label="Are you aware of the applicants involvement in any safeguarding investigations previous or current ." value={investigations} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.investigationsyes("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.investigationsyes} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.investigationsyes ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="investigationsdetails" value={investigationsdetails || ""} placeholder="" />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="vulnerablepeople" label="Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?" value={vulnerablepeople} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.vulnerablepeopleyes("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.vulnerablepeopleyes} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.vulnerablepeopleyes ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="vulnerablepeopledetails" value={vulnerablepeopledetails || ""} placeholder="" />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="6">
                                    <AvRadioGroup
                                      inline
                                      name="criminaloffences"
                                      label="To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?"
                                      value={criminaloffences === "yes" ? "yes" : "no"}
                                      required
                                    >
                                      <AvRadio label="Yes" value="yes" onClick={() => this.criminaloffenceyes("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.criminaloffenceyes} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.criminaloffenceyes ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="criminaloffencedetails" value={criminaloffencedetails || ""} placeholder="" />
                                    </Col>
                                  ) : null}
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <AvField type="textarea" label="Additional Comments" name="additionalcomments" value={additionalcomments} placeholder="" />
                                  </Col>
                                </Row>
                                {/* <Row>
                              <Col xs="12" md="12">
                                <h4>PLEASE CONFIRM:</h4>
                                <p>
                                  I can confirm that all the details provided are accurate at the time that this reference was completed. I can confirm that I am authorised to provide a reference on behalf of my organisation. I understand
                                  this reference may be shown to a third party for auditing purposes and I can confirm that Local Care Force has this organisation s consent and authorisation to disclose the contents of this reference to its
                                  end user, hirer clients. I understand that the applicant has the legal right to request a copy of their reference.
                                </p>
                              </Col>
                            </Row>
                            <Row>
                              <Col xs="12" md="12">
                               <AvInput type="checkbox" name="confirm" onChange={this.onChange} checked={confirm} required/>  Check to confirm
                              </Col>
                            </Row>*/}
                                <Row>
                                  <Col xs="12" md="4">
                                    <Label for="exampleCustomFileBrowser">Official company stamp.</Label>
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser"
                                      name="campanystamp"
                                      onChange={this.fileChangedHandlerCN1}
                                      label={this.state.campanystampName ? this.state.campanystampName : "Upload  Image"}
                                      encType="multipart/form-data"
                                    />
                                  </Col>
                                  {campanystamp && (
                                    <Col xs="12" md="12">
                                      <Button color="success" onClick={() => this.FileDownloader(campanystamp, "CompanyStamp.jpg")}>
                                        Download
                                      </Button>
                                    </Col>
                                  )}
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <p>If you are not using an official company email please upload a picture of your company stamp / business card/letterhead/compliment slip.</p>
                                  </Col>
                                </Row>
                                {/* <Row>
                              <Col xs="12" md="12">
                                  <AvInput type="checkbox" name="agree" onChange={this.onChange} checked={this.state.agree} required/>  By using this form you agree with the storage and handling of your data by this website as defined in our Privacy Policy.
                              </Col>
                            </Row>*/}
                                <Row className="d-flex justify-content-end">
                                  <Col xs="12" md="2">
                                    <button type="submit" title="Click here to Register" className="btn btn-primary btn-lg btn-block site-btn--accent form__submit mb-4">
                                      Update
                                    </button>
                                  </Col>
                                </Row>
                              </AvForm>
                            </Collapse>
                          </div>
                        ) : null}
                        <Row>
                          <Col xs="12" md="12">
                            <h4>Reference 2</h4>
                          </Col>
                        </Row>
                        {form_status >= 6 && form_status < 8 ? (
                          <Row className="d-flex justify-content-end">
                            {reference_details2.status === 0 ? (
                              <Col xs="12" md="2">
                                <Button color="success" type="submit" onClick={() => this.MailModalOpen("refrence2")}>
                                  Send Form
                                </Button>
                              </Col>
                            ) : null}
                            {reference_details2.status >= 1 ? (
                              <Col xs="12" md="2">
                                <Button color="success" type="submit" onClick={() => this.MailModalOpen("refrence2")}>
                                  Resend Form
                                </Button>
                              </Col>
                            ) : null}
                            {reference_details2 && reference_details2.status >= 2 ? (
                              <Col xs="12" md="2">
                                <Button color="info" type="button" onClick={() => this.DownloadRefereeForm("reference2")}>
                                  Download Form
                                </Button>
                              </Col>
                            ) : null}
                          </Row>
                        ) : null}
                        {reference_details2.status > 1 ? (
                          <div className="tog-refern">
                            <Button color="primary" block onClick={() => this.refereeCollapse("referee2")}>
                              Referee Form 2{" "}
                              <span className="pull-right">
                                {" "}
                                <i className="fa fa-caret-down" />{" "}
                              </span>
                            </Button>
                            <Collapse isOpen={this.state.refereecollapse2}>
                              <AvForm className="differ-colors" ref={clear => (this.form = clear)} onSubmit={this.RefereeSubmit2}>
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>Referee Details</h4>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereefirstname2" label="First name" value={refereefirstname2} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereelastname2" label="Last name" value={refereelastname2} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="email" name="refereeemail2" label="Email" value={refereeemail2} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <Label className={this.state.phoneerror5 ? "error-color" : null}>Phone</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={refereephone2 && refereephone2.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror5 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler5}
                                      value={refereephone2 && refereephone2.number}
                                      required
                                    />
                                    {this.state.phoneerror5 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" name="refereephone2" label="Phone" required /> */}
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereejobtitle2" label="Job Title" value={refereejobtitle2} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="refereecompany2" label="Company" value={refereecompany2} required />
                                  </Col>
                                </Row>{" "}
                                <Row>
                                  <Col xs="12" md="12">
                                    <AvField type="textarea" name="refereecapacity2" label="In what capacity did you know the applicant?:" value={refereecapacity2} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>Applicant Details</h4>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantfirstname2" label="First name" value={applicantfirstname2} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantlastname2" label="Last name" value={applicantlastname2} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="email" name="applicantemail2" label="Email" value={applicantemail2} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <Label className={this.state.phoneerror7 ? "error-color" : null}>Phone</Label>
                                    <IntlTelInput
                                      style={{ width: "100%" }}
                                      defaultCountry={applicantphone2 && applicantphone2.dialcountry}
                                      utilsScript={libphonenumber}
                                      css={this.state.phoneerror7 ? ["intl-tel-input", "form-control", "error-color"] : ["intl-tel-input", "form-control"]}
                                      onPhoneNumberChange={this.handler7}
                                      value={applicantphone2 && applicantphone2.number}
                                      required
                                    />
                                    {this.state.phoneerror7 ? <div className={"error-color"}>Enter Valid Phone Number!</div> : null}
                                    {/* <AvField type="number" name="applicantphone2" label="Phone" required /> */}
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantjobtitle2" label="Job Title" value={applicantjobtitle2} required />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvField type="text" name="applicantcompany2" label="Company" value={applicantcompany2} required />
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <label>Date Employed from:</label>
                                    <SingleDatePicker
                                      // className={this.state.finaldateError ? "error-color" : null}
                                      date={moment(this.state.employedfrom2)}
                                      onDateChange={employedfrom2 => this.setState({ employedfrom2 })}
                                      focused={this.state.focused11}
                                      onFocusChange={({ focused: focused11 }) => this.setState({ focused11 })}
                                      id="employedfrom2"
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={() => false}
                                    />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <label>Date Employed to:</label>
                                    <SingleDatePicker
                                      // className={this.state.finaldateError ? "error-color" : null}
                                      date={moment(this.state.employedto2)}
                                      onDateChange={employedto2 => this.setState({ employedto2 })}
                                      focused={this.state.focused21}
                                      onFocusChange={({ focused: focused21 }) => this.setState({ focused21 })}
                                      id="employedto2"
                                      displayFormat="DD-MM-YYYY"
                                      isOutsideRange={() => false}
                                    />
                                  </Col>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="remployee2" label="Would you re-employ this person?" value={remployee2} required>
                                      <AvRadio label="Yes" value="yes" onClick={this.remployeeyes2} />
                                      <AvRadio label="No" value="no" onClick={() => this.remployeeyes2("no")} />
                                    </AvRadioGroup>
                                  </Col>
                                </Row>
                                {this.state.remployeeyes2 ? (
                                  <Row>
                                    <Col xs="12" md="12">
                                      <AvField type="textarea" label="Please explain why you would not re-employ them." name="remployeenotes2" value={remployeenotes2 || ""} placeholder="" onChange={this.onChange} />
                                    </Col>
                                  </Row>
                                ) : null}
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>SECTION 1 - HOW WOULD YOU ASSESS THE FOLLOWING?</h4>
                                    <h6>Please tick the relevant boxes Excellent, Good, Average, Poor</h6>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="followcareplans2" label="Ability to follow care plans" value={followcareplans2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="reliability2" label="Reliability, timekeeping,attendance" value={reliability2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="character2" label="Character" value={character2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="attitude2" label="Attitude" value={attitude2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="dignity2" label="Ability to ensure dignity is upheld" value={dignity2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="communication2" label="Communication" value={communication2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="relationships2" label="Relationships with colleagues" value={relationships2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                  <Col xs="12" md="4">
                                    <AvRadioGroup inline name="workunderinitiative2" label="Ability to work under own initiative" value={workunderinitiative2} required>
                                      <AvRadio label="Excellent" value="excellent" />
                                      <AvRadio label="Good" value="good" />
                                      <AvRadio label="Average" value="average" />
                                      <AvRadio label="Poor" value="poor" />
                                    </AvRadioGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <h4>SECTION - 2 Please answer the following questions</h4>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="disciplinaryaction2" label="Has the applicant been subject to any disciplinary action?" value={disciplinaryaction2} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.disciplinaryactionyes2("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.disciplinaryactionyes2} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.disciplinaryactionyes2 ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="disciplinaryactiondetails2" placeholder="" value={disciplinaryactiondetails2 || ""} />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="investigations2" label="Are you aware of the applicants involvement in any safeguarding investigations previous or current ." value={investigations2} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.investigationsyes2("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.investigationsyes2} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.investigationsyes2 ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="investigationsdetails2" placeholder="" value={investigationsdetails2 || ""} />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="6">
                                    <AvRadioGroup inline name="vulnerablepeople2" label="Are you aware of any reasons why the applicant should not be employed to work with children or vulnerable people?" value={vulnerablepeople2} required>
                                      <AvRadio label="Yes" value="yes" onClick={() => this.vulnerablepeopleyes2("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.vulnerablepeopleyes2} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.vulnerablepeopleyes2 ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="vulnerablepeopledetails2" placeholder="" value={vulnerablepeopledetails2 || ""} />
                                    </Col>
                                  ) : null}
                                  <Col xs="12" md="6">
                                    <AvRadioGroup
                                      inline
                                      name="criminaloffence2"
                                      label="To the best of your knowledge, has the applicant been convicted or cautioned of a criminal offence?"
                                      value={criminaloffence2 === "yes" ? "yes" : "no"}
                                      required
                                    >
                                      <AvRadio label="Yes" value="yes" onClick={() => this.criminaloffenceyes2("yes")} />
                                      <AvRadio label="No" value="no" onClick={this.criminaloffenceyes2} />
                                    </AvRadioGroup>
                                  </Col>
                                  {this.state.criminaloffenceyes2 ? (
                                    <Col xs="12" md="6">
                                      <AvField type="textarea" label="Please provide details:" name="criminaloffencedetails2" value={criminaloffencedetails2 || ""} placeholder="" />
                                    </Col>
                                  ) : null}
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <AvField type="textarea" label="Additional Comments" name="additionalcomments2" value={additionalcomments2} placeholder="" />
                                  </Col>
                                </Row>
                                {/* <Row>
                              <Col xs="12" md="12">

                                <h4>PLEASE CONFIRM:</h4>
                                <p>
                                  I can confirm that all the details provided are accurate at the time that this reference was completed. I can confirm that I am authorised to provide a reference on behalf of my organisation. I understand
                                  this reference may be shown to a third party for auditing purposes and I can confirm that Local Care Force has this organisation s consent and authorisation to disclose the contents of this reference to its
                                  end user, hirer clients. I understand that the applicant has the legal right to request a copy of their reference.
                                </p>
                              </Col>
                            </Row>
                            <div className="form__form-group form__form-group--read-and-agree mar-ajd">
                              <label className="checkbox-btn">
                                <AvGroup>
                                  <Label check for="confirm" />
                                  <AvInput className=" checkbox-btn__checkbox" type="checkbox" name="confirm2" value={this.state.confirm2} checked={this.state.confirm2} required />
                                  <span className="checkbox-btn__checkbox-custom mt-3">
                                    <i className="mdi mdi-check" />
                                  </span>
                                  <span className="checkbox-btn__label ">Check to confirm</span>
                                  <AvFeedback>This is required!</AvFeedback>
                                </AvGroup>
                              </label>
                            </div>*/}
                                <Row>
                                  <Col xs="12" md="4">
                                    <Label for="exampleCustomFileBrowser2">Official company stamp.</Label>
                                    <CustomInput
                                      type="file"
                                      id="exampleCustomFileBrowser2"
                                      name="campanystamp2"
                                      onChange={this.fileChangedHandlerCN2}
                                      label={this.state.campanystampName2 ? this.state.campanystampName2 : "Upload  Image"}
                                      encType="multipart/form-data"
                                    />
                                  </Col>
                                  {campanystamp2 && (
                                    <Col xs="12" md="12">
                                      <Button color="success" onClick={() => this.FileDownloader(campanystamp2, "CompanyStamp.jpg")}>
                                        Download
                                      </Button>
                                    </Col>
                                  )}
                                </Row>
                                <Row>
                                  <Col xs="12" md="12">
                                    <p>If you are not using an official company email please upload a picture of your company stamp / business card/letterhead/compliment slip.</p>
                                  </Col>
                                </Row>
                                {/* <Row>
                              <Col xs="12" md="12">
                                <div className="form__form-group form__form-group--read-and-agree mar-ajd">
                                  <label className="checkbox-btn">
                                    <AvGroup>
                                      <Label check for="agree" />
                                      <AvInput className="checkbox-btn__checkbox" type="checkbox" name="agree2" required value={this.state.agree2} checked={this.state.agree2} />

                                      <span className="checkbox-btn__checkbox-custom mt-3">
                                        <i className="mdi mdi-check" />
                                      </span>
                                      <span className="checkbox-btn__label">By using this form you agree with the storage and handling of your data by this website as defined in our Privacy Policy.</span>
                                      <AvFeedback>This is required!</AvFeedback>
                                    </AvGroup>
                                  </label>
                                </div>
                              </Col>
                            </Row>*/}
                                <Row className="d-flex justify-content-end">
                                  <Col xs="12" md="2">
                                    <button type="submit" title="Click here to Register" className="btn btn-primary btn-lg btn-block site-btn--accent form__submit mb-4">
                                      Update
                                    </button>
                                  </Col>
                                </Row>
                              </AvForm>
                            </Collapse>
                          </div>
                        ) : null}
                        <Row>
                          {form_status >= 7 ? (
                            <>
                              {reference_details1 && reference_details1.status > 1 ? (
                                <Col xs="12" md="6">
                                  <div className="pro-interview ref-gra1">
                                    <Col xs="12" md="12">
                                      <h4>
                                        <span className="lf-path">
                                          <i className="fa fa-file-text-o" aria-hidden="true" />{" "}
                                        </span>{" "}
                                        <span className="rg-path"> Reference 1 Verification </span>
                                      </h4>
                                    </Col>
                                    {form_status >= 7 ? (
                                      <div className="interview-buttons">
                                        <>
                                          {form_status < 8 ? (
                                            <Button className="int-views" type="button" onClick={() => this.DetailsFormModal("refverify1")}>
                                              Send Reference Verification Status
                                            </Button>
                                          ) : null}
                                          {reference_details1.verified_status ? <Badge className="int-views">Reference Verification Completed</Badge> : null}
                                          {reference_details1.verified_status ? (
                                            <>
                                              {reference_details1.verified_status === 1 ? <Badge className="int-qua">Qualified</Badge> : null}
                                              {reference_details1.verified_status === 0 ? <Badge className="int-dan">Disqualified</Badge> : null}
                                            </>
                                          ) : null}
                                        </>
                                      </div>
                                    ) : null}
                                  </div>
                                </Col>
                              ) : null}
                              {reference_details2 && reference_details2.status > 1 ? (
                                <Col xs="12" md="6">
                                  <div className="pro-interview ref-gra2">
                                    <Col xs="12" md="12">
                                      <h4>
                                        <span className="lf-path">
                                          <i className="fa fa-file-text-o" aria-hidden="true" />{" "}
                                        </span>{" "}
                                        <span className="rg-path"> Reference 2 Verification </span>
                                      </h4>
                                    </Col>
                                    <div className="interview-buttons">
                                      {form_status >= 7 ? (
                                        <>
                                          {form_status < 8 ? (
                                            <Button className="int-views" type="button" onClick={() => this.DetailsFormModal("refverify2")}>
                                              Send Reference Verification Status
                                            </Button>
                                          ) : null}
                                          {reference_details2.verified_status ? <Badge className="int-views">Reference Verification Completed</Badge> : null}
                                          {reference_details2.verified_status ? (
                                            <>
                                              {reference_details2.verified_status === 1 ? <Badge className="int-qua">Qualified</Badge> : null}
                                              {reference_details2.verified_status === 0 ? <Badge className="int-dan">Disqualified</Badge> : null}
                                            </>
                                          ) : null}
                                        </>
                                      ) : null}
                                    </div>
                                  </div>
                                </Col>
                              ) : null}
                            </>
                          ) : null}
                        </Row>
                      </TabPane>
                    </>
                  ) : null}
                  {path_name === Canditates_URL || path_name === Summary_URL ? (
                    <>
                      <TabPane tabId="8">
                        <AvForm ref={clear => (this.form = clear)} onSubmit={this.FinalVerification}>
                          <Row className="qual-cals refer-colors">
                            <div className="cus-design edited-section">
                              <Col xs="12" md="12">
                                <p className="h5">Final Verification </p>
                              </Col>
                              <Col xs="12" md="6">
                                <label>Date</label>
                                <SingleDatePicker
                                  date={moment(this.state.final_verify_date)}
                                  onDateChange={final_verify_date => this.setState({ final_verify_date })}
                                  focused={this.state.focused91}
                                  onFocusChange={({ focused: focused91 }) => this.setState({ focused91 })}
                                  id="final_verify_date"
                                  displayFormat="DD-MM-YYYY"
                                  isOutsideRange={() => false}
                                />

                                <AvField type="textarea" label="Notes" name="final_verify_notes" placeholder="Enter Notes" onChange={this.onChange} value={final_verify_notes} />

                                <div>
                                  <label>Status</label>
                                </div>
                                <Label className="switch switch-text switch-success new-switch">
                                  <Input
                                    type="checkbox"
                                    className="switch-input"
                                    checked={this.state.final_verify_status}
                                    onChange={e => {
                                      this.setState({ final_verify_status: e.target.checked });
                                    }}
                                  />
                                  <span className="switch-label" data-on="Qualified" data-off="Disqualified" />
                                  <span className="switch-handle new-handle" />
                                </Label>
                              </Col>
                              <Col xs="12" md="6">
                                {form_status >= 8 ? (
                                  <>
                                    {form_status < 9 ? (
                                      <Button ClassName="ver-cont" type="submit">
                                        Save & Continue
                                      </Button>
                                    ) : null}
                                    {form_status === 9 ? <Badge className="final-verification">Final Verification Completed</Badge> : null}
                                    {final_verify_status ? (
                                      <>
                                        {final_verify_status === 1 ? <Badge className="information-qualifies">Qualified</Badge> : null}
                                        {final_verify_status === 0 ? <Badge className="information-disqualifies">Disqualified</Badge> : null}
                                      </>
                                    ) : null}
                                  </>
                                ) : null}
                              </Col>
                            </div>
                          </Row>
                        </AvForm>
                        <h4>Summary</h4>
                        <div className="table-responsive mt-2">
                          <Table hover bordered responsive>
                            <thead>
                              <tr>
                                <th>Context</th>
                                <th>Date</th>
                                <th>Notes</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Skill Check</td>
                                <td>{moment(skillcheckdate).format("DD-MM-YYYY")}</td>
                                <td>{skillchecknotes}</td>
                                <td>{skillcheckstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                <td>DBS Check</td>
                                <td>{moment(dbscheckdate).format("DD-MM-YYYY")}</td>
                                <td>{dbschecknotes}</td>
                                <td>{dbscheckstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                <td>Health Check</td>
                                <td>{moment(healthcheckdate).format("DD-MM-YYYY")}</td>
                                <td>{healthchecknotes}</td>
                                <td>{healthcheckstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                <td>Interview</td>
                                <td>{moment(intervieweddate).format("DD-MM-YYYY")}</td>
                                <td>{interviewednotes}</td>
                                <td>{interviewedstatus === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                <td>Reference 1</td>
                                <td>{moment(referenceverifydate).format("DD-MM-YYYY")}</td>
                                <td>{referenceverifynotes}</td>
                                <td>{reference_details1 && reference_details1.verified_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                <td>Reference 2</td>
                                <td>{moment(referenceverifydate2).format("DD-MM-YYYY")}</td>
                                <td>{referenceverifynotes2}</td>
                                <td>{reference_details2 && reference_details2.verified_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                              <tr>
                                <td>
                                  <h5>Final Verification</h5>
                                </td>
                                <td>{moment(final_verify_date).format("DD-MM-YYYY")}</td>
                                <td>{final_verify_notes}</td>
                                <td>{final_verify_status === 1 ? <Badge color="success">Qualified</Badge> : <Badge color="danger">Disqualified</Badge>}</td>
                              </tr>
                            </tbody>
                          </Table>
                        </div>
                      </TabPane>
                    </>
                  ) : null}
                </TabContent>
              </CardBody>
              <CardFooter>
                <Link to="/agency/recruitment/candidates">
                  {this.state.activeTab === "1" ? (
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  ) : null}
                </Link>
                {this.state.activeTab > "1" ? (
                  <>
                    {(this.state.activeTab === "8" || this.state.activeTab === "7" || this.state.activeTab === "6" || this.state.activeTab === "4") &&
                    (path_name === Interview_URL || path_name === Files_URL || path_name === Summary_URL || path_name === Referee_URL) ? null : (
                      <Button
                        type="submit"
                        color="secondary"
                        title="Back"
                        onClick={() => {
                          this.setState({
                            activeTab: (Number(this.state.activeTab) - 1).toString()
                          });
                          window.scrollTo(0, 0);
                        }}
                      >
                        <i className="fa fa-arrow-left" /> previous
                      </Button>
                    )}
                  </>
                ) : null}
                {this.state.activeTab < "8" && (path_name !== Interview_URL || path_name !== Application_URL || path_name !== Files_URL || path_name !== Summary_URL) ? (
                  <>
                    {(this.state.activeTab === "7" || this.state.activeTab === "6" || this.state.activeTab === "5" || this.state.activeTab === "4") &&
                    (path_name === Interview_URL || path_name === Files_URL || path_name === Summary_URL || path_name === Referee_URL) ? null : (
                      <>
                        {this.state.activeTab === "5" && path_name === Application_URL ? null : (
                          <Button
                            type="submit"
                            color="info ne-movers"
                            title="Next"
                            onClick={() => {
                              this.setState({
                                activeTab: (Number(this.state.activeTab) + 1).toString()
                              });
                              window.scrollTo(0, 0);
                            }}
                          >
                            Next <i className="fa fa-arrow-right" />
                          </Button>
                        )}
                      </>
                    )}
                  </>
                ) : null}
                {this.state.activeTab === "5" && form_status < 2 ? (
                  <Button color="success"  className="mr-3 pull-right" onClick={this.ApplicationFormApproval}>
                    Approve Application Form
                  </Button>
                ) : null}
              </CardFooter>
            </Card>
          </Col>
        </Row>
        <Modal isOpen={this.state.mailmodal} className="modal-lg">
          <Fragment>
            <ModalHeader toggle={this.MailModalOpen}>{referencefor === "interviewcall" ? "Interview Call" : referencefor === "postinterviewcall" ? "Post Interview" : "Referee Form"}</ModalHeader>
            <AvForm
              onValidSubmit={
                referencefor === "interviewcall"
                  ? this.MailInterviewCall
                  : referencefor === "postinterviewcall"
                    ? this.PostInterview
                    : referencefor === "verificationstatus"
                      ? this.VerificationStatus
                      : referencefor === "refverify1"
                        ? this.ReferenceStatus
                        : referencefor === "refverify2"
                          ? this.ReferenceStatus
                          : referencefor === "finalverify"
                            ? this.FinalStatus
                            : this.SendRefereeForm
              }
            >
              <ModalBody>
                <Row>
                  {referencefor === "interviewcall" || referencefor === "postinterviewcall" || referencefor === "verificationstatus" || referencefor === "refverify1" || referencefor === "refverify2" || referencefor === "finalverify" ? (
                    <Col xs="12" md="6">
                      <AvField name="employee_email" onChange={this.onChange} value={employee_email} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                    </Col>
                  ) : null}
                  {referencefor !== "interviewcall" && referencefor !== "postinterviewcall" && referencefor !== "verificationstatus" && referencefor !== "refverify1" && referencefor !== "refverify2" && referencefor !== "finalverify" ? (
                    <Col xs="12" md="6">
                      <AvField name="refe_email" onChange={this.onChange} value={refe_email} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                    </Col>
                  ) : null}
                  <Col xs="12" md="6">
                    <AvField type="select" name="typeid" label="Email Template" onChange={this.onChangeSelect} value={typeid} required>
                      <option>Select</option>
                      {emailtemplatelist && emailtemplatelist.length > 0 ? (
                        <>
                          {emailtemplatelist.map((list, i) => (
                            <option key={i} value={list._id}>
                              {list.heading}
                            </option>
                          ))}
                        </>
                      ) : null}
                    </AvField>
                  </Col>
                  <Col xs="12" md="12">
                    <AvField name="email_subject" onChange={this.onChange} value={email_subject} placeholder="Subject" label="Subject" type="text" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="12">
                    <p>Message</p>
                    <Editor
                      apiKey={Settings.tinymceKey}
                      initialValue={email_content}
                      init={{
                        plugins: "link image code",
                        toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                        height: 480
                      }}
                      onChange={this.handleEditorChange}
                      value={email_content}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between modal-gal">
                <div>
                  <Button color="secondary" type="button" onClick={this.MailModalOpen} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                </div>
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  <i className="fa fa-envelope-open-o" />
                </Button>{" "}
                <UncontrolledTooltip placement="left" target="savecontinue2">
                  Send Mail
                </UncontrolledTooltip>
              </ModalFooter>
            </AvForm>
          </Fragment>
        </Modal>
        <Modal isOpen={this.state.compliancemodal} className="modal-lg">
          <Fragment>
            <ModalHeader toggle={this.ComplianceModalOpen}>Compliance</ModalHeader>
            <AvForm onValidSubmit={this.ComplianceForm}>
              <ModalBody>
                <Row>
                  <Col xs="12" md="6">
                    <AvField name="employee_email" onChange={this.onChange} value={employee_email} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  <Col xs="12" md="6">
                    <AvField type="select" name="typeid" label="Email Template" onChange={this.onChangeSelect} value={typeid} required>
                      <option>Select</option>
                      {emailtemplatelist && emailtemplatelist.length > 0 ? (
                        <>
                          {emailtemplatelist.map((list, i) => (
                            <option key={i} value={list._id}>
                              {list.heading}
                            </option>
                          ))}
                        </>
                      ) : null}
                    </AvField>
                  </Col>
                  <Col xs="12" md="12">
                    <AvField name="email_subject" onChange={this.onChange} value={email_subject} placeholder="Subject" label="Subject" type="text" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  {form_status < 2 ? (
                    <Col xs="12" md="12">
                      <AvInput tag={CustomInput} type="checkbox" name="edit_form" onChange={this.onChange} value={edit_form} label="Allow candidate to edit online application form" />
                    </Col>
                  ) : null}
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="12">
                    <Label>Message</Label>
                    <Editor
                      apiKey={Settings.tinymceKey}
                      initialValue={email_content}
                      init={{
                        plugins: "link image code",
                        toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                        height: 480
                      }}
                      onChange={this.handleEditorChange}
                      value={email_content}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between modal-gal">
                <div>
                  <Button color="secondary" type="button" onClick={this.ComplianceModalOpen} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                </div>
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  <i className="fa fa-envelope-open-o" />
                </Button>{" "}
                <UncontrolledTooltip placement="left" target="savecontinue2">
                  Send Mail
                </UncontrolledTooltip>
              </ModalFooter>
            </AvForm>
          </Fragment>
        </Modal>
        <Modal isOpen={this.state.applicationmodal} className="modal-lg">
          <Fragment>
            <ModalHeader toggle={this.ApplicationFormApproval}>Application Form Approval</ModalHeader>
            <AvForm onValidSubmit={this.ApplicationForm}>
              <ModalBody>
                <Row>
                  <Col xs="12" md="6">
                    <AvField name="employee_email" onChange={this.onChange} value={employee_email} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  <Col xs="12" md="6">
                    <AvField type="select" name="typeid" label="Email Template" onChange={this.onChangeSelect} value={typeid} required>
                      <option>Select</option>
                      {emailtemplatelist && emailtemplatelist.length > 0 ? (
                        <>
                          {emailtemplatelist.map((list, i) => (
                            <option key={i} value={list._id}>
                              {list.heading}
                            </option>
                          ))}
                        </>
                      ) : null}
                    </AvField>
                  </Col>
                  <Col xs="12" md="12">
                    <AvField name="email_subject" onChange={this.onChange} value={email_subject} placeholder="Subject" label="Subject" type="text" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  {form_status < 2 ? (
                    <Col xs="12" md="4">
                      <div>
                        <label>Approval</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input
                          type="checkbox"
                          className="switch-input"
                          checked={this.state.applicationcheckstatus}
                          onChange={e => {
                            this.setState({ applicationcheckstatus: e.target.checked });
                          }}
                          disabled={this.state.form_status > 1}
                        />
                        <span className="switch-label" data-on="Approved" data-off="Pending" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  ) : null}
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="12">
                    <Label>Message</Label>
                    <Editor
                      apiKey={Settings.tinymceKey}
                      initialValue={email_content}
                      init={{
                        plugins: "link image code",
                        toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                        height: 480
                      }}
                      onChange={this.handleEditorChange}
                      value={email_content}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between modal-gal">
                <div>
                  <Button color="secondary" type="button" onClick={this.ApplicationFormApproval} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                </div>
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  <i className="fa fa-envelope-open-o" />
                </Button>{" "}
                <UncontrolledTooltip placement="left" target="savecontinue2">
                  Send Mail
                </UncontrolledTooltip>
              </ModalFooter>
            </AvForm>
          </Fragment>
        </Modal>
        <Modal isOpen={this.state.detailsmodal}>
          <ModalHeader toggle={this.DetailsFormModal}>Details</ModalHeader>
          <AvForm onValidSubmit={referencefor === "refverify1" ? this.ReferenceVerification1 : referencefor === "refverify2" ? this.ReferenceVerification2 : this.InterviewDetailsForm}>
            <ModalBody>
              <>
                {referencefor === "interviewcall" ? (
                  <>
                    <Row>
                      <Col xs="12" md="4" className={"ml-2"}>
                        <label>Interview Date</label>
                        <SingleDatePicker
                          date={moment(this.state.interviewdate)}
                          onDateChange={interviewdate => this.setState({ interviewdate })}
                          focused={this.state.focused6}
                          onFocusChange={({ focused: focused6 }) => this.setState({ focused6 })}
                          id="interviewdate"
                          displayFormat="DD-MM-YYYY"
                          isOutsideRange={() => false}
                          disabled={this.state.form_status > 3}
                        />
                      </Col>
                      <Col xs="12" md="4">
                        <AvField type="time" label="Interview Time" name="interviewtime" placeholder="Enter Time" onChange={this.onChange} value={interviewtime} disabled={form_status > 3} />
                      </Col>
                    </Row>
                    <Col xs="12" md="12">
                      <AvField type="text" label="Video Call URL" name="videocallurl" placeholder="Enter URL" onChange={this.onChange} value={videocallurl} disabled={form_status > 3} />
                    </Col>
                    <Col xs="12" md="12">
                      <AvField type="textarea" label="Notes" name="interviewnotes" placeholder="Enter Notes" onChange={this.onChange} value={interviewnotes} disabled={form_status > 3} />
                    </Col>
                  </>
                ) : null}
                {referencefor === "postinterviewcall" ? (
                  <>
                    <Col xs="12" md="4">
                      <label>Interviewed Date</label>
                      <SingleDatePicker
                        date={moment(this.state.intervieweddate)}
                        onDateChange={intervieweddate => this.setState({ intervieweddate })}
                        focused={this.state.focused7}
                        onFocusChange={({ focused: focused7 }) => this.setState({ focused7 })}
                        id="intervieweddate"
                        displayFormat="DD-MM-YYYY"
                        isOutsideRange={() => false}
                        disabled={form_status > 3}
                      />
                    </Col>
                    <Col xs="12" md="8">
                      <AvField type="textarea" label="Notes" name="interviewednotes" placeholder="Enter Notes" onChange={this.onChange} value={interviewednotes} disabled={form_status > 3} />
                    </Col>
                    <Col xs="12" md="12">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input
                          type="checkbox"
                          className="switch-input"
                          checked={this.state.interviewedstatus}
                          onChange={e => {
                            this.setState({ interviewedstatus: e.target.checked });
                          }}
                          disabled={form_status > 3}
                        />
                        <span className="switch-label" data-on="Qualified" data-off="Disqualified" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </>
                ) : null}
                {console.log("status ", reference_details1.status)}
                {reference_details1 && reference_details1.status > 1 && referencefor === "refverify1" ? (
                  <>
                    <Col xs="12" md="12">
                      <label>Date</label>
                      <SingleDatePicker
                        date={moment(this.state.referenceverifydate)}
                        onDateChange={referenceverifydate => this.setState({ referenceverifydate })}
                        focused={this.state.focused8}
                        onFocusChange={({ focused: focused8 }) => this.setState({ focused8 })}
                        id="referenceverifydate"
                        displayFormat="DD-MM-YYYY"
                        isOutsideRange={() => false}
                        disabled={form_status > 7}
                      />
                    </Col>
                    <Col xs="12" md="12">
                      <AvField type="textarea" label="Notes" name="referenceverifynotes" placeholder="Enter Notes" onChange={this.onChange} value={form_reference1.verify_notes} disabled={form_status > 7} />
                    </Col>
                    <Col xs="12" md="12">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input
                          type="checkbox"
                          className="switch-input"
                          checked={this.state.referenceverifystatus}
                          onChange={e => {
                            this.setState({ referenceverifystatus: e.target.checked });
                          }}
                          disabled={form_status > 7}
                        />
                        <span className="switch-label" data-on="Qualified" data-off="Disqualified" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </>
                ) : null}
                {reference_details2 && reference_details2.status > 1 && referencefor === "refverify2" ? (
                  <>
                    <Col xs="12" md="12">
                      <label>Date</label>
                      <SingleDatePicker
                        date={moment(this.state.referenceverifydate2)}
                        onDateChange={referenceverifydate2 => this.setState({ referenceverifydate2 })}
                        focused={this.state.focused9}
                        onFocusChange={({ focused: focused9 }) => this.setState({ focused9 })}
                        id="referenceverifydate"
                        displayFormat="DD-MM-YYYY"
                        isOutsideRange={() => false}
                        disabled={form_status > 7}
                      />
                    </Col>
                    <Col xs="12" md="12">
                      <AvField type="textarea" label="Notes" name="referenceverifynotes2" placeholder="Enter Notes" onChange={this.onChange} value={form_reference2.verify_notes} disabled={form_status > 7} />
                    </Col>
                    <Col xs="12" md="12">
                      <div>
                        <label>Status</label>
                      </div>
                      <Label className="switch switch-text switch-success new-switch">
                        <Input
                          type="checkbox"
                          className="switch-input"
                          checked={this.state.referenceverifystatus2}
                          onChange={e => {
                            this.setState({ referenceverifystatus2: e.target.checked });
                          }}
                          disabled={form_status > 7}
                        />
                        <span className="switch-label" data-on="Qualified" data-off="Disqualified" />
                        <span className="switch-handle new-handle" />
                      </Label>
                    </Col>
                  </>
                ) : null}
              </>
            </ModalBody>
            <ModalFooter className="justify-content-between modal-gal">
              <div>
                <Button color="secondary" type="button" onClick={this.DetailsFormModal} title="Close">
                  <i className="fa fa-close" />
                </Button>
              </div>
              {referencefor === "interviewcall" || referencefor === "postinterviewcall" ? (
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  Save & Continue
                </Button>
              ) : null}
              {referencefor === "refverify1" ? (
                <>
                  {form_status >= 7 ? (
                    <>
                      {form_status < 8 ? (
                        <Button color="success" className="mr-3" type="submit">
                          Save & Continue
                        </Button>
                      ) : null}
                      {reference_details1.verified_status ? <Badge color="info">Reference Verification Completed</Badge> : null}
                      {reference_details1.verified_status ? (
                        <>
                          {reference_details1.verified_status === 1 ? <Badge color="info">Qualified</Badge> : null}
                          {reference_details1.verified_status === 0 ? <Badge color="danger">Disqualified</Badge> : null}
                        </>
                      ) : null}
                    </>
                  ) : null}
                </>
              ) : null}
              {referencefor === "refverify2" ? (
                <>
                  {form_status >= 7 ? (
                    <>
                      {form_status < 8 ? (
                        <Button color="success" className="mr-3" type="submit">
                          Save & Continue
                        </Button>
                      ) : null}
                      {reference_details2.verified_status ? <Badge color="info">Reference Verification Completed</Badge> : null}
                      {reference_details2.verified_status ? (
                        <>
                          {reference_details2.verified_status === 1 ? <Badge color="info">Qualified</Badge> : null}
                          {reference_details2.verified_status === 0 ? <Badge color="danger">Disqualified</Badge> : null}
                        </>
                      ) : null}
                    </>
                  ) : null}
                </>
              ) : null}
            </ModalFooter>
          </AvForm>
        </Modal>
        <Modal isOpen={this.state.checkmodal} className="modal-lg">
          <ModalHeader toggle={this.CheckFormModal}>Details</ModalHeader>
          <AvForm ref={clear => (this.form = clear)} onSubmit={this.VerificationSubmit}>
            <ModalBody>
              <Row>
                <Col xs="12" md="12">
                  <h4>Skills Check</h4>
                </Col>
                <Col xs="12" md="2">
                  <label>Date</label>
                  <SingleDatePicker
                    date={moment(this.state.skillcheckdate)}
                    onDateChange={skillcheckdate => this.setState({ skillcheckdate })}
                    focused={this.state.focused3}
                    onFocusChange={({ focused: focused3 }) => this.setState({ focused3 })}
                    id="skillcheckdate"
                    displayFormat="DD-MM-YYYY"
                    isOutsideRange={() => false}
                    disabled={this.state.form_status > 4}
                  />
                </Col>
                <Col xs="12" md="6">
                  <AvField type="textarea" label="Notes" name="skillchecknotes" placeholder="Enter Notes" onChange={this.onChange} value={skillchecknotes} disabled={this.state.form_status > 4} />
                </Col>
                <Col xs="12" md="4">
                  <div>
                    <label>Status</label>
                  </div>
                  <Label className="switch switch-text switch-success new-switch">
                    <Input
                      type="checkbox"
                      className="switch-input"
                      checked={this.state.skillcheckstatus}
                      onChange={e => {
                        this.setState({ skillcheckstatus: e.target.checked });
                      }}
                      disabled={this.state.form_status > 4}
                    />
                    <span className="switch-label" data-on="Verified" data-off="Pending" />
                    <span className="switch-handle new-handle" />
                  </Label>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="12">
                  <h4>DBS Check</h4>
                </Col>
                <Col xs="12" md="2">
                  <label>Date</label>
                  <SingleDatePicker
                    date={moment(this.state.dbscheckdate)}
                    onDateChange={dbscheckdate => this.setState({ dbscheckdate })}
                    focused={this.state.focused4}
                    onFocusChange={({ focused: focused4 }) => this.setState({ focused4 })}
                    id="dbscheckdate"
                    displayFormat="DD-MM-YYYY"
                    isOutsideRange={() => false}
                    disabled={this.state.form_status > 4}
                  />
                </Col>
                <Col xs="12" md="6">
                  <AvField type="textarea" label="Notes" name="dbschecknotes" placeholder="Enter Notes" onChange={this.onChange} value={dbschecknotes} disabled={form_status > 4} />
                </Col>
                <Col xs="12" md="4">
                  <div>
                    <label>Status</label>
                  </div>
                  <Label className="switch switch-text switch-success new-switch">
                    <Input
                      type="checkbox"
                      className="switch-input"
                      checked={this.state.dbscheckstatus}
                      onChange={e => {
                        this.setState({ dbscheckstatus: e.target.checked });
                      }}
                      disabled={this.state.form_status > 4}
                    />
                    <span className="switch-label" data-on="Verified" data-off="Pending" />
                    <span className="switch-handle new-handle" />
                  </Label>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="12">
                  <h4>Health Check</h4>
                </Col>
                <Col xs="12" md="2">
                  <label>Date</label>
                  <SingleDatePicker
                    date={moment(this.state.healthcheckdate)}
                    onDateChange={healthcheckdate => this.setState({ healthcheckdate })}
                    focused={this.state.focused5}
                    onFocusChange={({ focused: focused5 }) => this.setState({ focused5 })}
                    id="healthcheckdate"
                    displayFormat="DD-MM-YYYY"
                    isOutsideRange={() => false}
                    disabled={this.state.form_status > 4}
                  />
                </Col>
                <Col xs="12" md="6">
                  <AvField type="textarea" label="Notes" name="healthchecknotes" placeholder="Enter Notes" onChange={this.onChange} value={healthchecknotes} disabled={form_status > 4} />
                </Col>
                <Col xs="12" md="4">
                  <div>
                    <label>Status</label>
                  </div>
                  <Label className="switch switch-text switch-success new-switch">
                    <Input
                      type="checkbox"
                      className="switch-input"
                      checked={this.state.healthcheckstatus}
                      onChange={e => {
                        this.setState({ healthcheckstatus: e.target.checked });
                      }}
                      disabled={this.state.form_status > 4}
                    />
                    <span className="switch-label" data-on="Verified" data-off="Pending" />
                    <span className="switch-handle new-handle" />
                  </Label>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter className="justify-content-between modal-gal">
              <div>
                <Button color="secondary" type="button" onClick={this.CheckFormModal} title="Close">
                  <i className="fa fa-close" />
                </Button>
              </div>
              {this.state.form_status === 4 ? (
                <Button color="success" className="mr-3" type="submit">
                  Save
                </Button>
              ) : null}
              {this.state.form_status > 4 ? <Badge color="info">Verification Process Completed</Badge> : null}
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

export default onlineform;
