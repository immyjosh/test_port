import React, { Component, Fragment } from "react";
import { Col, Button, Badge } from "reactstrap";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps";
// import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import { compose, withProps, lifecycle } from "recompose";
import { Settings } from "../../../api/key";
import request, { NodeURL } from "../../../api/api";
import { toast } from "react-toastify";

const assignShift = (item, emplist) => {
  request({
    url: "/agency/assign_shift",
    method: "POST",
    data: {
      shiftId: emplist._id,
      employeeId: item.employee,
      status: emplist.status,
      employeeData: item
    }
  })
    .then(res => {
      if (res.status === 1) {
        toast.success("Assigned");
        setTimeout(() => {
          window.location.reload(true);
        }, 2000);
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    })
    .catch(error => {
      toast.error(error);
    });
};
const assignShiftOther = (item, emplist) => {
  request({
    url: "/agency/assign_shift",
    method: "POST",
    data: {
      shiftId: emplist._id,
      employeeId: item.employee
    }
  })
    .then(res => {
      if (res.status === 1) {
        toast.success("Assigned");
        setTimeout(() => {
          window.location.reload(true);
        }, 2000);
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    })
    .catch(error => {
      toast.error(error);
    });
};
const unAssign = (item, emplist) => {
  request({
    url: "/agency/unassign_shift",
    method: "POST",
    data: {
      id: emplist._id,
      employeeid: item.employee
    }
  })
    .then(res => {
      if (res.status === 1) {
        setTimeout(() => {
          window.location.reload(true);
        }, 2000);
        toast.success("Shift Cancelled");
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    })
    .catch(error => {
      toast.success(error);
    });
};

const MapComponent = compose(
  withProps({
    googleMapURL: Settings.googleMapURL + Settings.googleMapKey,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `600px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  lifecycle({
    componentDidMount() {
      if (this.props && this.props.markers.length > 0) {
        this.setState({
          zoomToMarkers: map => {
            const bounds = new window.google.maps.LatLngBounds();
            this.props.markers.forEach((child, index) => {
              bounds.extend(new window.google.maps.LatLng(child.lat, child.lng));
            });
            if (map) map.fitBounds(bounds);
          }
        });
      }
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap ref={props.zoomToMarkers} defaultZoom={5} defaultCenter={{ lat: 25.0391667, lng: 121.525 }} defaultOptions={{ gestureHandling: "greedy" }}>
        {props.markers.length && props.markers.map((marker, i) => (
          <div className="marger1" key={i}>
            <Marker
              className="marger"
              key={marker.id}
              position={{ lat: marker.lat, lng: marker.lng }}
              icon={{
                url: (marker && marker.avatar) || marker.label === "C" ? `${NodeURL}/${marker.label === "C" ? "uploads/map/icons8-building-48.png" : marker.avatar}` : "",
                // size: { width: 60, height: 80 },
                // anchor: { x: 15, y: 50 },
                scaledSize: { width: 40, height: 40 }
              }}
              onClick={() => props.location(marker)}
            >
              {props.isOpen && props.selectLocation.email === marker.email ? (
                <InfoWindow onCloseClick={() => props.closeModel()}>
                  <Col className="full-mapdetails">
                    <Col className="left-mapdetails">
                      <img width="80px" height="80px" alt="" src={NodeURL + "/" + props.selectLocation.avatar} />
                    </Col>
                    <Col className="right-mapdetails">
                      <p className="view-det map-names">
                        <span>{props.selectLocation.name}</span>
                      </p>
                      <p className="view-det job_type">
                        <span>
                          {props.selectLocation.job_type &&
                            props.selectLocation.job_type.map((list, key) => (
                              <Fragment key={key}>
                                {list}
                                <Fragment>,</Fragment>
                              </Fragment>
                            ))}
                        </span>
                      </p>
                      <p className="view-det location">
                        <span>
                          {props.selectLocation.locations &&
                            props.selectLocation.locations.map((list, key) => (
                              <Fragment key={key}>
                                {list}
                                <Fragment>,</Fragment>
                              </Fragment>
                            ))}
                        </span>
                      </p>
                      <p className="view-det email">
                        <span>{props.selectLocation.email}</span>
                      </p>
                      <p className="view-det buttons">
                        {props.selectLocation.label !== "C" ? (
                          <Fragment>
                            {props.emplist.status === 1 ? (
                              <Fragment>
                                <Button type="submit" size="sm" color="success ml-5" title="Assign" onClick={(item, list) => assignShift(props.selectLocation, props.emplist)}>
                                  Assign <i className="fa fa-check-circle" />
                                </Button>{" "}
                              </Fragment>
                            ) : (
                              <Fragment>
                                {props.selectLocation.status === 1 || props.selectLocation.status === 2 ? (
                                  <Fragment>
                                    {props.emplist.status <= 3 ? (
                                      <Fragment>
                                        <Badge color="info">Shift Accepted</Badge>
                                        <Button type="submit" size="sm" color="success ml-5" title="Assign" onClick={(item, list) => assignShiftOther(props.selectLocation, props.emplist)}>
                                          Assign <i className="fa fa-check-circle" />
                                        </Button>{" "}
                                      </Fragment>
                                    ) : null}
                                    {props.emplist.status > 3 && props.emplist.status < 5 ? (
                                      <>
                                        <Badge color="info">Assigned</Badge>
                                        <Button type="submit" size="sm" color="success ml-1 pull-left" title="Assign" onClick={items => unAssign(props.selectLocation, props.emplist)}>
                                          Un-assign <i className="fa fa-check-circle" />
                                        </Button>{" "}
                                      </>
                                    ) : null}

                                    {props.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                                    {props.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                                    {props.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                                    {props.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                                    {props.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                                    {props.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                                  </Fragment>
                                ) : props.emplist && props.emplist.status < 4 ? (
                                  <>
                                    <Badge color="warning">Requested</Badge>
                                    <Button type="submit" size="sm" color="success ml-5" title="Assign" onClick={(item, list) => assignShift(props.selectLocation, props.emplist)}>
                                      Assign <i className="fa fa-check-circle" />
                                    </Button>{" "}
                                  </>
                                ) : null}
                              </Fragment>
                            )}
                          </Fragment>
                        ) : null}
                      </p>
                    </Col>
                  </Col>
                </InfoWindow>
              ) : null}
            </Marker>
          </div>
        ))}
  </GoogleMap>
));

class Requestmap extends Component {
  state = {};

  locationDetails = location => {
    console.log(location);
    this.setState({
      selectLocation: location,
      isOpen: true
    });
  };

  closeModel = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    const { locations, emplist } = this.props;
    console.log('locations, emplist ', locations, emplist);
    return (
      <div className="small_map">
        {locations && locations.length > 0 ? (
          <MapComponent markers={[...emplist.client, ...locations]} location={this.locationDetails} selectLocation={this.state.selectLocation || false} isOpen={this.state.isOpen} closeModel={this.closeModel} emplist={emplist} />
        ) : (
          <MapComponent markers={[...emplist.client]} location={this.locationDetails} selectLocation={this.state.selectLocation || false} isOpen={this.state.isOpen} closeModel={this.closeModel} emplist={emplist} />
        )}
      </div>
    );
  }
}

export default Requestmap;
