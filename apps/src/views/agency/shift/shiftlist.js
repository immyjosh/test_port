import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import {
  Label,
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Row,
  Col,
  Table,
  UncontrolledTooltip
} from "reactstrap";
import request from "../../../api/api";
import { AvGroup, AvFeedback, AvInput, AvForm } from "availity-reactstrap-validation";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
import moment from "moment";
import { breaklist, timelist, monthlydates } from "../../common/utils";
import "react-dates/initialize";
import { DateRangePicker, SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import Widget02 from "../../../views/Template/Widgets/Widget02";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "150px"
};

class Shiftlist extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    isLoader: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      status : "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date1: null,
    end_date1: null,
    emplist: [],
    job_type1: "",
    jobname: "",
    jobrate: "",
    jobmodel: false,
    jobtypelistselect: [],
    eventlarge: false,
    nestedeventlarge: false,
    closeAll: false,
    large: false,
    allday: true,
    title: "",
    location1: "",
    starttime: "",
    endtime: "",
    breaktime: "",
    slotdata: [],
    shift_type: "",
    multiselecteddays: "",
    job_type: "",
    timelist: [],
    breaklist: [],
    monthlydates: [],
    eventlist: [],
    worklocationlist: [],
    eventdata: "",
    id: "",
    weekly: false,
    rotational: false,
    monthly: false,
    monthlydate: [],
    isGoing: true,
    sun: false,
    mon: false,
    tue: false,
    wed: false,
    thu: false,
    fri: false,
    sat: false,
    jobtypelist: [],
    option_data: {},
    shift_option: "",
    shifttemplatelist: [],
    shifttemplatelistselect: [],
    date_list: [],
    Weekly_select_date_list: [],
    montofri: 0,
    Weekly_select_date_count: 0,
    monthly_date_count: 0,
    shift_template: "",
    template: "",
    values: "",
    status: "",
    start_date_time: null,
    end_date_time: null,
    edit: false,
    clientlist: [],
    client1: "",
    clientview: "",
    locationview: "",
    job_typeview: "",
    xday: "",
    yday: "",
    shifttemplatemodel: false,
    shifttemplatename: "",
    shifttemplatefrom: "",
    shifttemplateto: "",
    shifttemplatebreak: "",
    radiodata: "",
    selecterror: false,
    selectbreakerror: false,
    selectenderror: false,
    requests: 0,
    accepted: 0,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    finished: 0,
    branchlist: [],
    branchview: "",
    branch: "",
    notes: "",
    latefee: false
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    this.setState({ timelist, breaklist, monthlydates });
    this.populateData();
    request({
      url: "/agency/jobtype/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
        const jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
          value: emp.job_id,
          label: emp.name,
          client: emp.client
        }));
        this.setState({
          jobtypelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/shifttemplate/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ shifttemplatelist: res.response.result });
        const shifttemplatelistselect = this.state.shifttemplatelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          shifttemplatelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/client/locations",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          clientlist: res.response.result || {},
          branchlist: res.response.result.length ? res.response.result.map(list => list.branches[0]) : []
        });
        const clientlistselect = this.state.clientlist.map((emp, key) => ({
          value: emp._id,
          label: emp.companyname
        }));
        this.setState({
          clientlistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    e.persist();
    this.setState({ [e.target.name]: value });
  };
  editpage = (e, id) => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    if (id.status !== 10) {
      return this.props.history.push({
        pathname: "/agency/requestemployees",
        state: { rowid: id }
      });
    }
  };
  populateData() {
    request({
      url: "/agency/shift/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
          const getshiftdata = res.response.result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const start_split = (item.starttime / 3600).toString().split(".");
            const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
            const starttimehr = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
            const end_split = (item.endtime / 3600).toString().split(".");
            const endmins = end_split[1] === undefined ? "00" : (+"0" + "." + end_split[1]) * 60;
            const endtimehr = num.indexOf(end_split[0]) === -1 ? end_split[0] + ":" + endmins : +"0" + "" + end_split[0] + ":" + endmins;
            return{
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              locations: item.locations,
              breaktime: item.breaktime ? item.breaktime : 0,
              start_date: item.start_date,
              end_date: item.end_date,
              job_type: item.job_type,
              title: item.title,
              shiftId: item.shiftId,
              client: item.client,
              notes: item.notes,
              branch1: item.branch_data[0].branchname,
              status: item.status
            };
          });
          const groupData = res.response.groupcount ? res.response.groupcount : {};
          this.setState({
            adminlist: getshiftdata,
            pages: res.response.fullcount,
            currPage: res.response.length,
            added: groupData.added,
            accepted: groupData.accepted,
            assigned: groupData.assigned,
            completed: groupData.completed,
            ongoing: groupData.ongoing,
            timehseet_approved: groupData.timehseet_approved,
            invoice_approved: groupData.invoice_approved,
            Payment_completed: groupData.Payment_completed,
            expired: groupData.expired
          });
          if (this.props && this.props.location.state) {
            // return this.upNotify.updateNotification();
          }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["start_date", "starttime", "job_type", "start_date", "branch", "location", "client", "title", "status"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }

  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Name") {
        state.tableOptions.filter = "name";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/administrators/agency/userexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  ShiftlisttoastId = "Shiftlist";
  nodify() {
    if (!toast.isActive(this.ShiftlisttoastId)) {
      this.ShiftlisttoastId = toast.warn("Updated");
    }
  }
  ShiftlistnodifytoastId = "Shiftlist";
  adminsavenodify() {
    if (!toast.isActive(this.ShiftlistnodifytoastId)) {
      this.ShiftlistnodifytoastId = toast.success("Saved!");
    }
  }
  sendRequest(item) {
    request({
      url: "/agency/send_shift_requests",
      method: "POST",
      data: {
        shiftId: item._id
      }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response.result);
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  EditModelOpen = (e, values) => {
    e.stopPropagation();
    this.setState({
      eventlarge: !this.state.eventlarge
    });
    this.eventset(values);
    this.EditModelClose();
  };
  eventset = values => {
    request({
      url: "/agency/shift/view",
      method: "POST",
      data: { id: values._id }
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            emplist: res.response.result[0],
            job_type1: res.response.result[0].job_type
          });
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = this.state.emplist.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startsec = splithr[1];
          if (startsec === undefined) {
            var startmins = "00";
          } else {
            startmins = (+"0" + "." + startsec) * 60;
          }

          if (num.indexOf(splithr[0]) === -1) {
            var starttimehr = splithr[0] + ":" + startmins;
          } else {
            starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
          }

          const eveendhr = this.state.emplist.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          if (endsec === undefined) {
            var endmins = "00";
          } else {
            endmins = (+"0" + "." + endsec) * 60;
          }

          if (num.indexOf(splitendhr[0]) === -1) {
            var endtimehr = splitendhr[0] + ":" + endmins;
          } else {
            endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
          }
          this.setState({
            edit: false,
            starttime: starttimehr,
            starttime_view: starttimehr,
            endtime: endtimehr,
            breaktime1: this.state.emplist.breaktime / 60,
            endtime_view: endtimehr,
            breaktime_view: this.state.emplist.breaktime / 60,
            starttime_edit: this.state.emplist.starttime,
            endtime_edit: this.state.emplist.endtime,
            breaktime_edit: this.state.emplist.breaktime,
            shift_type: this.state.emplist.shift_type,
            latefee: this.state.emplist.latefee,
            title: this.state.emplist.title,
            id: this.state.emplist._id,
            start_date_time: this.state.emplist.start_date,
            end_date_time: this.state.emplist.end_date,
            start_date1: moment(this.state.emplist.start_date).format("DD-MM-YYYY"),
            end_date1: moment(this.state.emplist.end_date).format("DD-MM-YYYY"),
            shift_option: this.state.emplist.shift_option,
            status: this.state.emplist.status,
            client1: res.response.result[0].client_data[0]._id || "",
            clientview: this.state.emplist.client,
            branchview: this.state.emplist.branch_data[0].branches.branchname,
            branch: this.state.emplist.branch || this.state.emplist.branch_data[0].branches._id,
            notes: this.state.emplist.notes,
            job_typeview: this.state.emplist.job_type,
            job_type1: res.response.result[0].jobtype_data[0]._id || "",
            locationview: this.state.emplist.locations,
            location1: res.response.result[0].location_data[0]._id || ""
          });
          // this.selectlocation();
          this.EditnestedModal();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => toast.error(err));
  };
  EditModelClose = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      title: "",
      location1: "",
      starttime: "",
      endtime: "",
      breaktime1: "",
      start_date1: null,
      end_date1: null,
      shift_type: "",
      job_type1: "",
      shift_option: "",
      shift_template: "",
      start_date_time: null,
      end_date_time: null,
      edit: false,
      client1: "",
      latefee: "",
      notes: "",
      branch: ""
    });
    this.componentDidMount();
  };
  EditnestedModalview = () => {
    this.setState({
      edit: false,
      starttime: this.state.starttime.label || this.state.starttime_view,
      endtime: this.state.endtime.label || this.state.endtime_view,
      breaktime1: this.state.breaktime.label || this.state.breaktime_view,
      shift_type: this.state.shift_type,
      title: this.state.title,
      id: this.state.id,
      start_date1: moment(this.state.start_date_time).format("DD-MM-YYYY"),
      end_date1: moment(this.state.end_date_time).format("DD-MM-YYYY"),
      shift_option: this.state.shift_option,
      status: this.state.status,
      clientview: this.state.clientview,
      client1: this.state.client1,
      branchview: this.state.branchview,
      latefee: this.state.latefee,
      branch: this.state.branch,
      job_typeview: this.state.job_typeview,
      job_type1: this.state.job_type1,
      notes: this.state.notes,
      locationview: this.state.locationview,
      location1: this.state.location1
    });
  };
  EditnestedModal = () => {
    if (this.state.shift_option === "weekly") {
      var sunday = this.state.sun;
      var monday = this.state.mon;
      var tueday = this.state.tue;
      var wedday = this.state.wed;
      var thuday = this.state.thu;
      var friday = this.state.fri;
      var satday = this.state.sat;
    } else {
      sunday = false;
      monday = false;
      tueday = false;
      wedday = false;
      thuday = false;
      friday = false;
      satday = false;
    }
    if (this.state.shift_option === "weekly") {
      this.setState({
        weekly: true,
        rotational: false,
        monthly: false
      });
    } else if (this.state.shift_option === "rotational") {
      this.setState({
        weekly: false,
        rotational: true,
        monthly: false
      });
    } else if (this.state.shift_option === "monthly") {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: true
      });
    } else {
      this.setState({
        weekly: false,
        rotational: false,
        monthly: false
      });
    }
    this.setState({
      edit: true,
      starttime: this.state.starttime_edit || this.state.starttime,
      endtime: this.state.endtime_edit || this.state.endtime,
      breaktime1: this.state.breaktime_edit || this.state.breaktime,
      shift_type: this.state.shift_type,
      title: this.state.title,
      id: this.state.id,
      start_date1: moment(this.state.start_date_time),
      end_date1: moment(this.state.end_date_time),
      shift_option: this.state.shift_option,
      status: this.state.status,
      clientview: this.state.clientview,
      client1: this.state.client1,
      latefee: this.state.latefee,
      notes: this.state.notes,
      branchview: this.state.branchview,
      branch: this.state.branch,
      job_typeview: this.state.job_typeview,
      job_type1: this.state.job_type1,
      locationview: this.state.locationview,
      location1: this.state.location1,
      sun: sunday,
      mon: monday,
      tue: tueday,
      wed: wedday,
      thu: thuday,
      fri: friday,
      sat: satday
    });
    this.selectlocation();
  };
  selectjoblocation = value => {
    if (value) {
      const jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
        value: emp.job_id,
        label: emp.name,
        client: emp.client
      })).filter(list => list.client === value.value);
      const filteredlist = this.state.clientlist.filter(list => list._id === value.value);
      const branchlistselected = filteredlist[0].branches.map((branch, key) => ({
        value: branch._id,
        label: branch.branchname,
        locationID: branch.branchlocation
      }));
      if (branchlistselected.length === 1) {
        this.setState({ client1: value.value, jobtypelistselect, branchlistselected, branch: branchlistselected[0].value, location1: branchlistselected[0].locationID });
      }
      if (branchlistselected.length > 1) {
        this.setState({ client1: value.value, jobtypelistselect, branchlistselected });
      }
    }
  };
  selectlocation = e => {
    const filteredlist = this.state.clientlist.filter(list => list._id === this.state.client1);
    const branchlistselected = filteredlist[0].branches.map((branch, key) => ({
      value: branch._id,
      label: branch.branchname,
      locationID: branch.branchlocation
    }));
    this.setState({
      client1: this.state.client1,
      branchlistselected: branchlistselected
    });
  };
  branchselect = value => {
    if (value) {
      this.setState({ branch: value.value, location1: value.locationID });
    }
  };

  shifttemplateChange = value => {
    this.setState({ shift_template: value });
    this.state.shifttemplatelist.filter((shift, key) => {
      if (value && shift._id === value.value) {
        this.setState({
          starttime: shift.from,
          endtime: shift.to,
          breaktime1: shift.break
        });
      }
      return true;
    });
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type1: value.value });
    }
  };
  OnFormEdit = (e, values) => {
    if (this.state.radiodata === "daily") {
      this.setState({ option_data: { daily: "daily" } });
    } else if (this.state.radiodata === "weekday") {
      this.setState({ option_data: { weekday: "weekday" } });
    } else if (this.state.radiodata === "weekly") {
      this.setState({
        option_data: {
          weekly: {
            0: this.state.sun,
            1: this.state.mon,
            2: this.state.tue,
            3: this.state.wed,
            4: this.state.thu,
            5: this.state.fri,
            6: this.state.sat
          }
        }
      });
    } else if (this.state.radiodata === "rotational") {
      this.setState({ option_data: { rotational: { x: this.state.xday, y: this.state.yday } } });
    } else if (this.state.radiodata === "monthly") {
      this.setState({ option_data: { monthly: this.state.monthlydate } });
    }
    request({
      url: "/agency/shift/save",
      method: "POST",
      data: {
        id: this.state.id,
        // title: this.state.title,
        locations: this.state.location1,
        client: this.state.client1,
        notes: this.state.notes,
        branch: this.state.branch,
        job_type: this.state.job_type1,
        starttime: this.state.starttime.value || this.state.starttime,
        endtime: this.state.endtime.value || this.state.endtime,
        breaktime: this.state.breaktime1 ? this.state.breaktime1 || this.state.breaktime1.value : 0,
        start_date: this.state.start_date1,
        end_date: this.state.end_date1,
        shift_type: this.state.shift_type,
        shift_option: this.state.shift_option,
        latefee: this.state.latefee,
        status: this.state.status,
        monthly_select_day: this.state.monthlydate,
        option_data: this.state.option_data
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Shift Updated!");
          this.setState({
            eventlarge: !this.state.eventlarge,
            slotdata: [],
            start_date: null,
            end_date: null,
            shifttitle: "",
            locations: "",
            starttime: "",
            endtime: "",
            breaktime: "",
            shift_type: "",
            job_type: "",
            shift_option: "",
            latefee: "",
            shift_template: "",
            option_data: [],
            start_date_time: null,
            end_date_time: null,
            client: "",
            notes: "",
            branch: ""
          });
          this.componentDidMount();
          this.props.history.push({
            pathname: "/agency/requestemployees",
            state: { rowid: { _id: res.shiftId, status: res.shift_status } }
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  shifttemplateModel = () => {
    this.setState({
      shifttemplatemodel: !this.state.shifttemplatemodel
    });
  };
  selectstarttimeChange1 = value => {
    if (value) {
      this.setState({ shifttemplatefrom: value.value, selecterror1: false });
    } else {
      this.setState({ shifttemplatefrom: "", selecterror1: true });
    }
  };
  selectendtimeChange1 = value => {
    if (value) {
      this.setState({ shifttemplateto: value.value, selectenderror1: false });
    } else {
      this.setState({ shifttemplateto: "", selectenderror1: true });
    }
  };
  selectbreaktimeChange1 = value => {
    if (value) {
      this.setState({ shifttemplatebreak: value.value, selectbreakerror1: false });
    } else {
      this.setState({ shifttemplatebreak: "", selectbreakerror1: true });
    }
  };
  selectfield1 = () => {
    if (this.state.shifttemplatefrom === "") {
      this.setState({
        selecterror1: true
      });
    }
    if (this.state.shifttemplateto === "") {
      this.setState({
        selectenderror1: true
      });
    }
    if (this.state.shifttemplatebreak === "") {
      this.setState({
        selectbreakerror1: true
      });
    }
  };
  OnFormSubmitshifttemplate = (e, values) => {
    // e.preventDefault();
    e.persist();
    request({
      url: "/agency/shifttemplate/save",
      method: "POST",
      data: {
        name: this.state.shifttemplatename,
        from: this.state.shifttemplatefrom,
        to: this.state.shifttemplateto,
        break: this.state.shifttemplatebreak,
        status: 1
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("New Shift Template Added");
          this.componentDidMount();
          this.shifttemplateModel();
          this.setState({ shifttemplatename: "", shifttemplatefrom: "", shifttemplateto: "", shifttemplatebreak: "" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ starttime: value.value, selecterror: false });
    } else {
      this.setState({ starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ endtime: value.value, selectenderror: false });
    } else {
      this.setState({ endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktime1: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktime1: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.starttime === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.endtime === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.breaktime1 === "") {
      this.setState({
        selectbreakerror: true
      });
    }
  };
  selectdateChange = value => {
    if (value) {
      const values = value.map(value => value.value);
      this.setState({ monthlydate: values });
    } else {
      this.setState({ monthlydate: "" });
    }
  };
  DeleteShift = (event,id) => {
    event.stopPropagation();
    request({
      url: "/agency/shift/deleteshift",
      method: "POST",
      data: { id }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response);
        this.componentDidMount();
      } else if(res.status === 0) {
        toast.error(res.response);
      }
    }).catch(error => {
      toast.error(error);
    });
  }
  ShiftChange = num => {
    if(num){
      this.setState(state=>{
        state.tableOptions.status = num;
        this.populateData();
      })
    }else{
        this.setState(state=>{
          state.tableOptions.status = "";
          this.populateData();
        })
    }
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    console.log("this.state.ongoing",this.state.ongoing);
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row className="sub-ranges ">
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(1)} header={this.state.added ? this.state.added : "0"} mainText="New" icon="fa fa-send" color="primary" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(3)} header={this.state.accepted ? this.state.accepted : "0"} mainText="Accepted" icon="fa fa-check-circle" color="warning" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(4)} header={this.state.assigned ? this.state.assigned : "0"} mainText="Assigned" icon="fa fa-list" color="info" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(5)} header={this.state.ongoing ? this.state.ongoing : "0"} mainText="Ongoing" icon="fa fa-spinner" color="success" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(6)} header={this.state.completed ? this.state.completed : "0"} mainText="Completed" icon="fa fa-check-square-o" color="success" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(7)}
              header={this.state.timehseet_approved ? this.state.timehseet_approved : "0"}
              mainText="Timesheets Approved"
              icon="fa fa-check"
              color="warning"
            />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(8)} header={this.state.invoice_approved ? this.state.invoice_approved : "0"} mainText="Invoice Approved" icon="fa fa-check" color="info" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(9)} header={this.state.Payment_completed ? this.state.Payment_completed : "0"} mainText="Paid" icon="fa fa-money" color="danger" />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02 className="cursor-pointer" onClick={()=>this.ShiftChange(10)} header={this.state.expired ? this.state.expired : "0"} mainText="Expired" icon="fa fa-close" color="secondary" />
          </Col>
        </Row>
        <Card>
          <CardHeader>
            <i className="icon-list" />
            <span className="cursor-pointer" onClick={()=>this.ShiftChange("")}>Shift List</span>
            <div
              className="card-actions primary"
              style={tStyle}
              onClick={() => {
                this.props.history.push("/agency/editshift");
              }}
            >
              <button style={tStyles}>
                <i className="fa fa-calendar" /> <strong> Calender View</strong>
                {/* <small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-between">
              {/* <div className="col-lg-1 pl-0">
                <Button
                  size="md"
                  color="primary pull-left"
                  onClick={this.export}
                >
                  Export
                </Button>
              </div> */}
              <div className="col-lg-7">
                <DateRangePicker
                  showClearDates={true}
                  startDate={this.state.start_date}
                  startDateId="start_date"
                  endDate={this.state.end_date}
                  endDateId="end_date"
                  onDatesChange={({ startDate, endDate }) => {
                    if (startDate ===null && endDate=== null ) {
                      this.setState(state => {
                        state.tableOptions.from_date = "";
                        state.tableOptions.to_date = "";
                      });
                      this.populateData();
                    }
                    this.setState({
                      start_date: startDate,
                      end_date: endDate
                    });
                  }}
                  isOutsideRange={day => day.isBefore(this.state.start_date)}
                  focusedInput={this.state.focusedInput}
                  onFocusChange={focusedInput => this.setState({ focusedInput })}
                  displayFormat="DD-MM-YYYY"
                />
                <Button
                  className="rounded-0"
                  size="md"
                  color="primary"
                  onClick={() => {
                    this.fromTo();
                  }}
                >
                  <i className="fa fa-search" />
                </Button>
              </div>
              <div className="col-lg-5 pr-0">
                <InputGroup>
                  <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                    <option>All</option>
                  </Input>
                  <Input
                    type="text"
                    ref="search"
                    placeholder="Search"
                    name="search"
                    onChange={e => this.search(e.target.value)}
                    className="rounded-0 col-lg-6"
                  />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("title");
                      }}
                    >
                      Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("client");
                      }}
                    >
                      Client <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("location");
                      }}
                    >
                      Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="location" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("branch");
                      }}
                    >
                      Work Location {/* branch */} <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("job_type");
                      }}
                    >
                      Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_type" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("starttime");
                      }}
                    >
                      Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("start_date");
                      }}
                    >
                      Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" />
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("end_date");
                      }}
                    >
                      End Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th>*/}
                    <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={(e, id) => this.editpage(e, item)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.title}</td>
                        <td>{item.client}</td>
                        <td>{item.locations}</td>
                        <td>{item.branch1}</td>
                        <td>{item.job_type}</td>
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        {/* <td>{moment(item.end_date).format("DD-MM-YYYY")}</td>*/}
                        <td>
                          {item.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                          {item.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                          {item.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                          {item.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                          {item.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                          {item.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                          {item.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                          {item.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                          {item.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                          {item.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                        </td>
                        <td>
                          {/* {item.status === 1 ? (
                            <div>
                              <button
                                type="button"
                                title="Edit"
                                className="btn btn-info btn-sm"
                                id={`edit${i}`}
                                onClick={id => this.sendRequest(item)}
                              >
                                <i className="fa fa-envelope" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                Send Request
                              </UncontrolledTooltip>
                            </div>
                          ) : ( */}
                          <div className={"d-flex"}>
                            {item.status !== 10 ? (
                              <Fragment>
                                <button
                                  type="button"
                                  title="Edit"
                                  className="btn view-table mr-1"
                                  id={`edit${i}`}
                                  onClick={(e, id) => this.editpage(e, item)}
                                >
                                  <i className="fa fa-eye" />
                                </button>
                                <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                  View
                                </UncontrolledTooltip>
                              </Fragment>
                            ) : null}
                            {item.status < 3 ? (
                              <Fragment>
                                {/* <button type="button" title="Edit" className="btn btn-info btn-sm" id={`edit21${i}`} onClick={id => this.EditModelOpen(item)}>
                                  <i className="fa fa-edit" />
                                </button> */}
                                {item.status !== 10 ? (
                                  <Fragment>
                                    <button
                                      type="button"
                                      title="Edit"
                                      className="btn view-table"
                                      id={`edit21${i}`}
                                      onClick={(e, id) => this.EditModelOpen(e, item)}
                                    >
                                      <i className="fa fa-edit" />
                                    </button>
                                    <UncontrolledTooltip placement="top" target={`edit21${i}`}>
                                      Edit Shifts
                                    </UncontrolledTooltip>
                                    <button
                                      type="button"
                                      title="Delete"
                                      className="btn view-table"
                                      id={`delete21${i}`}
                                      onClick={(e, id) => this.DeleteShift(e, item._id)}
                                    >
                                      <i className="fa fa-trash" />
                                    </button>
                                    <UncontrolledTooltip placement="top" target={`edit21${i}`}>
                                      Delete
                                    </UncontrolledTooltip>
                                  </Fragment>
                                ) : null}
                              </Fragment>
                            ) : null}
                          </div>
                          {/* )} */}
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={11}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
        <Modal isOpen={this.state.eventlarge} className={"modal-lg " + this.props.className}>
          {this.state.edit ? (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>EDIT SHIFT</ModalHeader>
              <AvForm onValidSubmit={this.OnFormEdit}>
                <ModalBody>
                  <Row>
                    <Col xs="12" md="4">
                      <Label>Client</Label>
                      <Select name="form-field-name2" value={this.state.client1} options={this.state.clientlistselect} onChange={this.selectjoblocation} />
                    </Col>
                    {/* <Col xs="12" md="4">
                      <Label>Location</Label>
                      <Select name="form-field-name2" value={this.state.location1} options={this.state.worklocationlist} onChange={this.worklocationselect} />
                    </Col>*/}
                    <Col xs="12" md="4">
                      <Label>Work Location {/* branch */}</Label>
                      <Select name="branch" value={this.state.branch} options={this.state.branchlistselected} onChange={this.branchselect} />
                    </Col>
                    <Col xs="12" md="4">
                      <Label>Role</Label>
                      <Select name="form-field-name2" value={this.state.job_type1} options={this.state.jobtypelistselect} onChange={this.saveChanges} />
                      {/* <small className="ml-1 cursor-pointer" onClick={this.jobTypeModel}>
                        {" "}
                        <i className="fa fa-info-circle" />
                        <span className="ml-1 cursor-pointer cm-strong">Add New Role</span>
                      </small> */}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="3">
                      <Label>Shift</Label>
                      <Select
                        name="form-field-name2"
                        value={this.state.shift_template}
                        options={this.state.shifttemplatelistselect}
                        onChange={this.shifttemplateChange}
                      />
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                      <Select
                        className={this.state.selecterror ? "custom_select_validation" : null}
                        name="starttime"
                        value={this.state.starttime}
                        options={this.state.timelist}
                        onChange={this.selectstarttimeChange}
                      />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select
                        className={this.state.selectenderror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.endtime}
                        options={this.state.timelist}
                        onChange={this.selectendtimeChange}
                      />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                        <Label>
                          Break <small>(In Minutes)</small>
                        </Label>
                      )}
                      <Select
                        className={this.state.selectbreakerror ? "custom_select_validation" : null}
                        name="form-field-name2"
                        value={this.state.breaktime1}
                        options={this.state.breaklist}
                        onChange={this.selectbreaktimeChange}
                      />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="6" className="p-0">
                    <Col xs="12" md="12">
                      <AvGroup>
                        <Label>Notes :</Label>
                        <AvInput
                          type="textarea"
                          name="notes"
                          placeholder="Enter Notes"
                          onChange={this.onChange}
                          value={this.state.notes}
                          autoComplete="notes"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="12">
                      <AvGroup check>
                        <Label check for="latefee">
                          <AvInput type="checkbox" name="latefee" value={this.state.latefee} checked={this.state.latefee} onChange={this.onChange} /> Late
                          Booking Fee
                        </Label>
                      </AvGroup>
                    </Col>
                    </Col>
                    <Col xs="12" md="6" className="p-0">
                      <Col xs="12" md="12">
                        {this.state.shift_type === "single" ? (
                            <Row>
                              <Col sm="12">
                                <p>Select Date</p>
                                <SingleDatePicker
                                    date={this.state.start_date1}
                                    openDirection={this.state.openDirection}
                                    onDateChange={date => this.setState({ start_date1: date })}
                                    focused={this.state.focused}
                                    onFocusChange={({ focused }) => this.setState({ focused })}
                                    id="start_date"
                                    displayFormat="DD-MM-YYYY"
                                />
                              </Col>
                            </Row>
                        ) : null}
                      </Col>
                    </Col>
                  </Row>
                </ModalBody>
                <ModalFooter className="justify-content-between">
                  <div>
                    <Button color="secondary" onClick={this.EditModelClose} title="Close">
                      <i className="fa fa-close" />
                    </Button>
                    {/* <Button color="secondary" className="ml-3" onClick={this.EditnestedModalview} title="Back to View Details">
                      <i className="fa fa-arrow-left" />
                    </Button> */}
                  </div>
                  <Button color="success" type="submit" title="Save & Continue" id="savecontinue2" onClick={this.selectfield}>
                    {" "}
                    <i className="fa fa-arrow-circle-right" />
                  </Button>{" "}
                  <UncontrolledTooltip placement="left" target="savecontinue2">
                    Save & Continue
                  </UncontrolledTooltip>
                </ModalFooter>
              </AvForm>
            </Fragment>
          ) : (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>Shift Details</ModalHeader>
              <ModalBody>
                <Row>
				<div className="dash-points">
                  <Col xs="12" md="4">
                    <b>Shift:</b> {this.state.title}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Client:</b> {this.state.clientview}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Area:</b> {this.state.locationview}
                  </Col>
				  </div>
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Work Location {/* branch */}:</b> {this.state.branchview}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Role:</b> {this.state.job_typeview}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Date:</b>
                    {this.state.start_date1}{" "}
                  </Col>
                </Row>{" "}
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Time:</b> {this.state.starttime} - {this.state.endtime}
                  </Col>
                  <Col xs="12" md="4">
                    <b>Break:</b> {this.state.breaktime1} Minutes
                  </Col>
                  <Col xs="12" md="4">
                    <b>Late Booking Fee :</b> {this.state.latefee ? "Yes" : "No"}
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col xs="12" md="4">
                    <b>Shift Status:</b>
                    {this.state.emplist.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                    {this.state.emplist.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                    {this.state.emplist.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                    {this.state.emplist.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                    {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                    {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                    {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                    {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                    {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                    {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                  </Col>
                  {this.state.shift_type === "multiple" ? (
                    <Col xs="12" md="8">
                      <span>
                        <b>Multiple Day:</b>
                        {this.state.shift_option === "daily" ? <span>Daily </span> : null}
                        {this.state.shift_option === "weekday" ? <span>Every Weekday (Mon - Fri)</span> : null}
                        {this.state.shift_option === "weekly" ? (
                          <span>
                            Weekly {this.state.sun === true ? <strong>Sun, </strong> : null}
                            {this.state.mon === true ? <strong>Mon, </strong> : null}
                            {this.state.tue === true ? <strong>Tue, </strong> : null}
                            {this.state.wed === true ? <strong>Wed, </strong> : null}
                            {this.state.thu === true ? <strong>Thu, </strong> : null}
                            {this.state.fri === true ? <strong>Fri, </strong> : null}
                            {this.state.sat === true ? <strong>Sat </strong> : null}
                            Days
                          </span>
                        ) : null}
                        {this.state.shift_option === "rotational" ? (
                          <span>
                            X Days : {this.state.xday} / Y Days : {this.state.yday}
                          </span>
                        ) : null}
                        {this.state.shift_option === "monthly" ? <span>Monthly {this.state.monthlydate} Days</span> : null}{" "}
                      </span>
                    </Col>
                  ) : null}
                  <Row className="mt-3">
                    <Col xs="12" md="6">
                      <b>Notes :</b>
                      {this.state.notes}{" "}
                    </Col>
                  </Row>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between">
                <Button color="secondary" onClick={this.EditModelClose} title="Close">
                  <i className="fa fa-close" />
                </Button>
                <div>
                  {this.state.emplist.sttus !== 10 ? (
                    <Button
                      color="success"
                      className="ml-2"
                      onClick={() => {
                        this.props.history.push({
                          pathname: "/agency/requestemployees",
                          state: { rowid: { _id: this.state.id, status: this.state.status } }
                        });
                      }}
                      title="View Assign Employee"
                    >
                      <i className="fa fa-eye" />
                    </Button>
                  ) : null}
                  {this.state.emplist.status < 3 ? (
                    <Fragment>
                      {this.state.emplist.status !== 10 ? (
                        <Button color="primary" className="ml-2" onClick={this.EditnestedModal} title="Edit">
                          <i className="fa fa-edit" />
                        </Button>
                      ) : null}
                    </Fragment>
                  ) : null}
                </div>
              </ModalFooter>
            </Fragment>
          )}
        </Modal>
        <Modal isOpen={this.state.shifttemplatemodel} toggle={this.shifttemplateModel} className={"modal-sm " + this.props.className}>
          <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmitshifttemplate}>
            <ModalHeader toggle={this.shifttemplateModel}>Add Shift</ModalHeader>
            <ModalBody>
              <Row>
                <Col xs="12" md="12">
                  <AvGroup>
                    <Label>Shift Title</Label>
                    <AvInput
                      type="text"
                      name="shifttemplatename"
                      placeholder="Enter Shift Title"
                      onChange={this.onChange}
                      value={this.state.shifttemplatename}
                      required
                      autoComplete="name"
                    />
                    <AvFeedback>This is required!</AvFeedback>
                  </AvGroup>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="6">
                  {this.state.selecterror1 ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                  <Select
                    className={this.state.selecterror1 ? "custom_select_validation" : null}
                    name="from"
                    value={this.state.shifttemplatefrom}
                    options={this.state.timelist}
                    onChange={this.selectstarttimeChange1}
                  />
                  {this.state.selecterror1 ? <div className="error-color"> This is required!</div> : null}
                </Col>
                <Col xs="12" md="6">
                  {this.state.selectenderror1 ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                  <Select
                    className={this.state.selectenderror1 ? "custom_select_validation" : null}
                    name="form-field-name2"
                    value={this.state.shifttemplateto}
                    options={this.state.timelist}
                    onChange={this.selectendtimeChange1}
                  />
                  {this.state.selectenderror1 ? <div className="error-color"> This is required!</div> : null}
                </Col>
                <Col xs="12" md="6">
                  {this.state.selectbreakerror1 ? (
                    <Label className="error-color">
                      {" "}
                      Break <small>(In Minutes)</small>
                    </Label>
                  ) : (
                    <Label>
                      Break <small>(In Minutes)</small>
                    </Label>
                  )}
                  <Select
                    className={this.state.selectbreakerror1 ? "custom_select_validation" : null}
                    name="form-field-name2"
                    value={this.state.shifttemplatebreak}
                    options={this.state.breaklist}
                    onChange={this.selectbreaktimeChange1}
                  />
                  {this.state.selectbreakerror1 ? <div className="error-color"> This is required!</div> : null}
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter className="d-flex justify-content-between">
              <Button color="secondary" onClick={this.shifttemplateModel}>
                <i className="fa fa-close" />
              </Button>
              <Button type="submit" color="success" title="Submit" onClick={this.selectfield1}>
                <i className="fa fa-check" />
              </Button>{" "}
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

export default Shiftlist;
