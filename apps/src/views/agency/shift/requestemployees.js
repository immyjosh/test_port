import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { Link } from "react-router-dom";
import ReactDOM from "react-dom";
import "react-select/dist/react-select.css";
import { toast, ToastContainer } from "react-toastify";
import Rating from "react-rating";
import { Badge, Button, Card, CardBody, CardFooter, CardHeader, Col, Row, Input, InputGroup, UncontrolledTooltip, Table, Modal, ModalHeader, ModalBody, ModalFooter, Label } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import RequestMapView from "./requestmap";
import Pagination from "react-js-pagination";
import moment from "moment";
import {AvGroup, AvFeedback, AvInput, AvForm, AvField} from "availity-reactstrap-validation";
import { breaklist, timelist, monthlydates } from "../../common/utils";
import "react-dates/initialize";
import { SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import FileSaver from "file-saver";
import {Editor} from "@tinymce/tinymce-react/lib/es2015";
import {Settings} from "../../../api/key";

const tStyle = {
  cursor: "pointer"
};


class RequestEmployees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      job_type: "",
      value: [],
      activePage: 1,
      pageRangeDisplayed: 4,
      sortOrder: true,
      bulk: [],
      count: 0,
      pages: "",
      currPage: 25,
      tableOptions: {
        search: "",
        filter: "all",
        page: {
          history: "",
          current: 1
        },
        order: "",
        field: "",
        limit: 10,
        skip: 0,
        shiftId: this.props && this.props.location && this.props.location.state && this.props.location.state.rowid._id
      },
      worklist: [],
      employee_requested: [],
      shiftId: "",
      employeeId: "",
      empcurrPage: "",
      mapviewShow: true,
      deletedisable: true,
      profilemodal: false,
      Profile_details: "",
      Profile_name: "",
      Profile_email: "",
      Profile_job_type: "",
      Profile_locations: "",
      Profile_holiday_allowance: "",
      Profile_joining_date: "",
      Profile_final_date: "",
      Profile_address: "",
      Profile_avatar: "",
      Profile_username: "",
      Profile_phone: "",
      Profile_status: "",
      emplist_status: "",
      RequestMap: false,
      RowID: "",
      job_type1: "",
      RowIDC: "",
      Shift_ID: "",
      notes: "",
      breaktime1: "",
      branch: ""
    };
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ Shift_ID: this.props.location.state.rowid._id });
    this.setState({ timelist, breaklist, monthlydates });
    request({
      url: "/agency/shift/view",
      method: "POST",
      data: { id: this.state.Shift_ID || this.props.location.state.rowid._id }
    })
      .then(res => {
        if (res.status === 1) {
          const brdata = res.response.result[0];
          const clientDetails =
            brdata.branch_data &&
            brdata.branch_data.length &&
            brdata.branch_data.map(client => {
              const { branches } = client;
              return {
                lat: branches.branchlat,
                lng: branches.branchlng,
                name: res.response.result ? res.response.result[0].client : "",
                email: res.response.result ? res.response.result[0].client_data[0].email : "",
                avatar: res.response.result ? res.response.result[0].client_data[0].avatar : "",
                locations: res.response.result ? res.response.result[0].locations : "",
                job_type: res.response.result ? res.response.result[0].job_type : "",
                label: "C"
              };
            });
          this.setState({
            emplist: res.response.result[0],
            employee_requested: res.response.result[0].employee_requested,
            // pages: res.response.fullcount,
            // currPage: res.response.length
            mapInfo: {
              ...brdata,
              client: clientDetails
            }
          });
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const evestarthr = this.state.emplist.starttime / 3600;
          const splithr = evestarthr.toString().split(".");
          const startsec = splithr[1];
          const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
          const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
          const eveendhr = this.state.emplist.endtime / 3600;
          const splitendhr = eveendhr.toString().split(".");
          const endsec = splitendhr[1];
          const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
          const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
          this.setState({
            RowID: this.state.Shift_ID || this.props.location.state.rowid._id,
            shiftId: this.state.emplist._id,
            title: this.state.emplist.title,
            breaktime: this.state.emplist.breaktime / 60 + " Minutes",
            end_date: moment(this.state.emplist.end_date).format("DD-MM-YYYY"),
            endtime: endtimehr,
            start_date: moment(this.state.emplist.start_date).format("DD-MM-YYYY"),
            starttime: starttimehr,
            location: this.state.emplist.locations,
            client: this.state.emplist.client,
            client_email: res.response.result ? res.response.result[0].client_data[0].email : "",
            client_data: res.response.result ? res.response.result[0].client_data[0] : "",
            branch: this.state.emplist.branch_data[0].branches.branchname,
            job_type: this.state.emplist.job_type,
            latefee: this.state.emplist.latefee,
            notes: this.state.emplist.notes,
            shift_type: this.state.emplist.shift_type
          });
          this.populateData();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        toast.error(err);
      });
    request({
      url: "/agency/jobtype/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          jobtypelist: res.response.result || {}
        });
        const jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
          value: emp.job_id,
          label: emp.name,
          client: emp.client
        }));
        this.setState({
          jobtypelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/shifttemplate/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({ shifttemplatelist: res.response.result });
        const shifttemplatelistselect = this.state.shifttemplatelist.map((emp, key) => ({
          value: emp._id,
          label: emp.name
        }));
        this.setState({
          shifttemplatelistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/client/locations",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          clientlist: res.response.result || {},
          branchlist: res.response.result.length ? res.response.result.map(list => list.branches[0]) : []
        });
        const clientlistselect = this.state.clientlist.map((emp, key) => ({
          value: emp._id,
          label: emp.companyname
        }));
        this.setState({
          clientlistselect
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/emailtemplate/selecttypedata",
      method: "POST",
      data: { type: "profile" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({ emailtemplatelist: res.response.result });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  populateData() {
    request({
      url: "/agency/shift/get_employees",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          employee_assign: res.response.result,
          employee_requested: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.result.length
        });
      }
    });
  }
  onChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    e.persist();
    this.setState({ [e.target.name]: value });
  };
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["locations", "email", "name", "employee_ratings", "distance"];
    for (const i in id) {
      if (i) {
        document.getElementById(id[i]).className = sorticondef;
      }
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      }
    });
    this.populateData();
  }

  RequestEmployeetoastId = "RequestEmployee";
  nodify() {
    if (!toast.isActive(this.RequestEmployeetoastId)) {
      this.RequestEmployeetoastId = toast.warn("Updated");
    }
  }
  RequestEmployeenodifytoastId = "RequestEmployee";
  adminsavenodify() {
    if (!toast.isActive(this.RequestEmployeenodifytoastId)) {
      this.RequestEmployeenodifytoastId = toast.success("Successfully Saved!");
    }
  }
  assignShift = item => {
    request({
      url: "/agency/assign_shift",
      method: "POST",
      data: {
        shiftId: this.state.shiftId,
        employeeId: item.employee,
        status: this.state.emplist.status,
        employeeData: item
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.componentDidMount();
          setTimeout(() => {
            this.componentDidMount();
          }, 3000);
          this.closeprofileModal();
          this.setState({ profilemodal: false, RequestMap: true, RowIDC: this.state.RowID });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  };
  assignShiftOther = item => {
    request({
      url: "/agency/assign_shift",
      method: "POST",
      data: {
        shiftId: this.state.shiftId,
        employeeId: item.employee,
        status: this.state.emplist.status,
        employeeData: item
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.componentDidMount();
          setTimeout(() => {
            this.componentDidMount();
          }, 3000);
          this.closeprofileModal();
          this.setState({ profilemodal: false, RequestMap: true, RowIDC: this.state.RowID });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  };

  checkbox(index, id) {
    const c = document.getElementById(index);
    this.setState({
      singlecheck: true
    });
    const ca = document.getElementById("checkall");
    let count = this.state.count;
    const len = this.state.currPage;
    const bulk = this.state.bulk;
    if (c.checked === true) {
      count++;
      bulk.push(id);
      this.setState({ count, bulk });
    } else {
      count--;
      const i = bulk.indexOf(id);
      bulk.splice(i, 1);
      this.setState({ count, bulk });
    }
    if (count === len) {
      ca.checked = true;
      this.checkall("checkall", len);
    } else {
      ca.checked = false;
    }
  }
  checkall(id, len) {
    const c = document.getElementById(id);
    const val = [];
    if (c.checked) {
      this.setState({ count: len });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = true;
        val.push(document.getElementById(i).value);
      }
      this.setState({ bulk: val });
    } else {
      this.setState({ count: 0 });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = false;
      }
      this.setState({ bulk: [] });
    }
  }
  sendtoselected(val) {
    request({
      url: "/agency/send_shift_requests",
      method: "POST",
      data: {
        shiftId: this.state.Shift_ID || this.props.location.state.rowid._id,
        employees: val
      }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response.result);
        this.componentDidMount();
        setTimeout(() => {
          this.componentDidMount();
        }, 2500);
        this.setState({ RequestMap: true, RowIDC: this.state.RowID });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  openRequest = () => {
    let toastId = "Open_Request_01";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/agency/open_request_to_all",
      method: "POST",
      data: {
        shiftId: this.props.location.state.rowid._id || this.state.Shift_ID
      }
    }).then(res => {
      if (res.status === 1) {
        // toast.success(res.response.result);
        this.componentDidMount();
        setTimeout(() => {
          this.componentDidMount();
        }, 3000);
        this.setState({ RequestMap: true, RowIDC: this.state.RowID });
        toast.update(toastId, { render: res.response.result, type: toast.TYPE.SUCCESS, autoClose: 2500 });
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };

  viewprofileModal = item => {
    this.setState({
      profilemodal: !this.state.profilemodal,
      Profile_details: item,
      Profile_name: item.name,
      Profile_username: item.username,
      Profile_phone: item.phone,
      Profile_email: item.email,
      Profile_locations: item.locations,
      Profile_holiday_allowance: item.holiday_allowance,
      Profile_joining_date: moment(item.joining_date).format("DD-MM-YYYY"),
      Profile_final_date: moment(item.final_date).format("DD-MM-YYYY"),
      Profile_address: item.address,
      Profile_job_type: item.job_type,
      Profile_avatar: item.avatar,
      Profile_status: item.status,
      emplist_status: this.state.emplist.status
    });
  };
  closeprofileModal = item => {
    this.setState({
      profilemodal: !this.state.profilemodal,
      Profile_details: "",
      Profile_name: "",
      Profile_username: "",
      Profile_phone: "",
      Profile_email: "",
      Profile_locations: "",
      Profile_holiday_allowance: "",
      Profile_joining_date: "",
      Profile_final_date: "",
      Profile_address: "",
      Profile_job_type: "",
      Profile_avatar: "",
      Profile_status: "",
      emplist_status: ""
    });
  };
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  EditModelOpen = values => {
    this.setState({
      eventlarge: !this.state.eventlarge
    });
    this.eventset(values);
  };
  eventset = values => {
    request({
      url: "/agency/shift/view",
      method: "POST",
      data: { id: values }
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            edit: true,
            starttime: res.response.result[0].starttime,
            endtime: res.response.result[0].endtime,
            breaktime1: res.response.result[0].breaktime,
            shift_type: res.response.result[0].shift_type,
            id: res.response.result[0]._id,
            start_date1: moment(res.response.result[0].start_date),
            shift_option: res.response.result[0].shift_option,
            latefee: res.response.result[0].latefee,
            title: res.response.result[0].title,
            status: res.response.result[0].status,
            client1: res.response.result[0].client_data[0]._id || "",
            branch: res.response.result[0].branch,
            notes: res.response.result[0].notes,
            job_type1: res.response.result[0].jobtype_data[0]._id || "",
            location1: res.response.result[0].location_data[0]._id || ""
          });
          const filteredlist = this.state.clientlist.filter(list => list._id === this.state.client1);
          const branchlistselected = filteredlist[0].branches.map((branch, key) => ({
            value: branch._id,
            label: branch.branchname,
            locationID: branch.branchlocation
          }));
          this.setState({
            branchlistselected: branchlistselected
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => toast.error(err));
  };
  EditModelClose = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      title: "",
      location1: "",
      starttime: "",
      endtime: "",
      breaktime1: "",
      start_date1: null,
      end_date1: null,
      shift_type: "",
      job_type1: "",
      shift_option: "",
      shift_template: "",
      start_date_time: null,
      end_date_time: null,
      edit: false,
      client1: "",
      latefee: "",
      notes: "",
      branch: ""
    });
    this.componentDidMount();
  };
  selectjoblocation = value => {
    if (value) {
      const jobtypelistselect = this.state.jobtypelist.map((emp, key) => ({
        value: emp.job_id,
        label: emp.name,
        client: emp.client
      })).filter(list => list.client === value.value);
      const filteredlist = this.state.clientlist.filter(list => list._id === value.value);
      const branchlistselected = filteredlist[0].branches.map((branch, key) => ({
        value: branch._id,
        label: branch.branchname,
        locationID: branch.branchlocation
      }));
      if (branchlistselected.length === 1) {
        this.setState({ client1: value.value, jobtypelistselect, branchlistselected, branch: branchlistselected[0].value, location1: branchlistselected[0].locationID });
      }
      if (branchlistselected.length > 1) {
        this.setState({ client1: value.value, jobtypelistselect, branchlistselected });
      }
    }
  };
  branchselect = value => {
    if (value) {
      this.setState({ branch: value.value, locations: value.locationID });
    }
  };
  shifttemplateChange = value => {
    this.setState({ shift_template: value });
    this.state.shifttemplatelist.filter((shift, key) => {
      if (value && shift._id === value.value) {
        this.setState({
          starttime: shift.from,
          endtime: shift.to,
          breaktime1: shift.break
        });
      }
      return true;
    });
  };
  saveChanges = value => {
    if (value) {
      this.setState({ job_type1: value.value });
    }
  };
  OnFormEdit = (e, values) => {
    if (this.state.radiodata === "daily") {
      this.setState({ option_data: { daily: "daily" } });
    } else if (this.state.radiodata === "weekday") {
      this.setState({ option_data: { weekday: "weekday" } });
    } else if (this.state.radiodata === "weekly") {
      this.setState({
        option_data: {
          weekly: {
            0: this.state.sun,
            1: this.state.mon,
            2: this.state.tue,
            3: this.state.wed,
            4: this.state.thu,
            5: this.state.fri,
            6: this.state.sat
          }
        }
      });
    } else if (this.state.radiodata === "rotational") {
      this.setState({ option_data: { rotational: { x: this.state.xday, y: this.state.yday } } });
    } else if (this.state.radiodata === "monthly") {
      this.setState({ option_data: { monthly: this.state.monthlydate } });
    }
    request({
      url: "/agency/shift/save",
      method: "POST",
      data: {
        id: this.state.id,
        // title: this.state.title,
        locations: this.state.location1,
        client: this.state.client1,
        notes: this.state.notes,
        branch: this.state.branch,
        job_type: this.state.job_type1,
        starttime: this.state.starttime.value || this.state.starttime,
        endtime: this.state.endtime.value || this.state.endtime,
        breaktime: this.state.breaktime1 ? this.state.breaktime1 || this.state.breaktime1.value : 0,
        start_date: this.state.start_date1,
        end_date: this.state.end_date1,
        shift_type: this.state.shift_type,
        shift_option: this.state.shift_option,
        latefee: this.state.latefee || 0,
        status: this.state.status,
        monthly_select_day: this.state.monthlydate,
        option_data: this.state.option_data
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success("Shift Updated!");
          this.setState({
            eventlarge: !this.state.eventlarge,
            slotdata: [],
            start_date: null,
            end_date: null,
            shifttitle: "",
            locations: "",
            starttime: "",
            endtime: "",
            breaktime: "",
            shift_type: "",
            job_type: "",
            shift_option: "",
            latefee: "",
            shift_template: "",
            option_data: [],
            start_date_time: null,
            end_date_time: null,
            client: "",
            notes: "",
            branch: ""
          });
          this.componentDidMount();
          this.props.history.push({
            pathname: "/agency/requestemployees",
            state: { rowid: { _id: res.shiftId, status: res.shift_status } }
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  shifttemplateModel = () => {
    this.setState({
      shifttemplatemodel: !this.state.shifttemplatemodel
    });
  };
  selectstarttimeChange1 = value => {
    if (value) {
      this.setState({ shifttemplatefrom: value.value, selecterror1: false });
    } else {
      this.setState({ shifttemplatefrom: "", selecterror1: true });
    }
  };
  selectendtimeChange1 = value => {
    if (value) {
      this.setState({ shifttemplateto: value.value, selectenderror1: false });
    } else {
      this.setState({ shifttemplateto: "", selectenderror1: true });
    }
  };
  selectbreaktimeChange1 = value => {
    if (value) {
      this.setState({ shifttemplatebreak: value.value, selectbreakerror1: false });
    } else {
      this.setState({ shifttemplatebreak: "", selectbreakerror1: true });
    }
  };
  selectfield1 = () => {
    if (this.state.shifttemplatefrom === "") {
      this.setState({
        selecterror1: true
      });
    }
    if (this.state.shifttemplateto === "") {
      this.setState({
        selectenderror1: true
      });
    }
    if (this.state.shifttemplatebreak === "") {
      this.setState({
        selectbreakerror1: true
      });
    }
  };
  profileDownloadPDF = value => {
    let toastId = "Down_Profile_01";
    let name = this.state.Profile_details.name;
    if (value === "download") {
      client.defaults.responseType = "blob";
      toastId = toast.info("Downloading......", { autoClose: false });
    } else {
      client.defaults.responseType = "json";
      toastId = toast.info("Sending......", { autoClose: false });
    }
    request({
      url: "/agency/employee/downloadpdf",
      method: "POST",
      data: {
        id: this.state.Profile_details.employee,
        for: value
      }
    })
      .then(res => {
        if (res.status === 1) {
          if ((res.msg = "Mail Sent")) {
            // toast.success(res.msg);
            toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
          }
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        } else {
          if (res) {
            const file = new Blob([res], { type: "application/pdf" });
            const profile = name ? name.replace(" ", "_") : "Employee_Profile";
            FileSaver(file, `${profile}.pdf`);
            toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            // toast.success("Downloaded");
            this.componentDidMount();
          }
          client.defaults.responseType = "json";
        }
      })
      .catch(error => {
        client.defaults.responseType = "json";
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
  };
  profileMailPDF = () => {
    let toastId = "mail01_Profile_01";
    client.defaults.responseType = "json";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/agency/employee/downloadpdf",
      method: "POST",
      data: {
        id: this.state.Profile_details.employee,
        for: "mail",
        client_email: this.state.client_email,
        email_content: this.state.email_content,
        email_subject: this.state.email_subject,
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.MailModalOpen();
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      })
      .catch(error => {
        toast.error(error);
      });
  };
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ starttime: value.value, selecterror: false });
    } else {
      this.setState({ starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ endtime: value.value, selectenderror: false });
    } else {
      this.setState({ endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktime1: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktime1: "", selectbreakerror: true });
    }
  };
  selectfield = () => {
    if (this.state.starttime === "") {
      this.setState({
        selecterror: true
      });
    }
    if (this.state.endtime === "") {
      this.setState({
        selectenderror: true
      });
    }
    if (this.state.breaktime1 === "") {
      this.setState({
        selectbreakerror: true
      });
    }
  };
  selectdateChange = value => {
    if (value) {
      const values = value.map(value => value.value);
      this.setState({ monthlydate: values });
    } else {
      this.setState({ monthlydate: "" });
    }
  };

  DeleteShift = id => {
    request({
      url: "/agency/shift/deleteshift",
      method: "POST",
      data: { id }
    })
      .then(res => {
        if (res.status === 1) {
          toast.success(res.response);
          this.props.history.push("/agency/shiftlist");
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.error(error);
      });
  };
  unAssign(item) {
    request({
      url: "/agency/unassign_shift",
      method: "POST",
      data: {
        id: this.state.Shift_ID,
        employeeid: item.employee
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          toast.success("Shift Cancelled");
        } else if (res.status === 0) {
          this.componentDidMount();
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  }
  MailModalOpen = () => {
    if (!this.state.mailmodal) {
      const typeid = this.state.emailtemplatelist.filter(list => list.default_mail).map(list => list._id)[0];
      this.Templatelist(typeid);
      this.setState({ typeid });
    }
    this.setState({
      mailmodal: !this.state.mailmodal
    });
  };
  onChangeSelect = e => {
    if (e) {
      this.Templatelist(e.target.value);
      this.setState({ [e.target.name]: e.target.value });
    }
  };
  Templatelist(value) {
    request({
      url: "/agency/emailtemplate/getemailcontent",
      method: "POST",
      data: { id: value, client: this.state.client_data && this.state.client_data.companyname, employee: this.state.employee_name }
    }).then(res => {
      if (res.status === 1) {
        const data = res.response.result;
        this.setState({
          email_content: data.html,
          email_subject: data.subject
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  render() {
    const { emailtemplatelist, email_content, client_email, typeid, email_subject } = this.state;
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    // const { tableData } = this.state;
    // let current = this.state.tableOptions.page.current;
    const limit = this.state.tableOptions.limit;
    const len = Math.ceil(this.state.pages / limit);
    const arr = [];
    for (let i = 1; i <= len; i++) {
      arr.push(i);
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Assign Employee
            {this.state.emplist && this.state.emplist.status < 3 ? (
              <div className="card-actions" style={tStyle} onClick={id => this.EditModelOpen(this.state.Shift_ID || this.props.location.state.rowid._id)}>
                <button>
                  <i className="fa fa-edit" /> Edit Shift
                  {/* <small className="text-muted" />*/}
                </button>
                <button onClick={() => this.DeleteShift(this.state.shiftId)}>
                  <i className="fa fa-trash" /> Delete Shift
                  {/* <small className="text-muted" />*/}
                </button>
              </div>
            ) : null}
          </CardHeader>
          <CardBody>
            {this.state.emplist ? (
              <Fragment>
                <Row>
                  <Col xs="12" md="8">
                    <div>
                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-bars" /> Shift :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.title} </span>
                          </Col>
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-user" aria-hidden="true" /> Client :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.client} </span>
                          </Col>
                        </div>
                      </Row>
                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-map-marker" /> Area :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.location} </span>
                          </Col>
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-map-marker" /> Work Location {/* branch */}:
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.branch} </span>
                          </Col>
                        </div>
                      </Row>
                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-arrow-circle-right" /> Job Role :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.job_type} </span>
                          </Col>
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-calendar"> </i> Date :
                              </b>{" "}
                            </span>
                            <span className="po-right">{this.state.start_date}</span>
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-clock-o" /> Time :
                              </b>
                            </span>
                            <span className="po-right">
                              {" "}
                              {this.state.starttime} - {this.state.endtime}{" "}
                            </span>
                          </Col>
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                {" "}
                                <i className="fa fa-clock-o" /> Break :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.breaktime} </span>
                          </Col>
                        </div>
                      </Row>

                      <Row>
                        <div className="dash-points">
                          <Col xs="12" md="6">
                            <span className="po-left">
                              {" "}
                              <b>
                                <i className="fa fa-money" aria-hidden="true" /> Late Booking Fee :
                              </b>{" "}
                            </span>
                            <span className="po-right"> {this.state.latefee ? "Yes" : "No"} </span>
                          </Col>
                          {this.state.emplist ? (
                            <Col xs="12" md="6">
                              <span className="po-left">
                                {" "}
                                <b>
                                  {" "}
                                  <i className="fa fa-certificate" aria-hidden="true" /> Shift Status :
                                </b>
                              </span>
                              <span className="po-right">
                                {this.state.emplist.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                                {this.state.emplist.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                                {this.state.emplist.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                                {this.state.emplist.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                                {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                                {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                                {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                                {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                                {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                                {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                              </span>
                            </Col>
                          ) : null}
                        </div>
                      </Row>

                      {this.state.shift_type === "multiple" ? (
                        <Row>
                          <div className="dash-points">
                            <Col xs="12" md="6">
                              <span className="po-left">
                                {" "}
                                <b>
                                  {" "}
                                  <i className="fa fa-comment-o" aria-hidden="true" /> Notes :
                                </b>{" "}
                              </span>
                              <span className="po-right"> {this.state.notes} </span>
                            </Col>
                            <Col xs="12" md="6">
                              <span className="po-left">
                                {" "}
                                <b>
                                  <i className="fa fa-clock-o" /> Date :
                                </b>
                              </span>
                              <span className="po-right"> {this.state.start_date} </span>
                            </Col>
                            {/* <Col xs="12" md="4">*/}
                            {/* <b>End Date:</b> {this.state.end_date}*/}
                            {/* </Col>*/}
                            <Col xs="12" md="6">
                              <span className="po-left">
                                {" "}
                                <b>
                                  <i className="fa fa-filter" aria-hidden="true" /> Type :
                                </b>{" "}
                              </span>
                              <span className="po-right"> {this.state.shift_type} </span>
                            </Col>
                          </div>
                        </Row>
                      ) : null}
                    </div>
                    <div>
                      {this.state && this.state.emplist && this.state.emplist.status === 1 ? (
                        <div>
                          <div className="row d-flex justify-content-between">
                            <div className="col-lg-7">
                              <Button className="btn-open" id="open_shiftss" onClick={this.openRequest}>
                                Open Shift
                              </Button>
                              <UncontrolledTooltip placement="top" target="open_shiftss">
                                Open Shift (Send To All)
                              </UncontrolledTooltip>
                            </div>
                            <div className="col-lg-5 pr-0">
                              <InputGroup>
                                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary col-lg-4">
                                  <option>All</option>
                                </Input>
                                <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className=" col-lg-6" />
                                <Button
                                  className="rounded-0"
                                  color="primary"
                                  id="clear"
                                  onClick={() => {
                                    ReactDOM.findDOMNode(this.refs.search).value = "";
                                    this.search("");
                                  }}
                                >
                                  <i className="fa fa-remove" />
                                </Button>
                                <UncontrolledTooltip placement="top" target="clear">
                                  Clear
                                </UncontrolledTooltip>
                              </InputGroup>
                            </div>
                          </div>
                          <div className="table-responsive scroll-ptions mt-2">
                            <Table hover bordered responsive size="sm">
                              <thead>
                                <tr>
                                  <th>
                                    <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.currPage)} />
                                  </th>
                                  <th>S.No.</th>
                                  <th
                                    onClick={() => {
                                      this.sort("name");
                                    }}
                                  >
                                    Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("email");
                                    }}
                                  >
                                    Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("distance");
                                    }}
                                  >
                                    Distance <i style={{ paddingLeft: "25px" }} className={sorticondef} id="distance" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("employee_ratings");
                                    }}
                                  >
                                    Rating <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee_ratings" />
                                  </th>
                                  <th
                                    onClick={() => {
                                      this.sort("locations");
                                    }}
                                  >
                                    Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                                  </th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.employee_assign && this.state.employee_assign.length ? (
                                  this.state.employee_assign.map((item, i) => (
                                    <tr key={i}>
                                      <td>
                                        <input type="checkbox" className="checkbox1" id={i} value={item.employee} onClick={() => this.checkbox(i, item.employee)} />
                                      </td>
                                      <td>{this.state.tableOptions.skip + i + 1}</td>
                                      <td>{item.name}</td>
                                      <td>{item.email}</td>
                                      <td>{item.distance}</td>
                                      <td>
                                        <Rating initialRating={item.employee_ratings} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} value={item.employee_ratings} readonly />
                                      </td>
                                      <td>
                                        {" "}
                                        {item.locations.map((list, key) => (
                                          <span key={key}>
                                            {list} <Fragment>,</Fragment>
                                          </span>
                                        ))}
                                      </td>
                                      <td>
                                        {this.state.emplist.status === 1 ? (
                                          <Fragment>
                                            {/* <Badge color="success">Available</Badge>*/}
                                            <Button type="submit" size="sm" color="success ml-1 pull-left" title="Assign" onClick={items => this.assignShift(item)}>
                                              Assign <i className="fa fa-check-circle" />
                                            </Button>{" "}
                                          </Fragment>
                                        ) : (
                                          <Fragment>
                                            {item.status === 1 || item.status === 2 ? (
                                              <Fragment>
                                                {this.state.emplist.status <= 3 ? (
                                                  <Fragment>
                                                    <Badge color="info">Shift Accepted</Badge>
                                                    <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShiftOther(item)}>
                                                      Assign <i className="fa fa-check-circle" />
                                                    </Button>{" "}
                                                  </Fragment>
                                                ) : null}
                                                {this.state.emplist.status > 3 && this.state.emplist.status < 5 ? <Badge color="info">Assigned</Badge> : null}
                                                {this.state.emplist_status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                                                {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                                                {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                                                {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                                                {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                                                {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                                              </Fragment>
                                            ) : (
                                              <Fragment>{this.state.emplist && this.state.emplist.status < 4 ? <Badge color="warning">Requested</Badge> : null}</Fragment>
                                            )}
                                          </Fragment>
                                        )}
                                        <Button size="sm" color="info" className="ml-2 pull-right" onClick={items => this.viewprofileModal(item)}>
                                          <i className="fa fa-user-circle" />
                                        </Button>
                                      </td>
                                    </tr>
                                  ))
                                ) : (
                                  <tr className="text-center">
                                    <td colSpan={9}>
                                      <h2>
                                        <strong>No record available</strong>
                                      </h2>
                                    </td>
                                  </tr>
                                )}
                              </tbody>
                            </Table>
                            {this.state.bulk.length !== 0 ? (
                              <div>
                                <Button id="delete" size="sm" color="success" title="Assign" onClick={() => this.sendtoselected(this.state.bulk)}>
                                  Send Request <i className="fa fa-check" />
                                </Button>
                              </div>
                            ) : (
                              <Button id="delete" size="sm" color="success" title="Assign" onClick={() => this.sendtoselected(this.state.bulk)} disabled>
                                Send Request <i className="fa fa-check" />
                              </Button>
                            )}
                          </div>
                          <br />
                          <nav className="float-left">
                            <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                              <option>10</option>
                              <option>25</option>
                              <option>50</option>
                              <option>100</option>
                              <option>200</option>
                            </Input>
                          </nav>
                          <nav className="float-right">
                            <div>
                              <Pagination
                                prevPageText="Prev"
                                nextPageText="Next"
                                firstPageText="First"
                                lastPageText="Last"
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.tableOptions.limit}
                                totalItemsCount={this.state.pages}
                                pageRangeDisplayed={this.state.pageRangeDisplayed}
                                onChange={this.paginate}
                              />
                            </div>
                          </nav>
                        </div>
                      ) : (
                        <div className="table-responsive mt-2">
                          <Table hover bordered responsive size="sm">
                            <thead>
                              <tr>
                                {/* <th>
                            <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.empcurrPage)} />
                          </th> */}
                                <th>S.No.</th>
                                <th
                                  onClick={() => {
                                    this.sort("name");
                                  }}
                                >
                                  Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("email");
                                  }}
                                >
                                  Email <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("locations");
                                  }}
                                >
                                  Area <i style={{ paddingLeft: "25px" }} className={sorticondef} id="locations" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("distance");
                                  }}
                                >
                                  Distance <i style={{ paddingLeft: "25px" }} className={sorticondef} id="distance" />
                                </th>
                                <th
                                  onClick={() => {
                                    this.sort("employee_ratings");
                                  }}
                                >
                                  Rating <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee_ratings" />
                                </th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.employee_requested && this.state.employee_requested.length ? (
                                this.state.employee_requested.map((item, i) => (
                                  <tr key={i}>
                                    <td>{this.state.tableOptions.skip + i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td>
                                      {" "}
                                      {item.locations &&
                                        item.locations.map((list, key) => (
                                          <span key={key}>
                                            {list} <Fragment>,</Fragment>
                                          </span>
                                        ))}
                                    </td>
                                    <td>{item.distance}</td>
                                    <td>
                                      <Rating initialRating={item.employee_ratings} emptySymbol="fa fa-star-o fa-2x" fullSymbol="fa fa-star fa-2x" fractions={2} value={item.employee_ratings} readonly />
                                    </td>
                                    <td>
                                      {this.state.emplist.status === 1 ? (
                                        <div>
                                          {/* <Badge color="success">Available</Badge>*/}
                                          <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShift(item)}>
                                            Assign <i className="fa fa-check-circle" />
                                          </Button>{" "}
                                        </div>
                                      ) : (
                                        <Fragment>
                                          {item.status === 2 ? (
                                            <Fragment>
                                              {this.state.emplist.status <= 3 ? (
                                                <Fragment>
                                                  <Badge color="info">Shift Accepted</Badge>
                                                  <Button type="submit" size="sm" color="success ml-1" title="Assign" onClick={items => this.assignShiftOther(item)}>
                                                    Assign <i className="fa fa-check-circle" />
                                                  </Button>{" "}
                                                </Fragment>
                                              ) : null}
                                              {this.state.emplist.status > 3 && this.state.emplist.status < 5 ? (
                                                <>
                                                  <Badge color="info">Assigned</Badge>{" "}
                                                  <Button type="submit" size="sm" color="success ml-1 pull-left" title="Assign" onClick={items => this.unAssign(item)}>
                                                    Un-assign <i className="fa fa-check-circle" />
                                                  </Button>{" "}
                                                </>
                                              ) : null}
                                              {this.state.emplist.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                                              {this.state.emplist.status === 6 ? <Badge color="danger">Shift Completed</Badge> : null}
                                              {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                                              {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                                              {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                                              {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                                            </Fragment>
                                          ) : (
                                            <Fragment>
                                              {this.state.emplist && this.state.emplist.status < 4 ? (
                                                <>
                                                  <Badge color="warning">Requested</Badge>
                                                  <Button type="submit" size="sm" color="success ml-1 pull-left" title="Assign" onClick={items => this.assignShift(item)}>
                                                    Assign <i className="fa fa-check-circle" />
                                                  </Button>{" "}
                                                </>
                                              ) : null}
                                            </Fragment>
                                          )}
                                        </Fragment>
                                      )}
                                      <Button size="sm" color="info" className="ml-2" onClick={items => this.viewprofileModal(item)}>
                                        <i className="fa fa-user-circle" />
                                      </Button>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                <tr className="text-center">
                                  <td colSpan={10}>
                                    <h2>
                                      <strong>No record available</strong>
                                    </h2>
                                  </td>
                                </tr>
                              )}
                            </tbody>
                          </Table>
                          {/* <Button
                              id="delete"
                              size="sm"
                              color="success"
                              title="Assign"
                              onClick={() => this.sendtoselected(this.state.bulk)}
                              disabled={this.state.deletedisable}
                            >
                              Send Request <i className="fa fa-check" />
                            </Button> */}
                        </div>
                      )}
                    </div>
                  </Col>
                  <Col xs="12" md="4" className="p-0">
                    {this.state && this.state.mapInfo ? (
                      <RequestMapView locations={this.state.employee_assign} request={this.state.employee_requested} emplist={this.state.mapInfo} />
                    ) : null}
                  </Col>
                </Row>
              </Fragment>
            ) : null}
          </CardBody>
          <CardFooter>
            <Link to="/agency/shiftlist">
              <Button type="submit" color="secondary" title="Back">
                <i className="fa fa-arrow-left" /> Back
              </Button>
            </Link>
          </CardFooter>
        </Card>
        <Modal isOpen={this.state.profilemodal} className={"modal-lg view_profile"}>
          <ModalHeader toggle={this.closeprofileModal}>Profile</ModalHeader>
          <ModalBody>
            <section className="empl_profle ">
              <div className="fl_flex">
                <div className="pro_lft">
                  <img alt="" src={NodeURL + "/" + this.state.Profile_avatar} />
                </div>
                <div className="pro_rght">
                  <p>{this.state.Profile_name}</p>
                  <p>{this.state.Profile_email}</p>
                  <p>
                    {this.state.Profile_job_type &&
                      this.state.Profile_job_type.map((list, key) => (
                        <Fragment key={key}>
                          {list}
                          <Fragment>,</Fragment>
                        </Fragment>
                      ))}
                  </p>
                </div>
              </div>

              <div className="emp_list">
                <ul className="list_det">
                  <li>
                    <span>Name </span>
                    <span>{this.state.Profile_name} </span>
                  </li>
                  <li>
                    <span>Area</span>
                    <span>
                      {this.state.Profile_locations.length > 0 &&
                        this.state.Profile_locations.map((list, key) => (
                          <span key={key}>
                            {list} <Fragment>,</Fragment>
                          </span>
                        ))}
                    </span>
                  </li>{" "}
                  <li>
                    <span>Phone</span>
                    <span>
                      {" "}
                      {this.state.Profile_phone.code} - {this.state.Profile_phone.number}
                    </span>
                  </li>{" "}
                  <li>
                    <span>Job Role </span>
                    <span>
                      {" "}
                      {this.state.Profile_job_type &&
                        this.state.Profile_job_type.map((list, key) => (
                          <Fragment key={key}>
                            {list}
                            <Fragment>,</Fragment>
                          </Fragment>
                        ))}
                    </span>
                  </li>{" "}
                  <li>
                    <span>User Name </span>
                    <span>{this.state.Profile_username}</span>
                  </li>{" "}
                  <li>
                    <span>Email </span>
                    <span>{this.state.Profile_email}</span>
                  </li>{" "}
                  <li>
                    <span>Joining Date </span>
                    <span>{this.state.Profile_joining_date}</span>
                  </li>
                  <li>
                    <span>Address </span>
                    <span>
                      {this.state.Profile_address.formatted_address} , {this.state.Profile_address.zipcode} .
                    </span>
                  </li>
                </ul>
              </div>
            </section>
          </ModalBody>
          <ModalFooter>
            <Button size="sm" color="info" className="pull-left" onClick={() => this.profileDownloadPDF("download")}>
              <i className="fa fa-download" />
            </Button>
            <Button size="sm" color="info" className="pull-left" onClick={this.MailModalOpen}>
              <i className="fa fa-envelope" />
            </Button>
            <a href="url" download="url">
              {" "}
            </a>
            {this.state.emplist_status === 1 ? (
              <Fragment>
                {/* <Badge color="success">Available</Badge>*/}
                <Button
                  type="submit"
                  size="sm"
                  color="success ml-1"
                  title="Assign"
                  onClick={() => {
                    this.assignShift(this.state.Profile_details);
                  }}
                >
                  Assign <i className="fa fa-check-circle" />
                </Button>{" "}
              </Fragment>
            ) : (
              <Fragment>
                {this.state.Profile_status === 1 || this.state.Profile_status === 2 ? (
                  <Fragment>
                    {this.state.emplist_status <= 3 ? (
                      <Fragment>
                        <Badge color="info">Shift Accepted</Badge>
                        <Button
                          type="submit"
                          size="sm"
                          color="success ml-1"
                          title="Assign"
                          onClick={() => {
                            this.assignShiftOther(this.state.Profile_details);
                          }}
                        >
                          Assign <i className="fa fa-check-circle" />
                        </Button>{" "}
                      </Fragment>
                    ) : null}
                    {this.state.emplist_status > 3 && this.state.emplist_status < 5 ? <Badge color="info">Assigned</Badge> : null}
                    {this.state.emplist_status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                    {this.state.emplist.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                    {this.state.emplist.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                    {this.state.emplist.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                    {this.state.emplist.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                    {this.state.emplist.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}
                  </Fragment>
                ) : (
                  <Fragment>{this.state.emplist && this.state.emplist.status < 4 ? <Badge color="warning">Requested</Badge> : null}</Fragment>
                )}
              </Fragment>
            )}
            <Button size="sm" color="secondary" onClick={this.closeprofileModal}>
              <i className="fa fa-close" />
            </Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.eventlarge} className={"modal-lg " + this.props.className}>
          {this.state.edit ? (
            <Fragment>
              <ModalHeader toggle={this.EditModelClose}>EDIT SHIFT</ModalHeader>
              <AvForm onValidSubmit={this.OnFormEdit}>
                <ModalBody>
                  <Row>
                    <Col xs="12" md="4">
                      <Label>Client</Label>
                      <Select name="form-field-name2" value={this.state.client1} options={this.state.clientlistselect} onChange={this.selectjoblocation} />
                    </Col>
                    {/* <Col xs="12" md="4">
                      <Label>Location</Label>
                      <Select name="form-field-name2" value={this.state.location1} options={this.state.worklocationlist} onChange={this.worklocationselect} />
                    </Col>*/}
                    <Col xs="12" md="4">
                      <Label>Work Location {/* branch */}</Label>
                      <Select name="branch" value={this.state.branch} options={this.state.branchlistselected} onChange={this.branchselect} />
                    </Col>
                    <Col xs="12" md="4">
                      <Label>Role</Label>
                      <Select name="form-field-name2" value={this.state.job_type1} options={this.state.jobtypelistselect} onChange={this.saveChanges} />
                      {/* <small className="ml-1 cursor-pointer" onClick={this.jobTypeModel}>
                        {" "}
                        <i className="fa fa-info-circle" />
                        <span className="ml-1 cursor-pointer cm-strong">Add New Role</span>
                      </small> */}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="3">
                      <Label>Shift</Label>
                      <Select name="form-field-name2" value={this.state.shift_template} options={this.state.shifttemplatelistselect} onChange={this.shifttemplateChange} />
                      <small className="ml-1 cursor-pointer" onClick={this.shifttemplateModel}>
                        {" "}
                        <i className="fa fa-info-circle" />
                        <span className="ml-1 cursor-pointer cm-strong">Add New Shift</span>
                      </small>
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selecterror ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                      <Select className={this.state.selecterror ? "custom_select_validation" : null} name="starttime" value={this.state.starttime} options={this.state.timelist} onChange={this.selectstarttimeChange} />
                      {this.state.selecterror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectenderror ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                      <Select className={this.state.selectenderror ? "custom_select_validation" : null} name="form-field-name2" value={this.state.endtime} options={this.state.timelist} onChange={this.selectendtimeChange} />
                      {this.state.selectenderror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                    <Col xs="12" md="3">
                      {this.state.selectbreakerror ? (
                        <Label className="error-color">
                          {" "}
                          Break <small>(In Minutes)</small>
                        </Label>
                      ) : (
                        <Label>
                          Break <small>(In Minutes)</small>
                        </Label>
                      )}
                      <Select className={this.state.selectbreakerror ? "custom_select_validation" : null} name="form-field-name2" value={this.state.breaktime1} options={this.state.breaklist} onChange={this.selectbreaktimeChange} />
                      {this.state.selectbreakerror ? <div className="error-color"> This is required!</div> : null}
                    </Col>
                  </Row>
                  <Row className="mt-3">
                    <Col xs="12" md="6" className="p-0">
                    <Col xs="12" md="12">
                      <AvGroup>
                        <Label>Notes :</Label>
                        <AvInput type="textarea" name="notes" placeholder="Enter Notes" onChange={this.onChange} value={this.state.notes} autoComplete="notes" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="12">
                      <AvGroup check>
                        <Label check for="latefee">
                          <AvInput type="checkbox" name="latefee" value={this.state.latefee} checked={this.state.latefee} onChange={this.onChange} /> Late Booking Fee
                        </Label>
                      </AvGroup>
                    </Col>
                    </Col>
                    <Col xs="12" md="6" className="p-0">
                      <Col xs="12" md="12">
                        {this.state.shift_type === "single" ? (
                            <Row>
                              <Col sm="12">
                                <p>Select Date</p>
                                <SingleDatePicker
                                    date={this.state.start_date1}
                                    openDirection={this.state.openDirection}
                                    onDateChange={date => this.setState({ start_date1: date })}
                                    focused={this.state.focused}
                                    onFocusChange={({ focused }) => this.setState({ focused })}
                                    id="start_date"
                                    displayFormat="DD-MM-YYYY"
                                />
                              </Col>
                            </Row>
                        ) : null}
                  </Col>
                    </Col>
                  </Row>
                  {/* <Row>
                    <Col xs="12" md="4">
                      <AvField
                        type="select"
                        name="shift_type"
                        label="Day"
                        placeholder="Select shift_type"
                        onChange={this.onChange}
                        value={this.state.shift_type}
                        validate={{required: {value: true, errorMessage: "This is required!"}}}
                        disabled
                      >
                        <option>Please Select</option>
                        <option value="single">Single Day</option>
                        <option value="multiple">Multiple Days</option>
                      </AvField>
                    </Col>
                  </Row> */}
                </ModalBody>
                <ModalFooter className="justify-content-between modal-gal">
                  <div>
                    <Button color="secondary" onClick={this.EditModelClose} title="Close">
                      <i className="fa fa-close" />
                    </Button>
                    {/* <Button color="secondary" className="ml-3" onClick={this.EditnestedModalview} title="Back to View Details">
                      <i className="fa fa-arrow-left" />
                    </Button> */}
                  </div>
                  <Button color="success" type="submit" title="Save & Continue" id="savecontinue2" onClick={this.selectfield}>
                    {" "}
                    <i className="fa fa-arrow-circle-right" />
                  </Button>{" "}
                  <UncontrolledTooltip placement="left" target="savecontinue2">
                    Save & Continue
                  </UncontrolledTooltip>
                </ModalFooter>
              </AvForm>
            </Fragment>
          ) : null}
        </Modal>
        <Modal isOpen={this.state.mailmodal} className="modal-lg">
          <Fragment>
            <ModalHeader toggle={this.MailModalOpen}>Profile</ModalHeader>
            <AvForm onValidSubmit={this.profileMailPDF}>
              <ModalBody>
                <Row>
                  <Col xs="12" md="6">
                    <AvField name="client_email" onChange={this.onChange} value={client_email} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  <Col xs="12" md="6">
                    <AvField type="select" name="typeid" label="Email Template" onChange={this.onChangeSelect} value={typeid} required>
                      <option>Select</option>
                      {emailtemplatelist && emailtemplatelist.length > 0 ? (
                          <>
                            {emailtemplatelist.map((list, i) => (
                                <option key={i} value={list._id}>
                                  {list.heading}
                                </option>
                            ))}
                          </>
                      ) : null}
                    </AvField>
                  </Col>
                  <Col xs="12" md="12">
                    <AvField name="email_subject" onChange={this.onChange} value={email_subject} placeholder="Subject" label="Subject" type="text" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="12">
                    <p>Message</p>
                    <Editor
                        apiKey={Settings.tinymceKey}
                        initialValue={email_content}
                        init={{
                          plugins: "link image code",
                          toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                          height: 480
                        }}
                        onChange={this.handleEditorChange}
                        value={email_content}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between modal-gal">
                <div>
                  <Button color="secondary" type="button" onClick={this.MailModalOpen} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                </div>
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  <i className="fa fa-envelope-open-o" />
                </Button>{" "}
                <UncontrolledTooltip placement="left" target="savecontinue2">
                  Send Mail
                </UncontrolledTooltip>
              </ModalFooter>
            </AvForm>
          </Fragment>
        </Modal>
      </div>
    );
  }
}

export default RequestEmployees;
