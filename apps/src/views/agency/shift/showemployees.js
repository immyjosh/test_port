import React, { Component } from "react";
import { Card, CardBody, CardHeader, Col } from "reactstrap";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Circle } from "react-google-maps";
// import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import { compose, withProps, lifecycle } from "recompose";
import { Settings } from "../../../api/key";
import request, { NodeURL } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import { withRouter } from "react-router-dom";

const MapComponent = compose(
  withProps({
    googleMapURL: Settings.googleMapURL + Settings.googleMapKey,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  lifecycle({
    componentDidMount() {
      if (this.props.markers.length)
        this.setState({
          zoomToMarkers: map => {
            const bounds = new window.google.maps.LatLngBounds();
            this.props.markers.forEach((child, index) => {
              bounds.extend(new window.google.maps.LatLng(child.lat, child.lng));
            });
            if (map) map.fitBounds(bounds);
          }
        });
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap ref={props.zoomToMarkers} defaultZoom={5} defaultCenter={{ lat: 25.0391667, lng: 121.525 }} defaultOptions={{ gestureHandling: "greedy" }}>
    {/*<MarkerClusterer onClick={props.onMarkerClustererClick} averageCenter enableRetinaIcons gridSize={60}>*/}
       {props.markers.map((marker, i) => (
        <div className="marger1" key={i}>
          <Marker
            className="marger"
            key={marker.id}
            position={{ lat: marker.lat, lng: marker.lng }}
            icon={{
              url: marker && marker.avatar ? NodeURL + "/" + marker.avatar : "",
              scaledSize: { width: 40, height: 40 }
            }}
            onClick={() => props.client(marker)}
          >
            {props.isOpen && props.selectLocation.email === marker.email ? (
              <InfoWindow onCloseClick={() => props.closeModel()}>
                <Col className="full-mapdetails">
                  <Col className="left-mapdetails">
                    <img width="80px" height="80px" alt="" src={NodeURL + "/" + props.selectLocation.avatar} />
                  </Col>
                  <Col className="right-mapdetails">
                    <p className="view-det map-names">
                      <span>{props.selectLocation.title}</span>
                    </p>
                    <p className="view-det job_type">
                      <span>{props.selectLocation.job_type}</span>
                    </p>
                    <p className="view-det phone">
                      {props.selectLocation.phone && props.selectLocation.phone.code && props.selectLocation.phone.number ? (
                        <span>
                          {props.selectLocation.phone.code} - {props.selectLocation.phone.number}
                        </span>
                      ) : null}
                    </p>
                    <p className="view-det location">
                      <span>{props.selectLocation.locations}</span>
                    </p>
                    <p className="view-det email">
                      <span>{props.selectLocation.email}</span>
                    </p>
                    <p className="view-det">
                      <button
                        className="btn btn-info btn-sm"
                        onClick={(id, job) => props.viewpage(props.selectLocation._id, props.selectLocation.job_type)}
                      >
                        View Profile
                      </button>
                    </p>
                  </Col>
                </Col>
              </InfoWindow>
            ) : marker && marker.radius ? (
              <Circle
                center={marker}
                radius={parseInt(marker && marker.radius, 0)}
                onCenterChanged={e => this.onCenterChanged(e)}
                onRadiusChanged={e => this.onRadiusChanged(e)}
                options={{
                  fillColor: "#f00",
                  strokeColor: "#f00"
                }}
              />
            ) : null}
          </Marker>
        </div>
      ))}
    {/*</MarkerClusterer>*/}
  </GoogleMap>
));

class DashboardMap extends Component {
  state = {
    godlist: []
  };

  componentDidMount() {
    request({
      url: "/agency/location/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        // this.setState({ worklist: res.response.result });
        const arealist = res.response.result && res.response.result.length > 0 ? res.response.result : [];
        request({
          url: "/administrators/employee/show_employees",
          method: "POST"
        }).then(res => {
          if (res.status === 1) {
            const employeeList = [];
            res.response.locations.map(location => {
              let employeeInfo = location;
              employeeInfo = {
                ...employeeInfo,
                type: "employee"
              };
              return employeeList.push(employeeInfo);
            });
            const list = employeeList.concat(arealist);
            const godlist =
              list &&
              list.map(list => ({
                lat: list.lat ? list.lat : 51.50853,
                lng: list.lng ? list.lng : -0.12574,
                avatar: list.avatar ? list.avatar : "",
                _id: list._id ? list._id : "",
                email: list.email ? list.email : "",
                locations: list.locations ? list.locations : "",
                job_type: list.job_type ? list.job_type : "",
                phone: list.phone ? list.phone : "",
                label: "S",
                draggable: false,
                center: { lat: 41.878, lng: -87.629 },
                population: 2714856,
                radius: list.radius ? list.radius * 0.000621371192 * 999999 : "",
                type: list.type ? list.type : false,
                title: list.title
              }));
            this.setState({ godlist: godlist || [] });
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  locationDetails = location => {
    this.setState({
      selectLocation: location,
      isOpen: location.type ? true : false
    });
  };

  closeModel = () => {
    this.setState({
      isOpen: false
    });
  };
  viewpage = (id, job_t) => {
      console.log('this.props ', this.props);
      return this.props.history.push({
          pathname: "/agency/viewemployee",
          state: { rowid: id, job_type: job_t[0]._id }
      });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
            <CardHeader className="located-map"><span className="left-loca"><i className="icon-map" /> Locations </span> {/*<span className="right-loca"> <ul> <li className="agency-pro"><span></span> Agency</li> <li className="client-pro"><span></span> Client</li> <li className="emply-pro"><span></span> Employee</li> </ul> </span>*/}</CardHeader>
          <CardBody className="p-0">
            {this.state.godlist.length > 0 ? <MapComponent client={this.locationDetails} selectLocation={this.state.selectLocation || false} markers={this.state.godlist} isOpen={this.state.isOpen} closeModel={this.closeModel} viewpage={this.viewpage} /> : null}
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default withRouter(DashboardMap);
