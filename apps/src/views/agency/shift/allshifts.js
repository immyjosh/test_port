/* eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Badge
} from "reactstrap";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import { withRouter } from "react-router-dom";
import "react-big-calendar/lib/css/react-big-calendar.css";
import request from "../../../api/api";
import { toast } from "react-toastify";

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));

const currDate = new Date();
const currYear = currDate.getFullYear();
const currMonth = currDate.getMonth();

class Calenterview extends Component {
  state = {
    id: "",
    location: "",
    starttime: "",
    daycount: "",
    endtime: "",
    breaktime: "",
    slotdata: [],
    start_date: "",
    end_date: "",
    shift_type: "",
    title: "",
    job_type: "",
    eventlarge: false,
    eventsdata: [],
    date_list: [],
    Weekly_select_date_list: [],
    num: "",
    montofri: 0,
    Weekly_select_date_count: 0,
    monthly_date_count: 0,
    count: 0,
    start_date_time: "",
    end_date_time: "",
    option_data: [],
    monthlydate: "",
    xday: "",
    yday: "",
    sun: false,
    mon: false,
    tue: false,
    wed: false,
    thu: false,
    fri: false,
    sat: false,
    shifttitle: "",
    HolidayModal: false,
    HolidayName: "",
    HolidayDate: "",
    HolidayDesc: "",
    status: "",
    latefee: "",
    notes: "",
    next: <span>Next<i className="fa fa fa-angle-double-right ml-2" ></i></span>,
    previous: <span><i className="fa fa fa-angle-double-left mr-2" ></i>Previous</span>
  };
  componentDidMount() {
    const token = this.props.token_role;
    request({
      url: "/agency/shift/list",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        const evelist = res.response.result || [];
        const eventsdata = evelist.map((eve, key) => {
          const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
          const start_split = (eve.starttime / 3600).toString().split(".");
          const startmins = start_split[1] === undefined ? "00" : (+"0" + "." + start_split[1]) * 60;
          const starttimehr = num.indexOf(start_split[0]) === -1 ? start_split[0] + ":" + startmins : +"0" + "" + start_split[0] + ":" + startmins;
          const start_times = starttimehr.split(":");
          const end_split = (eve.endtime / 3600).toString().split(".");
          const endmins = end_split[1] === undefined ? "00" : (+"0" + "." + end_split[1]) * 60;
          const endtimehr = num.indexOf(end_split[0]) === -1 ? end_split[0] + ":" + endmins : +"0" + "" + end_split[0] + ":" + endmins;
          const end_times = endtimehr.split(":");
          const start_d = new Date(eve.start_date);
          start_d.setHours(Number(start_times && start_times[0]));
          start_d.setMinutes(Number(start_times && start_times[1]));
          const end_d = new Date(eve.start_date);
          end_d.setHours(Number(end_times && end_times[0]));
          end_d.setMinutes(Number(end_times && end_times[1]));
          let title = eve.jobtype_data && eve.jobtype_data.length > 0 ? eve.jobtype_data[0].name : "";
          if (eve.status > 3) {
            title = `${eve.employee_data && eve.employee_data.length > 0 ? eve.employee_data[0].name : ""} - ${eve.branch_data && eve.branch_data.length > 0 ? eve.branch_data[0].branchname : ""}`;
          } else {
            title = `${eve.jobtype_data && eve.jobtype_data.length > 0 ? eve.jobtype_data[0].name : ""} - ${eve.branch_data && eve.branch_data.length > 0 ? eve.branch_data[0].branchname : ""}`;
          }
          return {
            title: title,
            shifttitle: eve.title,
            start: start_d ? start_d : eve.start_date,
            end: end_d ? end_d : eve.start_date,
            locations: eve.locations,
            client: eve.client,
            branch: eve.branch_data ? eve.branch_data[0].branchname : "",
            starttime: eve.starttime,
            endtime: eve.endtime,
            breaktime: eve.breaktime,
            shift_type: eve.shift_type,
            job_type: eve.job_type,
            id: eve._id,
            start_date: eve.start_date,
            end_date: eve.end_date,
            shift_option: eve.shift_option,
            option_data: eve.option_data,
            status: eve.status,
            notes: eve.notes,
            latefee: eve.latefee
          };
        });
        request({
          url: "/agency/daysoff/forshiftslist",
          method: "GET"
        }).then(res => {
          if (res.status === 1) {
            const daysofflist =
              res.response &&
              res.response.result.length > 0 &&
              res.response.result.map(list => ({
                allDay: true,
                title: list.name,
                start: list.dates,
                end: list.dates,
                desc: list.reason,
                status: 12
              }));
            this.setState({
              eventsdata: eventsdata.concat(daysofflist)
            });
          }
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  eventmodel = event => {
    if (event.status === 12) {
      this.setState({ HolidayModal: !this.state.HolidayModal, HolidayName: event.title, HolidayDate: event.dates, HolidayDesc: event.desc });
    } else {
      this.setState({
        eventlarge: !this.state.eventlarge
      });
      this.eventset(event);
    }
  };
  HolidayModalclose = () => {
    this.setState({ HolidayModal: false, HolidayName: "", HolidayDate: "", HolidayDesc: "" });
  };
  eventset = event => {
    let daycounts = 0;
    this.setState({
      daycount: daycounts,
      date_list: [],
      Weekly_select_date_list: [],
      montofri: 0,
      Weekly_select_date_count: 0,
      monthly_date_count: 0,
      count: 0
    });
    if (event) {
      let num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
      let evestarthr = event.starttime / 3600;
      let splithr = evestarthr.toString().split(".");
      let startsec = splithr[1];
      if (startsec === undefined) {
        var startmins = "00";
      } else {
        startmins = (+"0" + "." + startsec) * 60;
      }

      if (num.indexOf(splithr[0]) === -1) {
        var starttimehr = splithr[0] + ":" + startmins;
      } else {
        starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
      }

      let eveendhr = event.endtime / 3600;
      let splitendhr = eveendhr.toString().split(".");
      let endsec = splitendhr[1];
      if (endsec === undefined) {
        var endmins = "00";
      } else {
        endmins = (+"0" + "." + endsec) * 60;
      }

      if (num.indexOf(splitendhr[0]) === -1) {
        var endtimehr = splitendhr[0] + ":" + endmins;
      } else {
        endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
      }
      if (event.shift_option === "weekly") {
        this.setState({
          sun: event.option_data[2].weekly[0],
          mon: event.option_data[2].weekly[1],
          tue: event.option_data[2].weekly[2],
          wed: event.option_data[2].weekly[3],
          thu: event.option_data[2].weekly[4],
          fri: event.option_data[2].weekly[5],
          sat: event.option_data[2].weekly[6]
        });
      } else if (event.shift_option === "rotational") {
        this.setState({
          xday: event.option_data[3].rotational.x,
          yday: event.option_data[3].rotational.y
        });
      } else if (event.shift_option === "monthly") {
        this.setState({
          monthlydate: event.option_data[4].monthly
        });
      }
      this.setState({
        location: event.locations,
        client: event.client,
        branch: event.branch,
        starttime: starttimehr,
        endtime: endtimehr,
        breaktime: event.breaktime / 60 + " Minutes",
        shift_type: event.shift_type,
        job_type: event.job_type,
        shifttitle: event.shifttitle,
        id: event.id,
        start_date: moment(event.start_date).format("DD-MM-YYYY"),
        end_date: moment(event.end_date).format("DD-MM-YYYY"),
        start_date_time: event.start_date,
        end_date_time: event.end_date,
        shift_option: event.shift_option,
        status: event.status,
        notes: event.notes,
        latefee: event.latefee
      });
    } else {
      this.setState({
        eventlarge: !this.state.eventlarge,
        id: "",
        shifttitle: "",
        location: "",
        branch: "",
        client: "",
        starttime: "",
        endtime: "",
        breaktime: "",
        start_date: "",
        end_date: "",
        shift_option: "",
        job_type: "",
        shift_type: "",
        shift_template: "",
        status: "",
        notes: "",
        latefee: ""
      });
    }

    if (event.shift_option) {
      let days = Math.floor((moment(event.end_date) - moment(event.start_date)) / 86400000) + 1;
      if (event.shift_option === "Daily") {
        this.setState({
          daycount: Math.floor((moment(event.end_date) - moment(event.start_date)) / 86400000) + 1
        });
      } else if (event.shift_option === "Every Weekday (Mon - Fri)") {
        let totaldays = Math.floor((moment(event.end_date) - moment(event.start_date)) / 86400000) + 1;
        for (let i = 0; i < totaldays; i++) {
          const startdate = moment(event.start_date)._d.getDay();
          let nums = startdate + i;
          if (nums < 7) {
            this.state.date_list.push(nums);
          } else {
            const newCount = nums - 7;
            if (newCount < 7) {
              this.state.date_list.push(newCount);
            } else {
              const div = Math.trunc(newCount / 7);
              const mul = div * 7;
              const sub = newCount - mul;
              this.state.date_list.push(sub);
            }
          }
        }
        if (this.state.date_list) {
          let n = 0;
          this.state.date_list.map((data, key) => {
            if (data !== 0 && data !== 6) {
              let count = n++ + 1;
              this.setState({
                daycount: count
              });
            }
            return true;
          });
        }
      } else if (event.shift_option === "Weekly (Selected Days)") {
        event.option_data.map((option_data_lists, key1) => {
          if (option_data_lists.sun === true) {
            const sundaynum = 0;
            this.state.Weekly_select_date_list.push(sundaynum);
          }
          if (option_data_lists.mon === true) {
            const mondaynum = 1;
            this.state.Weekly_select_date_list.push(mondaynum);
          }
          if (option_data_lists.tue === true) {
            const tuedaynum = 2;
            this.state.Weekly_select_date_list.push(tuedaynum);
          }
          if (option_data_lists.wed === true) {
            const weddaynum = 3;
            this.state.Weekly_select_date_list.push(weddaynum);
          }
          if (option_data_lists.thu === true) {
            const thudaynum = 4;
            this.state.Weekly_select_date_list.push(thudaynum);
          }
          if (option_data_lists.fri === true) {
            const fridaynum = 5;
            this.state.Weekly_select_date_list.push(fridaynum);
          }
          if (option_data_lists.sat === true) {
            const satdaynum = 6;
            this.state.Weekly_select_date_list.push(satdaynum);
          }
          return true;
        });
        let totaldays_weekly = Math.floor((moment(event.end_date) - moment(event.start_date)) / 86400000) + 1;
        for (let j = 0; j < totaldays_weekly; j++) {
          const startdate = moment(event.start_date)._d.getDay();
          let weeklynum = startdate + j;
          if (weeklynum < 7) {
            this.state.date_list.push(weeklynum);
          } else {
            const newCount = weeklynum - 7;
            if (newCount < 7) {
              this.state.date_list.push(newCount);
            } else {
              const div = Math.trunc(newCount / 7);
              const mul = div * 7;
              const sub = newCount - mul;
              this.state.date_list.push(sub);
            }
          }
        }
        if (this.state.date_list && this.state.Weekly_select_date_list) {
          let m = 0;
          const Weekly_select_date_list = [0, 1, 2];
          this.state.date_list.map((date_lists, key) => {
            this.state.Weekly_select_date_list.map((select_date_lists, key1) => {
              if (date_lists === select_date_lists) {
                let count = m++ + 1;
                this.setState({
                  daycount: count
                });
              }
              return true;
            });
            return true;
          });
        }
      } else if (event.shift_option === "Monthly (Day: 20)") {
        let arr = [];
        let dt = new Date(moment(event.start_date));
        while (dt <= moment(event.end_date)) {
          arr.push(new Date(dt));
          dt.setDate(dt.getDate() + 1);
        }
        let x = 0;
        arr.map((dates, key1) => {
          if (dates.getDate() === 20) {
            let count = x++ + 1;
            this.setState({
              daycount: count
            });
          }
          return true;
        });
      } else {
        // const third = next_day(trunc(sysdate,'mm')-1,'Saturday') + 14
      }
    }
  };
  eventcancel = () => {
    this.setState({
      eventlarge: !this.state.eventlarge,
      id: "",
      shifttitle: "",
      location: "",
      branch: "",
      client: "",
      starttime: "",
      endtime: "",
      breaktime: "",
      start_date: "",
      end_date: "",
      shift_type: "",
      job_type: "",
      shift_option: ""
    });
  };
  eventStyleGetter(event, start, end, isSelected) {
    if (event.status === 2) {
      const styless = {
        backgroundColor: "#ffc107",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 3 || event.status === 4) {
      const styless = {
        backgroundColor: "#17a2b8",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 5) {
      const styless = {
        backgroundColor: "#4dbd74",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 6 || event.status === 7 || event.status === 8 || event.status === 9) {
      const styless = {
        backgroundColor: "#f86c6b",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 10) {
      const styless = {
        backgroundColor: "#818a91",
        color: "white"
      };
      return {
        style: styless
      };
    } else if (event.status === 12) {
      const styless = {
        backgroundColor: "#e66929",
        color: "white"
      };
      return {
        style: styless
      };
    } else {
      const styless = {};
      return {
        style: styless
      };
    }
  }
  render() {
    const formats = {
      timeGutterFormat: "HH:mm",
      eventTimeRangeFormat: () => ""
    };
    return (
      <div className="animated">
        <Card>
          <CardHeader>
            <i
              onClick={() => {
                this.props.history.push("/agency/editshift");
              }}
              className="icon-calendar cursor-pointer"
            />
            <strong
              onClick={() => {
                this.props.history.push("/agency/editshift");
              }}
              className="cursor-pointer"
            >
              Shifts View
            </strong>
            <div className="card-actions">
              <div className="color-info">
                <UncontrolledDropdown>
                  <DropdownToggle className="btn-sm btn-info">
                    <i className="fa fa-info-circle" />
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem header>Color Information</DropdownItem>
                    <DropdownItem className="shift_added">New</DropdownItem>
                    <DropdownItem className="shift_requested">Requested</DropdownItem>
                    <DropdownItem className="shift_assigned">Accepted / Assigned</DropdownItem>
                    <DropdownItem className="shift_ongoing">Ongoing</DropdownItem>
                    <DropdownItem className="shift_completed">Completed / Paid</DropdownItem>
                    <DropdownItem className="shift_expired">Expired</DropdownItem>
                    <DropdownItem className="shift_holiday">Holiday</DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
            </div>
          </CardHeader>
          <CardBody className="p-2" style={{ height: "40em" }}>
            <BigCalendar
              popup
              events={this.state.eventsdata}
              onSelectEvent={event => this.eventmodel(event)}
              views={["month", "week", "day"]}
              step={30}
              messages={{ previous: this.state.previous, today: "Today", next: this.state.next, month: "Month", week: "Week", day: "Day" }}
              defaultDate={new Date(currYear, currMonth, 1)}
              defaultView="month"
              toolbar={true}
              eventPropGetter={this.eventStyleGetter}
              formats={formats}
            />
          </CardBody>
        </Card>
        <Modal isOpen={this.state.eventlarge} toggle={this.eventmodel} className={"modal-lg shift_details"}>
          <ModalHeader toggle={this.eventcancel}>Shift Details</ModalHeader>
          <ModalBody>
            <Row>
              <div className="dash-points">
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-bars" /> Shift:
                    </b>
                  </span>
                  <span className="po-right">{this.state.shifttitle}</span>
                </Col>
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-user" aria-hidden="true"></i> Client:
                    </b>
                  </span>
                  <span className="po-right"> {this.state.client} </span>
                </Col>
              </div>
            </Row>{" "}
            <Row>
              <div className="dash-points">
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-map-marker" /> Area:
                    </b>
                  </span>
                  <span className="po-right"> {this.state.location}</span>
                </Col>
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-map-marker" /> Work Location
                      {/* Branch*/}:
                    </b>
                  </span>
                  <span className="po-right">{this.state.branch}</span>
                </Col>
              </div>
            </Row>{" "}
            <Row>
              <div className="dash-points">
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-arrow-circle-right" />
                      Job Role:
                    </b>
                  </span>
                  <span className="po-right"> {this.state.job_type}</span>
                </Col>
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-calendar"> </i>
                      Date:
                    </b>{" "}
                  </span>
                  <span className="po-right"> {this.state.start_date}</span>
                </Col>
              </div>
            </Row>{" "}
            <Row>
              <div className="dash-points">
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-clock-o" /> Time:
                    </b>{" "}
                  </span>
                  <span className="po-right">
                    {" "}
                    {this.state.starttime} - {this.state.endtime}
                  </span>
                </Col>
                <Col xs="12" md="6">
                  <span className="po-left">
                    <b>
                      <i className="fa fa-clock-o" />
                      Break:
                    </b>{" "}
                  </span>
                  <span className="po-right"> {this.state.breaktime}</span>
                </Col>
              </div>
            </Row>{" "}
            <Row>
			  <div className="dash-points">
              <Col xs="12" md="6">
                 <span className="po-left"><b><i className="fa fa-money" aria-hidden="true"></i> Late Booking Fee:</b> </span>
				 <span className="po-right">{this.state.latefee ? "Yes" : "No"}</span>
              </Col>
              <Col xs="12" md="6">
                <span className="po-left"> <b><i className="fa fa-comment-o" aria-hidden="true"></i> Notes :</b> </span>
                <span className="po-right"> {this.state.notes}{" "} </span>
              </Col>
			 </div>
            </Row>
            <Row>
              <Col xs="12" md="6">
			  <div className="dash-points">
                <span className="po-left"> <b> <i className="fa fa-certificate" aria-hidden="true"></i>
Shift Status:</b> </span>
                <span className="po-right">{this.state.status === 1 ? <Badge color="success">Shift Added</Badge> : null}
                {this.state.status === 2 ? <Badge color="success">Employee requested</Badge> : null}
                {this.state.status === 3 ? <Badge color="success">Shift Accepted</Badge> : null}
                {this.state.status === 4 ? <Badge color="success">Shift Assigned</Badge> : null}
                {this.state.status === 5 ? <Badge color="success">Shift Ongoing</Badge> : null}
                {this.state.status === 6 ? <Badge color="success">Shift Completed</Badge> : null}
                {this.state.status === 7 ? <Badge color="danger">Timesheet Approved</Badge> : null}
                {this.state.status === 8 ? <Badge color="danger">Invoice Approved</Badge> : null}
                {this.state.status === 9 ? <Badge color="danger">Payment Completed</Badge> : null}
                {this.state.status === 10 ? <Badge color="secondary">Shift Expired</Badge> : null}</span>
			  </div>
              </Col>
              {this.state.shift_type === "multiple" ? (
                <Col xs="12" md="8">
                  <span>
                    <b>Multiple Day:</b>
                    {this.state.shift_option === "daily" ? <span>Daily </span> : null}
                    {this.state.shift_option === "weekday" ? <span>Every Weekday (Mon - Fri)</span> : null}
                    {this.state.shift_option === "weekly" ? (
                      <span>
                        Weekly {this.state.sun === true ? <strong>Sun, </strong> : null}
                        {this.state.mon === true ? <strong>Mon, </strong> : null}
                        {this.state.tue === true ? <strong>Tue, </strong> : null}
                        {this.state.wed === true ? <strong>Wed, </strong> : null}
                        {this.state.thu === true ? <strong>Thu, </strong> : null}
                        {this.state.fri === true ? <strong>Fri, </strong> : null}
                        {this.state.sat === true ? <strong>Sat </strong> : null}
                        Days
                      </span>
                    ) : null}
                    {this.state.shift_option === "rotational" ? (
                      <span>
                        X Days : {this.state.xday} / Y Days : {this.state.yday}
                      </span>
                    ) : null}
                    {this.state.shift_option === "monthly" ? <span>Monthly {this.state.monthlydate} Days</span> : null}{" "}
                  </span>
                </Col>
              ) : null}
            </Row>
          </ModalBody>
          <ModalFooter className="modal-gal">
            <Button color="secondary rounded-0" onClick={this.eventcancel}>
              <i className="fa fa-close" />
            </Button>
            {this.state.status !== 10 ? (
              <Button
                color="success"
                className="ml-2"
                onClick={() => {
                  this.props.history.push({
                    pathname: "/agency/requestemployees",
                    state: { rowid: { _id: this.state.id, status: this.state.status } }
                  });
                }}
                title="View Assign Employee"
              >
                <i className="fa fa-eye" />
              </Button>
            ) : null}
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.HolidayModal} toggle={this.HolidayModalOpen} className={this.props.className}>
          <ModalHeader toggle={this.HolidayModalOpen}>Holiday Details</ModalHeader>
          <ModalBody>
            <Row>
			 <div className="dash-points">
              <Col xs="12" md="12">
                <span className="po-left"> <strong><i class="fa fa-user-o" aria-hidden="true"></i> Name:</strong>  </span>
				<span className="po-right"> {this.state.HolidayName} </span>
              </Col>
			  </div>
            </Row>
           
            <Row>
			 <div className="dash-points">
              <Col xs="12" md="12">
                 <span className="po-left">  <strong> <i class="fa fa-clock-o"></i> Date:</strong>  </span>
				 <span className="po-right"> {moment(this.state.HolidayDate).format("DD-MM-YYYY")} </span>
              </Col>
			  </div>
            </Row>
          
            <Row>
			<div className="dash-points">
              <Col xs="12" md="12">
               <span className="po-left">  <strong> <i class="fa fa-commenting-o" aria-hidden="true"></i>
 Description:</strong>  </span>
			   <span className="po-right"> {this.state.HolidayDesc} </span>
              </Col>
			  </div>
            </Row>
			
          </ModalBody>
          <ModalFooter className="modal-gal">
            <Button color="secondary" onClick={this.HolidayModalclose}>
              <i class="fa fa-close"></i>
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default withRouter(Calenterview);
