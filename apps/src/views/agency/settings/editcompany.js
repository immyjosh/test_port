/*eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, CustomInput } from "reactstrap";
import request, { NodeURL } from "../../../api/api";
import { toast } from "react-toastify";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
// import ReactCrop from "react-image-crop";
// import "react-image-crop/dist/ReactCrop.css";

class Editcompany extends Component {
  state = {
    username: "",
    password: "",
    name: "",
    email: "",
    confirm_password: "",
    conform: "",
    dailcountry: "",
    number: "",
    code: "",
    number1: "",
    code1: "",
    company_email: "",
    company_logo: "",
    logo_file: "",
    company_logo_name: "",
    company_name: "",
    logo_return: "",
    postal_address: "",
    company_description: "",
    registration_number: "",
    organisation_type: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    phoneerror: false,
    avatar: "",
    status: "",
    avatar_return: "",
    _id: "",
    avatarName: "",
    bank_name: "",
    bank_ac_name: "",
    bank_sort_code: "",
    bank_ac: "",
    bank_becs: "",
    bank_sepa: "",
    dailcountry1: "",
    DeactivatepopoverOpen: false,
    src: null,
    crop: {
      x: 120,
      y: 120,
      aspect: 16 / 9,
      maxWidth: 180,
      maxHeight: 180
    }
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/agency/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result) {
          let data = res.response.result[0];
          this.setState({
            _id: data._id,
            address: data.formatted_address,
            line1: data.address.line1,
            line2: data.address.line2,
            city: data.address.city,
            state: data.address.state,
            country: data.address.country,
            zipcode: data.address.zipcode,
            formatted_address: data.address.formatted_address,
            company_name: data.company_name,
            company_description: data.company_description,
            dailcountry1: data.company_phone ? data.company_phone.dailcountry : "gb",
            number1: data.company_phone.number,
            code1: data.company_phone.code,
            postal_address: data.postal_address,
            organisation_type: data.organisation_type,
            company_email: data.company_email,
            registration_number: data.registration_number,
            logo_return: data.company_logo,
            bank_name: data.bank_details ? data.bank_details.name : "",
            bank_ac: data.bank_details ? data.bank_details.ac_no : "",
            bank_ac_name: data.bank_details ? data.bank_details.ac_name : "",
            bank_sort_code: data.bank_details ? data.bank_details.sort_code : "",
            fax: data.fax,
            vat_number: data.vat_number
            // bank_becs: res.response.result[0].bank_details ? res.response.result[0].bank_details.becs : "",
            // bank_sepa: res.response.result[0].bank_details ? res.response.result[0].bank_details.sepa : "",
            // company_logo_name: data.company_logo.substr(24, 30)
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  handleChange = formatted_address => {
    this.setState({ formatted_address });
    this.setState({ formatted_address: formatted_address });
  };
  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name+" "+results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarName: evt.target.files[0].name
    });
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2
    });
  };
  fileChangedHandler1 = evt => {
    this.setState({
      company_logo: evt.target.files[0],
      logo_file: URL.createObjectURL(evt.target.files[0]),
      company_logo_name: evt.target.files[0].name
    });
  };
  onEditAdmin = (e, values) => {
    if (this.state.number1 === "") {
      this.setState({
        phoneerror1: true
      });
    } else {
      const data = new FormData();
      data.append("_id", this.state._id);
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("address[line1]", this.state.line1);
      data.append("address[line2]", this.state.line2);
      data.append("address[city]", this.state.city);
      data.append("address[state]", this.state.state);
      data.append("address[country]", this.state.country);
      data.append("address[zipcode]", this.state.zipcode);
      data.append("address[formatted_address]", this.state.formatted_address);
      data.append("address[lat]", this.state.lat);
      data.append("address[lon]", this.state.lon);
      data.append("company_name", this.state.company_name);
      data.append("company_phone[code]", this.state.code1);
      data.append("company_phone[number]", this.state.number1);
      data.append("company_phone[dailcountry]", this.state.dailcountry1);
      data.append("company_email", this.state.company_email);
      data.append("company_logo", this.state.company_logo);
      data.append("postal_address", this.state.postal_address);
      data.append("company_description", this.state.company_description);
      data.append("registration_number", this.state.registration_number);
      data.append("organisation_type", this.state.organisation_type);
      data.append("fax", this.state.fax);
      data.append("vat_number", this.state.vat_number);
      data.append("bank_name", this.state.bank_name);
      data.append("bank_ac", this.state.bank_ac);
      data.append("bank_ac_name", this.state.bank_ac_name);
      data.append("bank_sort_code", this.state.bank_sort_code);
      data.append("status", this.state.status);
      request({
        url: "/agency/company/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            toast.success("Updated");
            this.setState({ src: null, croppedImageUrl: null });
            this.componentDidMount();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  editnodify() {
    return this.props.history.replace("/agency/dashboard"), this.flash.updatenodify();
  }

  /* onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => this.setState({ src: reader.result }));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
  };

  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, "newFile.png");
    this.setState({ croppedImageUrl: croppedImageUrl.url, company_logo: croppedImageUrl.file });
  };

  onCropChange = crop => {
    this.setState({ crop });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);
    return new Promise((resolve, reject) => {
      canvas.toBlob(file => {
        if (file) {
          file.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(file);
          resolve({ url: this.fileUrl, file: file });
        }
      }, "image/png");
    });
  }*/
  render() {
    // const { croppedImageUrl } = this.state;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-note" />
                  Company Details
                </CardHeader>
                <CardBody>
                  
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
				    <Col xs="12" md="12">
				     <p className="h5">Company Information</p>
				    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Company/Organisation Name</Label>
                        <AvInput name="company_name" type="text" placeholder="Enter Name" onChange={this.onChange} minlength="3" value={this.state.company_name} autoComplete="company_name" required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Email</Label>
                        <AvInput name="company_email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.company_email} autoComplete="company_email" required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4" className={"for-phone-right"}>
                      <Label>Tel</Label>
                      {this.state && this.state.dailcountry1 ? (
                        <IntlTelInput style={{ width: "100%" }} defaultCountry={this.state.dailcountry1} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler1} value={this.state.number1} />
                      ) : null}
                      {this.state.phoneerror1 ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Fax</Label>
                        <AvInput name="fax" type="number" placeholder="Enter Fax" onChange={this.onChange} minLength="3" value={this.state.fax} autoComplete="fax" required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					
                  
                    {/* <Col xs="6" md="4">
                      <Label for="exampleCustomFileBrowser1">Logo</Label>
                      <CustomInput
                        type="file"
                        id="exampleCustomFileBrowser1"
                        name="avatar"
                        onChange={this.fileChangedHandler1}
                        label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                        encType="multipart/form-data"
                      />
                    </Col> */}
                    <Col xs="6" md="4">
                      <Label for="exampleCustomFileBrowser1">Logo</Label>
                      {/*<CustomInput type="file" id="exampleCustomFileBrowser1" name="avatar" onChange={this.onSelectFile} label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"} encType="multipart/form-data" />*/}
                      <CustomInput
                        type="file"
                        accept=".png, .jpg, .jpeg"
                        id="exampleCustomFileBrowser189"
                        name="avatar"
                        onChange={this.fileChangedHandler1}
                        label={this.state.company_logo_name ? this.state.company_logo_name : "Upload  Image"}
                        encType="multipart/form-data"
                      />
                    </Col>
                    {/* <Col xs="6" md="4">
                      {this.state.src && <ReactCrop src={this.state.src} crop={this.state.crop} onImageLoaded={this.onImageLoaded} onComplete={this.onCropComplete} onChange={this.onCropChange} />}
                    </Col>
                    <Col xs="6" md="4">
                      {croppedImageUrl && <img width="100%" height="180px" alt="Crop" src={croppedImageUrl} />}{" "}
                    </Col>*/}
                    {this.state.logo_file ? (
                      <Col xs="6" md="3">
                        <Label>Preview</Label> <img className="prof-image img-fluid" width="180px" height="140px" src={this.state.logo_file} alt="Preview" />{" "}
                      </Col>
                    ) : null}
                    {this.state.logo_return ? (
                      <Col xs="6" md="4">
                        <img className="prof-image img-fluid" width="180px" height="140px" src={NodeURL + "/" + this.state.logo_return} alt="Preview" />
                      </Col>
                    ) : null}

                     <Col xs="12" md="12" className="mb-2">
					<Label>Business Address</Label>
                      <PlacesAutocomplete value={this.state.formatted_address} onChange={this.handleChange} onSelect={this.handleSelect} onFocus={this.geolocate}>
                        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                          <Fragment>
                            <input
                              {...getInputProps({
                                placeholder: "Search Places",
                                className: "form-control"
                              })}
                              required
                            />
                            <div className="autocomplete-dropdown-container absolute">
                              {suggestions.map(suggestion => {
                                const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                  ? {
                                      backgroundColor: "#fafafa",
                                      cursor: "pointer"
                                    }
                                  : {
                                      backgroundColor: "#ffffff",
                                      cursor: "pointer"
                                    };
                                return (
                                  <div
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                      style
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                  </div>
                                );
                              })}
                            </div>
                          </Fragment>
                        )}
                      </PlacesAutocomplete>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Line1</Label>
                        <AvInput type="text" name="line1" placeholder="Enter Line1" onChange={this.onChange} value={this.state.line1} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City/Town</Label>
                        <AvInput type="text" name="line2" placeholder="Enter Line2" onChange={this.onChange} value={this.state.line2} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    {/* <Col xs="12" md="4">
                      <AvGroup>
                        <Label>City</Label>
                        <AvInput
                          type="text"
                          name="city"
                          placeholder="Enter City"
                          onChange={this.onChange}
                          value={this.state.city}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col> */}
					
                  
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>State/Region</Label>
                        <AvInput type="text" name="state" placeholder="Enter State" onChange={this.onChange} value={this.state.state} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Country</Label>
                        <AvInput type="text" name="country" placeholder="Enter Country" onChange={this.onChange} value={this.state.country} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Post Code</Label>
                        <AvInput type="text" name="zipcode" placeholder="Enter Post Code" onChange={this.onChange} value={this.state.zipcode} required autoComplete="name" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					</div>
					</Col>
                  </Row>  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
                     <Col xs="12" md="4">
                      <AvGroup>
                        <Label>
                          VAT No : <small>(If Available)</small>
                        </Label>
                        <AvInput type="number" name="vat_number" placeholder="Enter Number" onChange={this.onChange} min="0" value={this.state.vat_number} autoComplete="vat_number" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvField type="select" name="organisation_type" label="Organisation Type" onChange={this.onChange} value={this.state.organisation_type} required>
                        <option>Select</option>
                        <option value="limited_company">Limited Company</option>
                        <option value="not_for_profit">Not For Profit</option>
                        <option value="partnership">Partnership</option>
                        <option value="soletrader">Soletrader</option>
                      </AvField>
                    </Col>
                     {this.state.organisation_type === "limited_company"?(
                         <Col xs="12" md="4">
                           <AvGroup>
                             <Label>Company Number</Label>
                             <AvInput type="number" name="registration_number" placeholder="Enter Number" onChange={this.onChange} min="0" value={this.state.registration_number} required autoComplete="registration_number" />
                             <AvFeedback>This is required!</AvFeedback>
                           </AvGroup>
                         </Col>
                     ):null}
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Organisation Description</Label>
                        <AvInput type="textarea" name="company_description" placeholder="Enter Description" onChange={this.onChange} value={this.state.company_description} required autoComplete="company_description" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Postal Address</Label>
                        <AvInput type="textarea" name="postal_address" placeholder="Enter Address" onChange={this.onChange} value={this.state.postal_address} required autoComplete="postal_address" />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>

                    <Col xs="12" md="4">
                      <AvField
                        name="bank_name"
                        onChange={this.onChange}
                        value={this.state.bank_name}
                        placeholder="Enter Name"
                        label="Bank Name"
                        type="text"
                        errorMessage="This is required (Minimun 4 Letters)"
                        validate={{ required: { value: true }, minLength: { value: 4 } }}
                      />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField
                        name="bank_ac_name"
                        onChange={this.onChange}
                        value={this.state.bank_ac_name}
                        placeholder="Enter AC.Name"
                        label="Account Name"
                        type="text"
                        errorMessage="This is required (Minimun 2 Letters)"
                        validate={{ required: { value: true }, minLength: { value: 2 } }}
                      />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField
                        name="bank_sort_code"
                        onChange={this.onChange}
                        value={this.state.bank_sort_code}
                        placeholder="Enter Sort Code"
                        label="Sort Code"
                        type="number"
                        errorMessage="This is required (Minimun 6 Numbers)"
                        validate={{ required: { value: true }, minLength: { value: 6 } }}
                      />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField
                        name="bank_ac"
                        onChange={this.onChange}
                        value={this.state.bank_ac}
                        placeholder="Enter AC.No"
                        label="Account Number"
                        type="number"
                        errorMessage="This is required (Minimun 8 Numbers)"
                        validate={{ required: { value: true }, minLength: { value: 8 } }}
                      />
                    </Col>
                    {/* <Col xs="12" md="4">
                      <AvField
                        name="bank_becs"
                        onChange={this.onChange}
                        value={this.state.bank_becs}
                        placeholder="Enter BECS"
                        label="BECS"
                        type="text"
                        errorMessage="This is required (Minimun 4 Letters)"
                        validate={{ required: { value: true }, minLength: { value: 4 } }}
                      />
                    </Col>
                    <Col xs="12" md="4">
                      <AvField
                        name="bank_sepa"
                        onChange={this.onChange}
                        value={this.state.bank_sepa}
                        placeholder="Enter SEPA"
                        label="SEPA"
                        type="text"
                        errorMessage="This is required (Minimun 4 Letters)"
                        validate={{ required: { value: true }, minLength: { value: 4 } }}
                      />
                    </Col>*/}
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i
                      className="fa fa-dot-circle-o"
                      onClick={() => {
                        this.phonefield(this.state.number);
                      }}
                    />{" "}
                    Update
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editcompany;
