import fileDownload from "js-file-download";
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { DateRangePicker } from "react-dates";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import ReactDOM from "react-dom";
import Rating from "react-rating";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import { SingleDatePicker } from "react-dates";
import moment from "moment";
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledTooltip,
  Label
} from "reactstrap";
import request from "../../../api/api";
import Pagination from "react-js-pagination";
import Widget02 from "../../Template/Widgets/Widget02";
import { breaklist, timelist } from "../../common/utils";
import Select from "react-select";
import "react-select/dist/react-select.css";

var tStyle = {
  cursor: "pointer"
};
var tStyles = {
  width: "90px"
};

class timesheet extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    timesheet_endtimeview: "",
    timesheet_starttimeview: "",
    status_timesheet: "",
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    viewtimesheetid: "",
    timesheet: []
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ timelist, breaklist, isLoader: true });
    this.populateData();
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  populateData() {
    request({
      url: "/agency/timesheet/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            if (earn._id === 2) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 1) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            }
            return true;
          });
          let getshiftdata = [];
          this.setState({
            timesheetlists: res.response.result
          });
          res.response.result.map((item, i) => {
            var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            var evestarthr = item.starttime / 3600;
            var splithr = evestarthr.toString().split(".");
            var startsec = splithr[1];
            if (startsec === undefined) {
              var startmins = "00";
            } else {
              startmins = (+"0" + "." + startsec) * 60;
            }

            if (num.indexOf(splithr[0]) === -1) {
              var starttimehr = splithr[0] + ":" + startmins;
            } else {
              starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
            }
            var eveendhr = item.endtime / 3600;
            var splitendhr = eveendhr.toString().split(".");
            var endsec = splitendhr[1];
            if (endsec === undefined) {
              var endmins = "00";
            } else {
              endmins = (+"0" + "." + endsec) * 60;
            }

            if (num.indexOf(splitendhr[0]) === -1) {
              var endtimehr = splitendhr[0] + ":" + endmins;
            } else {
              endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
            }
            if (item.timesheet_status) {
              var timesheet_status = item.timesheet_status;
            } else {
              timesheet_status = "";
            }
            getshiftdata.push({
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              agency_rate: item.agency_rate,
              title: item.title,
              shiftId: item.shiftId,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              comment: item.agency_comment,
              rating: item.agency_rating
            });

            this.setState({
              adminlist: getshiftdata,
              pages: res.response.fullcount,
              currPage: res.response.length
            });
            return true;
          });
          this.viewpage({ timesheetid: this.state.viewtimesheetid, update: "update" });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["title", "client", "location", "branch", "job_type", "employee", "starttime", "start_date", "status"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/timesheet/timesheetexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d || "";
        state.tableOptions.to_date = this.state.end_date._d || "";
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  TimesheettoastId = "Timesheet";
  nodify() {
    if (!toast.isActive(this.TimesheettoastId)) {
      this.TimesheettoastId = toast.warn("Updated");
    }
  }
  adminsavenodifytoastId = "Timesheet";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifytoastId)) {
      this.adminsavenodifytoastId = toast.success("Successfully Saved!");
    }
  }
  viewpageclose = e => {
    this.setState({
      success: false,
      edit: false
    });
  };
  viewpage = e => {
    this.state.timesheetlists.filter((value, key) => {
      if (value._id === e.timesheetid) {
        this.setState({
          timesheetviewdata: value,
          viewtimesheetid: e.timesheetid
        });
        if (e.timesheet_status === 2) {
          this.setState({
            status_timesheet: e.timesheet_status
          });
        } else {
          this.setState({
            status_timesheet: ""
          });
        }
        var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
        var evestarthr = value.starttime / 3600;
        var splithr = evestarthr.toString().split(".");
        var startsec = splithr[1];
        if (startsec === undefined) {
          var startmins = "00";
        } else {
          startmins = (+"0" + "." + startsec) * 60;
        }

        if (num.indexOf(splithr[0]) === -1) {
          var starttimehr = splithr[0] + ":" + startmins;
        } else {
          starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
        }
        var eveendhr = value.endtime / 3600;
        var splitendhr = eveendhr.toString().split(".");
        var endsec = splitendhr[1];
        if (endsec === undefined) {
          var endmins = "00";
        } else {
          endmins = (+"0" + "." + endsec) * 60;
        }

        if (num.indexOf(splitendhr[0]) === -1) {
          var endtimehr = splitendhr[0] + ":" + endmins;
        } else {
          endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
        }
        this.setState({
          breaktimesheet: value.breaktime,
          employee_rate_sheet: value.employee_rate,
          time_start_date: moment(value.start_date),
          time_end_date: moment(value.end_date),
          agency_rate_sheet: value.agency_rate,
          timesheetcommand: value.comment || e.comment || "",
          rating: value.rating || e.rating || "",
          timesheet_starttime: value.starttime,
          timesheet_endtime: value.endtime,
          timesheet: value.timesheet,
          timesheet_starttimeview: starttimehr,
          timesheet_endtimeview: endtimehr
        });
      }
      return true;
    });
    if (e.update !== "update") {
      this.setState({
        success: !this.state.success,
        edit: false
      });
    } else {
      this.setState({
        edit: false
      });
    }
  };
  EditnestedModal = () => {
    this.setState({
      edit: true
    });
  };
  EditnestedModalview = () => {
    this.setState({
      edit: false
    });
  };
  approve() {
    request({
      url: "/agency/timesheet/approval",
      method: "POST",
      data: {
        shiftId: this.state.timesheetviewdata._id,
        comment: this.state.timesheetcommand,
        rating: this.state.rating
      }
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        toast.success("Timesheet Approved!");
        this.setState({
          success: !this.state.success
        });
        setTimeout(() => {
          this.componentDidMount();
        }, 3500);
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  editapprove() {
    const timesheet = {
      shiftId: this.state.timesheetviewdata._id,
      employee_rate: this.state.employee_rate_sheet,
      agency_rate: this.state.agency_rate_sheet,
      start_date: this.state.time_start_date,
      end_date: this.state.time_end_date,
      starttime: this.state.timesheet_starttime,
      endtime: this.state.timesheet_endtime,
      breaktime: this.state.breaktimesheet,
      comment: this.state.timesheetcommand,
      rating: this.state.rating
    };
    request({
      url: "/agency/timesheet/save",
      method: "POST",
      data: timesheet
    }).then(res => {
      if (res.status === 1) {
        toast.success("Updated!");
        this.componentDidMount();
      } else {
        toast.error(res.response);
      }
    });
  }
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ timesheet_starttime: value.value, selecterror: false });
    } else {
      this.setState({ timesheet_starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ timesheet_endtime: value.value, selectenderror: false });
    } else {
      this.setState({ timesheet_endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktimesheet: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktimesheet: "", selectbreakerror: true });
    }
  };
  PageChange(value) {
    if (value === "employee") {
      return this.props.history.push("/agency/timesheetemployee");
    } else if (value === "shifts") {
      return this.props.history.push("/agency/timesheet");
    }
  }
  render() {
    if (this.state.timesheetviewdata.start_date) {
      var timesheetstart = moment(this.state.timesheetviewdata.start_date).format("DD-MM-YYYY");
    }
    // if (this.state.timesheetviewdata.end_date) {
    //   var timesheetend = moment(this.state.timesheetviewdata.end_date).format("DD-MM-YYYY");
    // }
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" sm="6" lg="2" />
          <Col xs="12" sm="6" lg="4">
            <Widget02
              header={this.state.earncount ? <Fragment>{this.state.earncount}</Fragment> : "0"}
              mainText="Approved"
              icon="fa fa-check"
              color="primary"
            />
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Widget02
              header={this.state.pendingcount ? <Fragment>{this.state.pendingcount}</Fragment> : "0"}
              mainText="pending"
              icon="fa fa-clock-o"
              color="warning"
            />
          </Col>
        </Row>
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Time Sheet List
            <div className="card-actions" style={tStyle} onClick={this.export}>
              <button style={tStyles}>
                <i className="fa fa-upload" /> Export
                {/*<small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row">
              <div className="col-lg-7 agency_times">
                <div className="col-lg-8">
                  <DateRangePicker
                    showClearDates={true}
                    startDate={this.state.start_date}
                    startDateId="start_date"
                    endDate={this.state.end_date}
                    endDateId="end_date"
                    onDatesChange={({ startDate, endDate }) => {
                      this.setState({
                        start_date: startDate,
                        end_date: endDate
                      });
                    }}
                    isOutsideRange={day => day.isBefore(this.state.start_date)}
                    focusedInput={this.state.focusedInput}
                    onFocusChange={focusedInput => this.setState({ focusedInput })}
                    displayFormat="DD-MM-YYYY"
                  />
                  <Button
                    className="rounded-0"
                    size="md"
                    color="primary"
                    onClick={() => {
                      this.fromTo();
                    }}
                  >
                    <i className="fa fa-search" />
                  </Button>
                </div>
                <div className="col-lg-4">
                  <Input onChange={e => this.PageChange(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-8">
                    <option value="shifts" selected>
                      Shifts
                    </option>
                    <option value="employee">Employee</option>
                  </Input>
                </div>
              </div>
              <div className="col-lg-5 pr-0">
                <InputGroup>
                  <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                    <option>All</option>
                  </Input>
                  <Input
                    type="text"
                    ref="search"
                    placeholder="Search"
                    name="search"
                    onChange={e => this.search(e.target.value)}
                    className="rounded-0 col-lg-6"
                  />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive size="sm">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("title");
                      }}
                    >
                      Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("client");
                      }}
                    >
                      Client <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("location");
                      }}
                    >
                      Location <i style={{ paddingLeft: "25px" }} className={sorticondef} id="location" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("branch");
                      }}
                    >
                      Branch <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("job_type");
                      }}
                    >
                      Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_type" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("employee");
                      }}
                    >
                      Employee <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee" />
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("employee_rate");
                      }}
                    >
                      Employee Rate <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("agency_rate");
                      }}
                    >
                      Agency Rate <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th> */}
                    <th
                      onClick={() => {
                        this.sort("starttime");
                      }}
                    >
                      Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" />
                    </th>

                    <th
                      onClick={() => {
                        this.sort("start_date");
                      }}
                    >
                      Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" />
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("end_date");
                      }}
                    >
                      End Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th> */}

                    <th>
                      Status
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th> */}
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item.shiftId}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.title}</td>
                        <td>{item.client}</td>
                        <td>{item.location}</td>
                        <td className="wid_ten">{item.branch}</td>
                        <td>{item.job_type}</td>
                        <td>{item.employee}</td>
                        {/* <td>£ {item.employee_rate}</td>
                        <td>£ {item.agency_rate}</td> */}
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        {/* <td>{moment(item.end_date).format("DD-MM-YYYY")}</td> */}

                        <td>{item.timesheet_status === 2 ? <Badge color="success">Approved</Badge> : <Badge color="danger">Pending</Badge>}</td>
                        <td>
                          <div>
                            <button type="button" title="Edit" className="btn btn-primary" id={`edit${i}`} onClick={e => this.viewpage(item)}>
                              <i className="fa fa-eye" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit${i}`}>
                              View
                            </UncontrolledTooltip>
                          </div>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <Modal isOpen={this.state.success} toggle={this.viewpage} className={"modal-lg " + this.props.className}>
              {this.state.edit ? (
                <Fragment>
                  <ModalHeader>EDIT Time Sheet</ModalHeader>
                  {/* <AvForm onValidSubmit={this.OnFormEdit}> */}
                  <ModalBody>
                    {/* <Row>
                      <Col xs="12" md="4">
                        <Label>Shift:</Label> {this.state.timesheetviewdata.title}
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Added By:</Label> {this.state.timesheetviewdata.addedBy}
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Role:</Label> {this.state.timesheetviewdata.job_type}
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col xs="12" md="4">
                        <Label>Client:</Label> {this.state.timesheetviewdata.client}
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Location:</Label> {this.state.timesheetviewdata.location}
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Branch:</Label>{" "}
                        {this.state.timesheetviewdata && this.state.timesheetviewdata.branch.map((list, key) => list.branches.branchname)}
                      </Col>
                    </Row>
                    <br /> */}
                    <Row>
                      {/* <Col xs="12" md="4">
                        <Label>Employee Name:</Label>
                        {this.state.timesheetviewdata.employee}
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Agency Rate:</Label>{" "}
                        <Input
                          type="text"
                          name="agency_rate_sheet"
                          placeholder="Enter Job Title"
                          onChange={this.onChange}
                          value={this.state.agency_rate_sheet}
                          required
                        />
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Employee Rate:</Label>
                        <Input
                          type="text"
                          name="employee_rate_sheet"
                          placeholder="Enter Job Title"
                          onChange={this.onChange}
                          value={this.state.employee_rate_sheet}
                          required
                        />
                      </Col> */}
                      <Col xs="12" md="3">
                        <Label>Date:</Label>
                        <br />
                        <SingleDatePicker
                          date={this.state.time_start_date}
                          openDirection={this.state.openDirection}
                          onDateChange={date => this.setState({ time_start_date: date })}
                          focused={this.state.focused}
                          onFocusChange={({ focused }) => this.setState({ focused })}
                          id="time_start_date"
                          className="form-control"
                          displayFormat="DD-MM-YYYY"
                        />
                      </Col>
                      <Col xs="12" md="3">
                        <Label>Start Time:</Label>
                        <Select
                          name="timesheet_starttime"
                          value={this.state.timesheet_starttime}
                          options={this.state.timelist}
                          onChange={this.selectstarttimeChange}
                        />
                      </Col>
                      <Col xs="12" md="3">
                        <Label>End Time:</Label>
                        <Select
                          name="timesheet_endtime"
                          value={this.state.timesheet_endtime}
                          options={this.state.timelist}
                          onChange={this.selectendtimeChange}
                        />
                      </Col>
                      <Col xs="12" md="3">
                        <Label>Break Time:</Label>
                        <Select name="breaktimesheet" value={this.state.breaktimesheet} options={this.state.breaklist} onChange={this.selectbreaktimeChange} />
                      </Col>
                      {/* <Col xs="12" md="3">
                        <Label>End Date:</Label>
                        <br />
                        <SingleDatePicker
                          date={this.state.time_end_date}
                          openDirection={this.state.openDirection}
                          onDateChange={date => this.setState({ time_end_date: date })}
                          focused={this.state.focusedEnd}
                          onFocusChange={({ focused }) => this.setState({ focusedEnd: focused })}
                          id="time_end_date"
                          className="form-control"
                          displayFormat="DD-MM-YYYY"
                        />
                      </Col> */}
                    </Row>

                    <br />
                    {/* <Row>
                      <Col xs="12" md="4">
                        <Label>Rating</Label>
                        <Rating
                          initialRating={this.state.rating}
                          emptySymbol="fa fa-star-o fa-2x"
                          fullSymbol="fa fa-star fa-2x"
                          fractions={2}
                          onChange={rate => this.setState({ rating: rate })}
                          value={this.state.rating}
                        />
                      </Col>
                      <Col xs="12" md="4">
                        <Label>Comments</Label>
                        <Input
                          name="timesheetcommand"
                          //onChange={(text) => this.ratingComment(text)}
                          placeholder="Comments"
                          type="textarea"
                          onChange={this.onChange}
                          value={this.state.timesheetcommand}
                        />
                      </Col>
                    </Row> */}
                  </ModalBody>
                  <ModalFooter className="justify-content-between">
                    <div>
                      <Button color="secondary" onClick={this.viewpageclose} title="Close">
                        <i className="fa fa-close" />
                      </Button>
                      <Button color="secondary" className="ml-3" onClick={this.EditnestedModalview} title="Back To View">
                        <i className="fa fa-arrow-left" />
                      </Button>
                    </div>

                    <Button color="success" id="updateapprove" onClick={e => this.editapprove(this.state.timesheetviewdata.shiftId)}>
                      <i className="fa fa-check" />
                    </Button>
                    <UncontrolledTooltip placement="top" target="updateapprove">
                      Update Time Sheet
                    </UncontrolledTooltip>
                    {/* <Button color="success" type="submit">
                        {" "}
                        <i className="fa fa-check" />
                      </Button>{" "} */}
                  </ModalFooter>
                  {/* </AvForm> */}
                </Fragment>
              ) : (
                <Fragment>
                  <ModalHeader>Time Sheet Details</ModalHeader>
                  <ModalBody>
                    <Row>
                      <Col xs="12" md="4">
                        <b>Tittle:</b> {this.state.timesheetviewdata.title}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Added By:</b> {this.state.timesheetviewdata.addedBy}
                      </Col>

                      <Col xs="12" md="4">
                        <b>Role:</b> {this.state.timesheetviewdata.job_type}
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col xs="12" md="4">
                        <b>Client:</b> {this.state.timesheetviewdata.client}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Location:</b> {this.state.timesheetviewdata.locations}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Branch:</b> {this.state.timesheetviewdata && this.state.timesheetviewdata.branch.map((list, key) => list.branches.branchname)}
                      </Col>
                    </Row>
                    {/* <br />
                    <Row>
                      <Col xs="12" md="4">
                        <b>Employee Name:</b> {this.state.timesheetviewdata.employee}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Agency Rate:</b> {this.state.timesheetviewdata.agency_rate}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Employee Rate:</b> {this.state.timesheetviewdata.employee_rate}
                      </Col>
                    </Row> */}
                    <br />
                    <Row>
                      <Col xs="12" md="4">
                        <b>Date:</b>
                        {timesheetstart}
                        {/* {timesheetend} */}
                      </Col>
                      <Col xs="12" md="4">
                        <b>Time:</b> {this.state.timesheet_starttimeview} - {this.state.timesheet_endtimeview}
                      </Col>

                      <Col xs="12" md="4">
                        <b>Break:</b> {this.state.timesheetviewdata.breaktime / 60 + " mins"}
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col xs="12" md="4">
                        <b>Employee Name:</b> {this.state.timesheetviewdata.employee}
                      </Col>
                    </Row>
                    <br />
                    {this.state.timesheet ? (
                      <Row>
                        <Col xs="12" md="4">
                          {this.state.timesheet.length > 0 && this.state.timesheet[0].start ? (
                            <Fragment>
                              <b>Worked Time : </b> {moment(this.state.timesheet[0].start).format("HH:mm")} -{" "}
                              {moment(this.state.timesheet[0].end).format("HH:mm")}
                            </Fragment>
                          ) : null}
                        </Col>
                        <Col xs="12" md="4">
                          {this.state.timesheet.length > 0 && this.state.timesheet[0].holdedmins ? (
                            <Fragment>
                              <b>Holded Duration : </b> {Math.round(this.state.timesheet[0].holdedmins / 60)} Minutes
                            </Fragment>
                          ) : null}
                        </Col>
                        <Col xs="12" md="4">
                          {this.state.timesheet.length > 0 && this.state.timesheet[0].workmins ? (
                            <Fragment>
                              <b> Total Duration : </b> {Math.round(this.state.timesheet[0].workmins / 60)} Minutes
                            </Fragment>
                          ) : null}
                        </Col>
                      </Row>
                    ) : null}
                    <br />
                    {this.state.status_timesheet === "" ? (
                      <Row>
                        <Col xs="12" md="4">
                          <b>Rating : </b>
                          <Rating
                            initialRating={this.state.rating}
                            emptySymbol="fa fa-star-o fa-2x"
                            fullSymbol="fa fa-star fa-2x"
                            fractions={2}
                            onChange={rate => this.setState({ rating: rate })}
                            value={this.state.rating}
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <b>Comments :</b>
                          <Input
                            name="timesheetcommand"
                            //onChange={(text) => this.ratingComment(text)}
                            placeholder="Comments"
                            type="textarea"
                            onChange={this.onChange}
                            value={this.state.timesheetcommand}
                          />
                        </Col>
                      </Row>
                    ) : (
                      <Row>
                        <Col xs="12" md="4">
                          <b>Rating : </b>
                          <Rating
                            initialRating={this.state.rating}
                            emptySymbol="fa fa-star-o fa-2x"
                            fullSymbol="fa fa-star fa-2x"
                            fractions={2}
                            value={this.state.rating}
                            readonly
                          />
                        </Col>
                        <Col xs="12" md="4">
                          <b>Comments : </b>
                          {this.state.timesheetcommand}
                        </Col>
                      </Row>
                    )}
                  </ModalBody>
                  <ModalFooter className="justify-content-between">
                    <Button color="secondary" onClick={this.viewpageclose} title="Close">
                      <i className="fa fa-close" />
                    </Button>
                    {this.state.status_timesheet === "" ? (
                      <Fragment>
                        <Button id="approve" color="success" onClick={e => this.approve(this.state.timesheetviewdata.shiftId)}>
                          <i className="fa fa-calendar-check-o" aria-hidden="true" />
                        </Button>
                        <UncontrolledTooltip placement="top" target="approve">
                          Approve Time Sheet
                        </UncontrolledTooltip>
                        <Button color="primary" className="ml-2" onClick={this.EditnestedModal} title="Edit">
                          <i className="fa fa-edit" />
                        </Button>
                      </Fragment>
                    ) : null}
                  </ModalFooter>
                </Fragment>
              )}
              {/* <ModalFooter>
                {this.state.status_timesheet === "" ? (
                  <Button color="success" onClick={e => this.approve(this.state.timesheetviewdata.shiftId)}>
                    Approve
                  </Button>
                ) : null}
                <Button color="secondary" onClick={this.viewpage}>
                  Close
                </Button>
              </ModalFooter> */}
            </Modal>
            <nav className="float-left">
              {/*<Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default timesheet;
