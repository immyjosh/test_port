import fileDownload from "js-file-download";
import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { toast, ToastContainer } from "react-toastify";
import ReactDOM from "react-dom";
import { DateRangePicker } from "react-dates";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import Loader from "../../common/loader";
import moment from "moment";
import { Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip, Badge } from "reactstrap";
import request from "../../../api/api";
import { breaklist, timelist } from "../../../views/common/utils";
import Pagination from "react-js-pagination";

var tStyle = {
  cursor: "pointer"
};
var tStyles = {
  width: "90px"
};

class Invoice extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    breaktimesheet: "",
    timesheet_endtime: "",
    timesheet_starttime: "",
    status_timesheet: "",
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      to_date: "",
      from_date: "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    earning: "",
    earncount: "",
    pending: "",
    pendingcount: "",
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    edit: false
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ timelist, breaklist, isLoader: true });
    this.populateData();
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/invoicedetails",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/agency/invoice/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          res.response.earnings.map((earn, i) => {
            if (earn._id === 7) {
              this.setState({
                earning: earn.client_rate,
                earncount: earn.count
              });
            }
            if (earn._id === 6) {
              this.setState({
                pending: earn.client_rate,
                pendingcount: earn.count
              });
            }
            return true;
          });
          let getshiftdata = [];
          this.setState({
            timesheetlists: res.response.result
          });
          res.response.result.map((item, i) => {
            var num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            var evestarthr = item.starttime / 3600;
            var splithr = evestarthr.toString().split(".");
            var startsec = splithr[1];
            if (startsec === undefined) {
              var startmins = "00";
            } else {
              startmins = (+"0" + "." + startsec) * 60;
            }

            if (num.indexOf(splithr[0]) === -1) {
              var starttimehr = splithr[0] + ":" + startmins;
            } else {
              starttimehr = +"0" + "" + splithr[0] + ":" + startmins;
            }
            var eveendhr = item.endtime / 3600;
            var splitendhr = eveendhr.toString().split(".");
            var endsec = splitendhr[1];
            if (endsec === undefined) {
              var endmins = "00";
            } else {
              endmins = (+"0" + "." + endsec) * 60;
            }

            if (num.indexOf(splitendhr[0]) === -1) {
              var endtimehr = splitendhr[0] + ":" + endmins;
            } else {
              endtimehr = +"0" + "" + splitendhr[0] + ":" + endmins;
            }
            getshiftdata.push({
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              client: item.client,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              job_type: item.job_type,
              employee: item.employee,
              employee_rate: item.employee_rate,
              client_rate: item.client_rate,
              title: item.shift_id,
              shiftId: item.shift_id,
              status: item.status
            });

            this.setState({
              adminlist: getshiftdata,
              pages: res.response.fullcount,
              currPage: res.response.length
            });
            return true;
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["start_date", "starttime", "title", "client", "location", "branch", "job_type", "employee", "client_rate", "employee_rate", "status"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/invoice/export",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }

  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  PageChange(value) {
    if (value === "client") {
      return this.props.history.push("/agency/invoiceclientlist");
    } else if (value === "shifts") {
      return this.props.history.push("/agency/invoice");
    }
  }
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Invoice List
            <div className="card-actions" style={tStyle} onClick={this.export}>
              <button style={tStyles}>
                <i className="fa fa-upload" /> Export
                {/*<small className="text-muted" />*/}
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row">
              <div className="col-lg-7 left-invoice-width agency_times">
               <div className="col-lg-8">
			   <DateRangePicker
                  showClearDates={true}
                  startDate={this.state.start_date}
                  startDateId="start_date"
                  endDate={this.state.end_date}
                  endDateId="end_date"
                  onDatesChange={({ startDate, endDate }) => {
                    this.setState({
                      start_date: startDate,
                      end_date: endDate
                    });
                  }}
                  isOutsideRange={day => day.isBefore(this.state.start_date)}
                  focusedInput={this.state.focusedInput}
                  onFocusChange={focusedInput => this.setState({ focusedInput })}
                  displayFormat="DD-MM-YYYY"
                />
                <Button
                  className="rounded-0"
                  size="md"
                  color="primary"
                  onClick={() => {
                    this.fromTo();
                  }}
                >
                  <i className="fa fa-search" />
                </Button>
				</div>
				<div className="col-lg-4">
				
				<Input onChange={e => this.PageChange(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-8">
                  <option value="shifts" selected>
                    Shifts
                  </option>
                  <option value="client">Client</option>
                </Input>
				</div>
              </div>
              <div className="col-lg-5 pr-0 right-invoice-width">
                <InputGroup>
                  <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-4">
                    <option>All</option>
                  </Input>
                  <Input
                    type="text"
                    ref="search"
                    placeholder="Search"
                    name="search"
                    onChange={e => this.search(e.target.value)}
                    className="rounded-0 col-lg-6"
                  />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive ct_scrl mt-2">
              <Table hover bordered responsive size="sm" className="custom_scroll">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("title");
                      }}
                    >
                      Shift <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("client");
                      }}
                    >
                      Client <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("location");
                      }}
                    >
                      Location <i style={{ paddingLeft: "25px" }} className={sorticondef} id="location" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("branch");
                      }}
                    >
                      Branch <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("job_type");
                      }}
                    >
                      Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_type" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("employee");
                      }}
                    >
                      Employee <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("client_rate");
                      }}
                    >
                      Client Rate <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client_rate" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("employee_rate");
                      }}
                    >
                      Employee Rate <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee_rate" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("starttime");
                      }}
                    >
                      Time <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" />
                    </th>

                    <th
                      onClick={() => {
                        this.sort("start_date");
                      }}
                    >
                      Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" />
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("end_date");
                      }}
                    >
                      End Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="username" />
                    </th> */}
                    <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Action</th>
                    {/* <th>Actions</th> */}
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={i}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.title}</td>
                        <td>{item.client}</td>
                        <td>{item.location}</td>
                        <td>{item.branch}</td>
                        <td>{item.job_type}</td>
                        <td>{item.employee}</td>
                        <td>£ {item.client_rate}</td>
                        <td>£ {item.employee_rate}</td>
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        {/* <td>{moment(item.end_date).format('DD-MM-YYYY')}</td> */}
                        <td>{item.status === 2 ? <Badge color="success">Approved</Badge> : <Badge color="danger">Pending</Badge>}</td>
                        <td>
                          <div>
                            <button type="button" title="Edit" className="btn btn-primary btn-sm" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                              <i className="fa fa-eye" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit${i}`}>
                              View
                            </UncontrolledTooltip>
                          </div>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={13}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              {/*<Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Invoice;
