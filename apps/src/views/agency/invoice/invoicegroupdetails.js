import fileDownload from "js-file-download";
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { SingleDatePicker } from "react-dates";
import { AvForm, AvField, AvGroup, AvInput, AvFeedback } from "availity-reactstrap-validation";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import moment from "moment";
import { Button, Card, CardBody, CardHeader, CardFooter, Col, Badge, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table, UncontrolledTooltip, Label } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import { breaklist, timelist } from "../../common/utils";
import { Link } from "react-router-dom";
import FileSaver from "file-saver";
import { Editor } from "@tinymce/tinymce-react";
import { Settings } from "../../../api/key";

const tStyle = {
  cursor: "pointer"
};

class Invoicegroupdetails extends Component {
  state = {
    adminlist: [],
    url: "",
    mailmodal: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    timesheetlists: [],
    timesheetid: "",
    timesheetviewdata: "",
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    singlecheck: false,
    count: 0,
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 100,
      skip: 0,
      to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : "",
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : "",
      clientID: this.props && this.props.location.state ? this.props.location.state.rowid._id.client[0]._id : ""
      // employeeID: this.props && this.props.location.state ? this.props.location.state.rowid._id.employee[0]._id : "",
      // locationID: this.props && this.props.location.state ? this.props.location.state.rowid._id.locations[0]._id : "",
      // branchID: this.props && this.props.location.state ? this.props.location.state.rowid._id.branch[0].branches._id : "",
      // jobtypeID: this.props && this.props.location.state ? this.props.location.state.rowid._id.job_type[0]._id : ""
    },
    start_date: null,
    end_date: null,
    success: false,
    timelist: [],
    breaklist: [],
    timesheetcommand: "",
    rating: 0,
    time_start_date: null || "",
    time_end_date: null || "",
    edit: false,
    focusedEnd: "",
    focused: "",
    openDirectiondata: "",
    client_avatar: "",
    agency_avatar: "",
    viewtimesheetid: "",
    client_data: {},
    timesheet: [],
    ForediState: false,
    ShowApprovebtn: true,
    total_rate: "",
    EditTbnState: false,
    clienttotal_rate: "",
    latetotal_rate: "",
    invoicepaiddate: "",
    invoicepaidthrough: "",
    invoiceamount: "",
    invoicereference: "",
    invoicePrefix: "",
    invoice_to_date: "",
    invoice_from_date: "",
    currentusername: "",
    InvoiceCycleDetails: "",
    INVCurrentTotal: "",
    invoice_due_days: "",
    client_email: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      timelist,
      breaklist,
      currentusername: token && token.username,
      isLoader: true,
      invoice_to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
      invoice_from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : ""
    });
    this.populateData();
    request({
      url: "/agency/emailtemplate/selecttypedata",
      method: "POST",
      data: { type: "invocie" }
    }).then(res => {
      if (res.status === 1) {
        this.setState({ emailtemplatelist: res.response.result });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  populateData() {
    request({
      url: "/agency/invoice/employee/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      this.setState({ isLoader: false });
      if (res.status === 1) {
        if (res.response !== undefined) {
          const getshiftdata = [];
          this.setState({
            timesheetlists: res.response.result
          });
          const clientAr = res.response.result.map(list => list.client_rate);
          const clienttotalrate = clientAr.reduce((a, b) => a + b) || 0;
          const lateAr = res.response.result.map(list => list.late_amount);
          const latetotalrate = lateAr.reduce((a, b) => a + b) || 0;
          const totalrate = clienttotalrate + latetotalrate || 0;
          // totalrate = clienttotalrate;
          res.response.result.map((item, i) => {
            const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            const evestarthr = item.starttime / 3600;
            const splithr = evestarthr.toString().split(".");
            const startsec = splithr[1];
            const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
            const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
            const eveendhr = item.endtime / 3600;
            const splitendhr = eveendhr.toString().split(".");
            const endsec = splitendhr[1];
            const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
            const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
            const timesheet_status = item.timesheet_status ? item.timesheet_status : "";
            getshiftdata.push({
              _id: item._id,
              starttime: starttimehr,
              endtime: endtimehr,
              location: item.locations,
              branch: item.branch[0].branches.branchname,
              breaktime: item.breaktime,
              start_date: item.start_date,
              end_date: item.end_date,
              client: item.client,
              timesheet: item.timesheet,
              job_type: item.job_type,
              job_rate: item.rate_details.length > 0 ? item.rate_details[0].client_rate : "",
              employee: item.employee,
              employee_rate: item.employee_rate,
              late_amount: item.late_amount,
              client_rate: item.client_rate,
              title: item.shift_id,
              shiftId: item.shift_id,
              shift_data: item.shift_data,
              status: item.status,
              timesheetid: item._id,
              timesheet_status: timesheet_status,
              employee_avatar: item.employee_avatar,
              comment: item.agency_comment,
              rating: item.agency_rating
            });
            this.setState({
              adminlist: getshiftdata,
              pages: res.response.fullcount,
              currPage: res.response.length,
              total_rate: totalrate,
              clienttotal_rate: clienttotalrate,
              latetotal_rate: latetotalrate,
              client_avatar: res.response.result.length > 0 ? res.response.result[0].client_data[0].company_logo : "",
              agency_avatar: res.response.result.length > 0 ? res.response.result[0].agency_data[0].company_logo : "",
              client_data: res.response.result.length > 0 ? res.response.result[0].client_data[0] : {},
              client_email: res.response.result.length > 0 ? res.response.result[0].client_data[0].email : "",
              agency_data: res.response.result.length > 0 ? res.response.result[0].agency_data[0] : {}
            });
            return true;
          });
        }
        let invoicePrefix = "";
        const TimeID = res.response.result.filter(list => list.invoiceID);
        if (TimeID.length > 0) {
          invoicePrefix = TimeID[0].invoiceID;
          this.GetInvoice(invoicePrefix);
        } else {
          request({
            url: "/site/invoiceprefixnumber",
            method: "POST",
            data: { for: "agency", username: this.state.currentusername }
          }).then(res => {
            if (res.status === 1) {
              invoicePrefix = res.response;
            } else if (res.status === 0) {
              if (res.response === "invoice Settings Not Available") {
                // this.props.history.push({
                //   pathname: "/agency/general",
                //   state: { activetab: "4" }
                // });
                this.props.history.push("/agency/invoiceclientlist");
                toast.error("Please Update invoice Settings");
              } else {
                toast.error(res.response);
              }
            }
          });
        }
        setTimeout(() => {
          this.setState({ invoicePrefix });
        }, 1000);
        const time_Sta = res.response.result.filter(list => list.status === 1);
        if (time_Sta.length === 0) {
          this.setState({ ShowApprovebtn: false });
        }
        this.GetSettings();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  GetInvoice(value) {
    request({
      url: "/agency/invoice/getinvoicecycle",
      method: "POST",
      data: { invoiceID: value }
    }).then(res => {
      if (res.status === 1) {
        let CurrentRate = 0;
        if (res.response && res.response.payment_details && res.response.payment_details.length > 0) {
          const CurrAr = res.response.payment_details.map(list => list.amount);
          CurrentRate = CurrAr && CurrAr.length > 0 && CurrAr.reduce((a, b) => a + b);
        }
        this.setState({
          InvoiceCycleDetails: res.response,
          INVCurrentTotal: CurrentRate,
          invoice_due_date: res.response.due_date ? moment(res.response.due_date) : ""
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  GetSettings() {
    request({
      url: "/agency/profile",
      method: "POST",
      data: { id: this.state.currentusername }
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            invoice_due_days: res.response.result ? res.response.result[0].settings.general.invoice_due_days : ""
          });
        } else if (res.status === 0) {
          toast.error(res);
        }
      })
      .catch(error => {
        return console.log(error);
      });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["starttime", "start_date", "employee", "branch", "title", "client_rate", "late_amount"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/timesheet/timesheetexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  TimesheettoastId = "Timesheet";
  nodify() {
    if (!toast.isActive(this.TimesheettoastId)) {
      this.TimesheettoastId = toast.warn("Updated");
    }
  }
  adminsavenodifytoastId = "Timesheet";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifytoastId)) {
      this.adminsavenodifytoastId = toast.success("Successfully Saved!");
    }
  }
  viewpage = e => {
    this.state.timesheetlists.filter((value, key) => {
      if (value._id === e.timesheetid) {
        this.setState({
          timesheetviewdata: value,
          viewtimesheetid: e.timesheetid
        });
        if (e.timesheet_status === 2) {
          this.setState({
            status_timesheet: e.timesheet_status
          });
        } else {
          this.setState({
            status_timesheet: ""
          });
        }
        const num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
        const evestarthr = value.starttime / 3600;
        const splithr = evestarthr.toString().split(".");
        const startsec = splithr[1];
        const startmins = startsec === undefined ? "00" : (+"0" + "." + startsec) * 60;
        const starttimehr = num.indexOf(splithr[0]) === -1 ? splithr[0] + ":" + startmins : +"0" + "" + splithr[0] + ":" + startmins;
        const eveendhr = value.endtime / 3600;
        const splitendhr = eveendhr.toString().split(".");
        const endsec = splitendhr[1];
        const endmins = endsec === undefined ? "00" : (+"0" + "." + endsec) * 60;
        const endtimehr = num.indexOf(splitendhr[0]) === -1 ? splitendhr[0] + ":" + endmins : +"0" + "" + splitendhr[0] + ":" + endmins;
        this.setState({
          breaktimesheet: value.breaktime,
          employee_rate_sheet: value.employee_rate,
          time_start_date: moment(value.start_date),
          time_end_date: moment(value.end_date),
          client_rate_sheet: value.client_rate,
          timesheetcommand: value.comment || e.comment || "",
          rating: value.rating || e.rating || "",
          timesheet_starttime: value.starttime,
          timesheet_endtime: value.endtime,
          timesheet: value.timesheet,
          shift_id: value.shift_id,
          timesheet_starttimeview: starttimehr,
          timesheet_endtimeview: endtimehr
        });
      }
      return true;
    });
    if (e.update !== "update") {
      this.setState({
        success: !this.state.success,
        edit: false
      });
    } else {
      this.setState({
        edit: false
      });
    }
  };
  EditnestedModal = () => {
    this.setState({
      edit: true
    });
  };
  EditnestedModalview = () => {
    this.setState({
      edit: false
    });
  };
  approve() {
    request({
      url: "/agency/invoice/approval",
      method: "POST",
      data: {
        shiftId: this.state.timesheetviewdata._id,
        comment: this.state.timesheetcommand,
        rating: this.state.rating,
        invoiceID: this.state.invoicePrefix,
        to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
        from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
      }
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        toast.success("Invoice Approved!");
        this.setState({
          success: !this.state.success
        });
        setTimeout(() => {
          this.componentDidMount();
        }, 3500);
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  selectstarttimeChange = value => {
    if (value) {
      this.setState({ timesheet_starttime: value.value, selecterror: false });
    } else {
      this.setState({ timesheet_starttime: "", selecterror: true });
    }
  };
  selectendtimeChange = value => {
    if (value) {
      this.setState({ timesheet_endtime: value.value, selectenderror: false });
    } else {
      this.setState({ timesheet_endtime: "", selectenderror: true });
    }
  };
  selectbreaktimeChange = value => {
    if (value) {
      this.setState({ breaktimesheet: value.value, selectbreakerror: false });
    } else {
      this.setState({ breaktimesheet: "", selectbreakerror: true });
    }
  };
  checkbox(index, id) {
    const c = document.getElementById(index);
    this.setState({
      singlecheck: true
    });
    const ca = document.getElementById("checkall");
    let count = this.state.count;
    const len = this.state.currPage;
    const bulk = this.state.bulk;
    if (c.checked === true) {
      count++;
      bulk.push(id);
      this.setState({ count, bulk });
    } else {
      count--;
      const i = bulk.indexOf(id);
      bulk.splice(i, 1);
      this.setState({ count, bulk });
    }
    if (count === len) {
      ca.checked = true;
      this.checkall("checkall", len);
    } else {
      ca.checked = false;
    }
  }
  checkall(id, len) {
    const c = document.getElementById(id);
    const val = [];
    if (c.checked) {
      this.setState({ count: len });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = true;
        val.push(document.getElementById(i).value);
      }
      this.setState({ bulk: val });
    } else {
      this.setState({ count: 0 });
      for (let i = 0; i < len; i++) {
        document.getElementById(i).checked = false;
      }
      this.setState({ bulk: [] });
    }
  }
  ApproveAll() {
    let toastId = "Approve_Invoice_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    const valued = this.state.adminlist.filter(list => list.status === 1);
    const values = valued.map(list => list._id);
    request({
      url: "/agency/invoice/groupapproval",
      method: "POST",
      data: {
        invoiceids: values,
        invoiceID: this.state.invoicePrefix,
        to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
        from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
      }
    }).then(res => {
      if (res.status === 1) {
        this.componentDidMount();
        this.setState({ ForediState: false });
        setTimeout(() => {
          this.componentDidMount();
        }, 3500);
        toast.update(toastId, { render: "Invoice Approved!", type: toast.TYPE.SUCCESS, autoClose: 2500 });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  EditBTN() {
    this.setState({ EditTbnState: !this.state.EditTbnState });
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/invoicedetails",
      state: { rowid: id, Getpage: { previd: this.props.location.state.rowid, Dates: this.props.location.state.Dates } }
    });
  };
  MailModalOpen = () => {
    if (!this.state.mailmodal) {
      const typeid = this.state.emailtemplatelist.filter(list => list.default_mail).map(list => list._id)[0];
      this.Templatelist(typeid);
      this.setState({ typeid });
    }
    this.setState({
      mailmodal: !this.state.mailmodal
    });
  };
  InvoiceDownloadPDF = () => {
    let toastId = "Down_Invoice_01";
    client.defaults.responseType = "blob";
    toastId = toast.info("Downloading......", { autoClose: false });
    if (this.state.invoicePrefix) {
      request({
        url: "/agency/invoice/downloadpdf",
        method: "POST",
        data: {
          TabOpt: this.state.tableOptions,
          for: "download",
          invoice_invoicePrefix: this.state.invoicePrefix,
          client_email: this.state.client_email,
          invoice_to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
          invoice_from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
        }
      })
        .then(res => {
          if (res) {
            const file = new Blob([res], { type: "application/pdf" });
            const name = `Invoice_${this.state.invoicePrefix}.pdf`;
            FileSaver(file, name);
            toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
            client.defaults.responseType = "json";
          } else {
            toast.update(toastId, { render: "Please try again later", type: toast.TYPE.ERROR, autoClose: 2500 });
          }
          client.defaults.responseType = "json";
        })
        .catch(error => {
          client.defaults.responseType = "json";
          toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
        });
    } else {
      client.defaults.responseType = "json";
      toast.update(toastId, { render: "Unable to Download", type: toast.TYPE.ERROR, autoClose: 2500 });
    }
  };
  InvoiceMailPDF = () => {
    let toastId = "Mail_Invoice_01021";
    client.defaults.responseType = "json";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/agency/invoice/downloadpdf",
      method: "POST",
      data: {
        TabOpt: this.state.tableOptions,
        for: "mail",
        invoice_invoicePrefix: this.state.invoicePrefix,
        client_email: this.state.client_email,
        email_content: this.state.email_content,
        email_subject: this.state.email_subject,
        invoice_to_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.to).format("DD-MM-YYYY") : "",
        invoice_from_date: this.props && this.props.location.state ? moment(this.props.location.state.Dates.from).format("DD-MM-YYYY") : ""
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.MailModalOpen();
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  InvoicePayment = () => {
    if (this.state.invoicepaiddate === "") {
      toast.error("Please Select Date");
    } else {
      let toastId = "pay_Invoice_0111";
      toastId = toast.info("Please Wait......", { autoClose: false });
      request({
        url: "/agency/invoice/addpayment",
        method: "POST",
        data: {
          invoiceID: this.state.invoicePrefix,
          to_date: this.props && this.props.location.state ? this.props.location.state.Dates.to : "",
          from_date: this.props && this.props.location.state ? this.props.location.state.Dates.from : "",
          clientID: this.props && this.props.location.state ? this.props.location.state.rowid._id.client[0]._id : "",
          date: this.state.invoicepaiddate,
          through: this.state.invoicepaidthrough,
          amount: this.state.invoiceamount,
          reference: this.state.invoicereference
        }
      })
        .then(res => {
          if (res.status === 1) {
            toast.update(toastId, { render: "Payment Success", type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
            setTimeout(() => {
              this.form && this.form.reset();
              this.setState({ invoicepaiddate: "" });
            }, 1000);
          } else if (res.status === 0) {
            toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  UpdateInvoiceCycle = () => {
    let toastId = "update_Invoice_Cycle_0111";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/agency/invoicecycle/update",
      method: "POST",
      data: {
        invoiceID: this.state.invoicePrefix,
        due_date: this.state.invoice_due_date
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: "Updated", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
          this.setState({ EditTbnState: false });
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  onChangeSelect = e => {
    if (e) {
      this.Templatelist(e.target.value);
      this.setState({ [e.target.name]: e.target.value });
    }
  };
  Templatelist(value) {
    request({
      url: "/agency/emailtemplate/getemailcontent",
      method: "POST",
      data: { id: value, client: this.state.client_data && this.state.client_data.companyname, employee: this.state.employee_name }
    }).then(res => {
      if (res.status === 1) {
        const data = res.response.result;
        this.setState({
          email_content: data.html,
          email_subject: data.subject
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  render() {
    const { emailtemplatelist, email_content, client_email, typeid, email_subject } = this.state;
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            {/* <i className="icon-list" />*/}
            Invoice Details
            <div className="card-actions" style={tStyle}>
              <button onClick={() => this.InvoiceDownloadPDF()}>
                <i className="fa fa-download" /> Download
                <small className="text-muted" />
              </button>
              <button onClick={this.MailModalOpen}>
                <i className="fa fa-envelope" /> Mail
                <small className="text-muted" />
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="new_invoice_pages">
              <div className="invoice_logos" />
              <div className="top_address_wraps">
                <div className="col-md-6">
                  <div className="left_invoive_add">
                    <h1>INVOICE</h1>
                    <p>
                      {this.state.client_data.companyname}
                      <br />
                      {this.state.client_data.address ? (
                        <Fragment>
                          {this.state.client_data.address.formatted_address},{this.state.client_data.address.zipcode} .
                        </Fragment>
                      ) : null}{" "}
                      <br />
                      {this.state.client_data.address ? (
                        <Fragment>
                          Tel. : {this.state.client_data.phone.code} - {this.state.client_data.phone.number}
                        </Fragment>
                      ) : null}{" "}
                    </p>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="col-md-12 crop-img">
                    <img
                      width="250px"
                      src={`${NodeURL}/${this.state.agency_avatar}`}
                      alt="Profile"
                      onError={() => {
                        this.setState({ agency_avatar: "../../img/user-profile.png" });
                      }}
                    />
                  </div>

                  <div className="right_invoive_add">
                    <div className="col-md-6">
                      <h1>Invoice Date </h1>
                      <p>
                        {moment(this.state.invoice_from_date).format("DD-MM-YYYY")} - {moment(this.state.invoice_to_date).format("DD-MM-YYYY")}{" "}
                      </p>
                      <h1>Invoice Number</h1>
                      <p>{this.state.invoicePrefix}</p>
                    </div>
                    <div className="col-md-6">
                      <p>
                        {" "}
                        {this.state.agency_data ? <Fragment>{this.state.agency_data.company_name}</Fragment> : null} <br />
                        {this.state.agency_data && this.state.agency_data.address ? (
                          <Fragment>
                            {this.state.agency_data.address.formatted_address},{this.state.agency_data.address.zipcode} .
                          </Fragment>
                        ) : null}{" "}
                        <br />
                        {this.state.agency_data && this.state.agency_data.address ? (
                          <Fragment>
                            Tel. : {this.state.agency_data.phone.code} - {this.state.agency_data.phone.number}
                          </Fragment>
                        ) : null}{" "}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              {this.state.adminlist.length &&
                this.state.adminlist.slice(0, 1).map((list, i) => (
                  <div className="agency-arra pt-0" key={i}>
                    <div className="name-date">
                      <div className="timesheet-info">
                        <h5>Client Name</h5>
                        <p>
                          <span>{list.client}</span>
                        </p>
                      </div>
                      <div className="timesheet-info">
                        <h5>Date</h5>
                        <p>{moment(new Date()).format("DD-MM-YYYY")}</p>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
            <div className="table-responsive mt-2 new_table_design">
              <Table hover responsive>
                <thead>
                  <tr>
                    {/* <th>
                                        <input type="checkbox" id="checkall" onClick={() => this.checkall("checkall", this.state.currPage)} />
                                    </th>*/}
                    {/* <th>S.No.</th> */}
                    <th
                    // onClick={() => {
                    //   this.sort("title");
                    // }}
                    >
                      Shift No.
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="title" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("branch");
                    // }}
                    >
                      Work Location {/* Branch*/}
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="branch" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("employee");
                    // }}
                    >
                      Employee
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employee" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("start_date");
                    // }}
                    >
                      Date
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="start_date" /> */}
                    </th>
                    <th
                    // onClick={() => {
                    //   this.sort("starttime");
                    // }}
                    >
                      Time
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="starttime" /> */}
                    </th>
                    {/* <th
                      onClick={() => {
                        this.sort("late_amount");
                      }}
                    >
                      Late Fee <i style={{ paddingLeft: "25px" }} className={sorticondef} id="late_amount" />
                    </th> */}
                    {/* <th
                      onClick={() => {
                        this.sort("status");
                      }}
                      >
                      Status
                      <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th> */}
                    <th
                    // onClick={() => {
                    //   this.sort("job_rate");
                    // }}
                    >
                      Rate
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="job_rate" /> */}
                    </th>
                    <th>Time Worked</th>
                    <th
                    // onClick={() => {
                    //   this.sort("client_rate");
                    // }}
                    >
                      Total Rate
                      {/* <i style={{ paddingLeft: "25px" }} className={sorticondef} id="client_rate" /> */}
                    </th>
                    {this.state.EditTbnState ? <th>Actions</th> : null}
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item.shiftId}>
                        {/* <td>
                                                <input type="checkbox" className="checkbox1" id={i} value={item._id} onClick={() => this.checkbox(i, item._id)} />
                                            </td>*/}
                        {/* <td>{this.state.tableOptions.skip + i + 1}</td> */}
                        <td>{item.title}</td>
                        <td>{item.branch}</td>
                        <td>{item.employee}</td>
                        <td>{moment(item.start_date).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.starttime} - {item.endtime}
                        </td>
                        <td>£ {item.job_rate}</td>
                        <td>{Math.round(item.shift_data[0].timesheet[0].workmins / 60)} Minutes</td>
                        <td>£ {item.client_rate}</td>
                        {this.state.EditTbnState ? (
                          <td>
                            <Fragment>
                              <button type="button" title="Edit" className="btn btn-primary btn-sm" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                                <i className="fa fa-eye" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                View
                              </UncontrolledTooltip>
                            </Fragment>
                          </td>
                        ) : null}
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={20}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}

                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
              <div className="overall-total ">
                <div className="total-price">
                  <ul>
                    {/* <li>
                      <span className="pull-left">Client Rate</span> <span className="pull-right">£ {this.state.clienttotal_rate}</span>{" "}
                    </li>*/}
                    <li className="line-values">
                      <span className="pull-left align-width-left">Late Booking Fee</span> <span className="align-width-right"> £ {this.state.latetotal_rate}</span>
                    </li>
                    <li>
                      <span className="pull-left align-width-left">
                        <b>Total</b>
                      </span>{" "}
                      <span className="align-width-right"> £ {this.state.total_rate}</span>
                    </li>
                    <li>
                      <span className="pull-left align-width-left">Due Amount</span> <span className="align-width-right">£ {this.state.total_rate - this.state.INVCurrentTotal}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </CardBody>
          <CardFooter>
            <Link to="/agency/invoiceclientlist">
              <Button type="button" color="secondary" title="Back">
                <i className="fa fa-arrow-left" /> Back
              </Button>
            </Link>
            {this.state.ShowApprovebtn ? (
              <Fragment>
                <Button id="approve" size="md" color="success" title="Approve" className={"pull-right"} onClick={() => this.ApproveAll()}>
                  Approve
                </Button>
              </Fragment>
            ) : null}
            <Button id="approve" color="danger" title="Edit" className={"pull-right"} onClick={() => this.EditBTN()}>
              Edit
            </Button>
          </CardFooter>
          <CardBody>
            <Row className="d-flex justify-content-end">
              {this.state.total_rate - this.state.INVCurrentTotal === 0 ? (
                <Badge color="success" className="pull-right">
                  Paid
                </Badge>
              ) : null}
            </Row>
            {!this.state.ShowApprovebtn && this.state.total_rate - this.state.INVCurrentTotal !== 0 ? (
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.InvoicePayment}>
                <Row>
                  <Col xs="12" md="2">
                    <AvGroup>
                      <Label>Amount</Label>
                      <AvInput type="text" name="invoiceamount" placeholder="Enter Amount" onChange={this.onChange} value={this.state.invoiceamount} required min="0" numberOfMonths={1} autoComplete="invoiceamount" />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="2">
                    <Label>Date</Label>
                    <SingleDatePicker
                      numberOfMonths="1"
                      date={this.state.invoicepaiddate}
                      onDateChange={date => this.setState({ invoicepaiddate: date })}
                      focused={this.state.focused2}
                      onFocusChange={({ focused: focused2 }) => this.setState({ focused2 })}
                      id="invoicepaiddate"
                      displayFormat="DD-MM-YYYY"
                    />
                  </Col>
                  <Col xs="12" md="2">
                    <AvField type="select" name="invoicepaidthrough" label="Paid Through" onChange={this.onChange} value={this.state.invoicepaidthrough} required>
                      <option>Select</option>
                      <option value="cash">Cash</option>
                      <option value="debit_card">Debit Card</option>
                      <option value="credit_card">Credit Card</option>
                    </AvField>
                  </Col>
                  <Col xs="12" md="3">
                    <AvGroup>
                      <Label>Reference</Label>
                      <AvInput type="text" name="invoicereference" placeholder="" onChange={this.onChange} value={this.state.invoicereference} autoComplete="invoicereference" required />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
                  <Col xs="12" md="3" className="mt-4">
                    <Button color="success" title="Add Payment" className={"pull-right"} type="submit">
                      Add Payment
                    </Button>
                  </Col>
                </Row>
              </AvForm>
            ) : null}
          </CardBody>
          <div className="due_payment">
            <div className="inner_dues dues-parts">
              {!this.state.ShowApprovebtn && this.state.EditTbnState && this.state.total_rate - this.state.INVCurrentTotal !== 0 ? (
                <AvForm onValidSubmit={this.UpdateInvoiceCycle}>
                  <Row className="virw-page">
                    <Col xs="12" md="12" className="arrr-inputs">
                      <h1>
                        Due Date:{" "}
                        <SingleDatePicker
                          numberOfMonths="1"
                          date={this.state.invoice_due_date}
                          onDateChange={date => this.setState({ invoice_due_date: date })}
                          focused={this.state.focused}
                          onFocusChange={({ focused }) => this.setState({ focused })}
                          id="invoice_due_date"
                          displayFormat="DD-MM-YYYY"
                        />
                        <Button size="md" color="success" title="Add Payment" type="submit">
                          Update
                        </Button>
                      </h1>
                    </Col>
                  </Row>
                </AvForm>
              ) : (
                <h1>Due Date: {moment(this.state.invoice_due_date).format("DD-MM-YYYY")} </h1>
              )}
            </div>
          </div>
        </Card>
        <Modal isOpen={this.state.mailmodal} className="modal-lg">
          <Fragment>
            <ModalHeader toggle={this.MailModalOpen}>Invoices</ModalHeader>
            <AvForm onValidSubmit={this.InvoiceMailPDF}>
              <ModalBody>
                <Row>
                  <Col xs="12" md="6">
                    <AvField name="client_email" onChange={this.onChange} value={client_email} placeholder="E-mail" label="TO" type="email" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                  <Col xs="12" md="6">
                    <AvField type="select" name="typeid" label="Email Template" onChange={this.onChangeSelect} value={typeid} required>
                      <option>Select</option>
                      {emailtemplatelist && emailtemplatelist.length > 0 ? (
                        <>
                          {emailtemplatelist.map((list, i) => (
                            <option key={i} value={list._id}>
                              {list.heading}
                            </option>
                          ))}
                        </>
                      ) : null}
                    </AvField>
                  </Col>
                  <Col xs="12" md="12">
                    <AvField name="email_subject" onChange={this.onChange} value={email_subject} placeholder="Subject" label="Subject" type="text" errorMessage="This is required" validate={{ required: { value: true } }} />
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="12">
                    <p>Message</p>
                    <Editor
                      apiKey={Settings.tinymceKey}
                      initialValue={email_content}
                      init={{
                        plugins: "link image code",
                        toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                        height: 480
                      }}
                      onChange={this.handleEditorChange}
                      value={email_content}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between">
                <div>
                  <Button color="secondary" type="button" onClick={this.MailModalOpen} title="Close">
                    <i className="fa fa-close" />
                  </Button>
                </div>
                <Button color="success" type="submit" title="Send Mail" id="savecontinue2">
                  {" "}
                  <i className="fa fa-envelope-open-o" />
                </Button>{" "}
                <UncontrolledTooltip placement="left" target="savecontinue2">
                  Send Mail
                </UncontrolledTooltip>
              </ModalFooter>
            </AvForm>
          </Fragment>
        </Modal>
      </div>
    );
  }
}

export default Invoicegroupdetails;
