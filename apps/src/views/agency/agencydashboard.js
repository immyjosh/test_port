import React, { Component, Fragment } from "react";
import { toast } from "react-toastify";
import { Col, Row, Card, CardBody, CardHeader, Input } from "reactstrap";
import { Bar } from "react-chartjs-2";
import request, { client } from "../../api/api";
import Widget02 from "../../views/Template/Widgets/Widget02";
import Recentclientlist from "./client/recentclientlist";
import Recentemployeelist from "./employees/recentemployeelist";
import Calenterview from "./shift/allshifts";
import GodView from "./shift/showemployees";

class Agencydashboard extends Component {
  state = {
    userlist: [],
    sublist: [],
    clientlist: [],
    employeelist: [],
    allclientcount: [],
    allemployeescount: [],
    agenydashboard: "",
    deletedisable: true,
    assigned: 0,
    completed: 0,
    ongoing: 0,
    timehseet_approved: "",
    invoice_approved: "",
    Payment_completed: "",
    expired: "",
    draft: "",
    approved: "",
    part_paid: "",
    paid: "",
    draft_rate: "",
    approved_rate: "",
    part_paid_rate: "",
    paid_rate: "",
    selectshift: "all",
    selecttimesheet: 0,
    selectinvoice: "all",
    totalshiftCount: 0
  };

  componentDidMount() {
    const token = this.props.token_role;
    if (this.props.location.state === true) {
      this.nodify(token);
    }
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    client.defaults.headers.common["Authorization"] = this.props.token;
    this.populate();
  }
  populate() {
    request({
      url: "/agencydashboard",
      method: "post",
      data: { selectshift: this.state.selectshift, selecttimesheet: this.state.selecttimesheet, selectinvoice: this.state.selectinvoice }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result && res.response.result.length > 0) {
          this.setState({ agenydashboard: res.response.result && res.response.result.length > 0 ? res.response.result[0] : {} });
          if (res.response.result[0].shifts) {
            const data = res.response.result[0].shifts && res.response.result[0].shifts.length > 0 && res.response.result[0].shifts[0];
            this.setState({
              assigned: data.assigned === 0 ?  0 : data.assigned ,
              completed: data.completed === 0 ? 0 : data.completed ,
              ongoing:  data.ongoing === 0 ? 0 : data.ongoing ,
              timehseet_approved: data.timehseet_approved || 0,
              invoice_approved: data.invoice_approved || 0,
              Payment_completed: data.Payment_completed || 0,
              expired: data.expired || 0,
              totalshiftCount:(data.assigned ? data.assigned : 0) +
                              (data.completed ? data.completed : 0) +
                              (data.ongoing ? data.ongoing : 0) +
                              (data.timehseet_approved ? data.timehseet_approved : 0) +
                              (data.invoice_approved ? data.invoice_approved : 0) +
                              (data.Payment_completed ? data.Payment_completed : 0) +
                              (data.expired ? data.expired : 0)
                              
            });
          }
          if (res.response.result[0].invoices) {
            const data = res.response.result[0].invoices && res.response.result[0].invoices.length > 0 && res.response.result[0].invoices[0];
            this.setState({
              draft: data.draft || 0,
              approved: data.approved || 0,
              part_paid: data.part_paid || 0,
              paid: data.paid || 0,
              draft_rate: data.draft_rate || 0,
              approved_rate: data.approved_rate || 0,
              part_paid_rate: data.part_paid_rate || 0,
              paid_rate: data.paid_rate || 0
            });
          }
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  nodifytoastId = "Agencydashboard";
  nodify = token => {
    if (!toast.isActive(this.nodifytoastId)) {
      this.nodifytoastId = toast.success(`Welcome  ${token.username}`);
      this.props.history.push({ state: false });
    }
  };
  updatenodifytoastId = "Agencydashboard";
  updatenodify() {
    if (!toast.isActive(this.updatenodifytoastId)) {
      this.updatenodifytoastId = toast.success("Updated!");
    }
  }
  paymentnodifytoastId = "Agencydashboard";
  paymentnodify() {
    if (!toast.isActive(this.paymentnodifytoastId)) {
      this.paymentnodifytoastId = toast.success("Payment Completed!");
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.populate());
  };
  render() {
    // const doughnut = {
    //   labels: ["Active", "In-active"],
    //   datasets: [
    //     {
    //       data: [
    //         this.state.agenydashboard && this.state.agenydashboard.activeclient ? this.state.agenydashboard.activeclient : "0",
    //         this.state.agenydashboard && this.state.agenydashboard.inactiveclient ? this.state.agenydashboard.inactiveclient : "0"
    //       ],
    //       backgroundColor: ["#4dbd74", "#f86c6b"],
    //       hoverBackgroundColor: ["#4dbd74", "#f86c6b"]
    //     }
    //   ]
    // };
    // const pie = {
    //   labels: ["Active", "In-active"],
    //   datasets: [
    //     {
    //       data: [
    //         this.state.agenydashboard && this.state.agenydashboard.activeemployee ? this.state.agenydashboard.activeemployee : "0",
    //         this.state.agenydashboard && this.state.agenydashboard.inactiveemployee ? this.state.agenydashboard.inactiveemployee : "0"
    //       ],
    //       backgroundColor: ["#4dbd74", "#f86c6b"],
    //       hoverBackgroundColor: ["#4dbd74", "#f86c6b"]
    //     }
    //   ]
    // };

    const bar = {
      labels: ["Shifts"],
      datasets: [
        // {
        //   label: "Added",
        //   backgroundColor: "#20a8d8b8",
        //   borderColor: "#1382a9",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#20a8d8",
        //   hoverBorderColor: "#1382a9",
        //   data: [this.state && this.state.added]
        // },
        // {
        //   label: "Accepted",
        //   backgroundColor: "#f8cb00b0",
        //   borderColor: "#bb9a04",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#f8cb00",
        //   hoverBorderColor: "#bb9a04",
        //   data: [this.state && this.state.accepted]
        // },
        {
          label: "Total",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.totalshiftCount]
        },
        {
          label: "Assigned",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.assigned]
        },
        {
          label: "Ongoing",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.ongoing]
        },
        {
          label: "Completed",
          backgroundColor: "#4dbd74",
          borderColor: "#4dbd74",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#4dbd74",
          data: [this.state && this.state.completed]
        }
        // {
        //   label: "Timesheets Approved",
        //   backgroundColor: "#f8cb00b0",
        //   borderColor: "#bb9a04",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#f8cb00",
        //   hoverBorderColor: "#bb9a04",
        //   data: [this.state && this.state.timehseet_approved]
        // },
        // {
        //   label: "Invoice Approved",
        //   backgroundColor: "#63c2dead",
        //   borderColor: "#2fa0c1",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#63c2de",
        //   hoverBorderColor: "#2fa0c1",
        //   data: [this.state && this.state.invoice_approved]
        // },
        // {
        //   label: "Paid",
        //   backgroundColor: "#818a91b8",
        //   borderColor: "#595d61",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#818a91",
        //   hoverBorderColor: "#595d61",
        //   data: [this.state && this.state.Payment_completed]
        // },
        // {
        //   label: "Expired",
        //   backgroundColor: "#f86c6bb3",
        //   borderColor: "#c3403f",
        //   borderWidth: 1,
        //   hoverBackgroundColor: "#f86c6b",
        //   hoverBorderColor: "#c3403f",
        //   data: [this.state && this.state.expired]
        // }
      ]
    };

    const bar1 = {
      labels: ["Invoices"],
      datasets: [
        {
          label: "Draft",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state && this.state.draft_rate]
        },
        {
          label: "Approved Rate",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state && this.state.approved_rate]
        },
        {
          label: "Part Paid",
          backgroundColor: "#20a8d8b8",
          borderColor: "#1382a9",
          borderWidth: 1,
          hoverBackgroundColor: "#20a8d8",
          hoverBorderColor: "#1382a9",
          data: [this.state && this.state.part_paid_rate]
        },
        {
          label: "Paid",
          backgroundColor: "#4dbd74ab",
          borderColor: "#28924d",
          borderWidth: 1,
          hoverBackgroundColor: "#4dbd74",
          hoverBorderColor: "#28924d",
          data: [this.state && this.state.paid_rate]
        }
      ]
    };
    // const pie1 = {
    //   labels: ["Approved", "Pending"],
    //   datasets: [
    //     {
    //       data: [
    //         this.state.agenydashboard && this.state.agenydashboard.timesheetapproved ? this.state.agenydashboard.timesheetapproved : "0",
    //         this.state.agenydashboard && this.state.agenydashboard.timesheetpending ? this.state.agenydashboard.timesheetpending : "0"
    //       ],
    //       backgroundColor: ["#4dbd74", "#f8cb00"],
    //       hoverBackgroundColor: ["#4dbd74", "#f8cb00"]
    //     }
    //   ]
    // };
    /* const bar2 = {
      labels: ["Timesheets"],
      datasets: [
        {
          label: "Approved",
          backgroundColor: "#63c2dead",
          borderColor: "#2fa0c1",
          borderWidth: 1,
          hoverBackgroundColor: "#63c2de",
          hoverBorderColor: "#2fa0c1",
          data: [this.state.agenydashboard && this.state.agenydashboard.timesheetapproved ? this.state.agenydashboard.timesheetapproved : "0"]
        },
        {
          label: "Pending",
          backgroundColor: "#f8cb00b0",
          borderColor: "#bb9a04",
          borderWidth: 1,
          hoverBackgroundColor: "#f8cb00",
          hoverBorderColor: "#bb9a04",
          data: [this.state.agenydashboard && this.state.agenydashboard.timesheetpending ? this.state.agenydashboard.timesheetpending : "0"]
        }
      ]
    };*/
    return (
      <div className="animated fadeIn all-dashboard dashboard-str">
        {/* <ToastContainer position="top-right" autoClose={2500} />*/}
        <Row>
          <Col md="6" xs="12">
		  
            <Card className="chart-dashboards">
              <CardHeader>
                <i className="fa fa-bar-chart" /> Shifts
                <div className="card-actions">
                  <button
                    onClick={() => {
                      this.props.history.push("/agency/editshift");
                    }}
                  >
                    <i className="fa fa-plus" /> New Shift
                    <small className="text-muted" />
                  </button>
                  <button
                    onClick={() => {
                      this.props.history.push("/agency/shiftlist");
                    }}
                  >
                    <i className="fa fa-th-list" /> View Shifts
                    <small className="text-muted" />
                  </button>
                </div>
              </CardHeader>
              <CardBody className="p-2 dashboard-ranges graph-cus">
                <Row className="d-flex justify-content-end">
                  <Col md="4" xs="12">
                    <Input type="select" name="selectshift" value={this.state.selectshift} onChange={this.onChange}>
                      <option value="all">All</option>
                      <option value="1">Today</option>
                      <option value="8">Last 7 Days</option>
                      <option value="31">Last 30 Days</option>
                      <option value="366">Last 12 Months</option>
                    </Input>
                  </Col>
                </Row>
                <div className="chart-wrapper">
                  <Bar
                    data={bar}
                    options={{
                      maintainAspectRatio: false
                    }}
                  />
                </div>
              </CardBody>
            </Card>

          </Col>
		  
		  
          <Col md="6" xs="12">
            <Card>
              <CardHeader>
                <i className="fa fa-area-chart" /> Invoices
                <div className="card-actions">
                  <button
                    onClick={() => {
                      this.props.history.push("/agency/invoiceclientlist");
                    }}
                  >
                    <i className="fa fa-th-list" /> View Invoices
                    <small className="text-muted" />
                  </button>
                </div>
              </CardHeader>
              <CardBody className="p-2 graph-cus dashboard-ranges">
                <Row className="d-flex justify-content-end">
                  <Col md="4" xs="12">
                    <Input type="select" name="selectinvoice" value={this.state.selectinvoice} onChange={this.onChange}>
                      <option value="all">All</option>
                      <option value="1">Today</option>
                      <option value="8">Last 7 Days</option>
                      <option value="31">Last 30 Days</option>
                      <option value="366">Last 12 Months</option>
                    </Input>
                  </Col>
                </Row>
				
				
				
                <Row>
                  <Col md="12" xs="12" className="draft-details">
                    <p>
                      <strong>Draft : </strong> {this.state.draft ? <Fragment> {this.state.draft}</Fragment> : <Fragment> 0</Fragment>}{" "}
                    </p>
                   {/* <p>
                      <strong>Draft Rate : </strong> {this.state.draft_rate ? <Fragment>£ {this.state.draft_rate}</Fragment> : <Fragment>£ 0</Fragment>}{" "}
                    </p>*/}
                    {/*<p>
                      <strong>Awaiting Payment : </strong> {this.state.approved ? <Fragment> {this.state.approved}</Fragment> : <Fragment> 0</Fragment>}{" "}
                    </p>*/}
                    <p>
                      <strong>Awaiting Payment Rate : </strong> {this.state.approved_rate ? <Fragment>£ {this.state.approved_rate}</Fragment> : <Fragment>£ 0</Fragment>}{" "}
                    </p>
                   {/* <p>
                      <strong>Part Paid : </strong> {this.state.part_paid ? <Fragment> {this.state.part_paid}</Fragment> : <Fragment> 0</Fragment>}{" "}
                    </p>
                    <p>
                      <strong>Part Paid Rate : </strong> {this.state.part_paid_rate ? <Fragment>£ {this.state.part_paid_rate}</Fragment> : <Fragment>£ 0</Fragment>}{" "}
                    </p>
                    <p>
                      <strong>Paid : </strong> {this.state.paid ? <Fragment> {this.state.paid}</Fragment> : <Fragment> 0</Fragment>}{" "}
                    </p>
                    <p>
                      <strong>Paid Rate : </strong> {this.state.paid_rate ? <Fragment>£ {this.state.paid_rate}</Fragment> : <Fragment>£ 0</Fragment>}{" "}
                    </p>*/}
                  </Col>
                </Row>
                <div className="chart-wrapper">
                  <Bar
                    data={bar1}
                    options={{
                      maintainAspectRatio: false
                    }}
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
		
		<Row>
		   <Col md="6" xs="12">
              <Card className="sub-ranges">
                <CardHeader>
                  <i className="fa fa-pie-chart" /> Clients
                  <div className="card-actions">
                    <button
                      onClick={() => {
                        this.props.history.push("/agency/clientlist");
                      }}
                    >
                      <i className="fa fa-th-list" /> View Clients
                      <small className="text-muted" />
                    </button>
                  </div>
                </CardHeader>
                <div className="dash-timesheet">
                  <Col xs="12" md="4" className="pr-1 rounded-0">
                    <Widget02
                      onClick={() => {
                        this.props.history.push("/agency/clientlist");
                      }}
                      header={this.state.agenydashboard.clients ? this.state.agenydashboard.clients : "0"}
                      mainText="Total"
                      icon="fa fa-users"
                      color="primary"
                      variant="1"
                      className="cursor-pointer"
                    />
                  </Col>
                  <Col xs="12" md="4" className="pr-1 pl-1 rounded-0">
                    <Widget02
                      onClick={() => {
                        this.props.history.push("/agency/clientlist");
                      }}
                      header={this.state.agenydashboard.activeclient ? this.state.agenydashboard.activeclient : "0"}
                      mainText="Active"
                      icon="fa fa-user-plus"
                      color="success"
                      variant="1"
                      className="cursor-pointer"
                    />
                  </Col>
                  <Col xs="12" md="4" className="pl-1 rounded-0">
                    <Widget02
                      onClick={() => {
                        this.props.history.push("/agency/clientlist");
                      }}
                      header={this.state.agenydashboard.inactiveclient ? this.state.agenydashboard.inactiveclient : "0"}
                      mainText="In-Active"
                      icon="fa fa-user-times"
                      color="danger"
                      variant="1"
                      className="cursor-pointer"
                    />
                  </Col>
                </div>
                {/* <CardBody className="p-0">
                <div className="chart-wrapper">
                  <Doughnut data={doughnut} />
                </div>
              </CardBody>*/}
              </Card>
            </Col>
			
			<Col md="6" xs="12">
              <Card className="sub-ranges">
                <CardHeader>
                  <i className="fa fa-pie-chart" /> Employees
                  <div className="card-actions">
                    <button
                      onClick={() => {
                        this.props.history.push("/agency/employeelist");
                      }}
                    >
                      <i className="fa fa-th-list" /> View Employees
                      <small className="text-muted" />
                    </button>
                  </div>
                </CardHeader>
                <div className="dash-timesheet">
                  <Col xs="12" md="4" className="pr-1 rounded-0">
                    <Widget02
                      onClick={() => {
                        this.props.history.push("/agency/employeelist");
                      }}
                      header={this.state.agenydashboard.employees ? this.state.agenydashboard.employees : "0"}
                      mainText="Total"
                      icon="fa fa-users"
                      color="info"
                      variant="1"
                      className="cursor-pointer"
                    />
                  </Col>
                  <Col xs="12" md="4" className="pr-1 pl-1 rounded-0">
                    <Widget02
                      onClick={() => {
                        this.props.history.push("/agency/employeelist");
                      }}
                      header={this.state.agenydashboard.activeemployee ? this.state.agenydashboard.activeemployee : "0"}
                      mainText="Active"
                      icon="fa fa-user-plus"
                      color="success"
                      variant="1"
                      className="cursor-pointer"
                    />
                  </Col>
                  <Col xs="12" md="4" className="pl-1 rounded-0">
                    <Widget02
                      onClick={() => {
                        this.props.history.push("/agency/employeelist");
                      }}
                      header={this.state.agenydashboard.inactiveemployee ? this.state.agenydashboard.inactiveemployee : "0"}
                      mainText="In-Active"
                      icon="fa fa-user-times"
                      color="danger"
                      variant="1"
                      className="cursor-pointer"
                    />
                  </Col>
                </div>
                {/* <CardBody className="p-0">
                <div className="chart-wrapper">
                  <Pie data={pie} />
                </div>
              </CardBody>*/}
              </Card>
            </Col>
		
		</Row>
		
		<Row>
		  <Col xs="12" md="12">
		   
            <Card className="sub-ranges">
              <CardHeader>
                <i className="fa fa-pie-chart" /> Timesheets
                <div className="card-actions">
                  <button
                    onClick={() => {
                      this.props.history.push("/agency/timesheetemployee");
                    }}
                  >
                    <i className="fa fa-th-list" /> View Timesheets
                    <small className="text-muted" />
                  </button>
                </div>
              </CardHeader>
			  
              <div className="dash-timesheet">
			  
                <Col xs="12" md="4" className="pr-2 rounded-0">
                  <Widget02
                    onClick={() => {
                      this.props.history.push("/agency/timesheetemployee");
                    }}
                    header={this.state.agenydashboard ? this.state.agenydashboard.timesheetapproved + this.state.agenydashboard.timesheetpending : "0"}
                    mainText="Total"
                    icon="fa fa-users"
                    color="warning"
                    variant="1"
                    className="cursor-pointer"
                  />
                </Col>
                <Col xs="12" md="4" className="pr-2 rounded-0">
                  <Widget02
                    onClick={() => {
                      this.props.history.push("/agency/timesheetemployee");
                    }}
                    header={this.state.agenydashboard.timesheetapproved ? this.state.agenydashboard.timesheetapproved : "0"}
                    mainText="Approved"
                    icon="fa fa-user-plus"
                    color="success"
                    variant="1"
                    className="cursor-pointer"
                  />
                </Col>
                <Col xs="12" md="4" className="pl-2 rounded-0">
                  <Widget02
                    onClick={() => {
                      this.props.history.push("/agency/timesheetemployee");
                    }}
                    header={this.state.agenydashboard.timesheetpending ? this.state.agenydashboard.timesheetpending : "0"}
                    mainText="Pending"
                    icon="fa fa-user-times"
                    color="warning"
                    variant="1"
                    className="cursor-pointer"
                  />
                </Col>

              </div>
            </Card>		  
		  </Col>        
		</Row>
		
        <br />
        <Calenterview />
        <Row>
          <Col xs="12" md="6">
            <Recentclientlist />
          </Col>
          <Col xs="12" md="6">
            <Recentemployeelist />
          </Col>
        </Row>
        <GodView />
      </div>
    );
  }
}

export default Agencydashboard;
