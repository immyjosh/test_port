/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, Input, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import moment from "moment";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";

class Editdaysoff extends Component {
  state = {
    name: "",
    numberofday: "",
    dates: [],
    reason: "",
    status: "",
    daysofflist: "",
    stausactive: false,
    dates1: "",
    selectedDays: []
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  onChangecolor = e => {
    this.setState({
      [e.target.name]: e.target.value,
      colorerror: false
    });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/agency/daysoff/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      this.setState({ daysofflist: res.response.result[0] });
      this.setState({
        name: this.state.daysofflist.name,
        numberofday: this.state.daysofflist.numberofday,
        reason: this.state.daysofflist.reason,
        status: this.state.daysofflist.status
      });
      if (this.state.daysofflist.dates) {
        const date = this.state.daysofflist.dates.map((date, key) => {
          return moment(date)._d;
        });
        this.setState({ dates: date });
      }
      if (this.state.daysofflist.status === 1) {
        this.setState({
          stausactive: true
        });
      } else {
        this.setState({
          stausactive: false
        });
      }
    });
  }

  OnFormSubmit = (e, values) => {
    if (this.state.numberofday.toString() !== this.state.dates.length.toString()) {
      toast.error("No. Of Days Must be equal to Selected Dates");
    } else {
      request({
        url: "/agency/daysoff/save",
        method: "POST",
        data: {
          _id: this.props.location.state.rowid,
          name: this.state.name,
          numberofday: this.state.numberofday,
          dates: this.state.dates,
          reason: this.state.reason,
          status: this.state.status
        }
      }).then(res => {
        if (res.status === 1) {
          this.editnodify();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
    }
  };
  editnodifytoastId = "Editdaysoff";
  editnodify() {
    if (!toast.isActive(this.editnodifytoastId)) {
      this.editnodifytoastId = toast.warn("Updated");
    }
    this.componentDidMount();
  }
  handleDayClick = (day, { selected }) => {
    if (selected) {
      const selectedIndex = this.state.dates.findIndex(selectedDay => DateUtils.isSameDay(selectedDay, day));
      this.state.dates.splice(selectedIndex, 1);
    } else {
      this.state.dates.push(day);
    }
    this.setState({ dates: this.state.dates });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Add Work Location
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
				 <Row>
				   <Col xs="12" md="12">
				 <div className="cus-design edited-section">
				  <Col xs="12" md="12">
                  <p className="h5">Daysoff Details</p>
                   </Col>
                  <Col xs="12" md="8">
				   <Row>
				   <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Name</Label>
                      <AvInput type="text" name="name" placeholder="Enter username" value={this.state.name} onChange={this.onChange} required autoComplete="name" />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
					</Col>
					 <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Number of day</Label>
                      <AvInput
                        type="number"
                        name="numberofday"
                        placeholder="Enter number of day"
                        value={this.state.numberofday}
                        onChange={this.onChange}
                        required
                        autoComplete="numberofday"
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>

                  <Col xs="12" md="12">
                    <AvGroup>
                      <Label>Reason</Label>
                      <AvInput type="textarea" name="reason" placeholder="Enter reason" value={this.state.reason} onChange={this.onChange} required autoComplete="reason" />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>

                  <Col xs="12" md="12">
                   
                      <label>Status</label>
                    <Label className="switch switch-text switch-success new-switch">
                      <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                      <span className="switch-label" data-on="active" data-off="inactive" />
                      <span className="switch-handle new-handle" />
                    </Label>
                  </Col>
				  
					</Row>
                  </Col>
                 
				    <Col xs="12" md="4">
                    <Label>Dates</Label>
                    <DayPicker initialMonth={new Date(moment(this.state.dates[0]).format("YYYY,MM"))} selectedDays={this.state.dates} onDayClick={this.handleDayClick} />
                  </Col>
				  
				  </div>
				  </Col>
				  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i className="fa fa-dot-circle-o" />
                    Update
                  </Button>
                  <Link to="/agency/listpublicholidays">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editdaysoff;
