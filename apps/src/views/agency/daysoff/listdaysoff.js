import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import moment from "moment";
import request from "../../../api/api";
import Loader from "../../common/loader";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "140px"
};

class Listdaysoff extends Component {
  state = {
    adminredirect: false,
    sortOrder: true,
    isLoader: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    count: 0,
    pages: "",
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0
    },
    daysofflist: []
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    request({
      url: "/agency/daysoff/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status ===1) {
        this.setState({
          isLoader: false,
          daysofflist: res.response.result,
          pages: res.response.fullCount,
          currPage: res.response.length
        });
      } else if(res.status === 0){
        toast.error(res.response)
      }
    });
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/editpublicholidays",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/agency/daysoff/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (!res.response.result && !res.response.fullcount) {
        this.setState({ daysofflist: [], pages: "" });
      } else {
        this.setState({
          daysofflist: res.response.result,
          pages: res.response.fullCount,
          currPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["numberofday", "reason", "name", "createdAt","status","dates"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }

  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Number of day") {
        state.tableOptions.filter = "numberofday";
      } else if (value === "Reason") {
        state.tableOptions.filter = "reason";
      } else if (value === "Dates") {
        state.tableOptions.filter = "dates";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/daysoff/daysoffexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  nodifystoastId = "Editlist";
  nodify() {
    if (!toast.isActive(this.nodifystoastId)) {
      this.nodifystoastId = toast.warn("Updated");
    }
  }
  adminsavenodifystoastId = "Editlist";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifystoastId)) {
      this.nodiadminsavenodifystoastIdfystoastId = toast.success("New Dayoff Added!");
    }
  }
  render() {
    let order = this.state.sortOrder;
    let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    let sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            List of Days Off
            <div className="card-actions" style={tStyle}>
              <button style={tStyles} onClick={()=>this.props.history.push("/agency/addpublicholidays")}>
                <i className="fa fa-plus" /> Add New
              </button>
              <button style={tStyles} onClick={this.export}>
                <i className="fa fa-upload" /> Export
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-end">
              <div className="col-lg-auto pr-0">
                <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                  <option>All</option>
                  <option>Name</option>
                  <option>Reason</option>
                  <option>Dates</option>
                </Input>
              </div>
              <div className="col-lg-3 pl-0">
                <InputGroup>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Reason Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("numberofday");
                      }}
                    >
                      Number of day <i style={{ paddingLeft: "25px" }} className={sorticondef} id="numberofday" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("reason");
                      }}
                    >
                      Reason <i style={{ paddingLeft: "25px" }} className={sorticondef} id="reason" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("dates");
                      }}
                    >
                      Dates <i style={{ paddingLeft: "25px" }} className={sorticondef} id="dates" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("createdAt");
                      }}
                    >
                      Created Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                    </th>
                    <th   onClick={() => {
                        this.sort("status");
                      }}>

                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.daysofflist.length > 0 ? (
                    this.state.daysofflist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.numberofday}</td>
                        <td>{item.reason}</td>
                        <td>
                          {item.dates.map((e, key) => {
                            return moment(e).format("DD-MM-YYYY") + ",   ";
                          })}
                        </td>
                        <td>{moment(item.createdAt).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                        </td>
                        <td>
                          <button type="button" title="Edit" className="btn table-edit" id={`edit${i}`} onClick={id => this.editpage(item._id)}>
                            <i className="fa fa-edit" />
                          </button>
                          <UncontrolledTooltip placement="top" target={`edit${i}`}>
                            Edit
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={13}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Listdaysoff;
