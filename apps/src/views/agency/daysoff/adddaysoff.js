/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import savenodify from "./listdaysoff";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";

class Adddaysoff extends Component {
  state = {
    name: "",
    numberofday: "",
    reason: "",
    status: "",
    dates: [],
    dateerror: false,
    daysoffdates: []
  };
  flash = new savenodify();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/agency/daysoff/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        let dates = res.response.result && res.response.result.map(list => list.dates).flat();
        this.setState({
          daysoffdates: dates
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  handleDayClick = (day, { selected, highlighted }) => {
    if (selected) {
      const selectedIndex = this.state.dates.findIndex(selectedDay => DateUtils.isSameDay(selectedDay, day));
      this.state.dates.splice(selectedIndex, 1);
    } else if (highlighted) {
      toast.error("Already Added");
    } else {
      this.state.dates.push(day);
    }
    this.setState({ dates: this.state.dates });
  };
  datefield = value => {
    if (value === "") {
      this.setState({
        dateerror: true
      });
    }
  };
  OnFormSubmit = (e, values) => {
    e.persist();
    if (this.state.numberofday.toString() !== this.state.dates.length.toString()) {
      toast.error("No. Of Days Must be equal to Selected Dates");
    } else {
      request({
        url: "/agency/daysoff/save",
        method: "POST",
        data: {
          name: this.state.name,
          numberofday: this.state.numberofday,
          dates: this.state.dates,
          reason: this.state.reason,
          status: 1
        }
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
      this.form && this.form.reset();
    }
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangecolor = e => {
    this.setState({
      [e.target.name]: e.target.value,
      colorerror: false
    });
  };
  saveuser = () => {
    return this.props.history.push("/agency/listpublicholidays"), this.flash.adminsavenodify();
  };

  render() {
    let highl = this.state.daysoffdates && this.state.daysoffdates.map(list => new Date(list));
    const modifiers = {
      highlighted: highl
    };
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Add Daysoff
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
                  
                 <Row>
				  <Col xs="12" md="12">
				 <div className="cus-design edited-section">
				 <Col xs="12" md="12">
				 <p className="h5">Daysoff Details</p>
				 </Col>
                  <Col xs="12" md="8">
				   <Row>
				    <Col xs="12" md="6">
                    <AvGroup>
                      <Label>Title</Label>
                      <AvInput
                        type="text"
                        name="name"
                        placeholder="Enter Title"
                        value={this.state.name}
                        onChange={this.onChange}
                        required
                        autoComplete="name"
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
					</Col>
					<Col xs="12" md="6">
                    <AvGroup>
                      <Label>Number of days</Label>
                      <AvInput
                        type="number"
                        name="numberofday"
                        placeholder="Enter Number of Days"
                        value={this.state.numberofday}
                        onChange={this.onChange}
                        required
                        autoComplete="numberofday"
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
				  <Col xs="12" md="12">
                    <AvGroup>
                      <Label>Reason</Label>
                      <AvInput
                        type="textarea"
                        name="reason"
                        placeholder="Enter Reason"
                        value={this.state.reason}
                        onChange={this.onChange}
                        required
                        autoComplete="reason"
                      />
                      <AvFeedback>This is required!</AvFeedback>
                    </AvGroup>
                  </Col>
				  </Row>	
                  </Col>
                  
                  
                  <Col xs="12" md="4">
                    <Label>Dates</Label>
                    <DayPicker selectedDays={this.state.dates} disabledDays={{ before: new Date() }} modifiers={modifiers} onDayClick={this.handleDayClick} />
                    {this.state.dateerror ? <div style={{ color: "red" }}> Enter Valid Date!</div> : null}
                  </Col>
				  </div>
				  </Col>
				  </Row>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    title="Add Public Holidays"
                    onClick={() => {
                      this.datefield(this.state.dates);
                    }}
                  >
                    Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Adddaysoff;
