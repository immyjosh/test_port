/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import request from "../../../api/api";
import savenodify from "./listjobtype";
import { breaklist, timelist } from "../../common/utils";
import Select from "react-select";
import "react-select/dist/react-select.css";
import classnames from "classnames";

class Addjobtype extends Component {
  state = {
    name: "",
    client: "",
    clientlist: [],
    mon_day_from: "",
    mon_day_to: "",
    mon_day_client_rate: "",
    mon_day_employee_rate: "",
    tue_day_from: "",
    tue_day_to: "",
    tue_day_client_rate: "",
    tue_day_employee_rate: "",
    wed_day_from: "",
    wed_day_to: "",
    wed_day_client_rate: "",
    wed_day_employee_rate: "",
    thu_day_from: "",
    thu_day_to: "",
    thu_day_client_rate: "",
    thu_day_employee_rate: "",
    fri_day_from: "",
    fri_day_to: "",
    fri_day_client_rate: "",
    fri_day_employee_rate: "",
    sat_day_from: "",
    sat_day_to: "",
    sat_day_client_rate: "",
    sat_day_employee_rate: "",
    sun_day_from: "",
    sun_day_to: "",
    sun_day_client_rate: "",
    sun_day_employee_rate: "",
    public_day_from: "",
    public_day_to: "",
    public_day_client_rate: "",
    public_day_employee_rate: "",
    mon_night_from: "",
    mon_night_to: "",
    mon_night_client_rate: "",
    mon_night_employee_rate: "",
    tue_night_from: "",
    tue_night_to: "",
    tue_night_client_rate: "",
    tue_night_employee_rate: "",
    wed_night_from: "",
    wed_night_to: "",
    wed_night_client_rate: "",
    wed_night_employee_rate: "",
    thu_night_from: "",
    thu_night_to: "",
    thu_night_client_rate: "",
    thu_night_employee_rate: "",
    fri_night_from: "",
    fri_night_to: "",
    fri_night_client_rate: "",
    fri_night_employee_rate: "",
    sat_night_from: "",
    sat_night_to: "",
    sat_night_client_rate: "",
    sat_night_employee_rate: "",
    sun_night_from: "",
    sun_night_to: "",
    sun_night_client_rate: "",
    sun_night_employee_rate: "",
    public_night_from: "",
    public_night_to: "",
    public_night_client_rate: "",
    public_night_employee_rate: "",
    mon_day_from_error: false,
    mon_day_to_error: false,
    tue_day_from_error: false,
    tue_day_to_error: false,
    wed_day_from_error: false,
    wed_day_to_error: false,
    thu_day_from_error: false,
    thu_day_to_error: false,
    fri_day_from_error: false,
    fri_day_to_error: false,
    sat_day_from_error: false,
    sat_day_to_error: false,
    sun_day_from_error: false,
    sun_day_to_error: false,
    public_day_from_error: false,
    public_day_to_error: false,
    mon_night_from_error: false,
    mon_night_to_error: false,
    tue_night_from_error: false,
    tue_night_to_error: false,
    wed_night_from_error: false,
    wed_night_to_error: false,
    thu_night_from_error: false,
    thu_night_to_error: false,
    fri_night_from_error: false,
    fri_night_to_error: false,
    sat_night_from_error: false,
    sat_night_to_error: false,
    sun_night_from_error: false,
    sun_night_to_error: false,
    public_night_from_error: false,
    public_night_to_error: false,
    late_fee_duration_error: false,
    client_error: false,
    late_fee_amount: "",
    late_fee_duration: "",
    activeTab: "1"
  };
  flash = new savenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ timelist, breaklist });
  }
  OnFormSubmit = (e, values) => {
    if (this.state.mon_day_from === "") {
      toast.error("Please Select Monday (Day) Start Time");
    } else if (this.state.mon_day_to === "") {
      toast.error("Please Select Monday (Day) End Time");
    } else if (this.state.tue_day_from === "") {
      toast.error("Please Select Tuesday (Day) Start Time");
    } else if (this.state.tue_day_to === "") {
      toast.error("Please Select Tuesday (Day) End Time");
    } else if (this.state.wed_day_from === "") {
      toast.error("Please Select Wednesday (Day) Start Time");
    } else if (this.state.wed_day_to === "") {
      toast.error("Please Select Wednesday (Day) End Time");
    } else if (this.state.thu_day_from === "") {
      toast.error("Please Select Thursday (Day) Start Time");
    } else if (this.state.thu_day_to === "") {
      toast.error("Please Select Thursday (Day) End Time");
    } else if (this.state.fri_day_from === "") {
      toast.error("Please Select Friday (Day) Start Time");
    } else if (this.state.fri_day_to === "") {
      toast.error("Please Select Friday (Day) End Time");
    } else if (this.state.sat_day_from === "") {
      toast.error("Please Select Saturday (Day) Start Time");
    } else if (this.state.sat_day_to === "") {
      toast.error("Please Select Saturday (Day) End Time");
    } else if (this.state.sun_day_from === "") {
      toast.error("Please Select Sunday (Day) Start Time");
    } else if (this.state.sun_day_to === "") {
      toast.error("Please Select Sunday (Day) End Time");
    } else if (this.state.public_day_from === "") {
      toast.error("Please Select Public Holiday (Day) Start Time");
    } else if (this.state.public_day_to === "") {
      toast.error("Please Select Public Holiday (Day) End Time");
    } else if (this.state.mon_night_from === "") {
      toast.error("Please Select Monday (Night) Start Time");
    } else if (this.state.mon_night_to === "") {
      toast.error("Please Select Monday (Night) End Time");
    } else if (this.state.tue_night_from === "") {
      toast.error("Please Select Tuesday (Night) Start Time");
    } else if (this.state.tue_night_to === "") {
      toast.error("Please Select Tuesday (Night) End Time");
    } else if (this.state.wed_night_from === "") {
      toast.error("Please Select Wednesday (Night) Start Time");
    } else if (this.state.wed_night_to === "") {
      toast.error("Please Select Wednesday (Night) End Time");
    } else if (this.state.thu_night_from === "") {
      toast.error("Please Select Thursday (Night) Start Time");
    } else if (this.state.thu_night_to === "") {
      toast.error("Please Select Thursday (Night) End Time");
    } else if (this.state.fri_night_from === "") {
      toast.error("Please Select Friday (Night) Start Time");
    } else if (this.state.fri_night_to === "") {
      toast.error("Please Select Friday (Night) End Time");
    } else if (this.state.sat_night_from === "") {
      toast.error("Please Select Saturday (Night) Start Time");
    } else if (this.state.sat_night_to === "") {
      toast.error("Please Select Saturday (Night) End Time");
    } else if (this.state.sun_night_from === "") {
      toast.error("Please Select Sunday (Night) Start Time");
    } else if (this.state.sun_night_to === "") {
      toast.error("Please Select Sunday (Night) End Time");
    } else if (this.state.public_night_from === "") {
      toast.error("Please Select Public Holiday (Night) Start Time");
    } else if (this.state.public_night_to === "") {
      toast.error("Please Select Public Holiday (Night) End Time");
    } else {
      request({
        url: "/agency/jobtype/save",
        method: "POST",
        data: {
          name: this.state.name,
          rate_details: [
            {
              name: "mon_day",
              from: this.state.mon_day_from,
              to: this.state.mon_day_to,
              client_rate: this.state.mon_day_client_rate,
              employee_rate: this.state.mon_day_employee_rate
            },
            {
              name: "tue_day",
              from: this.state.tue_day_from,
              to: this.state.tue_day_to,
              client_rate: this.state.tue_day_client_rate,
              employee_rate: this.state.tue_day_employee_rate
            },
            {
              name: "wed_day",
              from: this.state.wed_day_from,
              to: this.state.wed_day_to,
              client_rate: this.state.wed_day_client_rate,
              employee_rate: this.state.wed_day_employee_rate
            },
            {
              name: "thu_day",
              from: this.state.thu_day_from,
              to: this.state.thu_day_to,
              client_rate: this.state.thu_day_client_rate,
              employee_rate: this.state.thu_day_employee_rate
            },
            {
              name: "fri_day",
              from: this.state.fri_day_from,
              to: this.state.fri_day_to,
              client_rate: this.state.fri_day_client_rate,
              employee_rate: this.state.fri_day_employee_rate
            },
            {
              name: "sat_day",
              from: this.state.sat_day_from,
              to: this.state.sat_day_to,
              client_rate: this.state.sat_day_client_rate,
              employee_rate: this.state.sat_day_employee_rate
            },
            {
              name: "sun_day",
              from: this.state.sun_day_from,
              to: this.state.sun_day_to,
              client_rate: this.state.sun_day_client_rate,
              employee_rate: this.state.sun_day_employee_rate
            },
            {
              name: "public_holiday_day",
              from: this.state.public_day_from,
              to: this.state.public_day_to,
              client_rate: this.state.public_day_client_rate,
              employee_rate: this.state.public_day_employee_rate
            },
            {
              name: "mon_night",
              from: this.state.mon_night_from,
              to: this.state.mon_night_to,
              client_rate: this.state.mon_night_client_rate,
              employee_rate: this.state.mon_night_employee_rate
            },
            {
              name: "tue_night",
              from: this.state.tue_night_from,
              to: this.state.tue_night_to,
              client_rate: this.state.tue_night_client_rate,
              employee_rate: this.state.tue_night_employee_rate
            },
            {
              name: "wed_night",
              from: this.state.wed_night_from,
              to: this.state.wed_night_to,
              client_rate: this.state.wed_night_client_rate,
              employee_rate: this.state.wed_night_employee_rate
            },
            {
              name: "thu_night",
              from: this.state.thu_night_from,
              to: this.state.thu_night_to,
              client_rate: this.state.thu_night_client_rate,
              employee_rate: this.state.thu_night_employee_rate
            },
            {
              name: "fri_night",
              from: this.state.fri_night_from,
              to: this.state.fri_night_to,
              client_rate: this.state.fri_night_client_rate,
              employee_rate: this.state.fri_night_employee_rate
            },
            {
              name: "sat_night",
              from: this.state.sat_night_from,
              to: this.state.sat_night_to,
              client_rate: this.state.sat_night_client_rate,
              employee_rate: this.state.sat_night_employee_rate
            },
            {
              name: "sun_night",
              from: this.state.sun_night_from,
              to: this.state.sun_night_to,
              client_rate: this.state.sun_night_client_rate,
              employee_rate: this.state.sun_night_employee_rate
            },
            {
              name: "public_holiday_night",
              from: this.state.public_night_from,
              to: this.state.public_night_to,
              client_rate: this.state.public_night_client_rate,
              employee_rate: this.state.public_night_employee_rate
            }
          ],
          late_fee: {
            amount: this.state.late_fee_amount,
            duration: this.state.late_fee_duration
          },
          client: this.state.client,
          status: 1
        }
      })
        .then(res => {
          if (res.status === 1) {
            this.saveuser();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.error(error);
        });
    }
  };
  saveuser = () => {
    return this.form && this.form.reset(), this.props.history.push("/agency/listjobtype"), this.flash.adminsavenodify();
  };
  selectChange = value => {
    if (value) {
      switch (value.target.name) {
        case "mon_day_from":
          this.setState({
            mon_day_from: value.target.value ? value.target.value.value : "",
            mon_day_from_error: value.target.value ? false : true
          });
          break;
        case "tue_day_from":
          this.setState({
            tue_day_from: value.target.value ? value.target.value.value : "",
            tue_day_from_error: value.target.value ? false : true
          });
          break;
        case "wed_day_from":
          this.setState({ wed_day_from: value.target.value ? value.target.value.value : "", wed_day_from_error: value.target.value ? false : true });
          break;
        case "thu_day_from":
          this.setState({ thu_day_from: value.target.value ? value.target.value.value : "", thu_day_from_error: value.target.value ? false : true });
          break;
        case "fri_day_from":
          this.setState({ fri_day_from: value.target.value ? value.target.value.value : "", fri_day_from_error: value.target.value ? false : true });
          break;
        case "sat_day_from":
          this.setState({ sat_day_from: value.target.value ? value.target.value.value : "", sat_day_from_error: value.target.value ? false : true });
          break;
        case "sun_day_from":
          this.setState({ sun_day_from: value.target.value ? value.target.value.value : "", sun_day_from_error: value.target.value ? false : true });
          break;
        case "public_day_from":
          this.setState({ public_day_from: value.target.value ? value.target.value.value : "", public_day_from_error: value.target.value ? false : true });
          break;
        case "mon_day_to":
          this.setState({ mon_day_to: value.target.value ? value.target.value.value : "", mon_day_to_error: value.target.value ? false : true });
          break;
        case "tue_day_to":
          this.setState({ tue_day_to: value.target.value ? value.target.value.value : "", tue_day_to_error: value.target.value ? false : true });
          break;
        case "wed_day_to":
          this.setState({ wed_day_to: value.target.value ? value.target.value.value : "", wed_day_to_error: value.target.value ? false : true });
          break;
        case "thu_day_to":
          this.setState({ thu_day_to: value.target.value ? value.target.value.value : "", thu_day_to_error: value.target.value ? false : true });
          break;
        case "fri_day_to":
          this.setState({ fri_day_to: value.target.value ? value.target.value.value : "", fri_day_to_error: value.target.value ? false : true });
          break;
        case "sat_day_to":
          this.setState({ sat_day_to: value.target.value ? value.target.value.value : "", sat_day_to_error: value.target.value ? false : true });
          break;
        case "sun_day_to":
          this.setState({ sun_day_to: value.target.value ? value.target.value.value : "", sun_day_to_error: value.target.value ? false : true });
          break;
        case "public_day_to":
          this.setState({ public_day_to: value.target.value ? value.target.value.value : "", public_day_to_error: value.target.value ? false : true });
          break;
        case "mon_night_from":
          this.setState({ mon_night_from: value.target.value ? value.target.value.value : "", mon_night_from_error: value.target.value ? false : true });
          break;
        case "tue_night_from":
          this.setState({ tue_night_from: value.target.value ? value.target.value.value : "", tue_night_from_error: value.target.value ? false : true });
          break;
        case "wed_night_from":
          this.setState({ wed_night_from: value.target.value ? value.target.value.value : "", wed_night_from_error: value.target.value ? false : true });
          break;
        case "thu_night_from":
          this.setState({ thu_night_from: value.target.value ? value.target.value.value : "", thu_night_from_error: value.target.value ? false : true });
          break;
        case "fri_night_from":
          this.setState({ fri_night_from: value.target.value ? value.target.value.value : "", fri_night_from_error: value.target.value ? false : true });
          break;
        case "sat_night_from":
          this.setState({ sat_night_from: value.target.value ? value.target.value.value : "", sat_night_from_error: value.target.value ? false : true });
          break;
        case "sun_night_from":
          this.setState({ sun_night_from: value.target.value ? value.target.value.value : "", sun_night_from_error: value.target.value ? false : true });
          break;
        case "public_night_from":
          this.setState({ public_night_from: value.target.value ? value.target.value.value : "", public_night_from_error: value.target.value ? false : true });
          break;
        case "mon_night_to":
          this.setState({ mon_night_to: value.target.value ? value.target.value.value : "", mon_night_to_error: value.target.value ? false : true });
          break;
        case "tue_night_to":
          this.setState({ tue_night_to: value.target.value ? value.target.value.value : "", tue_night_to_error: value.target.value ? false : true });
          break;
        case "wed_night_to":
          this.setState({ wed_night_to: value.target.value ? value.target.value.value : "", wed_night_to_error: value.target.value ? false : true });
          break;
        case "thu_night_to":
          this.setState({ thu_night_to: value.target.value ? value.target.value.value : "", thu_night_to_error: value.target.value ? false : true });
          break;
        case "fri_night_to":
          this.setState({ fri_night_to: value.target.value ? value.target.value.value : "", fri_night_to_error: value.target.value ? false : true });
          break;
        case "sat_night_to":
          this.setState({ sat_night_to: value.target.value ? value.target.value.value : "", sat_night_to_error: value.target.value ? false : true });
          break;
        case "sun_night_to":
          this.setState({ sun_night_to: value.target.value ? value.target.value.value : "", sun_night_to_error: value.target.value ? false : true });
          break;
        case "public_night_to":
          this.setState({ public_night_to: value.target.value ? value.target.value.value : "", public_night_to_error: value.target.value ? false : true });
          break;
        case "late_fee_duration":
          this.setState({ late_fee_duration: value.target.value ? value.target.value.value : "", late_fee_duration_error: value.target.value ? false : true });
          break;
        case "client":
          this.setState({ client: value.target.value ? value.target.value.value : "", client_error: value.target.value ? false : true });
          break;
        default:
          this.setState({ state: "" });
      }
    }
  };
  checkSelectError = () => {
    if (this.state.mon_day_from === "") {
      this.setState({ mon_day_from_error: true });
    } else {
      this.setState({ mon_day_from_error: false });
    }
    if (this.state.mon_day_to === "") {
      this.setState({ mon_day_to_error: true });
    } else {
      this.setState({ mon_day_to_error: false });
    }
    if (this.state.tue_day_from === "") {
      this.setState({ tue_day_from_error: true });
    }
    if (this.state.tue_day_to === "") {
      this.setState({ tue_day_to_error: true });
    }
    if (this.state.wed_day_from === "") {
      this.setState({ wed_day_from_error: true });
    }
    if (this.state.wed_day_to === "") {
      this.setState({ wed_day_to_error: true });
    }
    if (this.state.thu_day_from === "") {
      this.setState({ thu_day_from_error: true });
    }
    if (this.state.thu_day_to === "") {
      this.setState({ thu_day_to_error: true });
    }
    if (this.state.fri_day_from === "") {
      this.setState({ fri_day_from_error: true });
    }
    if (this.state.fri_day_to === "") {
      this.setState({ fri_day_to_error: true });
    }
    if (this.state.sat_day_from === "") {
      this.setState({ sat_day_from_error: true });
    }
    if (this.state.sat_day_to === "") {
      this.setState({ sat_day_to_error: true });
    }
    if (this.state.sun_day_from === "") {
      this.setState({ sun_day_from_error: true });
    }
    if (this.state.sun_day_to === "") {
      this.setState({ sun_day_to_error: true });
    }
    if (this.state.public_day_from === "") {
      this.setState({ public_day_from_error: true });
    }
    if (this.state.public_day_to === "") {
      this.setState({ public_day_to_error: true });
    }
    if (this.state.mon_night_from === "") {
      this.setState({ mon_night_from_error: true });
    }
    if (this.state.mon_night_to === "") {
      this.setState({ mon_night_to_error: true });
    }
    if (this.state.tue_night_from === "") {
      this.setState({ tue_night_from_error: true });
    }
    if (this.state.tue_night_to === "") {
      this.setState({ tue_night_to_error: true });
    }
    if (this.state.wed_night_from === "") {
      this.setState({ wed_night_from_error: true });
    }
    if (this.state.wed_night_to === "") {
      this.setState({ wed_night_to_error: true });
    }
    if (this.state.thu_night_from === "") {
      this.setState({ thu_night_from_error: true });
    }
    if (this.state.thu_night_to === "") {
      this.setState({ thu_night_to_error: true });
    }
    if (this.state.fri_night_from === "") {
      this.setState({ fri_night_from_error: true });
    }
    if (this.state.fri_night_to === "") {
      this.setState({ fri_night_to_error: true });
    }
    if (this.state.sat_night_from === "") {
      this.setState({ sat_night_from_error: true });
    }
    if (this.state.sat_night_to === "") {
      this.setState({ sat_night_to_error: true });
    }
    if (this.state.sun_night_from === "") {
      this.setState({ sun_night_from_error: true });
    }
    if (this.state.sun_night_to === "") {
      this.setState({ sun_night_to_error: true });
    }
    if (this.state.public_night_from === "") {
      this.setState({ public_night_from_error: true });
    }
    if (this.state.public_night_to === "") {
      this.setState({ public_night_to_error: true });
    }
    if (this.state.late_fee_duration === "") {
      // this.setState({ late_fee_duration_error: true });
    }
    if (this.state.client === "") {
      this.setState({ client_error: true });
    }
  };
  handleInvalidSubmit(event, errors, values) {
    toast.error("Please Fill Required Fields");
  }
  Applytoallmorning = () => {
    if (this.state.mon_day_from === "" && this.state.mon_day_to === "" && this.state.mon_day_client_rate === "" && this.state.mon_day_employee_rate === "") {
      toast.error("Please fill all fields in first row");
    } else {
      this.setState({
        tue_day_from: this.state.mon_day_from,
        wed_day_from: this.state.mon_day_from,
        thu_day_from: this.state.mon_day_from,
        fri_day_from: this.state.mon_day_from,
        sat_day_from: this.state.mon_day_from,
        sun_day_from: this.state.mon_day_from,
        public_day_from: this.state.mon_day_from,
        tue_day_to: this.state.mon_day_to,
        wed_day_to: this.state.mon_day_to,
        thu_day_to: this.state.mon_day_to,
        fri_day_to: this.state.mon_day_to,
        sat_day_to: this.state.mon_day_to,
        sun_day_to: this.state.mon_day_to,
        public_day_to: this.state.mon_day_to,
        tue_day_client_rate: this.state.mon_day_client_rate,
        wed_day_client_rate: this.state.mon_day_client_rate,
        thu_day_client_rate: this.state.mon_day_client_rate,
        fri_day_client_rate: this.state.mon_day_client_rate,
        sat_day_client_rate: this.state.mon_day_client_rate,
        sun_day_client_rate: this.state.mon_day_client_rate,
        public_day_client_rate: this.state.mon_day_client_rate,
        tue_day_employee_rate: this.state.mon_day_employee_rate,
        wed_day_employee_rate: this.state.mon_day_employee_rate,
        thu_day_employee_rate: this.state.mon_day_employee_rate,
        fri_day_employee_rate: this.state.mon_day_employee_rate,
        sat_day_employee_rate: this.state.mon_day_employee_rate,
        sun_day_employee_rate: this.state.mon_day_employee_rate,
        public_day_employee_rate: this.state.mon_day_employee_rate,
      });
      toast.success("Applied")
    }
  };
  Applytoallnight = () => {
    if (this.state.mon_night_from === "" && this.state.mon_night_to === "" && this.state.mon_night_client_rate === "" && this.state.mon_night_employee_rate === "") {
      toast.error("Please fill all fields in first row");
    } else {
      this.setState({
        tue_night_from: this.state.mon_night_from,
        wed_night_from: this.state.mon_night_from,
        thu_night_from: this.state.mon_night_from,
        fri_night_from: this.state.mon_night_from,
        sat_night_from: this.state.mon_night_from,
        sun_night_from: this.state.mon_night_from,
        public_night_from: this.state.mon_night_from,
        tue_night_to: this.state.mon_night_to,
        wed_night_to: this.state.mon_night_to,
        thu_night_to: this.state.mon_night_to,
        fri_night_to: this.state.mon_night_to,
        sat_night_to: this.state.mon_night_to,
        sun_night_to: this.state.mon_night_to,
        public_night_to: this.state.mon_night_to,
        tue_night_client_rate: this.state.mon_night_client_rate,
        wed_night_client_rate: this.state.mon_night_client_rate,
        thu_night_client_rate: this.state.mon_night_client_rate,
        fri_night_client_rate: this.state.mon_night_client_rate,
        sat_night_client_rate: this.state.mon_night_client_rate,
        sun_night_client_rate: this.state.mon_night_client_rate,
        public_night_client_rate: this.state.mon_night_client_rate,
        tue_night_employee_rate: this.state.mon_night_employee_rate,
        wed_night_employee_rate: this.state.mon_night_employee_rate,
        thu_night_employee_rate: this.state.mon_night_employee_rate,
        fri_night_employee_rate: this.state.mon_night_employee_rate,
        sat_night_employee_rate: this.state.mon_night_employee_rate,
        sun_night_employee_rate: this.state.mon_night_employee_rate,
        public_night_employee_rate: this.state.mon_night_employee_rate,
      });
      toast.success("Applied")
    }
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Add Job Role
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
                <CardBody>

                  <Row>
				   <Col xs="12" md="12">
				    <div className="cus-design edited-section">
					<Col xs="12" md="12">
					 <p className="h5">
                    Job Details <small>(Shift / Rate Per Hour)</small>
                    </p>
				   </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Job Role / Grade</Label>
                        <AvInput
                          type="text"
                          name="name"
                          placeholder="Enter Job Role"
                          onChange={this.onChange}
                          value={this.state.name}
                          required
                          autoComplete="name"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
					<Col xs="12" md="6">
                    <strong className="late_fee">Late Booking Fee</strong>
					<Row>
                    <Col xs="12" md="6">
					
                      <AvGroup>
                        <Label>Amount (£)</Label>
                        <AvInput
                          min={"0"}
                          type="number"
                          name="late_fee_amount"
                          placeholder="Enter Amount (£)"
                          onChange={this.onChange}
                          value={this.state.late_fee_amount}
                          autoComplete="late_fee_amount"
                        />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="6">
                      {this.state.late_fee_duration_error ? (
                        <Label className="error-color"> Duration (In Minutes)</Label>
                      ) : (
                        <Label>Duration (In Minutes)</Label>
                      )}
                      <Select
                        className={this.state.late_fee_duration_error ? "custom_select_validation" : null}
                        name="late_fee_duration"
                        value={this.state.late_fee_duration}
                        options={this.state.breaklist}
                        onChange={val => {
                          this.selectChange({ target: { name: "late_fee_duration", value: val } });
                        }}
                      />
                      {this.state.late_fee_duration_error ? <div className="error-color"> This is required!</div> : null}
                    </Col>
					</Row>
					</Col>
					</div>
					</Col>
                  </Row>
                  <br />
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "1" })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Day Time
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === "2" })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Night Time
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Monday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.mon_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.mon_day_from_error ? "custom_select_validation" : null}
                            name="mon_day_from"
                            value={this.state.mon_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "mon_day_from", value: val } });
                            }}
                          />
                          {this.state.mon_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.mon_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.mon_day_to_error ? "custom_select_validation" : null}
                            name="mon_day_to"
                            value={this.state.mon_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "mon_day_to", value: val } });
                            }}
                          />
                          {this.state.mon_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="mon_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.mon_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="mon_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.mon_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <button type="button" className="btn btn-add ap-all" onClick={this.Applytoallmorning}>
                            Apply to all
                          </button>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Tuesday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.tue_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.tue_day_from_error ? "custom_select_validation" : null}
                            name="tue_day_from"
                            value={this.state.tue_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "tue_day_from", value: val } });
                            }}
                          />
                          {this.state.tue_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.tue_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.tue_day_to_error ? "custom_select_validation" : null}
                            name="tue_day_to"
                            value={this.state.tue_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "tue_day_to", value: val } });
                            }}
                          />
                          {this.state.tue_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="tue_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.tue_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="tue_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.tue_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Wednesday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.wed_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.wed_day_from_error ? "custom_select_validation" : null}
                            name="wed_day_from"
                            value={this.state.wed_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "wed_day_from", value: val } });
                            }}
                          />
                          {this.state.wed_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.wed_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.wed_day_to_error ? "custom_select_validation" : null}
                            name="wed_day_to"
                            value={this.state.wed_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "wed_day_to", value: val } });
                            }}
                          />
                          {this.state.wed_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="wed_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.wed_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="wed_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.wed_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Thursday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.thu_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.thu_day_from_error ? "custom_select_validation" : null}
                            name="thu_day_from"
                            value={this.state.thu_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "thu_day_from", value: val } });
                            }}
                          />
                          {this.state.thu_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.thu_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.thu_day_to_error ? "custom_select_validation" : null}
                            name="thu_day_to"
                            value={this.state.thu_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "thu_day_to", value: val } });
                            }}
                          />
                          {this.state.wed_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="thu_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.thu_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="thu_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.thu_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Friday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.fri_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.fri_day_from_error ? "custom_select_validation" : null}
                            name="fri_day_from"
                            value={this.state.fri_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "fri_day_from", value: val } });
                            }}
                          />
                          {this.state.fri_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.fri_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.fri_day_to_error ? "custom_select_validation" : null}
                            name="fri_day_to"
                            value={this.state.fri_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "fri_day_to", value: val } });
                            }}
                          />
                          {this.state.wed_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="fri_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.fri_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="fri_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.fri_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Saturday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sat_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.sat_day_from_error ? "custom_select_validation" : null}
                            name="sat_day_from"
                            value={this.state.sat_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sat_day_from", value: val } });
                            }}
                          />
                          {this.state.sat_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sat_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.sat_day_to_error ? "custom_select_validation" : null}
                            name="sat_day_to"
                            value={this.state.sat_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sat_day_to", value: val } });
                            }}
                          />
                          {this.state.wed_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sat_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sat_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sat_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sat_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Sunday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sun_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.sun_day_from_error ? "custom_select_validation" : null}
                            name="sun_day_from"
                            value={this.state.sun_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sun_day_from", value: val } });
                            }}
                          />
                          {this.state.sun_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sun_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.sun_day_to_error ? "custom_select_validation" : null}
                            name="sun_day_to"
                            value={this.state.sun_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sun_day_to", value: val } });
                            }}
                          />
                          {this.state.wed_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sun_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sun_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sun_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sun_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Public Holidays</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.public_day_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.public_day_from_error ? "custom_select_validation" : null}
                            name="public_day_from"
                            value={this.state.public_day_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "public_day_from", value: val } });
                            }}
                          />
                          {this.state.public_day_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.public_day_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.public_day_to_error ? "custom_select_validation" : null}
                            name="public_day_to"
                            value={this.state.public_day_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "public_day_to", value: val } });
                            }}
                          />
                          {this.state.wed_day_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="public_day_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.public_day_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="public_day_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.public_day_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                    </TabPane>
                    <TabPane tabId="2">
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Monday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.mon_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.mon_night_from_error ? "custom_select_validation" : null}
                            name="mon_night_from"
                            value={this.state.mon_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "mon_night_from", value: val } });
                            }}
                          />
                          {this.state.mon_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.mon_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.mon_night_to_error ? "custom_select_validation" : null}
                            name="mon_night_to"
                            value={this.state.mon_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "mon_night_to", value: val } });
                            }}
                          />
                          {this.state.mon_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="mon_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.mon_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="mon_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.mon_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <button type="button" className="btn btn-add ap-all" onClick={this.Applytoallnight}>
                            Apply to all
                          </button>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Tuesday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.tue_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.tue_night_from_error ? "custom_select_validation" : null}
                            name="tue_night_from"
                            value={this.state.tue_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "tue_night_from", value: val } });
                            }}
                          />
                          {this.state.tue_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.tue_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.tue_night_to_error ? "custom_select_validation" : null}
                            name="tue_night_to"
                            value={this.state.tue_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "tue_night_to", value: val } });
                            }}
                          />
                          {this.state.tue_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="tue_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.tue_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="tue_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.tue_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Wednesday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.wed_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.wed_night_from_error ? "custom_select_validation" : null}
                            name="wed_night_from"
                            value={this.state.wed_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "wed_night_from", value: val } });
                            }}
                          />
                          {this.state.wed_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.wed_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.wed_night_to_error ? "custom_select_validation" : null}
                            name="wed_night_to"
                            value={this.state.wed_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "wed_night_to", value: val } });
                            }}
                          />
                          {this.state.wed_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="wed_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.wed_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="wed_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.wed_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Thursday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.thu_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.thu_night_from_error ? "custom_select_validation" : null}
                            name="thu_night_from"
                            value={this.state.thu_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "thu_night_from", value: val } });
                            }}
                          />
                          {this.state.thu_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.thu_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.thu_night_to_error ? "custom_select_validation" : null}
                            name="thu_night_to"
                            value={this.state.thu_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "thu_night_to", value: val } });
                            }}
                          />
                          {this.state.wed_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="thu_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.thu_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="thu_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.thu_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Friday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.fri_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.fri_night_from_error ? "custom_select_validation" : null}
                            name="fri_night_from"
                            value={this.state.fri_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "fri_night_from", value: val } });
                            }}
                          />
                          {this.state.fri_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.fri_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.fri_night_to_error ? "custom_select_validation" : null}
                            name="fri_night_to"
                            value={this.state.fri_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "fri_night_to", value: val } });
                            }}
                          />
                          {this.state.wed_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="fri_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.fri_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="fri_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.fri_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Saturday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sat_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.sat_night_from_error ? "custom_select_validation" : null}
                            name="sat_night_from"
                            value={this.state.sat_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sat_night_from", value: val } });
                            }}
                          />
                          {this.state.sat_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sat_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.sat_night_to_error ? "custom_select_validation" : null}
                            name="sat_night_to"
                            value={this.state.sat_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sat_night_to", value: val } });
                            }}
                          />
                          {this.state.wed_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sat_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sat_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sat_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sat_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Sunday</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sun_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.sun_night_from_error ? "custom_select_validation" : null}
                            name="sun_night_from"
                            value={this.state.sun_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sun_night_from", value: val } });
                            }}
                          />
                          {this.state.sun_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.sun_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.sun_night_to_error ? "custom_select_validation" : null}
                            name="sun_night_to"
                            value={this.state.sun_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "sun_night_to", value: val } });
                            }}
                          />
                          {this.state.wed_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sun_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sun_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="sun_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.sun_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>{" "}
                      <Row>
                        <Col xs="12" md="2">
                          <Label className={"h6 mt-4"}>Public Holidays</Label>
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.public_night_from_error ? <Label className="error-color"> Start Time</Label> : <Label for="from">Start Time</Label>}
                          <Select
                            className={this.state.public_night_from_error ? "custom_select_validation" : null}
                            name="public_night_from"
                            value={this.state.public_night_from}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "public_night_from", value: val } });
                            }}
                          />
                          {this.state.public_night_from_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          {this.state.public_night_to_error ? <Label className="error-color"> End Time</Label> : <Label>End Time</Label>}
                          <Select
                            className={this.state.public_night_to_error ? "custom_select_validation" : null}
                            name="public_night_to"
                            value={this.state.public_night_to}
                            options={this.state.timelist}
                            onChange={val => {
                              this.selectChange({ target: { name: "public_night_to", value: val } });
                            }}
                          />
                          {this.state.wed_night_to_error ? <div className="error-color"> This is required!</div> : null}
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Client Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="public_night_client_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.public_night_client_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="2">
                          <AvGroup>
                            <Label>Employee Rate (£)</Label>
                            <AvInput
                              min={"0"}
                              type="number"
                              name="public_night_employee_rate"
                              placeholder="Enter Rate (£)"
                              onChange={this.onChange}
                              value={this.state.public_night_employee_rate}
                              required
                              autoComplete="jobrate"
                            />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                    </TabPane>
                  </TabContent>
                </CardBody>
                <CardFooter>
                  {this.state.activeTab === "2" ? (
                    <Fragment>
                      <Button
                        type="button"
                        color="secondary mb-3"
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Previous
                      </Button>
                      <Button type="submit" color="success pull-right mb-3" title="Add Job Role" onClick={this.checkSelectError}>
                        Add
                      </Button>
                    </Fragment>
                  ) : null}
                  {this.state.activeTab === "1" ? (
                    <Fragment>
                      <Button
                        type="button"
                        color="success pull-right mb-3"
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Next
                      </Button>
                    </Fragment>
                  ) : null}
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addjobtype;
