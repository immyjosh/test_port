/*eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import { Button, Input, InputGroup, Table, UncontrolledTooltip, Badge } from "reactstrap";
import request from "../../../api/api";
import callFile from "../../../api/gettoken";
import Pagination from "react-js-pagination";
// import moment from "moment";
// import classnames from "classnames";

class Listjobtype extends Component {
  state = {
    adminlist: [],
    subscribedlist: [],
    currentlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    subscribedstartdate: "",
    subscribedplan: "",
    sortOrder: true,
    subscribesortOrder: true,
    loading: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 5,
    tableOptions: {
      client_id: this.props && this.props.forClient ? this.props.forClient.clientid : "",
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 5,
      skip: 0,
      status: 1
    },
    subscribedpages: "",
    subscribedcurrPage: 5,
    subscriptionactivePage: 1,
    subscriptionpageRangeDisplayed: 4,
    subscriptiontableOptions: {
      client_id: this.props && this.props.forClient ? this.props.forClient.clientid : "",
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "",
      field: "",
      limit: 5,
      skip: 0
    },
    token_username: "",
    subscribedmsg: "",
    showSubscriptionsTable: false,
    activeTab: "2",
    subscribedids: []
  };
  call = new callFile();
  componentDidMount() {
    request({
      url: "/agency/client/jobtype/list",
      method: "POST",
      data: this.state.subscriptiontableOptions
    }).then(res => {
      if (res.status === 1) {
        let subscribedids = res.response.result.map(list => list.job_id);
        this.setState({
          currentlist: res.response.current,
          subscribedlist: res.response.result,
          subscribedids: subscribedids,
          subscribedpages: res.response.fullcount,
          subscribedcurrPage: res.response.length
        });
      } else if (res.status === 0) {
        this.setState({ subscribedmsg: res.response });
      }
    });
  }
  subscribepopulateData() {
    request({
      url: "/agency/client/jobtype/list",
      method: "POST",
      data: this.state.subscriptiontableOptions
    }).then(res => {
      if (!res.response.result && !res.response.fullcount) {
        this.setState({ currentlist: [], subscribedpages: "" });
      } else {
        this.setState({
          currentlist: res.response.current,
          subscribedlist: res.response.history,
          // subscribedstartdate: res.response.history[0].start,
          // subscribedplan: res.response.history[0].plan,
          subscribedpages: res.response.fullcount,
          subscribedcurrPage: res.response.length
        });
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  subscribechangeLimit = page => {
    this.setState(state => {
      state.subscriptiontableOptions.limit = parseInt(page, 10);
      state.subscriptiontableOptions.skip = 0;
      state.subscriptiontableOptions.page.history = 1;
      state.subscriptiontableOptions.page.current = 1;
      state.count = 0;
      state.subscriptionactivePage = 1;
    });
    this.subscribepopulateData();
  };
  sort(field) {
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  subscribesort(field) {
    this.setState(state => {
      state.subscribesortOrder = !state.subscribesortOrder;
    });
    this.setState(state => {
      state.subscriptiontableOptions.order = state.subscribesortOrder ? 1 : -1;
      state.subscriptiontableOptions.field = field;
      this.subscribepopulateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  subscribesearch(value) {
    this.setState(state => {
      state.subscriptiontableOptions.search = value;
    });
    this.subscribepopulateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Free days") {
        state.tableOptions.filter = "freedays";
      } else if (value === "No of days") {
        state.tableOptions.filter = "noofdays";
      } else if (value === "Amount") {
        state.tableOptions.filter = "amount";
      }
    });
    this.populateData();
  }
  subscribefilter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.subscriptiontableOptions.filter = "name";
      } else if (value === "Free days") {
        state.subscriptiontableOptions.filter = "freedays";
      } else if (value === "No of days") {
        state.subscriptiontableOptions.filter = "noofdays";
      } else if (value === "Amount") {
        state.subscriptiontableOptions.filter = "amount";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  subscribepaginate = data => {
    this.setState({ subscriptionactivePage: data });
    let history = this.state.subscriptiontableOptions.page.history;
    let limit = this.state.subscriptiontableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.subscriptiontableOptions.page.current = data;
          state.subscriptiontableOptions.page.history = data;
          state.subscriptiontableOptions.skip = data * limit - limit;
          this.subscribepopulateData();
        } else if (history === data) {
          state.subscriptiontableOptions.page.current = data;
          state.subscriptiontableOptions.page.history = data;
          state.subscriptiontableOptions.skip = data * limit - limit;
          this.subscribepopulateData();
        } else {
          state.subscriptiontableOptions.page.current = data;
          state.subscriptiontableOptions.page.history = data;
          state.subscriptiontableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.subscribepopulateData();
        }
      });
    }
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  AddJobtype = value => {
    request({
      url: "/agency/client/jobtype/add",
      method: "POST",
      data: {
        id: value,
        client_id: this.props.forClient.clientid
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          this.setState({ activeTab: "2" });
          toast.success("Added");
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  RemoveJobtype = value => {
    request({
      url: "/agency/client/jobtype/remove",
      method: "POST",
      data: {
        id: value,
        client_id: this.props.forClient.clientid
      }
    })
      .then(res => {
        if (res.status === 1) {
          this.componentDidMount();
          toast.success("Removed");
          // this.setState({ activeTab: "2" });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/editjobtypeclient",
      state: { rowid: id, clientID: this.props.forClient.clientid }
    });
  };
  render() {
    return (
      <Fragment>
        <div className="row d-flex justify-content-end">
          <div className="col-lg-auto pr-0">
            <Input onChange={e => this.subscribefilter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
              <option>All</option>
              <option>Name</option>
            </Input>
          </div>
          <div className="col-lg-3 pl-0">
            <InputGroup>
              <Input type="text" ref="subsearch" placeholder="Search" name="search" onChange={e => this.subscribesearch(e.target.value)} className="rounded-0" />
              <Button
                className="rounded-0"
                color="primary"
                id="clearrr"
                onClick={() => {
                  ReactDOM.findDOMNode(this.refs.subsearch).value = "";
                  this.subscribesearch("");
                }}
              >
                <i className="fa fa-remove" />
              </Button>
              <UncontrolledTooltip placement="top" target="clearrr">
                Clear
              </UncontrolledTooltip>
            </InputGroup>
          </div>
        </div>
        <div className="table-responsive mt-2">
          <Table hover bordered responsive>
            <thead>
              <tr>
                <th>S.No.</th>
                <th
                  onClick={() => {
                    this.sort("name");
                  }}
                >
                  Job Role <i className="fa fa-sort" />
                </th>
                <th
                  onClick={() => {
                    this.sort("status");
                  }}
                >
                  Status <i className="fa fa-sort" />
                </th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.subscribedlist && this.state.subscribedlist.length > 0 ? (
                this.state.subscribedlist.map((item, i) => (
                  <tr key={item._id}>
                    <td>{this.state.tableOptions.skip + i + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.job_id ? <Badge color="success">Active</Badge> : <Badge color="danger">In-active</Badge>}</td>
                    <td>
                      <Fragment>
                        {!item.job_id ? (
                          <>
                            <button type="button" title="Edit" className="btn btn-primary btn-sm" id={`edit52s${i}`} onClick={id => this.AddJobtype(item._id)}>
                              <i className="fa fa-check" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit52s${i}`}>
                              Add to Client
                            </UncontrolledTooltip>
                          </>
                        ) : (
                          <>
                            <button type="button" title="Edit" className="btn btn-danger btn-sm" id={`edit52s${i}`} onClick={id => this.RemoveJobtype(item._id)}>
                              <i className="fa fa-remove" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit52s${i}`}>
                              Remove from client
                            </UncontrolledTooltip>
                            <button type="button" title="Edit" className="btn btn-primary btn-sm ml-2" id={`edit875`} onClick={id => this.editpage(item._id)}>
                              <i className="fa fa-edit" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit875`}>
                              Edit
                            </UncontrolledTooltip>
                          </>
                        )}
                      </Fragment>
                    </td>
                  </tr>
                ))
              ) : (
                <tr className="text-center">
                  <td colSpan={9}>
                    <h5>No record available</h5>
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </div>
        <nav className="float-left">
          {/*<Label>Show no.of items : </Label>*/}
          <Input onChange={e => this.subscribechangeLimit(e.target.value)} type="select" name="select">
            <option>5</option>
            <option>10</option>
            <option>25</option>
            <option>50</option>
            <option>100</option>
          </Input>
        </nav>
        <nav className="float-right">
          <div>
            <Pagination
              prevPageText="Prev"
              nextPageText="Next"
              firstPageText="First"
              lastPageText="Last"
              activePage={this.state.subscriptionactivePage}
              itemsCountPerPage={this.state.subscriptiontableOptions.limit}
              totalItemsCount={this.state.subscribedpages}
              pageRangeDisplayed={this.state.subscriptionpageRangeDisplayed}
              onChange={this.subscribepaginate}
            />
          </div>
        </nav>
        <br />
      </Fragment>
    );
  }
}

export default withRouter(Listjobtype);
