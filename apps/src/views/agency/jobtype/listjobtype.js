import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import Loader from "../../common/loader";
import { Badge, Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import fileDownload from "js-file-download";
import Pagination from "react-js-pagination";
import { DateRangePicker } from "react-dates";
import moment from "moment";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "140px"
};

class Listjobtype extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    isLoader: false,
    subscriptionlist: [],
    activePage: 1,
    pageRangeDisplayed: 4,
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      from_date: "",
      to_date: ""
    },
    start_date: "",
    end_date: "",
    forClient: false
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      // return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    this.populateData();
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/editjobtype",
      state: { rowid: id }
    });
  };
  populateData() {
    request({
      url: "/agency/jobtype/common/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length,
          isLoader: false
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["name", "createdAt", "status"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "All") {
        state.tableOptions.filter = "all";
      } else if (value === "Name") {
        state.tableOptions.filter = "name";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  export() {
    request({
      url: "/agency/jobtype/jobtypeexport",
      method: "GET"
    }).then(res => {
      if (res.status === 0) {
        toast.error(res.response);
      } else if (res.status === 1) {
        fileDownload(res.response, "export.csv");
        toast.success("Document Exported!");
      }
    });
  }
  listjobtypetoastId = "Listjobtype";
  nodify() {
    if (!toast.isActive(this.listjobtypetoastId)) {
      this.listjobtypetoastId = toast.warn("Updated");
    }
  }
  adminsavenodifytoastId = "Listjobtype";
  adminsavenodify() {
    if (!toast.isActive(this.adminsavenodifytoastId)) {
      this.adminsavenodifytoastId = toast.success("New Jobtype Added");
    }
  }
  fromTo() {
    if (this.state.start_date !== null && this.state.end_date !== null) {
      this.setState(state => {
        state.tableOptions.from_date = this.state.start_date._d;
        state.tableOptions.to_date = this.state.end_date._d;
      });

      this.populateData();
    } else {
      this.setState(state => {
        state.tableOptions.from_date = "";
        state.tableOptions.to_date = "";
      });
    }
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            List of Job Roles
            <div className="card-actions" style={tStyle}>
              <button style={tStyles} onClick={() => this.props.history.push("/agency/addjobtype")}>
                <i className="fa fa-plus" /> Add New
              </button>
              <button style={tStyles} onClick={this.export}>
                <i className="fa fa-upload" /> Export
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row">
              <div className="col-lg-7">
                <DateRangePicker
                  showClearDates={true}
                  startDate={this.state.start_date}
                  startDateId="start_date"
                  endDate={this.state.end_date}
                  endDateId="end_date"
                  onDatesChange={({ startDate, endDate }) => {
                    if (startDate ===null && endDate=== null ) {
                      this.setState(state => {
                        state.tableOptions.from_date = "";
                        state.tableOptions.to_date = "";
                      });
                      this.populateData();
                    }
                    this.setState({
                      start_date: startDate,
                      end_date: endDate
                    });
                  }}
                  focusedInput={this.state.focusedInput}
                  onFocusChange={focusedInput => this.setState({ focusedInput })}
                  displayFormat="DD-MM-YYYY"
                  isOutsideRange={() => false}
                />
                <Button
                  size="md"
                  color="primary"
                  onClick={() => {
                    this.fromTo();
                  }}
                >
                  <i className="fa fa-search" />
                </Button>
              </div>
              <div className="col-lg-5 pr-0">
                <InputGroup>
                  <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0 col-lg-3">
                    <option>All</option>
                    <option>Name</option>
                  </Input>
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0 col-lg-7" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("name");
                      }}
                    >
                      Job Role <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("createdAt");
                      }}
                    >
                      Creadted Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("status");
                      }}
                    >
                      Status <i style={{ paddingLeft: "25px" }} className={sorticondef} id="status" />
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.name}</td>
                        <td>{moment(item.createdAt).format("DD-MM-YYYY")}</td>
                        <td>
                          {item.status === 1 ? <Badge color="success">Active</Badge> : null}
                          {item.status === 0 ? <Badge color="danger">In-active</Badge> : null}
                        </td>
                        <td>
                          <Fragment>
                            <button type="button" title="Edit" className="btn table-edit" id={`edit87${i}`} onClick={id => this.editpage(item._id)}>
                              <i className="fa fa-edit" />
                            </button>
                            <UncontrolledTooltip placement="top" target={`edit87${i}`}>
                              Edit
                            </UncontrolledTooltip>
                          </Fragment>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={13}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Listjobtype;
