import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import { Editor } from "@tinymce/tinymce-react";
import savenodify from "./listemailtemplate";
import { Settings } from "../../../api/key";

class addemailtemplate extends Component {
  state = {
    description: "",
    name: "",
    email_subject: "",
    subscriptionmailoption: "",
    sender_name: "",
    sender_email: "",
    email_content: ""
  };
  flash = new savenodify();
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  subChange = e => {
    this.setState({ subscriptionmailoption: e.target.checked });
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangeSelect = e => {
    if (e) {
      request({
        url: "/agency/emailtemplate/selecttypedata",
        method: "POST",
        data: { type: e.target.value, pagefor: "editpage" }
      }).then(res => {
        if (res.status === 1) {
          this.setState({ adminlist: res.response.result[0] });
          this.setState({
            heading: this.state.adminlist.heading,
            description: this.state.adminlist.description,
            email_subject: this.state.adminlist.email_subject,
            sender_name: this.state.adminlist.sender_name,
            sender_email: this.state.adminlist.sender_email,
            email_content: this.state.adminlist.email_content
          });
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      });
      this.setState({ [e.target.name]: e.target.value });
    }
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
  }
  OnFormSubmit = (e, values) => {
    e.persist();
    const adminform = {
      heading: this.state.heading,
      description: this.state.description,
      email_subject: this.state.email_subject,
      sender_name: this.state.sender_name,
      sender_email: this.state.sender_email,
      email_content: this.state.email_content,
      type: this.state.type
    };
    request({
      url: "/agency/emailtemplate/save",
      method: "POST",
      data: adminform
    })
      .then(res => {
        if (res.status === 1) {
          this.props.history.push("/agency/settings/email/list");
          toast.success("Template Added");
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Email Template
                {/* <div className="card-actions">
                  <a>
                    <i className="fa fa-user-plus" />
                    <small className="text-muted" />
                  </a>
                </div>*/}
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
                  <Row>
				    <Col xs="12" md="12">
					<div className="cus-design edited-section">
                    <Col xs="12" md="6">
                      <AvField type="select" name="type" label="Email Template Type" onChange={this.onChangeSelect} value={this.state.type} required autoFocus>
                        <option>Select</option>
                        <option value="profile">Profile</option>
                        <option value="invocie">Invoice</option>
                        <option value="timesheet">Timesheet</option>
                        <option value="candidatewelcome">Candidate Welcome</option>
                        <option value="sendrefereeform">Reference Form</option>
                        <option value="interviewcall">Interview Call</option>
                        <option value="signupsuccessmessage">Registration</option>
                        <option value="compliancemail">Compliance</option>
                        <option value="applicationapproval">Online Application Form Approval</option>
                        <option value="interviewstatus">Interview Status</option>
                        <option value="refereeverification">Reference Verification Status</option>
                      </AvField>
                    </Col>
                
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Template Name</Label>
                        <AvInput type="text" name="heading" placeholder="Enter templatename" onChange={this.onChange} value={this.state.heading} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Email Subject</Label>
                        <AvInput type="text" name="email_subject" placeholder="Enter Email Subject" onChange={this.onChange} value={this.state.email_subject} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                 
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Sender Name</Label>
                        <AvInput type="text" name="sender_name" placeholder="Enter Sender Name" onChange={this.onChange} value={this.state.sender_name} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                  
                    <Col xs="12" md="12">
                      <label>Email Content</label>
                      <Editor
                        apiKey={Settings.tinymceKey}
                        initialValue={this.state.email_content}
                        init={{
                          plugins: "link image code",
                          toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                          height: 460
                        }}
                        onChange={this.handleEditorChange}
                        value={this.state.email_content}
                      />
                    </Col>
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="success pull-right mb-3" title="Add Email Template">
                    Add
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default addemailtemplate;
