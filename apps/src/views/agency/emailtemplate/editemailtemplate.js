/*eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import { Editor } from "@tinymce/tinymce-react";
import { Settings } from "../../../api/key";

class editemailtemplate extends Component {
  state = {
    description: "",
    heading: "",
    email_subject: "",
    subscriptionmailoption: "",
    sender_name: "",
    sender_email: "",
    email_content: "",
    tokenusername: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleEditorChange = value => {
    this.setState({ email_content: value.level.content });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/agency/emailtemplate/edit",
      method: "POST",
      data: { id: this.props.location.state.rowid }
    }).then(res => {
      if (res.status === 1) {
        const value = res.response.result;
        this.setState({
          heading: value.heading,
          description: value.description,
          email_subject: value.email_subject,
          sender_name: value.sender_name,
          sender_email: value.sender_email,
          email_content: value.email_content,
          default_mail: value.default_mail,
          mail_def: value.mail_def,
          type: value.type
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  onEditAdmin = (e, values) => {
    request({
      url: "/agency/emailtemplate/save",
      method: "POST",
      data: {
        _id: this.props.location.state.rowid,
        heading: this.state.heading,
        description: this.state.description,
        email_subject: this.state.email_subject,
        sender_name: this.state.sender_name,
        sender_email: this.state.sender_email,
        email_content: this.state.email_content,
        type: this.state.type
      }
    }).then(res => {
      if (res.status === 1) {
        toast.success("Updated");
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-note" />
                  Edit Email Template
                  {/* <div className="card-actions">
                    <a>
                      <i className="fa fa-user" />
                      <small className="text-muted" />
                    </a>
                  </div>*/}
                </CardHeader>
                <CardBody>
                  <Row>
				    <Col xs="12" md="12">
					<div className="cus-design edited-section">
                    <Col xs="12" md="6">
                      <AvField type="select" name="type" label="Email Template Type" onChange={this.onChangeSelect} value={this.state.type} required disabled readOnly>
                        <option>Select</option>
                        <option value="profile">Profile</option>
                        <option value="invocie">Invoice</option>
                        <option value="timesheet">Timesheet</option>
                        <option value="candidatewelcome">Candidate Welcome</option>
                        <option value="sendrefereeform">Reference Form</option>
                        <option value="interviewcall">Interview Call</option>
                        <option value="signupsuccessmessage">Registration</option>
                        <option value="compliancemail">Compliance</option>
                        <option value="applicationapproval">Online Application Form Approval</option>
                        <option value="interviewstatus">Interview Status</option>
                        <option value="refereeverification">Reference Verification Status</option>
                      </AvField>
                    </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Template Name</Label>
                        <AvInput type="text" name="heading" placeholder="Enter templatename" onChange={this.onChange} value={this.state.heading} autoFocus />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Email Subject</Label>
                        <AvInput type="text" name="email_subject" placeholder="Enter Email Subject" onChange={this.onChange} value={this.state.email_subject} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="6">
                      <AvGroup>
                        <Label>Sender Name</Label>
                        <AvInput type="text" name="sender_name" placeholder="Enter Sender Name" onChange={this.onChange} value={this.state.sender_name} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="12">
                      <label>Email Content</label>
                      <Editor
                        apiKey={Settings.tinymceKey}
                        initialValue={this.state.email_content}
                        init={{
                          plugins: "link image code",
                          toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code",
                          height: 460
                        }}
                        onChange={this.handleEditorChange}
                        value={this.state.email_content}
                        disabled={this.state.mail_def === "yes"}
                      />
                    </Col>
					</div>
					</Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i className="fa fa-dot-circle-o" /> Update
                  </Button>
                  <Link to="/agency/settings/email/list">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default editemailtemplate;
