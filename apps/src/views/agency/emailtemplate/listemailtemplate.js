import React, { Component } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardHeader, Input, InputGroup, Table, UncontrolledTooltip } from "reactstrap";
import request from "../../../api/api";
import Loader from "../../common/loader";
import Pagination from "react-js-pagination";

class listemailtemplate extends Component {
  state = {
    adminlist: [],
    url: "",
    adminredirect: false,
    isLoader: false,
    deletedisable: true,
    subscriptionlist: [],
    sortOrder: true,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 25,
    activePage: 1,
    pageRangeDisplayed: 4,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 10,
      skip: 0,
      type: ""
    }
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ isLoader: true });
    this.populateData();
  }
  editpage = id => {
    return this.props.history.push({
      pathname: "/agency/settings/email/edit",
      state: { rowid: id }
    });
  };
  deletetemplate = (e, id) => {
    e.stopPropagation();
    request({
      url: "/agency/emailtemplate/delete",
      method: "POST",
      data: { id }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response);
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  changeDefault = (e, id, type) => {
    console.log("e, id, type ", e, id, type);
    e.stopPropagation();
    request({
      url: "/agency/emailtemplate/defaultstatus",
      method: "POST",
      data: { id, type }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response);
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  populateData() {
    request({
      url: "/agency/emailtemplate/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          isLoader: false,
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  ListbasedType = value => {
    if (value) {
      this.setState(state => {
        state.tableOptions.type = value;
        this.populateData();
      });
    }
  };
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 10);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    const sorticondef = "fa fa-sort";
    const id = ["heading", "email_subject", "type"];
    for (const i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Template Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Sender Name") {
        state.tableOptions.filter = "sender_name";
      } else if (value === "Sender Email") {
        state.tableOptions.filter = "sender_email";
      } else if (value === "Email Subject") {
        state.tableOptions.filter = "email_subject";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    const history = this.state.tableOptions.page.history;
    const limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };

  nodify() {
    return toast.warn("Updated");
  }
  adminsavenodify() {
    return toast.success("New Template Added");
  }
  render() {
    const order = this.state.sortOrder;
    const sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    const sorticondef = "fa fa-sort";
    if (this.state.tableOptions.field) {
      if (document.getElementById(this.state.tableOptions.field)) {
        document.getElementById(this.state.tableOptions.field).className = sorticon;
      }
    }
    return (
      <div className="animated">
        <ToastContainer position="top-right" autoClose={2500} />
        <Card>
          <CardHeader>
            <i className="icon-list" />
            Email Template List
            <div className="card-actions">
              <button onClick={() => this.props.history.push("/agency/settings/email/add")}>
                <i className="fa fa-plus" /> Add New
                <small className="text-muted" />
              </button>
            </div>
          </CardHeader>
          <CardBody>
            <div className="row d-flex justify-content-between">
              <div className="col-lg-4">
                <Input onChange={e => this.ListbasedType(e.target.value)} type="select" name="type" className="btn btn-primary rounded-0">
                  <option value="all">All</option>
                  <option value="profile">Profile</option>
                  <option value="invocie">Invoice</option>
                  <option value="timesheet">Timesheet</option>
                  <option value="candidatewelcome">Candidate Welcome</option>
                  <option value="sendrefereeform">Reference Form</option>
                  <option value="interviewcall">Interview</option>
                  <option value="signupsuccessmessage">Registration</option>
                  <option value="compliancemail">Compliance</option>
                </Input>
              </div>
              <div className="col-lg-4 pl-0">
                <InputGroup>
                  {/* <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                    <option>All</option>
                    <option>Template Name</option>
                    <option>Email Subject</option>
                  </Input>*/}
                  <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                  <Button
                    className="rounded-0"
                    color="primary"
                    id="clear"
                    onClick={() => {
                      ReactDOM.findDOMNode(this.refs.search).value = "";
                      this.search("");
                    }}
                  >
                    <i className="fa fa-remove" />
                  </Button>
                  <UncontrolledTooltip placement="top" target="clear">
                    Clear
                  </UncontrolledTooltip>
                </InputGroup>
              </div>
            </div>
            <div className="table-responsive mt-2">
              <Table hover bordered responsive size="sm">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th
                      onClick={() => {
                        this.sort("heading");
                      }}
                    >
                      Template Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="heading" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("email_subject");
                      }}
                    >
                      Email Subject <i style={{ paddingLeft: "25px" }} className={sorticondef} id="email_subject" />
                    </th>
                    <th
                      onClick={() => {
                        this.sort("type");
                      }}
                    >
                      Email Type <i style={{ paddingLeft: "25px" }} className={sorticondef} id="type" />
                    </th>
                    <th>Default</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.adminlist.length > 0 ? (
                    this.state.adminlist.map((item, i) => (
                      <tr key={item._id} onClick={id => this.editpage(item._id)}>
                        <td>{this.state.tableOptions.skip + i + 1}</td>
                        <td>{item.heading}</td>
                        <td>{item.email_subject}</td>
                        <td>
                          {item.type === "profile"
                            ? "Profile"
                            : item.type === "invocie"
                              ? "Invocie"
                              : item.type === "timesheet"
                                ? "Timesheet"
                                : item.type === "candidatewelcome"
                                  ? "Candidatew Welcome"
                                  : item.type === "sendrefereeform"
                                    ? "Reference Form"
                                    : item.type === "interviewcall"
                                      ? "Interview Call"
                                      : item.type === "signupsuccessmessage"
                                        ? "Registration"
                                        : item.type === "compliancemail"
                                          ? "Compliance"
                                          : item.type === "applicationapproval"
                                            ? "Online Application Form Approval"
                                            : item.type === "interviewstatus"
                                              ? "Interview Status"
                                              : item.type === "refereeverification"
                                              ? "Reference Verification Status"
                                              : ""}
                        </td>
                        <td>
                          {item.default_mail ? (
                            <>
                              <button type="button" title="Edit" className="btn view-table" color="info" id={`default1${i}`} style={{ cursor: "default" }}>
                                <i className="fa fa-star fa-2x" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`default1${i}`}>
                                Default
                              </UncontrolledTooltip>
                            </>
                          ) : (
                            <>
                              <button type="button" title="Edit" className="btn view-table" color="info" id={`default1${i}`} onClick={(e, id, type) => this.changeDefault(e, item._id, item.type)}>
                                <i className="fa fa-star-o fa-2x" />
                              </button>
                              <UncontrolledTooltip placement="top" target={`default1${i}`}>
                                Set As Default
                              </UncontrolledTooltip>
                            </>
                          )}
                        </td>
                        <td>
                          <button type="button" title="Edit" className="btn view-table" color="info" id={`editee${i}`} onClick={id => this.editpage(item._id)}>
                            <i className="fa fa-edit" />
                          </button>
                          <UncontrolledTooltip placement="top" target={`editee${i}`}>
                            Edit
                          </UncontrolledTooltip>
                          {!item.default_mail ? (
                            <>
                              {item.mail_def && item.mail_def === "yes" ? null : (
                                <>
                                  <button type="button" title="Edit" className="btn view-table ml-4" color="info" id={`editss${i}`} onClick={(e, id) => this.deletetemplate(e, item._id)}>
                                    <i className="fa fa-trash" />
                                  </button>
                                  <UncontrolledTooltip placement="top" target={`editss${i}`}>
                                    Delete
                                  </UncontrolledTooltip>
                                </>
                              )}
                            </>
                          ) : null}
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr className="text-center">
                      <td colSpan={9}>{!this.state.isLoader && <h5>No record available</h5>}</td>
                    </tr>
                  )}
                  {this.state.isLoader && <Loader />}
                </tbody>
              </Table>
            </div>
            <nav className="float-left">
              {/* <Label>Show no.of items : </Label>*/}
              <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
              </Input>
            </nav>
            <nav className="float-right">
              <div>
                <Pagination
                  prevPageText="Prev"
                  nextPageText="Next"
                  firstPageText="First"
                  lastPageText="Last"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.tableOptions.limit}
                  totalItemsCount={this.state.pages}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.paginate}
                />
              </div>
            </nav>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default listemailtemplate;
