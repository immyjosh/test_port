/*eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
// import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardHeader, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane, UncontrolledTooltip, Badge } from "reactstrap";
import request, { client } from "../../../api/api";
import callFile from "../../../api/gettoken";
import Pagination from "react-js-pagination";
import moment from "moment";
import classnames from "classnames";

const subh4 = {
  color: "rgb(177, 50, 50)"
};

class Subscribe extends Component {
  state = {
    adminlist: [],
    subscribedlist: [],
    currentlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    subscribedstartdate: "",
    subscribedplan: "",
    sortOrder: true,
    subscribesortOrder: true,
    loading: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 10,
    tableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 5,
      skip: 0,
      status: 1,
      availability: 1,
    },
    subscribedpages: "",
    subscribedcurrPage: 10,
    subscriptionactivePage: 1,
    subscriptionpageRangeDisplayed: 4,
    subscriptiontableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 9,
      skip: 0,
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    token_username: "",
    subscribedmsg: "",
    showSubscriptionsTable: false,
    activeTab: this.props && this.props.location && this.props.location.state && this.props.location.state.activetab? this.props.location.state.activetab:"1"
  };
  call = new callFile();
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      token,
      token_username: token.username
    });

    this.populateData();
    request({
      url: "/agency/subscription/subscriptions",
      method: "POST",
      data: this.state.subscriptiontableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          currentlist: res.response.current,
          subscribedlist: res.response.history,
          // subscribedstartdate: res.response.history[0].start,
          // subscribedplan: res.response.history[0].plan,
          subscribedpages: res.response.fullcount,
          subscribedcurrPage: res.response.length
        });
      } else if (res.status === 0) {
        this.setState({ subscribedmsg: res.response });
      }
    });
  }
  populateData() {
    request({
      url: "/administrators/plans/list",
      method: "POST",
      data: this.state.tableOptions
    }).then(res => {
      if (res.status === 1) {
        this.setState({
          adminlist: res.response.result,
          pages: res.response.fullcount,
          currPage: res.response.length
        });
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  changeLimit = page => {
    this.setState(state => {
      state.tableOptions.limit = parseInt(page, 9);
      state.tableOptions.skip = 0;
      state.tableOptions.page.history = 1;
      state.tableOptions.page.current = 1;
      state.count = 0;
      state.activePage = 1;
    });
    this.populateData();
  };
  sort(field) {
    let sorticondef = "fa fa-sort";
    let id = ["name", "noofdays", "freedays", "employees", "createdAt", "amount", "description"];
    for (let i in id) {
      document.getElementById(id[i]).className = sorticondef;
    }
    this.setState(state => {
      state.sortOrder = !state.sortOrder;
    });
    this.setState(state => {
      state.tableOptions.order = state.sortOrder ? 1 : -1;
      state.tableOptions.field = field;
      this.populateData();
    });
  }
  search(value) {
    this.setState(state => {
      state.tableOptions.search = value;
    });
    this.populateData();
  }
  filter(value) {
    this.setState(state => {
      if (value === "Name") {
        state.tableOptions.filter = "name";
      } else if (value === "Free days") {
        state.tableOptions.filter = "freedays";
      } else if (value === "No of days") {
        state.tableOptions.filter = "noofdays";
      } else if (value === "Amount") {
        state.tableOptions.filter = "amount";
      }
    });
    this.populateData();
  }
  paginate = data => {
    this.setState({ activePage: data });
    let history = this.state.tableOptions.page.history;
    let limit = this.state.tableOptions.limit;
    if (data) {
      this.setState(state => {
        if (history === "") {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else if (history === data) {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          this.populateData();
        } else {
          state.tableOptions.page.current = data;
          state.tableOptions.page.history = data;
          state.tableOptions.skip = data * limit - limit;
          state.bulk = [];
          state.count = 0;
          this.populateData();
        }
      });
    }
  };
  nodify() {
    return toast.warn("Updated");
  }
  nodifynosub() {
    return toast.warn("Please Subscribe!");
  }
  adminsavenodify() {
    return toast.success("Saved!");
  }
  subscribeNow = id => {
    const sub = {
      planid: id
    };
    request({
      url: "/agency/subscription/subscribe",
      method: "POST",
      data: sub
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result.status === 1) {
          if (res.auth_token) {
            localStorage.setItem("APUSA", res.auth_token);
            client.defaults.headers.common["Authorization"] = res.auth_token;
            this.call.tokenagain();
            window.location.reload(true);
          }
          return toast.success("Free Trial Activated"), this.componentDidMount();
        } else if (res.response.result.status === 0) {
          return this.props.history.push({
            pathname: "/agency/subscription/account",
            state: {
              data: {
                subid: res.response.result._id,
                plandetails: res.response.result.plan,
                username: this.state.token_username
              }
            }
          });
        }
      } else if (res.status === 2) {
        return toast.success("res.response");
      }
    });
  };
  paymentnodify() {
    return toast.success("Payment Completed!");
  }
  paymentNow = list => {
    this.props.history.push({
      pathname: "/agency/subscription/account",
      state: {
        data: {
          subid: list._id,
          plandetails: list.plan,
          username: this.state.token_username
        }
      }
    });
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };
  render() {
    let current_date = new Date();
    const {token} = this.state
    // let order = this.state.sortOrder;
    // let sorticon = `fa fa-sort${order === null ? "" : order === true ? "-amount-asc" : "-amount-desc"}`;
    // let sorticondef = "fa fa-sort";
    // if (this.state.tableOptions.field) {
    //   if (document.getElementById(this.state.tableOptions.field)) {
    //     document.getElementById(this.state.tableOptions.field).className = sorticon;
    //   }
    // }
    return (
      <Fragment>
        <div className="animated">
          <ToastContainer position="top-right" autoClose={2500} />
          <Card>
            <CardHeader>
              <i className="icon-list" />
              Subscription
            </CardHeader>
            <CardBody className="p-0">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "1" })}
                    onClick={() => {
                      this.toggle("1");
                    }}
                  >
                    <i className={"fa fa-gift"} /> Current Subscription
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === "2" })}
                    onClick={() => {
                      this.toggle("2");
                    }}
                  >
                    <i className={"fa fa-list-alt"} /> Available Subscriptions
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                  <Fragment>
                    {this.state.currentlist && this.state.currentlist.length > 0 ? (
                      <Row>
                        <Col md="12" xs="12">
                          <button className="btn btn-primary pull-right hover-drop">
                            Options <i className="fa fa-caret-down" />
                            <ul className="rel-postion">
                              <li
                                onClick={() => {
                                  this.props.history.push("/agency/subscription/invoice");
                                }}
                              >
                                <button className="btn ch-views">View Invoice</button>
                              </li>
                              <li
                                onClick={() => {
                                  this.props.history.push("/agency/subscription/editaccount");
                                }}
                              >
                                <button className="btn ch-views">Billing Accounts</button>
                              </li>
                              <li onClick={() => this.toggle("2")}>
                                <button className="btn ch-views">Change Subscription</button>
                              </li>
                              <li
                                onClick={() => {
                                  this.props.history.push("/agency/subscription/invoicelist");
                                }}
                              >
                                <button className="btn ch-views">View Statement</button>
                              </li>
                              <li onClick={() => this.toggle("1")}>
                                <button className="btn ch-views">Current Subscription</button>
                              </li>
                            </ul>
                          </button>
                        </Col>
                        <Col md="12" xs="12">
                          <div className="table-responsive mt-2">
                            <table className="table table-hover subsc-table" size="sm">
                              <thead>
                                <tr>
                                  <th>Plan</th>
                                  <th>Trial Period</th>
                                  <th>Billing Period</th>
                                  <th>Dates</th>
                                  <th>Employees Limit</th>
                                  <th>Recruitment Module</th>
                                  <th>Amount</th>
                                  <th>Description</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.currentlist.map(list => (
                                  <tr key={list._id}>
                                    <td className="plan-power">{list.plan.name}</td>
                                    <td>{list.plan.freedays} Days</td>
                                    <td> {list.plan.noofdays=== 30? "Monthly":list.plan.noofdays=== 365? "Yearly":""}</td>
                                    <td>
                                      {moment(list.start).format("DD-MM-YYYY")} - {moment(list.end).format("DD-MM-YYYY")}
                                    </td>
                                    <td>{list.plan.employees}</td>
                                    <td>{list.plan.recruitment_module === 1 ? "Yes" : "No"}</td>
                                    <td className="plan-power">£ {list.plan.amount}</td>
                                    <td>{list.plan.description}</td>
                                    <td>
                                      {" "}
                                      {list.payment_status === 0 || current_date >= list.end ? (
                                        <Button color="info" onClick={id => this.paymentNow(list)}>
                                          Pay Now
                                        </Button>
                                      ) : null}
                                      {list.payment_status === 1 ? <Badge color="success">Paid</Badge> : null}
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </table>
                          </div>
                        </Col>
                      </Row>
                    ) : (
                      <div className="text-center">
                        <div>
                          <h4 style={subh4}>
                            <p>
                              <strong>Please subscribe any plan to continue.</strong>
                            </p>
                            <button
                              onClick={() => {
                                this.toggle("2");
                              }}
                              className={"btn bnt-subs cursor-pointer"}
                            >
                              Click here to subscribe
                            </button>
                          </h4>
                        </div>
                      </div>
                    )}
                  </Fragment>
                </TabPane>
                <TabPane tabId="2">
                  <Fragment>
                    {/* <div className="row d-flex justify-content-end">
                      <div className="col-lg-auto pr-0">
                        <Input onChange={e => this.filter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                          <option>All</option>
                          <option>Name</option>
                        </Input>
                      </div>
                      <div className="col-lg-3 pl-0">
                        <InputGroup>
                          <Input type="text" ref="search" placeholder="Search" name="search" onChange={e => this.search(e.target.value)} className="rounded-0" />
                          <Button
                            className="rounded-0"
                            color="primary"
                            id="clear"
                            onClick={() => {
                              ReactDOM.findDOMNode(this.refs.search).value = "";
                              this.search("");
                            }}
                          >
                            <i className="fa fa-remove" />
                          </Button>
                          <UncontrolledTooltip placement="top" target="clear">
                            Clear
                          </UncontrolledTooltip>
                        </InputGroup>
                      </div>
                    </div> */}
                    <Row>
                    <Col md="12" xs="12">
                      {token && token.subscription === 1? (
                          <button className="btn btn-primary pull-right hover-drop">
                            Options <i className="fa fa-caret-down" />
                            <ul className="rel-postion">
                              <li
                                  onClick={() => {
                                    this.props.history.push("/agency/subscription/invoice");
                                  }}
                              >
                                <button className="btn ch-views">View Invoice</button>
                              </li>
                              <li
                                  onClick={() => {
                                    this.props.history.push("/agency/subscription/editaccount");
                                  }}
                              >
                                <button className="btn ch-views">Billing Accounts</button>
                              </li>
                              <li onClick={() => this.toggle("2")}>
                                <button className="btn ch-views">Change Subscription</button>
                              </li>
                              <li
                                  onClick={() => {
                                    this.props.history.push("/agency/subscription/invoicelist");
                                  }}
                              >
                                <button className="btn ch-views">View Statement</button>
                              </li>
                              <li onClick={() => this.toggle("1")}>
                                <button className="btn ch-views">Current Subscription</button>
                              </li>
                            </ul>
                          </button>
                      ): null}
                        </Col>
                    </Row>
                    <div className="payment-headings">
                      <ul>
                        <li className="active">
                          {" "}
                          <span> 1 </span> Select Plan{" "}
                        </li>
                        <li>
                          {" "}
                          <span> 2 </span> Select Billing Account{" "}
                        </li>
                        <li>
                          {" "}
                          <span> 3 </span> Review & Pay{" "}
                        </li>
                      </ul>
                    </div>
                    <div className="price-subscribe">
                      <div className="price-category">
                        {this.state.adminlist.length > 0 ? (
                          this.state.adminlist.map((item, i) => (
                            <div className="subscribe-lists" key={item._id}>
                              <div className="sub-design">
                                <h4>
                                  {item.name}{" "}
                                  <p>
                                    Trial Period - <span>{item.freedays} days</span>
                                  </p>
                                </h4>
                                <h2>$ {item.amount}</h2>
                                <div className="subs-lists">
                                  <p>
                                    Billing Period - <span> {item.noofdays=== 30? "Monthly":item.noofdays=== 365? "Yearly":""}</span>
                                  </p>
                                </div>
                                <div className="subs-lists">
                                  <p>
                                    Employees Limit - <span>{item.employees}</span>
                                  </p>
                                </div>
                                <div className="subs-lists">
                                  <p>
                                    Recruitment Module - <span>{item.recruitment_module === 1 ? "Yes" : "No"}</span>
                                  </p>
                                </div>
                                <div className="subs-des">
                                  <p>{item.description}</p>
                                </div>
                                <div className="subs-buttons">
                                  <p>
                                    <button type="button" title="Subscribe Now" className="btn btn-primary subscribe-btn" color="info" id={`edit${i}`} onClick={id => this.subscribeNow(item._id)}>
                                      Subscribe Now
                                    </button>
                                    <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                      Subscribe Now
                                    </UncontrolledTooltip>
                                  </p>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <tr className="text-center">
                            <td colSpan={9}>
                              <h2>
                                <strong>
                                  <h4>No record available</h4>
                                </strong>
                              </h2>
                            </td>
                          </tr>
                        )}
                      </div>
                    </div>

                    {/* <div className="table-responsive mt-2">
                      <Table hover bordered responsive size="sm">
                        <thead>
                          <tr>
                            <th>S.No.</th>
                            <th
                              onClick={() => {
                                this.sort("name");
                              }}
                            >
                              Name <i style={{ paddingLeft: "25px" }} className={sorticondef} id="name" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("freedays");
                              }}
                            >
                              Trial Period <i style={{ paddingLeft: "25px" }} className={sorticondef} id="freedays" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("noofdays");
                              }}
                            >
                              Billing Period <i style={{ paddingLeft: "25px" }} className={sorticondef} id="noofdays" />
                            </th>
                            <th
                              onClick={() => {
                                this.subscribesort("employees");
                              }}
                            >
                              Employees Limit <i style={{ paddingLeft: "25px" }} className={sorticondef} id="employees" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("amount");
                              }}
                            >
                              Amount <i style={{ paddingLeft: "25px" }} className={sorticondef} id="amount" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("description");
                              }}
                            >
                              Description <i style={{ paddingLeft: "25px" }} className={sorticondef} id="amount" />
                            </th>
                            <th
                              onClick={() => {
                                this.sort("createdAt");
                              }}
                              className="d-none"
                            >
                              Created Date <i style={{ paddingLeft: "25px" }} className={sorticondef} id="createdAt" />
                            </th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.adminlist.length > 0 ? (
                            this.state.adminlist.map((item, i) => (
                              <tr key={item._id}>
                                <td>{this.state.tableOptions.skip + i + 1}</td>
                                <td>{item.name}</td>
                                <td>
                                  {item.freedays} <small>Days</small>
                                </td>
                                <td>
                                  {item.noofdays} <small>Days</small>
                                </td>{" "}
                                <td>{item.employees}</td>
                                <td>£ {item.amount}</td>
                                <td>{item.description}</td>
                                <td className="d-none">{item.createdAt}</td>
                                <td>
                                  <button
                                    type="button"
                                    title="Subscribe Now"
                                    className="btn btn-primary btn-sm"
                                    color="info"
                                    id={`edit${i}`}
                                    onClick={id => this.subscribeNow(item._id)}
                                  >
                                    <i className="fa fa-money" />
                                  </button>
                                  <UncontrolledTooltip placement="top" target={`edit${i}`}>
                                    Subscribe Now
                                  </UncontrolledTooltip>
                                </td>
                              </tr>
                            ))
                          ) : (
                            <tr className="text-center">
                              <td colSpan={9}>
                                <h2>
                                  <strong>
                                    <h4>No record available</h4>
                                  </strong>
                                </h2>
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </Table>
                    </div> */}
                    {/* <nav className="float-left">
                      <Input onChange={e => this.changeLimit(e.target.value)} type="select" name="select">
                        <option>10</option>
                        <option>25</option>
                        <option>50</option>
                        <option>100</option>
                      </Input>
                    </nav> */}
                    <nav className="float-right">
                      <div>
                        <Pagination
                          prevPageText="Prev"
                          nextPageText="Next"
                          firstPageText="First"
                          lastPageText="Last"
                          activePage={this.state.activePage}
                          itemsCountPerPage={this.state.tableOptions.limit}
                          totalItemsCount={this.state.pages}
                          pageRangeDisplayed={this.state.pageRangeDisplayed}
                          onChange={this.paginate}
                        />
                      </div>
                    </nav>
                  </Fragment>
                  <br />
                  <br />
                </TabPane>
              </TabContent>
            </CardBody>
          </Card>
        </div>
      </Fragment>
    );
  }
}

export default Subscribe;
