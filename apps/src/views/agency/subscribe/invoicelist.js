/*eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
// import ReactDOM from "react-dom";
import { toast, ToastContainer } from "react-toastify";
import { Row, Col, Button, Card, CardBody, CardHeader, Table, CardFooter } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
// import Pagination from "react-js-pagination";
import moment from "moment";
import FileSaver from "file-saver";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "125px"
};

class InvoiceList extends Component {
  state = {
    adminlist: [],
    subscribedlist: [],
    currentlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    subscribedstartdate: "",
    subscribedplan: "",
    sortOrder: true,
    subscribesortOrder: true,
    loading: false,
    activePage: 1,
    pageRangeDisplayed: 4,
    bulk: [],
    count: 0,
    pages: "",
    currPage: 10,
    subscribedpages: "",
    subscribedcurrPage: 10,
    subscriptionactivePage: 1,
    subscriptionpageRangeDisplayed: 4,
    subscriptiontableOptions: {
      search: "",
      filter: "all",
      page: {
        history: "",
        current: 1
      },
      order: "-1",
      field: "createdAt",
      limit: 50,
      skip: 0,
      notification: this.props && this.props.location.state ? this.props.location.state.notificationName : ""
    },
    token_username: "",
    subscribedmsg: "",
    showSubscriptionsTable: false,
    activeTab: "1",
    subscribedlistTotal: "",
    logo: "",
    siteaddress: "",
    postal_address: "",
    company_name: ""
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      token_username: token.username
    });
    this.subscribepopulateData();
    request({
      url: "/site/settings",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        if (res.response.settings) {
          this.setState({
            sitetitle: res.response.settings.site_title,
            email: res.response.settings.email_address,
            sitelocation: res.response.settings.location,
            siteaddress: res.response.settings.siteaddress,
            logo: res.response.settings.logo
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result) {
          this.setState({
            _id: res.response.result[0]._id,
            company_name: res.response.result[0].company_name,
            postal_address: res.response.result[0].postal_address,
            company_email: res.response.result[0].company_email
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  subscribepopulateData() {
    request({
      url: "/agency/subscription/subscriptions",
      method: "POST",
      data: this.state.subscriptiontableOptions
    }).then(res => {
      if (res.status === 1) {
        let subscribedlistTotal = res.response.history && res.response.history.map(list => Number(list.plan.amount)).reduce((a, b) => a + b);
        this.setState({
          currentlist: res.response.current,
          subscribedlist: res.response.history,
          // subscribedstartdate: res.response.history[0].start,
          // subscribedplan: res.response.history[0].plan,
          subscribedpages: res.response.fullcount,
          subscribedcurrPage: res.response.length,
          subscribedlistTotal
        });
      } else if (res.status === 0) {
        this.setState({ subscribedmsg: res.response });
      }
    });
  }
  // changeLimit = page => {
  //   this.setState(state => {
  //     state.tableOptions.limit = parseInt(page, 10);
  //     state.tableOptions.skip = 0;
  //     state.tableOptions.page.history = 1;
  //     state.tableOptions.page.current = 1;
  //     state.count = 0;
  //     state.activePage = 1;
  //   });
  //   this.populateData();
  // };
  // subscribechangeLimit = page => {
  //   this.setState(state => {
  //     state.subscriptiontableOptions.limit = parseInt(page, 10);
  //     state.subscriptiontableOptions.skip = 0;
  //     state.subscriptiontableOptions.page.history = 1;
  //     state.subscriptiontableOptions.page.current = 1;
  //     state.count = 0;
  //     state.subscriptionactivePage = 1;
  //   });
  //   this.subscribepopulateData();
  // };
  // sort(field) {
  //   let sorticondef = "fa fa-sort";
  //   let id = ["name", "noofdays", "freedays", "employees", "createdAt", "amount", "description"];
  //   for (let i in id) {
  //     document.getElementById(id[i]).className = sorticondef;
  //   }
  //   this.setState(state => {
  //     state.sortOrder = !state.sortOrder;
  //   });
  //   this.setState(state => {
  //     state.tableOptions.order = state.sortOrder ? 1 : -1;
  //     state.tableOptions.field = field;
  //     this.populateData();
  //   });
  // }
  // subscribesort(field) {
  //   let sorticondef = "fa fa-sort";
  //   let id = ["plan.name", "plan.freedays", "plan.noofdays", "plan.employees", "plan.amount", "plan.noofdays", "createdAt", "type", "description"];
  //   for (let i in id) {
  //     document.getElementById(id[i]).className = sorticondef;
  //   }
  //   this.setState(state => {
  //     state.subscribesortOrder = !state.subscribesortOrder;
  //   });
  //   this.setState(state => {
  //     state.subscriptiontableOptions.order = state.subscribesortOrder ? 1 : -1;
  //     state.subscriptiontableOptions.field = field;
  //     this.subscribepopulateData();
  //   });
  // }
  // search(value) {
  //   this.setState(state => {
  //     state.tableOptions.search = value;
  //   });
  //   this.populateData();
  // }
  // subscribesearch(value) {
  //   this.setState(state => {
  //     state.subscriptiontableOptions.search = value;
  //   });
  //   this.subscribepopulateData();
  // }
  // filter(value) {
  //   this.setState(state => {
  //     if (value === "Name") {
  //       state.tableOptions.filter = "name";
  //     } else if (value === "Free days") {
  //       state.tableOptions.filter = "freedays";
  //     } else if (value === "No of days") {
  //       state.tableOptions.filter = "noofdays";
  //     } else if (value === "Amount") {
  //       state.tableOptions.filter = "amount";
  //     }
  //   });
  //   this.populateData();
  // }
  // subscribefilter(value) {
  //   this.setState(state => {
  //     if (value === "Name") {
  //       state.subscriptiontableOptions.filter = "name";
  //     } else if (value === "Free days") {
  //       state.subscriptiontableOptions.filter = "freedays";
  //     } else if (value === "No of days") {
  //       state.subscriptiontableOptions.filter = "noofdays";
  //     } else if (value === "Amount") {
  //       state.subscriptiontableOptions.filter = "amount";
  //     }
  //   });
  //   this.populateData();
  // }
  // paginate = data => {
  //   this.setState({ activePage: data });
  //   let history = this.state.tableOptions.page.history;
  //   let limit = this.state.tableOptions.limit;
  //   if (data) {
  //     this.setState(state => {
  //       if (history === "") {
  //         state.tableOptions.page.current = data;
  //         state.tableOptions.page.history = data;
  //         state.tableOptions.skip = data * limit - limit;
  //         this.populateData();
  //       } else if (history === data) {
  //         state.tableOptions.page.current = data;
  //         state.tableOptions.page.history = data;
  //         state.tableOptions.skip = data * limit - limit;
  //         this.populateData();
  //       } else {
  //         state.tableOptions.page.current = data;
  //         state.tableOptions.page.history = data;
  //         state.tableOptions.skip = data * limit - limit;
  //         state.bulk = [];
  //         state.count = 0;
  //         this.populateData();
  //       }
  //     });
  //   }
  // };
  // subscribepaginate = data => {
  //   this.setState({ subscriptionactivePage: data });
  //   let history = this.state.subscriptiontableOptions.page.history;
  //   let limit = this.state.subscriptiontableOptions.limit;
  //   if (data) {
  //     this.setState(state => {
  //       if (history === "") {
  //         state.subscriptiontableOptions.page.current = data;
  //         state.subscriptiontableOptions.page.history = data;
  //         state.subscriptiontableOptions.skip = data * limit - limit;
  //         this.subscribepopulateData();
  //       } else if (history === data) {
  //         state.subscriptiontableOptions.page.current = data;
  //         state.subscriptiontableOptions.page.history = data;
  //         state.subscriptiontableOptions.skip = data * limit - limit;
  //         this.subscribepopulateData();
  //       } else {
  //         state.subscriptiontableOptions.page.current = data;
  //         state.subscriptiontableOptions.page.history = data;
  //         state.subscriptiontableOptions.skip = data * limit - limit;
  //         state.bulk = [];
  //         state.count = 0;
  //         this.subscribepopulateData();
  //       }
  //     });
  //   }
  // };
  SubscribeInvoicePDF = () => {
    let toastId = "Subscribe_mail_invocie_01689";
    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/agency/subscription/downloadpdf",
      method: "POST",
      data: {
        TabOpt: this.state.tableOptions,
        for: "mail",
        page: "statement"
      }
    })
      .then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  SubscribeInvoicePDFDown = () => {
    let toastId = "Subscribe_Down_invocie_01968";
    client.defaults.responseType = "blob";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/agency/subscription/downloadpdf",
      method: "POST",
      data: {
        TabOpt: this.state.tableOptions,
        for: "download",
        page: "statement"
      }
    })
      .then(res => {
        if (res) {
          const file = new Blob([res], { type: "application/pdf" });
          FileSaver(file, "statement.pdf");
          toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
          client.defaults.responseType = "json";
      })
      .catch(error => {
        client.defaults.responseType = "json";
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
  };
  render() {
    // let order1 = this.state.subscribesortOrder;
    // let sorticon1 = `fa fa-sort${order1 === null ? "" : order1 === true ? "-amount-asc" : "-amount-desc"}`;
    // let sorticondef1 = "fa fa-sort";
    // if (this.state.subscriptiontableOptions.field) {
    //   if (document.getElementById(this.state.subscriptiontableOptions.field)) {
    //     document.getElementById(this.state.subscriptiontableOptions.field).className = sorticon1;
    //   }
    // }
    return (
      <Fragment>
        <div className="animated">
          <ToastContainer position="top-right" autoClose={2500} />
          <Card>
            <CardHeader>
              <i className="icon-list" />
              Subscription - Statement
              <div className="card-actions" style={tStyle}>
                <button style={tStyles} onClick={() => this.SubscribeInvoicePDFDown()}>
                  <i className="fa fa-download" /> Download
                  <small className="text-muted" />
                </button>
                <button style={tStyles} onClick={() => this.SubscribeInvoicePDF()}>
                  <i className="fa fa-envelope" /> Mail
                  <small className="text-muted" />
                </button>
              </div>
            </CardHeader>
            <CardBody>
              <Fragment>
                {/* <div className="row d-flex justify-content-end">
                  <div className="col-lg-auto pr-0">
                    <Input onChange={e => this.subscribefilter(e.target.value)} type="select" name="select" className="btn btn-primary rounded-0">
                      <option>All</option>
                      <option>Name</option>
                    </Input>
                  </div>
                  <div className="col-lg-3 pl-0">
                    <InputGroup>
                      <Input type="text" ref="subsearch" placeholder="Search" name="search" onChange={e => this.subscribesearch(e.target.value)} className="rounded-0" />
                      <Button
                        className="rounded-0"
                        color="primary"
                        id="clear"
                        onClick={() => {
                          ReactDOM.findDOMNode(this.refs.subsearch).value = "";
                          this.subscribesearch("");
                        }}
                      >
                        <i className="fa fa-remove" />
                      </Button>
                      <UncontrolledTooltip placement="top" target="clear">
                        Clear
                      </UncontrolledTooltip>
                    </InputGroup>
                  </div>
                </div> */}
                <Row>
                  <Col md="12" xs="12">
                    <button className="btn btn-primary pull-right hover-drop">
                      Options <i className="fa fa-caret-down" />
                      <ul className="rel-postion">
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/invoice");
                          }}
                        >
                          <button className="btn ch-views">View Invoice</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/editaccount");
                          }}
                        >
                          <button className="btn ch-views">Billing Accounts</button>
                        </li>
                        <li onClick={() => {
                            this.props.history.push({
                              pathname: "/agency/subscribe",
                              state: { activetab: "2" }
                            });
                          }}>
                          <button className="btn ch-views">Change Subscription</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/invoicelist");
                          }}
                        >
                          <button className="btn ch-views">View Statement</button>
                        </li>
                        <li onClick={() => {
                            this.props.history.push({
                              pathname: "/agency/subscribe",
                              state: { activetab: "1" }
                            });
                          }}>
                                <button className="btn ch-views">Current Subscription</button>
                              </li>
                      </ul>
                    </button>
                  </Col>
                </Row>
                <div className="single-invoice mt-2">
                  <div className="full-invoice">
                    <div className="left-heading-invoice">
                      <h4>
                        {" "}
                        Statement - <span> Invoices </span>{" "}
                      </h4>
                    </div>
                    <div className="right-heading-invoice">
                      <img
                        width="280px"
                        height="80px"
                        src={`${NodeURL}/${this.state.logo}`}
                        alt="Profile"
                        onError={() => {
                          this.setState({ logo: "../../img/user-profile.png" });
                        }}
                      />
                    </div>
                  </div>

                  <div className="invoice-forms">
                    <div className="col-md-6 no-pad">
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>To</p>
                        </div>
                        <div className="invoice-ans">
                          <p>{this.state && this.state.company_name} </p>
                          <p>{this.state && this.state.postal_address} </p>
                        </div>
                      </div>
                      {/* <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>From Date</p>
                        </div>
                        <div className="invoice-ans">
                          <p>1 November 2017</p>
                        </div>
                      </div>
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>To Date</p>
                        </div>
                        <div className="invoice-ans">
                          <p>1 November 2018</p>
                        </div>
                      </div>
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>As At</p>
                        </div>
                        <div className="invoice-ans">
                          <p>1 November 2018</p>
                        </div>
                      </div> */}
                    </div>
                    <div className="col-md-6 no-pad">
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>From</p>
                        </div>
                        <div className="invoice-ans">
                          <p>{this.state && this.state.siteaddress} </p>
                        </div>
                      </div>
                      {/* <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>VAT Number</p>
                        </div>
                        <div className="invoice-ans">
                          <p>80157985 </p>
                        </div>
                      </div> */}
                    </div>
                  </div>
                </div>

                <div className="table-responsive mt-2">
                  <Table hover bordered responsive size="sm">
                    <thead>
                      <tr>
                        {/* <th>S.No.</th> */}
                        <th
                        // onClick={() => {
                        //   this.subscribesort("plan.name");
                        // }}
                        >
                          Name
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.name" /> */}
                        </th>
                        <th
                        // onClick={() => {
                        //   this.subscribesort("plan.freedays");
                        // }}
                        >
                          Trial Period
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.freedays" /> */}
                        </th>
                        <th
                        // onClick={() => {
                        //   this.subscribesort("plan.noofdays");
                        // }}
                        >
                          Billing Period
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.noofdays" /> */}
                        </th>
                        <th
                        // onClick={() => {
                        //   this.subscribesort("plan.employees");
                        // }}
                        >
                          Employees Limit
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.employees" /> */}
                        </th>
                        <th
                        // onClick={() => {
                        //   this.subscribesort("plan.employees");
                        // }}
                        >
                          Recruitment Module
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.employees" /> */}
                        </th>

                        {/* <th
                        onClick={() => {
                          this.subscribesort("plan.description");
                        }}
                        >
                          Description
                           <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.amount" />
                        </th>*/}
                        <th>Date</th>
                        <th
                        // onClick={() => {
                        //   this.subscribesort("type");
                        // }}
                        >
                          Type
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="type" /> */}
                        </th>
                        <th
                          // onClick={() => {
                          //   this.subscribesort("createdAt");
                          // }}
                          className="d-none"
                        >
                          createdAt
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="createdAt" /> */}
                        </th>
                        <th
                        // onClick={() => {
                        //   this.subscribesort("plan.amount");
                        // }}
                        >
                          Amount
                          {/* <i style={{ paddingLeft: "25px" }} className={sorticondef1} id="plan.amount" /> */}
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.subscribedlist.length > 0 ? (
                        this.state.subscribedlist.map((item, i) => (
                          <tr key={item._id}>
                            {/* <td>{this.state.subscriptiontableOptions.skip + i + 1}</td> */}
                            <td>{item.plan.name}</td>
                            <td>
                              {item.plan.freedays} <small>Days</small>
                            </td>
                            <td>
                              {/* {item.plan.noofdays} <small>Days</small> */}
                              {item.plan.noofdays=== 30? "Monthly":item.plan.noofdays=== 365? "Yearly":""}
                            </td>
                            <td>{item.plan.employees}</td>
                            <td>{item.plan.recruitment_module === 1 ? "Yes" : "No"}</td>
                            {/*<td>{item.plan.description}</td>*/}
                            <td>
                              {moment(item.start).format("DD-MM-YYYY")} - {moment(item.end).format("DD-MM-YYYY")}
                            </td>
                            <td>{item.type}</td>
                            <td className="d-none">{item.createdAt}</td>
                            <td>£ {item.plan.amount}</td>
                          </tr>
                        ))
                      ) : (
                        <tr className="text-center">
                          <td colSpan={9}>
                            <h5>No record available</h5>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>

                  <div className="balance-gbr">
                    <div className="balance-dates">
                      <h5>Total Amount</h5>
                    </div>
                    <div className="balance-price">
                      <p>{this.state.subscribedlist && this.state.subscribedlist.length > 0 ? <Fragment>£ {this.state.subscribedlistTotal}</Fragment> : 0}</p>
                    </div>
                  </div>
                </div>
                {/* <nav className="float-left">
                  <Input onChange={e => this.subscribechangeLimit(e.target.value)} type="select" name="select">
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                  </Input>
                </nav>
                <nav className="float-right">
                  <div>
                    <Pagination
                      prevPageText="Prev"
                      nextPageText="Next"
                      firstPageText="First"
                      lastPageText="Last"
                      activePage={this.state.subscriptionactivePage}
                      itemsCountPerPage={this.state.subscriptiontableOptions.limit}
                      totalItemsCount={this.state.subscribedpages}
                      pageRangeDisplayed={this.state.subscriptionpageRangeDisplayed}
                      onChange={this.subscribepaginate}
                    />
                  </div>
                </nav> */}
                <br />
              </Fragment>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                color="secondary"
                title="Submit"
                onClick={() => {
                  this.props.history.push("/agency/subscribe");
                }}
              >
                <i className="fa fa-arrow-left mr-2" />
                Back
              </Button>
            </CardFooter>
          </Card>
        </div>
      </Fragment>
    );
  }
}

export default InvoiceList;
