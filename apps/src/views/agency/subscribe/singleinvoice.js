/* eslint no-sequences: 0*/
import React, { Component, Fragment } from "react";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { toast, ToastContainer } from "react-toastify";
import { Row, Col, Button, Card, CardBody, CardHeader, Table, CardFooter } from "reactstrap";
import request, { NodeURL, client } from "../../../api/api";
import FileSaver from "file-saver";
import moment from "moment";
// import ReactDOM from "react-dom";
// import Pagination from "react-js-pagination";

const tStyle = {
  cursor: "pointer"
};
const tStyles = {
  width: "125px"
};

class Singleinvoice extends Component {
  state = {
    adminlist: [],
    subscribedlist: [],
    currentlist: [],
    url: "",
    adminredirect: false,
    deletedisable: true,
    subscriptionlist: [],
    subscribesortOrder: true,
    loading: false,
    token_username: "",
    subscribedmsg: "",
    logo: "",
    siteaddress: "",
    postal_address: "",
    company_name: "",
    subscribedaccount: {}
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({
      token_username: token.username
    });
    this.subscribepopulateData();
    request({
      url: "/site/settings",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        if (res.response.settings) {
          this.setState({
            sitetitle: res.response.settings.site_title,
            email: res.response.settings.email_address,
            sitelocation: res.response.settings.location,
            siteaddress: res.response.settings.siteaddress,
            logo: res.response.settings.logo
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
    request({
      url: "/agency/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result) {
          this.setState({
            _id: res.response.result[0]._id,
            company_name: res.response.result[0].company_name,
            postal_address: res.response.result[0].postal_address,
            company_email: res.response.result[0].company_email
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  subscribepopulateData() {
    request({
      url: "/agency/subscription/subscriptions",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        const datas = res.response.current ? res.response.current : [];
        const subscribedlistTotal = datas && datas.map(list => list.plan.amount);
        const subscribedlistdesc = datas && datas.map(list => list.plan.description);
        const subscribedaccount = datas && datas.map(list => list.account_details);
        this.setState({
          currentlist: datas ? datas : [],
          // subscribedlist: res.response.history,
          // subscribedstartdate: res.response.history[0].start,
          // subscribedplan: res.response.history[0].plan,
          subscribedpages: res.response.fullcount,
          subscribedcurrPage: res.response.length,
          subscribedlistTotal: subscribedlistTotal && subscribedlistTotal.length > 0 ? subscribedlistTotal[0] : "",
          subscribedlistdesc: subscribedlistdesc && subscribedlistdesc.length > 0 ? subscribedlistdesc[0] : "",
          subscribedaccount: subscribedaccount && subscribedaccount.length > 0 ? subscribedaccount[0] : ""
        });
      } else if (res.status === 0) {
        this.setState({ subscribedmsg: res.response });
      }
    });
  }
  SubscribeInvoicePDF = () => {
    let toastId = "Subscribe_Down_invocie_022";

    toastId = toast.info("Sending......", { autoClose: false });
    request({
      url: "/agency/subscription/downloadpdf",
      method: "POST",
      data: {
        TabOpt: this.state.tableOptions,
        for: "download",
        page: "single"
      }
    })
      .then(res => {
        if (res.status === 1) {
          if ((res.msg = "Mail Sent")) {
            toast.update(toastId, { render: res.msg, type: toast.TYPE.SUCCESS, autoClose: 2500 });
            this.componentDidMount();
          }
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  SubscribeInvoicePDFDown = value => {
    let toastId = "Subscribe_Down_invocie_022";
    client.defaults.responseType = "blob";
    toastId = toast.info("Downloading......", { autoClose: false });
    request({
      url: "/agency/subscription/downloadpdf",
      method: "POST",
      data: {
        TabOpt: this.state.tableOptions,
        for: "mail",
        page: "single"
      }
    })
      .then(res => {
        if (res) {
          const file = new Blob([res], { type: "application/pdf" });
          FileSaver(file, "statement.pdf");
          toast.update(toastId, { render: "Downloaded", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
        client.defaults.responseType = "json";
      })
      .catch(error => {
        client.defaults.responseType = "json";
        toast.update(toastId, { render: error, type: toast.TYPE.ERROR, autoClose: 2500 });
      });
  };
  render() {
    return (
      <Fragment>
        <div className="animated">
          <ToastContainer position="top-right" autoClose={2500} />
          <Card>
            <CardHeader>
              <i className="icon-list" />
              Subscription - Invoice
              <div className="card-actions" style={tStyle}>
                <button style={tStyles} onClick={() => this.SubscribeInvoicePDFDown()}>
                  <i className="fa fa-download" /> Download
                  <small className="text-muted" />
                </button>
                <button style={tStyles} onClick={() => this.SubscribeInvoicePDF()}>
                  <i className="fa fa-envelope" /> Mail
                  <small className="text-muted" />
                </button>
              </div>
            </CardHeader>
            <CardBody>
              <Fragment>
                <Row>
                  <Col md="12" xs="12">
                    <button className="btn btn-primary pull-right hover-drop">
                      Options <i className="fa fa-caret-down" />
                      <ul className="rel-postion">
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/invoice");
                          }}
                        >
                          <button className="btn ch-views">View Invoice</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/editaccount");
                          }}
                        >
                          <button className="btn ch-views">Billing Accounts</button>
                        </li>
                        <li onClick={() => {
                            this.props.history.push({
                              pathname: "/agency/subscribe",
                              state: { activetab: "2" }
                            });
                          }}>
                          <button className="btn ch-views">Change Subscription</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/invoicelist");
                          }}
                        >
                          <button className="btn ch-views">View Statement</button>
                        </li>
                        <li onClick={() => {
                            this.props.history.push({
                              pathname: "/agency/subscribe",
                              state: { activetab: "1" }
                            });
                          }}>
                                <button className="btn ch-views">Current Subscription</button>
                              </li>
                      </ul>
                    </button>
                  </Col>
                </Row>
                <div className="single-invoice mt-2">
                  {/* <div className="invoice-paid">
                    <h3>Paid</h3>
                  </div> */}
                  <div className="full-invoice">
                    <div className="left-heading-invoice">
                      <h4>Tax Invoice</h4>
                    </div>
                    <div className="right-heading-invoice">
                      <img
                        width="280px"
                        height="80px"
                        src={`${NodeURL}/${this.state.logo}`}
                        alt="Profile"
                        onError={() => {
                          this.setState({ logo: "../../img/user-profile.png" });
                        }}
                      />
                    </div>
                  </div>
                  <div className="invoice-forms">
                    <div className="col-md-6 no-pad">
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>To</p>
                        </div>
                        <div className="invoice-ans">
                          {this.state.currentlist && this.state.subscribedaccount ? (
                            <Fragment>
                              <p>{this.state.subscribedaccount.name} </p>
                              <p>{this.state.subscribedaccount.address} </p>
                            </Fragment>
                          ) : (
                            <Fragment>
                              <p>{this.state && this.state.company_name} </p>
                              <p>{this.state && this.state.postal_address} </p>
                            </Fragment>
                          )}
                        </div>
                      </div>
                      {/* <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>From Date</p>
                        </div>
                        <div className="invoice-ans">
                          <p>1 November 2017</p>
                        </div>
                      </div>
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>To Date</p>
                        </div>
                        <div className="invoice-ans">
                          <p>1 November 2018</p>
                        </div>
                      </div>
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>As At</p>
                        </div>
                        <div className="invoice-ans">
                          <p>1 November 2018</p>
                        </div>
                      </div> */}
                    </div>
                    <div className="col-md-6 no-pad">
                      <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>From</p>
                        </div>
                        <div className="invoice-ans">
                          <p>{this.state && this.state.siteaddress} </p>
                        </div>
                      </div>
                      {/* <div className="mult-pages">
                        <div className="invoice-lab">
                          <p>VAT Number</p>
                        </div>
                        <div className="invoice-ans">
                          <p>80157985 </p>
                        </div>
                      </div> */}
                    </div>
                  </div>
                </div>
                <div className="table-responsive mt-2">
                  <Table hover bordered responsive size="sm">
                    <thead>
                      <tr>
                        {/* <th>S.No.</th>*/}
                        <th>Name</th>
                        <th>Trial Period</th>
                        <th>Billing Period</th>
                        <th>Employees Limit</th>
                        <th>Recruitment Module</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th className="d-none">createdAt</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.currentlist.length > 0 ? (
                        this.state.currentlist.map((item, i) => (
                          <tr key={item._id}>
                            {/* <td>{this.state.subscriptiontableOptions.skip + i + 1}</td>*/}
                            <td>{item.plan.name}</td>
                            <td>
                              {item.plan.freedays} <small>Days</small>
                            </td>
                            <td>
                            {item.plan.noofdays=== 30? "Monthly":item.plan.noofdays=== 365? "Yearly":""}
                            </td>
                            <td>{item.plan.employees}</td>
                            <td>{item.plan.recruitment_module === 1 ? "Yes" : "No"}</td>
                            <td>£ {item.plan.amount}</td>
                            <td>
                              {moment(item.start).format("DD-MM-YYYY")} - {moment(item.end).format("DD-MM-YYYY")}
                            </td>
                            <td>{item.type}</td>
                            <td className="d-none">{item.createdAt}</td>
                          </tr>
                        ))
                      ) : (
                        <tr className="text-center">
                          <td colSpan={9}>
                            <h5>No record available</h5>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                  <div className="invoice-des">
                    <div className="left-desp">
                      <h6>Description</h6>
                    </div>
                    <div className="right-desp">
                      <h6>Total Amount</h6>
                    </div>
                  </div>
                  <div className="invoice-des">
                    <div className="left-desp">
                      <p>{this.state.subscribedlistdesc}</p>
                    </div>
                    <div className="right-desp">
                      <p>£ {this.state.subscribedlistTotal}</p>
                    </div>
                  </div>

                  {/* <div className="balance-gbr">
                    <div className="balance-dates">
                      <p>Subtotal</p>
                    </div>
                    <div className="balance-price">
                      <p>000</p>
                    </div>
                  </div>
                  <div className="balance-gbr">
                    <div className="balance-dates">
                      <h5>Total VAT 20%</h5>
                    </div>
                    <div className="balance-price">
                      <p>5.20</p>
                    </div>
                  </div>
                  <div className="balance-gbr">
                    <div className="balance-dates">
                      <p>Less Amount Paid</p>
                    </div>
                    <div className="balance-price">
                      <p>5.20</p>
                    </div>
                  </div>  <div className="balance-gbr">
                    <div className="balance-dates">
                      <h5>Total Amount</h5>
                    </div>
                    <div className="balance-price">
                      <p>£ {this.state.subscribedlistTotal}</p>
                    </div>
                  </div>*/}
                </div>
                <br />
              </Fragment>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                color="secondary"
                title="Submit"
                onClick={() => {
                  this.props.history.push("/agency/subscribe");
                }}
              >
                <i className="fa fa-arrow-left mr-2" />
                Back
              </Button>
            </CardFooter>
          </Card>
        </div>
      </Fragment>
    );
  }
}

export default Singleinvoice;
