/*eslint no-sequences: 0*/
import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row } from "reactstrap";
import request from "../../../api/api";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
import Loader from "../../common/loader";

class Accounts extends Component {
  state = {
    address: "",
    name: "",
    email: "",
    number: "",
    code: "",
    phoneerror: false,
    loading: false,
    token_username: "",
    payment_details: [],
    stausactive: "",
    plans_details: []
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ token_username: token.username, isLoader: true });
    request({
      url: "/agency/subscription/accountdetails",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      if (res.status === 1) {
        if (res.response) {
          let data = res.response.result.data;
          let agency = res.response.agency;
          this.setState({
            _id: agency._id,
            name: agency.company_name,
            number: agency.company_phone.number,
            code: agency.company_phone.code,
            address: agency.postal_address,
            email: agency.company_email,
            payment_details: data,
            isLoader: false
            // logo_return: this.state.adminlist.company_logo,
            // company_logo_name: this.state.adminlist.company_logo.substr(24, 30)
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }

  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarfile: URL.createObjectURL(evt.target.files[0]),
      avatarName: evt.target.files[0].name
    });
  };

  OnFormSubmit = (e, values) => {
    if (this.state.number === "" || this.state.name === "" || this.state.email === "" || this.state.code === "" || this.state.address === "") {
      toast.error("Please fill all fields");
    } else {
      let account = {
        name: this.state.name,
        email: this.state.email,
        address: this.state.address,
        number: this.state.number,
        code: this.state.code
      };
      let prevdata = this.props.location && this.props.location.state.data;
      this.props.history.push({
        pathname: "/agency/subscriptionpayment",
        state: {
          data: {
            account_details: account,
            subid: prevdata.subid,
            plandetails: prevdata.plandetails,
            username: this.state.token_username
          }
        }
      });
    }
  };
  PayWithAccount = list => {
    let account = {
      name: list.metadata.name,
      email: list.metadata.email,
      address: list.metadata.address,
      number: list.metadata.number,
      code: list.metadata.code
    };
    let prevdata = this.props.location && this.props.location.state.data;
    this.props.history.push({
      pathname: "/agency/subscriptionpayment",
      state: {
        data: {
          account_details: account,
          subid: prevdata.subid,
          plandetails: prevdata.plandetails,
          username: this.state.token_username,
          db_charges: list
        }
      }
    });
  };
  render() {
    return (
      <div className="animated fadeIn">
        {this.state.loading ? <div className="loading">Loading&#8230;</div> : null}
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Billing Accounts
              </CardHeader>
              <AvForm ref={clear => (this.form = clear)} onValidSubmit={this.OnFormSubmit}>
                <CardBody>
                  <div className="accounts-billing">
                    <div className="payment-headings">
                      <ul>
                        <li>
                          {" "}
                          <span> 1 </span> Select Plan{" "}
                        </li>
                        <li className="active">
                          {" "}
                          <span> 2 </span> Select Billing Account{" "}
                        </li>
                        <li>
                          {" "}
                          <span> 3 </span> Review & Pay{" "}
                        </li>
                      </ul>
                    </div>

                    <p className="h5 text-center pb-2 text-uppercase">Pay Through An Existing Account</p>
                    <Row>
                      {this.state.payment_details && this.state.payment_details.length > 0 ? (
                        <Fragment>
                          {this.state.payment_details.map((list, i) => (
                            <Col xs="12" md="4" key={i}>
                              <div className="bill-account">
                                <div className="upper-bill">
                                  <h4>{list.metadata.name}</h4>
                                  <p>Email: {list.metadata.email}</p>
                                </div>
                                <div className="below-bill">
                                  <p>Payment by {list.object}</p>
                                  <p>Card Type: {list.brand}</p>
                                  <p>Card Number: **** **** **** {list.last4}</p>
                                  {/* <p>Name : {list.name}</p> */}
                                  <p>
                                    Expriy Date: {list.exp_month}/{list.exp_year}
                                  </p>
                                  <button type="button" className="btn text-capitalize" onClick={() => this.PayWithAccount(list)}>
                                    Pay with this account
                                  </button>
                                </div>
                              </div>
                            </Col>
                          ))}
                        </Fragment>
                      ) : (
                        <Fragment>
                          <Col className="d-flex justify-content-center text-center m-5">
                            {this.state.isLoader && <Loader />}
                            <h2>No Accounts Added</h2>
                          </Col>
                        </Fragment>
                      )}
                    </Row>
                  </div>
                  <p className="h5 text-center pb-2 text-uppercase">Or Continue To Payment</p>
                  <div className="acc-pages">
                    <div className="accounts-widths">
                      {/*<p className="h5">Basic Details</p>*/}
                      <Row>
                        <Col xs="12" md="12">
                          <AvGroup>
                            <Label>Name</Label>
                            <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} autoComplete="name" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="12" className={"for-phone-right"}>
                          <Label>Phone</Label>
                          <IntlTelInput style={{ width: "100%" }} defaultCountry={"gb"} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler} value={this.state.number} />
                          {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" md="12">
                          <AvGroup>
                            <Label>Email</Label>
                            <AvInput name="email" type="email" placeholder="Enter Email" onChange={this.onChange} value={this.state.email} autoComplete="email" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                        <Col xs="12" md="12">
                          <AvGroup>
                            <Label>Address</Label>
                            <AvInput type="textarea" name="address" placeholder="Enter address" onChange={this.onChange} value={this.state.address} autoComplete="name" required />
                            <AvFeedback>This is required!</AvFeedback>
                          </AvGroup>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </CardBody>
                <CardFooter>
                  <Button
                    type="submit"
                    color="secondary"
                    title="Submit"
                    onClick={() => {
                      this.props.history.push("/agency/subscribe");
                    }}
                  >
                    <i className={"fa fa-arrow-left"} /> Back
                  </Button>
                  <Button
                    type="submit"
                    color="success pull-right mb-3"
                    title="Submit"
                    onClick={() => {
                      this.phonefield(this.state.number);
                    }}
                  >
                    Continue To Payment <i className={"fa fa-arrow-right"} />
                  </Button>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Accounts;
