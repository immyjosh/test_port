/*eslint no-sequences: 0*/
// import { AvFeedback, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component, Fragment } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, Input } from "reactstrap";
import request from "../../../api/api";
// import IntlTelInput from "react-intl-tel-input";
// import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
// import "react-intl-tel-input/dist/main.css";
import Loader from "../../common/loader";

class Addagency extends Component {
  state = {
    address: "",
    name: "",
    email: "",
    number: "",
    code: "",
    phoneerror: false,
    loading: false,
    token_username: "",
    payment_details: [],
    stausactive: false,
    plans_details: []
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    this.setState({ token_username: token.username, isLoader: true });
    request({
      url: "/agency/subscription/accountdetails",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      if (res.status === 1) {
        if (res.response) {
          let data = res.response.result.data;
          let agency = res.response.agency;
          this.setState({
            _id: agency._id,
            name: agency.company_name,
            number: agency.company_phone.number,
            code: agency.company_phone.code,
            address: agency.postal_address,
            email: agency.company_email,
            stausactive: agency.auto_renewal,
            payment_details: data,
            isLoader: false
            // logo_return: this.state.adminlist.company_logo,
            // company_logo_name: this.state.adminlist.company_logo.substr(24, 30)
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
    request({
      url: "/agency/subscription/subscriptions/autorenewal",
      method: "POST",
      data: { value: value }
    }).then(res => {
      if (res.status === 1) {
        toast.success(res.response);
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  };
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    this.setState({
      avatar: evt.target.files[0],
      avatarfile: URL.createObjectURL(evt.target.files[0]),
      avatarName: evt.target.files[0].name
    });
  };

  DeleteAccount = list => {
    if (this.state.payment_details.length === 1 && this.state.stausactive === 1) {
      toast.error("Please Turn Off Auto Renewal Option");
    } else {
      let toastId = "Delete_account_01";
      toastId = toast.info("Please Wait......", { autoClose: false });
      request({
        url: "/agency/subscription/subscriptions/delete",
        method: "POST",
        data: { id: list.id }
      }).then(res => {
        if (res.status === 1) {
          toast.update(toastId, { render: "Account Deleted", type: toast.TYPE.SUCCESS, autoClose: 2500 });
          this.componentDidMount();
        } else if (res.status === 0) {
          toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
        }
      });
    }
  };
  UpdateAccount = list => {
    let toastId = "Update_account_01";
    toastId = toast.info("Please Wait......", { autoClose: false });
    request({
      url: "/agency/subscription/subscriptions/update",
      method: "POST",
      data: { id: list.id }
    }).then(res => {
      if (res.status === 1) {
        toast.update(toastId, { render: "Account Updated", type: toast.TYPE.SUCCESS, autoClose: 2500 });
        this.componentDidMount();
      } else if (res.status === 0) {
        toast.update(toastId, { render: res.response, type: toast.TYPE.ERROR, autoClose: 2500 });
      }
    });
  };
  render() {
    return (
      <div className="animated fadeIn">
        {this.state.loading ? <div className="loading">Loading&#8230;</div> : null}
        <ToastContainer position="top-right" autoClose={2500} />
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="icon-plus" />
                Edit Billing Accounts
              </CardHeader>
              <CardBody>
                <Row>
                  <Col md="12" xs="12">
                    <button className="btn btn-primary pull-right hover-drop">
                      Options <i className="fa fa-caret-down" />
                      <ul className="rel-postion">
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/invoice");
                          }}
                        >
                          <button className="btn ch-views">View Invoice</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/editaccount");
                          }}
                        >
                          <button className="btn ch-views">Billing Accounts</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push({
                              pathname: "/agency/subscribe",
                              state: { activetab: "2" }
                            });
                          }}
                        >
                          <button className="btn ch-views">Change Subscription</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push("/agency/subscription/invoicelist");
                          }}
                        >
                          <button className="btn ch-views">View Statement</button>
                        </li>
                        <li
                          onClick={() => {
                            this.props.history.push({
                              pathname: "/agency/subscribe",
                              state: { activetab: "1" }
                            });
                          }}
                        >
                          <button className="btn ch-views">Current Subscription</button>
                        </li>
                      </ul>
                    </button>
                  </Col>
                </Row>
                <div className="accounts-billing">
                  <p className="h5 text-center pb-2 text-uppercase">Billing Accounts</p>
                  {this.state.payment_details && this.state.payment_details.length > 0 ? (
                    <Row className="d-flex mb-3">
                      <Col md="2" xs="12">
                        <div className="left-pay">
                          <label>Auto Renewal</label>
                        </div>
                        <div className="right-pay">
                          <Label className="switch switch-text switch-success new-switch">
                            <Input type="checkbox" className="switch-input" checked={this.state.stausactive}  onChange={this.statusChangeses} />
                            <span className="switch-label" data-on="Yes" data-off="No" />
                            <span className="switch-handle new-handle" />
                          </Label>
                        </div>
                      </Col>
                    </Row>
                  ) : null}
                  <Row>
                    {this.state.payment_details && this.state.payment_details.length > 0 ? (
                      <Fragment>
                        {this.state.payment_details.map((list, i) => (
                          <Col xs="12" md="4" key={list.id}>
                            <div className="bill-account">
                              <div className="upper-bill">
                                <h4>{list.metadata.name}</h4>
                                <p>Email: {list.metadata.email}</p>
                                <div>
                                  <p>Plan Name: {list.metadata.planname}</p>
                                  <p>Billing Peroid: {Number(list.metadata.plannoofdays) === 30? "Monthly": Number(list.metadata.plannoofdays)=== 365? "Yearly":""}</p>
                                  <p>Employees Limit: {list.metadata.planemployee}</p>
                                  <p>Recruitment Module: {Number(list.metadata.planrecruitment_module) === 1 ? "Yes" : "No"}</p>
                                  <p>Description: {list.metadata.plandesc}</p>
                                </div>
                              </div>
                              <div className="below-bill">
                                <p>Payment by {list.object}</p>
                                <p>Card Type: {list.brand}</p>
                                <p>Card Number: **** **** **** {list.last4}</p>
                                {/* <p>Name : {list.name}</p> */}
                                <p>
                                  Expriy Date: {list.exp_month}/{list.exp_year}
                                </p>
                                <Row>
                                  <Col md="6">
                                    <button type="button" className="btn btn-outline-danger" onClick={() => this.DeleteAccount(list)}>
                                      Delete account
                                    </button>
                                  </Col>
                                  <Col md="6">
                                    {i !== 0 ? (
                                      <button type="button" className="btn btn-outline-danger" onClick={() => this.UpdateAccount(list)}>
                                        Set As Default
                                      </button>
                                    ) : null}
                                  </Col>
                                </Row>
                                {i === 0 ? (
                                  <small>
                                    <i class="fa fa-info-circle" />
                                    This Account & Plan Will be Applied Automatically For Auto-Renewal ( If Auto-Renewal Is On) .
                                  </small>
                                ) : null}
                              </div>
                            </div>
                          </Col>
                        ))}
                      </Fragment>
                    ) : (
                      <Fragment>
                        <Col className="d-flex justify-content-center text-center m-5">
                          {this.state.isLoader && <Loader />}
                          <h2>No Accounts Added</h2>
                        </Col>
                      </Fragment>
                    )}
                  </Row>
                </div>
              </CardBody>
              <CardFooter>
                <Button
                  type="submit"
                  color="secondary"
                  title="Submit"
                  onClick={() => {
                    this.props.history.push("/agency/subscribe");
                  }}
                >
                  <i className={"fa fa-arrow-left"} /> Back
                </Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Addagency;
