/* eslint no-unused-vars: [0, { "caughtErrors": "none" }]*/
/* eslint no-sequences: 0*/
import React, { Fragment } from "react";
import { CardCVCElement, CardExpiryElement, CardNumberElement, Elements, injectStripe, PostalCodeElement, StripeProvider } from "react-stripe-elements";
import { withRouter } from "react-router-dom";
import { Button, Card, CardBody, CardHeader, Col, Row, Input, Label } from "reactstrap";
import request, { client, NodeURL } from "../../../api/api";
import { toast, ToastContainer } from "react-toastify";
import "spinkit/css/spinkit.css";
import paymentnodify from "./subscribe";
import moment from "moment";
import { stripeURL } from "./stripe";

class _SplitForm extends React.Component {
  state = {
    paiddetails: "",
    spinkit: false,
    plandetailsData: "",
    stausactive: "",
    payment_details_length: ""
  };
  componentDidMount() {
    if (this.props.userdata) {
      this.setState({ plandetailsData: this.props.userdata.plandetails, accountData: this.props.userdata.account_details, db_charges: this.props.userdata.db_charges });
    }
    request({
      url: "/agency/subscription/accountdetails",
      method: "POST"
    }).then(res => {
      if (res.status === 1) {
        if (res.response) {
          this.setState({
            payment_details_length: res.response.result && res.response.result.data ? res.response.result.data.length : 0
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  handleSubmit = ev => {
    ev.preventDefault();
    if (this.props.stripe) {
      this.props.stripe.createToken().then(payload => {
        if (payload.error) {
          toast.error(payload.error.message ? payload.error.message : "Please Check Card Details");
          this.setState({ spinkit: false });
        } else {
          this.setState({ spinkit: true });
          if (payload.token) {
            const tokendetails = ("[token]", payload);
            if (tokendetails.token !== undefined) {
              const paiddetails = {
                subid: this.props.userdata.subid,
                username: this.props.userdata.username,
                plandetails: this.props.userdata.plandetails,
                account_details: this.props.userdata.account_details,
                auto_save: this.state.stausactive,
                payload: tokendetails.token
              };
              request({
                url: "/agency/subscription/payment",
                method: "POST",
                data: paiddetails
              }).then(res => {
                if (res.status === 1) {
                  if (res.auth_token) {
                    localStorage.setItem("APUSA", res.auth_token);
                    client.defaults.headers.common["Authorization"] = res.auth_token;
                    return (
                      this.props.data.data.history.push("/agency/subscribe"),
                      this.props.nodify.paymentnodify(),
                      setTimeout(() => {
                        window.location.reload(true);
                      }, 2600)
                    );
                  } else {
                    // return this.props.data.data.history.push("/agency/subscribe"), this.props.nodify.paymentnodify();
                    toast.error(res.response || "Error In Payment");
                  }
                } else if (res.status === 0) {
                  toast.error(res.response);
                  this.setState({
                    spinkit: false
                  });
                }
              });
            }
          }
        }
      });
    } else {
      this.setState({
        spinkit: false
      });
    }
  };
  paymentWithAccount = ev => {
    ev.preventDefault();
    this.setState({
      spinkit: true
    });
    const paiddetails = {
      subid: this.props.userdata.subid,
      username: this.props.userdata.username,
      plandetails: this.props.userdata.plandetails,
      account_details: this.props.userdata.account_details,
      payaccount: 1,
      payload: this.state.db_charges
    };
    request({
      url: "/agency/subscription/payment",
      method: "POST",
      data: paiddetails
    }).then(res => {
      if (res.status === 1) {
        if (res.auth_token) {
          localStorage.setItem("APUSA", res.auth_token);
          client.defaults.headers.common["Authorization"] = res.auth_token;
          return (
            this.props.data.data.history.push("/agency/subscribe"),
            this.props.nodify.paymentnodify(),
            setTimeout(() => {
              window.location.reload(true);
            }, 2600)
          );
        } else {
          return this.props.data.data.history.push("/agency/subscribe"), this.props.nodify.paymentnodify();
        }
      } else if (res.status === 0) {
        toast.error(res.response);
        this.setState({
          spinkit: false
        });
      }
    });
  };
  statusChangeses = e => {
    const value = e.target.checked ? 1 : 0;
    this.setState({
      stausactive: e.target.checked,
      status: value
    });
  };
  render() {
    let enddate = moment(new Date()).format("DD-MM-YYYY");
    if (this.state.plandetailsData) {
      enddate = moment(new Date())
        .add(Number(this.state.plandetailsData.noofdays), "days")
        .format("DD-MM-YYYY");
    }
    return (
      <Card>
        {this.state.spinkit ? <div className="loading">Loading&#8230;</div> : null}
        <ToastContainer position="top-right" autoClose={2500} />
        <CardHeader>
          <strong>
            <h4>Payment Details</h4>
          </strong>
        </CardHeader>
        <CardBody>
          <div className="payment-headings">
            <ul>
              <li>
                {" "}
                <span> 1 </span> Select Plan{" "}
              </li>
              <li>
                {" "}
                <span> 2 </span> Select Billing Account{" "}
              </li>
              <li className="active">
                {" "}
                <span> 3 </span> Review & Pay{" "}
              </li>
            </ul>
          </div>
          <Row>
            <Col md="6" xs="6">
              {this.state.plandetailsData && this.state.accountData ? (
                <Fragment>
                  {this.state.db_charges ? (
                    <div className="payment-plans">
                      <div className="upper-pays">
                        <h4>
                          {this.state.plandetailsData.name} - {this.state.db_charges.metadata.name}
                        </h4>
                        <p>
                          Your Plan starts from <strong> {moment().format("DD-MM-YYYY")} </strong> to <strong> {enddate} </strong>
                        </p>
                        <ul>
                          <li className="bolding-text">
                            <span className="pull-left"> Total Amount </span> <span className="pull-right">£{this.state.plandetailsData.amount} </span>{" "}
                          </li>
                        </ul>
                      </div>
                      <div className="billing-info">
                        <h6>Bill to</h6>
                        <h5>{this.state.db_charges.metadata.name}</h5>
                        <p>{this.state.db_charges.metadata.email}</p>
                        <p>
                          {this.state.db_charges && this.state.db_charges.metadata ? (
                            <Fragment>
                              {this.state.db_charges.metadata.code} - {this.state.db_charges.metadata.number}
                            </Fragment>
                          ) : null}
                        </p>
                        <p className="billing-add"> {this.state.db_charges.metadata.address} </p>
                      </div>
                    </div>
                  ) : (
                    <div className="payment-plans">
                      <div className="upper-pays">
                        <h4>
                          {this.state.plandetailsData.name} - {this.state.accountData.name}
                        </h4>
                        <p>
                          <p>
                            Your Plan starts from <strong> {moment().format("DD-MM-YYYY")} </strong> to <strong> {enddate} </strong>
                          </p>
                        </p>
                        <ul>
                          {/* <li className="bolding-text">
                        <span className="pull-left"> 300 Media Ltd </span> <span className="pull-right"> 27.50 </span>{" "}
                      </li>
                      <li>
                        <span className="pull-left"> Subtotal </span> <span className="pull-right"> 27.50 </span>{" "}
                      </li>
                      <li>
                        <span className="pull-left"> Less 5% Discount </span> <span className="pull-right"> 1.38 </span>{" "}
                      </li>
                      <li>
                        <span className="pull-left"> Discount Sub Total </span> <span className="pull-right"> 26.13 </span>{" "}
                      </li>
                      <li>
                        <span className="pull-left"> VAT </span> <span className="pull-right"> 5.23 </span>{" "}
                      </li>*/}
                          <li className="bolding-text">
                            <span className="pull-left"> Total Amount </span> <span className="pull-right">£ {this.state.plandetailsData.amount} </span>{" "}
                          </li>
                        </ul>
                      </div>
                      <div className="billing-info">
                        <h6>Bill to</h6>
                        <h5>{this.state.accountData.name}</h5>
                        <p>{this.state.accountData.email}</p>
                        <p>
                          {this.state.accountData ? (
                            <Fragment>
                              {this.state.accountData.code} - {this.state.accountData.number}
                            </Fragment>
                          ) : null}
                        </p>
                        <p className="billing-add"> {this.state.accountData.address} </p>
                      </div>
                    </div>
                  )}
                </Fragment>
              ) : (
                <h2> Please Wait</h2>
              )}
            </Col>
            <Col md="6" xs="6">
              {this.state.db_charges ? (
                <Row>
                  <div className="form-options">
                    <div className="group-control">
                      <Col md="12">
                        <label>Card number</label>
                      </Col>
                      <Col md="12">
                        <Input value={`**** **** **** ${this.state.db_charges && this.state.db_charges.last4}`} disabled />
                      </Col>
                    </div>
                  </div>
                  <div className="form-options">
                    <div className="group-control full-pay">
                      <Col md="6">
                        <div className="left-pay">
                          <label>Expiration date</label>
                        </div>
                        <div className="right-pay">
                          <Input value={`${this.state.db_charges && this.state.db_charges.exp_month}/${this.state.db_charges && this.state.db_charges.exp_year}`} disabled />
                        </div>
                      </Col>
                      <Col md="6">
                        <div className="left-pay">
                          <label>CVC</label>
                        </div>
                        <div className="right-pay">
                          <Input value={`***`} disabled />
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="form-options">
                    <div className="group-control">
                      <Col md="12">
                        <label>Postal Code</label>
                      </Col>
                      <Col md="12">
                        <Input value={this.state.db_charges && this.state.db_charges.address_zip} disabled />
                      </Col>
                    </div>
                  </div>
                  <div className="form-options ">
                    <div className="group-control">
                      <Col md="12">
                        <Button color="mb-1" size="lg" block title="Pay" type={"button"} onClick={this.paymentWithAccount}>
                          Authorise Payment
                        </Button>
                      </Col>
                    </div>
                  </div>
                  <div className="pay-strips">
                    <i className="fa fa-lock" /> &nbsp; Powered by <span className={"stripe_h"}>Stripe</span>
                  </div>
                </Row>
              ) : (
                <form onSubmit={this.handleSubmit}>
                  <div className="form-options">
                    <div className="group-control">
                      <Col md="12">
                        <label>Card number</label>
                      </Col>
                      <Col md="12">
                        <CardNumberElement />
                      </Col>
                    </div>
                  </div>
                  <div className="form-options">
                    <div className="group-control full-pay">
                      <Col md="6">
                        <div className="left-pay">
                          <label>Expiration date</label>
                        </div>
                        <div className="right-pay">
                          <CardExpiryElement />
                        </div>
                      </Col>
                      <Col md="6">
                        <div className="left-pay">
                          <label>CVC</label>
                        </div>
                        <div className="right-pay">
                          <CardCVCElement/>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="form-options">
                    <div className="group-control">
                      <Col md="12">
                        <label>Postal Code</label>
                      </Col>
                      <Col md="12">
                        <PostalCodeElement />
                      </Col>
                    </div>
                  </div>
                  {this.state.payment_details_length <= 6 ? (
                    <div className="form-options">
                      <div className="group-control full-pay">
                        <Col md="6">
                          <div className="left-pay">
                            <label>Save Account</label>
                          </div>
                          <div className="right-pay">
                            <Label className="switch switch-text switch-success new-switch">
                              <Input type="checkbox" className="switch-input" checked={this.state.stausactive} onChange={this.statusChangeses} />
                              <span className="switch-label" data-on="Yes" data-off="No" />
                              <span className="switch-handle new-handle" />
                            </Label>
                          </div>
                        </Col>
                      </div>
                    </div>
                  ) : null}
                  <div className="form-options">
                    <div className="group-control">
                      <Col md="12">
                        <Button color="mb-1" size="lg" block title="Pay">
                          Authorise Payment
                        </Button>
                      </Col>
                    </div>
                  </div>
                  <p className="pay-strips">
                    <i className="fa fa-lock" /> &nbsp; Powered by <span className={"stripe_h"}>Stripe</span>
                  </p>
				   <div className="pay-image">
				   <img src={`${NodeURL}/uploads/payment_icons.png`} alt="payment" />
				   </div>
                </form>
              )}
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}
const SplitForm = injectStripe(_SplitForm);
class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      elementFontSize: window.innerWidth < 450 ? "14px" : "18px"
    };
    window.addEventListener("resize", () => {
      if (window.innerWidth < 450 && this.state.elementFontSize !== "14px") {
        this.setState({ elementFontSize: "14px" });
      } else if (window.innerWidth >= 450 && this.state.elementFontSize !== "18px") {
        this.setState({ elementFontSize: "18px" });
      }
    });
  }
  render() {
    const { elementFontSize } = this.state;
    return (
      <div className="Checkout">
        <Elements>
          <SplitForm data={this.props} flash={this.props.flash} nodify={new paymentnodify()} fontSize={elementFontSize} userdata={this.props.details} />
        </Elements>
      </div>
    );
  }
}
class app extends React.Component {
  state = {
    admin_publishable_key: ""
  };
  nodify = new paymentnodify();
  adminsavenodify() {
    return toast.success("Make Payment!");
  }
  componentDidMount() {
    const paiddetails = {
      subid: this.props.location.state.data.subid,
      username: this.props.location.state.data.username,
      plandetails: this.props.location.state.data.plandetails,
      account_details: this.props.location.state.data.account_details,
      db_charges: this.props.location.state.data.db_charges
    };
    request({
      url: "/agency/subscription/payment",
      method: "POST",
      data: paiddetails
    })
      .then(res => {
        if (res.status === 1) {
          this.setState({
            admin_publishable_key: res.response.admin_publishable_key
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    return (
      <Fragment>
        {this.state.admin_publishable_key ? (
          <StripeProvider apiKey={this.state.admin_publishable_key}>
            <Checkout data={this.props} flash={toast} nodify={new paymentnodify()} details={this.props.location.state.data} />
          </StripeProvider>
        ) : null}
      </Fragment>
    );
  }
}
export default withRouter(app);
