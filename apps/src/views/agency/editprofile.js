/*eslint no-sequences: 0*/
import { AvFeedback, AvField, AvForm, AvGroup, AvInput } from "availity-reactstrap-validation";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Label, Row, PopoverHeader, Popover, PopoverBody } from "reactstrap";
import request, { NodeURL } from "../../api/api";
import editnodify from "./agencydashboard";
import deactivatenodify from "./agencylogin/agencylogin";
import { toast } from "react-toastify";
import IntlTelInput from "react-intl-tel-input";
import { libphonenumber } from "react-intl-tel-input/dist/libphonenumber.js";
import "react-intl-tel-input/dist/main.css";
// import PlacesAutocomplete, { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import Avatar from "react-avatar-edit";

class Editprofile extends Component {
  state = {
    username: "",
    password: "",
    name: "",
    email: "",
    confirm_password: "",
    conform: "",
    number: "",
    code: "",
    dailcountry: "",
    dailcountry1: "",
    number1: "",
    code1: "",
    company_email: "",
    company_logo: "",
    logo_file: "",
    company_logo_name: "",
    company_name: "",
    logo_return: "",
    postal_address: "",
    company_description: "",
    registration_number: "",
    organisation_type: "",
    address: "",
    line1: "",
    line2: "",
    city: "",
    state: "",
    country: "",
    zipcode: "",
    formatted_address: "",
    phoneerror: false,
    avatar: "",
    status: "",
    avatar_return: "",
    _id: "",
    avatarName: "",
    DeactivatepopoverOpen: false,
    propreview: null
  };
  flash = new editnodify();
  deactiveflash = new deactivatenodify();
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const token = this.props.token_role;
    if (!token || (token && (!token.username || token.role !== "agency"))) {
      return this.props.history.replace("/agency");
    }
    request({
      url: "/agency/profile",
      method: "POST",
      data: { id: token.username }
    }).then(res => {
      if (res.status === 1) {
        if (res.response.result) {
          let data = res.response.result[0];
          this.setState({
            _id: data._id,
            username: data.username,
            name: data.name,
            email: data.email,
            status: data.status,
            number: data.phone.number,
            code: data.phone.code,
            dailcountry: res.response.result[0].phone ? res.response.result[0].phone.dailcountry : "gb",
            address: data.formatted_address,
            line1: data.address.line1,
            line2: data.address.line2,
            city: data.address.city,
            state: data.address.state,
            country: data.address.country,
            zipcode: data.address.zipcode,
            formatted_address: data.address.formatted_address,
            avatar_return: data.avatar,
            surname: data.surname,
            avatarName: data.avatar.substr(24, 30),
            company_name: data.company_name,
            company_description: data.company_description,
            number1: data.company_phone.number,
            code1: data.company_phone.code,
            postal_address: data.postal_address,
            organisation_type: data.organisation_type,
            company_email: data.company_email,
            registration_number: data.registration_number
          });
        }
      } else if (res.status === 0) {
        toast.error(res.response);
      }
    });
  }
  /* handleChange = formatted_address => {
    this.setState({ formatted_address });
    this.setState({ formatted_address: formatted_address });
  };
  handleSelect = address => {
    this.setState({ address: address });
    this.setState({ formatted_address: address });
    geocodeByAddress(address)
      .then(results => {
        if (results[0].address_components.length === 1) {
          this.setState({
            country: results[0].address_components[0].long_name
          });
        } else if (results[0].address_components.length === 2) {
          this.setState({
            state: results[0].address_components[0].long_name,
            country: results[0].address_components[1].long_name
          });
        } else if (results[0].address_components.length === 3) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[1].long_name,
            country: results[0].address_components[2].long_name
          });
        } else if (results[0].address_components.length === 4) {
          this.setState({
            city: results[0].address_components[0].long_name,
            state: results[0].address_components[2].long_name,
            country: results[0].address_components[3].long_name
          });
        } else if (results[0].address_components.length === 5) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name
          });
        } else if (results[0].address_components.length === 6) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[2].long_name,
            state: results[0].address_components[3].long_name,
            country: results[0].address_components[4].long_name,
            zipcode: results[0].address_components[5].long_name
          });
        } else if (results[0].address_components.length === 7) {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[4].long_name,
            country: results[0].address_components[5].long_name,
            zipcode: results[0].address_components[6].long_name
          });
        } else if (results[0].address_components.length === 8) {
          this.setState({
            line1: results[0].address_components[0].long_name+" "+results[0].address_components[1].long_name,
            line2: results[0].address_components[2].long_name,
            city: results[0].address_components[3].long_name,
            state: results[0].address_components[5].long_name,
            country: results[0].address_components[6].long_name,
            zipcode: results[0].address_components[7].long_name
          });
        } else {
          this.setState({
            line1: results[0].address_components[0].long_name,
            line2: results[0].address_components[1].long_name,
            city: results[0].address_components[results[0].address_components.length - 3].long_name,
            state: results[0].address_components[results[0].address_components.length - 2].long_name,
            country: results[0].address_components[results[0].address_components.length - 1].long_name
          });
        }
        getLatLng(results[0]).then(latlong => {
          this.setState({ lat: latlong.lat });
          this.setState({ lon: latlong.lng });
        });
      })
      .catch(error => console.error("Error", error));
  };*/
  handler = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror: false
      });
    } else {
      this.setState({
        phoneerror: true
      });
    }
    this.setState({
      number: value,
      code: countryData.dialCode,
      dailcountry: countryData.iso2
    });
  };
  phonefield = value => {
    if (value === "") {
      this.setState({
        phoneerror: true
      });
    }
  };
  fileChangedHandler = evt => {
    if (evt) {
      const file_data = evt.target.files[0];
      if (file_data.size < 5242880) {
        this.setState({
          avatar: file_data,
          avatarName: file_data.name
        });
      } else {
        toast.error("Maximum upload size is 5MB");
      }
    }
  };
  handler1 = (status, value, countryData, number, id) => {
    if (status === true) {
      this.setState({
        phoneerror1: false
      });
    } else {
      this.setState({
        phoneerror1: true
      });
    }
    this.setState({
      number1: value,
      code1: countryData.dialCode,
      dailcountry1: countryData.iso2
    });
  };
  fileChangedHandler1 = evt => {
    this.setState({
      company_logo: evt.target.files[0],
      logo_file: URL.createObjectURL(evt.target.files[0]),
      company_logo_name: evt.target.files[0].name
    });
  };
  onEditAdmin = (e, values) => {
    if (this.state.number === "") {
      this.setState({
        phoneerror: true
      });
    } else {
      const data = new FormData();
      data.append("_id", this.state._id);
      data.append("page", "profile");
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("confirm_password", this.state.confirm_password);
      data.append("name", this.state.name);
      data.append("email", this.state.email);
      data.append("avatar", this.state.avatar);
      data.append("phone[code]", this.state.code);
      data.append("phone[number]", this.state.number);
      data.append("surname", this.state.surname);
      data.append("phone[dailcountry]", this.state.dailcountry);
      data.append("status", this.state.status);
      request({
        url: "/agency/profile/save",
        method: "POST",
        data: data
      })
        .then(res => {
          if (res.status === 1) {
            toast.success("Updated");
            this.componentDidMount();
            // this.editnodify();
          } else if (res.status === 0) {
            toast.error(res.response);
          }
        })
        .catch(error => {
          toast.success(error);
        });
    }
  };
  Deactivatetoggle = () => {
    this.setState({
      DeactivatepopoverOpen: !this.state.DeactivatepopoverOpen
    });
  };
  deactivateUser = () => {
    const data = new FormData();
    data.append("_id", this.state._id);
    data.append("status", 0);
    request({
      url: "/agency/profile/deactivate",
      method: "POST",
      data: data
    })
      .then(res => {
        if (res.status === 1) {
          this.userdeactive();
        } else if (res.status === 0) {
          toast.error(res.response);
        }
      })
      .catch(error => {
        toast.success(error);
      });
  };
  userdeactive() {
    return localStorage.removeItem("APUSA"), this.props.history.replace("/agency"), this.deactiveflash.deactivatenodify();
  }
  editnodify() {
    return this.props.history.replace("/agency/dashboard"), this.flash.updatenodify();
  }
  onClose = () => {
    this.setState({ propreview: null });
  };

  onCrop = propreview => {
    function base64ImageToBlob(propreview) {
      // extract content type and base64 payload from original string
      var pos = propreview.indexOf(";base64,");
      var type = propreview.substring(5, pos);
      var b64 = propreview.substr(pos + 8);
      // decode base64
      var imageContent = atob(b64);
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      var buffer = new ArrayBuffer(imageContent.length);
      var view = new Uint8Array(buffer);
      // fill the view, using the decoded base64
      for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
      // convert ArrayBuffer to Blob
      var blob = new Blob([buffer], { type: type });
      return blob;
    }
    let dataURItoBlob = base64ImageToBlob(propreview);
    this.setState({ propreview, avatar: dataURItoBlob });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <AvForm onValidSubmit={this.onEditAdmin}>
                <CardHeader>
                  <i className="icon-note" />
                  Edit Profile
                </CardHeader>
                <CardBody>
                 
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
				   <Col xs="12" md="12">
				    <p className="h5">Login Details</p>
					</Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Username</Label>
                        <AvInput type="text" name="username" placeholder="Enter Username" onChange={this.onChange} value={this.state.username} disabled />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvField name="email" label="Email" type="email" placeholder="Enter Email" value={this.state.email} onChange={this.onChange} required />
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Password</Label>
                        <AvInput type="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password} />
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Confirm Password</Label>
                        <AvInput type="password" name="confirm_password" placeholder="Enter Password" value={this.state.confirm_password} onChange={this.onChange} validate={{ match: { value: "password" } }} />
                        <AvFeedback>Match Password!</AvFeedback>
                      </AvGroup>
                    </Col>
					</div>
					</Col>
                  </Row>
                 
                  <Row>
				   <Col xs="12" md="12">
				   <div className="cus-design edited-section">
				   <Col xs="12" md="12">
				    <p className="h5">Basic Details</p>
					</Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <Label>Name</Label>
                        <AvInput type="text" name="name" placeholder="Enter Name" onChange={this.onChange} value={this.state.name} required />
                        <AvFeedback>This is required!</AvFeedback>
                      </AvGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <AvField
                        name="surname"
                        onChange={this.onChange}
                        value={this.state.surname}
                        placeholder="Enter"
                        label="Surname"
                        type="text"
                        errorMessage="This is required (Minimun 4 Letters)"
                        validate={{ required: { value: true }, minLength: { value: 4 } }}
                      />
                    </Col>
                    <Col xs="12" md="4">
                      <AvGroup>
                        <p>Tel</p>
                        {this.state && this.state.dailcountry ? (
                          <IntlTelInput style={{ width: "100%" }} defaultCountry={this.state.dailcountry} utilsScript={libphonenumber} css={["intl-tel-input", "form-control"]} onPhoneNumberChange={this.handler} value={this.state.number} />
                        ) : null}
                        {this.state.phoneerror ? <div style={{ color: "red" }}> Enter Valid Phone Number!</div> : null}
                      </AvGroup>
                    </Col>
					
                 
                    <Col xs="6" md="4">
                      {/* <Label for="exampleCustomFileBrowser">Profile Image</Label>
                      <CustomInput
                        type="file"
                        id="exampleCustomFileBrowser"
                        name="avatar"
                        onChange={this.fileChangedHandler}
                        label={this.state.avatarName ? this.state.avatarName : "Upload  Image"}
                        encType="multipart/form-data"
                      />*/}
                      <Avatar width={180} height={180} imageWidth={180} onCrop={this.onCrop} onClose={this.onClose} />
                    </Col>
                    {this.state.propreview ? (
                      <Col xs="6" md="3">
                        <Label>Preview</Label> <img width="180px" height="180px" src={this.state.propreview} alt="Preview" />{" "}
                      </Col>
                    ) : null}
                    <Col xs="6" md="3">
                      <Label>Profile Image</Label>
                      <img
                        width="180px"
                        height="180px"
                        src={NodeURL + "/" + this.state.avatar_return}
                        alt="Profile"
                        onError={() => {
                          this.setState({ avatar_return: "../../img/user-profile.png" });
                        }}
                      />
                    </Col>
					</div>
					</Col>
                  </Row>
                  <br />
                </CardBody>
                <CardFooter>
                  <Button type="submit" color="primary pull-right" title="Update">
                    <i
                      className="fa fa-dot-circle-o"
                      onClick={() => {
                        this.phonefield(this.state.number);
                      }}
                    />{" "}
                    Update
                  </Button>
                  <Link to="/agency/dashboard">
                    <Button type="submit" color="secondary" title="Back">
                      <i className="fa fa-arrow-left" /> Back
                    </Button>
                  </Link>

                  <Button id="Popover1" className="ml-5" color="danger" onClick={this.Deactivatetoggle}>
                    <i className="fa fa-remove" /> Deactivate Account
                  </Button>
                  <Popover placement="bottom" isOpen={this.state.DeactivatepopoverOpen} target="Popover1" toggle={this.Deactivatetoggle}>
                    <PopoverHeader>
                      <h5>Are You Sure ?</h5>
                    </PopoverHeader>
                    <PopoverBody>
                      <Button
                        className="ml-1"
                        color="danger"
                        onClick={() => {
                          this.deactivateUser();
                        }}
                        title="Deactivate Account"
                      >
                        Yes
                      </Button>
                      <Button className="ml-3" onClick={this.Deactivatetoggle}>
                        No
                      </Button>
                    </PopoverBody>
                  </Popover>
                </CardFooter>
              </AvForm>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Editprofile;
