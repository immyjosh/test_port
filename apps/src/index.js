import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import { ReactTitle } from "react-meta-tags";
// Styles
// Import Flag Icons Set
import "./../node_modules/flag-icon-css/css/flag-icon.min.css";
// Import Font Awesome Icons Set
import "./../node_modules/font-awesome/css/font-awesome.min.css";
// Import Simple Line Icons Set
import "./../node_modules/simple-line-icons/css/simple-line-icons.css";
// Import Main styles for this application
import "./scss/style.scss";
import "./../node_modules/react-datetime/css/react-datetime.css";
// Temp fix for reactstrap
import "./scss/core/_dropdown-menu-right.scss";
import "./scss/toast.scss";
import "./scss/admin-responsive.scss";

import Routing from "./containers/routing";
import Alllogin from "./views/common/alllogin";
import Adminlogin from "./views/admin/adminlogin";
import Agencylogin from "./views/agency/agencylogin/agencylogin";
import ClientAdminLogin from "./views/clients/clientlogin/clientlogin";
import adminforgotpassword from "./views/admin/forgotpassword";
import adminchangepassword from "./views/admin/changepassword";
import AgencyRegister from "./views/agency/agencylogin/register";
import agencyforgotpassword from "./views/agency/agencylogin/forgotpassword";
import agencychangepassword from "./views/agency/agencylogin/changepassword";
// import clientRegister from "./views/clients/clientlogin/register";
import clientforgotpassword from "./views/clients/clientlogin/forgotpassword";
import clientchangepassword from "./views/clients/clientlogin/changepassword";
import SocialLogin from "./views/agency/agencylogin/sociallogin";
import Loader from "./views/common/loader";
import Login from "./views/Template/Pages/Login/";
import Register from "./views/Template/Pages/Register/";
import Page404 from "./views/Template/Pages/Page404/";
import Page500 from "./views/Template/Pages/Page500/";
import { ToastContainer } from "react-toastify";

ReactDOM.render(
  <>
    <ReactTitle title="Agency Portal" />
    <ToastContainer position="top-right" autoClose={2500} />
    <BrowserRouter basename={"/portal"}>
      <Switch>
        <Route exact path="/" name="Login" component={Alllogin} />
        <Route exact path="/admin" name="AdminLogin" component={Adminlogin} />
        <Route exact path="/agency" name="AgencyLogin" component={Agencylogin} />
        <Route exact path="/sociallogin" name="SocialLogin" component={SocialLogin} />
        <Route exact path="/loader" name="Loader" component={Loader} />
        <Route exact path="/agency/register" name="AgencyRegister" component={AgencyRegister} />
        {/*<Route exact path="/client/register" name="clientRegister" component={clientRegister} />*/}
        <Route exact path="/client" name="ClientLogin" component={ClientAdminLogin} />
        <Route exact path="/admin/forgotpassword" name="adminforgotpassword" component={adminforgotpassword} />
        <Route exact path="/admin/resetpassword/:id/:resetcode" name="adminchangepassword" component={adminchangepassword} />
        <Route exact path="/agency/forgotpassword" name="agencyforgotpassword" component={agencyforgotpassword} />
        <Route exact path="/agency/resetpassword/:id/:resetcode" name="agencychangepassword" component={agencychangepassword} />
        <Route exact path="/client/forgotpassword" name="clientforgotpassword" component={clientforgotpassword} />
        <Route exact path="/client/resetpassword/:id/:resetcode" name="clientchangepassword" component={clientchangepassword} />
        <Route exact path="/loginpage" name="Login Page" component={Login} />
        <Route exact path="/register" name="Register Page" component={Register} />
        <Route exact path="/404" name="Page 404" component={Page404} />
        <Route exact path="/500" name="Page 500" component={Page500} />
        <Routing />
      </Switch>
    </BrowserRouter>
  </>,
  document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
